<?php $this->load->view('vwHeader');
$blog_category = array("1" => "Natuur Knowledge", "2"=>"Daily Wisdom", "3" => "DIY Workshop", "4"=> "Chemicals in Daily Life", "5" => "Health & Wellness", "6" => "Beauty");
//echo "<pre>";print_r($blog);echo "</pre>"; ?>
<style>
.header-nav{
	border-bottom: 1px solid #99cc33;
}
.section-title{
	font-size: 24px;text-decoration: underline;
	border-bottom:none;
	text-decoration:none;
	text-align:center;
	margin:0px;
	padding:15px;
}
.link li{padding-right:10px;}
h3, .h3 {
    font-size: 18px;
}
.database a{
	color: #99cc33 !important;
}

.database a:hover{
	text-decoration: underline;
}
</style>
<div class="body-content outer-top-xs" id="top-banner-and-menu" style="background-color:#fff;">
	<div class="row" style="background-color:#f3f3f3;color:#5f5f5f;">
	  <div class="col-xs-12 col-sm-12 col-md-1 homebanner-holder"></div>
		  <div class="col-xs-12 col-sm-12 col-md-11 homebanner-holder">
			<div class="info-boxes wow fadeInUp" style="">
				<div class=""  style="background-color:#f3f3f3;color:#5f5f5f;">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-lg-4">
							<div class="info-boxx">
								<div class="row">								
									<div class="" style="padding-left:0px;">
										<h1 style="font-size:28px;color:#5f5f5f;margin-top:8px;margin-bottom:0px;font-family: 'Open Sans', sans-serif; text-transform: uppercase;" class="info-box-heading green">&nbsp;&nbsp;&nbsp;<?php echo $blog->title;?></span></h1>
									</div>
								</div>
							</div>
						</div><!-- .col -->

						<div class="hidden-md col-sm-8 col-lg-8">
							<div class="info-box" style="padding: 16px 25px;">
								<div class="row">
									<ul style="color:#5f5f5f;">
										<li style="float:left;margin-left:15px;cursor:pointer;">
											<a href="<?php echo base_url();?>blog/category/natuur_knowledge"><strong>Natuur Knowledge</strong></a>
										</li>
										<li style="float:left;margin-left:15px;cursor:pointer;">
											<a href="<?php echo base_url();?>blog/category/daily_wisdom"><strong>Daily Wisdom</strong></a>
										</li>
										<li style="float:left;margin-left:15px;cursor:pointer;">
											<a href="<?php echo base_url();?>blog/category/diy_workshop"><strong>DIY Workshop</strong></a>
										</li>
										<li style="float:left;margin-left:15px;cursor:pointer;">
											<a href="<?php echo base_url();?>blog/category/chemicals_in_daily_life"><strong>Chemicals in Daily Life</strong></a>
										</li>
										<li style="float:left;margin-left:15px;cursor:pointer;">
											<a href="<?php echo base_url();?>blog/category/health_wellness"><strong>Health & Wellness</strong></a>
										</li>
										<li style="float:left;margin-left:15px;cursor:pointer;">
											<a href="<?php echo base_url();?>blog/category/beauty"><strong>Beauty</strong></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><!--
	<div class="row" style="background-color:#f3f3f3;color:#5f5f5f;border-bottom: 1px solid #9c3;">
		<div class="col-xs-12 col-sm-12 col-md-12 homebanner-holder">
			<div class="info-boxes wow fadeInUp" style="">
				<div class="" style="background-color:#f3f3f3;color:#5f5f5f;">
					<h3 class="section-title"><?php echo $blog->title;?></h3>
				</div>
			</div>
		</div>
	</div> -->
	<div class="container" style="min-height:400px;">
		<div class="row">
			<div class="col-md-12 col-sm-12" style="padding-left:30px;text-align:justify;font-size:18px;">
				<div><br/>
					<span class="pull-right" style="font-size:14px;"><a href="<?php echo base_url();?>blog">Blog</a> <i class="fa fa-angle-double-right" aria-hidden="true"></i> <a href=""><?php echo $blog_category[$blog->blog_cat];?></a> <i class="fa fa-angle-double-right" aria-hidden="true"></i> <strong><?php echo $blog->title;?></strong></span>
				</div><?php
				if(!empty($blog->blog_image)){?>
					<div><br/>
						<img src="<?php echo base_url()."uploads/blog/".$blog->blog_image;?>" class="img-responsive" style="width:100%;max-height:400px;">
					</div><?php
				}?>
				<div class="database">
				<?php echo $blog->content;?>
				</div>
				<span class="pull-right" style="font-size:14px;"><?php echo date("F d, Y", strtotime($blog->creation_time));?></span><br/><br/>
			</div>
		</div>
	</div>
</div><?php
$this->load->view('vwFooter'); ?>
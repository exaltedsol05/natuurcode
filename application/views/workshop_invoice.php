<?php $this->load->view('vwHeader');?>

<style>#showMenu{display:none;}</style>

<style>

@media print {

  body * {

    visibility: hidden;

  }

  #print_container, #print_container * {

    visibility: visible;

  }

  #print_container {

    position: absolute;

    left: 0;

    top: 0;

  }
}
</style>
<section class="container" id="order_success_container">
	<div class="row"><?php
		require('workshop_payment_invoice.php');?>
	</div>		
</section>
<script>
	$(document).ready(function(){
		$('#menu').click(function(){
			$('#showMenu').slideToggle('fast');
		});
	});
	
	function print_window_open(){
		window.print();
	}
</script>

<?php
$this->load->view('vwFooter');?>


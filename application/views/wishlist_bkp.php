<?php $this->load->view('vwHeader'); 
$tot_wish =  count($product_records);?>
<style>#showMenu{display:none;}</style>

<section class="container">
<?php if ($this->session->flashdata('message')):?>
			<div class="alert alert-info">
				<a class="close" data-dismiss="alert">×</a>
				<?php echo $this->session->flashdata('message');?>
			</div>
		<?php endif;?>
		
		<?php if ($this->session->flashdata('error')):?>
			<div class="alert alert-danger">
				<a class="close" data-dismiss="alert">×</a>
				<?php echo $this->session->flashdata('error');?>
			</div>
		<?php endif;?>
		
		<?php if (!empty($error)):?>
			<div class="alert alert-danger">
				<a class="close" data-dismiss="alert">×</a>
				<?php echo $error;?>
			</div>
		<?php endif;?>
		
		<?php if($tot_wish < 1):?>
			<div class="alert alert-info">
				<a class="close" data-dismiss="alert">×</a>
				<?php echo 'There are no products in your Wishlist!';?>
			</div>
		<?php else: ?>
	<div class="row">
	    <div class="col-sm-12">
	        <?php if ($this->session->flashdata('message')):?>
			<div class="alert alert-info">
				<a class="close" data-dismiss="alert">×</a>
				<?php echo $this->session->flashdata('message');?>
			</div>
		    <?php endif;?>
		
			<?php if ($this->session->flashdata('error')):?>
			<div class="alert alert-danger">
				<a class="close" data-dismiss="alert">×</a>
				<?php echo $this->session->flashdata('error');?>
			</div>
			<?php endif;?>
		
			<?php if (!empty($error)):?>
			<div class="alert alert-danger">
				<a class="close" data-dismiss="alert">×</a>
				<?php echo $error;?>
			</div>
			<?php endif;?>
	    </div>
	</div>
    <div class="row">
    	<div class="col-xs-12 col-sm-3">
        	<?php $this->load->view('user_account_sidebar');?>
        </div>	
    <div class="page-header" style="width:75%;float:right;">
        <h3><?php echo 'Your Wishlist';?></h3>
    </div>
    <?php echo form_open('cart/update_cart', array('id'=>'update_cart_form'));?>
    
    <?php include('checkout/wishlist_summary.php');?>
    
    
 
</form>
<?php endif; ?>
</section><?php $this->load->view('vwFooter'); ?>
<?php $this->load->view('vwHeader');

$password	= array('id'=>'password', 'class'=>'form-control', 'name'=>'password', 'value'=>'');
$confirm	= array('id'=>'confirm', 'class'=>'form-control', 'name'=>'confirm', 'value'=>''); ?>

<section class="container">
<div class="row">
	<div class="col-md-8 col-md-offset-2 ">
	
		<div class="contact-page">
		<h1 class="text-center"><?php echo $arrContact->title;?></h1>
		<hr>
		<form method="post" action="<?php echo base_url('secure/update_password'); ?>" id="up_form"  class="" autocomplete="off" />
			<div class="row">
				<div class="col-md-12 contact-form">
					<div class="col-md-12 contact-title text-center">
						<h3><?php echo $msg; ?></h3>
					</div>
					
					<?php if($flag){ ?>
						<div class="col-sm-12">
							<div class="row"> 
								<div class="col-sm-12">
									<div class="form-group">
										<input type="hidden" name="email1" value="<?php echo $email1; ?>">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label for="npw">New Password <span class="color-red"></span></label>
										<?php echo form_password($password);?>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label for="cnpw">Confirm New Password <span class="color-red"></span></label>
									   <?php echo form_password($confirm);?>
									  </div>
								</div>
							</div>                   
						</div>
						
						<div class="col-sm-12">
								<input type="submit" value="Submit" class="btn btn-sm btn-primary pull-right" />
						</div>
					<?php }else{ ?>
						<div class="col-md-12 contact-title text-center">
							<a href="<?php echo site_url('secure/login');?>" class="btn btn-primary">Login</a>
						</div>
					<?php } ?>
				</div>	
			</div><!-- /.contact-page -->
		</form>	
			
		</div>
    </div>
</div>
</section>
<script>
$(document).ready(function(){
	
	$("#up_form").validate({
		rules:{
			password:
			{
				required:true,
				minlength:6,
			},
			confirm:
			{
				equalTo: "#password"
			}
		},
        submitHandler: function (form){
     		form.submit();
        }
    });
});
</script>

<?php $this->load->view('vwFooter');?>
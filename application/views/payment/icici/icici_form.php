<?php ob_start();
$responseSuccessURL = base_url("checkout/icici_response");
$responseFailURL = base_url("checkout/icici_response");
$TXN_AMOUNT =$natuur['total'] + $natuur['shipping_cost'];
//$TXN_AMOUNT=10;
//Change below accesskey, secretkey to test 
$accesskey = 'e77d0dd0-f32b-11ea-af57-6bd6a40b1a92';
$secretkey = 'be288e83b4c926433d527beaf1372a50f983c25c';
//$remote_script = "https://sandbox-payments.open.money/layer";
//for production
$remote_script = "https://payments.open.money/layer";

//Changing environment to live requires remote_script also to be used for live and change accesskey,secretkey too
$environment = 'live';

/*
 * PHP Kit Name: Layer Payment - Open Payment Gateway
 * Plugin URI: https://open.money/
 * Description: Open's Layer Payment Gateway integration kit 
 * for PHP 5 and 7 compatible mode
 * Version: 1.0.2
 * Author: Openers
 * Author URI: https://open.money/
*/

/*
 * Class File: Open Payment Gateway Class
 * Version: 1.1
 * Author: Openers
 * Author URI: https://open.money/
*/
Class LayerApi{
	const BASE_URL_SANDBOX = "https://sandbox-icp-api.bankopen.co/api";
    const BASE_URL_UAT = "https://icp-api.bankopen.co/api";

    public function __construct($env,$access_key,$secret_key){

        $this->env = $env;
        $this->access_key = $access_key;
        $this->secret_key = $secret_key;
    }

    public function create_payment_token($data){

        try {
            $pay_token_request_data = array(
                'amount'   			=> (!empty($data['amount']))? $data['amount'] : NULL,
                'currency' 			=> (!empty($data['currency']))? $data['currency'] : NULL,
                'name'     			=> (!empty($data['name']))? $data['name'] : NULL,
                'email_id' 			=> (!empty($data['email_id']))? $data['email_id'] : NULL,
                'contact_number' 	=> (!empty($data['contact_number']))?  $data['contact_number'] : NULL,
                'mtx'    			=> (!empty($data['mtx']))? $data['mtx'] : NULL,
                'udf'    			=> (!empty($data['udf']))? $data['udf'] : NULL,
            );

            $pay_token_data = $this->http_post($pay_token_request_data,"payment_token");

            return $pay_token_data;
        } catch (Exception $e){			
            return [
                'error' => $e->getMessage()
            ];

        } catch (Throwable $e){
			
			return [
                'error' => $e->getMessage()
            ];
        }
    }

    public function get_payment_token($payment_token_id){

        if(empty($payment_token_id)){

            throw new Exception("payment_token_id cannot be empty.");
        }

        try {

            return $this->http_get("payment_token/".$payment_token_id);

        } catch (Exception $e){

            return [
                'error' => $e->getMessage()
            ];

        } catch (Throwable $e){

            return [
                'error' => $e->getMessage()
            ];
        }

    }

    public function get_payment_details($payment_id){

        if(empty($payment_id)){

            throw new Exception("payment_id cannot be empty.");
        }

        try {

            return $this->http_get("payment/".$payment_id);

        } catch (Exception $e){
			
            return [
                'error' => $e->getMessage()
            ];

        } catch (Throwable $e){

            return [
                'error' => $e->getMessage()
            ];
        }

    }


    function build_auth(){

        return array(                       
            'Content-Type: application/json',                                 
            'Authorization: Bearer '.$this->access_key.':'.$this->secret_key            
        );

    }


    function http_post($data,$route){

        foreach (@$data as $key=>$value){

            if(empty($data[$key])){

                unset($data[$key]);
            }
        }

        if($this->env == 'live'){

            $url = self::BASE_URL_UAT."/".$route;

        } else {

            $url = self::BASE_URL_SANDBOX."/".$route;
        }


        $header = $this->build_auth();
        
        try
        {
            $curl = curl_init();
		    curl_setopt($curl, CURLOPT_URL, $url);
		    curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		    curl_setopt($curl, CURLOPT_SSLVERSION, 6);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_MAXREDIRS,10);
		    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		    curl_setopt($curl, CURLOPT_ENCODING, '');		
		    curl_setopt($curl, CURLOPT_TIMEOUT, 60);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data, JSON_HEX_APOS|JSON_HEX_QUOT ));
            
		    $response = curl_exec($curl);
            $curlerr = curl_error($curl);
            
            if($curlerr != '')
            {
                return [
                    "error" => "Http Post failed.",
                    "error_data" => $curlerr,
                ];
            }
            return json_decode($response,true);
        }
        catch(Exception $e)
        {
            return [
                "error" => "Http Post failed.",
                "error_data" => $e->getMessage(),
            ];
        }           
        
    }

    function http_get($route){

        if($this->env == 'live'){

            $url = self::BASE_URL_UAT."/".$route;

        } else {

            $url = self::BASE_URL_SANDBOX."/".$route;
        }


        $header = $this->build_auth();

        try
        {
           
            $curl = curl_init();
		    curl_setopt($curl, CURLOPT_URL, $url);
		    curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		    curl_setopt($curl, CURLOPT_SSLVERSION, 6);
		    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		    curl_setopt($curl, CURLOPT_ENCODING, '');		
		    curl_setopt($curl, CURLOPT_TIMEOUT, 60);		   
            $response = curl_exec($curl);
            $curlerr = curl_error($curl);
            if($curlerr != '')
            {
                return [
                    "error" => "Http Get failed.",
                    "error_data" => $curlerr,
                ];
            }
            return json_decode($response,true);
        }
        catch(Exception $e)
        {
            return [
                "error" => "Http Get failed.",
                "error_data" => $e->getMessage(),
            ];
        }
    }
}



//Sample data
$sample_data = [
                'amount' => $TXN_AMOUNT,
                'currency' => 'INR',
                'name'  => $customer['bill_address']['firstname'].''.$customer['bill_address']['lastname'],
                'email_id' => $customer['bill_address']['email'],
                'contact_number' => $customer['bill_address']['phone'],
				'mtx' => ''
            ];


//Hash functions requried in both request and response
function create_hash($data,$accesskey,$secretkey){
    ksort($data);
    $hash_string = $accesskey;
    foreach ($data as $key=>$value){
        $hash_string .= '|'.$value;
    }
    return hash_hmac("sha256",$hash_string,$secretkey);
}

function verify_hash($data,$rec_hash,$accesskey,$secretkey){
    $gen_hash = create_hash($data,$accesskey,$secretkey);
    if($gen_hash === $rec_hash){
        return true;
    }
    return false;
}


//main logic
$error = '';
//$tranid=date("ymd").'-'.rand(1,100);
$tranid=$order_id;

$sample_data['mtx']=$tranid; //unique transaction id to be passed for each transaction 
$layer_api = new LayerApi($environment,$accesskey,$secretkey);
$layer_payment_token_data = $layer_api->create_payment_token($sample_data);
   
if(empty($error) && isset($layer_payment_token_data['error'])){
	$error = 'E55 Payment error. ' . ucfirst($layer_payment_token_data['error']);  
	if(isset($layer_payment_token_data['error_data']))
	{
		foreach($layer_payment_token_data['error_data'] as $d)
			$error .= " ".ucfirst($d[0]);
	}
}

if(empty($error) && (!isset($layer_payment_token_data["id"]) || empty($layer_payment_token_data["id"]))){				
    $error = 'Payment error. ' . 'Layer token ID cannot be empty.';        
}   

if(!empty($layer_payment_token_data["id"]))
    $payment_token_data = $layer_api->get_payment_token($layer_payment_token_data["id"]);
    
if(empty($error) && !empty($payment_token_data)){
    if(isset($layer_payment_token_data['error'])){
        $error = 'E56 Payment error. ' . $payment_token_data['error'];            
    }

    if(empty($error) && $payment_token_data['status'] == "paid"){
        $error = "Layer: this order has already been paid.";            
    }

    if(empty($error) && $payment_token_data['amount'] != $sample_data['amount']){
        $error = "Layer: an amount mismatch occurred.";
    }

    $jsdata['payment_token_id'] = html_entity_decode((string) $payment_token_data['id'],ENT_QUOTES,'UTF-8');
    $jsdata['accesskey']  = html_entity_decode((string) $accesskey,ENT_QUOTES,'UTF-8');
        
	$hash = create_hash(array(
        'layer_pay_token_id'    => $payment_token_data['id'],
        'layer_order_amount'    => $payment_token_data['amount'],
        'tranid'    => $tranid,
    ),$accesskey,$secretkey);
        
    $html =  "<form action='".$responseFailURL."' method='post' style='display: none' name='layer_payment_int_form'>
		<input type='hidden' name='layer_pay_token_id' value='".$payment_token_data['id']."'>
        <input type='hidden' name='tranid' value='".$tranid."'>
        <input type='hidden' name='layer_order_amount' value='".$payment_token_data['amount']."'>
        <input type='hidden' id='layer_payment_id' name='layer_payment_id' value='".time()."'>
        <input type='hidden' id='fallback_url' name='fallback_url' value='".$responseFailURL."'>
        <input type='hidden' name='hash' value='".$hash."'>
        </form>";
    $html .= "<script>";
    $html .= "var layer_params = " . json_encode( $jsdata ) . ';'; 
    
    $html .="</script>";
    $html .= '<script src="'.HTTP_JS_PATH.'/layer_checkout.js"></script>';
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Natuur :: Payment</title>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

<script src="<?php echo $remote_script; ?>"></script>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" >

</head>
<style type="text/css">
	.main {
		margin-left:30px;
		font-family:Verdana, Geneva, sans-serif, serif;
		text-align: center;
	}
	.text {
		float:left;
		width:180px;
	}
	.dv {
		margin-bottom:5px;
        margin-top:5px;
	}
	.logo {
		margin-bottom:20px;
        margin-top:5px;
	}
	.button{
		width: 100px;
    background: green;
    color: #fff;
    border: 0;
    font-size: 16px;
    margin-left: 20px;
    background: #ad4525;
    height: 42px;
    line-height: 42px;
    text-transform: capitalize;
    min-width: 100px;
	}
</style>
<body>
<center><h1>Please do not refresh this page...</h1></center>
<div class="main">
	<div class="logo">
		<img src="https://www.natuur.in/assets/img/logo/logo.png"  alt="Layer Payment" />
	</div>

	<div class="dv">
		<label>Full Name: <?php echo $sample_data['name']; ?></label>
	</div>
	<div class="dv">
		<label>E-mail: <?php echo $sample_data['email_id']; ?></label>
	</div>
	<div class="dv">
		<label>Mobile Number: <?php echo $sample_data['contact_number']; ?></label>
	</div>
	<div class="dv">
		<label>Amount: <?php echo $sample_data['currency'].' '.$TXN_AMOUNT; ?></label>
	</div>
		
	
	<div id="layerloader">
		
		<?php 
			if(!empty($error)) echo $error;
			if (isset($html)) { ?>
			<div class="dv">
				<input id="submit" name="submit" value="Pay Now" class="button" type="button" onclick="triggerLayer();">
				
			</div>
		<?php echo $html;
		}?>
	</div>
</div>

</body>
</html>
<?php
$this->load->view('vwHeader');
?>
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="<?php echo base_url();?>">Home</a></li>
				<li><a href="#"><?php echo $page_title;?></a></li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->
<section class="container">
<div class="row">
	<div class="col-sm-12">
		<div style="background-color: #fff;box-shadow: 0 2px 4px 0 rgba(0,0,0,.08);padding: 20px;overflow: hidden;">
		<h1 class="text-center"><?php echo $arrCancel->title;?></h1>
		<hr>
			<?php echo $arrCancel->content; ?>
		</div>
    </div>
</div>
</section>
<?php
$this->load->view('vwFooter');
?>
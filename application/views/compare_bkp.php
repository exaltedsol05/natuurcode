<?php $this->load->view('vwHeader'); 
$tot_wish =  count($product_records);?>
<style>#showMenu{display:none;}</style>

<section class="container">
<?php if ($this->session->flashdata('message')):?>
			<div class="alert alert-info">
				<a class="close" data-dismiss="alert">×</a>
				<?php echo $this->session->flashdata('message');?>
			</div>
		<?php endif;?>
		
		<?php if ($this->session->flashdata('error')):?>
			<div class="alert alert-danger">
				<a class="close" data-dismiss="alert">×</a>
				<?php echo $this->session->flashdata('error');?>
			</div>
		<?php endif;?>
		
		<?php if (!empty($error)):?>
			<div class="alert alert-danger">
				<a class="close" data-dismiss="alert">×</a>
				<?php echo $error;?>
			</div>
		<?php endif;?>
		
		<?php if($tot_wish < 1):?>
			<div class="alert alert-info">
				<a class="close" data-dismiss="alert">×</a>
				<?php echo 'There are no products in your Wishlist!';?>
			</div>
		<?php else: ?>
	<div class="row">
	    <div class="col-sm-12">
	        <?php if ($this->session->flashdata('message')):?>
			<div class="alert alert-info">
				<a class="close" data-dismiss="alert">×</a>
				<?php echo $this->session->flashdata('message');?>
			</div>
		    <?php endif;?>
		
			<?php if ($this->session->flashdata('error')):?>
			<div class="alert alert-danger">
				<a class="close" data-dismiss="alert">×</a>
				<?php echo $this->session->flashdata('error');?>
			</div>
			<?php endif;?>
		
			<?php if (!empty($error)):?>
			<div class="alert alert-danger">
				<a class="close" data-dismiss="alert">×</a>
				<?php echo $error;?>
			</div>
			<?php endif;?>
	    </div>
	</div>
    <!-- <div class="row">
    	<div class="col-md-3">
        	<div class="filter-box">
            	<div class="filter-col">
                    <h2 class="dash-head">My Account</h2>
                    <div class="show-filter">
                        <div class="filter">
                            <ul>
                                <li class="active">
                                	<a href="<?php echo site_url('secure/my_account');?>">ACCOUNT DASHBOARD</a>
                                </li>
                                <li>    
                                	<a href="<?php echo site_url('secure/my_information');?>">ACCOUNT INFORMATION</a>                                
                                </li>
                                <li>
                                	<a href="<?php echo site_url('secure/my_address');?>">ADDRESS BOOK</a>
                                </li>
                                <li>
                                	<a href="<?php echo site_url('secure/my_orders');?>">MY ORDERS</a>
                                </li>
                                
                                <li>
                                	<a href="#">RECURRING PROFILES</a>
                                </li>
                                
                                <li>
                                	<a href="<?php echo base_url(); ?>ratenreview_controller/wishlist">MY WISHLIST</a>
                                </li>
                                
                                <li><a href="#">MY REWARD POINTS</a></li>
                                
                                
                            </ul>
                        </div>
                        
                        
                    </div>                   
                </div>
                <div class="filter-col">
                    <h2 class="dash-head">Compare Products</h2>
                    <div class="show-filter">
                        <div class="filter"><?php
							$compare_count = $this->dentkart->compare_items();
							if($compare_count < 1){
								echo "<p>You have no items to compare.</p>";
							}else{
								echo "<p>&nbsp;&nbsp;&nbsp;You have ".$compare_count." items to compare.</p>";
							}	?>
                        </div>                 
                        
                    </div>                   
                </div>
            </div>
        </div>	 -->
    <div class="page-header" style="width:75%;float:right;">
        <h3><?php echo 'Your Compare Product';?></h3>
    </div>
    <?php echo form_open('cart/update_cart', array('id'=>'update_cart_form'));?>
    
    <?php include('checkout/compare_summary.php');?>
    
    
 
</form>
<?php endif; ?>
</section><?php $this->load->view('vwFooter'); ?>
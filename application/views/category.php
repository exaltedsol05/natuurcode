<?php $this->load->view('vwHeader');?>
<style>
.span1 {cursor:pointer; }

.minus, .plus{
	width: 45px;
    height: 51px;
    background: #f2f2f2;
    /* border-radius: 4px; */
    font-size: 16px;
    font-weight: bold;
    padding: 13px 0px 0px 0px;
    border: 1px solid #ddd;
    display: inline-block;
    vertical-align: middle;
    text-align: center;
}
.quantity1{
height: 52px;
    width: 76px;
    text-align: center;
    font-size: 26px;
    border: 1px solid #ddd;
    /* border-radius: 4px; */
    display: inline-block;
    vertical-align: middle;
}			
</style>
 <!--breadcrumbs area start-->
 <?php $background = '';
    if($category->image !=''){
        $background = "background:url(".base_url('uploads/images/full/'.$category->image).") no-repeat center !important";
    }

 ?>
    <div class="breadcrumbs_area" style="<?=$background?>">
        <div class="container">   
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <h3>Shop</h3>
                        <ul>
                            <?php $this->breadcrumbs->generate('category'); ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>         
    </div>
    <!--breadcrumbs area end-->
    
    <!--shop  area start-->
    <div class="shop_area shop_reverse">
        <div class="container">
            <div class="row">
                

			   <div class="col-lg-12 col-md-12">
                    <!--shop wrapper start-->
                    <!--shop toolbar start-->
                    <div class="shop_title">
                        <h1 style="text-align:center;"><?php echo $category->name;?></h1>
                    </div>
                    <div class="shop_banner">
                        <p><?php echo $category->description;?></p>
                    </div>
                    
                    <div class="shop_toolbar_wrapper">
                        <div class="shop_toolbar_btn">

                            <button data-role="grid_3" type="button" class=" btn-grid-3" data-toggle="tooltip" title="3"></button>

                            <button data-role="grid_4" type="button"  class=" btn-grid-4" data-toggle="tooltip" title="4"></button>

                            <button data-role="grid_list" type="button"  class="active btn-list" data-toggle="tooltip" title="List"></button>
                        </div>
                        <!-- <div class=" niceselect_option">                           
                                <select name="orderby" id="sort_products" onchange="window.location='<?php echo current_full_url();?>'+$(this).val();" >
                                   <option value=''><?php echo 'Default';?></option>
								<option <?php echo(!empty($_GET['by']) && $_GET['by']=='name/asc')?' selected="selected"':'';?> value="&by=name/asc"><?php echo 'Sort by name A to Z';?></option>
								<option <?php echo(!empty($_GET['by']) && $_GET['by']=='name/desc')?' selected="selected"':'';?>  value="&by=name/desc"><?php echo 'Sort by name Z to A';?></option>
								<option <?php echo(!empty($_GET['by']) && $_GET['by']=='price/asc')?' selected="selected"':'';?>  value="&by=price/asc"><?php echo 'Sort by price Low to High';?></option>
								<option <?php echo(!empty($_GET['by']) && $_GET['by']=='price/desc')?' selected="selected"':'';?>  value="&by=price/desc"><?php echo 'Sort by price High to Low';?></option>
                                </select>
                        </div> -->
                        <div class="page_amount">
						    <div class="pagination">
                            <?php echo $this->pagination->create_links();?>&nbsp;
							</div>
                        </div>
                    </div>
                    <!--shop toolbar end-->

                     <div class="row shop_wrapper grid_list">
					    <?php foreach($products as $product){
							
							$discount=round(100*($product->price-$product->saleprice)/$product->price);
							$photo  = theme_img('no_picture.png', 'No Image Available');
							
                           	$product->images    = array_values($product->images);
                          
                           	if(!empty($product->images[0])){
                           		$primary    = $product->images[0];
                           		foreach($product->images as $photo){
                           			if(isset($photo->primary)){
                           				$primary    = $photo;
                           			}
                           		}
                           
                           		$photo  = '<img src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'"/>';
                           }
						   if(!empty($product->images[1])){
                           		$secondry    = $product->images[1];
                           		
                           
                           		$photo1  = '<img src="'.base_url('uploads/images/medium/'.$secondry->filename).'" alt="'.$product->seo_title.'"/>';
                           }
						   
						   ?>
                        <div class="col-12 ">
                            <div class="single_product cat">
                                <div class="product_thumb">
                                    <a class="primary_img" href="<?php echo base_url($product->slug); ?>"><?php echo $photo;?></a>
                                    <a class="secondary_img" href="<?php echo base_url($product->slug); ?>"><?php echo $photo1;?></a>
									<div class="wishlist_view_only myWishlistViewOnly">
										<?php if($this->Customer_model->is_logged_in(false, false)){?>
											<a class="btn" onclick="add_to_wishlist(<?php echo $product->id;?>);" href="javascript:;" title="Wishlist">
											<i class="ion-android-favorite-outline"></i></a>
										<?php
											}else{
										?>
											<a class="btn" href="#" title="Wishlist" aria-hidden="true" data-toggle="modal" data-target="#myLoginModal">
											<i class="ion-android-favorite-outline"></i></a>
										<?php } ?>
									</div>
                                    <div class="label_product">
                                        <span class="label_sale">new</span>
                                    </div>                                    
                                </div>	
                                <div class="product_content grid_content">
                                    <div class="product_name">
                                        <h4><a href="<?php echo base_url($product->slug); ?>"><?php echo $product->name;?></a></h4>
                                    </div>
                                    <div class="product_rating" product_size="<?php echo $product->product_size;?>">
                                        <ul>
                                            <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                                            <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                                            <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                                            <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                                            <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price-container">
                                         <div class="price_box">
                                                <?php 
												$price_design = $this->natuur->get_natuur_currency_function($product->price,$product->saleprice);
												echo $price_design['price_design'];
											?>									
                                        </div>
                                       <!--<div class="wishlist_btn">
                                            <a href="wishlist.html" title="wishlist"><i class="ion-android-favorite-outline"></i></a>
                                        </div>-->
										<div class="action_links">
											<div class="number number_quantity">
												<span class="minus span1" >-</span>
													<input type="text" value="1" id="quantity<?php echo $product->id;?>" class="quantity1" name="quantity"/>
												<span class="plus span1">+</span>
												<div class="quick_view"><a class="btn" href="#" data-toggle="modal" data-target="#modal_box" title="Quick View"><i class="ion-eye"></i></a></div>
											</div>
											<div class="grid_cart_details">
												<div class="add_to_cart"><a class="btn add_to_cart_button" title="add to cart"href="javascript:void(0);" onclick="addToCartProduct(<?php echo $product->id;?>);">Add to Bag</a></div>
											</div>
										</div>
                                    </div>
                                </div>
                                <div class="product_content list_content">
                                    <div class="product_name">
                                       <h4><a href="<?php echo base_url($product->slug); ?>"><?php echo $product->name;?></a></h4>
                                    </div>
                                    <div class="product_rating">
                                        <ul>
                                            <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                                            <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                                            <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                                            <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                                            <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                                        </ul>
                                    </div>
                                     <div class="price_box">
                                        <?php 
											$price_design = $this->natuur->get_natuur_currency_function($product->price,$product->saleprice);
											echo $price_design['price_design'];
										?>		 
                                    </div>
                                    <div class="product_desc">
                                        <p><?php echo $product->excerpt;?></p>
                                    </div>									
                                    <div class="action_links">
										<div class="number number_quantity">
											<span class="minus span1" >-</span>
												<input type="text" value="1" id="quantity<?php echo $product->id;?>" class="quantity1" name="quantity"/>
											<span class="plus span1">+</span>
										</div>
										<div class="grid_cart_details">
                                            <div class="add_to_cart"><a class="btn add_to_cart_button" title="add to cart"href="javascript:void(0);" onclick="addToCartProduct(<?php echo $product->id;?>);">Add to Bag</a></div>
                                            <div class="wishlist_view_only">
											<?php if($this->Customer_model->is_logged_in(false, false)){?>
												<a class="btn" onclick="add_to_wishlist(<?php echo $product->id;?>);" href="javascript:;" title="Wishlist">
												<i class="ion-android-favorite-outline"></i></a>
											<?php
												}else{
											?>
												<a class="btn" href="#" title="Wishlist" aria-hidden="true" data-toggle="modal" data-target="#myLoginModal">
												<i class="ion-android-favorite-outline"></i></a>
											<?php } ?>
											</div>
											<div class="quick_view" rel="<?php echo $product->id;?>"><a class="btn" href="javascript:void(0)" title="Quick View"><i class="ion-eye"></i></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<?php } ?>
					</div>

                    <div class="shop_toolbar t_bottom">
                        <div class="pagination">
                            <!--<ul>
                                <li class="current">1</li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li class="next"><a href="#">next</a></li>
                                <li><a href="#">>></a></li>
                            </ul>-->
							<?php echo $this->pagination->create_links();?>
                        </div>
                    </div>
                    <!--shop toolbar end-->
                    <!--shop wrapper end-->
                </div>
            </div>
        </div>
    </div>
	<!-- modal area start-->
    <div class="modal fade" id="modal_box" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body" id="quick_product_details">
                    
                </div>    
            </div>
        </div>
    </div>
    <!-- modal area end-->
    <!--shop  area end-->
    <?php 
   function theme_img($uri, $tag=false){
   	if($tag){
   		return '<img src="'.theme_url('assets/assets/images/'.$uri).'" alt="'.$tag.'">';
   	}else{
   		return theme_url('assets/assets/images/'.$uri);
   	}
   	
   }
   function theme_url($uri){
   	$CI =& get_instance();
   	return $CI->config->base_url('/'.$uri);
   }
   ?>
<?php $this->load->view('vwFooter');?>
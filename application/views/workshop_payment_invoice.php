	<div class="col-md-12"><h3 class="success_thankyou">THANK YOU!<span>You successfully booked workshop!!</span></h3></div>
	<div class="col-md-12"><h5 class="order_summary">Booking Summary</h5></div>
	<div class="col-md-12">
		<div class="my_order_section">
			<div class="row">
				<div class="col-md-4 col-sm-6 col-xs-12">
					<strong>Transaction ID:</strong> <br/><?php echo $booking_details->payment_id;?>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12">
					<strong>Workshop Date:</strong> <br/><?php echo $workshop_details->workshop_date;?>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12">
					<strong>Workshop Time:</strong> <br/><?php echo $workshop_details->workshop_time;?>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Meta -->
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<link rel="shortcut icon" href="<?php echo HTTP_IMAGES_PATH; ?>favicon.png" type="images/png">
	    <title>Natuur</title>
		<meta name="Keywords" content="Grocery Shop, Grocery Shop eCommerce, Grocery Shop">
		<meta name="Description" content="Grocery Shop is an big ecommerce shopping site">
		
		<!-- Bootstrap Core CSS -->
	    <link rel="stylesheet" href="<?php echo HTTP_CSS_PATH; ?>bootstrap.min.css">
	    
	    <!-- Customizable CSS -->
	    <link rel="stylesheet" href="<?php echo HTTP_CSS_PATH; ?>main.css">
	    <link rel="stylesheet" href="<?php echo HTTP_CSS_PATH; ?>blue.css">
	    <link rel="stylesheet" href="<?php echo HTTP_CSS_PATH; ?>owl.carousel.css">
		<link rel="stylesheet" href="<?php echo HTTP_CSS_PATH; ?>owl.transitions.css">
		<link rel="stylesheet" href="<?php echo HTTP_CSS_PATH; ?>animate.min.css">
		<link rel="stylesheet" href="<?php echo HTTP_CSS_PATH; ?>rateit.css">
		<link rel="stylesheet" href="<?php echo HTTP_CSS_PATH; ?>bootstrap-select.min.css">
		<link rel="stylesheet" href="<?php echo HTTP_CSS_PATH; ?>lightbox.css">
		<link rel="stylesheet" href="<?php echo HTTP_CSS_PATH; ?>star-rating.min.css">

		

		
		<!-- Icons/Glyphs -->
		<link rel="stylesheet" href="<?php echo HTTP_CSS_PATH; ?>font-awesome.css">

        <!-- Fonts --> 
		<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,600italic,700,700italic,800' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
		<script src="<?php echo HTTP_JS_PATH; ?>jquery-1.11.1.min.js"></script>
		<script src="<?php echo HTTP_JS_PATH; ?>bootstrap.min.js"></script>
		<script src="<?php echo HTTP_JS_PATH; ?>jquery.validate.min.js"></script>
		
		<script src="<?php echo HTTP_JS_PATH; ?>bootstrap-hover-dropdown.min.js"></script>
		<script src="<?php echo HTTP_JS_PATH; ?>owl.carousel.min.js"></script>
		<script src="<?php echo HTTP_JS_PATH; ?>jquery.blockUI.js"></script>
		<script src="<?php echo HTTP_JS_PATH; ?>jquery.spin.js"></script>
		<script src="<?php echo HTTP_JS_PATH; ?>echo.min.js"></script>
		<script src="<?php echo HTTP_JS_PATH; ?>jquery.easing-1.3.min.js"></script>
		<!--<script src="<?php echo HTTP_JS_PATH; ?>bootstrap-slider.min.js"></script>-->
		<script src="<?php echo HTTP_JS_PATH; ?>jquery.rateit.min.js"></script>
		<script src="<?php echo HTTP_JS_PATH; ?>lightbox.min.js"></script>
		<script src="<?php echo HTTP_JS_PATH; ?>bootstrap-select.min.js"></script>
		<script src="<?php echo HTTP_JS_PATH; ?>wow.min.js"></script>
		<script src="<?php echo HTTP_JS_PATH; ?>scripts.js"></script>
		<script src="<?php echo HTTP_JS_PATH; ?>star-rating.min.js"></script>
		
		<style>
			.custom{position:relative !important;}
		</style>
		
	</head>

<body class="cnt-home">
    <?php
    $pg = isset($page) && $page != '' ?  $page :'home'  ;    
    ?>

<!-- ============================================== HEADER ============================================== -->
<header class="header-style-1">

	<!-- ============================================== TOP MENU ============================================== -->
<div class="top-bar animate-dropdown">
	<div class="container">
		<div class="header-top-inner">
			<div class="cnt-account">
				<ul class="list-unstyled">
                <?php if($this->Customer_model->is_logged_in(false, false)):?>
				<li><a href="<?php echo  site_url('secure/my_account');?>"><i class="icon fa fa-user"></i>My Account</a></li>
				<li><a href="<?php echo base_url().'ratenreview_controller/my_wishlist'; ?>"><i class="icon fa fa-heart"></i>Wishlist</a></li>
				<li><a href="<?php echo base_url().'cart/view_cart'; ?>"><i class="icon fa fa-shopping-cart"></i>My Cart</a></li>
				<li><a href="<?php echo base_url()."cart/update_cart/checkout";  ?>"><i class="icon fa fa-check"></i>Checkout</a></li>
				<li><a href="<?php echo site_url('secure/logout');?>">Logout</a></li>
				<?php else: ?>
					<!--<li><a href="<?php echo site_url('secure/register');?>">Sign up</a> |</li>
					<li><a href="<?php echo site_url('secure/login');?>"><i class="icon fa fa-lock"></i>Login</a> |</li>-->
					<li><a href="#" data-toggle="modal" data-target="#signup">Sign up</a> |</li>
					<li><a href="#" data-toggle="modal" data-target="#authentication"><i class="icon fa fa-lock"></i>Login</a> |</li>
				<?php endif; ?>
			    
				</ul>
			</div><!-- /.cnt-account -->

			
			<div class="clearfix"></div>
		</div><!-- /.header-top-inner -->
	</div><!-- /.container -->
</div><!-- /.header-top -->
<!-- ============================================== TOP MENU : END ============================================== -->
	<div class="main-header">
		<div class="container">
		<div id="productAlerts"></div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-3 logo-holder">
					<!-- ============================================================= LOGO ============================================================= -->
					<div class="logo">
						<a href="<?php echo base_url(); ?>">
							<img src="<?=HTTP_IMAGES_PATH;?>newLogo.png" alt="">
						</a>
					</div><!-- /.logo -->
					<!-- ============================================================= LOGO : END ============================================================= -->				
				</div><!-- /.logo-holder -->

				<div class="col-xs-12 col-sm-12 col-md-7 top-search-holder">
					<!-- /.contact-row -->
				<!-- ============================================================= SEARCH AREA ============================================================= -->
<div class="search-area">


   <?php echo form_open('cart/search', 'id="search_frm"');?>
        <div class="control-group"><?php
			$option = array(" " => Categories);
			$search_cat = $this->menus->get_search_menu(0);
            echo '<select name="search_cat_product" id="search_cat_product" style="height:42px; border:none; background-color:#f6f6f6; padding:10px; color:#666;border-right: 1px solid #eee;">';
				echo "<option value='0'>Pick Category</option>";
				foreach($search_cat as $catName){
					echo "<option value='".$catName -> id."'>".ucwords(strtolower($catName -> name))."</option>";
				}	
			echo "</select>";?>
			
            <input type="search" class="search-field" placeholder="Search here.." id="product_search" autocomplete="off" name="term">
            <button type="button" id="header_search_submit" class="search-button"><i class="icon icon-header-search"></i></button>
			
			<div id="serch_box" style="margin-top: -3px;display: none;position: absolute;z-index: 99999;height: auto;max-height: 250px;width: 653px;border: 1px solid #ddd;background: #fff;overflow: auto;border-bottom-right-radius: 5px;border-bottom-left-radius: 5px;">
				<ul id="finalResult"></ul>
			</div>

        </div>
    </form>
</div><!-- /.search-area -->
<!-- ============================================================= SEARCH AREA : END ============================================================= -->				</div><!-- /.top-search-holder -->

<div class="col-xs-12 col-sm-12 col-md-2 animate-dropdown top-cart-row">
<!-- ============================================================= SHOPPING CART DROPDOWN ============================================================= -->
<?php
$cartarray =  $this->dentkart->contents();
if(count($cartarray) > 0){
	$total = 0;
	foreach ($this->dentkart->contents() as $cartkey=>$product):  $i++;
	$prdtotal = $product['quantity'] *$product['saleprice'];
	$total += $prdtotal;
	endforeach;
}
?>
	<div class="dropdown dropdown-cart">
		<?php if($this->dentkart->total_items()>0){?> 
			<a href="#" class="dropdown-toggle lnk-cart" data-toggle="dropdown">
		<?php }else{?>
			<a href="#" class="dropdown-toggle lnk-cart">
		<?php } ?>
		
			<div class="items-cart-inner">
				<div class="basket">
					<i class="glyphicon glyphicon-shopping-cart"></i>
				</div>
				
				<div class="basket-item-count items red-trigger">
					<span class="count">
						<?php if($this->dentkart->total_items()){ echo $this->dentkart->total_items();}else{  echo 0;}?>
					</span>
				</div>
				<div class="total-price-basket">
					<span class="lbl">cart -</span>
					<span class="total-price">
						<span class="sign">₹ </span><span class="value"><?php echo $total>0 ? $total:0; ?></span>
					</span>
				</div>
		    </div>
		</a>
		<ul class="dropdown-menu" style="border:1px solid #ddd; max-height: 300px;overflow-y: scroll;">
			<?php
			$cartarray =  $this->dentkart->contents();
			if(count($cartarray) > 0){
				$total = 0;
				foreach ($this->dentkart->contents() as $cartkey=>$product):  $i++;?><?php
					$prdimag = json_decode($product['images'], true);
					$prdimag = json_decode(json_encode($prdimag), true);
					$filename = "";
					foreach($prdimag as $prdimgrow){
						$filename = $prdimgrow['filename'];
					}
					
					$prdtotal = $product['quantity'] *$product['saleprice'];
					$total += $prdtotal;?>
					<div class="col-md-12" style="padding:0px;margin-top:10px;border-bottom:1px solid #ececec;" id="prd<?php echo $cartkey; ?>">
						<div class="col-md-3" style="float:left;padding-left:0px;">
							<img src="<?php echo base_url().'uploads/images/small/'.$filename; ?>" alt="product name" style="max-height:78px;padding:5px;max-width:78px;">
						</div>
						<div class="col-md-9" style="padding: 0px 10px;">
							<small ><b><?php echo $product['name']; ?></b></small>
							
							<p style="margin:3px 0px;"><?php echo $product['quantity']; ?> X  <?php echo $product['saleprice'];?>= <i class="fa fa-inr"></i> <?php echo $prdtotal?>.00</small></p>
							
							<p style="cursor:pointer; margin:3px 0px; text-align:right; font-size:14px;float:right;padding-top:0px;height:auto;">
								<a onclick="remove_product('<?php echo site_url('cart/ajax_remove_item/'.$cartkey);?>');" style="color:#337ab7;"><i class="fa fa-trash" aria-hidden="true"></i></a>
							</p>
						</div>
					</div><?php
				endforeach;?>	
				<!--second product end--->
				<div class="col-md-12" style="padding-top:5px; margin-top:10px; border-top:1px solid #fff;">
					
					<div class="pull-right">
						<span class="text">Sub Total :</span><span class="price"><i class="fa fa-inr"></i> <?php echo $total; ?>.00</span>
					</div>
					<!--<a href="<?php echo base_url()."cart/view_cart";  ?>" class="btn-info" style="background: #511E3E;border-radius:0px;border:0px;padding:8px 15px;float:right;color:#fff;text-align:center;">View Cart</a>-->
					<div class="clearfix"></div>
					
					<?php
					if($this->Customer_model->is_logged_in(false, false)){?>
						<a href="<?php echo base_url()."cart/update_cart/checkout";  ?>" class="btn btn-upper btn-primary btn-block m-t-20">Checkout</a>
					<?php
					}else{?>
						<a href="#" onclick="$('#red-box').addClass('hide').removeClass('in');" data-toggle="modal" data-target="#authentication" class="btn btn-upper btn-primary btn-block m-t-20">Checkout</a>
					<?php
					}?>
				</div><?php
			}else{?>
				<div class="col-md-12" style="padding:10px;text-align:center;">
					<a href="<?php echo base_url();?>" class="btn-info" style="display:inline;float:none;color:#fff;border-radius: 0px;border: 0px;font-size:15px;background-color:#b10000;">Continue Shopping</a>
				</div>
		<?php
			}?>
		</ul>
	</div><!-- /.dropdown-cart -->

<!-- ============================================================= SHOPPING CART DROPDOWN : END============================================================= -->				</div><!-- /.top-cart-row -->
			</div><!-- /.row -->

		</div><!-- /.container -->

	</div><!-- /.main-header -->

	<!-- ============================================== NAVBAR ============================================== -->
<div class="header-nav animate-dropdown">
    <div class="container">
        <div class="yamm navbar navbar-default" role="navigation">
            <div class="navbar-header">
                <button data-target="#mc-horizontal-menu-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="nav-bg-class">
                <div class="navbar-collapse collapse" id="mc-horizontal-menu-collapse">
	<div class="nav-outer">
		<ul class="nav navbar-nav">
			<li class="<?php if($page=='home'){ echo "active";} ?> dropdown yamm-fw" style="border-top-left-radius: 6px;">
				<a href="<?php echo base_url(); ?>">Home</a>
			</li><?php 
			
			//Add category and subcategory in menu
			$this->menus->show_menu();?><!--
			
			<li class="<?php if($page=='feature'){ echo "active";} ?> dropdown yamm-fw">
				<a href="<?php echo base_url('cart/feature'); ?>">Featured Products</a>
			</li>
			<li class="<?php if($page=='new'){ echo "active";} ?> dropdown yamm-fw">
				<a href="<?php echo base_url('cart/newproduct'); ?>">New Products</a>
			</li>-->
			
            <li class="<?php if($page=='about'){ echo "active";} ?> dropdown yamm-fw">
				<a href="<?php echo base_url('cms/cms_about');?>">About Us</a>
			</li>
            <li class="<?php if($page=='contact'){ echo "active";} ?> dropdown yamm-fw">
				<a href="<?php echo base_url('cms/cms_contactus');?>">Contact Us</a>
			</li>
		</ul><!-- /.navbar-nav -->
		<div class="clearfix"></div>				
	</div><!-- /.nav-outer -->
</div><!-- /.navbar-collapse -->


            </div><!-- /.nav-bg-class -->
        </div><!-- /.navbar-default -->
    </div><!-- /.container-class -->

</div><!-- /.header-nav -->
<!-- ============================================== NAVBAR : END ============================================== -->

</header>
<script>
function remove_product(call_url){
	$.ajax({
		type: "POST",
		url: call_url, 
		success: function(data){
			$.ajax({
				url: '<?php echo base_url(); ?>cart/mini_cart',
				type: 'post',
				success: function(data){
					$('#red-box').addClass('hide').removeClass('in');
					location.reload(true);/*
					prval = 0;
					newtiems = 0;
					if(parseInt($(".current_cart_value").text())){
						prval = parseInt($(".current_cart_value").text());
						newtiems = prval-1;
					}
					$(".current_cart_value").html(<?php echo $this->dentkart->total_items();?> + " Item");
					$('#red-box').addClass('hide').removeClass('in');
					
					//alert(data);
					//$('.view_cart-box').html(data);*/
				}
			});
		}
	});
}

$(document).ready(function(){
	
	$('.red-trigger').hover(function (){
		$.ajax({
			url: '<?php echo base_url(); ?>cart/mini_cart',
			type: 'post',
			success: function(data) {
				//alert(data);
				$('.view_cart-box').html(data);
				$('#red-box').addClass('in').removeClass('hide');
			}
		});
    }, function(){
		$('.view_cart-box').html('');
		$('#red-box').addClass('hide').removeClass('in');
	});
	
	$(document).mouseleave(function(e) {
		if ( $(e.target).closest('#red-box').length || $(e.target).closest('.ui-corner-all').length) {
			$('#red-box').addClass('hide').removeClass('in');
			//show = 1;
		}else if (!$(e.target).closest('#red-box').length ) {
			$('#red-box').addClass('hide').removeClass('in');
			show = 0;
		}
	});
	
	
    $("#product_search").keyup(function(){
		cat_id = $("#search_cat_product").val();
		$("#search_cat_error").text('');
        if($("#product_search").val().length>2){
            prd_key = $("#product_search").val();
			$.ajax({
               type: "post",
               url: "<?php echo base_url().'cart/product_search_by_ajax';?>",
               cache: false,
               data:'search='+prd_key+'&cat_id='+cat_id,
               success: function(response){
                    $('#finalResult').html("");
                    var obj = JSON.parse(response);
                    //console.log(obj);
                    if(obj.length>0){
                        try{
                            var items=[];
                            $.each(obj, function(i,val){
								var serach_cont = '<a href="<?php echo base_url();?>'+val.slug+'"  style="font-size:14px;">'+val.name+'<br/><span style="color:#000;">'+val.slug+'</span></a>';
								items.push($('<li style="list-style-type:circle;margin-left:25px;"></li>').html(serach_cont));
                            });
                            $('#finalResult').append.apply($('#finalResult'), items);
                            $('#serch_box').css("display", "block");
                        }
                        catch(e) {
                            alert('Exception while request..');
                        }
                    }else{
                        $('#finalResult').html($('<li/>').text("No Data Found"));
                    }
               },
               error: function(){
					//alert(response);
                    //alert('Error while request..');
               }
            });
        }else{
            $('#serch_box').css("display", "none");
            return false;
        }
    });

	$("#header_search_submit").click(function(){
		$("#search_cat_error").text('');
		cat_id = $("#search_cat_product").val();
		if(cat_id == 0){
			$("#search_cat_error").text('Please, Select Category First.');
			$("#search_cat_error").focus();
			return false;
		}else if($.trim($("#product_search").val()) == ''){
			$("#search_cat_error").text('Please, enter search keyword.');
			$("#product_search").val("");
			$("#product_search").focus();
			return false;
		}
		$("#search_cat_error").text('');
		$("#search_frm").submit();
	});
	/*
	$("body").click(
		function(e){
			if(e.target.className !== "view_cart-box"){
				$('#red-box').addClass('hide').removeClass('in');
			}
		}
	);
	*/
	$(".search-box").keydown(function(event){
		if(event.keyCode == 13) {
		  event.preventDefault();
		  return false;
		}
	});	
	
});
	
</script>

<div id="print_container" class="col-md-10 success_container_page">
	<h3 class="success_thankyou">THANK YOU!<span>Your Order Has Been Placed Successfully!!</span></h3>
	<h5 class="order_summary">Order Summary</h5>
	<div class="row">
		<div class="col-md-4 my_order_section">
			<strong>Order ID:</strong> <br/><?php echo $order_id;?><br><br>
			<strong>Order Date:</strong> <br/><?php echo $order_date;?><br><br>
			<strong>Mode of Payment: <br/></strong> <?php 
			if($payment['description'] == "Charge on Delivery"){
				echo "COD";
			}else{
				echo $payment['description'];
			}?><br><br>
		</div><?php
			$ship = $customer['ship_address'];
			$bill = $customer['bill_address'];?>
		<div class="col-md-4 my_order_section">
			<strong>Billing Address</strong><br>
			<p><?php echo format_address($bill, TRUE);?><br/>
			Email : <?php echo $bill['email'];?><br/>
			Contact No. : <?php echo $bill['phone'];?></p>
		</div>
		<div class="col-md-4 my_order_section">
			<strong>Shipping Address</strong>
			<p><?php echo format_address($ship, TRUE);?><br/>
				Email : <?php echo $ship['email'];?><br/>
				Contact No. : <?php echo $ship['phone'];?></p>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
	<div class="checkout-box">
		<div class="custom-form">
			<div class="table-responsive" style="min-height: .01%;overflow-x: auto;box-sizing: border-box;">
				<table class="table table-bordered" style="border:1px solid #ddd;width: 100%;max-width: 100%;margin-bottom: 20px;border-spacing: 0;border-collapse: collapse;">
					<thead>
						<tr>
							<th width="7%" style="border:1px solid #ccc;color:#fff !imortant;"><strong>S. No</strong></th>
							<th width="12%" style="border:1px solid #ccc;">SKU</th>
							<th width="34%" style="border:1px solid #ccc;">Product Details</th>
							<th width="10%" style="border:1px solid #ccc;">Quantity</th>
							<th width="10%" style="border:1px solid #ccc;">Rate(INR)</th>
							<th width="10%" style="border:1px solid #ccc;">Amount(INR)</th>
						</tr>
					</thead>
					<tbody style="box-sizing: border-box;border-collapse: collapse;"><?php
						$count = 1;
						foreach ($natuur['contents'] as $cartkey=>$product):?>
							<tr class="items" style="box-sizing: border-box;border-collapse: collapse;">
								<td style="padding-left:5px;border:1px solid #ccc;"><?php echo $count;?>.</td>
								<td style="padding-left:5px;border:1px solid #ccc;"><?php echo $product['sku'];?></td>
								<td style="padding-left:5px;border:1px solid #ccc;"><?php echo $product['name']; ?></td>
								<td style="padding-right:5px;border:1px solid #ccc;text-align:right;"><?php 
									echo $product['quantity'];?>
								</td>
								<td style="padding-right:5px;border:1px solid #ccc;" align="right">
									<span class="<?php echo $product['currency_symbol'];?>"></span> <?php echo sprintf ("%.2f", $product['product_currency_price']);?>
								</td>
								<td style="padding-right:5px;border:1px solid #ccc;" align="right">
									<span class="<?php echo $product['currency_symbol'];?>"></span>  <?php echo $product['subtotal']; ?>
								</td>
							</tr><?php
							$count++;
						endforeach;
						$cur =  $this->natuur->get_currncy();
						?>	
						<tr>
							<td style="padding-left:5px;border:1px solid #ccc;text-align:right;" colspan="5" align="left"><strong>Subtotal</strong></td>
							<td style="border:1px solid #ccc;padding-right:5px;" align="right"><span class="<?php echo $cur['currency_symbol'];?>"></span> <?php echo sprintf ("%.2f", $natuur['subtotal']); ?></td>
						</tr><?php
						if($natuur['coupon_discount'] > 0)  : ?>
							<tr>
								<td style="padding-left:5px;border:1px solid #ccc;text-align:right;" colspan="5" align="left"><i>Coupon Discount</i></td>
								<td style="border:1px solid #ccc;padding-right:5px;" align="right">-<span class="<?php echo $cur['currency_symbol'];?>"></span> <?php echo sprintf ("%.2f", $natuur['coupon_discount']); ?></td>
							</tr><?php								
						endif;
						if($natuur['shipping_cost'] > 0)  : ?>
							<tr>
								<td style="padding-left:5px;border:1px solid #ccc;text-align:right;" colspan="5" align="left"><i>Shipping &amp; Handling</i></td>
								<td style="border:1px solid #ccc;padding-right:5px;" align="right"><span class="<?php echo $cur['currency_symbol'];?>"></span> <?php echo $natuur['shipping_cost']; ?></td>
							</tr><?php								
						endif;
						if($natuur['order_tax'] != 0) :?>
							<tr>
								<td style="padding-left:5px;border:1px solid #ccc;text-align:right;" colspan="5" align="left"><i>Taxes</i></td>
								<td style="border:1px solid #ccc;padding-right:5px;" align="right"><span class="<?php echo $cur['currency_symbol'];?>"></span> <?php echo sprintf ("%.2f", $natuur['order_tax']); ?></td>
							</tr><?php								
						endif;
						$grandtotal = $natuur['total']+$natuur['shipping_cost'];
						?>
						<tr>
							<td style="padding-left:5px;border:1px solid #ccc;text-align:right;" colspan="5" align="left" ><strong>Total</strong></td>
							<td style="border:1px solid #ccc;padding-right:5px;" align="right"><strong><span class="<?php echo $cur['currency_symbol'];?>"></span> <?php echo sprintf ("%.2f", $grandtotal); ?></strong></td>
						</tr>
					</tbody><?php
					if(!empty($user_notes)){?>
						<tfoot>
							<tr>
								<td style="padding-left:5px;border:1px solid #ccc;width:200px;text-align:right;" align="left" ><i>Comment</i></td>
								<td style="border:1px solid #ccc;padding-right:5px;text-align:justify;" align="left" colspan="5"><?php echo $user_notes; ?></td>
							</tr>
						</tfoot><?php
					}?>
				</table>
			</div>					
		</div>		
		<div class="custom-form">
			<div class="discount-box">
				<div class="row">
					<div class="col-md-10 col-sm-10 col-xs-10 estimate_date">
						<p><strong>Estimated Delivery Date:</strong> <?php $date = strtotime("+7 day");echo date('d M, Y', $date);?></p>
					   <p>We will be sending you an email and SMS with the shipment tracking details. <br>
						In case you have any queries, please feel free to write to us at <a href="mailto:support@natuur.com"><strong>support@natuur.com</strong></a> or
						call us on <strong>+91 9718500111</strong>  from 9 am to 9 pm IST.</p>
					</div>
				</div>
			</div>
		</div>
	</div>			
</div>
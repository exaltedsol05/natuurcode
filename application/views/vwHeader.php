<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
  
    <title><?php echo (!empty($seo_title)) ? $seo_title .' - ' : ''; ?> Natuur - Natural|Pure|Handicrafted</title>
    <?php if(isset($meta)):?>
    <?php echo $meta;?>
    <?php else:?>
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <?php endif;?>
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo HTTP_IMAGES_PATH; ?>logo/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo HTTP_IMAGES_PATH; ?>logo/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo HTTP_IMAGES_PATH; ?>logo/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo HTTP_IMAGES_PATH; ?>logo/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo HTTP_IMAGES_PATH; ?>logo/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo HTTP_IMAGES_PATH; ?>logo/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo HTTP_IMAGES_PATH; ?>logo/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo HTTP_IMAGES_PATH; ?>logo/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo HTTP_IMAGES_PATH; ?>logo/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo HTTP_IMAGES_PATH; ?>logo/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo HTTP_IMAGES_PATH; ?>logo/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo HTTP_IMAGES_PATH; ?>logo/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo HTTP_IMAGES_PATH; ?>logo/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo HTTP_IMAGES_PATH; ?>logo/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo HTTP_IMAGES_PATH; ?>logo/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    
    <!-- CSS 
    ========================= -->

    <!-- Plugins CSS -->
    <link rel="stylesheet" href="<?php echo HTTP_CSS_PATH; ?>plugins.css">
    
    <!-- Main Style CSS -->
    <link rel="stylesheet" href="<?php echo HTTP_CSS_PATH; ?>style.css">
    
    <!-- JS
============================================ -->
<style>
/* Preloader */

#preloader {
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #fff;
  /* change if the mask should have another color then white */
  z-index: 99;
  /* makes sure it stays on top */
}

#status {
  width: 200px;
  height: 200px;
  position: absolute;
  left: 50%;
  /* centers the loading animation horizontally one the screen */
  top: 50%;
  /* centers the loading animation vertically one the screen */
  background-image: url(https://raw.githubusercontent.com/niklausgerber/PreLoadMe/master/img/status.gif);
  /* path to your loading animation */
  background-repeat: no-repeat;
  background-position: center;
  margin: -100px 0 0 -100px;
  /* is width and height divided by two */
}

</style>

</head>

<body>
<!--<div id="preloader">
  <div id="status">&nbsp;</div>
</div>-->
    <!--header area start-->
    <header class="header_area">
		
	<section style="color: red;padding: 0 20px;font-weight:bold;">
		Dear Patron , <br>
		Our website is being enhanced for added convenience and you will need to offer offline.
		We will be back in a few days <br>
		Until then to order Natuur products 
		Plesse sms or WhatsApp at <b>+91-9718500111</b> or email at <b>management@natuur.in</b>
	</section>
        <!--header top area start-->
        <div class="header_top">
            <div class="top-header owl-carousel top_header_sticky_carousel">
                <div>Natuur Announcements</div>
                <div>We are delivering Pan India & internationally</div>
                <div>Look out for our empowering DIY workshops</div>
            </div>
        </div>
        <!--<div class="header_top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="header_top_left">
                            <ul>
                                <li class="currency"><a href="#">Blog</a>
                                    
                                </li>
                             
                                <li class="account">
                                 <?php if($this->Customer_model->is_logged_in(false, false)){?> 
                                <a href="#">Hi,<?php echo $this->customer['firstname'];?> <i class="ion-ios-arrow-down"></i></a>
                                <?php }else{?>
                                    <a href="<?php echo  site_url('secure/my_account');?>">My Account <i class="ion-ios-arrow-down"></i></a>
                                <?php } ?>
                                    <ul class="dropdown_links">
                                        <li><a href="<?php echo  site_url('checkout');?>">Checkout </a></li>
                                        <li><a href="<?php echo  site_url('secure/my_account');?>">My Account </a></li>
                                        <li><a href="<?php echo  site_url('cart/view_cart');?>">Shopping Cart</a></li>
                                        <li><a href="<?php echo  site_url('secure/my_wishlist');?>">Wishlist</a></li>
                                         <?php if($this->Customer_model->is_logged_in(false, false)){?>
                                        <li><a href="<?php echo  site_url('secure/logout');?>"><i class="fa fa-sign-out"></i>Logout</a></li>
                                        <?php } ?>
                                    </ul>
                                 </li>
                                
                                
                            </ul>
                        </div> 
                    </div>
                    <div class="col-lg-6">
                        <div class="header_top_right">
                            <div class="header_shipping">
                                <a href="#"><i class="ion-android-call"></i> Customer Care:(+91) 9718500111</a>
                            </div>
                            <div class="header_social">
                                <ul>
                                    <li><a href="https://www.facebook.com/NaturalMama2016/" target="_blank"><i class="ion-social-facebook"></i></a></li>
                                    <li><a href="https://twitter.com/NaturlaMama" target="_blank"><i class="ion-social-twitter"></i></a></li>
                                    <li><a href="https://www.instagram.com/natuur.in"><i class="ion-social-instagram" target="_blank"></i></a></li>
                                    <li><a href="https://www.linkedin.com/company/natural-mama-india" target="_blank"><i class="ion-social-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
        
        <!--header middle area start-->
        <div class="header_middle">
            <div class="container">
                <div class="row">
                     <div class="col-lg-4 col-md-12 inrConversion">
                        <div class="header_contact">
                            <!--<div class="contact_static" style="padding: 0px !important;border: 0px !important;">
                                <a href="email:support@natuur.in"><i class="ion-android-mail"></i> support@natuur.in</a>
                                <span>Natuur ONLINE SUPPORT 24H/7</span>
                            </div>-->
                            <div class="header_top_left">
                                <ul>
                                    <li class="account inr_conversion">
									<?php
									$cur =  $this->natuur->get_currncy();
									?>
                                        <a href="#" data-toggle="modal" data-target="#inrModal"><!-- <i class="fa fa-money mobileIconHide" aria-hidden="true"></i> -->
										<span>
										<?php
										$curr = $cur['currency_code'];
										echo $curr;
										?>
										</span>
										<i class="ion-ios-arrow-down"></i></a>
                                        <!-- <ul class="dropdown_links">
                                            <li><a href="#">INR</a></li>
                                            <li><a href="#">USD</a></li>
                                        </ul> -->
										<div class="modal fade" id="inrModal" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											<div class="modal-dialog modal-dialog-centered" role="document">
												<div class="modal-content">
													<!-- <div class="modal-header">
														<div class="closeCurrency">
															<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span>
															</button>
														</div>
													</div> -->
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span></button>
														<h3>Currency</h3>
													</div>
													<div class="modal-body">
														<!-- <div class="currencyTitle"><h6>Currency</h6></div> -->
														<div class="usdINR">
															<?php if($cur['currency']!=1){ ?>
															<div><a href="<?php echo base_url('secure/currency/1');?>"><i class="fa fa-inr" aria-hidden="true"></i><span>INR</span></a></div>
															<?php }if($cur['currency']!=2){ ?>
															<div><a href="<?php echo base_url('secure/currency/2');?>"><i class="fa fa-usd" aria-hidden="true"></i><span>USD</span></a></div>
															<?php }if($cur['currency']!=3){ ?>
															<div><a href="<?php echo base_url('secure/currency/3');?>"><i class="fa fa-eur" aria-hidden="true"></i><span>Euro</span></a></div>
															<?php }if($cur['currency']!=4){ ?>
															<div><a href="<?php echo base_url('secure/currency/4');?>"><i class="fa fa-gbp" aria-hidden="true"></i><span>Pound Sterling</span></a></div>
															<?php }if($cur['currency']!=5){ ?>
															<div><a href="<?php echo base_url('secure/currency/5');?>"><i class="fa fa-usd" aria-hidden="true"></i><span>Australian Dollars</span></a></div>
															<?php }if($cur['currency']!=6){ ?>
															<!-- <div><a href="<?php //echo base_url('secure/currency/6');?>"><i class="fa fa-usd" aria-hidden="true"></i><span>AED for GCC</span></a></div> -->
															<div><a href="<?php echo base_url('secure/currency/6');?>"><i class="aed_gcc_image"></i><span>AED for GCC</span></a></div>
															<?php } ?>
														</div>
													</div>
												</div>
											</div>
										</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                     </div>
                    <div class="col-lg-4 col-md-12 mainLogo">                       
                        <div class="logo logo_three" align="center">
                            <a href="<?php echo base_url('/'); ?>">
                                <img src="<?=HTTP_IMAGES_PATH;?>logo/logo.png" alt="Natuur">
                            </a>                            
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 headerCartPart">
                        <div class="header_middle_right">
                            <div class="header_top_left">
                                <ul>
                                    <li class="account">
                                     <?php if($this->Customer_model->is_logged_in(false, false)){?> 
                                    <a href="#">Hi<?php //echo ','.$this->customer['firstname'];?> <i class="ion-ios-arrow-down"></i></a>
                                        <ul class="dropdown_links">                                             
                                            <li><a href="<?php echo  site_url('secure/my_account');?>">My Account </a></li>
                                            <li><a href="<?php echo  site_url('secure/logout');?>"><i class="fa fa-sign-out"></i>Logout</a></li>
                                        </ul>
                                    <?php }else{?>
                                        <a href="#" aria-hidden="true" data-toggle="modal" data-target="#myLoginModal"><span class="mobileTextHide">Login</span><i class="fa fa-sign-in mobileIconHide" aria-hidden="true" data-toggle="modal" data-target="#myLoginModal"></i></a>
                                        <!--<ul class="dropdown_links">
                                            <li><a href="<?php echo  site_url('secure/register');?>">Sign Up </a></li>   
                                        </ul>-->

                                    <?php } ?>
                                        
                                    </li>
									<li class="currency">
										<?php if($this->Customer_model->is_logged_in(false, false)){?> 
											<a href="<?php echo  site_url('secure/my_wishlist');?>"><span class="mobileTextHide">WishList</span><!-- <i class="ion-android-favorite-outline mobileIconHide"></i> --></a>
										<?php }else{?>
											<a href="#" aria-hidden="true" data-toggle="modal" data-target="#myLoginModal"><span class="mobileTextHide">WishList</span></a>
										<?php }?>
									</li>
                                </ul>
                            </div>
                            <div class="mini_cart_wrapper mini_cart_three">
                                <a href="javascript:void(0)"><i class="ion-bag"><span><?php echo $this->natuur->total_items();?></span></i><small>₹​<?php echo $this->natuur->total();?></small></a>
                                <!--mini cart-->
                                <div class="mini_cart up_mini_cart">
                                    <?php
                    $cartarray =  $this->natuur->contents();
            if(count($cartarray) > 0){
                $total = 0;
                foreach ($this->natuur->contents() as $cartkey=>$product):  $i++;?><?php
                    $prdimag = json_decode($product['images'], true);
                    $prdimag = json_decode(json_encode($prdimag), true);
                    $filename = "";
                    foreach($prdimag as $prdimgrow){
                        $filename = $prdimgrow['filename'];
                    }
                    if($product['saleprice']<1)
                    {
                        $product['saleprice']=$product['price'];
                    }
                    $prdtotal = $product['quantity'] *$product['saleprice'];
                    $total += $prdtotal;?>
                    <div class="cart_item">
                        <div class="cart_img">
                            <img src="<?php echo base_url('uploads/images/small/'.$filename); ?>" alt="product name">
                        </div>
                        <div class="cart_info">
                            <a href="#"><?php echo substr($product['name'],0,20); ?></a>
                            
                            <span class="quantity">Qty:<?php echo $product['quantity']; ?></span>
                            <span class="quantity" style="font-weight:600;"><?php echo $product['product_size']; ?></span>
                            <span class="price_cart"><i class="<?php echo $cur['currency_symbol'];?>"></i> <?php echo $product['subtotal']; ?></span>
                            
                            
                        </div>
                        <div class="cart_remove">
                                <a href="javascript:void(0)" class="remove_product" data-id="<?php echo $cartkey;?>"><i class="ion-android-close" aria-hidden="true"></i></a>
                            </div>
                    </div><?php
                endforeach;?>   
                                   
                                    
                                    <div class="mini_cart_table">
                                        <div class="cart_total">
                                            <span>Subtotal:</span>
                                            <span class="price"><i class="<?php echo $cur['currency_symbol'];?>"></i> <?php echo $this->natuur->total(); ?></span>
                                        </div>
                                    </div>
                                    <div class="mini_cart_footer">
                                       <div class="cart_button">
                                            <a href="<?php echo base_url('cart/view_cart')."";  ?>">View cart</a>
                                            <a href="<?php echo base_url('cart/update_cart/checkout');  ?>">Checkout</a>
                                        </div>
                                    </div>
                                <?php }else{?>
                                      <p class="text-center">Your shopping cart is empty!</p>
                                <?php }?>
                                </div>
                                
                                <!--mini cart end-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!--header middle area end-->
        
        <!--header bottom start-->
        <div class="header_bottom sticky-header">
            <div class="container">
                <div class="header_container_right container_position">
                    <div class="main_menu menu_three"> 
                        <nav>  
                            <ul class="f0">
                                <li><a href="<?php echo base_url(); ?>" style="padding-left:0">Home</a></li>
                                <?php $this->menus->show_menu();?>
                                <li><a href="<?php echo base_url(); ?>workshops">Workshops</a></li>
                                <li class="active"><a  href="<?php echo base_url('/');?>about-us" style="padding-left: 0;">About Us</a> </li>
                                <li><a href="<?php echo base_url(); ?>contact-us">Contact Us</a></li>
                            </ul>  
                        </nav> 
                    </div>
                    <div class="header_block_right">
                        <ul>
                            <li class="search_bar"><a href="javascript:void(0)"><i class="ion-ios-search-strong"></i></a> </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--header bottom end-->
        </div>
    </header>
    <!--header area end-->
    <!--search overlay-->
    
    <div class="dropdown_search dropdown_search_three">
        <div class="search_close_btn">
            <i class="ion-android-close btn-close"></i>
        </div>
        <div class="search_container">
            <form action="#">
                <input placeholder="I’m shopping for..." type="text">
                <button type="submit"><i class="ion-ios-search-strong"></i></button>
            </form>
        </div>
    </div>
    
    <!--search overlay end-->

    <!--Offcanvas menu area start-->
    
    <div class="off_canvars_overlay">
                
    </div>
    <div class="Offcanvas_menu">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="canvas_open">
                        <span>MENU</span>
                        <a href="javascript:void(0)"><i class="ion-navicon"></i></a>
                    </div>
                    <div class="Offcanvas_menu_wrapper">
                        <div class="canvas_close">
                              <a href="javascript:void(0)"><i class="ion-android-close"></i></a>  
                        </div>
                        <div class="header_top_right">
                            <div class="header_shipping">
                                <!--<a href="#">Free Shipping on order over $99</a>-->
                            </div>
                            <div class="header_social">
                                <ul>
                                    <li><a href="#"><i class="ion-social-facebook"></i></a></li>
                                    <li><a href="#"><i class="ion-social-twitter"></i></a></li>
                                    <li><a href="#"><i class="ion-social-googleplus-outline"></i></a></li>
                                    <li><a href="#"><i class="ion-social-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="header_block_right">
                            <ul>
                                <li class="search_bar"><a href="javascript:void(0)"><i class="ion-ios-search-strong"></i></a>
                                    <div class="dropdown_search">
                                        <div class="search_close_btn">
                                            <i class="ion-android-close btn-close"></i>
                                        </div>
                                        <div class="search_container">
                                            <form action="#">
                                                <input placeholder="I’m shopping for..." type="text">
                                                <button type="submit"><i class="ion-ios-search-strong"></i></button>
                                            </form>
                                        </div>
                                    </div>
                                </li>
                                <!-- <li class="mini_cart_wrapper"><a href="javascript:void(0)"><i class="ion-bag"></i> <span><?php echo $this->natuur->total_items();?></span></a>
                                    <div class="mini_cart">
                                       <?php $cartarray =  $this->natuur->contents();
                                        if(count($cartarray) > 0){
                                        $total = 0;
                                        foreach ($this->natuur->contents() as $cartkey=>$product):  $i++;
                                            $prdimag = json_decode($product['images'], true);
                                            $prdimag = json_decode(json_encode($prdimag), true);
                                            $filename = "";
                                                foreach($prdimag as $prdimgrow){
                                                    $filename = $prdimgrow['filename'];
                                                }
                                                if($product['saleprice']<1)
                                                {
                                                    $product['saleprice']=$product['price'];
                                                }
                                            $prdtotal = $product['quantity'] *$product['saleprice'];
                                        $total += $prdtotal;?>
                                        <div class="cart_item">
                                           <div class="cart_img">
                                               <a href="#"><img src="<?php echo base_url('uploads/images/small/'.$filename); ?>" alt=""></a>
                                           </div>
                                            <div class="cart_info">
                                                <a href="#"><?php echo substr($product['name'],0,20); ?></a>

                                                <span class="quantity">Qty: <?php echo $product['quantity']; ?></span>
                                                <span class="price_cart"><i class="fa fa-inr"></i> <?php echo $prdtotal;?>.00</span>

                                            </div>
                                            <div class="cart_remove">
                                                <a href="javascript:;" onclick="remove_product('<?php echo site_url('cart/ajax_remove_item/'.$cartkey);?>');"><i class="ion-android-close"></i></a>
                                            </div>
                                        </div>
                                <?php endforeach;
                                        }
                                ?>        

                                       <div class="mini_cart_table">
                                            <div class="cart_total">
                                                <span>Subtotal:</span>
                                                <span class="price"><?php echo $total; ?>.00</span>
                                            </div>
                                        </div>
                                        <div class="mini_cart_footer">
                                           <div class="cart_button">
                                                <a href="<?php echo base_url('cart/view_cart')."";  ?>">View cart</a>
                                            <a href="<?php echo base_url('cart/update_cart/checkout');  ?>">Checkout</a>
                                            </div>
                                        </div>
                                    </div>
                                </li> -->
                                <li class="setting_container"><a href="javascript:void(0)"><i class="ion-navicon"></i></a>
                                    <div class="setting_wrapper">
                                        <div class="setting_close_btn">
                                            <i class="ion-android-close btn-close"></i>
                                        </div>
                                        <div class="logo">
                                            <a href="<?php echo base_url('/'); ?>">
                                                <img src="<?=HTTP_IMAGES_PATH;?>logo/logo.png" alt="Natuur">
                                            </a>
                                        </div>
                                        <!--<div class="header_description">
                                            <p>We are a team of designers and developers that create high quality Magento, Prestashop, Opencart themes and provide premium and dedicated support to our products.</p>
                                        </div>-->
                                        <div class="top_links">
                                            <ul>
                                                <li><span>My Account</span>
                                                    <ul class="sub_links">
                                                        <li><a href="<?php echo site_url('secure/register');?>"> Register</a></li>
                                                        <li><a href="<?php echo site_url('secure/login');?>"> Login</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="setting_social">
                                             <ul>
                                                <li><a href="https://www.facebook.com/NaturalMama2016/" target="_blank"><i class="ion-social-facebook"></i></a></li>
                                                <li><a href="https://twitter.com/NaturlaMama" target="_blank"><i class="ion-social-twitter"></i></a></li>
                                                <li><a href="https://www.instagram.com/natuur.in"><i class="ion-social-instagram" target="_blank"></i></a></li>
                                                <li><a href="https://www.linkedin.com/company/natural-mama-india" target="_blank"><i class="ion-social-linkedin"></i></a></li>
                                             </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div id="menu" class="text-left ">
                            <style type="text/css">li.menu-item-has-children a {text-transform: uppercase;}</style>
                            <ul class="offcanvas_main_menu">
                                <li class="menu-item-has-children active">
                                    <a href="<?php echo base_url();?>">HOME</a>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="<?php echo base_url('/');?>personal-care">PERSONAL CARE</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item-has-children">
                                            <a href="<?php echo base_url('/');?>facial-care">FACIAL CARE</a>
                                            <!-- <ul class="sub-menu">
                                                <li><a href="shop.html">shop</a></li>
                                                <li><a href="shop-fullwidth.html">Full Width</a></li>
                                                <li><a href="shop-fullwidth-list.html">Full Width list</a></li>
                                                <li><a href="shop-right-sidebar.html">Right Sidebar </a></li>
                                                <li><a href="shop-right-sidebar-list.html"> Right Sidebar list</a></li>
                                                <li><a href="shop-list.html">List View</a></li>
                                            </ul> -->
                                        </li>

                                        <li class="menu-item-has-children">
                                            <a href="<?php echo base_url('/');?>hair-care">HAIR CARE</a>
                                        </li>
                                        <li class="menu-item-has-children">
                                            <a href="<?php echo base_url('/');?>body-care">BODY CARE</a>
                                        </li>
                                        <li class="menu-item-has-children">
                                            <a href="<?php echo base_url('/');?>baby-care">BABY &amp; KIDS CARE</a>
                                        </li>
                                        <li class="menu-item-has-children">
                                            <a href="<?php echo base_url('/');?>oral-care">ORAL CARE</a>
                                        </li>
                                        <li class="menu-item-has-children">
                                            <a href="<?php echo base_url('/');?>fragrances">FRAGRANCES</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="<?php echo base_url('/');?>home-care-products">HOME CARE</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item-has-children">
                                            <a href="<?php echo base_url();?>/laundry">Laundry</a>
                                        </li>
                                        <li class="menu-item-has-children">
                                            <a href="<?php echo base_url();?>/home-cleansers">Home Cleaners</a>
                                        </li>
                                        <li class="menu-item-has-children">
                                            <a href="<?php echo base_url();?>/candels">Specialty Candles</a>
                                        </li>
                                        <li class="menu-item-has-children">
                                            <a href="<?php echo base_url();?>/soy-wax">Soy Wax</a>
                                        </li>
                                        <li class="menu-item-has-children">
                                            <a href="<?php echo base_url();?>/bees-wax">Bees Wax</a>
                                        </li>
                                        <li class="menu-item-has-children">
                                            <a href="<?php echo base_url();?>/air-freshners">Air Freshners</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="<?php echo base_url('/');?>speciality-foods">SPECIALITY FOODS</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item-has-children">
                                            <a href="<?php echo base_url();?>/cold-pressed-olis">Cold Pressed Olis</a>
                                        </li>
                                        <li class="menu-item-has-children">
                                            <a href="<?php echo base_url();?>/desi-cow-ghee">Desi Cow Ghee</a>
                                        </li>
                                        <li class="menu-item-has-children">
                                            <a href="<?php echo base_url();?>/kefir">Kefir</a>
                                        </li>
                                        <li class="menu-item-has-children">
                                            <a href="<?php echo base_url();?>/kombucha">Kombucha</a>
                                        </li>
                                        <li class="menu-item-has-children">
                                            <a href="<?php echo base_url();?>/nut-butters">Nut Butters</a>
                                        </li>
                                        <li class="menu-item-has-children">
                                            <a href="<?php echo base_url();?>/protein-powder">Protein Powder</a>
                                        </li>
                                        <li class="menu-item-has-children">
                                            <a href="<?php echo base_url();?>/pure-honey">Pure Honey</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="<?php echo base_url('/');?>gifts">GIFTS</a>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="<?php echo base_url('/');?>workshops">WORKSHOPS</a>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="<?php echo base_url('/');?>about-us">ABOUT US</a> 
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="<?php echo base_url('/');?>contact-us">CONTACT US</a> 
                                </li>

                                <li class="menu-item-has-children active">
                                    <a href="<?php echo base_url();?>/">MY ACCOUNT</a>
                                    <ul class="sub-menu">
                                        <li><a href="<?php echo  site_url('checkout');?>">Checkout </a></li>
                                        <li><a href="<?php echo  site_url('secure/my_account');?>">My Account </a></li>
                                        <li><a href="<?php echo  site_url('cart/view_cart');?>">Shopping Cart</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="<?php echo  site_url('secure/my_wishlist');?>">WISHLIST</a> 
                                </li>
                            </ul>
                        </div>

                        <div class="Offcanvas_footer">
                            <span><a href="#"><i class="fa fa-envelope-o"></i> info@natuur.in</a></span>
                            <ul>
                                <li class="facebook"><a href="https://www.facebook.com/NaturalMama2016/"><i class="fa fa-facebook"></i></a></li>
                                <li class="twitter"><a href="https://twitter.com/NaturlaMama"><i class="fa fa-twitter"></i></a></li>
                                <li class="instagram"><a href="https://www.instagram.com/natuur.in"><i class="fa fa-instagram"></i></a></li>
                               
                                <li class="linkedin"><a href="https://www.linkedin.com/company/natural-mama-india"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Offcanvas menu area end-->
    
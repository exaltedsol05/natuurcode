<?php $this->load->view('vwHeader');?>
<style>
.or-seperator {
    margin-top: 32px;
    text-align: center;
    border-top: 1px solid #e0e0e0;
}
.or-seperator b {
    color: #666;
    padding: 0 8px;
    width: 30px;
    height: 30px;
    font-size: 13px;
    text-align: center;
    line-height: 26px;
    background: #fff;
    display: inline-block;
    border: 1px solid #e0e0e0;
    border-radius: 50%;
    position: relative;
    top: -15px;
    z-index: 1;
}
</style>
    <!--breadcrumbs area start-->
    <div class="breadcrumbs_area">
        <div class="container">   
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <ul>
                            <li><a href="index.html">home</a></li>
                            <li><a href="login.html">login</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>         
    </div>
    <!--breadcrumbs area end-->
    
    
    <!-- customer login start -->
    <div class="customer_login">
        <div class="container">
            
			 <div class="row">
               <!--login area start-->
                <div class="col-lg-6 col-md-6 offset-md-3 offset-lg-3">
                    <div class="account_form">
					<?php if ($this->session->flashdata('message')):?>
					<div class="alert alert-info">
					<a class="close" data-dismiss="alert">×</a>
					<?php echo $this->session->flashdata('message');?>
					</div>
					<?php endif;?>

					<?php if ($this->session->flashdata('error')):?>
					<div class="alert alert-danger">
					<a class="close" data-dismiss="alert">×</a>
					<?php echo $this->session->flashdata('error');?>
					</div>
					<?php endif;?>

					<?php if (!empty($error)):?>
					<div class="alert alert-danger">
					<a class="close" data-dismiss="alert">×</a>
					<?php echo $error;?>
					</div>
					<?php endif;?>
                        <h2 style="text-align:center;">login</h2>
                       <?php echo form_open('secure/login', 'class="custom-form"'); ?>
					   <input type="hidden" value="<?php echo $redirect; ?>" name="redirect"/>
                            <p>   
                                <label>Username or email <span>*</span></label>
                                <input type="text" id="email1" name="email">
                             </p>
                             <p>   
                                <label>Passwords <span>*</span></label>
                                <input type="password" id="password" name="password">
                             </p>   
                            <div class="login_submit">
                               <a href="<?php echo site_url('secure/forgot_password'); ?>">Lost your password?</a>
                                <label for="remember">
                                    <input id="remember" name="remember" value="true" type="checkbox">
                                    Remember me
                                </label>
                                <button type="submit" value="submitted" name="submitted">login</button>
                               
                            </div>

                        </form>
						 <p style="text-align: center;margin-top: 20px;">Looking to create an account?&nbsp;&nbsp;<a href="<?php echo site_url('secure/register'); ?>" style=" text-decoration: underline;color: #83b53b;"> Register</a>
							    </p>
						<div class="or-seperator"><b>or</b></div>
						<p style="text-align:center;font-size:16px;">Sign in with your social media account</p>
						<div class="text-center social-btn">
							<a href="#" class="btn btn-primary btn-block"><i class="fa fa-facebook"></i> Sign in with <b>Facebook</b></a>
							<a href="#" class="btn btn-danger btn-block"><i class="fa fa-google"></i> Sign in with <b>Google</b></a>
						</div>
                     </div>    
                </div>
                <!--login area start-->

                
                <!--register area end-->
            </div>
        </div>    
    </div>
    <!-- customer login end -->

<?php $this->load->view('vwFooter');?>
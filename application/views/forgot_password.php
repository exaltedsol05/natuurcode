<?php $this->load->view('vwHeader');?>
<section class="container">
<div class="row">
	<div class="col-md-8 col-md-offset-2 ">
		<div class="contact-page">
			<div class="col-md-8 col-sm-offset-2">
				<div>
					<?php if ($this->session->flashdata('message')):?>
					<div class="alert alert-info">
						<a class="close" data-dismiss="alert">×</a>
						<?php echo $this->session->flashdata('message');?>
					</div>
					<?php endif;?>
				
					<?php if ($this->session->flashdata('error')):?>
					<div class="alert alert-danger">
						<a class="close" data-dismiss="alert">×</a>
						<?php echo $this->session->flashdata('error');?>
					</div>
					<?php endif;?>
				
					<?php if (!empty($error)):?>
					<div class="alert alert-danger">
						<a class="close" data-dismiss="alert">×</a>
						<?php echo $error;?>
					</div>
					<?php endif;?>
				</div>
				
		
					<h1>Forgot Password</h1>
					<hr>
					<?php echo form_open('secure/forgot_password', 'class="form-horizontal"','id="password_form"') ?>
						<fieldset>
							<div class="control-group">
								<label class="control-label" for="email">Email</label>
								<div class="controls">
									<input type="text" name="email" id="email" class="form-control"/>
								</div>
							</div>
							<br/>
							<div class="control-group">
								<div class="col-md-6">
									<div class="controls">
										<input type="hidden" value="submitted" name="submitted"/>
										<input type="submit" value="Reset Password" name="submit" class="btn btn-primary"/>
									</div>
								</div>
								<div class="col-md-6">
									<div style="float:right;">
										<a class="btn btn-primary" href="#" data-toggle="modal" data-target="#authentication" >Return to Login</a>
									</div>
								</div>
							</div>
						</fieldset>
					</form>
				<br>
				
			
		</div>
    </div>
</div>
</section>
<script>
$(document).ready(function(){
	$("#password_form").validate({
		rules:{
			email:"required"
		},
        submitHandler: function (form){
        	form.submit();
        }
    });
});
	
</script>
<?php $this->load->view('vwFooter');?>
<?php $this->load->view('vwHeader');?>
<link href="<?php echo base_url().'assets/admin/css/datepicker/datepicker3.css'; ?>" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url().'assets/admin/js/plugins/datepicker/bootstrap-datepicker.js';?>" type="text/javascript"></script>
<script>
$(document).ready(function(){
	$("#dob").datepicker({
		changeMonth: true,
		changeYear: true,
		format : 'yyyy-mm-dd'
	});

	$("#cdob").datepicker({
		changeMonth: true,
		changeYear: true,
		format : 'yyyy-mm-dd'
	});

	$("#mdob").datepicker({

		changeMonth: true,

		changeYear: true,

		format : 'yyyy-mm-dd'

		

	});

    

	$(".change_password").click(function(){

		$("#change_password").slideToggle(300);

    });

	

	$('.delete_address').click(function(){

		if($('.delete_address').length > 1){

			if(confirm('Are you sure you want to delete this address?')){

				$.post("<?php echo site_url('secure/delete_address');?>", { id: $(this).attr('rel') },

				function(data){

					$('#address_'+data).remove();

					$('#address_list .my_account_address').removeClass('address_bg');

					$('#address_list .my_account_address:even').addClass('address_bg');

				});

			}

		}else{

			alert('You Must leave at least 1 address in the Address Manager.');

		}	

	});

	

	$('.edit_address').click(function(){

		$.post('<?php echo site_url('secure/address_form'); ?>/'+$(this).attr('rel'),

			function(data){	

				$('#address-form-container').html(data);

				$('#myModal').modal('show');

			}

		);

	});

});
function set_default(address_id, type){

	$.post('<?php echo site_url('secure/set_default_address') ?>/',{id:address_id, type:type});

}
</script>
<?php
$company	= array('id'=>'company', 'class'=>'form-control', 'name'=>'company', 'value'=> set_value('company', $customer['company']));
$first		= array('id'=>'firstname', 'class'=>'form-control', 'name'=>'firstname', 'value'=> set_value('firstname', $customer['firstname']));
$last		= array('id'=>'lastname', 'class'=>'form-control', 'name'=>'lastname', 'value'=> set_value('lastname', $customer['lastname']));
$email		= array('id'=>'email', 'class'=>'form-control', 'name'=>'email', 'readonly'=>'true','value'=> set_value('email', $customer['email']));
$dob = '';
if($customer['dob'] == '0000-00-00'){
	$dob = '';
	$dob = array('id'=>'dob', 'class'=>'form-control', 'name'=>'dob', 'placeholder'=>'yyyy-mm-dd','value'=> set_value('dob', $dob));
}else{
	$dob = $customer['dob'];
	$dob = array('id'=>'dob','class'=>'form-control', 'name'=>'dob', 'placeholder'=>'yyyy-mm-dd','value'=> set_value('dob', $dob));
}
//


$canniversary = '';
if($customer['clinic_anniversary'] == '0000-00-00'){
	$canniversary = '';
	$cdob = array('id'=>'cdob', 'class'=>'form-control', 'name'=>'cdob', 'placeholder'=>'yyyy/mm/dd','value'=> set_value('cdob', $canniversary));
}else{
	$canniversary = $customer['clinic_anniversary'];
	$cdob = array('class'=>'form-control', 'readonly'=>'true', 'name'=>'cdob', 'placeholder'=>'yyyy/mm/dd','value'=> set_value('cdob', $canniversary));
}


$manniversary = '';
if($customer['marrg_aniversary'] == '0000-00-00'){
	$anniversary = '';
	$mdob = array('id'=>'mdob', 'class'=>'form-control', 'name'=>'mdob', 'placeholder'=>'yyyy/mm/dd','value'=> set_value('mdob', $manniversary));
}else{
	$manniversary = $customer['marrg_aniversary'];
	$mdob = array('class'=>'form-control', 'readonly'=>'true', 'name'=>'mdob', 'placeholder'=>'yyyy/mm/dd','value'=> set_value('mdob', $manniversary));
}

$phone		= array('id'=>'phone', 'class'=>'form-control', 'name'=>'phone', 'readonly'=>'true', 'value'=> set_value('phone', $customer['phone']));
$password	= array('id'=>'password', 'class'=>'form-control', 'name'=>'password', 'value'=>'');
$confirm	= array('id'=>'confirm', 'class'=>'form-control', 'name'=>'confirm', 'value'=>''); ?>
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner myAccount_breadcum">
			<ul class="list-inline list-unstyled">
				<li><a href="<?php echo base_url();?>">Home</a></li>
				<li><a href="#" class="active">My Information</a></li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->
<div class="body-content outer-top-xs">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php if ($this->session->flashdata('message')):?>
				<div class="alert alert-info">
					<a class="close" data-dismiss="alert">×</a>
					<?php echo $this->session->flashdata('message');?>
				</div>
				<?php endif;?>
				<?php if ($this->session->flashdata('error')):?>
				<div class="alert alert-danger">
					<a class="close" data-dismiss="alert">×</a>
					<?php echo $this->session->flashdata('error');?>
				</div>
				<?php endif;?>
				<?php if (!empty($error)):?>
				<div class="alert alert-danger">
					<a class="close" data-dismiss="alert">×</a>
					<?php echo $error;?>
				</div>
				<?php endif;?>
			</div>
			<div class="col-md-3 myaccount_left">
				<?php $this->load->view('user_account_sidebar');?>
			</div>
			<div class="col-md-9 myaccount_right">
				<div class="row">
					<div class="col-sm-12 myDashboard"><h3>ACCOUNT INFORMATION</h3></div>
					<div class="col-sm-6">
						<h5><b>EDIT ACCOUNT INFORMATION</b></h5> 
					</div>
					<div class="col-sm-6">
						<h5 class="pull-right"><b class="color-red">* Required Fields</b></h5>
					</div>
				</div>
				<div class="row">
					<!-- <div class="col-sm-12">
						<h4><b>ACCOUNT INFORMATION</b></h4>
					</div> -->
					<div class="col-sm-6" style="display:none;">
						<h4><b>CHANGE PASSWORD</b></h4>
					</div>
				</div>
				<hr>
				<?php echo form_open('secure/my_information'); ?>
				<h4 style="color:red;"><?php echo validation_errors(); ?></h4>
				<div class="row default_form">
					<div class="col-sm-12">
						<div class="row">
							<div class="col-sm-12">
							<p><i><?php echo $customer['phone'];?></i></p>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label for="fname" class="form_label">First Name <span class="color-red">*</span></label>
								   <?php echo form_input($first);?>
								  </div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label for="lname" class="form_label">Last Name <span class="color-red">*</span></label>
									<?php echo form_input($last);?>
								  </div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label for="email" class="form_label">Email Address</label>
								   <?php echo form_input($email);?>
								  </div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									 <label for="dob" class="form_label">Date of Birth</label>
									 <?php echo form_input($dob);?>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="row" style="display:none;">
							<div class="col-sm-12">
								<div class="form-group">
									<label for="cpw" class="form_label">Current Password <span class="color-red">*</span></label>
									<input type="password" class="form-control" id="cpw">
								  </div>
							</div>                		
						</div>
						<div class="row">                		
							<div class="col-sm-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label for="npw" class="form_label">New Password <span class="color-red"></span></label>
									<?php echo form_password($password);?>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label for="cnpw" class="form_label">Confirm New Password <span class="color-red"></span></label>
								   <?php echo form_password($confirm);?>
								  </div>
							</div>
						</div>                   
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-sm-12 form_submit">
						<input type="submit" value="Submit" class="btn btn-sm btn-primary" />
					</div>
				</div>
				</form>  
			</div>
		</div>
	</div>
</div>	

<?php 
$this->load->view('vwFooter');
?>
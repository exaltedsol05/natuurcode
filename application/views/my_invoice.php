	<?php $this->load->view('vwHeader'); ?>
<style>#showMenu{display:none;}
@media print {
  body * {
    visibility: hidden;
  }

  #print_container, #print_container * {
    visibility: visible;
  }

  #print_container {
    position: absolute;
    left: 0;
    top: 0;
  }
}
</style>

<section class="container" id="print_container">
	<div class="invoice-container filters-container" id="printdiv" style="width:900px; padding:20px; margin:20px auto; border:1px solid #ccc;">
		<div style="float:left; width:30%;">
			<img src="<?php echo site_url();?>assets/assets/images/newLogo.jpg" alt="Natuur" class="img-responsive">
		</div>
		<div style="float:left; width:70%;">
			<p style="font-size:14px; font-family:verdana;float:right;"><strong>NATUUR</strong><br/>
			Natuur Manufacturing Pvt Ltd,</br>
			Plot No -49, HSIIDC, Udyog Kunj,</br>
			Alipur, Sohna, Gurugram, 122102   </br>                                                                               care@natuur.com</br>
			GST NO: 06AAGCN8280J1Z2</br>
			Help Line: +91-9718500111</br>
			care@natuur.in
		</div>
        <div style="width:100%;"><hr>
			<p style="font-weight:600; font-family:verdana; font-size:25px; margin:0px; text-align:center;"><?php echo $page_title; ?></p><hr>
        </div>
        <div style="width:40%; float:left; min-height:150px;">
			<p style="font-family:verdana;"><strong>Order Details</strong></p><?php
			if($invoice){?>
				<p style="font-family:verdana;font-size:14px;">Invoice No.: <?php echo $invoice_no;?><br/>
				Invoice Date: <?php echo $invoice_no_date;?></p><br/><?php
			}?>
			<p style="font-family:verdana;font-size:14px;">Order ID: <?php echo $order -> order_number;?><br/>
			Order Date: <?php $date = strtotime($order -> ordered_on);
			echo  date("F d, Y", $date);?></p><br/>
			<p style="font-family:verdana;font-size:14px;">Mode of Payment: <?php echo $order -> payment_info; ?></p>
        </div>
		<div style="width:30%; float:left; min-height:150px;">
			<p style="font-family:verdana;"><strong>Billing Address</strong></p>
			<p style="font-size:14px; font-family:verdana;">
				<?php echo ucfirst($order -> bill_firstname);?> <?php echo ucfirst($order -> bill_lastname);?><br/>
				<?php echo ucfirst($order -> bill_address1);?>,<br/>
				<?php echo ucfirst($order -> bill_address2);?>,<br/>
				<?php echo ucfirst($order -> bill_city);?>, <?php echo $order -> bill_zone;?> <?php echo $order -> bill_zip;?><br/>
				<?php echo ucfirst($order -> bill_country);?><br/><br/>
				Email : <?php echo $order -> bill_email;?><br/>
				Contact No. : <?php echo $order -> bill_phone;?>
			</p>
		</div>
		<div style="width:30%; float:left; min-height:150px;">
			<p style="font-family:verdana;"><strong>Shipping Address</strong></p>
			<p style="font-size:14px; font-family:verdana;">
				<?php echo ucfirst($order -> ship_firstname);?> <?php echo ucfirst($order -> ship_lastname);?><br/>
				<?php echo ucfirst($order -> ship_address1);?>,<br/>
				<?php echo ucfirst($order -> ship_address2);?>,<br/>
				<?php echo ucfirst($order -> ship_city);?>, <?php echo $order -> ship_zone;?> <?php echo $order -> ship_zip;?><br/>
				<?php echo ucfirst($order -> ship_country);?><br/><br/>
				Email : <?php echo $order -> ship_email;?><br/>
				Contact No. : <?php echo $order -> ship_phone;?>
			</p>
		</div>
        <div style="margin:20px 0;">
            <table style="width:100%; margin:30px 0px;" cellspacing="0px" border="1px" border-color="#ddd">
                <thead>
                    <tr>
                        <th width="8%" style="text-align:center;font-family:verdana; font-size:14px; padding:5px;">S. No.</th>
                        <th width="12%" style="text-align:center;font-family:verdana; font-size:14px; padding:5px;">SKU</th>
                        <th width="35%" style="text-align:center;font-family:verdana; font-size:14px; padding:5px;">Product Details</th>
                        <th width="10%" style="text-align:center;font-family:verdana; font-size:14px; padding:5px;">Quantity</th>
                        <th width="15%" style="text-align:center;font-family:verdana; font-size:14px; padding:5px;">Rate</th>
                        <th width="15%" style="text-align:center;font-family:verdana; font-size:14px; padding:5px;">Amount</th>
                    </tr>
                </thead>
                <tbody><?php
					$count = 1;
					$gtotal = 0;
                    foreach ($order_items as $order_product):?>
						<tr>
							<td style="font-family:verdana; font-size:14px; padding:5px;">
								<?php echo $count;?>
							</td>
							<td style="font-family:verdana; font-size:14px; padding:5px;">
								<?php echo $order_product['info']->sku;?>
							</td>
							<td style="font-family:verdana; font-size:14px; padding:5px;">
								<?php echo $order_product['info']->name;?>
							</td>
							<td style="font-family:verdana; font-size:14px; padding:5px;">
								<?php echo $order_product['qty'];?>
							</td>
							<td style="font-family:verdana; font-size:14px; padding:5px;">
								<span class="<?php echo $order_product['info']->currency_symbol;?>"></span> <?php echo $order_product['info']->product_currency_price;?>
							</td>
							<td style="font-family:verdana; font-size:14px; padding:5px;">
								<span class="<?php echo $order_product['info']->currency_symbol;?>"></span> 
								<?php $product_price_total =  $order_product['info']->subtotal;
								echo sprintf ("%.2f", $product_price_total);
								$gtotal = $gtotal + $product_price_total;?>
							</td>
						</tr><?php
						$count++;
					endforeach; 
					if($order->order_currency=='1'){
						$symbol = 'fa fa-inr';
					}else if($order->order_currency=='2'){
						$symbol = 'fa fa-usd';
					}else if($order->order_currency=='3'){
						$symbol = 'fa fa-eur';
					}else if($order->order_currency=='4'){
						$symbol = 'fa fa-gbp';
					}else if($order->order_currency=='5'){
						$symbol = 'fa fa-usd';
					}else if($order->order_currency=='6'){
						$symbol = 'aed_gcc_image';
					}
					?>
                    <tr>                        
                        <td style="font-family:verdana; font-size:14px; padding:5px;" colspan="5" align="right">
							<i>Sub Total (Rs)</i>
						</td>
                        <td style="font-family:verdana; font-size:14px; padding:5px;">
							<span class="<?php echo $symbol;?>"></span> <?php echo sprintf ("%.2f", $gtotal); ?>
						</td>
                    </tr><?php 
					if($order -> coupon_discount > 0)  : ?>
						<tr>                        
							<td style="font-family:verdana; font-size:14px; padding:5px;" colspan="5" align="right">
								<i>Coupon Discount</i>
							</td>
							<td style="font-family:verdana; font-size:14px; padding:5px;">
								<?php echo sprintf ("%.2f", $order -> coupon_discount); 
								$gtotal = $gtotal - $order->coupon_discount; ?>
							</td>
						</tr><?php
					endif;
					if($order->shipping>0) :?>
						<tr>                        
							<td style="font-family:verdana; font-size:14px; padding:5px;" colspan="5" align="right">
								<i>Shipping</i>
							</td>
							<td style="font-family:verdana; font-size:14px; padding:5px;">
								<span class="<?php echo $symbol;?>"></span> <?php echo $order->shipping; 
								$gtotal = $gtotal + $order->shipping; ?>
							</td>
						</tr><?php
					endif;
					if($order->reward_amt>0) :?>
						<tr>                        
							<td style="font-family:verdana; font-size:14px; padding:5px;" colspan="5" align="right">
								<i>Withdraw eCash</i>
							</td>
							<td style="font-family:verdana; font-size:14px; padding:5px;">
								-<?php echo sprintf ("%.2f", $order->reward_amt/100); 
								$gtotal = $gtotal - ($order->reward_amt/100); ?>
							</td>
						</tr><?php
					endif;
					if($order->tax>0) :?>
						<tr>                        
							<td style="font-family:verdana; font-size:14px; padding:5px;" colspan="5" align="right">
								<i>Taxes</i>
							</td>
							<td style="font-family:verdana; font-size:14px; padding:5px;">
								<?php echo sprintf ("%.2f", $order -> tax); ?>
							</td>
						</tr><?php
					endif;?>
					<tr>                        
						<td style="font-family:verdana; font-size:14px; padding:5px;" colspan="5" align="right">
							<b>Grand Total</b>
						</td>
						<td style="font-family:verdana; font-size:14px; padding:5px;">
							<span class="<?php echo $symbol;?>"></span> <?php echo sprintf ("%.2f", $gtotal); ?>
						</td>
					</tr><?php 
					if(!empty($order->shipping_notes)){?>
						<tr>   
							<td style="font-family:verdana; font-size:14px; padding:5px;" >
								<i>Comment</i>
							</td>
							<td style="font-family:verdana; font-size:14px; padding:5px;" colspan="5">
								<?php echo $order->shipping_notes; ?>
							</td>
						</tr><?php
					}?>
                </tbody>
            </table>
        </div>
        <div style="width:100%; float:right;">
        	<div style="width:25%; float:right;">
            	<p style="font-size:14px; font-family:verdana; text-align:center;" >For NATUUR SERVICES</p>
				<p style="font-size:13px; font-family:verdana; text-align:center; margin-bottom:30px;">Authorized Approved</p>
            </div>
        </div>
        <div style="margin:20px 0;">
            <p style="font-size:15px; font-family:verdana;"><b>Declaration:</b> We declare that this Invoice shows the actual price of the goods described and that all particulars are true and correct.</p>
        </div>
		<div>
			<p><b>Terms & Conditions:</b></p>
			<p style="margin-left:30px; font-family:verdana; font-size:13px;">1. Goods sold will be exchanged only as per www.natuur.com store policy.<br/>
			2. Objections regarding any items of this Invoice will be entertained unless made within 07 days from the date of receipt.</p>
		</div>
	</div>
</section>
<div style="text-align:center;margin-bottom:15px;">
	<input type="button" value="Print" onclick="print_window_open();" />
</div>
<script type="text/javascript">
	function print_window_open(){
		window.print();
	}
</script>
<?php $this->load->view('vwFooter');?>
<?php

$f_id		= array('id'=>'f_id', 'style'=>'display:none;', 'name'=>'id', 'value'=> set_value('id',$id));
//$f_address1	= array('id'=>'f_address1', 'placeholder'=>'Address (Area and Street)', 'class'=>'form-control', 'name'=>'address1', 'value'=>set_value('address1',$address1));
$f_first	= array('id'=>'f_firstname', 'placeholder'=>'Name', 'class'=>'form-control', 'name'=>'firstname', 'value'=> set_value('firstname',$firstname));
$f_phone	= array('id'=>'f_phone', 'placeholder'=>'10-digit Mobile Number', 'class'=>'form-control', 'name'=>'phone', 'value'=> set_value('phone',$phone));
$f_alt_phone	= array('id'=>'f_alt_phone', 'placeholder'=>'Alternative Phone (Optional)', 'class'=>'form-control', 'name'=>'alt_phone', 'value'=> set_value('alt_phone',$alt_phone));
$f_landmark	= array('id'=>'f_landmark', 'placeholder'=>'Landmark (Optional)', 'class'=>'form-control', 'name'=>'landmark', 'value'=> set_value('landmark',$landmark));
$f_city		= array('id'=>'f_city', 'placeholder'=>'City/District/Town', 'class'=>'form-control', 'name'=>'city', 'value'=>set_value('city',$city));
$f_zip		= array('id'=>'f_zip', 'placeholder'=>'Pincode', 'maxlength'=>'10', 'class'=>'form-control', 'name'=>'zip', 'value'=> set_value('zip',$zip));

echo form_input($f_id);

?>

<div class="address_form default_form">
	<div id="form-error"></div>
	<div class="row">
		<div class="col-md-6 col_md_right">
			<?php echo form_input($f_first);?>
		</div>
		<div class="col-md-6 col_md_left">
			<?php echo form_input($f_phone);?>
		</div>
		<div class="col-md-6 col_md_right">
			<?php echo form_input($f_alt_phone);?>
		</div>
		<div class="col-md-6 col_md_left">
			<?php echo form_input($f_zip);?>
		</div>
		<div class="col-md-12">
			<textarea type="text" name="address1" id="f_address1" class="form-control" placeholder="Address (Area and Street)"><?php echo set_value('address1',$address1);?></textarea>
		</div>
		<div class="col-md-6 col_md_right">
			<?php echo form_input($f_landmark);?>
		</div>
		<div class="col-sm-6 col_md_left checkout_select_state">
			<?php //echo form_dropdown('country_id',$countries, @$customer['bill_address']['country_id'], 'id="country_id" class=""');?>  
			<?php echo form_dropdown('country_id', $countries_menu, set_value('country_id', $country_id), 'id="f_country_id" class="form-control selectpicker"  data-live-search="true"', 'Please choose country');?>
		</div>
		<div class="col-md-6 col_md_right checkout_select_state">
			<?php echo form_dropdown('zone_id', $zones_menu, set_value('zone_id', $zone_id), 'id="f_zone_id" class="form-control selectpicker"', 'Please choose state');?>
		</div>
		<div class="col-md-6 col_md_left">
			<?php echo form_input($f_city);?>
		</div>
		<div class="col-md-12">
			<div class="delivery_address_save">
				<button class="btn" onclick="save_address(); return false;">save address</button>
				<button type="button" class="btn cancel_address">cancel</button>
			</div>
		</div>
	</div>
</div>

<script>
$(function(){
	$('#f_country_id').change(function(){
			$.post('<?php echo site_url('locations/get_zone_menu');?>',{id:$('#f_country_id').val()}, function(data) {
			  $('#f_zone_id').html('<option>Please choose state</option>'+data);		
			$('.selectpicker').selectpicker('refresh');
			});
		});
});

function save_address()
{
	address_save();
}
function address_save()
{
	$.post("<?php echo site_url('secure/address_user_form');?>/"+$('#f_id').val(), {
																				firstname: $('#f_firstname').val(),
																				phone: $('#f_phone').val(),
																				alt_phone: $('#f_alt_phone').val(),
																				zip: $('#f_zip').val(),
																				address1: $('#f_address1').val(),
																				landmark: $('#f_landmark').val(),
																				country_id: $('#f_country_id').val(),
																				city: $('#f_city').val(),
																				zone_id: $('#f_zone_id').val(),
																				},
		function(data){

			if(data == 1)
			{
				$("#checkout_address").load("<?php echo base_url();?>checkout .checkout_address", function() {
					$("#checkout_address").on('click', '.edit_user_address', function () {
					//$(".edit_user_address").click(function(){
						var rel = $(this).attr('rel');
						function_add_edit_user_address(rel);
					});
					$("#checkout_address").on('change', 'input[type=radio][name=address_checked]', function () {
						var rel = $(this).val();
						function_address_checked(rel);
					});
					$("#checkout_address").on('click', '.cancel_address', function () {
						var rel = $("input[type=radio][name=address_checked]:checked").val();
						function_cancel_address(rel);
					});
					$("#checkout_address").on('click', '.deliver_here_btn', function () {
						var rel = $(this).attr('data-id');
						select_ship_address(rel);
					});
					$("#delivered_address").on('click', '#change_delivered_address', function () {
						$('#delivered_address_details,#address_error').html('');
						$('#delivered_address,#checkout_order_summary,#checkout_payment_options').hide();
						$('#checkout_address,#delivered_summary,#delivered_payment').show();
					});
					$("#delivered_summary").on('click', '#change_order_summary', function () {
						$('#address_error').html('');
						$('#delivered_address,#checkout_order_summary,#delivered_payment').show();
						$('#checkout_address,#delivered_summary,#checkout_payment_options').hide();
					});
					$("#checkout_order_summary").on('click', '.cart_continue', function () {
						$('#address_error').html('');
						$('.change_order_btn').html('<div class="changeBtn"><button type="button" class="btn" id="change_order_summary">change</button></div>');
						$('#checkout_order_summary,#delivered_payment').hide();
						$('#checkout_payment_options,#delivered_summary').show();
					});
					function save_address()
					{
						address_save();
					}
				});
				//alert(data);
				//window.location = "<?php echo site_url('secure/my_account');?>";
			}
			else
			{
				//alert(data);
				$('#form-error').html('<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>'+data+'</div>');
			}
		});
}
</script>
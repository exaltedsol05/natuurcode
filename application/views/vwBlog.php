<?php $this->load->view('vwHeader');
$blog_category = array("1" => "Natuur Knowledge", "2"=>"Daily Wisdom", "3" => "DIY Workshop", "4"=> "Chemicals in Daily Life", "5" => "Health & Wellness", "6" => "Beauty");
//echo "<pre>";print_r($cat_blogs);echo "</pre>"; ?>
<style>
.header-nav{
	border-bottom: 1px solid #99cc33;
}
.section-title{
	font-size: 24px;text-decoration: underline;
    -moz-text-decoration-color: #99cc33;
    text-decoration-color: #99cc33;
	border-bottom:none;
	padding-bottom:0px;
}
.link li{padding-right:10px;}
h3, .h3 {
    font-size: 18px;
}
</style>
<div class="body-content outer-top-xs" id="top-banner-and-menu" style="background-color:#fff;">
<div class="row" style="background-color:#f3f3f3;color:#5f5f5f;">
	  <div class="col-xs-12 col-sm-12 col-md-1 homebanner-holder"></div>
	  <div class="col-xs-12 col-sm-12 col-md-11 homebanner-holder">
		<div class="info-boxes wow fadeInUp" style="">
			<div class=""  style="background-color:#f3f3f3;color:#5f5f5f;">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-lg-4">
						<div class="info-boxx">
							<div class="row">								
								<div class="" style="padding-left:0px;">
									<h1 style="font-size:28px;color:#5f5f5f;margin-top:8px;margin-bottom:0px;font-family: 'Open Sans', sans-serif;" class="info-box-heading green">Natuur <span style="color:#99cc33;">| Blog</span></h1>
								</div>
							</div>
						</div>
					</div><!-- .col -->

					<div class="hidden-md col-sm-8 col-lg-8">
						<div class="info-box" style="padding: 16px 25px;">
							<div class="row">
								<ul style="color:#5f5f5f;">
									<li style="float:left;margin-left:15px;cursor:pointer;">
										<a href="<?php echo base_url();?>blog/category/natuur_knowledge"><strong>Natuur Knowledge</strong></a>
									</li>
									<li style="float:left;margin-left:15px;cursor:pointer;">
										<a href="<?php echo base_url();?>blog/category/daily_wisdom"><strong>Daily Wisdom</strong></a>
									</li>
									<li style="float:left;margin-left:15px;cursor:pointer;">
										<a href="<?php echo base_url();?>blog/category/diy_workshop"><strong>DIY Workshop</strong></a>
									</li>
									<li style="float:left;margin-left:15px;cursor:pointer;">
										<a href="<?php echo base_url();?>blog/category/chemicals_in_daily_life"><strong>Chemicals in Daily Life</strong></a>
									</li>
									<li style="float:left;margin-left:15px;cursor:pointer;">
										<a href="<?php echo base_url();?>blog/category/health_wellness"><strong>Health & Wellness</strong></a>
									</li>
									<li style="float:left;margin-left:15px;cursor:pointer;">
										<a href="<?php echo base_url();?>blog/category/beauty"><strong>Beauty</strong></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container" >
	<div class="row" style="background-color:#fff;"><?php
		if(count($cat_blogs[2]) > 2){ ?>
			<div class="col-xs-12 col-sm-12 col-md-4 sidebar" style="margin-top:5px;">
				<div class=" wow fadeInUp"><br/>
					<h3 class="section-title">Daily Wisdom</h3><?php
					$count = 1;
					foreach($cat_blogs[2] as $dw){
						$count++;	?>
						<div id="advertisementt" class="advertisementt">
							<div class="item">
								<h3 style="color:#99cc33;margin-top:5px;margin-bottom:5px;">SEARCH</h3>
								<div class="testimonials" style="text-align: left !important;"><?php 
									echo implode(' ', array_slice(explode(' ', strip_tags($dw->content)), 0, 30));?><br/>
									<a href="<?php echo base_url()."blog/blog_page/".$dw->id;?>" class="btn btn-primary btn-sm" style="padding: 2px 6px;color:#fff;margin-top:5px;">Read More</a>
								</div>
							</div>
						</div><?php
						if($count > 2)
							break;
					}?>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-8 sidebar" style="margin-top:5px;">	<?php
		}else{?>
			<div class="col-xs-12 col-sm-12 col-md-12 sidebar" style="margin-top:5px;"><?php
		}?>
			<div class=""><br/>
			    <h3 class="section-title">What's New : </h3>
				<div id="advertisementt" class="advertisement"><?php
					if(count($new_blogs) > 0){ ?>
						<div class="item">
							<p class="">
								<strong><?php echo implode(' ', array_slice(explode(' ', strip_tags($new_blogs[0]->content)), 0, 8)); ?>... </strong>
								<strong class="pull-right"><?php echo date("F d, Y", strtotime($new_blogs[0]->creation_time)); ?></strong>
								<a href="<?php echo base_url()."blog/blog_page/".$new_blogs[0]->id;?>">
									<img src="<?php echo base_url()."uploads/blog/".$new_blogs[0]->blog_image;?>" alt="New Blog Image" style="width:100%;max-height:255px;">
								</a>	
							</p>
						</div><?php
					}else{
						//
					}?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="wide-banner cnt-strip">
				<div class="image">
					<?php $this->banners->show_collection(16, 1, 'blog_banner');?><!--
					<img src="http://localhost/natuur/uploads/e2c0ddbe34e9e25d1682f3ead709dd81.jpg" alt="User Name" style="width:100%;"> -->
				</div>
			</div>
		</div>
	</div>
				
	<div class="row" style="margin-bottom: 10px;">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<h3 style="color:#99cc33;">POPULAR ARTICLES</h3><?php
			$count = 1;
			foreach($popular_blogs as $dw){
				$count++;	?>
				<div class="col-xs-12 col-sm-12 col-md-6">
					<div class="wow fadeInUp" style="">
						<div id="advertisementt" class="advertisementt">
							<div class="item" style="background-color: #f3f3f3;">
								<div class="">
									<img src="<?php echo base_url()."uploads/blog/".$dw->blog_thumb_image;?>" alt="New Blog Image" style="width:100%;max-height:255px;">
								</div><br/>
								<h3 class="section-title" style="padding:10px;text-decoration:none;"><?php echo $blog_category[$dw->blog_cat];?></h3>
								<h4 class="section-title" style="padding:10px;text-decoration:none;font-size:18px;"><?php echo ucfirst($dw->title);?></h4>
								<div class="testimonials" style="text-align: left !important;padding:10px;">
									<p><?php echo implode(' ', array_slice(explode(' ', strip_tags($dw->content)), 0, 15)); ?>...</p>
									<p><?php echo date("F d, Y", strtotime($dw->creation_time)); ?></p>
									<a href="<?php echo base_url()."blog/blog_page/".$dw->id;?>" class="btn btn-primary btn-sm" style="color:#fff;">Read More</a>
									<div class="no-padding social pull-right" style="padding-left:0px;">
										<ul class="link">
											<li class="fb pull-left">
												<a href="https://www.facebook.com/NaturalMama2016/" target="_blank" title="Facebook" class="facebook" style="cursor:pointer;border-radius:50%;"></a>
											</li>
											<li class="tw pull-left">
												<a href="https://twitter.com/NaturlaMama" target="_blank" class="twitter" title="Twitter" style="cursor:pointer;border-radius:50%;"></a>
											</li>
											<li class="linkedin pull-left">
												<a href="https://www.linkedin.com/company/natural-mama-india" target="_blank" class="linkedin" style="cursor:pointer;border-radius:50%;"></a>
											</li>
											<li class="pintrest pull-left">
												<a href="#" target="_blank" class="pintrest" style="cursor:pointer;border-radius:50%;"></a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div><?php
				if($count > 2)
					break;
			} ?>
		</div>	
		<div class="col-xs-12 col-sm-12 col-md-4" style="display:none;">
			<h3 style="color:#99cc33;">SEARCH</h3>
			<input type="text" class="form-control" name="search"/>
			<h3 style="color:#99cc33;">CATEGORIES</h3>
			<textarea  class="form-control" rows="3"></textarea>
			<h3 style="color:#99cc33;">NEWS LETTERS</h3>
			<input type="text" class="form-control" name="search"/>
			<h3 style="color:#99cc33;">SHARE</h5>
			<div class="no-padding social" style="padding-left:0px;">
			<ul class="link">
				<li class="fb pull-left">
					<a href="https://www.facebook.com/NaturalMama2016/" target="_blank" title="Facebook" class="facebook" style="cursor:pointer;border-radius:50%;"></a>
				</li>
				<li class="tw pull-left">
					<a href="https://twitter.com/NaturlaMama" target="_blank" class="twitter" title="Twitter" style="cursor:pointer;border-radius:50%;"></a>
				</li>
				<li class="linkedin pull-left">
					<a href="https://www.linkedin.com/company/natural-mama-india" target="_blank" class="linkedin" style="cursor:pointer;border-radius:50%;"></a>
				</li>
				<li class="pintrest pull-left">
					<a href="#" target="_blank" class="pintrest" style="cursor:pointer;border-radius:50%;"></a>
				</li>
			</ul>
			</div>
			<h3 style="color:#99cc33;">DISCLAIMER</h5>
			<p>I just want to say thank you very much for the professional job that you did for my business. I will continue using your service for sure because it is faster, very good price and done with honesty.</p>
		
		</div><!-- /.container -->
	</div><!-- /#top-banner-and-menu -->
</div>
</div>
<?php
function theme_img($uri, $tag=false){
	if($tag){
		return '<img src="'.theme_url('assets/assets/<?=HTTP_IMAGES_PATH;?>'.$uri).'" alt="'.$tag.'">';
	}else{
		return theme_url('assets/assets/<?=HTTP_IMAGES_PATH;?>'.$uri);
	}
}
function theme_url($uri){
	$CI =& get_instance();
	return $CI->config->base_url('/'.$uri);
}?>

<?php echo $this->load->view('vwFooter'); ?>
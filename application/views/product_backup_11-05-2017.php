<?php $this->load->view('vwHeader');?>
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="<?php echo base_url();?>" alt="Dentkart">Home</a></li><?php
				$url_path	= '';
				$count	 	= 1;
				foreach($base_url as $bc):
					$url_path .= '/'.$bc;
					if($count == count($base_url)):?>
						<li class="active"><?php echo $bc;?></li>
					<?php else:?>
						<li><a href="<?php echo site_url($url_path);?>"><?php echo $bc;?></a></li> <span class="divider">/</span>
					<?php endif;
					$count++;
				endforeach;?>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->
<div class="body-content outer-top-xs">
	<div class='container'>
		<div class='row single-product'>
			<div class='col-md-3 sidebar'>
				<div class="sidebar-module-container">
					<div class="home-banner outer-top-n">
						<?php $this->banners->show_collection(11, 1, 'default');?>
					</div>	
					<hr>
					<!-- ============================================== HOT DEALS ============================================== -->
					<div class="sidebar-widget hot-deals wow fadeInUp outer-bottom-xs">
						<h3 class="section-title">hot deals</h3>
						<div class="owl-carousel sidebar-carousel custom-carousel owl-theme outer-top-ss">
						
						<?php
						if(count($hotproducts)>=1){?>
						<?php
						foreach($hotproducts as $hproduct){	
							$discount=round(100*($hproduct->price-$hproduct->saleprice)/$hproduct->price);
							
							$photo  = theme_img('no_picture.png', 'No Image Available');
							$hproduct->images   = array_values($hproduct->images);
							if(!empty($hproduct->images[0])){
								$primary    = $hproduct->images[0];
								foreach($hproduct->images as $photo){
									if(isset($photo->primary)){
										$primary    = $photo;
									}
								}

								$photo  = '<img src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$hproduct->seo_title.'"/>';
							}
							
							$salesprice = $this -> dentkart -> product_price($hproduct);
							$discount=round(100*($hproduct->price-$salesprice)/$hproduct->price);

							//[from_date] => 2016-06-20[to_date] => 2016-06-25
							$start_date = strtotime($hproduct->hotdeals_from);
							$end_date = strtotime($hproduct->hotdeals_to);
							$currenttimstamp = time();
							
							$prd_link = base_url().$hproduct->slug;
							//site_url(implode('/', $base_url).'/'.$hproduct->slug);
							$prd_display = true;
							if($currenttimstamp > $end_date){
								$prd_link = 'javascript:void(0)';
								$prd_display = false;
							}
							if($currenttimstamp < $start_date){
								$prd_link = 'javascript:void(0)';
								$prd_display = false;
							}
							if($prd_display){ ?>
							
							<div class="item">
								<div class="products">
									<div class="hot-deal-wrapper">
										<div class="image">
											<a href="<?php echo $prd_link; ?>"><?php echo $photo;?></a>
										</div>
										<div class="sale-offer-tag">
											<span>
												<?php 
													$sales_price = $this -> dentkart -> product_price($hproduct);
													$discount=round(100*($hproduct->price-$sales_price)/$hproduct->price);
													if($hproduct->quantity < 1){
														echo 'Out of Stock';
													}else if($discount){?>
														<?php echo $discount;?>% <br> OFF
														<?php 
													} 
												?>
											</span>
										</div>
										
										<div class="deal-counter" data-countdown="true" ndate="<?php echo $hproduct->hotdeals_to; ?>"></div>
										
									</div><!-- /.hot-deal-wrapper -->

									<div class="product-info text-center m-t-20">
										<h3 class="name"><a href="<?php echo $prd_link; ?>"><?php echo $hproduct->name; ?></a></h3>
										
										<div class="col-md-12">
											<div class="rating-box">
												<ul class="clearfix"><?php
													$all_ret = $this->dentkart->product_rating($hproduct -> id);
													$product_rating = explode("*-*", $all_ret);
													$avg_rating = explode("*-*", $all_ret);
													//print_r($product_rating);

													$star1 	=$product_rating[0];
													$star2 	=$product_rating[1];
													$star3 	=$product_rating[2];
													$star4 	=$product_rating[3];
													$star5 	=$product_rating[4];
													$starall 	=$product_rating[5];
													$avt_rating = (1*($star1) + 2*($star2) +3*($star3) +4*($star4) + 5*($star5))/($starall);
													$avt_rating = number_format((float)$avt_rating, 1, '.', '');
													if(empty($avt_rating)){
														$avt_rating = 0;
													}
													for($i = 0; $i < $avt_rating; $i++){
														echo '<li class="active"><a href="#"></a></li>';
													}

													$dact = 5-$avt_rating;
													for($j = 0; $j < $dact; $j++){
														echo '<li><a href="#"></a></li>';
													}?>
													<li class="rc">(<?php echo $starall; ?> Rating)</li>
												</ul>
											</div>
										</div>

										<div class="product-price">	
											<?php if($discount > 0):?>
												<span class="price">
													₹ <?php echo $hproduct->saleprice;?>	</span>
												<span class="price-before-discount">₹ <?php echo $hproduct->price;?></span>
											<?php else: ?>
											  <span class="price">₹ <?php echo $hproduct->price;?></span>
											<?php endif;?>
										</div><!-- /.product-price -->
										
									</div><!-- /.product-info -->

									<div class="cart clearfix animate-effect">
										<div class="action">
											<div class="add-cart-button btn-group">
												<!--<button class="btn btn-primary icon" data-toggle="dropdown" type="button"><i class="fa fa-shopping-cart"></i></button>
												<button class="btn btn-primary cart-btn" type="button">Add to cart</button>-->		
											</div>
										</div><!-- /.action -->
									</div><!-- /.cart -->
								</div>	
							</div>
							<?php
							} } }
							?>
						</div><!-- /.sidebar-widget -->
					</div>
					<!-- ============================================== HOT DEALS: END ============================================== -->
					
				</div>
			</div><!-- /.sidebar -->
			<div class='col-md-9'>
				<div class="detail-block">
					<div class="row  wow fadeInUp">
						<div class="col-xs-12 col-sm-6 col-md-5 gallery-holder">
							<div class="product-item-holder size-big single-product-gallery small-gallery">
								<div id="owl-single-product">
								<?php
									$photo  = theme_img('no_picture.png', 'No Image Available');
									$product->images    = array_values($product->images);

									if(!empty($product->images[0]))
									{
										$primary    = $product->images[0];
										$i=0;
										foreach($product->images as $photo)
										{?>
										<div class="single-product-gallery-item" id="slide<?php echo $i; ?>">
											<a data-lightbox="image-1" data-title="Gallery" href="<?php echo base_url('uploads/images/medium/'.$photo->filename);?>">
												<img class="img-responsive" alt="" src="<?php echo base_url('uploads/images/medium/'.$photo->filename);?>" data-echo="<?php echo base_url('uploads/images/medium/'.$photo->filename);?>" style="width:317px;height:317px;"/>
											</a>
										</div><!-- /.single-product-gallery-item -->
								<?php	
									$i++;}
									}
								?>
								</div><!-- /.single-product-slider -->


								<div class="single-product-gallery-thumbs gallery-thumbs">
									<div id="owl-single-product-thumbnails">
										<?php if(count($product->images) > 1):?>
											<?php $i=0; foreach($product->images as $image):?>
												<div class="item">
													<a class="horizontal-thumb active" data-target="#owl-single-product" data-slide="<?php echo $i; ?>" href="#slide<?php echo $i; ?>">
														<img class="img-responsive" alt="" src="<?php echo base_url('uploads/images/thumbnails/'.$image->filename);?>" data-echo="<?php echo base_url('uploads/images/thumbnails/'.$image->filename);?>" style="width:68px;height:68px;"/>
													</a>
												</div>
											<?php $i++; endforeach;?>
										<?php endif;?>
									</div><!-- /#owl-single-product-thumbnails -->
								</div><!-- /.gallery-thumbs -->
							</div><!-- /.single-product-gallery -->
						</div><!-- /.gallery-holder --> 
						
						<div class='col-sm-6 col-md-7 product-info-block'>
							<div class="product-info">
								<h2 class="name"><?php //echo "<pre>";print_r($product);exit; 
								echo $product->name;?></h2>
								<h4><?php if(!empty($product->sku)):?><?php echo $product->sku; ?><?php endif;?></h4>
								<div class="rating-reviews m-t-20">
									<div class="row">
										<div class="col-sm-6">
											<div class="rating-box">
												<ul class="clearfix"><?php
													$all_ret = $this->dentkart->product_rating($product -> id);
													$product_rating = explode("*-*", $all_ret);
													$avg_rating = explode("*-*", $all_ret);
													//print_r($product_rating);

													$star1 	=$product_rating[0];
													$star2 	=$product_rating[1];
													$star3 	=$product_rating[2];
													$star4 	=$product_rating[3];
													$star5 	=$product_rating[4];
													$starall 	=$product_rating[5];
													$avt_rating = (1*($star1) + 2*($star2) +3*($star3) +4*($star4) + 5*($star5))/($starall);
													$avt_rating = number_format((float)$avt_rating, 1, '.', '');
													if(empty($avt_rating)){
														$avt_rating = 0;
													}
													for($i = 0; $i < $avt_rating; $i++){
														echo '<li class="active"><a href="#"></a></li>';
													}

													$dact = 5-$avt_rating;
													for($j = 0; $j < $dact; $j++){
														echo '<li><a href="#"></a></li>';
													}?>
													<li class="rc">(<?php echo $starall; ?> Rating)</li>
												</ul>
											</div>
										</div>
										<div class="col-sm-6"><?php
											if($total_review > 0){?>
												<div class="reviews">
													<a href="#all_review"><?php echo $total_review; ?> Review </a>
												</div>
											<?php
											}else{?>
												<div class="reviews">
													<a><?php echo $total_review; ?> Review </a>
												</div>
											<?php
											}?>	
										</div>
									</div><!-- /.row -->		
								</div><!-- /.rating-reviews -->

								<div class="stock-container info-container m-t-10">
									<div class="row">
										<div class="col-sm-2">
											<div class="stock-box">
												<span class="label">Availability :</span>
											</div>	
										</div>
										<div class="col-sm-9">
											<div class="stock-box">
												<?php if($product->quantity < 1):?>
													<span class="value">Out of Stock</span>
												<?php else:?>
													<span class="value">In Stock</span>
												<?php endif;?>
											</div>	
										</div>
									</div><!-- /.row -->	
								</div><!-- /.stock-container -->

								<div class="description-container m-t-20">
									<?php echo $product->excerpt;?>
								</div><!-- /.description-container -->

								<div class="price-container info-container m-t-20">
									<div class="row">
										<div class="col-sm-6">
											<div class="price-box">
												<?php 
												   $discount = $product->price-$product->saleprice;
												   if($discount>0){?> 
														<span class="price"><i class="fa fa-inr"></i> <?php echo $product->saleprice; ?> </span>
												   
														<span class="price-strike"><i class="fa fa-inr"></i> <?php echo $product->price; ?></span>
												   <?php }else{?>
														<span class="price"><i class="fa fa-inr"></i> <?php echo $product->price; ?></span>
												   <?php } ?>
											</div>
										</div>

										<div class="col-sm-6">
											<div class="favorite-button m-t-10">
												<?php if($this->Customer_model->is_logged_in(false, false)){?>
													<a  onclick="add_to_wishlist(<?php echo $product->id;?>);" class="btn btn-primary"  title="Wishlist" href="#">
														<i class="fa fa-heart"></i>
													</a>
												<?php
												}else{?>
													<a class="btn btn-primary" data-toggle="modal" data-target="#authentication" title="Wishlist" href="#">
														<i class="fa fa-heart"></i>
													</a>
												<?php
												}?>
											
												<?php if($this->Customer_model->is_logged_in(false, false)){?>
													<a onclick="add_to_compare(<?php echo $product->id;?>);" class="btn btn-primary" data-toggle="modal" data-placement="right" title="Add to Compare" href="#">
														<i class="fa fa-signal"></i>
													</a>
												<?php
												}else{?>
													<a class="btn btn-primary" data-placement="right" title="Add to Compare" href="#" data-toggle="modal" data-target="#authentication">
														<i class="fa fa-signal"></i>
													</a>
												<?php
												}?>
											</div>
										</div>

									</div><!-- /.row -->
								</div><!-- /.price-container -->
									<?php 
									$total_gruped_product =  count($product->grouped_products);
									if($total_gruped_product <= 1){
										$prdid = $product->id;?>
									<?php echo form_open('cart/add_to_cart_ajax', 'class="form-horizontal" id="add-to-cart_'.$product->id.'" name="add-to-cart_'.$prdid.'" accept-charset="utf-8"');?>
										<input type="hidden" name="cartkey" value="<?php echo $this->session->flashdata('cartkey');?>" />
										<input type="hidden" name="id" value="<?php echo $product->id?>"/>
										<input type="hidden" name="saleprice_pricetier" value="<?php echo $sales_price;?>" id="saleprice_pricetier"/><?php
										if($product->quantity >= 1){?>
										
										<div class="quantity-container info-container">
											<div class="row">
												<div class="col-sm-2">
													<span class="label">Qty :</span>
												</div>
												<div class="col-sm-4">
													<div class="cart-quantity">
														<div class="quant-control">
															<a value="-" class="qtyminus btn-sm" field="quantity" ><i class="fa fa-minus"></i></a>
															<span class="quantity">
																<input style="width: 40px;height: 35px;text-align: center;" type="text" name="quantity" value="1" id="cart_quantity">
															</span>
															<a type="button" value="+" class="qtyplus btn-sm change" field="quantity"><i class="fa fa-plus"></i></a>
														</div>
													</div>
												</div>
												
												<div class="col-sm-6" id="val_zip">
													<button class="btn btn-sm btn-primary cart-button cart-button" type="button" value="submit" title="Add to Cart" id="aaddcartbut" style="margin:0px 15px;" onclick="addToCart(<?php echo $product->id;?>);"><i class="fa fa-shopping-cart inner-right-vs"></i> Add To Cart</button>
												</div>
											</div><!-- /.row -->
										</div><!-- /.quantity-container -->
										<?php
										}?>
									</form>
									<?php } ?>
								
							</div><!-- /.product-info -->
						</div><!-- /.col-sm-7 -->
					</div><!-- /.row -->
                </div>
				
				<div class="product-tabs inner-bottom-xs  wow fadeInUp">
					<div class="row">
						<div class="col-sm-3">
							<ul id="product-tabs" class="nav nav-tabs nav-tab-cell">
								<li class="active"><a data-toggle="tab" href="#description">DESCRIPTION</a></li>
								<li><a data-toggle="tab" href="#review">REVIEW</a></li>
								<li style="display:none;"><a data-toggle="tab" href="#tags">TAGS</a></li>
							</ul><!-- /.nav-tabs #product-tabs -->
						</div>
						<div class="col-sm-9">
							<div class="tab-content">
								<div id="description" class="tab-pane in active">
									<div class="product-tab">
										<p class="text">
											<?php echo $product->description; ?>
										</p>
									</div>	
								</div><!-- /.tab-pane -->

								<div id="review" class="tab-pane">
									<div class="product-tab">	
										<div class="product-reviews">
											<h4 class="title">Rate & Review this Product</h4>
											</hr>
											<p id="pop_err_msg"></p>
											<!--rating section start -->
											<div class="col-md-12 pull-left">
												<?php $user_rate = $this->dentkart->user_rating($product -> id);
													if(empty($user_rate))
													{
														$user_rate = 0;
													} 
												?>
												<div class="aa-your-rating">
													<input id="rating-input" />
													<input type="hidden" name="prdid" id="prdid" value="<?php echo $product -> id; ?>" />
												</div>
												<script>
													$(document).ready(function(){
														$('#rating-input').rating({
															update: <?php echo $user_rate; ?>,
															min: 1,
															max: 5,
															step: 1,
															size: 'x',
															showClear: false,
															starCaptions: {
																1:'Disappointing',
																2:'Bad',
																3:'Average',
																4:'Good',
																5:'Perfect'
															}
														});
													});
												</script>
											</div>
											<!-- rating section end-->
											
											<div class="review-form">
												<div class="form-container">
													<form method="post" action="<?php echo base_url(); ?>ratenreview_controller/add_review" name="frm_review" id="frm_review" class="cnt-form">
													
													<input type="hidden" id="prdid" name="prdid" value="<?php echo $product -> id; ?>"/>
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label for="review_title">Title <span class="astk">*</span></label>
																	<input name="title" type="text" class="form-control txt" id="review_title" placeholder="">
																</div><!-- /.form-group -->
															</div>

															<div class="col-md-12">
																<div class="form-group">
																	<label for="pop_review">Review <span class="astk">*</span></label>
																	<textarea name="review" class="form-control txt txt-review" id="pop_review" rows="4" placeholder=""></textarea>
																</div><!-- /.form-group -->
															</div>
														</div><!-- /.row -->
														
														<div class="action text-right">
														
															<?php $this->load->library('session');
															$cust_id = $this->customer['id'];
															if(empty($cust_id)){?>
																<a href="#" data-toggle="modal" data-target="#authentication" class="btn btn-primary">SUBMIT REVIEW</a><?php
															}else{?>
																<a onclick="post_review_form();" class="btn btn-primary">SUBMIT REVIEW</a><?php
															}?>
														</div><!-- /.action -->
													</form><!-- /.cnt-form -->
												</div><!-- /.form-container -->
											</div><!-- /.review-form -->
										</div><!-- /.product-add-review -->
										
										<hr>
										<div class="product-reviews">
											<h4 class="title">Customer Reviews</h4>
											<?php //echo "<pre>";print_r($product_review_recent);exit;?>
											<?php foreach($product_review_recent as $prd_reviews){?>
												<div class="reviews">
													<div class="review">
														<div class="review-title">
															<span class="summary"><?php echo $prd_reviews->title; ?></span>
															<span class="date"><i class="fa fa-calendar"></i>
															<span><?php echo $prd_reviews->reviewdate; ?></span></span>
														</div>
														<div class="rating-box" style="margin-bottom:0 !important;">
															<ul class="clearfix">
															<?php
															  $user_review_rate = $this->dentkart->review_user_rating($prd_reviews -> product_id, $prd_reviews -> user_id);

															  if(empty($user_review_rate)){
																$user_review_rate = 0;
															  }

															  $active = $user_review_rate;
															  $nonactive = 5-$user_review_rate;
															  for ($x = 1; $x <= $active; $x++){
																echo '<li class="active"><a href="#"></a>
																</li>';
															  }

															  for ($x = 1; $x <= $nonactive; $x++){
																echo '<li>
																	<a href="#"></a>
																</li>';
															  }?>
															</ul>
														</div></br>
														<div class="text"><?php echo $prd_reviews->review; ?></div>
													</div>
												</div><!-- /.reviews -->
											<?php } ?>
										</div><!-- /.product-reviews -->
							        </div><!-- /.product-tab -->
								</div><!-- /.tab-pane -->

								<div id="tags" class="tab-pane">
									<div class="product-tag">
										
										<h4 class="title">Product Tags</h4>
										<form role="form" class="form-inline form-cnt">
											<div class="form-container">
									
												<div class="form-group">
													<label for="exampleInputTag">Add Your Tags: </label>
													<input type="email" id="exampleInputTag" class="form-control txt">
													

												</div>

												<button class="btn btn-upper btn-primary" type="submit">ADD TAGS</button>
											</div><!-- /.form-container -->
										</form><!-- /.form-cnt -->

										<form role="form" class="form-inline form-cnt">
											<div class="form-group">
												<label>&nbsp;</label>
												<span class="text col-md-offset-3">Use spaces to separate tags. Use single quotes (') for phrases.</span>
											</div>
										</form><!-- /.form-cnt -->

									</div><!-- /.product-tab -->
								</div><!-- /.tab-pane -->

							</div><!-- /.tab-content -->
						</div><!-- /.col -->
					</div><!-- /.row -->
				</div><!-- /.product-tabs -->
				
				<!-- ============================================== UPSELL PRODUCTS ============================================== -->
				<section class="section featured-product wow fadeInUp">
					<h3 class="section-title">Upsell products</h3>
					<div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
					
						<?php foreach($arrUpsellProducts as $uproduct){
						  $photo  = theme_img('no_picture.png', 'No Image Available');
						  $uproduct->images   = array_values((array)json_decode($uproduct->images));
						  if(!empty($uproduct->images[0])){
							$primary    = $uproduct->images[0];
							foreach($uproduct->images as $photo){
							  if(isset($photo->primary)){
								$primary    = $photo;
							  }
							}

							$photo  = '<img src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$uproduct->seo_title.'" style="" />';
						  }
						  $discount=round(100*($uproduct->price-$uproduct->saleprice)/$uproduct->price);

						  //[from_date] => 2016-06-20[to_date] => 2016-06-25
						  $start_date = strtotime($uproduct->from_date);
						  $end_date = strtotime($uproduct->to_date);
						  $currenttimstamp = time();
						  $prd_link = base_url().$uproduct->slug;
						  //site_url(implode('/', $base_url).'/'.$uproduct->slug);
						  $prd_display = true;

						  if($prd_display){
						?>
					
						<div class="item item-carousel">
							<div class="products">
								<div class="product">		
									<div class="product-image">
										<div class="image">
											<a href="<?php echo $prd_link;?>"><?php echo $photo;?></a>
										</div><!-- /.image -->
										<div class="tag sale">
											<span>
												<?php 
													$sales_price = $this -> dentkart -> product_price($uproduct);
													$discount=round(100*($uproduct->price-$sales_price)/$uproduct->price);
													if($uproduct->quantity < 1){
														echo 'Out of Stock';
													}else if($discount){?>
														<?php echo $discount;?>% <br> OFF
														<?php 
													} 
												?>
											</span>
										</div>               		   
									</div><!-- /.product-image -->
										
									
									<div class="product-info text-center">
										<h3 class="name"><a href="<?php echo $prd_link;?>"><?php echo $uproduct->name;?></a></h3>
										<div class="col-md-12">
											<div class="rating-box">
												<ul class="clearfix"><?php
													$all_ret = $this->dentkart->product_rating($uproduct -> id);
													$product_rating = explode("*-*", $all_ret);
													$avg_rating = explode("*-*", $all_ret);
													//print_r($product_rating);

													$star1 	=$product_rating[0];
													$star2 	=$product_rating[1];
													$star3 	=$product_rating[2];
													$star4 	=$product_rating[3];
													$star5 	=$product_rating[4];
													$starall 	=$product_rating[5];
													$avt_rating = (1*($star1) + 2*($star2) +3*($star3) +4*($star4) + 5*($star5))/($starall);
													$avt_rating = number_format((float)$avt_rating, 1, '.', '');
													if(empty($avt_rating)){
														$avt_rating = 0;
													}
													for($i = 0; $i < $avt_rating; $i++){
														echo '<li class="active"><a href="#"></a></li>';
													}

													$dact = 5-$avt_rating;
													for($j = 0; $j < $dact; $j++){
														echo '<li><a href="#"></a></li>';
													}?>
													<li class="rc">(<?php echo $starall; ?> Rating)</li>
												</ul>
											</div>
										</div>
										<div class="description"></div>
										<div class="product-price">	
												<?php if($discount > 0):?>
													<span class="price">
														₹ <?php echo $uproduct->saleprice;?>	</span>
													<span class="price-before-discount">₹ <?php echo $uproduct->price;?></span>
												<?php else: ?>
												  <span class="price">₹ <?php echo $uproduct->price;?></span>
												<?php endif;?>
										</div><!-- /.product-price -->
									</div><!-- /.product-info -->
									
									<div class="cart clearfix animate-effect">
										<div class="action">
											<ul class="list-unstyled">
												<li class="lnk">
													<a href="<?php echo site_url(implode('/', $base_url).'/'.$uproduct->slug); ?>">
													<i class="fa fa-shopping-cart"></i></a>
												</li>
											   
												<li class="lnk wishlist">
													<?php if($this->Customer_model->is_logged_in(false, false)){?>
														<a onclick="add_to_wishlist(<?php echo $uproduct->id;?>);" class="add-to-cart" href="href="javascript:;" title="Wishlist">
														<i class="icon fa fa-heart"></i></a>
													<?php
													}else{?>
														<a class="add-to-cart" href="#" title="Wishlist"data-toggle="modal" data-target="#authentication">
														<i class="icon fa fa-heart"></i></a>
													<?php
													}?>
												</li>

												<li class="lnk">
													<?php if($this->Customer_model->is_logged_in(false, false)){?>
														<a onclick="add_to_compare(<?php echo $uproduct->id;?>);" class="add-to-cart"  title="Compare">
														<i class="fa fa-signal"></i></a>
													<?php
													}else{?>
														<a class="add-to-cart" href="#" title="Compare" data-toggle="modal" data-target="#authentication">
														<i class="fa fa-signal"></i></a>
													<?php
													}?>
												</li>
											</ul>
										</div><!-- /.action -->
									</div><!-- /.cart -->
								</div><!-- /.product -->
							</div><!-- /.products -->
						</div><!-- /.item -->
						
						<?php } }?>   
						
						
					</div><!-- /.home-owl-carousel -->
				</section><!-- /.section -->
				<!-- ============================================== UPSELL PRODUCTS : END ============================================== -->
			
			</div><!-- /.col -->
			<div class="clearfix"></div>
		</div><!-- /.row -->
			
	</div><!-- /.container -->
</div><!-- /.body-content -->


<script>

jQuery(document).ready(function(){
    // This button will increment the value
    $('.qtyplus').click(function(e){
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('input[name='+fieldName+']').val());
        // If is not undefined
        if (!isNaN(currentVal)) {
            // Increment
            $('input[name='+fieldName+']').val(currentVal + 1);
        } else {
            // Otherwise put a 0 there
            $('input[name='+fieldName+']').val(0);
        }
    });
    // This button will decrement the value till 0
    $(".qtyminus").click(function(e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('input[name='+fieldName+']').val());
        // If it isn't undefined or its greater than 0
        if (!isNaN(currentVal) && currentVal > 0) {
            // Decrement one
            $('input[name='+fieldName+']').val(currentVal - 1);
        } else {
            // Otherwise put a 0 there
            $('input[name='+fieldName+']').val(0);
        }
    });

})

	if($(window).width() > 768){
		// Hide all but first tab content on larger viewports
		$('.accordion__content:not(:first)').hide();
		
		// Activate first tab
		$('.accordion__title:first-child').addClass('active');
	} else {
		// Hide all content items on narrow viewports
		$('.accordion__content').hide();
	};

	// Wrap a div around content to create a scrolling container which we're going to use on narrow viewports
	//$( ".accordion__content" ).wrapInner( "<div class='overflow-scrolling'></div>" );
	// The clicking action
	$('.accordion__title').on('click', function() {
		$('.accordion__content').hide();
		$(this).next().show().prev().addClass('active').siblings().removeClass('active');
	});


	function checkzipcode(){
		var zip = $.trim($("#pincode").val());
		var zipRegex = /^\d{6}$/;
		var msg = '';
		if (!zipRegex.test(zip)){
			$("#chckpinmsg-green").html('');
			$("#chckpinmsg-red").html("Please enter valid zip code.");
		}else{
			$.ajax({
				url: '<?php echo base_url(); ?>cart/checkzip',
				type: 'post',
				data: {'zipcode':zip},
				dataType:"json",
				success: function(data) {
					if(data != '0'){
						var zip_details = eval(data);
						//{"id":"43","postcode":"110043","isship":"1","iscod":"1","daystodeliver":"2 to 4"}
						
						if(zip_details['isship'] == '1'){
							$("#addcartbut").prop("disabled", false);
							if(zip_details['iscod'] == '1'){
								msg = 'Yes, Cash on Delivery available in your area and We can deliver product in '+zip_details['iscod']+' Days';
								$("#chckpinmsg-green").html(msg);
								$("#chckpinmsg-red").html('')
							}else{
								msg = 'Sorry, Cash on Delivery not available in your area and We can deliver product in '+zip_details['iscod']+' Days';
								$("#chckpinmsg-green").html('');
								$("#chckpinmsg-red").html(msg);
							}
						}else{
							$("#addcartbut").prop("disabled", true);
							$("#chckpinmsg-green").html('');
							$("#chckpinmsg-red").html("Sorry, We could not provide shipping service in your area.");
						}
					}else{
						$("#addcartbut").prop("disabled", true);
						$("#chckpinmsg-green").html('');
						$("#chckpinmsg-red").html("Sorry, We could not provide shipping service in your area.");
					}
				}
			});
		}			
	}
	
	$('#rating-input').on('rating.change', function(event, value, caption) {
		product_id = $("#prdid").val();
		$.ajax({
		  url: '<?php echo base_url(); ?>ratenreview_controller/add_rating',
		  type: 'post',
		  data: {'product_id':product_id, 'rating':value},
		  success: function(data) {
			if(data == 'logout'){
			  //url = "<?php echo base_url(); ?>secure/login";
			  //$( location ).attr("href", url);
			   $("#authentication").modal();
			}else if(data == 'no'){
			  msg = 'You already rate this product.';
			  $('#pop_err_msg').html('<div class="alert alert-success">'+msg+ '<i class="close"></i></div>');
				//$("html, body").animate({ scrollTop: "0px" });
			  //location.reload();
			}else if(data == 'yes'){
			  msg = '';
			  msg = 'Your rating added for this product successfully.';
			  $.blockUI({ 
				css: { 
				  border: 'none', 
				  padding: '15px',
				  message: 'Add to Compare successfully',
				  backgroundColor: '#000', 
				  '-webkit-border-radius': '10px', 
				  '-moz-border-radius': '10px', 
				  opacity: .5, 
				  color: '#fff' 
				} 
			  });
			  setTimeout($.unblockUI, 2000);
			  $('#pop_err_msg').html('<div class="alert alert-success">'+msg+ '<i class="close"></i></div>');
				//$("html, body").animate({ scrollTop: "0px" });
			  //location.reload();
			} 
		  }
		});
	});
</script>

<?php

function theme_img($uri, $tag=false)
{
	if($tag)
	{
		return '<img src="'.theme_url('assets/assets/images/'.$uri).'" alt="'.$tag.'">';
	}
	else
	{
		return theme_url('assets/assets/images/'.$uri);
	}
	
}
function theme_url($uri)
{
	$CI =& get_instance();
	return $CI->config->base_url('/'.$uri);
}

$this->load->view('vwFooter');
?>
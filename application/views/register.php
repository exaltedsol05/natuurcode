<?php $this->load->view('vwHeader');?>

<?php

$first		= array('id'=>'firstname', 'class'=>'form-control', 'name'=>'firstname', 'value'=> set_value('firstname'));
$last		= array('id'=>'lastname', 'class'=>'form-control', 'name'=>'lastname', 'value'=> set_value('lastname'));
$email		= array('id'=>'email', 'class'=>'form-control', 'name'=>'email', 'value'=>set_value('email'));
$phone		= array('id'=>'phone', 'class'=>'form-control', 'name'=>'phone', 'value'=> set_value('phone'));
?>
 <!--breadcrumbs area start-->
    <div class="breadcrumbs_area">
        <div class="container">   
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <ul>
                            <li><a href="index.html">home</a></li>
                            <li><a href="login.html">login</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>         
    </div>
    <!--breadcrumbs area end-->
    
    
    <!-- customer login start -->
    <div class="customer_login">
        <div class="container">
            <div class="row">
               
                <!--login area start-->

                <!--register area start-->
                <div class="col-lg-6 col-md-6 offset-md-3 offset-lg-3">
                    <div class="account_form register">
                        <h2 style="text-align:center;">Register</h2>
                        <form method="post" action="<?php echo base_url();?>secure/register" id="reg_form"  />
					<input type="hidden" name="hidden_otp" id="hidden_otp" value="" />
					<input type="hidden" name="redirect" id="redirect" value="" />
					<input type="hidden" name="submitted" value="submitted" />
					<input type="hidden" name="redirect" value="<?php echo $redirect;?>" />
                            <p>   
                               <label for="fname">First Name <span>*</span></label>
                               
								<?php echo form_input($first);?>
						        <span class="text-danger"><?php echo form_error('firstname'); ?></span>
                             </p>
                             <p>   
                                 <label for="lname">Last Name <span>*</span></label>
                                <?php echo form_input($last);?>
								<span class="text-danger"><?php echo form_error('lastname'); ?></span>
                             </p>
							 <p>
							 <label for="email">Email Address <span>*</span></label>
                                <?php echo form_input($email);?>
								<span class="text-danger"><?php echo form_error('email'); ?></span>
							 </p>
							<p>
								<label for="phone">Mobile Number  <span>*</span></label>
								<?php echo form_input($phone);?>
								<span class="text-danger"><?php echo form_error('phone'); ?></span>
							</p>
							<p>
							<label for="Password">Password <span>*</span></label>
                               <input type="password" name="password" id="password" value="" class="form-control" autocomplete="off" />
							   
						      <span class="text-danger"><?php echo form_error('password'); ?></span>
							 </p>
							 <p>
							 <label for="cPassword">Confirm Password <span>*</span></label>
                               <input type="password" name="confirm" value="" class="form-control" autocomplete="off" />
						       <span class="text-danger"><?php echo form_error('confirm'); ?></span>
							 </p>
							 <p>
								
								<input type="checkbox" style="height: 14px;width: 16px;margin: 0px 7px 0px 0px;" name="email_subscribe" value="1" <?php echo set_radio('email_subscribe', '1', TRUE); ?>/>Subscribe to our email list
								
							</p>
                            <div class="login_submit1">
                                <button type="submit" class="btn btn-lg btn-block" style="margin-left:0px;">Register</button>
                            </div>
							<p style="text-align: center;margin-top: 20px;">
							Already have an account?&nbsp;&nbsp;<a href="<?php echo site_url('secure/login'); ?>" style=" text-decoration: underline;color: #83b53b;"> Login</a>
							</p>
                        </form>
                    </div>    
                </div>
                <!--register area end-->
            </div>
        </div>    
    </div>
    <!-- customer login end -->

<?php 
$this->load->view('vwFooter');
?>
 <style>
.error {
	color:#FF0000;  /* red */
}

</style>
 
 <!--footer area start-->
    <footer class="footer_widgets footer_three">
        <div class="container">  
            <div class="footer_top">
                <div class="row">
                    <div class="col-lg-4 col-md-4 fFirst">
                        <div class="widgets_container contact_us">
                            <h3>CONTACT US</h3>
                            <div class="footer_contact">
                                <ul>
                                    <li><i class="ion-ios-location"></i><span>Addresss:</span> 
									Natuur Manufacturing Pvt Ltd, <br>
									Plot number 49<br>
									HSIIDC , Udyog Kunj,<br>
									Alipur , Sohna,<br>
									District Gurugram Pin - 122102
									</li>
                                    <li><i class="ion-ios-telephone"></i><span>Call Us:</span>9718500111</li>
                                    <li><i class="ion-android-mail"></i><span>Email:</span>management@natuur.in</li>
                                </ul>
                            </div>
							<h3>NEWSLETTER SIGNUP!</h3>
							<div class="newsletter_form">
                                <form action="#" method="post">
                                    <input placeholder="NEWSLETTER SIGNUP!" type="text" id="news_email">
                                    <button id="newslatter"><i class="ion-android-mail"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-3 fSecond">
                        <div class="widgets_container widget_menu">
                            <h3>NATUUR</h3>
                            <div class="footer_menu">
                                <ul>

                                    <li><a href="<?php echo site_url('contact-us');?>">Contact Us</a></li>
                                    <li><a href="<?php echo site_url('about-us');?>">About Us</a></li>
                                    <li><a href="<?php echo site_url('blog');?>">Blog</a></li>
                                    <li><a href="<?php echo site_url('careers');?>">Careers</a></li>
                                    <li><a href="<?php echo site_url('ingredients');?>">Ingredients</a></li>
                                   
                                </ul>
                            </div>

                        </div>
                    </div>
					<div class="col-lg-3 col-md-2 fThird">
                        <div class="widgets_container widget_menu">
                            <h3>HELP</h3>
                            <div class="footer_menu">
                                <ul>
                                    <li><a href="https://www.natuur.in/secure/login">Register / Login</a></li>
                                    <li><a href="<?php echo site_url('payments-saved-cards');?>">Payments & Saved Cards</a></li>
                                    <li><a href="<?php echo site_url('shipping-delivery');?>">Shipping & Delivery</a></li>
                                    <li><a href="<?php echo site_url('cancellation-returns');?>">Cancellation & Return</a></li>
                                    <li><a href="<?php echo site_url('faqs');?>">FAQ's</a></li>
                                    <li><a href="<?php echo site_url('privacy-policy');?>">Privacy Policy</a></li>
                                </ul>
                            </div>

                        </div>
                    </div>					
                    <div class="col-lg-3 col-md-3 fFourth">
                       <div class="widgets_container widget_menu">
                            <h3>WHY BUY FROM NATUUR</h3>
                            <div class="footer_menu">
                                <ul>

                                    <li><a href="<?php echo site_url('secure-shopping');?>">Secure Shopping</a></li>
                                    <li><a href="<?php echo site_url('easy-buy');?>">Easy Buy</a></li>
                                    <li><a href="<?php echo site_url('hassle-free-return-policy');?>">Hassle Free Return Policy</a></li>
                                    <li><a href="<?php echo site_url('gift-natuur');?>">Gift Natuur</a></li>
                                    <li><a href="<?php echo site_url('naturr-loyalty-points');?>">Natuur Loyalty Points</a></li>                                   
                                </ul>
                            </div>
                        </div>
						<div class="footer_social">
							<ul>
								<li><a href="https://www.facebook.com/NatuurIndia/" target="_blank"><i class="ion-social-facebook"></i></a></li>
								<!-- <li><a href="https://twitter.com/NaturlaMama" target="_blank"><i class="ion-social-twitter"></i></a></li> -->
								<li><a href="https://www.instagram.com/natuur.in/?hl=en"><i class="ion-social-instagram" target="_blank"></i></a></li>
								<!-- <li><a href="https://www.linkedin.com/company/natural-mama-india" target="_blank"><i class="ion-social-linkedin"></i></a></li> -->
							</ul>
						</div>
                    </div>                   
                </div>
            </div>
        </div>
         <div class="footer_bottom">      
            <div class="footer_adjust">
               <div class="row">
                    <div class="col-lg-6 col-md-5">
                        <div class="footer_payment">
							<h5>Payment Methods</h5>
                            <!-- <a><img src="<?php echo base_url('assets/img/icon/papyel.png'); ?>" alt=""></a> -->
                            <a><img src="<?php echo base_url('uploads/home_static_image/payment-method.png'); ?>" alt=""></a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-7">
                        <div class="copyright_area">
                            <p>Copyright &copy; <?php echo date('Y');?><a href="https://natuur.in/">Natuur</a>All Right Reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>     
    </footer>
    <!--footer area end-->
<!-- Login Modal Start -->
<div class="modal fade" role="dialog" id="myLoginModal">
    <div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">			
			<!-- Modal body -->
			<div class="modal-body">
			<div class="closeBtnModal"><button type="button" class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button></div>
			<div class="row" id="loginModal">
				<div class="col-md-3 login_left">
					<div class="loginInformation" id="popupLeftInformation">
						<h3>Login</h3>
						<p>Get access to your Orders, Wishlist and Recommendations</p>
						<div class="loginIcon"><i class="fa fa-user" aria-hidden="true"></i></div>
					</div>
				</div>
				<div class="col-md-9 login_right">
					<div id="loginForm">
						<div class="login_modal_section">
							<input type="text" class="inputText" id="popup_login_phone_input" maxlength="10" autocomplete="off" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required="">
							<span class="floating-label">Enter Mobile Number</span>
							<span class="login_forgot_password d-none"><a href="#">Change?</a></span>
							<div class="error_mesg" id="popup_login_phone_input_error"></div>
						</div>
						<div class="login_modal_section">
							<input type="password" class="inputText" id="popup_login_password_input" required="">
							<span class="floating-label">Enter Password</span>
							<span class="login_forgot_password fPasswordLink"><a href="javascript:void(0)">Forgot?</a></span>
							<div class="error_mesg" id="popup_login_password_input_error"></div>
						</div>
						<div class="login_privacy_policy">By continuing, you agree to Natuur <a href="#">Terms of Use</a> and <a href="#">Privacy Policy.</a></div>
						<div class="checkout_continue"><button class="btn" id="popup_login_btn">Login</button></div>
						<div class="new_ccount signupLink"><a href="javascript:void(0)">Looking to create an account? Signup</a></div>
					</div>
					<div id="SignupForm" style="display:none;">
						<div class="login_modal_section" id="popup_signup_mobile_div">
							<input type="text" class="inputText" id="popup_signup_phone_input" maxlength="10" autocomplete="off" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required="">
							<span class="floating-label">Enter Mobile Number</span>
							<span class="login_forgot_password d-none"><a href="#">Change?</a></span>
							<div class="error_mesg" id="popup_signup_phone_input_error"></div>
						</div>
						<div class="login_privacy_policy">By continuing, you agree to Natuur <a href="#">Terms of Use</a> and <a href="#">Privacy Policy.</a></div>
						<div class="checkout_continue"><button class="btn" id="popup_signup_btn">Submit</button></div>
						<div class="new_ccount loginLink"><a href="javascript:void(0)">Already have an account? Login</a></div>
					</div>
					<div id="fPasswordForm" style="display:none;">
						<div class="login_modal_section" id="popup_fpassword_mobile_div">
							<input type="text" class="inputText" id="popup_fpassword_phone_input" maxlength="10" autocomplete="off" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required="">
							<span class="floating-label">Enter Mobile Number</span>
							<span class="login_forgot_password d-none"><a href="#">Change?</a></span>
							<div class="error_mesg" id="popup_fpassword_phone_input_error"></div>
						</div>
						<div class="checkout_continue"><button class="btn" id="popup_fpassword_btn">Submit</button></div>
						<div class="new_ccount loginLink"><a href="javascript:void(0)">Return to Login</a></div>
					</div>
					<div class="d-none">
						<div class="resend_otp">
							<div class="otp_left">OTP sent to Mobile</div>
							<div class="otp_right"><a href="#">Resend?</a></div>
						</div>
						<div class="login_modal_section login_input_number">
							<input type="number" class="inputText" required="">
							<span class="floating-label">Enter OTP</span>
							<div class="error_mesg"><span>Please enter valid OTP</span></div>
						</div>
						<div class="login_modal_section">
							<input type="password" class="inputText" required="">
							<span class="floating-label">Set Password</span>
							<div class="error_mesg"><span>Plesae Set Password</span></div>
						</div>
						<div class="login_privacy_policy">By continuing, you agree to Natuur <a href="#">Terms of Use</a> and <a href="#">Privacy Policy.</a></div>
						<div class="checkout_continue"><button class="btn">Login</button></div>
						<div class="checkout_continue_signup d-none"><button class="btn">New to Flipcart? Sign Up</button></div>
						<!--<div class="login_or">or</div>
						<div class="checkout_request_otp"><button class="btn">New to Flipcart? Sign Up</button></div>-->
						<div class="new_ccount"><a href="#">New to Flipkart? Create an account</a></div>
					</div>
				</div>
			</div>
			</div>       
		</div>
    </div>
</div>
<!-- Login Modal End -->
<!-- Plugins JS -->
<script src="<?php echo HTTP_JS_PATH; ?>plugins.js"></script>
<!-- Main JS -->
<script src="<?php echo HTTP_JS_PATH; ?>main.js?v10"></script>
<script src="<?php echo HTTP_JS_PATH; ?>equal_heights.js"></script>
<script src="<?php echo HTTP_JS_PATH; ?>jquery.blockUI.js"></script>
<script src="<?php echo HTTP_JS_PATH; ?>jquery.validate.min.js"></script>
<script src="<?php echo HTTP_JS_PATH; ?>star-rating.min.js"></script>
<script src="<?php echo HTTP_JS_PATH; ?>jquery.cookieMessage.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
});
</script>
<script type="text/javascript">
$.cookieMessage({
    'mainMessage': 'This website uses cookies. By using this website you consent to our use of these cookies. For more information visit our <a href="<?php echo site_url('privacy-policy');?>">Privacy Policy</a>. ',
    'acceptButton': 'Got It!',
    'fontSize': '16px',
    'backgroundColor': '#222',
});
</script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5e202b9b27773e0d832dd285/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

<script type="text/javascript">

$(window).on('load', function() { // makes sure the whole site is loaded 
  $('#status').fadeOut(); // will first fade out the loading animation 
  $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website. 
  $('body').delay(350).css({'overflow':'visible'});
});
	$(document).ready(function(){
		
		//if we support placeholder text, remove all the labels
		if(!supports_placeholder()){
			$('.placeholder').show();
		}<?php
		// Restore previous selection, if we are on a validation page reload
		$zone_id = set_value('zone_id');
		echo "\$('#zone_id').val($zone_id);\n";
		$zone_id1 = set_value('zone_id1');
		echo "\$('#zone_id1').val($zone_id1);\n";
		echo "\$('#zone_id2').val($zone_id1);\n";
		?>
	});
	function supports_placeholder()
	{
		return 'placeholder' in document.createElement('input');
	}


$(document).ready(function() {
	$('#country_id').change(function(){
		populate_zone_menu();
	});	

	$('#country_id1').change(function(){
		populate_zone_menu1();
	});	
});

// context is ship or bill

function populate_zone_menu(value)
{
	$.post('<?php echo site_url('locations/get_zone_menu');?>',{id:$('#country_id').val()}, function(data) {
		$('#zone_id').html('<option>Please choose state</option>'+data);
		$('.selectpicker').selectpicker('refresh');
	});
}
function populate_zone_menu1(value)
{
	$.post('<?php echo site_url('locations/get_zone_menu');?>',{id:$('#country_id1').val()}, function(data) {
		$('#zone_id1').html('<option>Please choose state</option>'+data);
		$('#zone_id2').html('<option>Please choose state</option>'+data);		
		$('.selectpicker').selectpicker('refresh');
	});
}	
$(document).ready(function(){
		/* 
		$(".payment_mode").change(function(){
		var zipcode = '';
		if(!document.getElementById("use_shipping").checked){
			var str_zip = document.forms["checkoutform"]["zip1"].value;
			if (str_zip == null || str_zip == "") {
				alert("Shipping address city must be filled out");
				document.forms["checkoutform"]["zip1"].focus();
				$(".payment_mode").prop('checked', false);
				return false;
			}else{
				zipcode = str_zip; 
			}
		}else{
			var str_zip = document.forms["checkoutform"]["zip"].value;
			if (str_zip == null || str_zip == "") {
				alert("Billing address city must be filled out");
				document.forms["checkoutform"]["zip"].focus();
				$(".payment_mode").prop('checked', false);
				return false;
			}else{
				zipcode = str_zip; 
			}
		}		
		//change button text and show bank details
		var selectedVal = "";
		var selected = $(".radio input[class='payment_mode']:checked");
		if (selected.length > 0) {
			selectedVal = selected.val();
		}		
		if(selectedVal == 'payu_money'){
			$("#confirm_btn").val('Proceed to payment');
			$("#bank_transfer_details").slideUp('fast');
		}else if(selectedVal == 'bank_transfer_payment'){
			$("#confirm_btn").val('Place Order');	
			$("#bank_transfer_details").slideDown('fast');
		}else{
			$("#bank_transfer_details").slideUp('fast');
			$("#confirm_btn").val('Place Order');	
		}
		$.ajax({
			url: '<?php echo base_url(); ?>cart/checkzip',
			type: 'post',
			data: {'zipcode':zipcode},
			dataType:"json",
			success: function(data){
				if(data.color == 'green'){
					$("#chckpinmsg-red").html('');
					grant_total = $("#order_grant_total").val();
					if(data.message == 'Only Prepaid Delivery Available'){
						$("#cod_mode").prop('checked', false);
						$("#chckpinmsg-green").html(data.message+'<br/> Expected Time : '+data.estimate_time);
					}else if(grant_total > 10000){
						grant_total = $("#order_grant_total").val();
						$("#cod_mode").prop('checked', false);
						$("#chckpinmsg-green").html("Only Prepaid Delivery Available if order total is greater then <i class='fa fa-inr'></i>10,000'<br/> Expected Time : "+data.estimate_time);
					}else{
						$("#chckpinmsg-green").html(data.message+'<br/> Expected Time : '+data.estimate_time);
						$("#chckpinmsg-red").html('');					
					}					
				}else{
					$(".payment_mode").prop('checked', false);
					$("#chckpinmsg-red").html(data.message);
					$("#chckpinmsg-green").html('');
				}
			}
		});		
	}); */
    $('#use_shipping').change(function(){
        if(this.checked){
			$(".shiping_fa  ").addClass("fa-check");
		}else{
			$(".shiping_fa  ").removeClass("fa-check");
		}     
    });	
	$(".ishipping_charges").click(function(){
		var charge_shipped = $(this).attr('ship_charge');
		if($(".ishipping_charges").is(':checked')){ 
			$(".shippingtr").css("display", "table-row");
			$(".antishippingtr").css("display", "none");
		}else{
			$(".shippingtr").css("display", "none");
			$(".antishippingtr").css("display", "table-row");
		}
	});
	$("#checkout_info").on('click', '#otp_login_btn', function () {
		if ($('#chk_email').val() != '') {
			$('#chk_user').attr('disabled', true);
			var ep_emailval = $('#chk_email').val();
			var intRegex = /[0-9 -()+]+$/;

			if(intRegex.test(ep_emailval)) {
				if((ep_emailval.length < 10) || (!intRegex.test(ep_emailval)))
				{
					$('#mail-error').html('Please enter a valid phone number.');
					$('#chk_user').attr('disabled', false);
					return false;
				}else{
					$('#mail-error').html('');
				}
				var textForm = 'phone';
			}
			$.post("<?php echo site_url('secure/ajax_otp_login');?>", { usermail: $('#chk_email').val(),userotp: $('#chk_otp').val(),userpw: $('#new_password').val(),textForm: textForm },
			function(data){
				if(data==1){
					location.reload();
				}else{
					$('#mail-error').html('Otp not matched');
					return false;
				}
			});
		}else{
			$('#mail-error').html('Please enter Phone or Email');
			return false;
		}
	});
	$("#checkout_info").on('click', '#forgot_password_link', function () {
		if ($('#chk_email').val() != '') {
			$('#chk_user').attr('disabled', true);
			var ep_emailval = $('#chk_email').val();
			var intRegex = /[0-9 -()+]+$/;

			if(intRegex.test(ep_emailval)) {
				if((ep_emailval.length < 10) || (!intRegex.test(ep_emailval)))
				{
					$('#mail-error').html('Please enter a valid phone number.');
					$('#chk_user').attr('disabled', false);
					return false;
				}else{
					$('#mail-error').html('');
				}
				var textForm = 'phone';
			}
			$.post("<?php echo site_url('secure/ajax_sent_otp');?>", { usermail: $('#chk_email').val(),textForm: textForm },
			function(data){
				if(data==1){
					$('.password_div,.otpPassword_div').remove();
					//$('.password_div').remove();
					var otpPasswordField = '<div class="otpPassword_div logedout_email_mobile"><input type="text" id="chk_otp" name="otp" class="inputText" placeholder=""><span class="floating-label">Enter Otp</span><div id="otp-error" class="error_mesg"></div></div><div class="otpPassword_div logedout_email_mobile"><input type="password" id="new_password" name="new_password" class="inputText" placeholder=""><span class="floating-label">New Password</span><div id="new_password-error" class="error_mesg"></div></div>';
					$('#chk_user').attr('disabled', false);
					$(".btn_div").html('<button type="button" class="btn" id="otp_login_btn">Submit</button>');
					$(otpPasswordField).insertAfter("#mailField");
				}
			});
		}else{
			$('#mail-error').html('Please enter Phone or Email');
			return false;
		}
	});
	$("#checkout_info").on('click', '#login_btn', function () {
		if ($('#chk_email').val() != '') {
			$('#chk_user').attr('disabled', true);
			var ep_emailval = $('#chk_email').val();
			var intRegex = /[0-9 -()+]+$/;

			if(intRegex.test(ep_emailval)) {
				if((ep_emailval.length < 10) || (!intRegex.test(ep_emailval)))
				{
					$('#mail-error').html('Please enter a valid phone number.');
					$('#chk_user').attr('disabled', false);
					return false;
				}else{
					$('#mail-error').html('');
				}
				var textForm = 'phone';
			}
			$.post("<?php echo site_url('secure/ajax_login');?>", { usermail: $('#chk_email').val(),userpass: $('#chk_password').val(),textForm: textForm },
			function(data){
				if(data==1){
					location.reload();
				}
				if(data==2){
					$('#mail-error').html('Your username or password is incorrect.');
				}
			});
		}else{
			$('#mail-error').html('Please enter Phone or Email');
			return false;
		}
	});
	$('#chk_user').click(function(){
		if ($('#chk_email').val() != '') {
			$('#chk_user').attr('disabled', true);
			var ep_emailval = $('#chk_email').val();
			var intRegex = /[0-9 -()+]+$/;

			if(intRegex.test(ep_emailval)) {
				if((ep_emailval.length < 10) || (!intRegex.test(ep_emailval)))
				{
					$('#mail-error').html('Please enter a valid phone number.');
					$('#chk_user').attr('disabled', false);
					return false;
				}else{
					$('#mail-error').html('');
				}
				var textForm = 'phone';
			}
			$.post("<?php echo site_url('secure/chk_user');?>", { userVal: $('#chk_email').val(),textForm: textForm },
			function(data){
				if(data==1){
					var passwordField = '<div class="password_div logedout_email_mobile"><input type="password" id="chk_password" name="password" class="inputText" placeholder=""><span class="floating-label">Password</span><div id="password-error" class="error_mesg"></div></div><a  href="javascript:void(0)" id="forgot_password_link">Forgot password?</a>';
					$('#chk_user').attr('disabled', false);
					$(".btn_div").html('<button type="button" class="btn" id="login_btn">Login</button>');
					$(passwordField).insertAfter("#mailField");
					$("#chk_password").focus();
				}
				if(data==2){
					$('.password_div,.otpPassword_div').remove();
					//$('.password_div').remove();
					var otpPasswordField = '<div class="otpPassword_div logedout_email_mobile"><input type="text" id="chk_otp" name="otp" class="inputText" placeholder=""><span class="floating-label">Enter Otp</span><div id="otp-error" class="error_mesg"></div></div><div class="otpPassword_div logedout_email_mobile"><input type="password" id="new_password" name="new_password" class="inputText" placeholder=""><span class="floating-label">Set Password  </span><div id="new_password-error" class="error_mesg"></div></div>';
					$('#chk_user').attr('disabled', false);
					$(".btn_div").html('<button type="button" class="btn" id="otp_login_btn">Submit</button>');
					$(otpPasswordField).insertAfter("#mailField");
					$("#chk_otp").focus();
				}
			});
		}else{
			$('#mail-error').html('Please enter Phone or Email');
			return false;
		}
	});
	
	function change_checkout_qty(fieldName,result) {
		// Stop acting like a button
		//e.preventDefault();
		// Get the field name
		fieldName = fieldName;
		// Get its current value
		var currentVal = parseInt($('input[id='+fieldName+']').val());
		// If is not undefined
		if (!isNaN(currentVal)) {
		// Increment
		if(result==1){
			$('input[id='+fieldName+']').val(currentVal + 1);
		}else if(result==2){
			var currentVal = parseInt($('input[id='+fieldName+']').val());
			// If it isn't undefined or its greater than 0
			if (!isNaN(currentVal) && currentVal > 1) {
			// Decrement one
			$('input[id='+fieldName+']').val(currentVal - 1);
			} else {
			// Otherwise put a 0 there
			$('input[id='+fieldName+']').val(1);
			}
		}
		$.ajax({
			type: "POST",
			datatype:"json",
			data: $("#checkoutform").serialize(),
			url: "<?php echo base_url();?>cart/update_cart", 
			success: function(msg){
				$("#checkout_section_right").load(window.location.href+" .list1");
				$("#checkout_order_summary").load(window.location.href+" .list", function() {
					$(".qtycheckoutplus").on( "click", function(e) {
					fieldName = $(this).attr('field');
						change_checkout_qty(fieldName,1);
					});
				   // This button will decrement the value till 0
				   $(".qtycheckoutminus").click(function(e) {
						fieldName = $(this).attr('field');
						change_checkout_qty(fieldName,2);
				   });
					$("#checkout_address").on('click', '.edit_user_address', function () {
					//$(".edit_user_address").click(function(){
						var rel = $(this).attr('rel');
						function_add_edit_user_address(rel);
					});
					$("#checkout_address").on('change', 'input[type=radio][name=address_checked]', function () {
						var rel = $(this).val();
						function_address_checked(rel);
					});
					$("#checkout_address").on('click', '.cancel_address', function () {
						var rel = $("input[type=radio][name=address_checked]:checked").val();
						function_cancel_address(rel);
					});
					$("#checkout_address").on('click', '.deliver_here_btn', function () {
						var rel = $(this).attr('data-id');
						select_ship_address(rel);
					});
					$("#delivered_address").on('click', '#change_delivered_address', function () {
						$('#delivered_address_details,#address_error').html('');
						$('#delivered_address,#checkout_order_summary,#checkout_payment_options').hide();
						$('#checkout_address,#delivered_summary,#delivered_payment').show();
					});
					$("#delivered_summary").on('click', '#change_order_summary', function () {
						$('#address_error').html('');
						$('#delivered_address,#checkout_order_summary,#delivered_payment').show();
						$('#checkout_address,#delivered_summary,#checkout_payment_options').hide();
					});
					$("#checkout_order_summary").on('click', '.cart_continue', function () {
						$('#address_error').html('');
						$('.change_order_btn').html('<div class="changeBtn"><button type="button" class="btn" id="change_order_summary">change</button></div>');
						$('#checkout_order_summary,#delivered_payment').hide();
						$('#checkout_payment_options,#delivered_summary').show();
					});
					$("#confirm_btn").click(function(){
						$("#checkoutform").submit();
					})
					$(".qtycheckoutremove").on( "click", function(e) {
						fieldName = $(this).attr('data-id');
						ajax_remove_cart_item_list(fieldName);
					});
					function save_address()
					{
						address_save();
					}
					$.ajax({
						type: "POST",
						datatype:"json",
						data: $("#checkoutform").serialize(),
						url: "<?php echo base_url();?>secure/select_ship_address_cart", 
						success: function(response){
							obj = jQuery.parseJSON(response);
							//alert(obj.get_shipping_fees);
							$('.cart_final_subtotal').html('<span class="fa fa-rupee"></span> '+obj.cart_subtotal);
							$('.shipping_fee').html('<span class="fa fa-rupee"></span> '+obj.get_shipping_fees);
						}
					});
				});
			}
		});
		
		} else {
			// Otherwise put a 0 there
			$('input[id='+fieldName+']').val(0);
		}
	}
	function ajax_remove_cart_item_list(fieldId){
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>cart/ajax_remove_cart_item/"+fieldId, 
			success: function(data){
				$("#checkout_section_right").load(window.location.href+" .list1");
				$("#checkout_order_summary").load(window.location.href+" .list", function() {
					$(".qtycheckoutplus").on( "click", function(e) {
					fieldName = $(this).attr('field');
						change_checkout_qty(fieldName,1);
					});
				   // This button will decrement the value till 0
				   $(".qtycheckoutminus").click(function(e) {
						fieldName = $(this).attr('field');
						change_checkout_qty(fieldName,2);
				   });
					$("#checkout_address").on('click', '.edit_user_address', function () {
					//$(".edit_user_address").click(function(){
						var rel = $(this).attr('rel');
						function_add_edit_user_address(rel);
					});
					$("#checkout_address").on('change', 'input[type=radio][name=address_checked]', function () {
						var rel = $(this).val();
						function_address_checked(rel);
					});
					$("#checkout_address").on('click', '.cancel_address', function () {
						var rel = $("input[type=radio][name=address_checked]:checked").val();
						function_cancel_address(rel);
					});
					$("#checkout_address").on('click', '.deliver_here_btn', function () {
						var rel = $(this).attr('data-id');
						select_ship_address(rel);
					});
					$("#delivered_address").on('click', '#change_delivered_address', function () {
						$('#delivered_address_details,#address_error').html('');
						$('#delivered_address,#checkout_order_summary,#checkout_payment_options').hide();
						$('#checkout_address,#delivered_summary,#delivered_payment').show();
					});
					$("#delivered_summary").on('click', '#change_order_summary', function () {
						$('#address_error').html('');
						$('#delivered_address,#checkout_order_summary,#delivered_payment').show();
						$('#checkout_address,#delivered_summary,#checkout_payment_options').hide();
					});
					$("#checkout_order_summary").on('click', '.cart_continue', function () {
						$('#address_error').html('');
						$('.change_order_btn').html('<div class="changeBtn"><button type="button" class="btn" id="change_order_summary">change</button></div>');
						$('#checkout_order_summary,#delivered_payment').hide();
						$('#checkout_payment_options,#delivered_summary').show();
					});
					$("#confirm_btn").click(function(){
						$("#checkoutform").submit();
					})
					$(".qtycheckoutremove").on( "click", function(e) {
						fieldName = $(this).attr('data-id');
						ajax_remove_cart_item_list(fieldName);
					});
					function save_address()
					{
						address_save();
					}
					$.ajax({
						type: "POST",
						datatype:"json",
						data: $("#checkoutform").serialize(),
						url: "<?php echo base_url();?>secure/select_ship_address_cart", 
						success: function(response){
							obj = jQuery.parseJSON(response);
							//alert(obj.get_shipping_fees);
							$('.cart_final_subtotal').html('<span class="'+obj.currency_symbol+'"></span> '+obj.cart_subtotal);
							$('.shipping_fee').html('<span class="'+obj.currency_symbol+'"></span> '+obj.get_shipping_fees);
						}
					});
				});
			}
		});		
	}
	$(".qtycheckoutremove").on( "click", function(e) {
		fieldName = $(this).attr('data-id');
		ajax_remove_cart_item_list(fieldName);
	});
	function remove_product_from_cart(fieldName){
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>cart/ajax_remove_cart_item/"+fieldName, 
			success: function(data){
				$(".mini_cart_three").load(window.location.href+" .up_mini_cart", function() {
					$(".remove_product").on( "click", function(e) {
						fieldName = $(this).attr('data-id');
						remove_product_from_cart(fieldName);
					});
				});	
			}
		});			
	}
	$(".remove_product").on( "click", function(e) {
		fieldName = $(this).attr('data-id');
		remove_product_from_cart(fieldName);
	});
	$(".qtycheckoutplus").on( "click", function(e) {
		fieldName = $(this).attr('field');
		change_checkout_qty(fieldName,1);
	});
   // This button will decrement the value till 0
   $(".qtycheckoutminus").click(function(e) {
		fieldName = $(this).attr('field');
		change_checkout_qty(fieldName,2);
   });
	$("#loginModal").on('click', '.signupLink', function () {
	   $('#SignupForm').show();
	   $('#loginForm,#fPasswordForm').hide();
	   $('#popupLeftInformation').html('<h3>Signup</h3><p>Looks like you are new here!</p><div class="loginIcon"><i class="fa fa-user-plus" aria-hidden="true"></i></div>');
	   $('#popup_signup_phone_input').focus();
	});
	$("#loginModal").on('click', '.loginLink', function () {
	   $('#loginForm').show();
	   $('#SignupForm,#fPasswordForm').hide();
	   $('#popupLeftInformation').html('<h3>Login</h3><p>Get access to your Orders</p><div class="loginIcon"><i class="fa fa-user" aria-hidden="true"></i></div>');
	   $('#popup_login_phone_input').focus();
	});
	$("#loginModal").on('click', '.fPasswordLink', function () {
	   $('#fPasswordForm').show();
	   $('#SignupForm,#loginForm').hide();
	   $('#popupLeftInformation').html('<h3>Forgot Password</h3><p>Enter your mobile number for reset password</p><div class="loginIcon"><i class="fa fa-key" aria-hidden="true"></i></div>');
	   $('#popup_fpassword_phone_input').focus();
	});
	$("#myLoginModal").on('shown.bs.modal', function () {
		$('#popup_login_phone_input').focus();
	});
	$("#loginModal").on('click', '#popup_signup_btn', function () {
		if ($('#popup_signup_phone_input').val() != '') {
			$('#popup_signup_phone_input_error').html('');
		}else{
			$('#popup_signup_phone_input_error').html('Please enter a valid phone number.');
			return false;
		}
		if($('#popup_signup_otp_input').length){
			if ($('#popup_signup_otp_input').val() != '') {
				$('#popup_signup_otp_input_error').html('');
			}else{
				$('#popup_signup_otp_input_error').html('Please enter otp.');
				return false;
			}
			if ($('#popup_signup_password_input').val() != '') {
				$('#popup_signup_password_input_error').html('');
			}else{
				$('#popup_signup_password_input_error').html('Please enter password.');
				return false;
			}
			var ep_emailval = $('#popup_signup_phone_input').val();
			var intRegex = /[0-9 -()+]+$/;

			if(intRegex.test(ep_emailval)) {
				if((ep_emailval.length < 10) || (!intRegex.test(ep_emailval)))
				{
					$('#popup_signup_phone_input_error').html('Please enter a valid phone number.');
					return false;
				}else{
					$('#popup_signup_phone_input_error').html('');
				}
				var textForm = 'phone';
			}
			$.post("<?php echo site_url('secure/ajax_otp_login');?>", { usermail: $('#popup_signup_phone_input').val(),userotp: $('#popup_signup_otp_input').val(),userpw: $('#popup_signup_password_input').val(),textForm: textForm },
			function(data){
				if(data==1){
					location.reload();
				}else{
					$('#popup_signup_otp_input_error').html('Otp not matched');
					return false;
				}
			});
		}else{
			var ep_emailval = $('#popup_signup_phone_input').val();
			var intRegex = /[0-9 -()+]+$/;

			if(intRegex.test(ep_emailval)) {
				if((ep_emailval.length < 10) || (!intRegex.test(ep_emailval)))
				{
					$('#popup_signup_phone_input_error').html('Please enter a valid phone number.');
					return false;
				}else{
					$('#popup_signup_phone_input_error').html('');
				}
				var textForm = 'phone';
			}
			$.post("<?php echo site_url('secure/chk_user');?>", { userVal: $('#popup_signup_phone_input').val(),textForm: textForm },
			function(data){
				if(data==1){
					$('#popup_signup_phone_input_error').html('You are already registered with us. Please login');
				}
				if(data==2){
					var otpPasswordField = '<div class="login_modal_section"><input type="text" id="popup_signup_otp_input" class="inputText" required=""><span class="floating-label">Enter Otp</span><div id="popup_signup_otp_input_error" class="error_mesg"></div></div><div class="login_modal_section"><input type="password" id="popup_signup_password_input" name="new_password" class="inputText" required=""><span class="floating-label">Set Password  </span><div id="popup_signup_password_input_error" class="error_mesg"></div></div>';
					$(otpPasswordField).insertAfter("#popup_signup_mobile_div");
					$('#popup_signup_otp_input').focus();
				}
			});
		}
	});
	$("#loginModal").on('click', '#popup_login_btn', function () {
		if ($('#popup_login_phone_input').val() != '') {
			$('#popup_login_phone_input_error').html('');
		}else{
			$('#popup_login_phone_input_error').html('Please enter a valid phone number.');
			return false;
		}
		if ($('#popup_login_password_input').val() == '') {
			$('#popup_login_password_input_error').html('Please enter your password.');		
			return false;			
		}else{
			$('#popup_login_password_input_error').html('');	
		}
		var ep_emailval = $('#popup_login_phone_input').val();
		var intRegex = /[0-9 -()+]+$/;

		if(intRegex.test(ep_emailval)) {
			if((ep_emailval.length < 10) || (!intRegex.test(ep_emailval)))
			{
				$('#popup_login_phone_input_error').html('Please enter a valid phone number.');
				return false;
			}else{
				$('#popup_login_phone_input_error').html('');
			}
			var textForm = 'phone';
		}
		$.post("<?php echo site_url('secure/ajax_login');?>", { usermail: $('#popup_login_phone_input').val(),userpass: $('#popup_login_password_input').val(),textForm: textForm },
		function(data){
			if(data==1){
				location.reload();
			}
			if(data==2){
				$('#popup_login_phone_input_error').html('Your username or password is incorrect.');
			}
		});
	});
	$("#fPasswordForm").on('click', '#popup_fpassword_btn', function () {
		if ($('#popup_fpassword_phone_input').val() != '') {
			$('#popup_fpassword_phone_input_error').html('');
		}else{
			$('#popup_fpassword_phone_input_error').html('Please enter a valid phone number.');
			return false;
		}
		if($('#popup_fpassword_otp_input').length){
			if ($('#popup_fpassword_otp_input').val() != '') {
				$('#popup_fpassword_otp_input_error').html('');
			}else{
				$('#popup_fpassword_otp_input_error').html('Please enter otp.');
				return false;
			}
			if ($('#popup_fpassword_password_input').val() != '') {
				$('#popup_fpassword_password_input_error').html('');
			}else{
				$('#popup_fpassword_password_input_error').html('Please enter password.');
				return false;
			}
			var ep_emailval = $('#popup_fpassword_phone_input').val();
			var intRegex = /[0-9 -()+]+$/;

			if(intRegex.test(ep_emailval)) {
				if((ep_emailval.length < 10) || (!intRegex.test(ep_emailval)))
				{
					$('#popup_fpassword_phone_input_error').html('Please enter a valid phone number.');
					return false;
				}else{
					$('#popup_fpassword_phone_input_error').html('');
				}
				var textForm = 'phone';
			}
			$.post("<?php echo site_url('secure/ajax_otp_login');?>", { usermail: $('#popup_fpassword_phone_input').val(),userotp: $('#popup_fpassword_otp_input').val(),userpw: $('#popup_fpassword_password_input').val(),textForm: textForm },
			function(data){
				if(data==1){
					location.reload();
				}else{
					$('#popup_fpassword_otp_input_error').html('Otp not matched');
					return false;
				}
			});
		}else{
			var ep_emailval = $('#popup_fpassword_phone_input').val();
			var intRegex = /[0-9 -()+]+$/;

			if(intRegex.test(ep_emailval)) {
				if((ep_emailval.length < 10) || (!intRegex.test(ep_emailval)))
				{
					$('#popup_fpassword_phone_input_error').html('Please enter a valid phone number.');
					return false;
				}else{
					$('#popup_fpassword_phone_input_error').html('');
				}
				var textForm = 'phone';
			}
			$.post("<?php echo site_url('secure/ajax_sent_otp');?>", { usermail: $('#popup_fpassword_phone_input').val(),textForm: textForm },
			function(data){
				if(data==1){
					var otpPasswordField = '<div class="login_modal_section"><input type="text" id="popup_fpassword_otp_input" class="inputText" required=""><span class="floating-label">Enter Otp</span><div id="popup_fpassword_otp_input_error" class="error_mesg"></div></div><div class="login_modal_section"><input type="password" id="popup_fpassword_password_input" name="new_password" class="inputText" required=""><span class="floating-label">Set Password  </span><div id="popup_fpassword_password_input_error" class="error_mesg"></div></div>';
					$(otpPasswordField).insertAfter("#popup_fpassword_mobile_div");
				}else{
					$('#popup_fpassword_phone_input_error').html('You are not registered with us. Please signup');
				}
			});
		}
	});
	$("#confirm_btn").click(function(){
		/*
		var strF_name = document.forms["checkoutform"]["firstname"].value;
		if (strF_name == null || strF_name == "") {
			alert("Billing address First Name must be filled out");
			document.forms["checkoutform"]["firstname"].focus();
			return false;
		}

		var strL_name = document.forms["checkoutform"]["lastname"].value;
		if (strL_name == null || strL_name == "") {
			alert("Billing address Last Name must be filled out");
			document.forms["checkoutform"]["lastname"].focus();
			return false;
		}
		var str_email = document.forms["checkoutform"]["email"].value;
		if (str_email == null || str_email == "") {
			alert("Billing address email must be filled out");
			document.forms["checkoutform"]["email"].focus();
			return false;
		}
		var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;  
		if(!str_email.match(mailformat)){  
			alert("You have entered Billing address an invalid email address!");
			document.forms["checkoutform"]["email"].focus();			
			return false;   
		}
		var str_address1 = document.forms["checkoutform"]["address1"].value;
		if (str_address1 == null || str_address1 == "") {
			alert("Billing address must be filled out");
			document.forms["checkoutform"]["address1"].focus();
			return false;
		}
		var str_city = document.forms["checkoutform"]["city"].value;
		if (str_city == null || str_city == "") {
			alert("Billing address city must be filled out");
			document.forms["checkoutform"]["city"].focus();
			return false;
		}
		var str_zip = document.forms["checkoutform"]["zip"].value;
		if (str_zip == null || str_zip == "") {
			alert("Billing address Pin Code must be filled out");
			document.forms["checkoutform"]["zip"].focus();
			return false;
		}else if(!isValidzip(str_zip)){
			alert("Billing address Pin Code must be correct");
			document.forms["checkoutform"]["zip"].focus();
			return false;
		}
		var str_phone = document.forms["checkoutform"]["phone"].value;
		if (str_phone == null || str_phone == "") {
			alert("Billing address phone must be filled out");
			document.forms["checkoutform"]["phone"].focus();
			return false;
		}else if(!isValidcontact(str_phone)){
			alert("Billing address phone must be correct mobile no");
			document.forms["checkoutform"]["phone"].focus();
			return false;
		}
		if(!document.getElementById("use_shipping").checked){
			var strF_name = document.forms["checkoutform"]["firstname1"].value;
			if (strF_name == null || strF_name == "") {
				alert("Shipping address First Name must be filled out");
				document.forms["checkoutform"]["firstname1"].focus();
				return false;
			}
			var strL_name = document.forms["checkoutform"]["lastname1"].value;
			if (strL_name == null || strL_name == "") {
				alert("Shipping address Last Name must be filled out");
				document.forms["checkoutform"]["lastname1"].focus();
				return false;
			}
			var str_email = document.forms["checkoutform"]["email1"].value;
			if (str_email == null || str_email == "") {
				alert("Shipping address email must be filled out");
				document.forms["checkoutform"]["email1"].focus();
				return false;
			}
			var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;  
			if(!str_email.match(mailformat)) {  
				alert("You have entered Shipping address an invalid email address!");
                document.forms["checkoutform"]["email1"].focus();				
				return false;   
			}
			var str_address1 = document.forms["checkoutform"]["address11"].value;
			if (str_address1 == null || str_address1 == "") {
				alert("Shipping address must be filled out");
				document.forms["checkoutform"]["address11"].focus();
				return false;
			}
			var str_city = document.forms["checkoutform"]["city1"].value;
			if (str_city == null || str_city == "") {
				alert("Shipping address city must be filled out");
				document.forms["checkoutform"]["city1"].focus();
				return false;
			}
			var str_zip = document.forms["checkoutform"]["zip1"].value;
			if (str_zip == null || str_zip == "") {
				alert("Shipping address Pin Code must be filled out");
				document.forms["checkoutform"]["zip1"].focus();
				return false;
			}else if(!isValidzip(str_zip)){
				alert("Shipping address Pin Code must be correct");
				document.forms["checkoutform"]["zip1"].focus();
				return false;
			}
			var str_phone = document.forms["checkoutform"]["phone1"].value;
			if (str_phone == null || str_phone == "") {				
				alert("Shipping address phone must be filled out");
				document.forms["checkoutform"]["phone1"].focus();
				return false;
			}else if(!isValidcontact(str_phone)){
				alert("Shipping address phone must be correct mobile no");
				document.forms["checkoutform"]["phone1"].focus();
				return false;
			}
		}
        var use_shipping = $('input[name="use_shipping"]');
		if(use_shipping.prop("checked") == true){
			var pin = $('input[name="zip"]').val();
		}else{
			var pin = $('#ship_zip').val();
		}		
		if(pin !='' && pin !=null){
			var total_weight = $("#weight").attr('value');
			$.ajax({
				type     : "POST",
				data     : {'pin':pin,'total_weight':total_weight},
				url      : "<?=base_url('cart/checkpin')?>",
				success  : function(response){
					$('.ship_amount').text(response);
					$('input[name="shippingtr"]').val(response);
					var sub_tot = parseInt($('.sub_total').attr('value'));
					var grand_total = sub_tot + parseInt(response);
					$('.grand_total').text(grand_total);
					$('#order_grant_total').val(grand_total);
					$('input[name="subtotalamt"]').val(grand_total);
					}
			});
		}
		if (!$('input[name=shipping_method]:checked').length) {
           // do something here
			alert("Please select shipping method");
			document.forms["checkoutform"]["shipping_method"].focus();
			return false;
        }

		if(!document.getElementById("term").checked){
			alert("Please accept all term and conditions");
			document.forms["checkoutform"]["term"].focus();
			return false;
		}
		*/
		$("#checkoutform").submit();
	})
});
window.onload = function(){
	$('.product').equalHeights();
}
</script>
<script>
$(document).ready(function(){	
	$(".quick_view").click(function(){
		$.ajax({
			type: "POST",
			url: '<?php echo site_url('secure/product_quick_view'); ?>/'+$(this).attr('rel'),
			//data: {id:id, status:status},
			beforeSend:function(){},
			success: function(msg){
				//alert(msg);
				$('#quick_product_details').html(msg);
	            $('#modal_box').modal('show');
			}
		});
	});		
	$(".edit_address").click(function(){
		$.ajax({
			type: "POST",
			url: '<?php echo site_url('secure/address_form'); ?>/'+$(this).attr('rel'),
			//data: {id:id, status:status},
			beforeSend:function(){},
			success: function(msg){
				//alert(msg);
				$('#address-form-container').html(msg);
	            $('#myModal').modal('show');
				$('.selectpicker').selectpicker('refresh');
			}
		});	
	});	
	$('input[type="checkbox"][id="use_shipping"]').on('change', function() {
		if($(this).is(":checked")) {
			$('#address-form-container-billing').show();
			$('#f_firstname').focus();			
		}else{
			$('#address-form-container-billing').hide();
		}
	});
	$(document).on('click', '.edit_user_address', function () {
	//$(".edit_user_address").click(function(){
		var rel = $(this).attr('rel');
		function_add_edit_user_address(rel);
	});
	$("#checkout_address").on('change', 'input[type=radio][name=address_checked]', function () {
		var rel = $(this).val();
		function_address_checked(rel);
	});
	$("#checkout_address").on('click', '.cancel_address', function () {
		var rel = $("input[type=radio][name=address_checked]:checked").val();
		function_cancel_address(rel);
	});
	$("#checkout_address").on('click', '.deliver_here_btn', function () {
		var rel = $(this).attr('data-id');
		select_ship_address(rel);
	});
	$("#delivered_address").on('click', '#change_delivered_address', function () {
		$('#delivered_address_details,#address_error').html('');
		$('#delivered_address,#checkout_order_summary,#checkout_payment_options,#checkout_billing_address').hide();
		$('#checkout_address,#delivered_summary,#delivered_payment,#billing_address').show();
	});
	$("#delivered_summary").on('click', '#change_order_summary', function () {
		$('#address_error').html('');
		$('#delivered_address,#checkout_order_summary,#delivered_payment,#billing_address').show();
		$('#checkout_address,#delivered_summary,#checkout_payment_options,#checkout_billing_address').hide();
	});
	$("#checkout_order_summary").on('click', '.cart_continue', function () {
		$('#address_error').html('');
		$('.change_order_btn').html('<div class="changeBtn"><button type="button" class="btn" id="change_order_summary">change</button></div>');
		$('#checkout_order_summary,#delivered_payment').hide();
		$('#checkout_payment_options,#delivered_summary').show();
	});
	$("#checkout_billing_address").on('click', '.delivery_continue', function () {
		//alert();
		if($('#use_shipping').is(":checked")) {
			if ($('#f_firstname').val() == '') {
				$('#delivery-form-error').html('Name is required');
				return false;
			}else if ($('#f_phone').val() == '') {
				$('#delivery-form-error').html('10-digit Mobile Number is required');
				return false;
			}else if ($('#f_zip').val() == '') {
				$('#delivery-form-error').html('Pin code is required');
				return false;
			}else if ($('#f_address').val() == '') {
				$('#delivery-form-error').html('Address is required');
				return false;
			}else if ($('#country_id').val() == '') {
				$('#delivery-form-error').html('Country is required');
				return false;
			}else if ($('#zone_id').val() == '') {
				$('#delivery-form-error').html('State is required');
				return false;
			}else if ($('#f_city').val() == '') {
				$('#delivery-form-error').html('City is required');
				return false;
			}else{
				$('#delivery-form-error').html('');
			}
			$('#delivered_billing_details').html('<b>'+$('#f_firstname').val()+'</b> '+$('#f_address').val()+', '+$('#f_landmark').val()+', '+$('#f_city').val()+', '+$("#country_id option:selected").html()+', '+$("#zone_id option:selected").html()+' - <b>'+$('#f_zip').val()+'</b>');
		}else{
			var rel = $("input[type=radio][name=address_checked]:checked").val();
			$.ajax({
				type: "POST",
				url: '<?php echo site_url('secure/select_ship_address'); ?>/'+rel,
				//data: {id:id, status:status},
				beforeSend:function(){},
				success: function(response){
					obj = jQuery.parseJSON(response);
					//alert(obj.get_available_pin);
					if(obj.id!=''){
						$('#delivered_billing_details').html('<b>'+obj.firstname+'</b> '+obj.address1+', '+obj.landmark+', '+obj.city+', '+obj.country+', '+obj.zone_name+' - <b>'+obj.zip+'</b>');
					}
				}
			});
		}
		$('.change_billing_btn').html('<div class="changeBtn"><button type="button" class="btn" id="change_billing_summary">change</button></div>');
		$('#checkout_billing_address,#delivered_summary').hide();
		$('#billing_address,#checkout_order_summary').show();
	});
	$("#billing_address").on('click', '#change_billing_summary', function () {
		$('#delivered_address,#delivered_summary,#delivered_payment,#checkout_billing_address').show();
		$('#checkout_address,#checkout_order_summary,#checkout_payment_options,#billing_address').hide();
		$('#delivered_billing_details').html('');
	});
	function function_add_edit_user_address(rel){
		$('.address-form-container,.address_edit,.deliver_here,#address_error').html('');
		$.ajax({
			type: "POST",
			url: '<?php echo site_url('secure/address_user_form'); ?>/'+rel,
			//data: {id:id, status:status},
			beforeSend:function(){},
			success: function(msg){
				//alert(msg);
				$('#address-form-container'+rel).html(msg);
				$('.selectpicker').selectpicker('refresh');
				$('#f_firstname').focus();
			}
		});
	}
	function function_address_checked(rel){
		$('.address_edit,.deliver_here,.address-form-container,#address_error').html('');
		$('#address_edit'+rel).html('<div class="edit_user_address" rel="'+rel+'">edit</div>');
		$('#deliver_here'+rel).html('<button type="button" class="btn deliver_here_btn" data-id="'+rel+'">deliver here</button>');
	}
	function function_cancel_address(rel){
		$('.address-form-container,#address_error').html('');
		$('#address_edit'+rel).html('<div class="edit_user_address" rel="'+rel+'">edit</div>');
		$('#deliver_here'+rel).html('<button type="button" class="btn deliver_here_btn" data-id="'+rel+'">deliver here</button>');
	}
	function select_ship_address(rel){
		$.ajax({
			type: "POST",
			url: '<?php echo site_url('secure/select_ship_address'); ?>/'+rel,
			//data: {id:id, status:status},
			beforeSend:function(){},
			success: function(response){
				obj = jQuery.parseJSON(response);
				//alert(response);
				if(obj.id!=''){
					$('#delivered_address_details').html('<b>'+obj.firstname+'</b>'+obj.address1+', '+obj.landmark+', '+obj.city+', '+obj.country+', '+obj.zone_name+' - <b>'+obj.zip+'</b>');
					$('#checkout_address,#billing_address').hide();
					$('#delivered_address,#checkout_billing_address').show();
					$('#address_error').html('');
					$('.cart_final_subtotal').html('<span class="'+obj.currency_symbol+'"></span> '+obj.cart_subtotal);
					$('.shipping_fee').html('<span class="'+obj.currency_symbol+'"></span> '+obj.get_shipping_fees);
				}else{
					$('#address_error').html(obj.error);
				}
			}
		});
	}
	$('#search_form_submit').click(function(){	 
	    $('#search_frm').submit();
	});
	
	$('.open_register_model').click(function(){
		$('#authentication').modal('hide');
	});
	$('.open_login_model').click(function(){
		$('#signup').modal('hide');
	});	
	$("#login_form").validate({
		rules:{
			email_id:"required",
			password:"required"
		},
        submitHandler: function (form){
     		$.ajax({
                type: "POST",
                datatype:"json",
                data: $("#login_form").serialize(),
                url: "<?php echo base_url(); ?>secure/login", 
                success: function(msg){
                	if(msg.status == false){
                		$("#alert_msg").html(msg.msg);
                	}else if(msg.status == true){
						//alert("hello");
                		window.location='<?php echo base_url(); ?>checkout';
                	}
                }
            });
        }
    });
	$("#reg_form").validate({
		rules:{
			reg_email:{
				required: true,
				email: true,
				remote: {
					type: 'post',
					url: '<?php echo base_url()."secure/checkemail"; ?>',
					data: {
						user_email: function() {
							return $( "#reg_email" ).val();
						}
					},
				}
			},
			reg_phone: {
				required: true,
				number: true,
				remote: {
					type: 'post',
					url: '<?php echo base_url()."secure/checkphone"; ?>',
					data: {
						user_phone: function() {
							return $( "#reg_phone" ).val();
						}
					},
				}
			},
			reg_password:
			{
				required:true,
				minlength:6,
			}
		},
		messages:
		{
			 reg_email:
			 {
				required: "Please enter your email address.",
				email: "Please enter a valid email address.",
				remote: jQuery.validator.format("{0} is already taken.")
			 },
			 reg_phone:
			 {
				required: "Please enter your phone number.",
				number: "Please enter a valid phone number.",
				remote: jQuery.validator.format("{0} is already taken.")
			 },
		},
        submitHandler: function (form) 
        {
			form.submit();
        }
    });	
	$("#newsletter_form").validate({
        rules: {
            exampleInputEmail1  : "required"
        },
        submitHandler: function(form) {
            var email = $('#exampleInputEmail1').val();
            $.ajax({
                url: '<?php echo base_url(); ?>secure/newsletterEmail',
                type: 'post',
                dataType: 'json',
                data: {email:email},
                success: function(data) {
                   
                       if(data==1)
                       {
                            $("#alt_msg").html('<p style="color:green;font-weight:300;padding:5px;">You have successfuly subscribed.</p>');
                       }
                       else
                       {
                            $("#alt_msg").html('<p style="color:red;font-weight:300;padding:5px;">You have already subscribed.</p>');
                       }                  
                }
            });
            
        }
    });
 
});

$('#rating-input').on('rating.change', function(event, value, caption) {
	$('#rating_val').val(value);
});
	
	//add review form
	function post_review_form(){
		product_id = $("#prdid").val();
		pop_review = $("#pop_review").val();
		pop_review_title = $("#review_title").val();
		rating_val = $("#rating_val").val();
		
		if(pop_review_title.length < 10){
			$("#pop_err_msg").html('<div class="alert alert-danger">Please make sure your title contains at least 10 characters.</div>');
			return false;
		}
		
		if(pop_review.length < 20){
			$("#pop_err_msg").html('<div class="alert alert-danger">Please make sure your review contains at least 20 characters.</div>');
			return false;
		}
		$.ajax({
			url: '<?php echo base_url(); ?>ratenreview_controller/add_review',
			type: 'post',
			data: {'pop_product_id':product_id, 'review':pop_review, 'title' : pop_review_title,'rating':rating_val},
			success: function(data) {
				if(data == 'logout'){
					url = "<?php echo base_url(); ?>secure/login";
					$( location ).attr("href", url);
				}else if(data == 'no'){
					$.blockUI({ 
						css: { 
							border: 'none', 
							padding: '15px',
							message: data.message, 				
							backgroundColor: '#000', 
							'-webkit-border-radius': '10px', 
							'-moz-border-radius': '10px', 
							opacity: .5, 
							color: '#fff' 
						},
						message:'<strong style="font-size:18px;">Your already review this product.</strong>'
					}); 

					setTimeout($.unblockUI, 2000); 
				}else if(data == 'yes'){
					$.blockUI({ 
						css: { 
							border: 'none', 
							padding: '15px',
							message: data.message, 				
							backgroundColor: '#000', 
							'-webkit-border-radius': '10px', 
							'-moz-border-radius': '10px', 
							opacity: .5, 
							color: '#fff' 
						},
						message:'<strong style="font-size:18px;">Your review add for this product successfully.</strong>'
					}); 

					setTimeout($.unblockUI, 2000); 
					location.reload();
				}	
			}
		});
	}


	function addToCart(id){
        var cart = $('#add-to-cart_'+id);
        $.post(cart.attr('action'), cart.serialize(), function(data){
			// return false;
			if(data.message != undefined){
				$.blockUI({ 
					css: { 
						border: 'none', 
						padding: '15px',
						message: data.message, 				
						backgroundColor: '#000', 
						'-webkit-border-radius': '10px', 
						'-moz-border-radius': '10px', 
						opacity: .5, 
						color: '#fff' 
					},
					message:'<strong style="font-size:18px;">'+data.message+'</strong>'
				}); 
				setTimeout($.unblockUI, 2000); 
			}else if(data.error != undefined){
				$.blockUI({ 
					css: { 
						border: 'none', 
						padding: '15px',
						message: data.message, 				
						backgroundColor: '#000', 
						'-webkit-border-radius': '10px', 
						'-moz-border-radius': '10px', 
						opacity: .5, 
						color: '#fff' 
					},
					message:'<strong style="font-size:18px;">'+data.error+'</strong>'
				}); 
				setTimeout($.unblockUI, 2000);
			}			
        }, 'json');
		
		prval = 0;
		if(parseInt($(".current_cart_value").text())){
			prval = parseInt($(".current_cart_value").text());
		}
		quantval = parseInt($("#add-to-cart_"+id+" input[name=quantity]").val());
		
		if(isNaN(quantval)){
			//return nan in case of grouped product list in product details page
			quantval = parseInt($("#nan_"+id).val());
		}
		
		if(quantval > 0){
			newtiems = prval+quantval;
			$(".current_cart_value").html(newtiems + " Items");
		}
		setTimeout(function(){ location.reload(); }, 2000);
		
    }
	
	
	//add to wishlist functions
	function add_to_wishlist(product_id = 0){
		$.ajax({
			url: '<?php echo base_url(); ?>ratenreview_controller/add_wishlist',
			type: 'post',
			data: {'product_id':product_id},
			success: function(data) {
				if(data == 'logout'){
					url = "<?php echo base_url(); ?>secure/login";
					$( location ).attr("href", url);
				}else{
					preval = $("#header_wishlist").text();
					msg = '';
					if(data == 'no'){
						msg = 'Your product has been already added.';
						$.blockUI({ 
							css: { 
								border: 'none', 
								padding: '15px',
								message: data.message, 				
								backgroundColor: '#000', 
								'-webkit-border-radius': '10px', 
								'-moz-border-radius': '10px', 
								opacity: .5, 
								color: '#fff' 
							},
							message:'<strong style="font-size:18px;">'+msg+'</strong>'
						}); 
						setTimeout($.unblockUI, 2000);
					}else{
						msg = 'You have addedd successfully product in Wishlist.';
						$.blockUI({ 
							css: { 
								border: 'none', 
								padding: '15px',
								message: data.message, 				
								backgroundColor: '#000', 
								'-webkit-border-radius': '10px', 
								'-moz-border-radius': '10px', 
								opacity: .5, 
								color: '#fff' 
							},
							message:'<strong style="font-size:18px;">'+msg+'</strong>'
						}); 
						setTimeout($.unblockUI, 2000);
					}
				}	
			}
		});
	}

	//add to compare functions
	function add_to_compare(product_id = 0){
		$.ajax({
			url: '<?php echo base_url(); ?>ratenreview_controller/add_compare',
			type: 'post',
			data: {'product_id':product_id},
			success: function(data) {
				//if(data == 'logout'){
				if(false){	
					url = "<?php echo base_url(); ?>secure/login";
					$( location ).attr("href", url);
				}else{
					preval = $("#header_compare").text();
					msg = '';
					if(data == "full"){
						msg = 'You can add maximum 4 product in Compare List';
					}if(data=='found'){
						msg = 'Your product has been already added to Compare List';
					}else if(data=='add'){
						msg = 'You have addedd successfully product in Compare List.';
					}else{
						// no query
					}
					$.blockUI({ 
						css: { 
							border: 'none', 
							padding: '15px',
							message: data.message, 				
							backgroundColor: '#000', 
							'-webkit-border-radius': '10px', 
							'-moz-border-radius': '10px', 
							opacity: .5, 
							color: '#fff' 
						},
						message:'<strong style="font-size:18px;">'+msg+'</strong>'
					}); 
					setTimeout($.unblockUI, 2000);
				}	
			}
		});
	}
	
	function checkphone()
    {
	    var phone =document.getElementById("reg_phone").value;
	    var pattern = /^\d{10}$/;
	    if (pattern.test(phone)) 
	    {
	        $.ajax({
			    type: 'post',
			    url: '<?php echo base_url()."secure/checkphone"; ?>',
			    data: {user_phone:phone},
			    success: function (response) {

			        if(response=="OK"){
	               		$('#reg_phone').css('border','3px solid green');
	                    return true;	
	                }else{
	                	$('#reg_phone').css('border','3px solid red');
	                    return false;	
	                }
             	}
		   });
	    }else{
		   $('#reg_phone').css('border','3px solid red');
		   return false;
	    }
	}

    function checkemail()
    {
	    var email =document.getElementById("reg_email").value;
	    var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    if(pattern.test(email)) 
	    {
	        $.ajax({
			    type: 'post',
			    url: '<?php echo base_url()."secure/checkemail"; ?>',
			    data: {user_email:email},
			    success: function (response) {

			        if(response=="OK")	
	                {
	               		$('#reg_email').css('border','3px solid green');
	                    return true;	
	                }
	                else
	                {
	                	$('#reg_email').css('border','3px solid red');
	                    return false;	
	                }
             	}
		   });
	    }
	    else
	    {
		   $('#reg_email').css('border','3px solid red');
		   return false;
	    }
	}
	
	function checkpassword()
	{
		var password =document.getElementById("reg_password").value;
	    if(password.length > 5) 
	    {
	        $('#reg_password').css('border','3px solid green');
	        return true;              
	    }
	    else
	    {
		   $('#reg_password').css('border','3px solid red');
		   return false;
	    }
	}
	
	function addToCartProduct(id){
		var quantity=$('#quantity'+id).val();
		$.ajax({
			type: "POST",  					
			url: "<?php echo base_url()."cart/add_to_cart_ajax";?>",
			data: {id:id, quantity:quantity},
			beforeSend:function() {},
			dataType: 'json',	
			success: function(data){
				$.blockUI({ 
					css: { 
						border: 'none', 
						padding: '15px',
						message: data.message, 				
						backgroundColor: '#000', 
						'-webkit-border-radius': '10px', 
						'-moz-border-radius': '10px', 
						opacity: .5, 
						color: '#fff' 
					},
					message:'<strong style="font-size:18px;">Product add to cart successfull.</strong>'
				}); 

				setTimeout($.unblockUI, 2000); 
				location.reload();
			}
		});		
	}


</script>

<script>
   $("#ask_question").validate({
   	rules:{
   		nickname	: "required",
   		email		: "required",
   		question	: "required"
   	},
   	submitHandler: function (form){
   		$.ajax({
   			type: "POST",
   			datatype:"json",
   			data: $("#ask_question").serialize(),
   			url: "<?php echo base_url();?>cart/ask_question", 
   			success: function(msg){
   				alert("Thank you for your question"); 
   				setTimeout(function(){ 
   					window.location.reload();
   				}, 2000);
   			}
   		});
   	}
   });
   jQuery(document).ready(function(){
   	$('#save_question_off').click(function(){
   		alert("Please login before ask question");
   	});
   	$("#img1").elevateZoom({ gallery: 'gallery_01', cursor: 'pointer', galleryActiveClass: "active" });
   	$("#img1").bind("click", function(e) {
   		var ez = $('#img1').data('elevateZoom');
   		ez.closeAll();
   //$.fancybox(ez.getGalleryList());
   return false;
   });
   // This button will increment the value
  // $('.qtyplus').click(function(e){
	function change_qty(fieldName,result) {
		// Stop acting like a button
		//e.preventDefault();
		// Get the field name
		fieldName = fieldName;
		// Get its current value
		var currentVal = parseInt($('input[id='+fieldName+']').val());
		// If is not undefined
		if (!isNaN(currentVal)) {
		// Increment
		if(result==1){
			$('input[id='+fieldName+']').val(currentVal + 1);
		}else if(result==2){
			var currentVal = parseInt($('input[id='+fieldName+']').val());
			// If it isn't undefined or its greater than 0
			if (!isNaN(currentVal) && currentVal > 1) {
			// Decrement one
			$('input[id='+fieldName+']').val(currentVal - 1);
			} else {
			// Otherwise put a 0 there
			$('input[id='+fieldName+']').val(1);
			}
		}
		$.ajax({
			type: "POST",
			datatype:"json",
			data: $("#update_cart_form").serialize(),
			url: "<?php echo base_url();?>cart/update_cart", 
			success: function(msg){
				$("#update_cart_form").load(window.location.href+" .list", function() {
					$( ".qtyplus" ).on( "click", function(e) {
						fieldName = $(this).attr('field');
						change_qty(fieldName,1);
					});
					$( ".qtyminus" ).on( "click", function(e) {
						fieldName = $(this).attr('field');
						change_qty(fieldName,2);
					});
				});
			}
		});
		} else {
			// Otherwise put a 0 there
			$('input[id='+fieldName+']').val(0);
		}
	}
	$(".qtyplus").on( "click", function(e) {
		fieldName = $(this).attr('field');
		change_qty(fieldName,1);
	});
   // This button will decrement the value till 0
   $(".qtyminus").click(function(e) {
		fieldName = $(this).attr('field');
		change_qty(fieldName,2);
   });
   
   $('.minus').click(function () {
				var $input = $(this).parent().find('input');
				var count = parseInt($input.val()) - 1;
				count = count < 1 ? 1 : count;
				$input.val(count);
				$input.change();
				return false;
			});
			$('.plus').click(function () {
				var $input = $(this).parent().find('input');
				$input.val(parseInt($input.val()) + 1);
				$input.change();
				return false;
			});
   
   })
   if($(window).width() > 768){
   // Hide all but first tab content on larger viewports
   $('.accordion__content:not(:first)').hide();
   // Activate first tab
   $('.accordion__title:first-child').addClass('active');
   } else {
   // Hide all content items on narrow viewports
   $('.accordion__content').hide();
   };
   // Wrap a div around content to create a scrolling container which we're going to use on narrow viewports
   //$( ".accordion__content" ).wrapInner( "<div class='overflow-scrolling'></div>" );
   // The clicking action
   $('.accordion__title').on('click', function() {
   $('.accordion__content').hide();
   $(this).next().show().prev().addClass('active').siblings().removeClass('active');
   });
   function checkzipcode(){
   var zip = $.trim($("#pincode").val());
   var zipRegex = /^\d{6}$/;
   var msg = '';
   if (!zipRegex.test(zip)){
   	$("#chckpinmsg-green").html('');
   	$("#chckpinmsg-red").html("Please enter valid zip code.");
   }else{
   	$.ajax({
   		url: '<?php echo base_url(); ?>cart/checkzip',
   		type: 'post',
   		data: {'zipcode':zip},
   		dataType:"json",
   		success: function(data) {
   			if(data != '0'){
   				var zip_details = eval(data);
   //{"id":"43","postcode":"110043","isship":"1","iscod":"1","daystodeliver":"2 to 4"}
   if(zip_details['isship'] == '1'){
   $("#addcartbut").prop("disabled", false);
   if(zip_details['iscod'] == '1'){
   	msg = 'Yes, Cash on Delivery available in your area and We can deliver product in '+zip_details['iscod']+' Days';
   	$("#chckpinmsg-green").html(msg);
   	$("#chckpinmsg-red").html('')
   }else{
   	msg = 'Sorry, Cash on Delivery not available in your area and We can deliver product in '+zip_details['iscod']+' Days';
   	$("#chckpinmsg-green").html('');
   	$("#chckpinmsg-red").html(msg);
   }
   }else{
   $("#addcartbut").prop("disabled", true);
   $("#chckpinmsg-green").html('');
   $("#chckpinmsg-red").html("Sorry, We could not provide shipping service in your area.");
   }
   }else{
   $("#addcartbut").prop("disabled", true);
   $("#chckpinmsg-green").html('');
   $("#chckpinmsg-red").html("Sorry, We could not provide shipping service in your area.");
   }
   }
   });
   }			
   }
</script>
<script>
$(document).ready(function(){
$('#rating-input').rating({
update: $('#user_rating').val(),
min: 1,
max: 5,
step: 1,
size: 'x',
showClear: false,
starCaptions: {
1:'Disappointing',
2:'Bad',
3:'Average',
4:'Good',
5:'Perfect'
}
});
});
</script>

<script>
	jQuery('#newslatter_btn').click(function() {
        var custom_name = jQuery('#custom_name').val();
        var custom_email = jQuery('#custom_email').val();
       
        if (custom_name == '') {
            jQuery('#name-error').show();
            jQuery('#custom_name').addClass('name-error-input');
            return false;
        }else{
        	jQuery('#name-error').hide();
            jQuery('#custom_name').removeClass('name-error-input');
        }

        if (custom_email == '') {
            jQuery('#email-error').show();
            jQuery('#email-error').html('This is a required field.');
            jQuery('#custom_email').addClass('email-error-input');
            return false;
        }

        if (!validateEmail(custom_email)) {
            jQuery('#email-error').show();
            jQuery('#custom_email').addClass('email-error-input');
            jQuery('#email-error').html('Email is invalid.');
            return false;
        }else{
        	jQuery('#email-error').hide();
            jQuery('#custom_email').removeClass('email-error-input');
            jQuery('#email-error').html('This is a required field.');
        }

        jQuery.ajax({
            type: "POST",
            url: "/secure/newsletterEmail/",
            async: true,
            data: {
                email: custom_email, name:custom_name
            },
            beforeSend: function() {
                jQuery('.ajax_loader').show();
            },
            success: function(data) {
            	console.log('data', data);
            	jQuery('.ajax_loader').hide();
            	if(data==1)
	               {
	                    $(".success_newsletter").html('<p style="color:green;font-weight:300;padding:5px;">You have sucessfully subscribed.</p>');
	               }
	               else
	               {
	                    $(".success_newsletter").html('<p style="color:red;font-weight:300;padding:5px;">You have already subscribed.</p>');
	               }
            },
            error: function(xhr) {
                alert("Error occured.please try again");
            }
        });


    });

    function validateEmail(email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if (!emailReg.test(email)) {
            return false;
        } else {
            return true;
        }
    }


</script>
<style>
.newsletter_field .msg-error {
	position: absolute;
    top: 20px;
    right: 30px;
    text-transform: uppercase;
    letter-spacing: 1.8px;
    font-size: 11px;
    margin: 0;
    color: #e02b27;
}
   .success_newsletter{color: green;;}
	input.name-error-input, input.email-error-input {
	    border: 1px solid #ce4924 !important;
	}
</style>
<!--<script type="text/javascript">
$(document).ready(function(){
              
            $("#reg_form").validate({
                rules: {
                    firstname: "required",
                    lastname: 'required',
                    email: {
                    		required: true,
                    		email: true
                    },
                    phone:{
                                required: true,
                                number: true,
                                minlength:10
                    },
                   	password: {
                   		required:true,
                   		minlength:6
                   	},
					confirm: {
					      equalTo: "#password"
					}
                },
                messages: {
                    firstname: "Please enter firstname.",
                   	lastname: "Please enter lastname.",
                   	email:{
                    		required: "Please enter email.",
                    		email: "Please enter valid email id."
                    },
                    phone:{
                            required: "Please enter mobile number.",
                            number: "only numbers allowed."
                    }
                },
                submitHandler: function (form) {
                	
                	var redirect = $("#redirect").val();
        
                	if(redirect == '')
                	{

                		$('#myModal').modal({backdrop: 'static', keyboard: false});
                		getOtp();
                	}
                	else
                	{
                		form.submit();
                	}
             
                }

            });

});

		function check_otp()
		{
			var hidden_otp = $("#hidden_otp").val();
			var enter_otp = $("#enter_otp").val();

			
				if(hidden_otp == enter_otp)
				{
					document.getElementById("modal_message").innerHTML = "<font color='green'>Thank you for verified the mobile number and please wait until page redirect.</font>";

					
					setTimeout(function(){
						
						$('#myModal').modal('hide');
						
						$("#subBtn").click();
					}, 3000);		
				}
				else
				{
					document.getElementById("modal_message").innerHTML = "<font color='#FF0000'>Incorrect Code</font>";
				}

		}

        function getOtp()
        {
        	// var mobile = document.getElementById("bill_phone").value;
	        // var pattern = /^\d{10}$/;
	        // if (pattern.test(mobile)) 
	        // {
           		var phone = $("#phone").val();
	            $.ajax({
	                type: "POST",
	                datatype:"json",
	                data: {phone:phone},
	                url: "<?php echo base_url(); ?>secure/generateOtp", 
	                success: function(otp) 
	                {
	                	if(otp !="not_ok")
	                	{
	                		$("#new_mob").html(phone);
	                		$("#hidden_otp").val(otp);
	                		$("#redirect").val(1);
	                	}
	                }
	            });
	          
	        // }
	        // alert("It is not valid mobile number.input 10 digits number!");
	        // return false;
  
        }
</script>-->
</body>
</html>
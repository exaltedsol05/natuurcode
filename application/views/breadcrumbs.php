<?php 
if(!empty($breadcrumbs)):?>
<li><a href="<?php echo site_url();?>"><i class="fa fa-home"></i></a></li>
    <?php for($i = 0; $i<count($breadcrumbs); $i++):?>
        <?php if($i != count($breadcrumbs)-1):?>
            <li><a href="<?php echo $breadcrumbs[$i]['link'];?>"><?php echo $breadcrumbs[$i]['name'];?></a></li>
        <?php else:?>
            <li><a href=""><?php echo $breadcrumbs[$i]['name'];?></a></li>
        <?php endif;?>
    <?php endfor;?>
<?php endif;
<?php $this->load->view('vwHeader');?>
<style>#showMenu{display:none;} .container.pages a{color: #007bff;}
section.container.pages h1 {font-size: 26px!important;margin: 25px 0 40px 0px;}
section.container.pages h2 {font-size: 22px!important;font-weight: 600;}</style>
<section class="container pages">
<div class="row">
	<div class="col-sm-12">
		<div class="page-header 1">
			<h1 style="font-size:28px;margin-top:5px;"><?php echo $page_title;?></h1>
		</div>
	</div>
	<div class="col-sm-12">
		<?php echo  $page->content; ?>
    </div>
</div>
</section>
<?php $this->load->view('vwFooter');?>

<?php 

if($page->slug == 'contact-us')
{
?>
<style type="text/css">

</style>
<script>
$(document).ready(function(){
	$("#contact_form").validate({
		rules:{
			exampleInputName:"required",
			exampleInputEmail:"required",
			exampleInputComments:"required"
		},
        submitHandler: function (form){
     		$.ajax({
                type: "POST",
                datatype:"json",
                data: $("#contact_form").serialize(),
                url: "<?php echo base_url(); ?>cart/contact_us_request", 
				beforeSend: function() { 
				      $("#alertt_msg").html('<span style="color:green;">sending....</span>');
				      $("#request").prop('disabled', true); // disable button
				},
                success: function(msg){
                	if(msg){
						$("#alertt_msg").html('<span style="color:green;">request sent sucessfully.</span>');
						$("#request").prop('disabled', false); // enable button

						$('#exampleInputName').val('');
						$('#exampleInputEmail').val('');
						$('#exampleInputComments').val('');
                	}
                }
            });
        }
    });
});
</script>
<?php
}

?>
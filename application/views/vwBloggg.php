<?php
$this->load->view('vwHeader'); ?>
<div class="breadcrumb">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="col-sm-4" style="font-size:36px;">
					<span>NATUUR</span> | <strong>Blog</strong>
				</div>
				<div class="col-sm-8">
					<ul class="list-inline list-unstyled">
						<li><a href="#">Natuur Knowledge</a></li>
						<li><a href="#">Daily Wisdom</a></li>
						<li><a href="#">DIY Workshop</a></li>
						<li><a href="#">Chemicals in Daily Life</a></li>
						<li><a href="#">Health & Wellness</a></li>
						<li><a href="#">Beauty</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="<?php echo base_url();?>">Home</a></li>
				<li><a href="#"><?php echo $page_title;?></a></li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->
<section class="container">
<div class="row">
	<div class="col-sm-12">
		<div style="background-color: #fff;box-shadow: 0 2px 4px 0 rgba(0,0,0,.08);padding: 20px;overflow: hidden;">
			<h1 class="text-center">Blog</h1><hr>
			<?php foreach ($arrBlog as $key => $value) { ?>
				<div class="col-md-4">
					<div class="aa-latest-blog-single">
					  <figure class="aa-blog-img">                    
						<a href="#"><img src="<?php echo base_url().'uploads/blog/small/'.$value->blog_image; ?>" alt="img"></a>
					  </figure>
					  <div class="aa-blog-info">
						<h3 class="aa-blog-title"><a href="#"><?php echo ucfirst(substr($value->title,0,15)); ?></a></h3>
						<p><?php //echo $value->content; ?></p></br>
						
						<a href="<?php echo base_url().'cms/cms_blog_read/'.$value->id; ?>" class="aa-read-mor-btn">Read more <span class="fa fa-long-arrow-right"></span></a>
						
					  </div>
					</div>
				</div>
			<?php } ?>
		</div>
    </div>
</div>
</section>
<?php
$this->load->view('vwFooter');
?>
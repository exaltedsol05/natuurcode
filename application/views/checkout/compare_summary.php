<div class="table-responsive" style="min-height:200px;"><?php 
	if(count($product_records)>0){?>
		<table class="table compare-table inner-top-vs">
			<tr>
				<th>Products</th><?php 
				$i=1; 
				foreach($product_records as $product) {
					if($i<=6) {  
						$images = (array)json_decode($product->images);?>
						<td>
							<div class="product">
								<div class="product-image" style="text-align:center;">
									<div class="image"><?php 
										foreach($images as $photo_id=>$photo_obj){
											if(!empty($photo_obj)){
												$photo = (array)$photo_obj;
											}
										} ?>
										<a href="<?php echo site_url(implode('/', $base_url).'/'.$product->slug); ?>">
											<img src="<?php echo base_url().'uploads/images/small/'.$photo['filename'];?>" alt="product name">
										</a> 
									</div>

									<div class="product-info text-left">
										<h3 class="name" style="">
											<a href="<?php echo site_url(implode('/', $base_url).'/'.$product->slug); ?>" style="text-align:center;"><strong><?php echo $product->name ?></strong></a>
										</h3>
										<div class="action" style="">
											<a href="javascript:void(0);" onclick="addToCartProduct(<?php echo $product->id;?>);" class="lnk btn btn-primary" href="javascript:void(0);" style="color:#fff;">Add To Cart</a>
										</div>

									</div>
								</div>
							</div>
						</td><?php   
					} 
					$i++;  
				}?> 
			</tr>
			<tr>
				<th>Price</th><?php 
				$i=1; 
				foreach($product_records as $product) {
					if($i<=6) { 
						$discount=round(100*($product->price-$product->saleprice)/$product->price); ?>
						<td>
							<div class="product-price">
								<?php if($discount > 0):?>
									<span class="price">
										₹ <?php echo $product->saleprice;?>	</span>
									<span class="price-before-discount">₹ <?php echo $product->price;?></span>
								<?php else: ?>
								  <span class="price">₹ <?php echo $product->price;?></span>
								<?php endif;?>
							</div>
						</td><?php   
					} 
					$i++;  
				}?> 
			</tr>

			<tr>
				<th>Description</th><?php 
				$i=1; 
				foreach($product_records as $product) {
					if($i<=6){ 
						$discount=round(100*($product->price-$product->saleprice)/$product->price);?>
						<td>
							<p class="text"><?php echo $product->description;?><p>
						</td><?php   
					}
					$i++;  
				}?> 
			</tr>
			<tr>
				<th>Remove</th><?php 
				$i=1; 
				foreach($product_records as $product) {
					if($i<=6) { 
						$discount=round(100*($product->price-$product->saleprice)/$product->price);?>
						<td class='text-center'>
							<a class="remove-icon" href="javascript:void(0);" onclick="if(confirm('Are you sure you want to remove this item from your Compare list?')){window.location='<?php echo site_url('Ratenreview_controller/delete_compr/'.$product -> id);?>';}"><i class="fa fa-times"></i></a>
						</td><?php   
					} 
					$i++;  
				}?> 
			</tr>
		</table><?php
	}else{
		echo "<h3>Oops! there is no product in Compare List</h3>";
	} ?>
</div>
<div class="row shop_wrapper grid_list">
	<?php
	foreach ($product_records as $cartkey=>$product){
		$discount=round(100*($product->price-$product->saleprice)/$product->price);
		$images = (array)json_decode($product->images);
		foreach($images as $photo_id=>$photo_obj){
			if(!empty($photo_obj))
			{
				$photo = (array)$photo_obj;
			}
		}
	   ?>
		<div class="col-12 ">
			<div class="single_product cat">
				<div class="product_thumb">
					<a href="<?php echo site_url(implode('/', $base_url).'/'.$product->slug); ?>"><img src="<?php echo base_url().'uploads/images/small/'.$photo['filename'];?>" alt="product name"></a>	
				</div>	
				<div class="product_content grid_content">
					<div class="product_name">
						<h4><a href="<?php echo site_url(implode('/', $base_url).'/'.$product->slug); ?>"><?php echo $product->name;?></a></h4>
					</div>
					<div class="product_rating11" product_size="<?php echo $product->product_size;?>">
						<ul>
							<li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
							<li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
							<li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
							<li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
							<li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
						</ul>
					</div>
					<div class="price-container">
						 <div class="price_box">
							<?php 
								$price_design = $this->natuur->get_natuur_currency_function($product->price,$product->saleprice);
								echo $price_design['price_design'];
							?>										
						</div>
						<div class="wishlist_btn">
							<a href="wishlist.html" title="wishlist"><i class="ion-android-favorite-outline"></i></a>
						</div>
					</div>
				</div>
				<div class="product_content list_content">
					<div class="product_name">
					   <h4><a href="<?php echo base_url($product->slug); ?>"><?php echo $product->name;?></a></h4>
					</div>
					<!-- <div class="product_rating">
						<ul>
							<li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
							<li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
							<li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
							<li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
							<li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
						</ul>
					</div> -->
					 <div class="price_box">
						<?php 
							$price_design = $this->natuur->get_natuur_currency_function($product->price,$product->saleprice);
							echo $price_design['price_design'];
						?>	 
					</div>
					<div class="action_links mywishlist_action_links wishlistTabPage">
						<ul>
							<li><div class="number number_quantity">
								<span class="minus span1" >-</span>
									<input type="text" value="1" id="quantity<?php echo $product->id;?>" class="quantity1" name="quantity"/>
								<span class="plus span1">+</span>
						</div></li>
							<li class="add_to_cart"><a class="btn add_to_cart_button" title="Add to Bag"href="javascript:void(0);" onclick="addToCartProduct(<?php echo $product->id;?>);">Add to Bag</a></li>
							<li><a href="#" class="confirm_delete" onclick="if(confirm('Are you sure you want to remove this item from your wishlist?')){window.location='<?php echo site_url('secure/delete_wishlist/'.$product -> id);?>';}"><i class="fa fa-trash"></i></a></li>
							<!--<li class="quick_view"><a href="#" data-toggle="modal" data-target="#modal_box" title="Quick View"><i class="ion-eye"></i></a></li>-->
						</ul>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>
</div>
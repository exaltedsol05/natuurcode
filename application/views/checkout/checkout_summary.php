
<?php 
$shipping_chrg =$this->session->userdata('shipping_price');
?>
<div class="table-responsive">	
	<table class="table table-bordered">
		<thead>
			<tr>
				<th  class="cart-romove item">Remove</th>
				<!--<th class="cart-description item">Image</th>-->
				<th  class="cart-product-name item">Product Name</th>
				<th  class="cart-qty item">Quantity</th>
				<th style="width:15%;" class="cart-sub-total item">Price</th>
				<th style="width:15%;" class="cart-total last-item">Totals</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$subtotal = 0;
            $i=0;
			foreach ($this->dentkart->contents() as $cartkey=>$product): 
				$photo1  = theme_img('no_picture.png', 'No Image Available');
				$arrimages = array_values((array)json_decode($product['images']));
				if(!empty($arrimages[0])){
					$primary1    = $arrimages[0];
					$photo1  = '<img style="width:70px;height:50px;" src="'.base_url('uploads/images/small/'.$primary1->filename).'" alt="'.$product['seo_title'].'"/>';
				}

				$i++;
			?>
			<tr>
				<td class="romove-item">
					<a href="#" onclick="if(confirm('Are you sure you want to remove this item from your cart?')){window.location='<?php echo site_url('cart/remove_item/'.$cartkey);?>';}" title="cancel" class="icon">
						<i class="fa fa-trash-o"></i>
					</a>
				</td>
				
				
				<!--<td class="cart-image">
					<a class="entry-thumbnail" href="<?php echo site_url($product['slug']); ?>"><?php echo $photo1; ?></a>
				</td>-->
				<td class="cart-product-name-info">
					<p class="cart-product-description 1"><a href="<?php echo site_url(implode('/', $base_url).'/'.$product['slug']); ?>"><?php echo $product['name']; ?></a></p>
				</td>
				
				<td style="white-space:nowrap">
					<?php if($this->uri->segment(1) == 'cart'): ?>
						<?php if(!(bool)$product['fixed_quantity']):?>
							<div class="control-group">
								<div class="controls" style="float:left;">
								   <div class="btn-group">
										<input class="form-control input-sm" name="cartkey[<?php echo $cartkey;?>]"  value="<?php echo $product['quantity'] ?>" type="text" id="cart_qunt<?php echo $product['sku']; ?>">
									</div>										
								</div>
						<?php else:?>
							<?php echo $product['quantity'] ?>
							<input type="hidden" name="cartkey[<?php echo $cartkey;?>]" value="1"/>
						<?php endif;?>
					<?php else: ?>
						<?php echo $product['quantity'] ?>
					<?php endif;?>
				</td>
				<td class="cart-product-sub-total"><span class="cart-sub-total-price"><i class="fa fa-inr"></i> <?php echo $product['price'];?></span></td>
				<td class="cart-product-grand-total"><span class="cart-grand-total-price"><i class="fa fa-inr"></i> <?php echo $product['price']*$product['quantity']; ?></span></td>
			</tr>
			<?php endforeach;?>
		</tbody><!-- /tbody -->
		<tfoot>
			<tr>
				<td colspan="4" style="text-align:right;"><strong><?php echo 'Subtotal';?></strong></td>
				<td id="gc_subtotal_price"><span class="fa fa-rupee"></span> <?php echo $this->dentkart->subtotal(); ?>
				<input type="hidden" value="<?php echo trim($this->dentkart->subtotal()); ?>" name="subtotalamt" id="subtotalamt" />
				</td>
			</tr>				
			<?php if($this->dentkart->coupon_discount() > 0) {?>
			<tr>
				<td colspan="4" style="text-align:right;"><strong><?php echo 'Coupon Discount';?></strong></td>
				<td id="gc_coupon_discount">- <span class="fa fa-rupee"></span> <?php echo $this->dentkart->coupon_discount();?></td>
			</tr>
				<?php if($this->dentkart->order_tax() != 0) { 
				// Only show a discount subtotal if we still have taxes to add (to show what the tax is calculated from)?> 
				<tr>
					<td colspan="4" style="text-align:right;"><strong><?php echo 'Discounted Subtotal';?></strong></td>
					<td id="gc_coupon_discount"><span class="fa fa-rupee"></span> <?php echo $this->dentkart->discounted_subtotal();?></td>
				</tr><?php
				}
			} 

			$charges = $this->dentkart->get_custom_charges();
			if(!empty($charges))
			{
				foreach($charges as $name=>$price) : ?>					
					<tr>
						<td colspan="4"  style="text-align:right;"><strong><?php echo $name?></strong></td>
						<td><span class="fa fa-rupee"></span> <?php echo $price; ?></td>
					</tr>
				<?php endforeach;
			}	
			if($this->dentkart->shipping_cost()>0) : ?>
				<tr>
				<td colspan="4" style="text-align:right;"><strong><?php echo 'Shipping';?></strong></td>
				<td><span class="fa fa-rupee"></span> <?php echo $this->dentkart->shipping_cost(); ?></td>
			</tr>
			<?php endif;
			if($this->dentkart->order_tax() > 0) :  ?>
			<tr>
				<td colspan="4" style="text-align:right;"><strong><?php echo 'Tax';?></strong></td>
				<td><span class="fa fa-rupee"></span> <?php echo $this->dentkart->order_tax();?></td>
			</tr>
			<?php endif; 
			if($this->dentkart->shipping_cost()>0) : ?>
				<tr>
				<td colspan="4" style="text-align:right;"><strong><?php echo 'Shipping';?></strong></td>
				<td><span class="fa fa-rupee"></span> <?php echo $this->dentkart->shipping_cost(); ?></td>
			</tr>
			<?php endif; 
			if($this->session->userdata('redim_amt') > 0){?>
				<tr>
					<td colspan="4" style="text-align:right;"><strong><?php echo 'E-Cash';?></strong></td>
					<td>-<span class="fa fa-rupee"></span> <?php echo $this->session->userdata('redim_amt'); ?></td>
			</tr><?php
			}
			//for shipping charge
			if(isset($shipping_chrg)){?>
				<tr class="shippingtr" style="display:none;">
					<td colspan="4" style="text-align:right;"><strong><?php echo 'Shipping Charges';?></strong></td>
					<td><span class="fa fa-rupee"></span> <span class="ship_amount"><?php echo $shipping_chrg; ?></span>
					<input type="hidden" value="<?php echo $shipping_chrg;?>" name="shippingtr"></td>
				</tr>
				<tr class="shippingtr" style="display:none;">
					<td colspan="4" style="text-align:right;"><strong>Grand Total</strong></td>
					<td><strong><span class="fa fa-rupee"></span> <?php $gtotal =  $this->dentkart->total(); ?>
					<span class="sub_total" value="<?=$gtotal?>"></span>
					<?php
						$gtotal = $gtotal + $shipping_chrg;
						if($this->session->userdata('redim_amt') > 0){
							$gtotal = $gtotal - $this->session->userdata('redim_amt');
						}?>
						<span class="grand_total"><?php echo $gtotal;?></span>
						</strong>
					</td>
				</tr><?php
			}?>
			<?php
			/**************************************************************
			Gift Cards
			**************************************************************/
			if($this->dentkart->gift_card_discount() > 0) : ?>
			<tr>
				<td colspan="4" style="text-align:right;"><strong>Gift Card Discount</strong></td>
				<td>-<span class="fa fa-rupee"></span><?php echo $this->dentkart->gift_card_discount(); ?></td>
			</tr>
			<?php endif; ?>
			<!-- end of shipping charge-->
			<tr class="antishippingtr">
				<td colspan="4" style="text-align:right;"><strong>Grand Total</strong></td>
				<td><strong><span class="fa fa-rupee"></span> <?php $gtotal =  $this->dentkart->total(); 
					if($this->session->userdata('redim_amt') > 0){
						$gtotal = $gtotal - $this->session->userdata('redim_amt');
					}
					echo $gtotal;
				?></strong><input type="hidden" name="order_grant_total" id="order_grant_total" value="<?php echo $gtotal; ?>" /></td>
			</tr>
		</tfoot>
	</table>
</div>

	<script>
	jQuery(document).ready(function(){

    // This button will increment the value
    $('.qtyplus').click(function(e){
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
		fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('#'+fieldName).val());
        // If is not undefined
        if (!isNaN(currentVal)) {
            // Increment
            $('#'+fieldName).val(currentVal + 1);
        } else {
            // Otherwise put a 0 there
            $('#'+fieldName).val(0);
        }
    });
    // This button will decrement the value till 0
    $(".qtyminus").click(function(e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('#'+fieldName).val());
        // If it isn't undefined or its greater than 0
        if (!isNaN(currentVal) && currentVal > 0) {
            // Decrement one
            $('#'+fieldName).val(currentVal - 1);
        } else {
            // Otherwise put a 0 there
            $('#'+fieldName).val(0);
        }
    });
	});
	</script>
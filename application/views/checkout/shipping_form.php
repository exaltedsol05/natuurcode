<?php $this->load->view('vwHeader');?>
<style>#showMenu{display:none;}</style>
<section class="container">

<div class="page-header">
	<h2>Checkout</h2>
</div>

<?php if (validation_errors()):?>
	<div class="alert alert-danger">
		<a class="close" data-dismiss="alert">×</a>
		<?php echo validation_errors();?>
	</div>
<?php endif;?>

<?php include('order_details.php');?>

<?php echo form_open('checkout/step_2');?>
	<div class="row">
		<div class="col-sm-12">
				<h2>Shipping Method</h2>
				<div class="alert alert-danger" id="shipping_error_box" style="display:none"></div>
				<table class="table">
					<?php
					
					foreach($shipping_methods as $key=>$val):
						$ship_encoded	= md5(json_encode(array($key, $val)));
					
						if($ship_encoded == $shipping_code)
						{
							$checked = true;
						}
						else
						{
							$checked = false;
						}
					?>
					<tr style="cursor:pointer">
						<td style="width:16px;">
							<label class="radio"><?php echo form_radio('shipping_method', $ship_encoded, set_radio('shipping_method', $ship_encoded, $checked), 'id="s'.$ship_encoded.'"');?></label>
						</td>
						<td onclick="toggle_shipping('s<?php echo $ship_encoded;?>');"><?php echo $key;?></td>
						<td onclick="toggle_shipping('s<?php echo $ship_encoded;?>');"><strong><?php echo $val['str'];?></strong></td>
					</tr>
					<?php endforeach;?>
				</table>
		</div>
		<div class="col-sm-12">
			<h2>Shipping Notes</h2>
			<?php echo form_textarea(array('name'=>'shipping_notes', 'value'=>set_value('shipping_notes', $this->dentkart->get_additional_detail('shipping_notes')), 'class'=>'col-sm-6', 'style'=>'height:75px;'));?>
		</div>
	</div>
	<br/>
	<input class="btn btn-primary" type="submit" value="Continue"/>
</form>
<script>
	function toggle_shipping(key)
	{
		var check = $('#'+key);
		if(!check.attr('checked'))
		{
			check.attr('checked', true);
		}
	}
</script>
</section>
<?php $this->load->view('vwFooter');?>
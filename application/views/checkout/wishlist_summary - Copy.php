<div class="table-responsive">
	<table class="table">
		<thead>
			<tr>
				<th colspan="4" class="heading-title">My Wishlist</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$subtotal = 0;
            $i=0;
			foreach ($product_records as $cartkey=>$product){
				$discount=round(100*($product->price-$product->saleprice)/$product->price);
				$images = (array)json_decode($product->images);
				foreach($images as $photo_id=>$photo_obj){
					if(!empty($photo_obj))
					{
						$photo = (array)$photo_obj;
					}
				}
				$i++;?>
				<tr>
					<td class="col-md-2">
						<a href="<?php echo site_url(implode('/', $base_url).'/'.$product->slug); ?>"><img src="<?php echo base_url().'uploads/images/small/'.$photo['filename'];?>" alt="product name"></a> 
					</td>
					<td class="col-md-7">
						<div class="product-name"><a href="<?php echo site_url(implode('/', $base_url).'/'.$product->slug); ?>"><?php echo $product->name ?></a></div>
						<div class="rating">
							<i class="fa fa-star rate"></i>
							<i class="fa fa-star rate"></i>
							<i class="fa fa-star rate"></i>
							<i class="fa fa-star rate"></i>
							<i class="fa fa-star non-rate"></i>
							<span class="review">( 06 Reviews )</span>
						</div>
						<div class="price">
							<?php if($discount > 0):?>
								₹ <?php echo $product->saleprice;?>	
								<span>₹ <?php echo $product->price;?></span>
							<?php else: ?>
							    ₹ <?php echo $product->price;?>
							<?php endif;?>
						</div>
					</td>
					<td class="col-md-2">
						<a href="javascript:void(0);" onclick="addToCartProduct(<?php echo $product->id;?>);" class="btn-upper btn btn-primary">Add to Cart</a>
					</td>
					<td class="col-md-1 close-btn">
						<a href="#" onclick="if(confirm('Are you sure you want to remove this item from your wishlist?')){window.location='<?php echo site_url('ratenreview_controller/delete_wishlist/'.$product -> id);?>';}"><i class="fa fa-times"></i></a>
					</td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
<?php $this->load->view('vwHeader');?>
<?php

$countries = $this->Location_model->get_countries_menu();
if(!empty($customer['bill_address']['country_id'])){
	$zone_menu	= $this->Location_model->get_zones_menu($customer['bill_address']['country_id']);
}else{
	$zone_menu = array(''=>'')+$this->Location_model->get_zones_menu(array_shift(array_keys($countries)));
}
unset($zone_menu[current(array_keys($zone_menu))]);



$address1	= array('placeholder'=>'House number and street name', 'class'=>'', 'name'=>'address1', 'value'=> set_value('address1', @$customer['bill_address']['address1']));

$address2	= array('placeholder'=>'Apartment, suite, unit etc. (optional)', 'class'=>'', 'name'=>'address2', 'value'=>  set_value('address2', @$customer['bill_address']['address2']));

$first		= array('placeholder'=>'First Name', 'class'=>'', 'id'=>'bill_fname', 'name'=>'firstname', 'value'=>  set_value('firstname', @$customer['firstname']));

$last		= array('placeholder'=>'Last Name', 'class'=>'', 'id'=> 'bill_lname', 'name'=>'lastname', 'value'=>  set_value('lastname', @$customer['lastname']));

$email		= array('placeholder'=>'Email', 'class'=>'', 'name'=>'email', 'value'=> set_value('email', @$customer['email']));

$phone		= array('placeholder'=>'Phone', 'class'=>'', 'id' =>'bill_phone', 'name'=>'phone', 'value'=> set_value('phone', @$customer['phone'])); 

$city		= array('placeholder'=>'City', 'class'=>'', 'name'=>'city', 'value'=> set_value('city', @$customer['bill_address']['city']));

$zip		= array('placeholder'=>'Zip', 'maxlength'=>'10', 'class'=>'', 'name'=>'zip', 'value'=> set_value('zip', @$customer['bill_address']['zip']));

//form elements


$address11	= array('placeholder'=>'House number and street name', 'class'=>'address1 ', 'name'=>'address11', 'value'=> set_value('address11', @$customer['ship_address']['address1']));

$address21	= array('placeholder'=>'Apartment, suite, unit etc. (optional)', 'class'=>'address1 ', 'name'=>'address21', 'value'=>  set_value('address21', @$customer['ship_address']['address2']));

$first1		= array('placeholder'=>'First Name', 'class'=>'address1 ', 'name'=>'firstname1', 'value'=>  set_value('firstname1', @$customer['firstname']));

$last1		= array('placeholder'=>'Last Name', 'class'=>'address1 ', 'name'=>'lastname1', 'value'=>  set_value('lastname1', @$customer['lastname']));

$email1		= array('placeholder'=>'Email', 'class'=>'address1 ', 'name'=>'email1', 'value'=> set_value('email1', @$customer['email']));

$phone1		= array('placeholder'=>'Phone', 'class'=>'address1 ', 'name'=>'phone1', 'value'=> set_value('phone1', @$customer['phone']));

$city1		= array('placeholder'=>'City', 'class'=>'address1 ', 'name'=>'city1', 'value'=> set_value('city1', @$customer['ship_address']['city']));

$zip1		= array('placeholder'=>'Zip', 'maxlength'=>'10', 'class'=>'address1 ', 'name'=>'zip1', 'id' => 'ship_zip',  'value'=> set_value('zip1', @$customer['ship_address']['zip']));

//echo "<pre>";print_r($customer['ship_address']);
?>
 <!--breadcrumbs area start-->
    <div class="breadcrumbs_area">
        <div class="container">   
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <ul>
                            <li><a href="index.html">home</a></li>
                            <li><a href="checkout.html">checkout</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>         
    </div>
    <!--breadcrumbs area end-->
    
    
    <!--Checkout page section-->
    <div class="Checkout_section">
       <div class="container">
            <div class="row">
               <div class="col-12">
			        
<?php if ($this->session->flashdata('message')):?>
	<div class="alert alert-info">
		<a class="close" data-dismiss="alert">×</a>
		<?php echo $this->session->flashdata('message');?>
	</div>
<?php endif;?>
<?php if ($this->session->flashdata('error')):?>
	<div class="alert alert-danger">
		<a class="close" data-dismiss="alert">×</a>
		<?php echo $this->session->flashdata('error');?>
	</div>
<?php endif;?>
<?php if (!empty($error)):?>
	<div class="alert alert-danger">
		<a class="close" data-dismiss="alert">×</a>
		<?php echo $error;?>
	</div>
<?php endif;?>
<?php if (validation_errors()):?>
	<div class="alert alert-danger">
		<a class="close" data-dismiss="alert">×</a>
		<?php echo validation_errors();?>
	</div>
<?php endif;?>
                    <div class="user-actions">
                        <h3> 
							<?php if(!$this->Customer_model->is_logged_in(false, false)){ ?>
								<i class="fa fa-file-o" aria-hidden="true"></i>
								Returning customer?
								<a class="Returning">Please login</a>   
								<div class="checkout_width" id="checkout_login">
									<div class="checkout_loggedout" id="checkout_info">
										<h4><span>1</span>Login or Signup</h4>
										<div class="checkout_logedpart">
											<div class="row">
												<div class="col-md-6 col-sm-6">
													<div class="logedout_email_mobile" id="mailField">
														<input type="text" class="inputText" id="chk_email" name="email"/>
														<span class="floating-label">Enter Email/Mobile Number</span>
														<div class="error_mesg" id="mail-error"></div>
													</div>
													<div class="checkout_continue btn_div"><button class="btn" id="chk_user">continue</button></div>
												</div>
												<div class="col-md-6 col-sm-6"></div>
											</div>
										</div>
									</div>
								</div>
						   <?php }else{?>
							   <div class="checkout_login">
									<div class="row">
										<div class="col-md-11 checkout_login_left">
											<div class="chk_login_left">
												<span>1</span>
												<small>LOGIN<i class="fa fa-check" aria-hidden="true"></i></small>
												<div class="namePH"><b><?php echo $customer['firstname']!='' ? $customer['firstname'] : $customer['email']; ?></b><?php echo $customer['phone']; ?></div>
											</div>
										</div>
										<!--<div class="col-md-1 checkout_login_right">
											<div class="changeBtn"><button class="btn">change</button></div>
										</div>-->
									</div>
								</div>
						   <?php }?>
								
								<div id="checkout_address">
									<div class="checkout_address">
										<h4><span>2</span>Delivery Address</h4>
										<div>
										<?php
										$c = 1;
										foreach($customer_addresses as $a){
											$checked = '';
											if($customer['default_shipping_address']==0){
												if($c==1){
													$checked = 'checked';
												}else{
													$checked = '';
												}
											}else if($customer['default_shipping_address']==$a['id']){
												$checked = 'checked';
											}else{
												$checked = '';
											}
										?>
											<div class="radio_section">
												<label for="address<?php echo $a['id']; ?>">
													<input type="radio" id="address<?php echo $a['id']; ?>" name="address_checked" value="<?php echo $a['id']; ?>" <?php  echo $checked; ?>>
													<div class="radio_text">
														<div class="clearfix">
															<div class="address_edit" id="address_edit<?php echo $a['id'];?>">
															<?php if($checked=='checked') { ?>
																<div class="edit_user_address" rel="<?php echo $a['id'];?>">edit</div>
															<?php } ?>	
															</div>
															<?php
															
															//print_r($a['field_data']);
														
															?>
															<div class="address_name"><span><?php echo $a['field_data']['firstname'].' '.$a['field_data']['phone']; ?></span></div>
														</div>
														<div class="address_details">
															<?php echo $a['field_data']['address1'].','.$a['field_data']['landmark'].','.$a['field_data']['city'].','.$a['field_data']['country'].','.$a['field_data']['zone_name'].' - '.$a['field_data']['zip']; ?>
														</div>
														<div class="deliver_here" id="deliver_here<?php echo $a['id'];?>">
														<?php if($checked=='checked') { ?>
															<button class="btn deliver_here_btn" data-id="<?php echo $a['id']; ?>">deliver here</button>
														<?php } ?>
														</div>
														<div class="address-form-container" id="address-form-container<?php echo $a['id']; ?>">
														</div>
													</div>
												</label>
											</div>
										<?php
											$c++;
										}
										?>	
										</div>
									</div>
									<!-- Add New Address Start -->
									<div class="add_new_address edit_user_address" rel="0">
										<div><i class="fa fa-plus" aria-hidden="true"></i>Add a New Address</div>
									</div>
									<!-- Add New Address End -->
									<div class="address-form-container" id="address-form-container0">
									</div>
								</div>
								<div class="checkout_login" id="delivered_address" style="display:none;">
									<div class="row">
										<div class="col-md-11 checkout_login_left">
											<div class="chk_login_left">
												<span>2</span>
												<small>Delivery Address<i class="fa fa-check" aria-hidden="true"></i></small>
												<div class="namePH" id="delivered_address_details"></div>
											</div>
										</div>
										<div class="col-md-1 checkout_login_right">
											<div class="changeBtn"><button class="btn" id="change_delivered_address">change</button></div>
										</div>
									</div>
								</div>
								<div id="checkout_order_summary">
									<div class="checkout_order_summary">
										<h4><span>2</span>Order Summary</h4>
										<?php echo form_open('cart/update_cart', array('id'=>'update_cart_form'));?>
										<div class="list">
											<div class="order_table table-responsive">
												<table>
													<thead>
														<tr>
															<th>Product Image</th>
															<th>Product</th>
															<th>Total</th>
														</tr>
													</thead>
													<tbody>
															<?php
															$subtotal = 0;
															$i=0;
															foreach ($this->natuur->contents() as $cartkey=>$product): 
																$photo1  = theme_img('no_picture.png', 'No Image Available');
																$arrimages = array_values((array)json_decode($product['images']));
																if(!empty($arrimages[0])){
																	$primary1    = $arrimages[0];
																	$photo1  = '<img style="width:70px;height:50px;" src="'.base_url('uploads/images/small/'.$primary1->filename).'" alt="'.$product['seo_title'].'"/>';
																}

																$i++;
															?>
															<tr>
																<td class="cart-image">
																	<a class="entry-thumbnail" href="<?php echo site_url($product['slug']); ?>"><?php echo $photo1; ?></a>
																	<div class="qtyNumber">
																		<!--<span class="minus">-</span>
																		<input type="number" class="count" name="qty" value="1">
																		<span class="plus">+</span>-->
																		<?php if($this->uri->segment(1) == 'checkout'){ ?>
																		<?php if(!(bool)$product['fixed_quantity']){?>
																			<div class="control-group">
																				<div class="controls">
																				   <div class="btn-group">
																					<a value="-" class="qtyminus btn-sm" field="cartkey<?php echo $product['sku']; ?>">
																						<i class="fa fa-minus" style="float:left;padding-top:12px;padding-right: 5px;"></i>
																					</a>
																						<input class="form-control input-sm" style="margin:0px;width:50px;float:left;height: 34px;border-radius: 0px;float:left;border: 1px solid #ced4da;" name="cartkey[<?php echo $cartkey;?>]"  value="<?php echo $product['quantity'] ?>" size="3" type="text" id="cartkey<?php echo $product['sku']; ?>">
																						 <a type="button" value="+" class="qtyplus btn-sm change" field="cartkey<?php echo $product['sku']; ?>" style="float:left;padding-top:8px;padding-left: 5px;"><i class="fa fa-plus"></i></a>
																					</div>										
																				</div>
																				</div>
																		<?php }else{?>
																			<?php echo $product['quantity'] ?>
																			<input type="hidden" name="cartkey[<?php echo $cartkey;?>]" value="1"/>
																		<?php };?>
																														<?php }else{ ?>
																		<?php echo $product['quantity'] ?>
																														<?php };?>
																	</div>
																	<!--<div class="removeCart"><a href="#">Remove</a></div>-->
																</td>
																<td class="cart-image">
																	<a class="entry-thumbnail" href="<?php echo site_url($product['slug']); ?>"><?php echo $product['name']; ?></a>
																</td>
																<td class="cart-product-grand-total" product_size="<?php echo $product['product_size']*$product['quantity']; ?>"><span class="cart-grand-total-price"><i class="fa fa-inr"></i> <?php echo $product['price']*$product['quantity']; ?></span></td>
															</tr>
															<?php endforeach;?>
														</tbody><!-- /tbody -->
													<tfoot>
														<tr>
															<th colspan="1">
																Total Weight: <?php echo $order_weight = $this->natuur->order_weight(); ?>. Gms
																<input type="hidden" id="weight" value="<?=$order_weight; ?>">
															</th>
															<th colspan="">Cart Subtotal</th>
															<td><span class="fa fa-rupee"></span> <?php echo $this->natuur->subtotal(); ?></td>
														</tr>
														<?php if($this->natuur->coupon_discount() > 0) {?>
														<tr>
														
															<th colspan="3">Coupon Discount</th>
															<td><strong>- <i class="fa fa-inr"></i> <?php echo $this->natuur->coupon_discount();?></strong></td>
														</tr>
														<?php } ?>
														<tr class="order_total">
															<th colspan="2">Order Total</th>
															<td><strong><span class="fa fa-rupee"></span> <?php echo $this->natuur->total(); ?></strong></td>
														</tr>
													</tfoot>
												</table>     
											</div>
										</div>
										</form>
									</div>
								</div>
                        </h3>   
                    </div>
                    
               </div>
            </div>
			<?php echo form_open('checkout/step_1', 'id="checkoutform" name="checkoutform"')?>
            <div class="checkout_form">
                <div class="row">
				   
                    <div class="col-lg-6 col-md-6">
                        
                            <h3>Billing Details</h3>
                            <div class="row">

                                <div class="col-lg-6 mb-20">
                                    <label>First Name <span>*</span></label>
                                    <?php echo form_input($first);?> 
                                </div>
                                <div class="col-lg-6 mb-20">
                                    <label>Last Name  <span>*</span></label>
                                    <?php echo form_input($last);?>
                                </div>
                                <div class="col-lg-6 mb-20">
                                    <label>Email Address</label>
                                   <?php echo form_input($email);?>   
                                </div>
								 <div class="col-lg-6 mb-20">
                                    <label>Mobile No.</label>
                                   <?php echo form_input($phone);?>
                                </div>
								
                                <div class="col-12 mb-20">
                                    <label for="country">country <span>*</span></label>
                                    
                                       <?php //echo form_dropdown('country_id',$countries, @$customer['bill_address']['country_id'], 'id="country_id" class="niceselect_option"');?>  

                                </div>
								 <div class="col-12 mb-20">
                                    <label for="country">State/UT <span>*</span></label>                     
                                      <?php //echo form_dropdown('zone_id',$zone_menu, @$customer['bill_address']['zone_id'], 'id="zone_id" class="niceselect_option" ');?>  

                                </div>

                                <div class="col-12 mb-20">
                                    <label>Street address  <span>*</span></label>
                                   
                                    <?php echo form_input($address1);?>									
                                </div>
                                <div class="col-12 mb-20">
                                   
									<?php echo form_input($address2);?>										
                                </div>
                                <div class="col-lg-6 mb-20">
                                    <label>Town / City <span>*</span></label>
                                   <?php echo form_input($city);?>  
                                </div> 
                                 <div class="col-lg-6 mb-20">
                                    <label>Pin Code <span>*</span></label>
                                    <?php echo form_input($zip);?>
                                </div> 
                               
                                <div class="col-12 mb-20">
                                    <input id="account" type="checkbox" data-target="createp_account" />
                                    <label for="account" data-toggle="collapse" data-target="#collapseOne" aria-controls="collapseOne">Create an account?</label>

                                    <div id="collapseOne" class="collapse one" data-parent="#accordion">
                                        <div class="card-body1">
                                           <label> Account password   <span>*</span></label>
                                            <input placeholder="password" type="password">  
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 mb-20">
                                    <?php $use_shipping='checked'; echo form_checkbox(array('name'=>'use_shipping', 'value'=>'yes', 'id'=>'use_shipping', 'checked'=>$use_shipping)) ?>
									
                                    <label class="righ_0" for="address" data-toggle="collapse" data-target="#collapsetwo" aria-controls="collapseOne">Ship to a different address?</label>

                                    <div id="collapsetwo" class="collapse one" data-parent="#accordion">
                                       <div class="row">
                                            <div class="col-lg-6 mb-20">
                                    <label>First Name <span>*</span></label>
                                    <?php echo form_input($first1);?> 
                                </div>
                                <div class="col-lg-6 mb-20">
                                    <label>Last Name  <span>*</span></label>
                                    <?php echo form_input($last1);?>
                                </div>
                                <div class="col-lg-6 mb-20">
                                    <label>Email Address</label>
                                   <?php echo form_input($email1);?>   
                                </div>
								 <div class="col-lg-6 mb-20">
                                    <label>Mobile No.</label>
                                   <?php echo form_input($phone1);?>
                                </div>
								
                                <div class="col-12 mb-20">
                                    <label for="country">country <span>*</span></label>
                                    
                                       <?php echo form_dropdown('country_id1',$countries, @$customer['bill_address']['country_id'], 'id="country_id1" class="niceselect_option"');?>  

                                </div>
								 <div class="col-12 mb-20">
                                    <label for="country">State/UT <span>*</span></label>                     
                                      <?php echo form_dropdown('zone_id1',$zone_menu, @$customer['bill_address']['zone_id'], 'id="zone_id1" class="niceselect_option" ');?>  

                                </div>

                                <div class="col-12 mb-20">
                                    <label>Street address  <span>*</span></label>
                                   
                                    <?php echo form_input($address11);?>									
                                </div>
                                <div class="col-12 mb-20">
                                   
									<?php echo form_input($address21);?>										
                                </div>
                                <div class="col-lg-6 mb-20">
                                    <label>Town / City <span>*</span></label>
                                   <?php echo form_input($city1);?>  
                                </div> 
                                 <div class="col-lg-6 mb-20">
                                    <label>Pin Code <span>*</span></label>
                                    <?php echo form_input($zip1);?>
                                </div> 
                               
										
										</div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="order-notes">
                                         <label for="order_note">Order Notes</label>
                                        <textarea name="shipping_notes" id="shipping_notes" placeholder="Notes about your order, e.g. special notes for delivery."></textarea>
										
                                    </div>    
                                </div>     	    	    	    	    	    	    
                            </div>
                          
                    </div>
                    <div class="col-lg-6 col-md-6">
                        
                            <h3>Your order</h3> 
                            <div class="order_table table-responsive">
                                <table>
                                    <thead>
                                        <tr>
										    <th>Product Image</th>
                                            <th>Product</th>
											<th>Quantity</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
											<?php
											$subtotal = 0;
											$i=0;
											foreach ($this->natuur->contents() as $cartkey=>$product): 
												$photo1  = theme_img('no_picture.png', 'No Image Available');
												$arrimages = array_values((array)json_decode($product['images']));
												if(!empty($arrimages[0])){
													$primary1    = $arrimages[0];
													$photo1  = '<img style="width:70px;height:50px;" src="'.base_url('uploads/images/small/'.$primary1->filename).'" alt="'.$product['seo_title'].'"/>';
												}

												$i++;
											?>
											<tr>
												<td class="cart-image">
													<a class="entry-thumbnail" href="<?php echo site_url($product['slug']); ?>"><?php echo $photo1; ?></a>
												</td>
												<td class="cart-image">
													<a class="entry-thumbnail" href="<?php echo site_url($product['slug']); ?>"><?php echo $product['name']; ?></a>
												</td>
												<td style="white-space:nowrap">
													<?php if($this->uri->segment(1) == 'cart'): ?>
														<?php if(!(bool)$product['fixed_quantity']):?>
															<div class="control-group">
																<div class="controls" style="float:left;">
																   <div class="btn-group">
																		<input class=" input-sm" name="cartkey[<?php echo $cartkey;?>]"  value="<?php echo $product['quantity'] ?>" type="text" id="cart_qunt<?php echo $product['sku']; ?>">
																	</div>
																</div>
															</div>
														<?php else:?>
															<?php echo $product['quantity'] ?>
															<input type="hidden" name="cartkey[<?php echo $cartkey;?>]" value="1"/>
														<?php endif;?>
													<?php else: ?>
														<?php echo $product['quantity'] ?>
													<?php endif;?>
												</td>
												<td class="cart-product-grand-total" product_size="<?php echo $product['product_size']*$product['quantity']; ?>"><span class="cart-grand-total-price"><i class="fa fa-inr"></i> <?php echo $product['price']*$product['quantity']; ?></span></td>
											</tr>
											<?php endforeach;?>
										</tbody><!-- /tbody -->
                                    <tfoot>
                                        <tr>
                                            <th colspan="">Total Weight:</th>
                                            <th colspan="">
                                            <?php echo $products_weight = $this->natuur->products_weight(); ?>. Gms
                                            <input type="hidden" id="weight" value="<?=$products_weight; ?>">

                                            </th>
                                            <th colspan="">Cart Subtotal</th>
                                            <td><span class="fa fa-rupee"></span> <?php echo $this->natuur->subtotal(); ?></td>
                                        </tr>
										<?php if($this->natuur->coupon_discount() > 0) {?>
                                        <tr>
										
                                            <th colspan="3">Coupon Discount</th>
                                            <td><strong>- <i class="fa fa-inr"></i> <?php echo $this->natuur->coupon_discount();?></strong></td>
                                        </tr>
										<?php } ?>
                                        <tr class="order_total">
                                            <th colspan="3">Order Total</th>
                                            <td><strong><span class="fa fa-rupee"></span> <?php echo $this->natuur->total(); ?></strong></td>
                                        </tr>
                                    </tfoot>
                                </table>     
                            </div>
                            <div class="payment_method">
                                 <div class="panel-default">
                                    <input id="shipping_method" name="shipping_method" checked type="checkbox" data-target="createp_account" />
                                    <label for="shipping_method" data-toggle="collapse" data-target="#method" aria-controls="method">Shipping Method(Fixed Shipping&nbsp;&nbsp;<i class="fa fa-inr"></i>&nbsp;<span class="ship_amount">0</span>)</label>

                                    <div id="method" class="collapse one" data-parent="#accordion">
                                        <div class="card-body1">
                                           <p>Please send a check to shipping method and fill shipping Postcode. just add shipping cost in total price.</p>
                                        </div>
                                    </div>
                                </div> 
                               <div class="panel-default">
							   
                                    
                                    <label for="payment_defult" data-toggle="collapse" data-target="#collapsedefult" aria-controls="collapsedefult">Payment Method </label>
									<br/>
                                     <!-- <input id="payment_defult" name="payment_mode" value="PayU_Money"  type="radio" data-target="createp_account" />PayU Money -->
									 <input id="payment_defult1" name="payment_mode" value="Icici" type="radio" data-target="createp_account" />Icici Bank 
									 <input id="payment_defult2" name="payment_mode" value="Razorpay" checked type="radio" data-target="createp_account" />Razorpay
                                    <div id="collapsedefult" class="collapse one" data-parent="#accordion">
                                        <div class="card-body1">
                                           <p>Pay via PayU Money; you can pay with your credit card if you don’t have a PayU Money account.</p> 
                                        </div>
                                    </div>
                                </div>
								
								
								 <div class="panel-default">
							   
                                    <input id="term" name="term" value="yes" type="checkbox" checked data-target="createp_account" />
                                    <label for="term" data-toggle="collapse" data-target="#term1" aria-controls="term1"> I accept the <a href="#">Terms and Conditions</a></label>

                                    <div id="term1" class="collapse one" data-parent="#accordion">
                                        <div class="card-body1">
                                           <p></p> 
                                        </div>
                                    </div>
                                </div>
								
                                <div class="order_button">
                                    <button type="button" value="Place Order" id="confirm_btn">Place Order</button> 
                                </div>    
                            </div> 
                              
                    </div>
					
                </div> 
            </div> 
			 </form> 
        </div>       
    </div>
    <!--Checkout page section end-->
 
 
 
<?php if($this->Customer_model->is_logged_in(false, false)) : ?>
<div class="modal fade" role="dialog" id="address_manager">
    <div class="modal-dialog">
    <!-- Modal content-->
		<div class="modal-content" >
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Your Addresses</h3>
			</div>
			<div class="modal-body">
				<p>
					<table class="table table-striped">
					<?php
					$c = 1;
					foreach($customer_addresses as $a):?>
						<tr>
							<td>
								<?php
								$b	= $a['field_data'];
								echo nl2br(format_address($b));
								?>
							</td>
							<td style="width:100px;"><input type="button" class="btn btn-primary choose_address pull-right" onclick="populate_address(<?php echo $a['id'];?>);" data-dismiss="modal" value="Choose" /></td>
						</tr>
					<?php endforeach;?>
					</table>
				</p>
			</div>

			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
			</div>
		</div>
    </div>
</div>

<div class="modal fade" role="dialog" id="address_manager1">
    <div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content" >
            <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Your Addresses</h3>
			</div>
			<div class="modal-body">
				<p>
					<table class="table table-striped">
					<?php
					$c = 1;
					foreach($customer_addresses as $a):?>
						<tr>
							<td>
								<?php
								$b	= $a['field_data'];
								echo nl2br(format_address($b));
								?>
							</td>
							<td style="width:100px;"><input type="button" class="btn btn-primary choose_address pull-right" onclick="populate_address1(<?php echo $a['id'];?>);" data-dismiss="modal" value="Choose" /></td>
						</tr>
					<?php endforeach;?>
					</table>
				</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
			</div>
		</div>
    </div>
</div>
<?php endif;?>
<script type="text/javascript">
function isValidcontact(contact) {
	var pattern = new RegExp(/^\d{10}$/);
	return pattern.test(contact);
};

function isValidzip(zip) {
	var pattern = new RegExp(/^\d{6}$/);
	return pattern.test(zip);
};


</script>
<?php 
function theme_img($uri, $tag=false)
{
	if($tag)
	{
		return '<img src="'.theme_url('assets/assets/images/'.$uri).'" alt="'.$tag.'">';
	}
	else
	{
		return theme_url('assets/assets/images/'.$uri);
	}
	
}
function theme_url($uri)
{
	$CI =& get_instance();
	return $CI->config->base_url('/'.$uri);
}

$this->load->view('vwFooter');?>


<div class="row">
	<?php if(!empty($customer['bill_address'])):?>
	<div class="col-sm-3">
		<a href="<?php echo site_url('checkout/step_1');?>" class="btn btn-primary">
		
			<?php if($customer['bill_address'] != @$customer['ship_address'])
			{
				echo 'Change Billing Address';
			}
			else
			{
				echo 'Change Address';
			}
			?>
		</a>

		<p>
			<?php echo format_address($customer['bill_address'], true);?>
		</p>
		<p>
			<?php echo $customer['bill_address']['phone'];?><br/>
			<?php echo $customer['bill_address']['email'];?>
		</p>
	</div>
	<?php endif;?>

<?php if(config_item('require_shipping')):?>
	<?php if($this->dentkart->requires_shipping()):?>
		<div class="col-sm-3">
			<a href="<?php echo site_url('checkout/shipping_address');?>" class="btn btn-primary">Change Shipping Address</a>
			<p>
				<?php echo format_address($customer['ship_address'], true);?>
			</p>
			<p>
				<?php echo $customer['ship_address']['phone'];?><br/>
				<?php echo $customer['ship_address']['email'];?><br/>
			</p>
		</div>

		<?php
		
		if(!empty($shipping_method) && !empty($shipping_method['method'])):?>
		<div class="col-sm-3">
			<p><a href="<?php echo site_url('checkout/step_2');?>" class="btn btn-primary">Change Shipping Method</a></p>
			<strong>Shipping Method</strong><br/>
			<?php echo $shipping_method['method'].': '.$shipping_method['price'];?>
		</div>
		<?php endif;?>
	<?php endif;?>
<?php endif;?>

<?php if(!empty($payment_method)):?>
	<div class="col-sm-3">
		<p><a href="<?php echo site_url('checkout/step_3');?>" class="btn btn-primary">Change Billing Method</a></p>
		<?php echo $payment_method['description'];?>
	</div>
<?php endif;?>
</div>


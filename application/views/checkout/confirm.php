<?php $this->load->view('vwHeader');?>
<style>#showMenu{display:none;}</style>
<section class="container">

<div class="page-header">
	<h2>Checkout</small></h2>
</div>
	
<?php include('order_details.php');?>
<?php include('summary.php');?>

<div class="row">
	<div class="col-sm-12">
		<a class="btn btn-primary pull-right" href="<?php echo site_url('checkout/place_order');?>">Submit Order</a>
	</div>
</div>
</section>
<?php $this->load->view('vwFooter');?>
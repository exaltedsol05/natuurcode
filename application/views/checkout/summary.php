	<table class="table">
		<thead>
			<tr><th class="cart-description item">Image</th>
				<th class="cart-product-name item">Product Name</th>
				<th class="cart-sub-total item" style="width:10%;">Price</th>
				<th class="cart-total last-item" style="width:10%;">Totals</th>
				<th class="cart-qty item">Quantity</th>
				<th class="cart-romove item"></th>
			</tr>
		</thead>
		<!--<tfoot>
			<tr>
				<td colspan="7">
					<div class="shopping-cart-btn">
						<span class="">
							<a href="<?php echo base_url(); ?>" class="btn btn-upper btn-primary outer-left-xs">Continue Shopping</a>
							<a href="javascript:void(0);" class="btn btn-upper btn-primary pull-right outer-right-xs" onclick="$(this).closest('form').submit();">Update shopping cart</a>
						</span>
					</div>
				</td>
			</tr>
		</tfoot>-->
		<tbody>
			<?php
			$subtotal = 0;
            $i=0;
			foreach ($this->natuur->contents() as $cartkey=>$product): 
				$photo1  = theme_img('no_picture.png', 'No Image Available');
				$arrimages = array_values((array)json_decode($product['images']));
				if(!empty($arrimages[0])){
					$primary1    = $arrimages[0];
					$photo1  = '<img src="'.base_url('uploads/images/small/'.$primary1->filename).'" alt="'.$product['seo_title'].'"/>';
				}

				$i++;
			?>
			<tr>				
				<td class="cart-image">
					<a class="entry-thumbnail" href="<?php echo site_url($product['slug']); ?>"><?php echo $photo1; ?></a>
				</td>
				<td class="cart-product-name-info 2">
					<h4 class="cart-product-description"><a href="<?php echo site_url(implode('/', $base_url).'/'.$product['slug']); ?>"><?php echo $product['name']; ?></a></h4>
					<small style="font-weight: 600;"><?php echo $product['product_size']; ?></small>
				</td>
				<td class="cart-product-sub-total" product_size="<?php echo $product['product_size']; ?>"><span class="cart-sub-total-price"><i class="<?php echo $product['currency_symbol'];?>"></i> <?php echo $product['product_currency_price'];?></span></td>
				<td class="cart-product-grand-total"><span class="cart-grand-total-price"><i class="<?php echo $product['currency_symbol'];?>"></i> <?php echo $product['subtotal']; ?></span></td>
				
				<td style="white-space:nowrap">
					<?php if($this->uri->segment(1) == 'cart'): ?>
						<?php if(!(bool)$product['fixed_quantity']):?>
							<div class="control-group qtyNumber">
								<div class="controls">
								   <div class="btn-group">
										<a value="-" class="qtyminus btn-sm" field="cartkey<?php echo $product['sku']; ?>">
											<i class="fa fa-minus" style="float:left;padding-top:12px;padding-right: 5px;"></i>
										</a>
										<input class="form-control input-sm" style="margin:0px;width:50px;float:left;height: 34px;border-radius: 0px;float:left;" name="cartkey[<?php echo $cartkey;?>]"  value="<?php echo $product['quantity'] ?>" size="3" type="text" id="cartkey<?php echo $product['sku']; ?>">
										 <a type="button" value="+" class="qtyplus btn-sm change" field="cartkey<?php echo $product['sku']; ?>" style="float:left;padding-top:8px;padding-left: 5px;"><i class="fa fa-plus"></i></a>
									</div>										
								</div>
						<?php else:?>
							<?php echo $product['quantity'] ?>
							<input type="hidden" name="cartkey[<?php echo $cartkey;?>]" value="1"/>
						<?php endif;?>
					<?php else: ?>
						<?php echo $product['quantity'] ?>
					<?php endif;?>
				</td>
				
				<td>
					<a href="javascript:void(0);" class="product-edit" onclick="$(this).closest('form').submit();" style="font-size:28px;"><i class="fa fa-refresh" aria-hidden="true"></i></a> <span style="font-size:28px;">|</span> 
					<a href="javascript:void(0);" onclick="if(confirm('Are you sure you want to remove this item from your cart?')){window.location='<?php echo site_url('cart/remove_item/'.$cartkey);?>';}" title="cancel" class="icon" style="font-size:28px;"><i class="fa fa-trash-o"></i></a></td>
			</tr>
			<?php endforeach;?>
		</tbody><!-- /tbody -->
	</table>
	
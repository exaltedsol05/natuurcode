
<?php function left_categories($parent_id, $cats, $sub='') {?>
		<?php foreach ($cats[$parent_id] as $cat):
		$j++;?>		
				<li class="dropdown menu-item">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon fa fa-shopping-bag" aria-hidden="true"></i><?php echo  $cat->name; ?></a>
					<ul class="dropdown-menu mega-menu">
						<li class="yamm-content">
							<div class="row">	
								<div class="">
									<ul class="links list-unstyled"> 
										<?php foreach ($cats[$cat->id] as $cat1):
										?>
										<li>
											 <a href="<?php echo site_url($cat1->slug);?>" style="padding: 3px 10px;">
												<?=$cat1->name;?>
											 </a>
										</li>
										<?php endforeach;?>	
									</ul>
								</div>
							</div><!-- /.row -->
						</li><!-- /.yamm-content --> 
					</ul><!-- /.dropdown-menu -->            
				</li><!-- /.menu-item --> 		
				<?php endforeach;?>
				
				<?php }
           ?>

        


					
	
                                				
				


<!-- ================================== TOP NAVIGATION ================================== -->
<div class="side-menu animate-dropdown outer-bottom-xs">
    <div class="head"><i class="icon fa fa-align-justify fa-fw"></i> Categories</div>        
    <nav class="yamm megamenu-horizontal" role="navigation">
        <ul class="nav">
             <?php 
if(isset($categories[0]))
{
    left_categories(0, $categories);
}
?>      
        </ul><!-- /.nav -->
    </nav><!-- /.megamenu-horizontal -->
</div><!-- /.side-menu -->
<!-- ================================== TOP NAVIGATION : END ================================== -->



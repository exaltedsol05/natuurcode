<?php $this->load->view('vwHeader');?>

<!--breadcrumbs area start-->
    <div class="breadcrumbs_area">
        <div class="container">   
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <ul>
                            <li><a href="index.html">home</a></li>
                            <li><a href="cart.html">cart page</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>         
    </div>
    <!--breadcrumbs area end-->
    
    <!--shopping cart area start -->
    <div class="shopping_cart_area">
        <div class="container">  
		    <?php 

if ($this->session->flashdata('message')):?>
	<div class="body-content outer-top-xs">
		<div class="container">
			<div class="row ">
				<div class="col-sm-12 alert alert-info">
						<a class="close" data-dismiss="alert">×</a>
						<?php echo $this->session->flashdata('message');?>
				</div>
			</div>
		</div>
	</div><?php 
endif;

if ($this->session->flashdata('error')):?>
	<div class="body-content outer-top-xs">
		<div class="container">
			<div class="row ">
				<div class="col-sm-12 alert alert-danger">
					<a class="close" data-dismiss="alert">×</a>
					<?php echo $this->session->flashdata('error');?>
				</div>
			</div>
		</div>
	</div><?php 
endif;
		
if(!empty($error)):?>
	<div class="body-content outer-top-xs">
		<div class="container">
			<div class="row ">
				<div class="col-sm-12 alert alert-danger">
					<a class="close" data-dismiss="alert">×</a>
					<?php echo $error;?>
				</div>
			</div>
		</div>
	</div><?php 
endif;?>
            <?php echo form_open('cart/update_cart', array('id'=>'update_cart_form'));?>
			<div class="list">
				<input id="redirect_path" type="hidden" name="redirect" value=""/>
                <div class="row">
                    <div class="col-12">
                        <div class="table_desc">
                            <div class="cart_page table-responsive">
                                <?php include('checkout/summary.php');?>
                            
							</div>  
                            <div class="cart_submit">
							     <a class="btn btn-primary pull-left" href="<?php echo site_url('/');?>">Continue Shopping</a>
                                <button type="submit">update cart</button>
                            </div>      
                        </div>
                     </div>
                 </div>
                 <!--coupon code area start-->
                <div class="coupon_area">
                    <div class="row">
						<?php $cur =  $this->natuur->get_currncy();?>
                        <div class="col-lg-6 col-md-6">
                            <div class="coupon_code left">
                                <h3>Coupon</h3>
                                <div class="coupon_inner">   
                                    <p>Enter your coupon code if you have one.</p>                                
                                    <input placeholder="Coupon code" type="text" name="coupon_code">
                                    <button type="submit">Apply coupon</button>
                                </div>    
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="coupon_code right">
                                <h3>Cart Totals</h3>
                                <div class="coupon_inner">
                                   <div class="cart_subtotal">
                                       <p>Subtotal</p>
                                       <p class="cart_amount"><i class="<?php echo $cur['currency_symbol'];?>"></i> <?php echo $this->natuur->subtotal(); ?></p>
                                   </div>
								   <?php if($this->natuur->coupon_discount() > 0) {?>
                                   <div class="cart_subtotal ">
                                       <p>Coupon Discount</p>
                                       <p class="cart_amount">- <i class="<?php echo $cur['currency_symbol'];?>"></i> <?php echo $this->natuur->coupon_discount();?></p>
                                   </div>
								   <?php }?>
                                  

                                   <div class="cart_subtotal">
                                       <p>Total</p>
                                       <p class="cart_amount"><i class="<?php echo $cur['currency_symbol'];?>"></i> <?php echo $this->natuur->total(); ?></p>
                                   </div>
                                   <div class="checkout_btn">
                                      
										
											<button onclick="$('#redirect_path').val('checkout');" type="submit" class="btn btn-primary checkout-btn">PROCCED TO CHEKOUT</button>
											
                                   </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <!--coupon code area end-->
            </form> 
        </div>     
    </div>
    <!--shopping cart area end -->
    <script>
	function checkzipcode(){
		var zip = $.trim($("#pincode").val());
		var zipRegex = /^\d{6}$/;
		var msg = '';
		if (!zipRegex.test(zip)){
			$("#chckpinmsg-green").html('');
			$("#chckpinmsg-red").html("Please enter valid zip code.");
		}else{
			$.ajax({
				url: '<?php echo base_url(); ?>cart/checkzip',
				type: 'post',
				data: {'zipcode':zip},
				dataType:"json",
				success: function(data){
					if(data.color == 'green'){
						$("#chckpinmsg-green").html(data.message+'<br/> Expected Time : '+data.estimate_time);
						$("#chckpinmsg-red").html('');
					}else{
						$("#chckpinmsg-red").html(data.message);
						$("#chckpinmsg-green").html('');
					}
				}
			});
		}
	}
</script>
<?php

function theme_img($uri, $tag=false){
	if($tag){
		return '<img src="'.theme_url('assets/assets/images/'.$uri).'" alt="'.$tag.'">';
	}else{
		return theme_url('assets/assets/images/'.$uri);
	}
}

function theme_url($uri){
	$CI =& get_instance();
	return $CI->config->base_url('/'.$uri);
}

$this->load->view('vwFooter'); ?>
<?php
$this->load->view('vwHeader');
?>
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="<?php echo base_url();?>">Home</a></li>
				<li><a href="#">Blog</a></li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->
<section class="container">
<div class="row">
	
	<div class="col-sm-12">
		<div style="background-color: #fff;box-shadow: 0 2px 4px 0 rgba(0,0,0,.08);padding: 20px;overflow: hidden;">
			<h1 class="text-center" id="share_title"><?php echo $blogDetails->title;?></h1>
			<hr>
			<div class="col-sm-9" id="share_content">
				<?php echo $blogDetails->content; ?>
			</div>
			<div class="col-sm-3">
				<a href="#"><img src="<?php echo base_url().'uploads/blog/'.$blogDetails->blog_image; ?>" alt="img"></a>
			</div>
			<div class="col-md-12 no-padding social">
				<ul class="link">
					<li class="fb pull-left">
						<a title="Facebook" class="facebook share-click" style="cursor:pointer;"></a>
					</li>
					<li class="tw pull-left">
						<a class="twitter share-click" title="Twitter" style="cursor:pointer;"></a>
					</li>
					<li class="googleplus pull-left">
						<a class="google share-click" style="cursor:pointer;"></a>
					</li>
					<li class="linkedin pull-left">
						<a class="linkedin share-click" style="cursor:pointer;"></a>
					</li>
					<li class="whatsapp pull-left">
						<a class="whatsapp share-click" style="cursor:pointer;"></a>
						<!--<a href="javascript:void(0);" class="mct_whatsapp_btn"></a>-->
					</li>
					<li class="pintrest pull-left">
						<a class="pinterest share-click" style="cursor:pointer;"></a>
					</li>
					<!--<li class="youtube pull-left">
						<a class="youtube share-click" style="cursor:pointer;"></a>
					</li>-->
					<li class="tumblr pull-left">
						<a class="tumblr share-click" style="cursor:pointer;"></a>
					</li>
				</ul>
			</div>
		</div>
    </div>
</div>
</section>
<?php
$this->load->view('vwFooter');
?>
<script>
$(document).ready(function() {
    //$(document).on("click", '.mct_whatsapp_btn', function() {    });
	/*social sharing start*/
	var share_url = "<?php echo current_url();?>";
	var image_url = "<?php echo base_url('assets/assets/images/logo.png')?>";
	var imgcontent_url = "<?php echo base_url().'uploads/blog/'.$blogDetails->blog_thumb_image; ?>";
	//var title = "Naturalmama";
	//var descpp = 'Naturalmama is the best destination for all the shopping lovers.';
	var title 	= $("#share_title").text();
	var descpp 	= $("#share_content").text();
	$('.share-click').click(function(event) {
		var shareName = $(this).attr('class').split(' ')[0]; //get the first class name of clicked element
		if(shareName == 'facebook'){
			$.ajaxSetup({ cache: true });
				$.getScript('//connect.facebook.net/en_US/sdk.js', function(){
				FB.init({
				  appId: '1698251497170459', //replace with your app ID
				  version: 'v2.8'
				});
				FB.ui({
					method: 'share',
					title: title,
					href: share_url,
					picture: image_url,
					caption: "Naturalmama",
					description: descpp
				});
			});
			return false;
		}else if(shareName == 'twitter'){
			var openLink = 'http://twitter.com/intent/tweet?url=' + encodeURIComponent(share_url)+'&text='+encodeURIComponent(descpp.substr(0,50)+'...');
			winWidth    = 650;
			winHeight   = 450;
			winLeft     = ($(window).width()  - winWidth)  / 2,
			winTop      = ($(window).height() - winHeight) / 2,
			winOptions   = 'width='  + winWidth  + ',height=' + winHeight + ',top='    + winTop    + ',left='   +winLeft;
			window.open(openLink,'Share',winOptions);
			return false;
		}else if(shareName == 'google'){
			var openLink = 'https://plus.google.com/share?url=' + encodeURIComponent(share_url);
			winWidth    = 650;
			winHeight   = 450;
			winLeft     = ($(window).width()  - winWidth)  / 2,
			winTop      = ($(window).height() - winHeight) / 2,
			winOptions   = 'width='  + winWidth  + ',height=' + winHeight + ',top='    + winTop    + ',left='   +winLeft;
			window.open(openLink,'Share',winOptions);
			return false;
		}else if(shareName == 'linkedin'){
			var openLink = 'http://www.linkedin.com/shareArticle?mini=true&url='+encodeURIComponent(share_url)+'&title='+encodeURIComponent(title);
			winWidth    = 650;
			winHeight   = 450;
			winLeft     = ($(window).width()  - winWidth)  / 2,
			winTop      = ($(window).height() - winHeight) / 2,
			winOptions   = 'width='  + winWidth  + ',height=' + winHeight + ',top='    + winTop    + ',left='   +winLeft;
			window.open(openLink,'Share',winOptions);
			return false;
		}else if(shareName == 'pinterest'){
			var openLink = 'http://pinterest.com/pin/create/button/?url='+encodeURIComponent(share_url)+'&media='+encodeURIComponent(imgcontent_url)+'&description='+encodeURIComponent(descpp);
			winWidth    = 650;
			winHeight   = 450;
			winLeft     = ($(window).width()  - winWidth)  / 2,
			winTop      = ($(window).height() - winHeight) / 2,
			winOptions   = 'width='  + winWidth  + ',height=' + winHeight + ',top='    + winTop    + ',left='   +winLeft;
			window.open(openLink,'Share',winOptions);
			return false;
		}
		else if(shareName == 'whatsapp'){
			if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
				var text = $("#share_content").text();
				var url = '<?php echo current_url() ?>';
				var message = encodeURIComponent(text) + " - " + encodeURIComponent(url);
				var whatsapp_url = "whatsapp://send?text=" + message;
				window.location.href = whatsapp_url;
			} else {
				alert("Please use an Mobile Device to Share this Article");
			}
		}
		else if(shareName == 'tumblr'){
			var openLink = 'http://www.tumblr.com/share/link?url='+encodeURIComponent(share_url)+'&description=' + encodeURIComponent(descpp) +'&img='+encodeURIComponent(image_url)+'&image_sharer=1';
			winWidth    = 650;
			winHeight   = 450;
			winLeft     = ($(window).width()  - winWidth)  / 2,
			winTop      = ($(window).height() - winHeight) / 2,
			winOptions   = 'width='  + winWidth  + ',height=' + winHeight + ',top='    + winTop    + ',left='   +winLeft;
			window.open(openLink,'Share',winOptions);
			return false;
		}
	});
});
</script>
<?php $this->load->view('vwHeader');?>
    <!--slider area start-->
    <section class="slider_section slider_section_three">
        
       <div class="slider_area owl-carousel">
            <?php $this->banners->show_collection(6, 5, '3_box_row');?>
        </div>
    </section>
    <!--slider area end-->
    
     <style type="text/css">.product_color3 .col-lg-3 {
    margin-bottom: 30px;
}</style>
         
     <!--banner area start-->
     <?php if( is_array($category) && count($category)){ ?>
    <div class="banner_area banner_three pb-70">
        <div class="container">
            
            <div class="row">
                <div class="col-12">
                    <div class="section_title title_style3">
                        <h3>Featured Category</h3>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <?php foreach($category as $arrCat){?>
                <div class="col-lg-3 col-md-6">
                    <div class="single_banner">
                        <div class="banner_thumb">
                            <a href="shop.html"><img src="<?php echo base_url('uploads/images/full/'.$arrCat->image1);?>" alt=""></a>
                            <div class="banner_text">
                               <a href="<?php echo site_url($arrCat->slug);?>"><?php echo $arrCat->name;?></a>
                            </div>
                        </div>
                       
                    </div>
                </div>
                <?php  } ?>                 
            </div>
            
        </div>
    </div>
    <?php  } ?> 
    <!--banner area end-->

   <!--new product area start-->
    <section class="product_area product_three mb-40">
        <div class="container">
            <h2 class="cat_subcat_main_heading mt-50">
                <span class="span pb-15">Shop</span>
            </h2>
            <h2 class="cat_subcat_main_heading mb-30">
            <div class="morePages"><a href="<?php echo base_url();?>/best-sellers">VIEW ALL<i class="ion-ios-arrow-thin-right"></i></a></div>
            Best Sellers
            </h2>
             
            <div class="product_color3">
                <div class="home_categories_slider owl-carousel">
                 
                    <?php foreach($arrBestSellerProducts_P as $product){
                  $photo  = theme_img('no_picture.png', 'No Image Available');
                  $product->images   = array_values((array)json_decode($product->images));
                  if(!empty($product->images[0])){
                    $primary    = $product->images[0];
                    foreach($product->images as $photo){
                      if(isset($photo->primary)){
                        $primary    = $photo;
                      }
                    }

                    $photo  = '<img src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'" style="" />';
                  }?>
                    <div class="pLeftRight-14">
                    
                        <div class="single_product">
                            <div class="price-container">
                                <div class="wishlist_btn mt-10 ml-10">
                                    <?php if($this->Customer_model->is_logged_in(false, false)){?>
                                            <a onclick="add_to_wishlist(<?php echo $product->id;?>);" href="javascript:;" title="Wishlist">
                                            <i class="ion-android-favorite-outline"></i></a>
                                    <?php
                                        }else{
                                    ?>
                                            <a  href="<?php echo site_url('secure/login');?>" title="Wishlist" data-toggle="modal" data-target="#myLoginModal">
                                            <i class="ion-android-favorite-outline"></i></a>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="product_thumb">
                                <a class="primary_img" href="<?php echo base_url($product->slug); ?>"><?php echo $photo;?></a>
                                <a class="secondary_img" href="<?php echo base_url($product->slug); ?>"><?php echo $photo;?></a>
                                
                               
                            </div>
                            <div class="product_content">
                                <div class="product_name">
                                   <h4><a href="<?php echo base_url($product->slug); ?>" title="<?php echo $product->name;?>"><?php echo $product->name;?></a></h4>
                                </div>
                                 
                                <div class="price-container">
                                     <div class="price_box">
                                         <small><?php echo $product->product_size; ?></small>
                                          <div class="priceArea">
                                          <?php if($product->saleprice > 0):  ?>

                                            <span class="priceArea_currentP"><i class="fa fa-inr"></i><?php echo $product->saleprice; ?></span>
                                            <span class="priceArea_oldP"><i class="fa fa-inr"></i><?php echo $product->price; ?></span>
                                            <!-- <span class="priceArea_offP"><i class="fa fa-inr"></i>41 off</span> -->
                                          <?php else: ?> 

                                            <span class="priceArea_currentP"><i class="fa fa-inr"></i><?php echo $product->price; ?></span>

                                          <?php endif;?>     

                                        </div>

                                    </div>
                                </div>
                                <form action="cart/add_to_cart_ajax" class="" id="add-to-cart_<?=$product->id?>" name="add-to-cart_<?=$product->id?>" method="post">   
                                    <input type="hidden" name="cartkey" value="<?php echo $this->session->flashdata('cartkey');?>" />
                                    <input type="hidden" name="id" value="<?php echo $product->id; ?>"/>
                                    <input type="hidden" value="1" id="quantity1" class="quantity1" name="quantity">
                                    
                                </form>
                                <button onclick="addToCart(<?php echo $product->id;?>);" title="Add to Bag" class="btn add_to_bag mt-20">
                                    <span>Add to Bag</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <!-- <div class="morePagesMobile"><a href="#" class="btn moreMobileBtn">VIEW ALL</a></div> -->
        </div>
    </section>
    <!--new product area end-->

        <!--Categories start-->
    <section class="product_area product_three mb-40">
        <div class="container">
            <h2 class="cat_subcat_main_heading mt-50">
                <span class="span pb-15">Categories</span>
            </h2>
            <h2 class="cat_subcat_main_heading mb-30"><!-- <div class="morePages"><a href="#">VIEW ALL<i class="ion-ios-arrow-thin-right"></i></a></div> -->Discover More</h2>
            <div class="product_color3">
                <div class="home_categories_slider owl-carousel">
                    
                    <?php  
                        if(isset($discover_categories[0])){
                             $i=0;       
                            foreach ($discover_categories[0] as $cat)
                            {

                                if($i==1)continue;
                                if(count($discover_categories[$cat->id]) > 0)
                                {

                                    foreach ($discover_categories[$cat->id] as $cat1)
                                    {
                                    ?>                       

                                        <div class="pLeftRight-14">
                                            <div class="single_product">
                                                <div class="product_thumb">
                                                    <?php if($cat1->image != ''){ ?>
                                                        <a class="primary_img" href="<?php echo site_url($cat1->slug);?>"><img src="<?php echo base_url('uploads/images/medium/'.$cat1->image.''); ?>"/>
                                                        </a>
                                                    <?php }else{ ?>
                                                        <a class="primary_img noImagePic" href="<?php echo site_url($cat1->slug);?>"><img src="<?php echo base_url('uploads/home_static_image/no_picture.png'); ?>"/>
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                                <div class="product_content">
                                                    <div class="product_name">
                                                       <h4><a href="<?php echo site_url($cat1->slug);?>" title="<?php echo $cat1->name;?>"><?php echo $cat1->name;?></a></h4>
                                                    </div>
                                                    <div class="morePages morePagesSlider"><a href="<?php echo site_url($cat1->slug);?>">VIEW ALL<i class="ion-ios-arrow-thin-right"></i></a></div>
                                                </div>
                                            </div>
                                        </div>

                                    <?php 
                                    }

                                }
                                $i++; 
                            }

                        }
                    ?>
  
                    <div class="pLeftRight-14">
                        <div class="single_product">
                            <div class="product_thumb">
                                <a class="primary_img" href="https://www.natuur.in/home-care-products"><img src="<?php echo base_url('uploads/images/medium/d4b4f3480ae4a4bcebe2722b02659ca7.jpg'); ?>"/>
                                </a>
                            </div>
                            <div class="product_content">
                                <div class="product_name">
                                   <h4><a href="https://www.natuur.in/home-care-products" title="HOME CARE">HOME CARE</a></h4>
                                </div>
                                <div class="morePages morePagesSlider"><a href="https://www.natuur.in/home-care-products">VIEW ALL<i class="ion-ios-arrow-thin-right"></i></a></div>
                            </div>
                        </div>
                    </div>

                    <div class="pLeftRight-14">
                        <div class="single_product">
                            <div class="product_thumb">
                                <a class="primary_img" href="https://www.natuur.in/speciality-foods"><img src="<?php echo base_url('uploads/images/medium/5f4cf55d4e27e36da687d91266976ebc.jpg'); ?>"/>
                                </a>
                            </div>
                            <div class="product_content">
                                <div class="product_name">
                                   <h4><a href="https://www.natuur.in/speciality-foods" title="Speciality Foods">SPECIALITY FOODS</a></h4>
                                </div>
                                <div class="morePages morePagesSlider"><a href="https://www.natuur.in/speciality-foods">VIEW ALL<i class="ion-ios-arrow-thin-right"></i></a></div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </section>
    <!--Categories end-->


    <!--new product area start-->
   <!--  <section class="product_area product_three mt-10 mb-40">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section_title title_style3">
                        <h3>Our Products</h3>
                    </div>
                    
                </div>
            </div>
            <div class="product_wrapper product_color3">
               
                   
                        <div class="product_title">
                             <h3 style="font-size: 20px;">PERSONAL CARE</h3>
                        </div>
                        <div class="row product_slick_row4">
                <?php foreach($product1 as $product){
                  $photo  = theme_img('no_picture.png', 'No Image Available');
                  $product->images   = array_values((array)json_decode($product->images));
                  if(!empty($product->images[0])){
                    $primary    = $product->images[0];
                    foreach($product->images as $photo){
                      if(isset($photo->primary)){
                        $primary    = $photo;
                      }
                    }

                    $photo  = '<img src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'" style="" />';
                  }?>
                    <div class="col-lg-3">
                        <div class="single_product">
                            <div class="product_thumb">
                                <a class="primary_img" href="<?php echo base_url($product->slug); ?>"><?php echo $photo;?></a>
                                <a class="secondary_img" href="<?php echo base_url($product->slug); ?>"><?php echo $photo;?></a>
                               
                               
                            </div>
                            <div class="product_content">
                                <div class="product_name">
                                   <h4><a href="<?php echo base_url($product->slug); ?>"><?php echo $product->name;?></a></h4>
                                </div>
                                
                                <div class="price-container">
                                     <div class="price_box">
                                        
                                        <?php if($product->saleprice > 0):
                                                  ?>
                                                <span class="current_price"><i class="fa fa-inr"></i> <?php echo $product->saleprice; ?></span>
                                                <span class="old_price"><i class="fa fa-inr"></i><?php echo $product->price; ?></span>
                                                <?php else: ?>
                                                <span class="current_price"><i class="fa fa-inr"></i><?php echo $product->price; ?></span><?php 
                                                   endif;
                                                 ?>     
                                    </div>
                                    <div class="wishlist_btn">
                                    <?php if($this->Customer_model->is_logged_in(false, false)){?>
                                    
                                                <a onclick="add_to_wishlist(<?php echo $product->id;?>);" href="javascript:;" title="Wishlist">
                                                <i class="ion-android-favorite-outline"></i></a>
                                            <?php
                                            }else{?>
                                                <a  href="<?php echo site_url('secure/login');?>" title="Wishlist">
                                                <i class="ion-android-favorite-outline"></i></a>
                                            <?php
                                            }?>
                                    
                                       
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>
                    <?php } ?>
                        </div>
                         <div class="product_title">
                             <h3 style="font-size: 20px;">HOME CARE</h3>
                        </div>
                        <div class="row product_slick_row4">
                            <?php foreach($product2 as $product){
                  $photo  = theme_img('no_picture.png', 'No Image Available');
                  $product->images   = array_values((array)json_decode($product->images));
                  if(!empty($product->images[0])){
                    $primary    = $product->images[0];
                    foreach($product->images as $photo){
                      if(isset($photo->primary)){
                        $primary    = $photo;
                      }
                    }

                    $photo  = '<img src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'" style="" />';
                  }?>
                    <div class="col-lg-3">
                        <div class="single_product">
                            <div class="product_thumb">
                                <a class="primary_img" href="<?php echo base_url($product->slug); ?>"><?php echo $photo;?></a>
                                <a class="secondary_img" href="<?php echo base_url($product->slug); ?>"><?php echo $photo;?></a>
                               
                               
                            </div>
                            <div class="product_content">
                                <div class="product_name">
                                   <h4><a href="<?php echo base_url($product->slug); ?>"><?php echo $product->name;?></a></h4>
                                </div>
                                 
                                <div class="price-container">
                                     <div class="price_box">
                                        
                                        <?php if($product->saleprice > 0):
                                                  ?>
                                                <span class="current_price"><i class="fa fa-inr"></i> <?php echo $product->saleprice; ?></span>
                                                <span class="old_price"><i class="fa fa-inr"></i><?php echo $product->price; ?></span>
                                                <?php else: ?>
                                                <span class="current_price"><i class="fa fa-inr"></i><?php echo $product->price; ?></span><?php 
                                                   endif;
                                                 ?>     
                                    </div>
                                    <div class="wishlist_btn">
                                    <?php if($this->Customer_model->is_logged_in(false, false)){?>
                                    
                                                <a onclick="add_to_wishlist(<?php echo $product->id;?>);" href="javascript:;" title="Wishlist">
                                                <i class="ion-android-favorite-outline"></i></a>
                                            <?php
                                            }else{?>
                                                <a  href="<?php echo site_url('secure/login');?>" title="Wishlist">
                                                <i class="ion-android-favorite-outline"></i></a>
                                            <?php
                                            }?>
                                    
                                       
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>
                    <?php } ?>
                        </div>
                   
                        <div class="product_title">
                             <h3 style="font-size: 20px;">SPECIALITY FOODS</h3>
                        </div> 
                        <div class="row product_slick_row4">
                            <?php foreach($product3 as $product){
                  $photo  = theme_img('no_picture.png', 'No Image Available');
                  $product->images   = array_values((array)json_decode($product->images));
                  if(!empty($product->images[0])){
                    $primary    = $product->images[0];
                    foreach($product->images as $photo){
                      if(isset($photo->primary)){
                        $primary    = $photo;
                      }
                    }

                    $photo  = '<img src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'" style="" />';
                  }?>
                    <div class="col-lg-3">
                        <div class="single_product">
                            <div class="product_thumb">
                                <a class="primary_img" href="<?php echo base_url($product->slug); ?>"><?php echo $photo;?></a>
                                <a class="secondary_img" href="<?php echo base_url($product->slug); ?>"><?php echo $photo;?></a>
                               
                               
                            </div>
                            <div class="product_content">
                                <div class="product_name">
                                   <h4><a href="<?php echo base_url($product->slug); ?>"><?php echo $product->name;?></a></h4>
                                </div>
                                 
                                <div class="price-container">
                                     <div class="price_box">
                                        
                                        <?php if($product->saleprice > 0):
                                                  ?>
                                                <span class="current_price"><i class="fa fa-inr"></i> <?php echo $product->saleprice; ?></span>
                                                <span class="old_price"><i class="fa fa-inr"></i><?php echo $product->price; ?></span>
                                                <?php else: ?>
                                                <span class="current_price"><i class="fa fa-inr"></i><?php echo $product->price; ?></span><?php 
                                                   endif;
                                                 ?>     
                                    </div>
                                    <div class="wishlist_btn">
                                    <?php if($this->Customer_model->is_logged_in(false, false)){?>
                                    
                                                <a onclick="add_to_wishlist(<?php echo $product->id;?>);" href="javascript:;" title="Wishlist">
                                                <i class="ion-android-favorite-outline"></i></a>
                                            <?php
                                            }else{?>
                                                <a  href="<?php echo site_url('secure/login');?>" title="Wishlist">
                                                <i class="ion-android-favorite-outline"></i></a>
                                            <?php
                                            }?>
                                    
                                       
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>
                    <?php } ?>
                        </div>
                   
                         <div class="product_title">
                             <h3 style="font-size: 20px;">GIFTS</h3>
                        </div> 
                        <div class="row product_slick_row4">
                            <?php foreach($product4 as $product){
                  $photo  = theme_img('no_picture.png', 'No Image Available');
                  $product->images   = array_values((array)json_decode($product->images));
                  if(!empty($product->images[0])){
                    $primary    = $product->images[0];
                    foreach($product->images as $photo){
                      if(isset($photo->primary)){
                        $primary    = $photo;
                      }
                    }

                    $photo  = '<img src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'" style="" />';
                  }?>
                    <div class="col-lg-3">
                        <div class="single_product">
                            <div class="product_thumb">
                                <a class="primary_img" href="<?php echo base_url($product->slug); ?>"><?php echo $photo;?></a>
                                <a class="secondary_img" href="<?php echo base_url($product->slug); ?>"><?php echo $photo;?></a>
                               
                               
                            </div>
                            <div class="product_content">
                                <div class="product_name">
                                   <h4><a href="<?php echo base_url($product->slug); ?>"><?php echo $product->name;?></a></h4>
                                </div>
                                 
                                <div class="price-container">
                                     <div class="price_box">
                                        
                                        <?php if($product->saleprice > 0):
                                                  ?>
                                                <span class="current_price"><i class="fa fa-inr"></i> <?php echo $product->saleprice; ?></span>
                                                <span class="old_price"><i class="fa fa-inr"></i><?php echo $product->price; ?></span>
                                                <?php else: ?>
                                                <span class="current_price"><i class="fa fa-inr"></i><?php echo $product->price; ?></span><?php 
                                                   endif;
                                                 ?>     
                                    </div>
                                    <div class="wishlist_btn">
                                    <?php if($this->Customer_model->is_logged_in(false, false)){?>
                                    
                                                <a onclick="add_to_wishlist(<?php echo $product->id;?>);" href="javascript:;" title="Wishlist">
                                                <i class="ion-android-favorite-outline"></i></a>
                                            <?php
                                            }else{?>
                                                <a  href="<?php echo site_url('secure/login');?>" title="Wishlist">
                                                <i class="ion-android-favorite-outline"></i></a>
                                            <?php
                                            }?>
                                    
                                       
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>
                    <?php } ?>
                        </div>
                   
               
            </div>
        </div>
    </section> -->
    <!--new product area end-->
    
    <!--banner fullwidth area start-->
    <div class="banner_fullwidth">
        <div class="container-fluid p-0">
            <div class="row no-gutters">
                <div class="col-12">
                    <div class="banner_thumb">
                        <a href="javascript:;"><?php $this->banners->show_collection(10, 1, 'default');?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--banner fullwidth area end-->
    
    </section>
    <!--Natuur – The world of Natuur - Enchantment – A symbiosis with nature  end-->
    <!--Customer review start-->
    <section class="product_area product_three mb-40">
        <div class="container">
            <h2 class="cat_subcat_main_heading mt-50">
                <span class="span pb-15">Trending Now</span>
            </h2>
            <h2 class="cat_subcat_main_heading mb-30">
            <div class="morePages"><a href="#">VIEW ALL<i class="ion-ios-arrow-thin-right"></i></a></div>
            Customer Reviews
            </h2>
        </div>
        <div class="customer_review">
            <div class="container">
                <div class="home_categories_slider owl-carousel">
                    <div class="mLeftRight-14 rev_section">
                        <div class="quote_div mt-10">
                            <span class="quote"><i class="fa fa-quote-left"></i></span>
                        </div>
                        <div class="customer_review_content">
                            <p>"I have been using the Mashobra Honey cleanser for 3 years and cannot complain. My skin feels replenished and fresh. Also, it provides the right moisture and doesn't leave the skin super dry. Works best during the winter season." </p>
                        </div>
                        <div class="customer-name">Sravan<span class="customer-date">Mumbai May 2021</span></div>
                    </div>
                    <div class="mLeftRight-14 rev_section">
                        <div class="quote_div mt-10">
                            <span class="quote"><i class="fa fa-quote-left"></i></span>
                        </div>
                        <div class="customer_review_content">
                            <p>"I have been using the Mashobra Honey cleanser for 3 years and cannot complain. My skin feels replenished and fresh. Also" </p>
                        </div>
                        <div class="customer-name">Sravan<span class="customer-date">Mumbai May 2021</span></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Customer review start-->

    <!--deals section area css here-->
    <section class="deals_section deals_section_three">
        <div class="container">
            <div class="deals_inner_three">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-12">
                        <div class="deals_carousel owl-carousel">
                            <div class="product_caption">
                               
                                <div class="product_title">
                                    <h3>Natuur is born in the heart, not in any factory!</h3>
                                </div>
                                <div class="product_desc">
                                    <p>
                                       Natuur is the dutch word for Nature. Nature, in the broadest sense, is the natural, physical, or material world or universe. "Nature" can refer to the phenomena of the physical world, and also to life in general. The word nature is derived from the Latin word natura, or "essential qualities, innate disposition", and in ancient times, literally meant "birth". Natuur was started by a mom, who loves the world and wants to give it a nurturing legacy.   Natural body and home care products made from edible products from your kitchen are very much possible to be made. The basic thumb rule is - what you cannot eat, you should not be using it on your body or for cleaning your home. </p>
                                </div>
                                
                            </div>
                           
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <div class="deals_banner">
                           <iframe width="560" height="315" src="https://www.youtube.com/embed/FWnG1mgWueo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--deals section area css end-->
    
    <!-- The Current Workshop Start -->
    <section class="cWorkshop">
        <div class="container">
            <div class="currentWorkshop">
                <h2 class="cat_subcat_main_heading mt-50">
                    <span class="span pb-15">Workshop</span>
                </h2>
                <h2 class="cat_subcat_main_heading mb-30">
                    <div class="morePages"><a href="#">Go to Workshop<i class="ion-ios-arrow-thin-right"></i></a></div>
                    NATUUR - A Panacea for Combination Skin
                </h2>
                <div class="row align-items-center">
                    <div class="col-md-6 col-sm-6 col-12 workshopVideo">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/Thb93Von3QE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        
                    </div>
                    <div class="col-md-6 col-sm-6 col-12 workshopDetails">
                        
                        <h3>The Current Workshop</h3>
                        <p>Our Natural Makeup Collection is inspired from re-introducing the age-old rituals to our discerning customers with diverse complexions, features and skin types. Discover the Som Rasa Skin Tints, Noor Nikhaar Cheek Tints, Madhu Rasa Tinted Lip Serums, Gulaab Khaas Kajals and more, to enhance your beauty with natural, skin benefitting Makeup.Our products are certified 100% natural and inspired by Ayurveda recipes by the ministry of AYUSH. We conform to the highest quality control standards.</p>
                    </div>
                    <div class="col-md-12 col-sm-12 col-12">
                        <div class="workshopBtn">
                            <a href="#" class="btn btnCalendar">Calendar for three months</a>
                            <a href="#" class="btn btnRegistration">Registration</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="morePagesMobile"><a href="#" class="btn moreMobileBtn">Go to Workshop</a></div>
        </div>
    </section>
    <!-- The Current Workshop End -->
    
    <!--Get In Touch Section start-->
    <section class="Keep_in_touch">
        <div class="container">
            <div class="middleRow">
                <h2 class="cat_subcat_main_heading"><span class="span pb-15">Newsletter</span></h2>
                <h2 class="cat_subcat_main_heading mb-30">Get In Touch</h2>
                <div class="field custom_name">        
                    <div class="control controlInput">
                        <div><input name="custom_name" type="text" id="custom_name" placeholder="NAME" class="form-control inputName"data-validate="{required:true}"></div>
                        <div class="inputIcon"><input name="custom_name" type="text" id="custom_name" placeholder="EMAIL" class="form-control inputEmail"data-validate="{required:true}"><i class="ion-ios-arrow-right" title="Suscribe"></i></div>
                    </div>
                </div>
            </div>
            <div class="keepAttributes">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-12 attributePart">
                        <div class="iconImage"><img src="<?php echo base_url('uploads/home_static_image/leaf-icon.png'); ?>"/></div>
                        <h3>Made In India</h3>
                        <p>Our products are certified 100% natural and inspired by Ayurveda recipes by the ministry of AYUSH. We conform to the highest quality control standards.</p>
                    </div>
                    <div class="col-md-3 col-sm-3 col-12 attributePart">
                        <div class="iconImage"><img src="<?php echo base_url('uploads/home_static_image/paw-icon.png'); ?>"/></div>
                        <h3>Shipping Globally</h3>
                        <p>Our products are non-toxic, not tested on animals, and unadulterated without the use of parabens, sulphates, chemical sunscreens, and synthetic fragrances.</p>
                    </div>
                    <div class="col-md-3 col-sm-3 col-12 attributePart">
                        <div class="iconImage"><img src="<?php echo base_url('uploads/home_static_image/box-icon.png'); ?>"/></div>
                        <h3>Cruelty Free</h3>
                        <p>On all International orders of INR 9999/- and above, and on all Domestic orders*</p>
                        <p><a href="#">T&C Apply.</a></p>
                    </div>
                    <div class="col-md-3 col-sm-3 col-12 attributePart">
                        <div class="iconImage"><img src="<?php echo base_url('uploads/home_static_image/hand-icon.png'); ?>"/></div>
                        <h3>Authentic Ingredients</h3>
                        <p>We celebrate the rich knowledge and heritage of India through our products, sourcing rare ingredients from different parts of the country.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Get In Touch Section end-->
    
     <!--blog area start-->
    
    <!--blog area end-->
    
    <!--custom product css here-->
    
    <!--custom product css end-->

    <!--banner area start-->
    <div class="banner_area banner_three_column2">
        <div class="container-fluid p-0">
            <div class="row no-gutters">
                <div class="col-lg-6 col-md-6">
                    <div class="single_banner">
                        <div class="banner_thumb">
                            <a href="shop.html"><?php $this->banners->show_collection(10, 1, 'default');?></a>
                        </div>
                    </div>
                </div>  
                <div class="col-lg-6 col-md-6">
                    <div class="single_banner">
                        <div class="banner_thumb">
                            <a href="shop.html"><?php $this->banners->show_collection(10, 1, 'default');?></a>
                        </div>
                    </div>
                </div>      
            </div>
        </div>
    </div>
    <!--banner area end-->

<?php
function theme_img($uri, $tag=false){
    if($tag){
        return '<img src="'.theme_url('assets/assets/images/'.$uri).'" alt="'.$tag.'">';
    }else{
        return theme_url('assets/assets/images/'.$uri);
    }
}
function theme_url($uri){
    $CI =& get_instance();
    return $CI->config->base_url('/'.$uri);
}

$this->load->view('vwFooter1'); ?>   
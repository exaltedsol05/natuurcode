<?php $this->load->view('vwHeader');?>
<script>
$(document).ready(function(){
	
	$('.delete_address').click(function(){
		if($('.delete_address').length > 1)
		{
			if(confirm('Are you sure you want to delete this address?'))
			{
				$.post("<?php echo site_url('secure/delete_address');?>", { id: $(this).attr('rel') },
					function(data){
						$('#address_'+data).remove();
						$('#address_list .my_account_address').removeClass('address_bg');
						$('#address_list .my_account_address:even').addClass('address_bg');
					});
			}
		}
		else
		{
			alert('You Must leave at least 1 address in the Address Manager.');
		}	
	});
	
	$('.edit_address').click(function(){
		$.post('<?php echo site_url('secure/address_form'); ?>/'+$(this).attr('rel'),
			function(data){
				
				$('#address-form-container').html(data);
				$('#myModal').modal('show');
			}
		);
	});
});


function set_default(address_id, type)
{
	$.post('<?php echo site_url('secure/set_default_address') ?>/',{id:address_id, type:type});
}


</script>
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner myAccount_breadcum">
			<ul class="list-inline list-unstyled">
				<li><a href="<?php echo base_url();?>">Home</a></li>
				<li><a href="#" class="active">My Address</a></li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->

<div class="body-content outer-top-xs">
	<div class='container'>
		<div class="row">
			<div class="col-md-12">
				<?php if ($this->session->flashdata('message')):?>
				<div class="alert alert-info">
					<a class="close" data-dismiss="alert">×</a>
					<?php echo $this->session->flashdata('message');?>
				</div>
				<?php endif;?>
			
				<?php if ($this->session->flashdata('error')):?>
				<div class="alert alert-danger">
					<a class="close" data-dismiss="alert">×</a>
					<?php echo $this->session->flashdata('error');?>
				</div>
				<?php endif;?>
			
				<?php if (!empty($error)):?>
				<div class="alert alert-danger">
					<a class="close" data-dismiss="alert">×</a>
					<?php echo $error;?>
				</div>
				<?php endif;?>
			</div>
		
    	<div class="col-md-3 myaccount_left">
        	<?php $this->load->view('user_account_sidebar');?>
        </div>    	
		<div class="col-md-9 myaccount_right">
        	 <div class="row">
				<div class="col-sm-8 myDashboard">
					<h3>ADDRESS BOOK</h3>
				</div>
				<div class="col-sm-4" style="text-align:right;">
					<input type="button" class="btn btn-sm edit_address btn-primary" rel="0" value="Add Address"/>
				</div>
		    </div>
            <hr> 
            <div class="row">
			<div class="col-sm-12" id='address_list'>
			<?php if(count($addresses) > 0):?>
				<table class="table table-bordered table-striped">
			<?php
			$c = 1;
				foreach($addresses as $a):?>
					<tr id="address_<?php echo $a['id'];?>">
						<td>
							<?php
							$b	= $a['field_data'];
							echo format_address($b, true);
							?>
						</td>
						<td>
							<div class="row">
								<div class="col-sm-12">
									<div class="btn-group pull-right myBtn">
										<input type="button" class="btn btn-sm edit_address green_btn" rel="<?php echo $a['id'];?>" value="Edit" />
										<input type="button" class="btn btn-sm btn-danger delete_address" rel="<?php echo $a['id'];?>" value="Delete" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="billing_shipping_section">
										<span><input type="radio" id="default_billing<?php echo $a['id'] ?>" name="bill_chk" onclick="set_default(<?php echo $a['id'] ?>, 'bill')" <?php if($customer['default_billing_address']==$a['id']) echo 'checked="checked"'?> /> <label for="default_billing<?php echo $a['id'] ?>">Default Billing</label></span><span><input type="radio" id="default_shipping<?php echo $a['id'] ?>" name="ship_chk" onclick="set_default(<?php echo $a['id'] ?>,'ship')" <?php if($customer['default_shipping_address']==$a['id']) echo 'checked="checked"'?>/> <label for="default_shipping<?php echo $a['id'] ?>">Default Shipping</label></span>
									</div>
								</div>
							</div>
						</td>
					</tr>
				<?php endforeach;?>
				</table>
			<?php else:?>
			<p>No address in your account.</p>
			<?php endif;?>
			</div>
		
			
			
			<hr>
            <div class="row">
            	
            </div>
            
            <hr>  
            
        </div>
    
    </div>   
</div>
</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" id="address-form-container">
      
	</div>

  </div>
</div>



<?php 
$this->load->view('vwFooter');
?>
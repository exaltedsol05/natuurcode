<?php $this->load->view('vwHeader');
//$top_brand=$_GET['top_brand'];

if($top_brand){
	$css='style="display:none;"';
	$class='col-xs-12 col-sm-12 col-md-12';
}else{
	$css='style="display:block;"';
	$class='col-xs-12 col-sm-9 col-md-10';
}?>
<style>#showMenu{display:none;}
.category_add-button{width: 115px;background: #FFFFFF;line-height: 14px;font-size: 14px;color: #511E3E;border: 1px solid #CCCCCC;text-transform: none;border-radius:3px;}
.category_add-button:hover{background: #e68824;color: #FFFFFF;}</style>
<script>
window.onload = function(){
	$('.product').equalHeights();
}
</script>

<section class="container">
    <div class="row">
    	<div class="col-xs-12 col-sm-3 col-md-2 product_landing_left_column" <?php echo $css;?>>
        	<div class="filter-box">
			    <?php if(count($categories[$category->id]) > 0){?>
					<div class="filter-col choose_category">
						<h2>CHOOSE CATEGORIES</h2>
						<div class="show-filter">
							<div class="filter">
								<ul><?php 
									foreach($categories[$category->id] as $subcategory){?>
										<li><?php
											$prditems = $this->categoryside->category_items($subcategory->id); ?>
											<a href="<?php echo site_url($subcategory->slug); ?>"><?php echo $subcategory->name;?> (<?php echo $prditems; ?>)</a>
										</li><?php 
									} ?>
								</ul>
							</div>
						</div>
					</div>	
				<?php }else{ ?>
					<div class="filter-col choose_category">
						<h2>CHOOSE CATEGORIES</h2>
						<div class="show-filter">
							<div class="filter">
								<ul><?php 
									foreach($categories[$category->parent_id] as $subcategory){?>
										<li><?php
											$prditems = $this->categoryside->category_items($subcategory->id); ?>
											<a href="<?php echo site_url($subcategory->slug); ?>"><?php echo $subcategory->name;?> (<?php echo $prditems; ?>)</a>
										</li><?php 
									} ?>
								</ul>
							</div>
						</div>
					</div><?php
				} ?>
                <div class="filter-col">
                    <h2>REFINE YOUR SEARCH</h2>
                    <h3 onClick="showFilter();">REFINE YOUR SEARCH</h3>
                    <div class="show-filter" id="showFilter1">
						<!-- if product is available then show in left bar --><?php 
						//if(count($products) > 0){ 
						if(true){ ?>
							<div class="filter" id="shop_by_brand">
								<h4>Shop By Brand</h4>
								<ul class="brandOffBox"><?php 
									foreach($arrBrand as $brand){ 
										$brandArr = explode(',', $brands);
										$check = '';
										if(in_array($brand->id, $brandArr)){
											$check = 'checked'; 
										}?>
										<li>
											<input name='dummy_brand' type="checkbox" <?php echo $check; ?> >
											<label for="brand_<?php echo $brand->id;?>"><input name='brand[]' id="brand_<?php echo $brand->id;?>" class="brand_type" type="checkbox" value="<?php echo $brand->id;?>" <?php echo $check; ?> style="margin-left:-15px;"><?php echo $brand->name;?></label>
										</li><?php 
									} ?>
								</ul>
							</div>
							<div class="filter" id="shop_by_manufacturer" style="display:none;">
								<h4>Manufacturer</h4>
								<ul class="manufacturerTypeBox"><?php 
									foreach($arrManufacturer as $manufacturer){ 
										$checkedArr = explode(',', $manufacturers);
										$check = '';
										if(in_array($manufacturer->id, $checkedArr)){
											$check = 'checked'; 
										}?>
										<li>
											<input name='manufacturer[]' id="manufacturer_<?php echo $manufacturer->id;?>" class="manufacturer_type" type="checkbox" value="<?php echo $manufacturer->id;?>" <?php echo $check; ?>
											><label for="manufacturer_<?php echo $manufacturer->id;?>"><?php echo $manufacturer->name;?></label>										   
										</li><?php 
									} ?>
								</ul>
							</div>
							<div class="filter" id="shop_by_book_type">
								<h4>Book Type</h4>
								<ul class="bookTypeBox"><?php 
									foreach($arrBooktype as $book){ 
										$checkedArr = explode(',', $booktype);
										$check = '';
										if(in_array($book->id, $checkedArr)){
											$check = 'checked'; 
										}?>
										<li>
											<input name='dummy_book' class="book_type" type="checkbox" <?php echo $check; ?> >
											<label for="book_<?php echo $book->id;?>" style="margin-left:-24px;"><input name='book[]' id="book_<?php echo $book->id;?>" class="book_type" type="checkbox" value="<?php echo $book->id;?>" <?php echo $check; ?> >&nbsp;<?php echo $book->name;?></label>
										</li><?php 
									} ?>
								</ul>
							</div>
							<div class="filter" id="shop_by_publisher">
								<h4>Publisher</h4>
								<ul class="publisherTypeBox">
									<?php foreach($arrPublishers as $publisher){ 
										$checkedArrP = explode(',', $publishertype);
										$checkp = '';
										
										if(in_array($publisher->id, $checkedArrP)){
											$checkp = 'checked';    
										}?>
										<li>
											<input name='dummy_publisher' class="publisher_type" type="checkbox" <?php echo $checkp; ?> >
											<label for="publisher_<?php echo $publisher->id;?>" style="margin-left:-24px;"><input name='publisher[]' id="publisher_<?php echo $publisher->id;?>" class="publisher_type" type="checkbox" value="<?php echo $publisher->id;?>" <?php echo $checkp; ?> >&nbsp;<?php echo $publisher->name;?></label>
										</li>
									<?php } ?>
								</ul>
							</div><?php 
						} ?>   
                        <div class="filter" id="shop_by_price">
                            <h4>Price Range</h4>
                            <div class="price-range clearfix">
                                <input type="text" id="amount" readonly>
                            </div>
							<div id="slider-range"></div>
                            <div class="price-input clearfix">
                                <div class="price-col">
                                    <input type="text" id="minPrice" value="<?php echo $minPrice;?>" placeholder="200">
                                </div>
                                <div class="price-seprate"></div>
                                <div class="price-col">
                                    <input type="text" id="maxPrice" value="<?php echo $maxPrice;?>" placeholder="200">
                                </div>
                            </div>
                        </div>
                        
                        <div class="filter">
                            <h4>Discount %</h4>
                            <ul class="taxOffBox"><?php
								$taxArr = explode(',', $taxtype);
								for($i=1; $i<=5; $i++){
									if($i==1){
										$taxRange = '1-10';
										$textVal = 'Up to 10%';
									}else if($i == 2){
										$taxRange = '11-20';
										$textVal = '11% - 20%';
									}else if($i == 3){
										$taxRange = '21-30';
										$textVal = '21% - 30%';
									}else if($i == 4){
										$taxRange = '31-40';
										$textVal = '31% - 40%';
									}else if($i == 5){
										$taxRange = '41-50';
										$textVal = '41% - 50%';
									} ?>
									<li>
										<input name='dummy_tax' type="checkbox" <?php if(in_array($taxRange, $taxArr)){ echo 'checked';}?> />
										<label for="tax_<?php echo $taxRange; ?>" style="margin-left:-25px;">
                                                <input name='tax' id="tax_<?php echo $taxRange; ?>" class="tax_type" type="checkbox" value="<?php echo $taxRange; ?>" <?php if(in_array($taxRange, $taxArr)){ echo 'checked';}?> >&nbsp;&nbsp;<?php echo $textVal; ?>
                                            </label>
									</li><?php 
								} ?>    
                            </ul>
                        </div>
                        
                        <div class="filter" style="display: none;">
                            <h4>Offers & Arrivals</h4>
                            <ul>
                                <li>
                                    <input type="checkbox" name="book" id="offers1">
                                    <label for="offers1">Products on Offer</label>
                                </li>
                                <li>
                                    <input type="checkbox" name="book" id="offers2">
                                    <label for="offers2">Latest </label>
                                </li>
                                
                            </ul>
                        </div>
                        
                        <div class="filter">
                            <h4>Customer Rating</h4>
                            <ul>
                                <li>
                                    <input type="checkbox" name="book" id="star5">
                                    <label for="star5"><img src="<?=HTTP_IMAGES_PATH;?>star5.png" alt="5 start"></label>
                                </li>
                                <li>
                                    <input type="checkbox" name="book" id="star4">
                                    <label for="star4"><img src="<?=HTTP_IMAGES_PATH;?>star4.png" alt="5 start"></label>
                                </li>
                                <li>
                                    <input type="checkbox" name="book" id="star3">
                                    <label for="star3"><img src="<?=HTTP_IMAGES_PATH;?>star3.png" alt="5 start"></label>
                                </li>
                                <li>
                                    <input type="checkbox" name="book" id="star2">
                                    <label for="star2"><img src="<?=HTTP_IMAGES_PATH;?>star2.png" alt="5 start"></label>
                                </li>
                                <li>
                                    <input type="checkbox" name="book" id="star1">
                                    <label for="star1"><img src="<?=HTTP_IMAGES_PATH;?>star1.png" alt="5 start"></label>
                                </li>
                                                               
                            </ul>
                        </div> 
                        
                        <div class="filter" style="display: none;">
                            <h4>Availability</h4>
                            <ul>
                                <li>
                                    <input type="checkbox" name="book" id="offers3">
                                    <label for="offers3">Exclude Out of Stock</label>
                                </li>
                                                               
                            </ul>
                        </div>
                    </div>                   
                </div>
            </div>
        </div>
    	<div class="<?php echo $class;?>"><?
			if($top_brand){
				$brandname = '';
				foreach($arrBrand as $brand){
					if(isset($_GET['top_brand'])){
						$top_brandid = $_GET['top_brand'];
					}else if(count($this->uri->segment_array()) > 3){
						$top_brandid = $this->uri->segment(3);
					}	
					if($top_brandid == $brand-> id){
						 $brandname = $brand-> name;
						 $brandbanner = $brand->brand_banner;
						 break;
					}
				}
			}
			  
			if(!empty($category->image)): ?>		   
				<div class="top-banner hidden-xs" style="padding:0px;">
					<img style="width:100%;max-height:90px;" src="<?php echo base_url('uploads/images/full/'.$category->image);?>" alt="<?php echo $category->name;?>"/>
				</div><?php 
			endif; 
			
			if($brandbanner){?>
				<div class="top-banner hidden-xs" style="padding:0px;background:none;">
					<img style="width:100%;max-height:90px;background:none;" src="<?php echo base_url('uploads/Brand/full/'.$brandbanner);?>" alt="<?php echo $brandname;?>"/>
				</div><?php
			}?>
			
            <?php $this->breadcrumbs->generate('category'); ?><?php
			
			if($top_brand){?>
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a></li>
                    <li><span><?php echo strtoupper($brandname); ?></span></li>
				</ul><?php
			}?>
            <?php if ($this->session->flashdata('message')):?>
					<div class="alert alert-info">
						<a class="close" data-dismiss="alert">×</a>
						<?php echo $this->session->flashdata('message');?>
					</div>
				<?php endif;?>

				<?php if ($this->session->flashdata('error')):?>
					<div class="alert alert-danger">
						<a class="close" data-dismiss="alert">×</a>
						<?php echo $this->session->flashdata('error');?>
					</div>
				<?php endif;?>

				<?php if (!empty($error)):?>
					<div class="alert alert-danger">
						<a class="close" data-dismiss="alert">×</a>
						<?php echo $error;?>
					</div>
				<?php endif;?>
            <div class="sort-box">
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-10 col-lg-9">
                        <div class="sort-by">
                            <ul>
                                <li>Sort By:</li>
                                <!-- <li class="active"><a href="#">Popularity </a></li> -->
                                <li><a href="<?php echo current_full_url();?>&by=price/desc">High Price</a></li>
                                <li><a href="<?php echo current_full_url();?>&by=price/asc">Low Price </a></li>
                                <!-- <li><a href="#"> Discount % </a></li> -->
                                <!-- <li><a href="#">New Arrivals </a></li> -->
                            </ul>
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-2 col-lg-3">
                        <div class="all-page">
                        	<!-- <select class="form-control" id="sort_products" onchange="window.location='<?php echo site_url(uri_string());?>/'+$(this).val();"> -->
                            <select class="form-control" id="sort_products" onchange="window.location='<?php echo current_full_url();?>'+$(this).val();">
								<option value=''><?php echo 'Default';?></option>
								<option <?php echo(!empty($_GET['by']) && $_GET['by']=='name/asc')?' selected="selected"':'';?> value="&by=name/asc"><?php echo 'Sort by name A to Z';?></option>
								<option <?php echo(!empty($_GET['by']) && $_GET['by']=='name/desc')?' selected="selected"':'';?>  value="&by=name/desc"><?php echo 'Sort by name Z to A';?></option>
								<option <?php echo(!empty($_GET['by']) && $_GET['by']=='price/asc')?' selected="selected"':'';?>  value="&by=price/asc"><?php echo 'Sort by price Low to High';?></option>
								<option <?php echo(!empty($_GET['by']) && $_GET['by']=='price/desc')?' selected="selected"':'';?>  value="&by=price/desc"><?php echo 'Sort by price High to Low';?></option>
							</select>
                        </div>
                    </div>
				</div>
			</div>
            <!-- products display starts here... -->
        	<div class="row product-theme"><?php 
				$product_value_array = array();
				if((count($products) == 0)){?>
					<h2 style="margin:50px 0px; text-align:center;"><?php echo 'There are currently no available products in this category.';?></h2><?php 
				}elseif(count($products) > 0){ 
					$arr_book_filter      = array();
					$arr_instument_filter = array();
					foreach($products as $product){
						
						if($product->product_filter=='1'){
							array_push($arr_book_filter,$product->product_filter);
						}else{
							array_push($arr_instument_filter,$product->product_filter);
						}
										
						$photo  = theme_img('no_picture.png', 'No Image Available');
						$product->images    = array_values($product->images);
						
						if(!empty($product->images[0])){
							$primary    = $product->images[0];
							foreach($product->images as $photo){
								if(isset($photo->primary)){
									$primary    = $photo;
								}
							}

							$photo  = '<img src="'.base_url('uploads/images/thumbnails/'.$primary->filename).'" alt="'.$product->seo_title.'"/>';
						}?>
						
						<form action="<?php echo base_url().'cart/add_to_cart_ajax'?>" id="add-to-cart_<?php echo $product->id;?>" name="add-to-cart_<?php echo $product->id;?>" class="form-horizontal" method="post" accept-charset="utf-8">
							<input type="hidden"  name="id" value="<?php echo $product->id;?>">
							<input type="hidden"  name="quantity" maxlength="12" value="1" title="Qty">
							<div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 product">
								<div class="product-block"><?php 
									$sales_price = $this -> dentkart -> product_price($product);
									$discount=round(100*($product->price-$sales_price)/$product->price);
									if($product->quantity < 1){
										echo '<div class="off-box">Out of Stock</div>';
									}else if($discount){?>
										<div class="off-box"><?php echo $discount;?>% OFF</div><?php 
									} ?>
									<div class="image-box">
										<a href="<?php echo site_url(implode('/', $base_url).'/'.$product->slug); ?>"><?php echo $photo;?></a>
									</div>
									<div class="product-title">
										<a href="<?php echo site_url(implode('/', $base_url).'/'.$product->slug); ?>"><?php echo $product->name;?></a>
									</div>
									<div class="price-box clearfix"><?php
										if(!empty($product->grouped_products)){
											$min_price = $this->dentkart->get_min_price($product->grouped_products);?>
											<div class="total-box"><span style="font-size:14px;">Starting at</span><span> <i class="fa fa-inr"></i><?php echo $min_price; ?></div><?php
											$product_value_array[] = $min_price;
										}else{?>
											 <?php if($sales_price > 0):
													$product_value_array[] = $sales_price;?>
													<div class="mrp-box"><i class="fa fa-inr"></i><?php echo $product->price; ?></div>
													<div class="total-box"><i class="fa fa-inr"></i> <?php echo $sales_price; ?></div>
											<?php else: ?>
												<div class="total-box"><i class="fa fa-inr"></i><?php echo $product->price; ?></div><?php
												$product_value_array[] = $product->price;
											endif;
										}?>
									</div><?php 
									if($product->reward_points_earned){?>
										<div class="reward-box">Earn <?php echo $product->reward_points_earned;?> E-Cash</div><?php  
									}else{?>
										<div class="reward-box" style="visibility:hidden">Earn 2900 E-Cash</div><?php
									} ?>
									<div class="rating-box">
										<ul class="clearfix"><?php
											$all_ret = $this->dentkart->product_rating($product -> id);
											$product_rating = explode("*-*", $all_ret);
											$avg_rating = explode("*-*", $all_ret);
											//print_r($product_rating);

											$star1 	=$product_rating[0];
											$star2 	=$product_rating[1];
											$star3 	=$product_rating[2];
											$star4 	=$product_rating[3];
											$star5 	=$product_rating[4];
											$starall 	=$product_rating[5];
											$avt_rating = (1*($star1) + 2*($star2) +3*($star3) +4*($star4) + 5*($star5))/($starall);
											$avt_rating = number_format((float)$avt_rating, 1, '.', '');
											if(empty($avt_rating)){
												$avt_rating = 0;
											}
											for($i = 0; $i < $avt_rating; $i++){
												echo '<li class="active"><a href="#"></a></li>';
											}

											$dact = 5-$avt_rating;
											for($j = 0; $j < $dact; $j++){
												echo '<li><a href="#"></a></li>';
											}?>
											<li class="rc">(<?php echo $starall; ?> Rating)</li>
										</ul>
									</div>
									<div class="action-block">
										<div class="clearfix">
											<div class="fovarite"><?php
												if($this->Customer_model->is_logged_in(false, false)){?>
													<a title="Add to favorite" href="javascript:;" onclick="add_to_wishlist(<?php echo $product->id;?>);" style="cursor:pointer;">Add to favorite</a><?php
												}else{?>
													<a title="Add to favorite" href="#" data-toggle="modal" data-target="#authentication" style="cursor:pointer;">Add to favorite</a><?php
												}?>
											</div><?php
											if($product->quantity < 1){
												echo '<div class="add-button" style="padding:5px 10px;">';
												echo "<span style='color:red;vertical-align: bottom;'>Out of Stock</span>";
												echo '</div>';
											}else{?><?php
												if(!empty($product->grouped_products)){?>
													<div class="add-button">
														<a href="<?php echo site_url(implode('/', $base_url).'/'.$product->slug); ?>" style="height: 30px;" class="category_add-button" >Add to Cart</a>
													</div><?php	
												}else{?>
													<div class="add-button">
														<button style="height: 30px;" class="category_add-button" type="button" onclick="addToCart(<?php echo $product->id;?>);">Add to Cart</button>
													</div><?php
												}												
											}?>
											<div class="compare"><?php
												if($this->Customer_model->is_logged_in(false, false)){?>
													<a title="Add to Compare" onclick="add_to_compare(<?php echo $product->id;?>);" style="cursor:pointer;">Add to Compare</a><?php
												}else{?>
													<a title="Add to Compare" href="#" data-toggle="modal" data-target="#authentication" style="cursor:pointer;">Add to Compare</a><?php
												}?>	
											</div>
										</div>
									</div>
								</div>
							</div>		
						</form><?php  
					}
				}?>
			</div><?php		
				//get maximum or min value from all products
			  $min_price = min($product_value_array);
			  $max_price = max($product_value_array);
			  echo "<input type='hidden' name='prd_min_price' value='".$min_price."' id='prd_min_price'/>";
			  echo "<input type='hidden' name='prd_max_price' value='".$max_price."' id='prd_max_price'/>";
									  
								        if(count($arr_book_filter)){?>									
											<script>
												document.getElementById("shop_by_book_type").style.display = "block";
												document.getElementById("shop_by_publisher").style.display = "block";
												document.getElementById("shop_by_brand").style.display = "none";
												
											</script>
										<?php }
										
										if(count($arr_instument_filter))
										{?>
											<script>
												document.getElementById("shop_by_brand").style.display = "block";
												
												document.getElementById("shop_by_book_type").style.display = "none";
												document.getElementById("shop_by_publisher").style.display = "none";
											</script>
											
										<?php }?>

									<?php //}?>

			   </div>
				
				
			<div class="pagination-holder">
				<div class="row">									
					<div class="col-xs-12 col-sm-12 text-right">								 
						<?php echo $this->pagination->create_links();?>&nbsp;
					</div>
			    </div><!-- /.row -->
			</div><!-- /.pagination-holder -->	
	    </div>
    </div>   
</section>


<script src="<?php echo HTTP_JS_PATH; ?>jquery-ui.js"></script>
<link href="<?php echo HTTP_CSS_PATH; ?>jquery-ui.css" rel="stylesheet" type="text/css" media="screen"/>
<script type="text/javascript">
	$(document).ready(function(){

        /* $("#minPrice").on('change',function(){
			alert("dsjfhu");
           var minPrice =  $('#minPrice').val();
           var maxPrice =  $('#maxPrice').val();

           url= '<?php echo current_full_url();?>&pricetype='+minPrice+'-'+maxPrice;
            window.location.href = url;
        }); */
        
		$(".manufacturerTypeBox label").click(function(){
			var url= '';
			var values = '';
			$(".manufacturerTypeBox label").each(function(){
				var checkId = $(this).attr('for');
				if($("#"+checkId).is(':checked'))
				{
					var value = $(this).children().val();
					//alert(value);
					values += value+',';
				}
			});
			url= '<?php echo current_full_url();?>&manufacturer='+values;
			window.location.href = url;
		});
		
		$(".brandOffBox label").click(function(){
			var url= '';
			var values = '';
			$(".brandOffBox label").each(function(){
				var checkId = $(this).attr('for');
				if($("#"+checkId).is(':checked'))
				{
					var value = $(this).children().val();
					//alert(value);
					values += value+',';
				}
			});
			url= '<?php echo current_full_url();?>&brand='+values;
			window.location.href = url;
		});
		
		$(".taxOffBox label").click(function(){
			var url= '';
			var values = '';
			$(".taxOffBox label").each(function(){
				var checkId = $(this).attr('for');
				if($("#"+checkId).is(':checked')){
					var value = $(this).children().val();
					//alert(value);
					values += value+',';
				}
			});
			url= '<?php echo current_full_url();?>&taxtype='+values;
			window.location.href = url;
		});

		$(".bookTypeBox label").click(function(){
			var url= '';
			var values = '';
			$(".bookTypeBox label").each(function(){
				var checkId = $(this).attr('for');
				//alert(checkId);
				if($("#"+checkId).is(':checked'))
				{
					var value = $(this).children().val();
					//alert(value);
					values += value+',';
				}
			});

			url= '<?php echo current_full_url();?>&booktype='+values;
			window.location.href = url;
		});
		
		$(".publisherTypeBox label").click(function(){
					
			var url= '';
			var values = '';
			$(".publisherTypeBox label").each(function(){
				var checkId = $(this).attr('for');
				//alert(checkId);
				if($("#"+checkId).is(':checked'))
				{
					var value = $(this).children().val();
					//alert(value);
					values += value+',';
				}
			});
			url= '<?php echo current_full_url();?>&publishertype='+values;
			window.location.href = url;
		});
		
	});
</script>
<script>
$(function(){ 
    $('.category_container').each(function(){
        $(this).children().equalHeights();
    }); 
});
$(function() {
	
	//alert('<?php echo $minPrice;?>');
	var minPrice = $("#prd_min_price").val();
	var maxPrice = $("#prd_max_price").val();
	if($("#prd_min_price").val() == '')
	{
		minPrice = 1;
	}
	if($("#prd_max_price").val() == '')
	{
		maxPrice = 5000;
	}
	
	//var priceRange = '[ '+minPrice+', '+maxPrice+' ]';
	//	alert(priceRange);
	$( "#slider-range" ).slider({
		range: true,
		min: 1,
		max: maxPrice,
		//values: [ <?php echo $minPrice;?>, <?php echo $maxPrice;?> ],
		values: [ minPrice, maxPrice ],
		slide: function( event, ui ) {
			$("#amount").val( "Rs." + ui.values[ 0 ] + " - Rs." + ui.values[ 1 ] );
			$("#minPrice").val(ui.values[0]);
			$("#maxPrice").val(ui.values[1]);
			
			url= '<?php echo current_full_url();?>&pricetype='+ui.values[ 0 ]+'-'+ui.values[ 1 ];
            window.location.href = url;
			
		}
	});
	$("#amount").val( "Rs." + $( "#slider-range" ).slider( "values", 0 ) +
		" - Rs." + $( "#slider-range" ).slider( "values", 1 ) );
	$("#minPrice").val($("#slider-range").slider( "values", 0 ));
	$("#maxPrice").val($("#slider-range").slider( "values", 1 ));
});
</script>
<?php 
function theme_img($uri, $tag=false){
	if($tag){
		return '<img src="'.theme_url('assets/assets/images/'.$uri).'" alt="'.$tag.'">';
	}else{
		return theme_url('assets/assets/images/'.$uri);
	}
	
}
function theme_url($uri){
	$CI =& get_instance();
	return $CI->config->base_url('/'.$uri);
}
?>
<?php $this->load->view('vwFooter');?>
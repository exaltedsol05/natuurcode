<?php $this->load->view('vwHeader');?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.0/jquery.fancybox.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/elevatezoom/3.0.8/jquery.elevatezoom.min.js" type="text/javascript"></script>
<!-- <script src="js/jquery.fancybox.pack.js" type="text/javascript"></script> -->
<style type="text/css">
   #gallery_01 img{border:2px solid white;width: 96px;}
   .active img{border:1px solid #333 !important;}
   a#ask_question_btn:hover, #save_question_on:hover {background: #fff !important; color: #99cc33;background: #f8f8f8 !important;; border: 1px #fff solid !important;;}
   a#ask_question_btn {
   background: #222 !important;
   isplay: block;
   padding: 12px 28px;
   font-size: 13px;
   font-family: 'Open Sans', sans-serif;
   line-height: 28px;
   text-transform: uppercase;
   position: relative;
   font-weight: bold;
   letter-spacing: 1px;
   background: #f8f8f8;
   color:#fff;
   }
   .b{
	   position:relative;
	   width:0;
	   -webkit-transition:width 0.3s linear 0s;
	   transition:width 0.3s linear 0s;
   }
   .a:hover +.b{
	   width:200px;
   }
</style>
<div class="breadcrumb">
   <div class="container">
      <div class="breadcrumb-inner">
         <ul class="list-inline list-unstyled">
            <li><a href="<?php echo base_url();?>" alt="natuur">Home</a></li>
            <?php
               $url_path	= '';
               $count	 	= 1;
               foreach($base_url as $bc):
               	$url_path .= '/'.$bc;
               	if($count == count($base_url)):?>
            <li class="active"><?php echo $bc;?></li>
            <?php else:?>
            <li><a href="<?php echo site_url($url_path);?>"><?php echo $bc;?></a></li>
            <span class="divider">/</span>
            <?php endif;
               $count++;
               endforeach;?>
         </ul>
      </div>
      <!-- /.breadcrumb-inner -->
   </div>
   <!-- /.container -->
</div>
<!-- /.breadcrumb -->
<div class="body-content outer-top-xs">
   <div class='container'>
      <div class='row single-product'>
        
         <div class='col-md-12'>
            <div class="detail-block">
               <div class="row  wow fadeInUp">
                  <div class="col-xs-12 col-sm-6 col-md-5 gallery-holder">
                     <div style="display:;">
                        <?php
                           $photo  = theme_img('no_picture.png', 'No Image Available');
                           $product->images    = array_values($product->images);
                           $count = count($product->images);
                           if(($count) > 1){
                           	for($j=0; $j<$count; $j++){	
                           		if($product->images[$j]->primary){?>	
                        <div class="single-product-gallery-item" id="slide<?php echo $i; ?>">
                           <a 	data-lightbox="image-1"
                              data-title="Gallery" href="<?php echo base_url('uploads/images/medium/'.$photo->filename);?>">
                           <img id="img1" src="<?php echo base_url('uploads/images/medium/'.$product->images[$j]->filename);?>" data-zoom-image="<?php echo base_url('uploads/images/full/'.$product->images[$j]->filename);?>" /> 
                           </a>
                        </div>
                        <?php	
                           }
                           }
                           }
                           ?>
                        <div id="gallery_01">
                           <?php
                              if(count($product->images) > 1):
                              	$i=0; 
                              	foreach($product->images as $image):
                              		?>
                           <a href="#" class="<?php echo ($i==0)?'active':'';?>" data-image="<?php echo base_url('uploads/images/medium/'.$image->filename);?>" data-zoom-image="<?php echo base_url('uploads/images/full/'.$image->filename);?>">
                           <img src="<?php echo base_url('uploads/images/thumbnails/'.$image->filename);?>"  />
                           </a>
                           <?php  $i++; endforeach;?>
                           <?php endif;?>
                        </div>
                     </div>
                  </div>
                  <!-- /.gallery-holder -->
                 
               <div class='col-sm-6 col-md-7 product-info-block'>
                  <div class="product-info">
                     <h2 class="name"><?php //echo "<pre>";print_r($product);exit; 
                        echo $product->name;?></h2>
                     <h4><?php if(!empty($product->sku)):?><?php echo $product->sku; ?><?php endif;?></h4>
                     <div class="rating-reviews m-t-20">
                        <div class="row">
                           <div class="col-sm-12">
                              <div class="rating-box">
                                 <ul class="clearfix">
                                    <?php
                                       $all_ret = $this->natuur->product_rating($product -> id);
                                       $product_rating = explode("*-*", $all_ret);
                                       $avg_rating = explode("*-*", $all_ret);
                                       //print_r($product_rating);
                                       $star1 	=$product_rating[0];
                                       $star2 	=$product_rating[1];
                                       $star3 	=$product_rating[2];
                                       $star4 	=$product_rating[3];
                                       $star5 	=$product_rating[4];
                                       $starall 	=$product_rating[5];
                                       $avt_rating = (1*($star1) + 2*($star2) +3*($star3) +4*($star4) + 5*($star5))/($starall);
                                       $avt_rating = number_format((float)$avt_rating, 1, '.', '');
                                       if(empty($avt_rating)){
                                       	$avt_rating = 0;
                                       }
                                       for($i = 0; $i < $avt_rating; $i++){
                                       	echo '<li class="active"><a href="#"></a></li>';
                                       }
                                       $dact = 5-$avt_rating;
                                       for($j = 0; $j < $dact; $j++){
                                       	echo '<li><a href="javascript:void(0);"></a></li>';
                                       }?>
                                    <li class="rc">(<?php echo $starall; ?> Rating)</li>
                                 </ul>
                              </div>
                           </div>
                           <div class="col-sm-12">
                              <?php
                                 if($total_review > 0){?>
                              <div class="reviews">
                                 <a href="#all_review"><?php echo $total_review; ?> Review </a>
                              </div>
                              <?php
                                 }else{?>
                              <div class="reviews">
                                 <a><?php echo $total_review; ?> Review </a>
                              </div>
                              <?php
                                 }?>	
                           </div>
                        </div>
                        <!-- /.row -->		
                     </div>
                     <!-- /.rating-reviews -->
                     <div class="stock-container info-container m-t-10">
                        <div class="row">
                           <div class="col-sm-2">
                              <div class="stock-box">
                                 <span class="label">Availability :</span>
                              </div>
                           </div>
                           <div class="col-sm-9">
                              <div class="stock-box">
                                 <?php if($product->quantity < 1):?>
                                 <span class="value">Out of Stock</span>
                                 <?php else:?>
                                 <span class="value">In Stock</span>
                                 <?php endif;?>
                              </div>
                           </div>
                        </div>
                        <!-- /.row -->	
                     </div>
                     <!-- /.stock-container -->
                     <div class="description-container m-t-20">
                        <?php echo $product->excerpt;?>
                     </div>
                     <!-- /.description-container -->
                     <div class="price-container info-container m-t-20">
                        <div class="row">
                           <div class="col-sm-6">
                              <div class="price-box">
                                 <?php 
                                    $discount = $product->price-$product->saleprice;
                                    if($discount>0){?> 
                                 <span class="price"><i class="fa fa-inr"></i> <?php echo $product->saleprice; ?> </span>
                                 <span class="price-strike"><i class="fa fa-inr"></i> <?php echo $product->price; ?></span>
                                 <?php }else{?>
                                 <span class="price"><i class="fa fa-inr"></i> <?php echo $product->price; ?></span>
                                 <?php } ?>
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="favorite-button m-t-10">
                                 <?php if($this->Customer_model->is_logged_in(false, false)){?>
                                 <a  onclick="add_to_wishlist(<?php echo $product->id;?>);" class="btn btn-primary"  title="Wishlist" href="javascript:void(0);">
                                 <i class="fa fa-heart"></i>
                                 </a>
                                 <?php
                                    }else{?>
                                 <a class="btn btn-primary" data-toggle="modal" data-target="#authentication" title="Wishlist" href="#">
                                 <i class="fa fa-heart"></i>
                                 </a>
                                 <?php
                                    }?>
                                 <?php /*if($this->Customer_model->is_logged_in(false, false)){?>
                                 <a onclick="add_to_compare(<?php echo $product->id;?>);" class="btn btn-primary" data-toggle="modal" data-placement="right" title="Add to Compare" href="#">
                                 <i class="fa fa-signal"></i>
                                 </a>
                                 <?php
                                    }else{?>
                                 <a class="btn btn-primary" data-placement="right" title="Add to Compare" href="#" data-toggle="modal" data-target="#authentication">
                                 <i class="fa fa-signal"></i>
                                 </a>
                                 <?php
                                    }*/?>
                              </div>
                           </div>
                        </div>
                        <!-- /.row -->
                     </div>
                     <!-- /.price-container -->
                     <?php 
                        // foreach ($this->natuur->contents() as $key => $val) break; echo $key;
                        $total_gruped_product =  count($product->grouped_products);
                        if($total_gruped_product <= 1){
                        	$prdid = $product->id;?>
                     <?php echo form_open('cart/add_to_cart_ajax', 'class="form-horizontal" id="add-to-cart_'.$product->id.'" name="add-to-cart_'.$prdid.'" accept-charset="utf-8"');?>
                     <input type="hidden" name="cartkey" value="<?php echo $this->session->flashdata('cartkey');?>" />
                     <input type="hidden" name="id" value="<?php echo $product->id?>"/>
                     <input type="hidden" name="rating_val" id="rating_val" value=""/>
                     <input type="hidden" name="saleprice_pricetier" value="<?php echo $sales_price;?>" id="saleprice_pricetier"/><?php
                        if($product->quantity >= 1){?>
                     <div class="quantity-container info-container">
                        <div class="row">
                           <div class="col-sm-2">
                              <span class="label">Qty :</span>
                           </div>
                           <div class="col-sm-4">
                              <div class="cart-quantity">
                                 <div class="quant-control">
                                    <a value="-" class="qtyminus btn-sm" field="quantity" ><i class="fa fa-minus"></i></a>
                                    <span class="quantity">
                                    <input style="width: 40px;height: 35px;text-align: center;" type="text" name="quantity" value="1" id="cart_quantity">
                                    </span>
                                    <a type="button" value="+" class="qtyplus btn-sm change" field="quantity"><i class="fa fa-plus"></i></a>
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-6" id="val_zip">
                              <button class="btn btn-sm btn-primary cart-button cart-button" type="button" value="submit" title="Add to Cart" id="aaddcartbut" style="margin:0px 15px;color:#fff;" onclick="addToCart(<?php echo $product->id;?>);"><i class="fa fa-shopping-cart inner-right-vs"></i> Add To Cart</button>
                           </div>
                        </div>
                        <!-- /.row -->
                     </div>
                     <!-- /.quantity-container -->
                     <?php
                        }?>
                     </form>
                     <?php } ?>
                  </div>
                  <!-- /.product-info -->
               </div>
               <!-- /.col-sm-7 -->
            </div>
            <!-- /.row -->
         </div>
         <div class="product-tabs inner-bottom-xs  wow fadeInUp">
            <div class="row">
               <div class="col-sm-12">
                  <ul id="product-tabs" class="nav nav-tabs nav-tab-cell">
                     <li class="active"><a data-toggle="tab" href="#description">DESCRIPTION</a></li>
                     <li><a data-toggle="tab" href="#howtouse">HOW TO USE</a></li>
                     <li><a data-toggle="tab" href="#review">REVIEW</a></li>
                     <li><a data-toggle="tab" href="#ask">ASK NATUUR</a></li>
                     <li><a data-toggle="tab" href="#blog">Blog</a></li>
                     <li><a data-toggle="tab" href="#video">Video</a></li>
                  </ul>
                  <!-- /.nav-tabs #product-tabs -->
               </div>
               <div class="col-sm-12">
                  <div class="tab-content">
                     <div id="description" class="tab-pane in active">
                        <div class="product-tab">
                           <p class="text">
                              <?php echo $product->description; ?>
                           </p>
                        </div>
                     </div>
                     <!-- /.tab-pane -->
                     <div id="howtouse" class="tab-pane">
                        <div class="product-tab">
                           <p class="text">
                              <?php echo $product->description2; ?>
                           </p>
                        </div>
                     </div>
                     <!-- /.tab-pane -->
                     <div id="review" class="tab-pane">
                        <div class="product-tab">
                           <div class="product-reviews">
                              <h4 class="title">Rate & Review this Product</h4>
                              </hr>
                              <p id="pop_err_msg"></p>
                              <!--rating section start -->
                              <div class="col-md-12 pull-left">
                                 <?php $user_rate = $this->natuur->user_rating($product -> id);
                                    if(empty($user_rate))
                                    {
                                    	$user_rate = 0;
                                    } 
                                    ?>
                                 <div class="aa-your-rating">
                                    <input id="rating-input" />
                                    <input type="hidden" name="prdid" id="prdid" value="<?php echo $product -> id; ?>" />
                                 </div>
                                 <script>
                                    $(document).ready(function(){
                                    	$('#rating-input').rating({
                                    		update: <?php echo $user_rate; ?>,
                                    		min: 1,
                                    		max: 5,
                                    		step: 1,
                                    		size: 'x',
                                    		showClear: false,
                                    		starCaptions: {
                                    			1:'Disappointing',
                                    			2:'Bad',
                                    			3:'Average',
                                    			4:'Good',
                                    			5:'Perfect'
                                    		}
                                    	});
                                    });
                                 </script>
                              </div>
                              <!-- rating section end-->
                              <div class="review-form">
                                 <div class="form-container">
                                    <form method="post" action="<?php echo base_url(); ?>ratenreview_controller/add_review" name="frm_review" id="frm_review" class="cnt-form">
                                       <input type="hidden" id="prdid" name="prdid" value="<?php echo $product -> id; ?>"/>
                                       <div class="row">
                                          <div class="col-sm-12">
                                             <div class="form-group">
                                                <label for="review_title">Title <span class="astk">*</span></label>
                                                <input name="title" type="text" class="form-control txt" id="review_title" placeholder="">
                                             </div>
                                             <!-- /.form-group -->
                                          </div>
                                          <div class="col-md-12">
                                             <div class="form-group">
                                                <label for="pop_review">Review <span class="astk">*</span></label>
                                                <textarea name="review" class="form-control txt txt-review" id="pop_review" rows="4" placeholder=""></textarea>
                                             </div>
                                             <!-- /.form-group -->
                                          </div>
                                       </div>
                                       <!-- /.row -->
                                       <div class="action text-right">
                                          <?php $this->load->library('session');
                                             $cust_id = $this->customer['id'];
                                             if(empty($cust_id)){?>
                                          <a href="#" data-toggle="modal" data-target="#authentication" class="btn btn-primary">SUBMIT REVIEW</a><?php
                                             }else{?>
                                          <a onclick="post_review_form();" class="btn btn-primary">SUBMIT REVIEW</a><?php
                                             }?>
                                       </div>
                                       <!-- /.action -->
                                    </form>
                                    <!-- /.cnt-form -->
                                 </div>
                                 <!-- /.form-container -->
                              </div>
                              <!-- /.review-form -->
                           </div>
                           <!-- /.product-add-review -->
                           <hr>
                           <div class="product-reviews">
                              <h4 class="title">Customer Reviews</h4>
                              <?php //echo "<pre>";print_r($product_review_recent);exit;?>
                              <?php foreach($product_review_recent as $prd_reviews){
								  ?>
                              <div class="reviews">
                                 <div class="review">
									<?php if(!empty($prd_reviews->image)){ ?>
									<img src="<?php echo base_url('uploads/customer_image/small');?>/<?=$prd_reviews->image?>" alt="user_image" width="60" height="60" style="float:left;margin-right:20px;border:0 !important;"/>
									<?php }else{ ?>
									<img src="<?php echo base_url('uploads/images');?>/no_image.png" alt="user_image" width="60" height="60" style="float:left;margin-right:20px;border:0 !important;"/>	
									<?php } ?>
                                    <div class="review-title">
                                       <span class="summary">
									   <?php if(!empty($prd_reviews->firstname) OR !empty($prd_reviews->lastname)){ 
										echo $prd_reviews->firstname." ".$prd_reviews->lastname;
										}else{  
										echo $prd_reviews->title;
											} 
										?>
									   
									   </span>
                                       <span class="date pull-right"><i class="fa fa-calendar"></i>
                                       <span><?php echo $prd_reviews->reviewdate; ?></span></span>
                                    </div>
                                    <div class="rating-box" style="margin-bottom:0 !important;">
                                       <ul class="clearfix">
                                          <?php
                                             $user_review_rate = $this->natuur->review_user_rating($prd_reviews -> product_id, $prd_reviews -> user_id);
                                             if(empty($user_review_rate)){
                                             	$user_review_rate = 0;
                                             }
                                             $active = $user_review_rate;
                                             $nonactive = 5-$user_review_rate;
                                             for ($x = 1; $x <= $active; $x++){
                                             	echo '<li class="active"><a href="#"></a>
                                             	</li>';
                                             }
                                             for ($x = 1; $x <= $nonactive; $x++){
                                             	echo '<li>
                                             	<a href="#"></a>
                                             	</li>';
                                             }?>
                                       </ul>
                                    </div>
                                    </br>
                                    <div class="text"><?php echo $prd_reviews->review; ?></div>
									<hr>
                                 </div>
                              </div>
                              <!-- /.reviews -->
                              <?php } ?>
                           </div>
                           <!-- /.product-reviews -->
                        </div>
                        <!-- /.product-tab -->
                     </div>
                     <!-- /.tab-pane -->
                     <div id="ask" class="tab-pane">
                        <div class="product-tab">
                           <p class="text">
                           <div class="">
                              <div class="col-md-12">
                                 <a href="#demo" class="btn btn-primary pull-right" id="ask_question_btn" data-toggle="collapse">Ask a Question</a>
                              </div>
                              <div class="clearfix"></div>
                              <div id="demo" class="collapse">
                                 <div class="row">
                                    <?php 
                                       $cart_contents = $this->session->userdata('cart_contents');
                                       $customer_id = $cart_contents['customer']['id'];
                                       ?> 
                                    <div class="col-md-10">
                                       <form class="form-horizontal" id="ask_question" name="ask_question" action="<?php echo base_url('cart/ask_question'); ?>" method="post" role="form">
                                          <input type="hidden" name="pid" value="<?php echo $product->id;?>" />
                                          <input type="hidden" name="customer_id" value="<?php echo $customer_id;?>" />
                                          <fieldset>
                                             <!-- Form Name -->
                                             <legend>Ask Question?</legend>
                                             <!-- Text input-->
                                             <div class="form-group">
                                                <label class="col-sm-2 control-label" for="textinput">Nick Name</label>
                                                <div class="col-sm-10">
                                                   <input type="text" name="nickname" placeholder="Enter Nick Name" class="form-control">
                                                </div>
                                             </div>
                                             <!-- Text input-->
                                             <div class="form-group">
                                                <label class="col-sm-2 control-label" for="textinput">Email</label>
                                                <div class="col-sm-10">
                                                   <input type="email" name="email" placeholder="Enter Email ID" class="form-control">
                                                </div>
                                             </div>
                                             <!-- Text input-->
                                             <div class="form-group">
                                                <label class="col-sm-2 control-label" for="textinput">Question</label>
                                                <div class="col-sm-10">
                                                   <textarea name="question" class="form-control" id="question" rows="3" placeholder="Your Question"></textarea>
                                                </div>
                                             </div>
                                             <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                   <div class="pull-right">
                                                      <?php if($this->Customer_model->is_logged_in(false, false)):?>
                                                      <button type="submit" id="save_question_on" name="save_question" class="btn btn-primary">Save</button>
                                                      <?php else: ?>
                                                      <button type="button" id="save_question_off" name="save_question" class="btn btn-primary">Save</button>
                                                      <?php endif; ?>
                                                   </div>
                                                </div>
                                             </div>
                                          </fieldset>
                                       </form>
                                    </div>
                                    <!-- /.col-lg-12 -->
                                 </div>
                                 <!-- /.row -->
                              </div>
                           </div>
                           <div class="container">
                              <div class="row">
                                 <div class="col-sm-12">
                                 </div>
                                 <!-- /col-sm-12 -->
                              </div>
                              <!-- /row -->
                              <div class="row" style="margin-top: 10px;">
                                 <?php
                                    foreach($arrQuestion as $Question){ 
                                    	if($Question->product_id == $product->id){
                                    		?>
                                 <div class="col-md-10">
                                    <div class="col-sm-1 col-md-offset-1" style="margin:0">
                                       <div class="thumbnails">
                                          <?php 
                                             foreach($customers as $customer){
                                             	if($customer->image){
                                             		$photo = base_url().'uploads/customer_image/small/'.$customer->image;
													$photo1 = base_url().'uploads/customer_image/full/'.$customer->image; 
                                             	}else{
                                             		$photo = 'https://ssl.gstatic.com/accounts/ui/avatar_2x.png';
                                             	}
                                             	if($Question->user_id == $customer->id){
                                             		echo '<img class="user-photo a" src="'.$photo.'" style="width: 71px;height: 87px;margin-top:15px;">';
													echo '<img class="user-photo b" style="border: none !important;" src="'.$photo1.'">';
                                             	}
                                             }
                                             ?>
                                       </div>
                                       <!-- /thumbnail -->
                                    </div>
                                    <!-- /col-sm-1 -->
                                    <div class="col-sm-8">
                                       <div class="panel panel-default">
                                          <div class="panel-heading">
                                             <strong><?php echo $Question->name; ?></strong> <span class="text-muted"><?php echo $strTimeAgo = timeAgo($Question->create_date); ?></span>
                                          </div>
                                          <div class="panel-body">
                                             <?php echo $Question->question; ?>
                                          </div>
                                          <!-- /panel-body -->
                                       </div>
                                       <!-- /panel panel-default -->
                                    </div>
                                    <!-- /col-sm-5 -->
                                 </div>
                                 <?php } }?>
                              </div>
                              <!-- /row -->
                           </div>
                           <!-- /container -->
                           </p>
                        </div>
                     </div>
                     <!-- /.tab-pane -->
                     <div id="blog" class="tab-pane">
                        <div class="product-tab">
                           <p class="text">
                              <?php echo $product->description4; ?>
                           </p>
                        </div>
                     </div>
                     <!-- /.tab-pane -->
                     <div id="video" class="tab-pane">
                        <div class="product-tab">
                           <p class="text">
                              <?php echo $product->description5; ?>
                           </p>
                        </div>
                     </div>
                     <!-- /.tab-pane -->
                  </div>
                  <!-- /.tab-content -->
               </div>
               <!-- /.col -->
            </div>
            <!-- /.row -->
         </div>
         <!-- /.product-tabs -->
         <!-- ============================================== UPSELL PRODUCTS ============================================== -->
         <section class="section featured-product wow fadeInUp">
            <!-- <h3 class="section-title">Upsell products</h3> -->
            <h3 class="section-title">SIMILAR products <small>(Customer's Who Bought This Also Bought below the similar products)</small></h3>
            <div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
               <?php foreach($related as $uproduct){
                  $photo  = theme_img('no_picture.png', 'No Image Available');
                  $uproduct->images   = array_values((array)json_decode($uproduct->images));
                  if(!empty($uproduct->images[0])){
                  	$primary    = $uproduct->images[0];
                  	foreach($uproduct->images as $photo){
                  		if(isset($photo->primary)){
                  			$primary    = $photo;
                  		}
                  	}
                  	$photo  = '<img src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$uproduct->seo_title.'" style="" />';
                  }
                  $discount=round(100*($uproduct->price-$uproduct->saleprice)/$uproduct->price);
                  //[from_date] => 2016-06-20[to_date] => 2016-06-25
                  $start_date = strtotime($uproduct->from_date);
                  $end_date = strtotime($uproduct->to_date);
                  $currenttimstamp = time();
                  $prd_link = base_url().$uproduct->slug;
                  //site_url(implode('/', $base_url).'/'.$uproduct->slug);
                  $prd_display = true;
                  if($prd_display){
                  	
                  	$discount=round(100*($uproduct->price-$uproduct->saleprice)/$uproduct->price);
                  	?>
               <div class="item item-carousel">
                  <div class="products">
                     <div class="product">
                        <div class="product-image">
                           <div class="image">
                              <a href="<?php echo $prd_link;?>"><?php echo $photo;?></a>
                           </div>
                           <!-- /.image -->
                           <div class="<?php echo($discount || $product->quantity < 1)?'tag sale':'';?>">
                              <span>
                              <?php
                                 if($uproduct->quantity < 1){
                                 	echo 'Out of Stock';
                                 }else if($discount){?>
                              <?php echo $discount;?>% <br> OFF
                              <?php 
                                 } 
                                 ?>
                              </span>
                           </div>
                        </div>
                        <!-- /.product-image -->
                        <div class="product-info text-center">
                           <h3 class="name"><a href="<?php echo $prd_link;?>"><?php echo $uproduct->name;?></a></h3>
                           <div class="col-md-12">
                              <div class="rating-box">
                                 <ul class="clearfix">
                                    <?php
                                       $all_ret = $this->natuur->product_rating($uproduct -> id);
                                       $product_rating = explode("*-*", $all_ret);
                                       $avg_rating = explode("*-*", $all_ret);
                                       //print_r($product_rating);
                                       $star1 	=$product_rating[0];
                                       $star2 	=$product_rating[1];
                                       $star3 	=$product_rating[2];
                                       $star4 	=$product_rating[3];
                                       $star5 	=$product_rating[4];
                                       $starall 	=$product_rating[5];
                                       $avt_rating = (1*($star1) + 2*($star2) +3*($star3) +4*($star4) + 5*($star5))/($starall);
                                       $avt_rating = number_format((float)$avt_rating, 1, '.', '');
                                       if(empty($avt_rating)){
                                       	$avt_rating = 0;
                                       }
                                       for($i = 0; $i < $avt_rating; $i++){
                                       	echo '<li class="active"><a href="#"></a></li>';
                                       }
                                       $dact = 5-$avt_rating;
                                       for($j = 0; $j < $dact; $j++){
                                       	echo '<li><a href="#"></a></li>';
                                       }?>
                                    <li class="rc">(<?php echo $starall; ?> Rating)</li>
                                 </ul>
                              </div>
                           </div>
                           <div class="description"></div>
                           <div class="product-price">	
                              <?php if($discount > 0):?>
                              <span class="price">
                              ₹ <?php echo $uproduct->saleprice;?>	</span>
                              <span class="price-before-discount">₹ <?php echo $uproduct->price;?></span>
                              <?php else: ?>
                              <span class="price">₹ <?php echo $uproduct->price;?></span>
                              <?php endif;?>
                           </div>
                        </div>
                        <div class="cart clearfix animate-effect">
                           <div class="action">
                              <ul class="list-unstyled">
                                 <li class="lnk">
                                    <a href="javascript:void(0);" onclick="addToCartProduct(<?php echo $product->id;?>);">
                                    <i class="fa fa-shopping-cart"></i></a>
                                 </li>
                                 <li class="lnk wishlist">
                                    <?php if($this->Customer_model->is_logged_in(false, false)){?>
                                    <a onclick="add_to_wishlist(<?php echo $uproduct->id;?>);" class="add-to-cart" href="javascript:void(0);" title="Wishlist">
                                    <i class="icon fa fa-heart"></i></a>
                                    <?php
                                       }else{?>
                                    <a class="add-to-cart" href="#" title="Wishlist"data-toggle="modal" data-target="#authentication">
                                    <i class="icon fa fa-heart"></i></a>
                                    <?php
                                       }?>
                                 </li>
                                 <!-- <li class="lnk">
                                    <?php /*if($this->Customer_model->is_logged_in(false, false)){?>
                                    <a onclick="add_to_compare(<?php echo $uproduct->id;?>);" class="add-to-cart"  title="Compare">
                                    <i class="fa fa-signal"></i></a>
                                    <?php
                                       }else{?>
                                    <a class="add-to-cart" href="#" title="Compare" data-toggle="modal" data-target="#authentication">
                                    <i class="fa fa-signal"></i></a>
                                    <?php
                                       }*/?>
                                    </li> -->
                              </ul>
                           </div>
                           <!-- /.action -->
                        </div>
                        <!-- /.cart -->
                     </div>
                     <!-- /.product -->
                  </div>
                  <!-- /.products -->
               </div>
               <!-- /.item -->
               <?php } }?>   
            </div>
            <!-- /.home-owl-carousel -->
         </section>
         <!-- /.section -->
         <!-- ============================================== UPSELL PRODUCTS : END ============================================== -->
      </div>
      <!-- /.col -->
      <div class="clearfix"></div>
   </div>
   <!-- /.row -->
</div>
<!-- /.container -->
</div><!-- /.body-content -->
<style>
   .thumbnail {
   padding:0px;
   }
   .panel {
   position:relative;
   }
   .panel>.panel-heading:after,.panel>.panel-heading:before{
   position:absolute;
   top:11px;left:-16px;
   right:100%;
   width:0;
   height:0;
   display:block;
   content:" ";
   border-color:transparent;
   border-style:solid solid outset;
   pointer-events:none;
   }
   .panel>.panel-heading:after{
   border-width:7px;
   border-right-color:#f7f7f7;
   margin-top:1px;
   margin-left:2px;
   }
   .panel>.panel-heading:before{
   border-right-color:#ddd;
   border-width:8px;
   }
</style>
<script>
   $("#ask_question").validate({
   	rules:{
   		nickname	: "required",
   		email		: "required",
   		question	: "required"
   	},
   	submitHandler: function (form){
   		$.ajax({
   			type: "POST",
   			datatype:"json",
   			data: $("#ask_question").serialize(),
   			url: "<?php echo base_url();?>cart/ask_question", 
   			success: function(msg){
   				alert("Thank you for your question"); 
   				setTimeout(function(){ 
   					window.location.reload();
   				}, 2000);
   			}
   		});
   	}
   });
   jQuery(document).ready(function(){
   	$('#save_question_off').click(function(){
   		alert("Please login before ask question");
   	});
   	$("#img1").elevateZoom({ gallery: 'gallery_01', cursor: 'pointer', galleryActiveClass: "active" });
   	$("#img1").bind("click", function(e) {
   		var ez = $('#img1').data('elevateZoom');
   		ez.closeAll();
   //$.fancybox(ez.getGalleryList());
   return false;
   });
   // This button will increment the value
   $('.qtyplus').click(function(e){
   // Stop acting like a button
   e.preventDefault();
   // Get the field name
   fieldName = $(this).attr('field');
   // Get its current value
   var currentVal = parseInt($('input[name='+fieldName+']').val());
   // If is not undefined
   if (!isNaN(currentVal)) {
   // Increment
   $('input[name='+fieldName+']').val(currentVal + 1);
   } else {
   // Otherwise put a 0 there
   $('input[name='+fieldName+']').val(0);
   }
   });
   // This button will decrement the value till 0
   $(".qtyminus").click(function(e) {
   // Stop acting like a button
   e.preventDefault();
   // Get the field name
   fieldName = $(this).attr('field');
   // Get its current value
   var currentVal = parseInt($('input[name='+fieldName+']').val());
   // If it isn't undefined or its greater than 0
   if (!isNaN(currentVal) && currentVal > 1) {
   // Decrement one
   $('input[name='+fieldName+']').val(currentVal - 1);
   } else {
   // Otherwise put a 0 there
   $('input[name='+fieldName+']').val(1);
   }
   });
   })
   if($(window).width() > 768){
   // Hide all but first tab content on larger viewports
   $('.accordion__content:not(:first)').hide();
   // Activate first tab
   $('.accordion__title:first-child').addClass('active');
   } else {
   // Hide all content items on narrow viewports
   $('.accordion__content').hide();
   };
   // Wrap a div around content to create a scrolling container which we're going to use on narrow viewports
   //$( ".accordion__content" ).wrapInner( "<div class='overflow-scrolling'></div>" );
   // The clicking action
   $('.accordion__title').on('click', function() {
   $('.accordion__content').hide();
   $(this).next().show().prev().addClass('active').siblings().removeClass('active');
   });
   function checkzipcode(){
   var zip = $.trim($("#pincode").val());
   var zipRegex = /^\d{6}$/;
   var msg = '';
   if (!zipRegex.test(zip)){
   	$("#chckpinmsg-green").html('');
   	$("#chckpinmsg-red").html("Please enter valid zip code.");
   }else{
   	$.ajax({
   		url: '<?php echo base_url(); ?>cart/checkzip',
   		type: 'post',
   		data: {'zipcode':zip},
   		dataType:"json",
   		success: function(data) {
   			if(data != '0'){
   				var zip_details = eval(data);
   //{"id":"43","postcode":"110043","isship":"1","iscod":"1","daystodeliver":"2 to 4"}
   if(zip_details['isship'] == '1'){
   $("#addcartbut").prop("disabled", false);
   if(zip_details['iscod'] == '1'){
   	msg = 'Yes, Cash on Delivery available in your area and We can deliver product in '+zip_details['iscod']+' Days';
   	$("#chckpinmsg-green").html(msg);
   	$("#chckpinmsg-red").html('')
   }else{
   	msg = 'Sorry, Cash on Delivery not available in your area and We can deliver product in '+zip_details['iscod']+' Days';
   	$("#chckpinmsg-green").html('');
   	$("#chckpinmsg-red").html(msg);
   }
   }else{
   $("#addcartbut").prop("disabled", true);
   $("#chckpinmsg-green").html('');
   $("#chckpinmsg-red").html("Sorry, We could not provide shipping service in your area.");
   }
   }else{
   $("#addcartbut").prop("disabled", true);
   $("#chckpinmsg-green").html('');
   $("#chckpinmsg-red").html("Sorry, We could not provide shipping service in your area.");
   }
   }
   });
   }			
   }
</script>
<?php
   function theme_img($uri, $tag=false)
   {
   	if($tag)
   	{
   		return '<img src="'.theme_url('assets/assets/images/'.$uri).'" alt="'.$tag.'">';
   	}
   	else
   	{
   		return theme_url('assets/assets/images/'.$uri);
   	}
   }
   function theme_url($uri)
   {
   	$CI =& get_instance();
   	return $CI->config->base_url('/'.$uri);
   }
   $this->load->view('vwFooter');
   ?>
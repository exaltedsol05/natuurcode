<?php $this->load->view('vwHeader');
$blog_category = array("1" => "Natuur Knowledge", "2"=>"Daily Wisdom", "3" => "DIY Workshop", "4"=> "Chemicals in Daily Life", "5" => "Health & Wellness", "6" => "Beauty");
reset($cat_blogs);
$catkey = key($cat_blogs); ?>
<style>
.header-nav{
	border-bottom: 1px solid #99cc33;
}
.section-title{
	font-size: 24px;text-decoration: underline;
	border-bottom:none;
	text-decoration:none;
	text-align:center;
	margin:0px;
	padding:15px;
}
.link li{padding-right:10px;}
h3, .h3 {
    font-size: 18px;
}
</style>
<div class="body-content outer-top-xs" id="top-banner-and-menu" style="background-color:#fff;">
	<div class="row" style="background-color:#f3f3f3;color:#5f5f5f;">
	  <div class="col-xs-12 col-sm-12 col-md-1 homebanner-holder"></div>
		  <div class="col-xs-12 col-sm-12 col-md-11 homebanner-holder">
			<div class="info-boxes wow fadeInUp" style="">
				<div class=""  style="background-color:#f3f3f3;color:#5f5f5f;">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-lg-4">
							<div class="info-boxx">
								<div class="row">								
									<div class="" style="padding-left:0px;">
										<h1 style="font-size:28px;color:#5f5f5f;margin-top:8px;margin-bottom:0px;font-family: 'Open Sans', sans-serif; text-transform: uppercase;" class="info-box-heading green">&nbsp;&nbsp;&nbsp;<?php echo $title;?></span></h1>
									</div>
								</div>
							</div>
						</div><!-- .col -->

						<div class="hidden-md col-sm-8 col-lg-8">
							<div class="info-box" style="padding: 16px 25px;">
								<div class="row">
									<ul style="color:#5f5f5f;">
										<li style="float:left;margin-left:15px;cursor:pointer;">
											<a href="<?php echo base_url();?>blog/category/natuur_knowledge"><strong>Natuur Knowledge</strong></a>
										</li>
										<li style="float:left;margin-left:15px;cursor:pointer;">
											<a href="<?php echo base_url();?>blog/category/daily_wisdom"><strong>Daily Wisdom</strong></a>
										</li>
										<li style="float:left;margin-left:15px;cursor:pointer;">
											<a href="<?php echo base_url();?>blog/category/diy_workshop"><strong>DIY Workshop</strong></a>
										</li>
										<li style="float:left;margin-left:15px;cursor:pointer;">
											<a href="<?php echo base_url();?>blog/category/chemicals_in_daily_life"><strong>Chemicals in Daily Life</strong></a>
										</li>
										<li style="float:left;margin-left:15px;cursor:pointer;">
											<a href="<?php echo base_url();?>blog/category/health_wellness"><strong>Health & Wellness</strong></a>
										</li>
										<li style="float:left;margin-left:15px;cursor:pointer;">
											<a href="<?php echo base_url();?>blog/category/beauty"><strong>Beauty</strong></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><!--
	<div class="row" style="background-color:#f3f3f3;color:#5f5f5f;border-bottom: 1px solid #9c3;">
		<div class="col-xs-12 col-sm-12 col-md-12 homebanner-holder">
			<div class="info-boxes wow fadeInUp" style="">
				<div class="" style="background-color:#f3f3f3;color:#5f5f5f;">
					<h3 class="section-title"><?php echo $blog->title;?></h3>
				</div>
			</div>
		</div>
	</div> -->
	<div class="container" style="min-height:400px;">
		<div class="row">
			<div class="col-md-12 col-sm-12" style="padding-left:30px;text-align:justify;font-size:18px;">
				<div><br/>
					<span class="pull-right" style="font-size:14px;"><a href="<?php echo base_url();?>blog">Blog</a> <i class="fa fa-angle-double-right" aria-hidden="true"></i> <strong><?php echo $title;?></strong></span>
				</div>
				<div class="row"></div><?php
				
				if(count($cat_blogs[$catkey]) > 0){
					foreach($cat_blogs[$catkey] as $dw){?>
						<div class="col-xs-12 col-sm-12 col-md-4" style="padding-left:0px;">
							<div class="wow fadeInUp" style="padding-left:0px;">
								<div id="advertisementt" class="advertisementt">
									<div class="item" style="background-color: #f3f3f3;">
										<div class="">
											<img src="<?php echo base_url()."uploads/blog/full/".$dw->blog_image;?>" alt="New Blog Image" style="width:100%;max-height:255px;">
										</div><br/>
										<h3 class="section-title" style="padding:10px;text-decoration:none;"><?php echo $blog_category[$dw->blog_cat];?></h3>
										<h4 class="section-title" style="padding:10px;text-decoration:none;font-size:18px;"><?php echo ucfirst($dw->title);?></h4>
										<div class="testimonials" style="text-align: left !important;padding:10px;">
											<p><?php echo implode(' ', array_slice(explode(' ', strip_tags($dw->content)), 0, 15)); ?>...</p>
											<p><?php echo date("F d, Y", strtotime($dw->creation_time)); ?></p>
											<a href="<?php echo base_url()."blog/blog_page/".$dw->id;?>" class="btn btn-primary btn-sm" style="color:#fff;">Read More</a>
										</div>
									</div>
								</div>
							</div>
						</div><?php
					}
				}else{
					echo "Sorry, No Blog found in <strong>$title</strong> category";
				}?>	
			</div>
		</div>
	</div>
</div><?php
$this->load->view('vwFooter'); ?>
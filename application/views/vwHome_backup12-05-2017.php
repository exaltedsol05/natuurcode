<?php $this->load->view('vwHeader');?>
<div class="body-content outer-top-xs" id="top-banner-and-menu">
<div class="container">
    <div class="row">
	  <div class="col-xs-12 col-sm-12 col-md-12 homebanner-holder">
		
		<!-- ========================================== SECTION – HERO ========================================= -->
			<div id="hero">
				<div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">
					 <?php $this->banners->show_collection(6, 5, '3_box_row');?>
					

				</div><!-- /.owl-carousel -->
			</div>
		<!-- ========================================= SECTION – HERO : END ========================================= -->	

		<!-- ============================================== INFO BOXES ============================================== -->
		<div class="info-boxes wow fadeInUp">
			<div class="info-boxes-inner">
				<div class="row">
					<div class="col-md-6 col-sm-4 col-lg-4">
						<div class="info-box">
							<div class="row">
								
								<div class="col-xs-12">
									<h4 class="info-box-heading green">money back</h4>
								</div>
							</div>	
							<h6 class="text">30 Days Money Back Guarantee</h6>
						</div>
					</div><!-- .col -->

					<div class="hidden-md col-sm-4 col-lg-4">
						<div class="info-box">
							<div class="row">
								
								<div class="col-xs-12">
									<h4 class="info-box-heading green">free shipping</h4>
								</div>
							</div>
							<h6 class="text">Shipping on orders over $99</h6>	
						</div>
					</div><!-- .col -->

					<div class="col-md-6 col-sm-4 col-lg-4">
						<div class="info-box">
							<div class="row">
								
								<div class="col-xs-12">
									<h4 class="info-box-heading green">Special Sale</h4>
								</div>
							</div>
							<h6 class="text">Extra $5 off on all items </h6>	
						</div>
					</div><!-- .col -->
				</div><!-- /.row -->
			</div><!-- /.info-boxes-inner -->
			
		</div><!-- /.info-boxes -->
		<!-- ============================================== INFO BOXES : END ============================================== -->
		
		</div>
	</div>
	<div class="row">
		<!-- ============================================== SIDEBAR ============================================== -->	
		<div class="col-xs-12 col-sm-12 col-md-3 sidebar" style="margin-top: 30px;">
			<?php //$this->load->view('vwHomeLeftMenu');?>
			
			<!-- ============================================== Sidebar Image ============================================== -->
			<div class="home-banner">
				<?php $this->banners->show_collection(11, 1, 'default');?>
			</div>
			<!-- ================================== HOT DEALS ================================ --><?php
			if(count($hotproducts)>=1){?>
				<div class="sidebar-widget hot-deals wow fadeInUp outer-bottom-xs" style="margin-top:30px;">
					<h3 class="section-title">hot deals</h3>
					<div class="owl-carousel sidebar-carousel custom-carousel owl-theme outer-top-ss"><?php
						foreach($hotproducts as $product){
							$discount=round(100*($product->price-$product->saleprice)/$product->price);
							$photo  = theme_img('no_picture.png', 'No Image Available');
							$product->images   = array_values($product->images);
							if(!empty($product->images[0])){
								$primary    = $product->images[0];
								foreach($product->images as $photo){
									if(isset($photo->primary)){
										$primary    = $photo;
									}
								}
								$photo  = '<img src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'"/>';
							}
							
							$salesprice = $this -> dentkart -> product_price($product);
							$discount=round(100*($product->price-$salesprice)/$product->price);

							//[from_date] => 2016-06-20[to_date] => 2016-06-25
							$start_date = strtotime($product->hotdeals_from);
							$end_date = strtotime($product->hotdeals_to);
							$currenttimstamp = time();
							
							$prd_link = site_url(implode('/', $base_url).'/'.$product->slug);
							$prd_display = true;
							if($currenttimstamp > $end_date){
								$prd_link = 'javascript:void(0)';
								$prd_display = false;
							}
							
							if($currenttimstamp < $start_date){
								$prd_link = 'javascript:void(0)';
								$prd_display = false;
							}
							
							if($prd_display){ ?>
								<div class="item">
									<div class="products">
										<div class="hot-deal-wrapper">
											<div class="image">
												<a href="<?php echo site_url(implode('/', $base_url).'/'.$product->slug); ?>"><?php echo $photo;?></a>
											</div>
											<div class="sale-offer-tag">
												<span>
													<?php 
														$sales_price = $this -> dentkart -> product_price($product);
														$discount=round(100*($product->price-$sales_price)/$product->price);
														if($product->quantity < 1){
															echo 'Out of Stock';
														}else if($discount){?>
															<?php echo $discount;?>% <br> OFF
															<?php 
														} 
													?>
												</span>
											</div>
											
											<div class="deal-counter" data-countdown="true" ndate="<?php echo $product->hotdeals_to; ?>"></div>
											
										</div><!-- /.hot-deal-wrapper -->

										<div class="product-info text-center m-t-20">
											<h3 class="name"><a href="<?php echo $product->slug; ?>"><?php echo$product->name; ?></a></h3>
											<div class="col-md-12">
												<div class="rating-box">
													<ul class="clearfix"><?php
														$all_ret = $this->dentkart->product_rating($product -> id);
														$product_rating = explode("*-*", $all_ret);
														$avg_rating = explode("*-*", $all_ret);
														//print_r($product_rating);

														$star1 	=$product_rating[0];
														$star2 	=$product_rating[1];
														$star3 	=$product_rating[2];
														$star4 	=$product_rating[3];
														$star5 	=$product_rating[4];
														$starall 	=$product_rating[5];
														$avt_rating = (1*($star1) + 2*($star2) +3*($star3) +4*($star4) + 5*($star5))/($starall);
														$avt_rating = number_format((float)$avt_rating, 1, '.', '');
														if(empty($avt_rating)){
															$avt_rating = 0;
														}
														for($i = 0; $i < $avt_rating; $i++){
															echo '<li class="active"><a href="#"></a></li>';
														}

														$dact = 5-$avt_rating;
														for($j = 0; $j < $dact; $j++){
															echo '<li><a href="#"></a></li>';
														}?>
														<li class="rc">(<?php echo $starall; ?> Rating)</li>
													</ul>
												</div>
											</div>

											<div class="product-price">	
												<?php if($discount > 0):?>
													<span class="price">
														₹ <?php echo $product->saleprice;?>	</span>
													<span class="price-before-discount">₹ <?php echo $product->price;?></span>
												<?php else: ?>
												  <span class="price">₹ <?php echo $product->price;?></span>
												<?php endif;?>
											</div><!-- /.product-price -->
											
										</div><!-- /.product-info -->

										<div class="cart clearfix animate-effect">
											<div class="action">
												<div class="add-cart-button btn-group">
													<!--<button class="btn btn-primary icon" data-toggle="dropdown" type="button"><i class="fa fa-shopping-cart"></i></button>
													<button class="btn btn-primary cart-btn" type="button">Add to cart</button>	-->	
												</div>
											</div><!-- /.action -->
										</div><!-- /.cart -->
									</div>	
								</div><?php
							} 
						} ?>
					</div><!-- /.sidebar-widget -->
				</div>	<?php
			}?>
			<!-- ============================= HOT DEALS: END ====================== -->
			<!-- =============================== SPECIAL OFFER ============== -->
			<div class="sidebar-widget outer-bottom-small wow fadeInUp" style="margin-top:30px;">
				<h3 class="section-title">Special Offer</h3>
				<div class="sidebar-widget-body outer-top-xs">
					<div class="owl-carousel sidebar-carousel special-offer custom-carousel owl-theme outer-top-xs">
						<?php foreach($arrSpecialOfferProducts as $product){
						  $photo  = theme_img('no_picture.png', 'No Image Available');
						  $product->images   = array_values((array)json_decode($product->images));
						  if(!empty($product->images[0])){
							$primary    = $product->images[0];
							foreach($product->images as $photo){
							  if(isset($photo->primary)){
								$primary    = $photo;
							  }
							}

							$photo  = '<img src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'" style="" />';
						  }
						  $discount=round(100*($product->price-$product->saleprice)/$product->price);

						  //[from_date] => 2016-06-20[to_date] => 2016-06-25
						  $start_date = strtotime($product->from_date);
						  $end_date = strtotime($product->to_date);
						  $currenttimstamp = time();
						  $prd_link = site_url(implode('/', $base_url).'/'.$product->slug);
						  $prd_display = true;

						  if($prd_display){
						?>
							<div class="item">
								<div class="products special-product">
									<div class="product">
										<div class="product-micro">
											<div class="row product-micro-row">
												<div class="col col-xs-5">
													<div class="product-image">
														<div class="image">
															<a href="<?php echo $prd_link;?>"><?php echo $photo;?></a>		
														</div><!-- /.image -->
													</div><!-- /.product-image -->
												</div><!-- /.col -->
												<div class="col col-xs-7">
													<div class="product-info text-center">
														<h3 class="name"><a href="<?php echo $prd_link;?>"><?php echo $product->name;?></a></h3>
														<div class="col-md-12">
															<div class="rating-box">
																<ul class="clearfix"><?php
																	$all_ret = $this->dentkart->product_rating($product -> id);
																	$product_rating = explode("*-*", $all_ret);
																	$avg_rating = explode("*-*", $all_ret);
																	//print_r($product_rating);

																	$star1 	=$product_rating[0];
																	$star2 	=$product_rating[1];
																	$star3 	=$product_rating[2];
																	$star4 	=$product_rating[3];
																	$star5 	=$product_rating[4];
																	$starall 	=$product_rating[5];
																	$avt_rating = (1*($star1) + 2*($star2) +3*($star3) +4*($star4) + 5*($star5))/($starall);
																	$avt_rating = number_format((float)$avt_rating, 1, '.', '');
																	if(empty($avt_rating)){
																		$avt_rating = 0;
																	}
																	for($i = 0; $i < $avt_rating; $i++){
																		echo '<li class="active"><a href="#"></a></li>';
																	}

																	$dact = 5-$avt_rating;
																	for($j = 0; $j < $dact; $j++){
																		echo '<li><a href="#"></a></li>';
																	}?>
																</ul>
															</div>
														</div>
														<div class="product-price">	
															<?php if($discount > 0):?>
																<span class="price">
																	₹ <?php echo $product->saleprice;?>	</span>
																<span class="price-before-discount">₹ <?php echo $product->price;?></span>
															<?php else: ?>
															  <span class="price">₹ <?php echo $product->price;?></span>
															<?php endif;?>
														</div><!-- /.product-price -->		
													</div>
												</div><!-- /.col -->
											</div><!-- /.product-micro-row -->
										</div><!-- /.product-micro -->
									</div>
								</div>
							</div> 
						<?php } }?>   
					</div>
				</div><!-- /.sidebar-widget-body -->
			</div><!-- /.sidebar-widget -->
			<!-- ============================================== SPECIAL OFFER : END ============================================== -->

			<!-- ============================================== Testimonials============================================== -->
			<div class="sidebar-widget  wow fadeInUp outer-top-vs ">
				<div id="advertisement" class="advertisement">
					<?php foreach ($arrTestimonial as $key => $value) { ?>
						<div class="item">							
							<div class="avatar"><img src="<?= base_url().'uploads/customer_image/small/'.$value->image;?>" alt="User Name"></div>
							<div class="testimonials"><em>"</em><?php echo $value->content;?><em>"</em></div>
							<div class="clients_author"><?php echo $value->firstname.' '.$value->lastname;?><span><?php echo $value->designation;?> <?php echo $value->company;?></span></div>
						</div>
						<?php 
					} ?>
				  
				</div><!-- /.owl-carousel -->
			</div>
			<!-- ============================================== Testimonials: END ============================================== -->
		</div><!-- /.sidemenu-holder -->
		<!-- ============================================== SIDEBAR : END ============================================== -->

		<!-- ============================================== CONTENT ============================================== -->
	<div class="col-xs-12 col-sm-12 col-md-9 homebanner-holder">
		
		
		<!-- ============================================== SCROLL TABS ============================================== -->
		<section class="section featured-product wow fadeInUp" style="margin-top:30px;">
			<h3 class="section-title">New products</h3>
			<div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
				<?php foreach($arrNewProducts as $product){
				  $photo  = theme_img('no_picture.png', 'No Image Available');
				  $product->images   = array_values((array)json_decode($product->images));
				  if(!empty($product->images[0])){
					$primary    = $product->images[0];
					foreach($product->images as $photo){
						if(isset($photo->primary)){
							$primary    = $photo;
						}
					}
					$photo  = '<img src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'" style="" />';
				  }
				  $discount=round(100*($product->price-$product->saleprice)/$product->price);

				  //[from_date] => 2016-06-20[to_date] => 2016-06-25
				  $start_date = strtotime($product->from_date);
				  $end_date = strtotime($product->to_date);
				  $currenttimstamp = time();
				  $prd_link = site_url(implode('/', $base_url).'/'.$product->slug);
				  $prd_display = true;

				  if($prd_display){
				?>
			
				<div class="item item-carousel">
					<div class="products">
						<div class="product">		
							<div class="product-image">
								<div class="image">
									<a href="<?php echo $prd_link;?>"><?php echo $photo;?></a>
								</div><!-- /.image -->
								<div class="tag sale">
									<span>
										<?php 
											$sales_price = $this -> dentkart -> product_price($product);
											$discount=round(100*($product->price-$sales_price)/$product->price);
											if($product->quantity < 1){
												echo 'Out of Stock';
											}else if($discount){?>
												<?php echo $discount;?>% OFF
												<?php 
											} 
										?>
									</span>
								</div>            		   
							</div><!-- /.product-image -->
								
							
							<div class="product-info text-center">
								<h3 class="name"><a href="<?php echo $prd_link;?>"><?php echo $product->name;?></a></h3>
								
								<div class="col-md-12">
									<div class="rating-box">
										<ul class="clearfix"><?php
											$all_ret = $this->dentkart->product_rating($product -> id);
											$product_rating = explode("*-*", $all_ret);
											$avg_rating = explode("*-*", $all_ret);
											//print_r($product_rating);

											$star1 	=$product_rating[0];
											$star2 	=$product_rating[1];
											$star3 	=$product_rating[2];
											$star4 	=$product_rating[3];
											$star5 	=$product_rating[4];
											$starall 	=$product_rating[5];
											$avt_rating = (1*($star1) + 2*($star2) +3*($star3) +4*($star4) + 5*($star5))/($starall);
											$avt_rating = number_format((float)$avt_rating, 1, '.', '');
											if(empty($avt_rating)){
												$avt_rating = 0;
											}
											for($i = 0; $i < $avt_rating; $i++){
												echo '<li class="active"><a href="#"></a></li>';
											}

											$dact = 5-$avt_rating;
											for($j = 0; $j < $dact; $j++){
												echo '<li><a href="#"></a></li>';
											}?>
											<li class="rc">(<?php echo $starall; ?> Rating)</li>
										</ul>
									</div>
								</div>
								
								<div class="description"></div>
								<div class="product-price">	
										<?php if($discount > 0):?>
											<span class="price">
												₹ <?php echo $product->saleprice;?>	</span>
											<span class="price-before-discount">₹ <?php echo $product->price;?></span>
										<?php else: ?>
										  <span class="price">₹ <?php echo $product->price;?></span>
										<?php endif;?>
								</div><!-- /.product-price -->
							</div><!-- /.product-info -->
							
							<div class="cart clearfix animate-effect">
								<div class="action">
									<ul class="list-unstyled">
										<li class="lnk">
											<a href="<?php echo site_url(implode('/', $base_url).'/'.$product->slug); ?>">
											<i class="fa fa-shopping-cart"></i></a>
										</li>
									   
										<li class="lnk wishlist">
											<?php if($this->Customer_model->is_logged_in(false, false)){?>
												<a onclick="add_to_wishlist(<?php echo $product->id;?>);" class="add-to-cart" href="javascript:;" title="Wishlist">
												<i class="icon fa fa-heart"></i></a>
											<?php
											}else{?>
												<a class="add-to-cart" href="#" title="Wishlist"data-toggle="modal" data-target="#authentication">
												<i class="icon fa fa-heart"></i></a>
											<?php
											}?>
										</li>

										<li class="lnk">
											<?php if($this->Customer_model->is_logged_in(false, false)){?>
												<a onclick="add_to_compare(<?php echo $product->id;?>);" class="add-to-cart"  title="Compare">
												<i class="fa fa-signal"></i></a>
											<?php
											}else{?>
												<a class="add-to-cart" href="#" title="Compare" data-toggle="modal" data-target="#authentication">
												<i class="fa fa-signal"></i></a>
											<?php
											}?>
										</li>
									</ul>
								</div><!-- /.action -->
							</div><!-- /.cart -->
						</div><!-- /.product -->
					</div><!-- /.products -->
				</div><!-- /.item -->
				
				<?php } }?>   
				
				
			</div><!-- /.home-owl-carousel -->
		</section><!-- /.section -->
		<!-- ============================================== SCROLL TABS : END ============================================== -->

		<!-- ============================================== WIDE PRODUCTS ============================================== -->
		<div class="wide-banners wow fadeInUp outer-bottom-xs">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="wide-banner cnt-strip">
						<div class="image">
						<?php $this->banners->show_collection(7, 1, 'default');?>
						</div>
					</div><!-- /.wide-banner -->
				</div><!-- /.col -->
				<div class="col-md-5 col-sm-5" style="display:none;">
					<div class="wide-banner cnt-strip">
						<div class="image">
							<img class="img-responsive" src="<?php echo HTTP_IMAGES_PATH;?>banners/home-banner2.jpg" alt="">
						</div>
					</div><!-- /.wide-banner -->
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.wide-banners -->
		<!-- ============================================== WIDE PRODUCTS : END ============================================== -->


		<!-- ============================================== FEATURED PRODUCTS ============================================== -->
		<section class="section featured-product wow fadeInUp">
			<h3 class="section-title">Featured products</h3>
			<div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
			
				<?php foreach($arrFeaturedProducts as $product){
				  $photo  = theme_img('no_picture.png', 'No Image Available');
				  $product->images   = array_values((array)json_decode($product->images));
				  if(!empty($product->images[0])){
					$primary    = $product->images[0];
					foreach($product->images as $photo){
					  if(isset($photo->primary)){
						$primary    = $photo;
					  }
					}

					$photo  = '<img src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'" style="" />';
				  }
				  $discount=round(100*($product->price-$product->saleprice)/$product->price);

				  //[from_date] => 2016-06-20[to_date] => 2016-06-25
				  $start_date = strtotime($product->from_date);
				  $end_date = strtotime($product->to_date);
				  $currenttimstamp = time();
				  $prd_link = site_url(implode('/', $base_url).'/'.$product->slug);
				  $prd_display = true;

				  if($prd_display){
				?>
			
				<div class="item item-carousel">
					<div class="products">
						<div class="product">		
							<div class="product-image">
								<div class="image">
									<a href="<?php echo $prd_link;?>"><?php echo $photo;?></a>
								</div><!-- /.image -->
								<div class="tag sale">
									<span>
										<?php 
											$sales_price = $this -> dentkart -> product_price($product);
											$discount=round(100*($product->price-$sales_price)/$product->price);
											if($product->quantity < 1){
												echo 'Out of Stock';
											}else if($discount){?>
												<?php echo $discount;?>% OFF
												<?php 
											} 
										?>
									</span>
								</div>            		   
							</div><!-- /.product-image -->
								
							
							<div class="product-info text-center">
								<h3 class="name"><a href="<?php echo $prd_link;?>"><?php echo $product->name;?></a></h3>
								
								<div class="col-md-12">
									<div class="rating-box">
										<ul class="clearfix"><?php
											$all_ret = $this->dentkart->product_rating($product -> id);
											$product_rating = explode("*-*", $all_ret);
											$avg_rating = explode("*-*", $all_ret);
											//print_r($product_rating);

											$star1 	=$product_rating[0];
											$star2 	=$product_rating[1];
											$star3 	=$product_rating[2];
											$star4 	=$product_rating[3];
											$star5 	=$product_rating[4];
											$starall 	=$product_rating[5];
											$avt_rating = (1*($star1) + 2*($star2) +3*($star3) +4*($star4) + 5*($star5))/($starall);
											$avt_rating = number_format((float)$avt_rating, 1, '.', '');
											if(empty($avt_rating)){
												$avt_rating = 0;
											}
											for($i = 0; $i < $avt_rating; $i++){
												echo '<li class="active"><a href="#"></a></li>';
											}

											$dact = 5-$avt_rating;
											for($j = 0; $j < $dact; $j++){
												echo '<li><a href="#"></a></li>';
											}?>
											<li class="rc">(<?php echo $starall; ?> Rating)</li>
										</ul>
									</div>
								</div>
								<div class="description"></div>
								<div class="product-price">	
										<?php if($discount > 0):?>
											<span class="price">
												₹ <?php echo $product->saleprice;?>	</span>
											<span class="price-before-discount">₹ <?php echo $product->price;?></span>
										<?php else: ?>
										  <span class="price">₹ <?php echo $product->price;?></span>
										<?php endif;?>
								</div><!-- /.product-price -->
							</div><!-- /.product-info -->
							
							<div class="cart clearfix animate-effect">
								<div class="action">
									<ul class="list-unstyled">
										<li class="lnk">
											<a href="<?php echo site_url(implode('/', $base_url).'/'.$product->slug); ?>">
											<i class="fa fa-shopping-cart"></i></a>
										</li>
									   
										<li class="lnk wishlist">
											<?php if($this->Customer_model->is_logged_in(false, false)){?>
												<a onclick="add_to_wishlist(<?php echo $product->id;?>);" class="add-to-cart" href="javascript:;" title="Wishlist">
												
												<i class="icon fa fa-heart"></i></a>
											<?php
											}else{?>
												<a class="add-to-cart" href="#" title="Wishlist"data-toggle="modal" data-target="#authentication">
												<i class="icon fa fa-heart"></i></a>
											<?php
											}?>
										</li>

										<li class="lnk">
											<?php if($this->Customer_model->is_logged_in(false, false)){?>
												<a onclick="add_to_compare(<?php echo $product->id;?>);" class="add-to-cart"  title="Compare">
												<i class="fa fa-signal"></i></a>
											<?php
											}else{?>
												<a class="add-to-cart" href="#" title="Compare" data-toggle="modal" data-target="#authentication">
												<i class="fa fa-signal"></i></a>
											<?php
											}?>
										</li>
									</ul>
								</div><!-- /.action -->
							</div><!-- /.cart -->
						</div><!-- /.product -->
					</div><!-- /.products -->
				</div><!-- /.item -->
				
				<?php } }?>   
				
				
			</div><!-- /.home-owl-carousel -->
		</section><!-- /.section -->
		<!-- ============================================== FEATURED PRODUCTS : END ============================================== -->

		<!-- ============================================== WIDE PRODUCTS ============================================== -->
		<div class="wide-banners wow fadeInUp outer-bottom-xs">
			<div class="row">

				<div class="col-md-12">
					<div class="wide-banner cnt-strip">
						<div class="image">
							<?php $this->banners->show_collection(10, 1, 'default');?>
						</div>	
						<div class="strip strip-text">
							<div class="strip-inner">
								<h2 class="text-right">New Fresh VEGETABLES <br>
								<span class="shopping-needs">Save up to 40% off</span></h2>
							</div>	
						</div>
						<div class="new-label">
							<div class="text">NEW</div>
						</div><!-- /.new-label -->
					</div><!-- /.wide-banner -->
				</div><!-- /.col -->

			</div><!-- /.row -->
		</div><!-- /.wide-banners -->
		<!-- ============================================== WIDE PRODUCTS : END ============================================== -->
		
		<!-- ============================================== BEST SELLER ============================================== -->
		<section class="section featured-product wow fadeInUp">
			<h3 class="section-title">Best Seller</h3>
			<div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
			
				<?php foreach($arrBestSellerProducts as $product){
				  $photo  = theme_img('no_picture.png', 'No Image Available');
				  $product->images   = array_values((array)json_decode($product->images));
				  if(!empty($product->images[0])){
					$primary    = $product->images[0];
					foreach($product->images as $photo){
					  if(isset($photo->primary)){
						$primary    = $photo;
					  }
					}

					$photo  = '<img src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'" style="" />';
				  }
				  $discount=round(100*($product->price-$product->saleprice)/$product->price);

				  //[from_date] => 2016-06-20[to_date] => 2016-06-25
				  $start_date = strtotime($product->from_date);
				  $end_date = strtotime($product->to_date);
				  $currenttimstamp = time();
				  $prd_link = site_url(implode('/', $base_url).'/'.$product->slug);
				  $prd_display = true;

				  if($prd_display){
				?>
			
				<div class="item item-carousel">
					<div class="products">
						<div class="product">		
							<div class="product-image">
								<div class="image">
									<a href="<?php echo $prd_link;?>"><?php echo $photo;?></a>
								</div><!-- /.image -->
								<div class="tag sale">
									<span>
										<?php 
											$sales_price = $this -> dentkart -> product_price($product);
											$discount=round(100*($product->price-$sales_price)/$product->price);
											if($product->quantity < 1){
												echo 'Out of Stock';
											}else if($discount){?>
												<?php echo $discount;?>% OFF
												<?php 
											} 
										?>
									</span>
								</div>            		   
							</div><!-- /.product-image -->
								
							
							<div class="product-info text-center">
								<h3 class="name"><a href="<?php echo $prd_link;?>"><?php echo $product->name;?></a></h3>
								
								<div class="col-md-12">
									<div class="rating-box">
										<ul class="clearfix"><?php
											$all_ret = $this->dentkart->product_rating($product -> id);
											$product_rating = explode("*-*", $all_ret);
											$avg_rating = explode("*-*", $all_ret);
											//print_r($product_rating);

											$star1 	=$product_rating[0];
											$star2 	=$product_rating[1];
											$star3 	=$product_rating[2];
											$star4 	=$product_rating[3];
											$star5 	=$product_rating[4];
											$starall 	=$product_rating[5];
											$avt_rating = (1*($star1) + 2*($star2) +3*($star3) +4*($star4) + 5*($star5))/($starall);
											$avt_rating = number_format((float)$avt_rating, 1, '.', '');
											if(empty($avt_rating)){
												$avt_rating = 0;
											}
											for($i = 0; $i < $avt_rating; $i++){
												echo '<li class="active"><a href="#"></a></li>';
											}

											$dact = 5-$avt_rating;
											for($j = 0; $j < $dact; $j++){
												echo '<li><a href="#"></a></li>';
											}?>
											<li class="rc">(<?php echo $starall; ?> Rating)</li>
										</ul>
									</div>
								</div>
								
								<div class="description"></div>
								<div class="product-price">	
										<?php if($discount > 0):?>
											<span class="price">
												₹ <?php echo $product->saleprice;?>	</span>
											<span class="price-before-discount">₹ <?php echo $product->price;?></span>
										<?php else: ?>
										  <span class="price">₹ <?php echo $product->price;?></span>
										<?php endif;?>
								</div><!-- /.product-price -->
							</div><!-- /.product-info -->
							
							<div class="cart clearfix animate-effect">
								<div class="action">
									<ul class="list-unstyled">
										<li class="lnk">
											<a href="<?php echo site_url(implode('/', $base_url).'/'.$product->slug); ?>">
											<i class="fa fa-shopping-cart"></i></a>
										</li>
									   
										<li class="lnk wishlist">
											<?php if($this->Customer_model->is_logged_in(false, false)){?>
												<a onclick="add_to_wishlist(<?php echo $product->id;?>);" class="add-to-cart" href="javascript:;" title="Wishlist">
												<i class="icon fa fa-heart"></i></a>
											<?php
											}else{?>
												<a class="add-to-cart" href="#" title="Wishlist"data-toggle="modal" data-target="#authentication">
												<i class="icon fa fa-heart"></i></a>
											<?php
											}?>
										</li>

										<li class="lnk">
											<?php if($this->Customer_model->is_logged_in(false, false)){?>
												<a onclick="add_to_compare(<?php echo $product->id;?>);" class="add-to-cart"  title="Compare">
												<i class="fa fa-signal"></i></a>
											<?php
											}else{?>
												<a class="add-to-cart" href="#" title="Compare" data-toggle="modal" data-target="#authentication">
												<i class="fa fa-signal"></i></a>
											<?php
											}?>
										</li>
									</ul>
								</div><!-- /.action -->
							</div><!-- /.cart -->
						</div><!-- /.product -->
					</div><!-- /.products -->
				</div><!-- /.item -->
				
				<?php } }?>   
				
				
			</div><!-- /.home-owl-carousel -->
		</section><!-- /.section -->
		<!-- ============================================== BEST SELLER : END ============================================== -->	

		<!-- ============================================== WIDE PRODUCTS ============================================== -->
		<div class="wide-banners wow fadeInUp outer-bottom-xs">
			<div class="row">

				<div class="col-md-12">
					<div class="wide-banner cnt-strip">
						<div class="image">
							<?php $this->banners->show_collection(14, 1, 'default');?>
						</div>	
						
						
					</div><!-- /.wide-banner -->
				</div><!-- /.col -->

			</div><!-- /.row -->
		</div><!-- /.wide-banners -->
		<!-- ============================================== WIDE PRODUCTS : END ============================================== -->
				
	</div><!-- /.homebanner-holder -->
	<!-- ============================================== CONTENT : END ============================================== -->
		
	</div><!-- /.row -->
		
	</div><!-- /.container -->
</div><!-- /#top-banner-and-menu -->

<?php
function theme_img($uri, $tag=false){
	if($tag){
		return '<img src="'.theme_url('assets/assets/<?=HTTP_IMAGES_PATH;?>'.$uri).'" alt="'.$tag.'">';
	}else{
		return theme_url('assets/assets/<?=HTTP_IMAGES_PATH;?>'.$uri);
	}
}
function theme_url($uri){
	$CI =& get_instance();
	return $CI->config->base_url('/'.$uri);
}

$this->load->view('vwFooter'); ?>
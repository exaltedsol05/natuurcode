<?php $this->load->view('vwHeader');?>
<style>
.span1 {cursor:pointer; }
.minus, .plus{ width: 28px; height: 30px; background: #f2f2f2; border-radius: 4px; padding: 0px 5px 8px 5px; border:1px solid #ddd; display: inline-block; vertical-align: middle; text-align: center; }
#quantity1{ height:34px; width: 100px; text-align: center; font-size: 26px; border:1px solid #ddd; border-radius:4px; display: inline-block; vertical-align: middle;}			
</style>
<!--breadcrumbs area start-->
    <div class="breadcrumbs_area">
        <div class="container">   
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <ul>
                            <li><a href="<?php echo site_url('/');?>">home</a></li>
                            <li><a href="javascript:;"><?php echo $product->name;?></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>         
    </div>
    <!--breadcrumbs area end-->
    
    <div class="product_container">
        <div class="container">
            <div class="product_container_inner mb-60">
                <!--product details start-->
                <div class="product_details mb-60">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                           <div class="product-details-tab" style="text-align: center;">
                                <div id="img-1" class="zoomWrapper single-zoom">
                                    
									<?php
									$photo  = theme_img('no_picture.png', 'No Image Available');
									$product->images    = array_values($product->images);
									$count = count($product->images);
										if(($count) > 1){
											for($j=0; $j<$count; $j++){	
												if($product->images[$j]->primary){?>	
													
														<a 	data-lightbox="image-1"
														data-title="Gallery" href="<?php echo base_url('uploads/images/medium/'.$photo->filename);?>">
														<img id="zoom1" src="<?php echo base_url('uploads/images/medium/'.$product->images[$j]->filename);?>" data-zoom-image="<?php echo base_url('uploads/images/full/'.$product->images[$j]->filename);?>" /> 
														</a>
													
												<?php	
												}
											}
										}
									?>
                                       
                                   
                                </div>
                                <div class="single-zoom-thumb">
                                    <ul class="s-tab-zoom owl-carousel single-product-active" id="gallery_01">
									<?php
									if(count($product->images) > 1):
									$i=0; 
									foreach($product->images as $image):
									?>
									<li>
									<a href="#" class="elevatezoom-gallery <?php echo ($i==0)?'active':'';?>" data-update="" data-image="<?php echo base_url('uploads/images/medium/'.$image->filename);?>" data-zoom-image="<?php echo base_url('uploads/images/full/'.$image->filename);?>">
										<img src="<?php echo base_url('uploads/images/thumbnails/'.$image->filename);?>"  />
									</a>
									 </li>
									<?php  $i++; endforeach;?>
									<?php endif;?>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="product_d_right">
                               <?php echo form_open('cart/add_to_cart_ajax', 'class="" id="add-to-cart_'.$product->id.'" name="add-to-cart_'.$prdid.'" accept-charset="utf-8"');?>
								<input type="hidden" name="cartkey" value="<?php echo $this->session->flashdata('cartkey');?>" />
								<input type="hidden" name="id" value="<?php echo $product->id?>"/>
								<input type="hidden" name="rating_val" id="rating_val" value=""/>

                                    <h1><?php echo $product->name;?>(<?php if(!empty($product->sku)):?><?php echo $product->sku; ?><?php endif;?>)</h1>
                                    
                                    <div class="product_rating">
                                        <ul>
										    <?php
                                       $all_ret = $this->natuur->product_rating($product -> id);
                                       $product_rating = explode("*-*", $all_ret);
                                       $avg_rating = explode("*-*", $all_ret);
                                       //print_r($product_rating);
                                       $star1 	=$product_rating[0];
                                       $star2 	=$product_rating[1];
                                       $star3 	=$product_rating[2];
                                       $star4 	=$product_rating[3];
                                       $star5 	=$product_rating[4];
                                       $starall 	=$product_rating[5];
                                       $avt_rating = (1*($star1) + 2*($star2) +3*($star3) +4*($star4) + 5*($star5))/($starall);
                                       $avt_rating = number_format((float)$avt_rating, 1, '.', '');
                                       if(empty($avt_rating)){
                                       	$avt_rating = 0;
                                       }
                                       for($i = 0; $i < $avt_rating; $i++){
                                       	echo '<li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>';
                                       }
                                       $dact = 5-$avt_rating;
                                       for($j = 0; $j < $dact; $j++){
                                       	echo ' <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>';
                                       }?>
                                       <li class="rc">(<?php echo $starall; ?> Rating)</li>
                                           
                                        </ul>
                                    </div>
									<div class="product_meta">
									<div class="row">
										<div class="col-sm-3">
											<div class="stock-box">
												<span class="label">Availability :</span>
											</div>
										</div>
										<div class="col-sm-9">
											<div class="stock-box">
											<?php if($product->quantity < 1):?>
												<span class="value">Out of Stock</span>
											<?php else:?>
												<span class="value">In Stock</span>
											<?php endif;?>
											</div>
										</div>
									</div>
									<!-- /.row -->	
									</div>
                                    <div class="price_box">
										<?php 
											$price_design = $this->natuur->get_natuur_currency_function($product->price,$product->saleprice);
											echo $price_design['price_design'];
										?>
                                    </div>
                                    <div class="product_desc">
                                        <p><?php echo $product->excerpt; ?></p>
                                    </div>                                    
                                    <div class="product_variant quantity">
                                        <label>quantity</label>
                                       <div class="number number_quantity"><span class="minus span1" >-</span><input type="text" value="1" id="quantity1" class="quantity1" name="quantity"/><span class="plus span1">+</span></div>
									   <div class="cartWishlist">
											<button class="button add_to_cart_button" type="button" onclick="addToCart(<?php echo $product->id;?>);" title="add to cart"><i class="zmdi zmdi-shopping-cart-plus"></i>Add To Bag</button><?php if($this->Customer_model->is_logged_in(false, false)){?><button class="button wishlist_button" type="button" onclick="add_to_wishlist(<?php echo $product->id;?>);" title="add to wishlist"><i class="fa fa-heart-o" aria-hidden="true"></i>Wishlist</button><?php
											}else{?><button class="button wishlist_button" type="button" title="Wishlist" data-toggle="modal" data-target="#myLoginModal" title="add to wishlist"><i class="fa fa-heart-o" aria-hidden="true"></i>Wishlist</button>
											<?php
											}?>
										</div>
                                    </div>
                                </form>
                                <div class="priduct_social" style="display:none;">
                                    <ul>
                                        <li><a class="facebook" href="#" title="facebook"><i class="fa fa-facebook"></i> Like</a></li>           
                                        <li><a class="twitter" href="#" title="twitter"><i class="fa fa-twitter"></i> tweet</a></li>           
                                        <li><a class="pinterest" href="#" title="pinterest"><i class="fa fa-pinterest"></i> save</a></li>           
                                        <li><a class="google-plus" href="#" title="google +"><i class="fa fa-google-plus"></i> share</a></li>        
                                        <li><a class="linkedin" href="#" title="linkedin"><i class="fa fa-linkedin"></i> linked</a></li>        
                                    </ul>      
                                </div>

                            </div>
                        </div>
                    </div>  
                </div>
                <!--product details end-->

                <!--product info start-->
                <div class="product_d_info">
                    <div class="row">
                        <div class="col-12">
                            <div class="product_d_inner">   
                                <div class="product_info_button">    
                                    <ul class="nav" role="tablist">
                                        <li >
                                            <a class="active" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="false">Description</a>
                                        </li>
                                        <li>
                                             <a data-toggle="tab" href="#sheet" role="tab" aria-controls="sheet" aria-selected="false">HOW TO USE</a>
                                        </li>
										 <li><a data-toggle="tab" href="#ask">ASK NATUUR</a></li>
										<li><a data-toggle="tab" href="#blog">Blog</a></li>
										<li><a data-toggle="tab" href="#video">Video</a></li>
                                        <li>
                                           <a data-toggle="tab" href="#reviews" role="tab" aria-controls="reviews" aria-selected="false">Reviews (1)</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane fade show active" id="info" role="tabpanel" >
                                        <div class="product_info_content">
                                            <?php echo $product->description; ?>
                                        </div>    
                                    </div>
                                    
									<div class="tab-pane fade" id="sheet" role="tabpanel" >
                                        <div class="product_d_table">
                                          <?php echo $product->description2; ?>
                                        </div>
									</div>
									<div class="tab-pane fade" id="ask" role="tabpanel" >
                                        <div class="product_d_table">
                                           <div class="product-tab">
                           <p class="text">
                           <div class="">
                              <div class="col-md-12">
                                 <a href="#demo" class="btn pull-right submit_button" id="ask_question_btn" data-toggle="collapse">Ask a Question</a>
                              </div>
                              <div class="clearfix"></div>
                              <div id="demo" class="collapse">
                                 <div class="row">
                                    <?php 
                                       $cart_contents = $this->session->userdata('cart_contents');
                                       $customer_id = $cart_contents['customer']['id'];
                                       ?> 
                                    <div class="col-md-10">
                                       <form class="form-horizontal" id="ask_question" name="ask_question" action="<?php echo base_url('cart/ask_question'); ?>" method="post" role="form">
                                          <input type="hidden" name="pid" value="<?php echo $product->id;?>" />
                                          <input type="hidden" name="customer_id" value="<?php echo $customer_id;?>" />
                                          <fieldset>
                                             <!-- Form Name -->
                                             <legend>Ask Question?</legend>
                                             <!-- Text input-->
                                             <div class="form-group">
                                                <label class="col-sm-2 control-label" for="textinput">Nick Name</label>
                                                <div class="col-sm-10">
                                                   <input type="text" name="nickname" placeholder="Enter Nick Name" class="form-control">
                                                </div>
                                             </div>
                                             <!-- Text input-->
                                             <div class="form-group">
                                                <label class="col-sm-2 control-label" for="textinput">Email</label>
                                                <div class="col-sm-10">
                                                   <input type="email" name="email" placeholder="Enter Email ID" class="form-control">
                                                </div>
                                             </div>
                                             <!-- Text input-->
                                             <div class="form-group">
                                                <label class="col-sm-2 control-label" for="textinput">Question</label>
                                                <div class="col-sm-10">
                                                   <textarea name="question" class="form-control" id="question" rows="3" placeholder="Your Question"></textarea>
                                                </div>
                                             </div>
                                             <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                   <div class="pull-right">
                                                      <?php if($this->Customer_model->is_logged_in(false, false)):?>
                                                      <button type="submit" id="save_question_on" name="save_question" class="btn submit_button">Save</button>
                                                      <?php else: ?>
                                                      <button type="button" id="save_question_off" name="save_question" class="btn submit_button">Save</button>
                                                      <?php endif; ?>
                                                   </div>
                                                </div>
                                             </div>
                                          </fieldset>
                                       </form>
                                    </div>
                                    <!-- /.col-lg-12 -->
                                 </div>
                                 <!-- /.row -->
                              </div>
                           </div>
                           <div class="container">
                              <div class="row">
                                 <div class="col-sm-12">
                                 </div>
                                 <!-- /col-sm-12 -->
                              </div>
                              <!-- /row -->
                              <div class="row" style="margin-top: 10px;">
                                 <?php
                                    foreach($arrQuestion as $Question){ 
                                    	if($Question->product_id == $product->id){
                                    		?>
                                 <div class="col-md-10">
                                    <div class="col-sm-1 col-md-offset-1" style="margin:0">
                                       <div class="thumbnails">
                                          <?php 
                                             foreach($customers as $customer){
                                             	if($customer->image){
                                             		$photo = base_url().'uploads/customer_image/small/'.$customer->image;
													$photo1 = base_url().'uploads/customer_image/full/'.$customer->image; 
                                             	}else{
                                             		$photo = 'https://ssl.gstatic.com/accounts/ui/avatar_2x.png';
                                             	}
                                             	if($Question->user_id == $customer->id){
                                             		echo '<img class="user-photo a" src="'.$photo.'" style="width: 71px;height: 87px;margin-top:15px;">';
													echo '<img class="user-photo b" style="border: none !important;" src="'.$photo1.'">';
                                             	}
                                             }
                                             ?>
                                       </div>
                                       <!-- /thumbnail -->
                                    </div>
                                    <!-- /col-sm-1 -->
                                    <div class="col-sm-8">
                                       <div class="panel panel-default">
                                          <div class="panel-heading">
                                             <strong><?php echo $Question->name; ?></strong> <span class="text-muted"><?php echo $strTimeAgo = timeAgo($Question->create_date); ?></span>
                                          </div>
                                          <div class="panel-body">
                                             <?php echo $Question->question; ?>
                                          </div>
                                          <!-- /panel-body -->
                                       </div>
                                       <!-- /panel panel-default -->
                                    </div>
                                    <!-- /col-sm-5 -->
                                 </div>
                                 <?php } }?>
                              </div>
                              <!-- /row -->
                           </div>
                           <!-- /container -->
                           </p>
                        </div>
                                        </div>
									</div>
									<div class="tab-pane fade" id="blog" role="tabpanel" >
                                        <div class="product_d_table">
                                          <?php echo $product->description4; ?>
                                        </div>
									</div>
									<div class="tab-pane fade" id="video" role="tabpanel" >
                                        <div class="product_d_table">
                                          <?php echo $product->description5; ?>
                                        </div>
									</div>
                                        
                                    <div class="tab-pane fade" id="reviews" role="tabpanel" >
                                        <div class="reviews_wrapper">
										 <h4 class="title">Rate & Review this Product</h4>
										</hr>
										<p id="pop_err_msg"></p>
                                            <?php $user_rate = $this->natuur->user_rating($product -> id);
											if(empty($user_rate))
											{
												$user_rate = 0;
											} 
											?>
										 <div class="aa-your-rating">
											<input id="rating-input" />
											
											<input type="hidden" name="user_rating" id="user_rating" value="<?php echo $user_rate; ?>" />
										 </div>
										
											</div>
											<!-- rating section end-->
										<div class="review-form">
                                 <div class="form-container">
                                    <form method="post" action="<?php echo base_url(); ?>ratenreview_controller/add_review" name="frm_review" id="frm_review" class="cnt-form">
                                       <input type="hidden" id="prdid" name="prdid" value="<?php echo $product -> id; ?>"/>
									   
									    <!-- <div class="rating">
											<span class="rating__result"></span> 
											<i class="rating__star fa fa-star-o"></i>
											<i class="rating__star fa fa-star-o"></i>
											<i class="rating__star fa fa-star-o"></i>
											<i class="rating__star fa fa-star-o"></i>
											<i class="rating__star fa fa-star-o"></i>
										</div> -->
									   
                                       <div class="row default_form">
                                          <div class="col-sm-12">
                                             <div class="form-group">
                                                <label class="form_label" for="review_title">Title <span class="astk">*</span></label>
                                                <input name="title" type="text" class="form-control txt" id="review_title" placeholder="">
                                             </div>
                                             <!-- /.form-group -->
                                          </div>
                                          <div class="col-md-12">
                                             <div class="form-group">
                                                <label class="form_label" for="pop_review">Review <span class="astk">*</span></label>
                                                <textarea name="review" class="form-control txt txt-review" id="pop_review" rows="4" placeholder=""></textarea>
                                             </div>
                                             <!-- /.form-group -->
                                          </div>
                                       </div>
                                       <!-- /.row -->
                                       <div class="action text-right">
                                          <?php $this->load->library('session');
                                             $cust_id = $this->customer['id'];
                                             if(empty($cust_id)){?>
                                          <a href="#" data-toggle="modal" data-target="#authentication" class="btn btn-primary">SUBMIT REVIEW</a><?php
                                             }else{?>
                                          <a onclick="post_review_form();" class="btn btn-primary">SUBMIT REVIEW</a><?php
                                             }?>
                                       </div>
                                       <!-- /.action -->
                                    </form>
                                    <!-- /.cnt-form -->
                                 </div>
                                 <!-- /.form-container -->
                              </div>
											<!-- /.review-form -->
										
										    
                                    </div>
                                </div>
                            </div>     
                        </div>
                    </div>  
                </div>  
                <!--product info end-->
            </div>
            
            <!--product area end-->

            <!--product area start-->
            <div class="product_wrapper upsell_products">
                <div class="row">
                        <div class="col-12">
                            <div class="section_title title_style4">
                                <h3>Similar Products</h3>
                            </div>
                            <div class="row product_slick_row4">
							     <?php foreach($related as $uproduct){
								$photo  = theme_img('no_picture.png', 'No Image Available');
								$uproduct->images   = array_values((array)json_decode($uproduct->images));
								if(!empty($uproduct->images[0])){
								$primary    = $uproduct->images[0];
								foreach($uproduct->images as $photo){
									if(isset($photo->primary)){
										$primary    = $photo;
									}
								}
									$photo  = '<img src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$uproduct->seo_title.'" style="" />';
								}
								$discount=round(100*($uproduct->price-$uproduct->saleprice)/$uproduct->price);
								
								
								$prd_link = base_url().$uproduct->slug;
								
								?>
                                <div class="col-lg-3">
                                    <div class="single_product pro">
										<div class="price-container">
											<div class="wishlist_btn wishlistBtn_similar">
												<?php if($this->Customer_model->is_logged_in(false, false)){?>
														<a onclick="add_to_wishlist(<?php echo $product->id;?>);" href="javascript:;" title="Wishlist">
														<i class="ion-android-favorite-outline"></i></a>
												<?php
													}else{
												?>
														<a  href="#" title="Wishlist" aria-hidden="true" data-toggle="modal" data-target="#myLoginModal">
														<i class="ion-android-favorite-outline"></i></a>
												<?php } ?>
											</div>
                                        </div>
                                        <div class="product_thumb">
                                            <a class="primary_img" href="<?php echo $prd_link;?>"><?php echo $photo;?></a>
											
                                            <a class="secondary_img" href="<?php echo $prd_link;?>"><?php echo $photo;?></a>
											<?php if($discount)
											{?>
                                            <div class="label_product">
                                                <span class="label_sale"><?php echo $discount;?> %</span>
                                            </div>
											<?php }?>                                           
                                        </div>
                                        <div class="product_content">
                                            <div class="product_name">
                                                <h4><a href="product-details.html"><?php echo $uproduct->name;?></a></h4>
                                            </div>
                                            <div class="product_rating">
                                                <ul>
                                                    <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                                                    <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                                                    <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                                                    <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                                                    <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                                                </ul>
                                            </div>
                                            <div class="price-container">
                                                <div class="price_box">
                                                    <?php 
														$price_design = $this->natuur->get_natuur_currency_function($product->price,$product->saleprice);
														echo $price_design['price_design'];
													?> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               <?php }?>   
							
							</div> 
                        </div>
                    </div> 
            </div>
            <!--product area end-->
        </div>
    </div>
<!-- <script>
	const ratingStars = [...document.getElementsByClassName("rating__star")];
	const ratingResult = document.querySelector(".rating__result");

	printRatingResult(ratingResult);

	function executeRating(stars, result) {
	   const starClassActive = "rating__star fa fa-star";
	   const starClassUnactive = "rating__star fa fa-star-o";
	   const starsLength = stars.length;
	   let i;
	   stars.map((star) => {
		  star.onclick = () => {
			 i = stars.indexOf(star);

			 if (star.className.indexOf(starClassUnactive) !== -1) {
				printRatingResult(result, i + 1);
				for (i; i >= 0; --i) stars[i].className = starClassActive;
			 } else {
				printRatingResult(result, i);
				for (i; i < starsLength; ++i) stars[i].className = starClassUnactive;
			 }
		  };
	   });
	}
	function printRatingResult(result, num = 0) {
	   result.textContent = `${num}/5`;
	}
</script> -->
  
<?php
   function theme_img($uri, $tag=false)
   {
   	if($tag)
   	{
   		return '<img src="'.theme_url('assets/assets/images/'.$uri).'" alt="'.$tag.'">';
   	}
   	else
   	{
   		return theme_url('assets/assets/images/'.$uri);
   	}
   }
   function theme_url($uri)
   {
   	$CI =& get_instance();
   	return $CI->config->base_url('/'.$uri);
   }
   $this->load->view('vwFooter');
   ?>
<?php

$f_id		= array('id'=>'f_id', 'style'=>'display:none;', 'name'=>'id', 'value'=> set_value('id',$id));
//$f_address1	= array('id'=>'f_address1', 'class'=>'form-control', 'name'=>'address1', 'value'=>set_value('address1',$address1));
$f_first	= array('id'=>'f_firstname', 'class'=>'form-control', 'name'=>'firstname', 'value'=> set_value('firstname',$firstname));
$f_phone	= array('id'=>'f_phone', 'class'=>'form-control', 'name'=>'phone', 'value'=> set_value('phone',$phone));
$f_alt_phone	= array('id'=>'f_alt_phone', 'placeholder'=>'Alternative Phone (Optional)', 'class'=>'form-control', 'name'=>'alt_phone', 'value'=> set_value('alt_phone',$alt_phone));
$f_landmark	= array('id'=>'f_landmark', 'placeholder'=>'Landmark (Optional)', 'class'=>'form-control', 'name'=>'landmark', 'value'=> set_value('landmark',$landmark));
$f_city		= array('id'=>'f_city', 'class'=>'form-control', 'name'=>'city', 'value'=>set_value('city',$city));
$f_zip		= array('id'=>'f_zip', 'maxlength'=>'10', 'class'=>'form-control', 'name'=>'zip', 'value'=> set_value('zip',$zip));

echo form_input($f_id);

?>
<!-- Modal content-->    
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>
	<h3>Address Form</h3>
</div>
<div class="modal-body default_form">
	<div id="form-error">
		
	</div>
	<div class="row">
		<div class="col-sm-6">
			<label class="form_label">Name</label>
			<?php echo form_input($f_first);?>
		</div>
		<div class="col-sm-6">
			<label class="form_label">Phone</label>
			<?php echo form_input($f_phone);?>
		</div>
		<div class="col-sm-6">
			<label class="form_label">Alternative Phone</label>
			<?php echo form_input($f_alt_phone);?>
		</div>
		<div class="col-sm-6">
			<label class="form_label">Pincode</label>
			<?php echo form_input($f_zip);?>
		</div>
		<div class="col-md-12">
			<label class="form_label">Address</label>
			<textarea type="text" name="address1" id="f_address1" class="form-control" placeholder="Address (Area and Street)"><?php echo set_value('address1',$address1);?></textarea>
		</div>
		<div class="col-md-6 col_md_right">
			<label class="form_label">Landmark</label>
			<?php echo form_input($f_landmark);?>
		</div>
		<div class="col-sm-6 checkout_select_state">
			<label class="form_label">Country</label>
			<?php echo form_dropdown('country_id', $countries_menu, set_value('country_id', $country_id), 'id="f_country_id" class="form-control selectpicker" data-live-search="true"', 'Please choose country');?>
		</div>
		<div class="col-sm-6 checkout_select_state">
			<label class="form_label">State</label>
			<?php echo form_dropdown('zone_id', $zones_menu, set_value('zone_id', $zone_id), 'id="f_zone_id" class="form-control selectpicker" data-live-search="true"', 'Please choose state');?>
		</div>
		<div class="col-sm-6">
			<label class="form_label">City</label>
			<?php echo form_input($f_city);?>
		</div>
	</div>
</div>
<div class="modal-footer myModalFooter">
	<a href="#" class="btn btn-sm btn-danger" data-dismiss="modal">Close</a>
	<a href="#" class="btn btn-sm green_btn" type="button" onclick="save_address(); return false;">Submit</a>
</div>
<script>
$(function(){
	$('#f_country_id').change(function(){
			$.post('<?php echo site_url('locations/get_zone_menu');?>',{id:$('#f_country_id').val()}, function(data) {
			  $('#f_zone_id').html('<option>Please choose state</option>'+data);	
			$('.selectpicker').selectpicker('refresh');
			});
		});
});

function save_address()
{
	$.post("<?php echo site_url('secure/address_form');?>/"+$('#f_id').val(), {	firstname: $('#f_firstname').val(),
																				phone: $('#f_phone').val(),
																				alt_phone: $('#f_alt_phone').val(),
																				zip: $('#f_zip').val(),
																				address1: $('#f_address1').val(),
																				landmark: $('#f_landmark').val(),
																				country_id: $('#f_country_id').val(),
																				city: $('#f_city').val(),
																				zone_id: $('#f_zone_id').val(),
																				},
		function(data){

			if(data == 1)
			{
				window.location = "<?php echo site_url('secure/my_account');?>";
			}
			else
			{
				$('#form-error').html('<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>'+data+'</div>');
			}
		});
}
</script>
<?php $this->load->view('vwHeader');?>

<script>
$(document).ready(function(){	
	$('.delete_address').click(function(){
		if($('.delete_address').length > 1){
			if(confirm('Are you sure you want to delete this address?')){
				$.post("<?php echo site_url('secure/delete_address');?>", { 
					id: $(this).attr('rel') 
				},
				function(data){
					$('#address_'+data).remove();
					$('#address_list .my_account_address').removeClass('address_bg');
					$('#address_list .my_account_address:even').addClass('address_bg');
				});
			}
		}else{
			alert('You Must leave at least 1 address in the Address Manager.');
		}	
	});
	
	$('.edit_address').click(function(){
		$.post('<?php echo site_url('secure/address_form'); ?>/'+$(this).attr('rel'),
			function(data){	
				$('#address-form-container').html(data);
				$('#myModal').modal('show');
			}
		);
	});
});


function set_default(address_id, type){
	$.post('<?php echo site_url('secure/set_default_address') ?>/',{
		id:address_id, type:type
	});
}
</script>

<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner myAccount_breadcum">
			<ul class="list-inline list-unstyled">
				<li><a href="<?php echo base_url();?>">Home</a></li>
				<li><a href="javascript:void(0);" class="active">My Orders</a></li>
			</ul>
		</div>
	</div>
</div>

<div class="body-content outer-top-xs">
	<div class='container'>
		<div class="row">
			<div class="col-md-3 myaccount_left">
				<?php $this->load->view('user_account_sidebar');?>
			</div>    	
			<div class="col-md-9 myaccount_right">
				<div class="row">
					<div class="col-sm-12">
						<h4><b>ORDER HISTORY</b></h4><?php 
						if($orders):
							echo $orders_pagination;?>
						<div class="table-responsive">
						<table class="table table-bordered table-striped">
							<thead>
								<tr class="my_order_th">
									<th>Ordered On</th>
									<th>Order Number</th>
									<th>Status</th>
									<th class="text-center">Details</th>
								</tr>
							</thead>
							<tbody><?php
								foreach($orders as $order): ?>
									<tr>
										<td><?php 
											$d = format_date($order->ordered_on); 
											$d = explode(' ', $d);
											echo $d[0].' '.$d[1].', '.$d[3];?>
										</td>
										<td><?php echo $order->order_number; ?></td>
										<td><?php echo $order->status;?></td>
										<td class="my_order_details">
											<a href="<?php echo base_url()."view/order/order/".$order->order_number;?>" alt="view invoice"><i class="fa fa-eye" aria-hidden="true"></i></a><?php
											if(!empty($order->fedex_track_no)){?>
												| <a href="javascript:void(0);" url="<?php echo base_url()."admin/fedex/track_shipment/".$order->fedex_track_no;?>" onclick="track_api('<?php echo $order->fedex_track_no;?>')" id="button<?php echo $order->fedex_track_no;?>" alt="view invoice">Track</a><br/>
												<div class="row fddiv" id="fedex_response<?php echo $order->fedex_track_no;?>"></div><?php
											}?>
										</td>
									</tr><?php 
								endforeach;?>
							</tbody>
						</table>
						</div>
						<?php else: ?>
							<b>You have no orders in your account history.</b>
						<?php endif;?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><?php 
$this->load->view('vwFooter'); ?>
<script>
	function track_api(orderid){
		$(".fddiv").html("");
		url = $("#button"+orderid).attr('url');
		$.ajax({
			type: "POST",  					
			url: url,
			beforeSend:function(){
				$.blockUI({
					message:"Just a moment.",
					css: {
						border: 'none', 
						padding: '15px',
						message: 'Just a moment.',
						backgroundColor: '#000', 
						'-webkit-border-radius': '10px', 
						'-moz-border-radius': '10px', 
						opacity: .5, 
						color: '#fff' 
					}
				});
			},			
			success: function(msg){
				$("#fedex_response"+orderid).html(msg);
				setTimeout($.unblockUI, 2000);
			}
		});	
	}

	function popitup(url){
		newwindow = window.open(url,'name','height=600,width=800');
		if (window.focus){
			newwindow.focus()
		}
		return false;
	}
</script>
<?php $this->load->view('vwHeader');?>
<style>#showMenu{display:none;}</style>

<section class="container">
	<div class="page-header">
		<h2>Order Number: <?php echo $order_id;?></h2>
	</div>



<div class="row">
	<div class="col-sm-4">
		<h3>Account Information</h3>
		<?php echo (!empty($customer['company']))?$customer['company'].'<br/>':'';?>
		<?php echo $customer['firstname'];?> <?php echo $customer['lastname'];?><br/>
		<?php echo $customer['email'];?> <br/>
		<?php echo $customer['phone'];?>
	</div>
	<?php
	$ship = $customer['ship_address'];
	$bill = $customer['bill_address'];
	?>
	<div class="col-sm-4">
		<h3><?php echo ($ship != $bill)?'Shipping Information':'Shipping &amp; Billing Information';?></h3>
		<?php echo format_address($ship, TRUE);?><br/>
		<?php echo $ship['email'];?><br/>
		<?php echo $ship['phone'];?>
	</div>
	<?php if($ship != $bill):?>
	<div class="col-sm-4">
		<h3>Billing Information</h3>
		<?php echo format_address($bill, TRUE);?><br/>
		<?php echo $bill['email'];?><br/>
		<?php echo $bill['phone'];?>
	</div>
	<?php endif;?>
</div>

<div class="row">
	<div class="col-sm-4">
		<h3>Additional Details</h3>
		<?php
		if(!empty($referral)):?><div><strong>How did you hear about us?</strong> <?php echo $referral;?></div><?php endif;?>
		<?php if(!empty($shipping_notes)):?><div><strong>Shipping Notes</strong> <?php echo $shipping_notes;?></div><?php endif;?>
	</div>

	<div class="col-sm-4">
		<h3 style="padding-top:10px;">Shipping Method</h3>
		<?php echo $shipping['method']; ?>
	</div>
	
	<div class="col-sm-4">
		<h3>Payment Information</h3>
		<?php echo $payment['description']; ?>
	</div>
	
</div>

<table class="table table-bordered table-striped" style="margin-top:20px;">
	<thead>
		<tr>
			<th style="width:10%;">SKU</th>
			<th style="width:20%;">Name</th>
			<th style="width:10%;">Price</th>
			<th>Description</th>
			<th style="width:10%;">Quantity</th>
			<th style="width:8%;">Totals</th>
		</tr>
	</thead>
	
	<tfoot>
		

		<tr>
			<td colspan="5"><strong>Subtotal</strong></td>
			<td><?php echo $natuur['subtotal']; ?></td>
		</tr>
		
		<?php if($natuur['coupon_discount'] > 0)  : ?> 
		<tr>
			<td colspan="5"><strong>Coupon Discount</strong></td>
			<td><?php echo (0-$natuur['coupon_discount']); ?></td>
		</tr>

		<?php if($natuur['order_tax'] != 0) : // Only show a discount subtotal if we still have taxes to add (to show what the tax is calculated from) ?> 
		<tr>
			<td colspan="5"><strong>Discounted Subtotal</strong></td>
			<td><?php echo $natuur['discounted_subtotal']; ?></td>
		</tr>
		<?php endif;

		endif; ?>
		<?php // Show shipping cost if added before taxes
		if($this->config->item('tax_shipping') && $natuur['shipping_cost']>0) : ?>
		<tr>
			<td colspan="5"><strong>Shipping</strong></td>
			<td><?php echo $natuur['shipping_cost']; ?></td>
		</tr>
		<?php endif ?>
		
		<?php if($natuur['order_tax'] != 0) : ?> 
		<tr>
			<td colspan="5"><strong>Taxes</strong></td>
			<td><?php echo $natuur['order_tax']; ?></td>
		</tr>
		<?php endif;?>
		
		<?php // Show shipping cost if added after taxes
		if(!$this->config->item('tax_shipping') && $natuur['shipping_cost']>0) : ?>
		<tr>
			<td colspan="5"><strong>Shipping</strong></td>
			<td><?php echo $natuur['shipping_cost']; ?></td>
		</tr>
		<?php endif;?>
		
		
		<tr> 
			<td colspan="5"><strong>Grand Total</strong></td>
			<td><?php echo $natuur['total']; ?></td>
		</tr>
	</tfoot>

	<tbody>
	<?php
	$subtotal = 0;
	foreach ($natuur['contents'] as $cartkey=>$product):?>
		<tr>
			<td><?php echo $product['sku'];?></td>
			<td><?php echo $product['name']; ?></td>
			<td><?php echo $product['base_price'];?></td>
			<td><?php echo $product['excerpt'];
				if(isset($product['options'])) {
					foreach ($product['options'] as $name=>$value)
					{
						if(is_array($value))
						{
							echo '<div><span class="gc_option_name">'.$name.':</span><br/>';
							foreach($value as $item)
								echo '- '.$item.'<br/>';
							echo '</div>';
						} 
						else 
						{
							echo '<div><span class="gc_option_name">'.$name.':</span> '.$value.'</div>';
						}
					}
				}
				?></td>
			<td><?php echo $product['quantity'];?></td>
			<td><?php echo $product['price']*$product['quantity']; ?></td>
		</tr>
			
	<?php endforeach; ?>
	</tbody>
</table>
</section>
<?php $this->load->view('vwFooter');?>
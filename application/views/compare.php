<?php $this->load->view('vwHeader');?>
<?php $tot_wish =  count($product_records);?>
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="<?php echo base_url();?>">Home</a></li>
				<li class='active'>Compare</li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->

<div class="body-content outer-top-xs">
	<div class="container">
		<div class="product-comparison">
			<div>
				<h1 class="page-title text-center heading-title">Product Comparison</h1>
				<?php include('checkout/compare_summary.php');?>
			</div>
		</div>
	</div>
</div><?php
 
function theme_img($uri, $tag=false){
	if($tag){
		return '<img src="'.theme_url('assets/assets/images/'.$uri).'" alt="'.$tag.'">';
	}else{
		return theme_url('assets/assets/images/'.$uri);
	}
}

function theme_url($uri){
	$CI =& get_instance();
	return $CI->config->base_url('/'.$uri);
} ?>

<?php $this->load->view('vwFooter');?>
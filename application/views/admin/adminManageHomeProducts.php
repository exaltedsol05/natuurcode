<?php $this->load->view('admin/adminHeader');?>
<?php $this->load->view('admin/adminLeftSidebar');?>
 <!-- Right side column. Contains the navbar and content of the page -->

	   
<script type="text/javascript">
function areyousure()
{
	return confirm('Are you sure to delete this type?');
}
</script>


 <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Feature Products
                        <small>admin panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#"> Catalog</a></li>
                        <li class="active"> Feature Products</li>
                    </ol>
                </section>
                <?php $this->load->view('admin/adminError');?>
                <!-- Main content -->
                <section class="content">
				    <div class="row">						
                        <div class="col-sm-12">
                           
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Display all Feature Products</h3>
                                </div><!-- /.box-header -->
								
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>IN</th>
                                                <th>sku</th>
                                                <th>name</th>
                                                <th>price</th>
                                                <th>saleprice</th>
                                                <th>quantity</th>
                                                <th>status</th>
												
                                            </tr>
                                        </thead>
                                        <tbody>
										 <?php 
											foreach ($arrHomeProducts as $cat){ ?>
												<tr>
												<td><?php echo  $cat->id; ?></td>
                                                <td><?php echo  $cat->sku; ?></td>
												<td><?php echo  $cat->name; ?></td>
                                                <td><?php echo  $cat->price; ?></td>
                                                <td><?php echo  $cat->saleprice; ?></td>
                                                <td><?php echo  $cat->quantity; ?></td>
												<td><?php echo ($cat->home_enabled == '1') ? 'Enabled' : 'Disabled'; ?></td>
												</tr>
										      <?php } ?>
                                            
											
                                        </tbody>
                                        
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                  
                    </div>
                </section><!-- /.content -->
</div><!-- /.right-side -->
<script>
  $(function () {
	
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": true,
	  "pageLength": 20,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>
<?php $this->load->view('admin/adminFooter');?>
   
	<footer class="main-footer">
    <div class="pull-right hidden-xs">
  
    </div>
    <strong>Copyright &copy; 2016-2017<a href="#">Eweblabs Pvt. Ltd.</a>.</strong> All rights
    reserved.
  </footer>
        <div id="resetpassModal" class="modal fade">
		   <div class="modal-dialog" style="color:#333;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">✕</button>
					<h4 class="modal-title">Change Your Password</h4>
				</div>
				<div class="modal-body">
				    
					<div id="forgotpassword">
                            <form method="POST" action="" name="forgot_password" id="forgot_password">
							
							<p>
								<input type="email" class="form-control" name="adminemail" id="adminemail" placeholder="Email">
							</p>
							<p>
								<input type="password" class="form-control" name="oldpassword" id="oldpassword" placeholder="Old Password">
							</p>
							<p>
								<input type="password" class="form-control" name="newpassword" id="newpassword" placeholder="New Password">
							</p>
							<p>
							<button type="submit" class="btn btn-primary">Submit</button>
						
							</p>
							<p id="message">
							</p>
							
						</form>
					</div>
				</div>

			    <div class="modal-footer">
                 
                </div>
			</div>
		</div>
	    </div>

	
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
	
<!-- FastClick -->
<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>app.js"></script>
<!-- Sparkline -->
<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
 <!-- iCheck -->
 <script src="<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/iCheck/icheck.min.js" type="text/javascript"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>demo.js"></script>

         <!-- Placed at the end of the document so the pages load faster -->
		 <script src="<?php echo HTTP_JS_PATH_ADMIN; ?>jquery.validate.min.js" type="text/javascript"></script>
		  <?php if($this->session->userdata('usertype')==''){?>
			<style>
			.modal-backdrop.in {
				
				opacity: 4.5;
				
			}
			</style>
			<?php } ?>
		 <script> 
		$(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();			
			$("#forgot_password").validate({
				rules: {
					adminemail  : "required",
					oldpassword : "required",
					newpassword : {
					               required:true,
								   equalTo: "#oldpassword"
								}
                    					
				},
				success: "valid",
				submitHandler: function(form) {
						$.ajax({
							url: '<?php echo base_url(); ?>admin/home/do_reset',
							type: 'post',
							dataType: 'json',
							data: $('form#forgot_password').serialize(),
							success: function(data) {
							   
							  $('#forgot_password')[0].reset();		 
							  $('#message').html('<div class="alert alert-dismissible alert-success"><strong>'+data+'</strong></div>');
							  
							  setTimeout(function(){ $('#resetpassModal').modal('hide'); $('#message').html(''); }, 5000);
							}
						});
				}
			});

           $("li").each(function(){
		        if($(this).hasClass('active'))
		        {
		          $(this).parent().show();
		          $(this).parent().prev().attr('class','open');
		        }
		    });

	     });
		 
		 
		//iCheck for checkbox and radio inputs
		$('input[type="checkbox"], input[type="radio"]').iCheck({
			checkboxClass: 'icheckbox_minimal-blue',
			radioClass: 'iradio_minimal-blue'
		});
		
		</script>
		<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>main.js"></script>
  </body>
</html>

<?php $this->load->view('admin/adminHeader');?>
<?php $this->load->view('admin/adminLeftSidebar');?>

<?php
//set "code" for searches;

if(!$code)
{
	$code = '';
}
else
{
	$code = '/'.$code;
}
function sort_url($lang, $by, $sort, $sorder, $code, $admin_folder)
{
   //echo $lang.'--'.$by.'--'.$sort.'--'.$sorder.'--'.$code.'--'.$admin_folder;
   
	if ($sort == $by)
	{
		if ($sorder == 'asc')
		{
			$sort	= 'desc';
			$icon	= ' <i class="icon-chevron-up"></i>';
		}
		else
		{
			$sort	= 'asc';
			$icon	= ' <i class="icon-chevron-down"></i>';
		}
	}
	else
	{
		$sort	= 'asc';
		$icon	= '';
	}
		

	$return = site_url('admin/products/index/'.$by.'/'.$sort.'/'.$code);
	
	return '<a href="'.$return.'">'.$lang.'</a>';

}
$msg='';
if(!empty($term)):
	$term = json_decode($term);
	if(!empty($term->term) || !empty($term->category_id)):
		$msg='<div class="alert alert-info">'.sprintf("Your searched returned %d result(s)", intval($total)).'</div>';
	 endif;
 endif;?>

<script type="text/javascript">
function areyousure()
{
	return confirm('Are you sure you want to delete this product?');
}
</script>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
			Products
			<small>Control panel</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Products</li>
			</ol>
		</section>
		<?php $this->load->view('admin/adminError');?>
		<section class="content">
		       
		       <div class="row">
			      <div class="col-sm-12">
				     <div class="box box-primary">
						<?php echo $msg;?>
						<div class="box-header">						
						   
							<?php echo form_open('admin/products/index', 'class="form-inline" style="float:right"');?>
							  <fieldset>
							<?php
							
							function list_categories($id, $categories, $sub='') {

								foreach ($categories[$id] as $cat):?>
								<option  value="<?php echo $cat->id;?>"><?php echo  $sub.$cat->name; ?></option>
								<?php
								if (isset($categories[$cat->id]) && sizeof($categories[$cat->id]) > 0)
								{
									$sub2 = str_replace('&rarr;&nbsp;', '&nbsp;', $sub);
									$sub2 .=  '&nbsp;&nbsp;&nbsp;&rarr;&nbsp;';
									list_categories($cat->id, $categories, $sub2);
								}
								endforeach;
							}
							
							if(!empty($categories))
							{
								echo '<select name="category_id" class="form-control">';
								echo '<option value="">Filter by Category</option>';
								list_categories(0, $categories);
								echo '</select>';
								
							}?>
							
							<input type="text" class="form-control" name="term" placeholder="Search Term" /> 
							<button class="btn" name="submit" value="search"><i class="fa fa-search"></i> Search</button>
							<a class="btn btn-info" href="<?php echo site_url('admin/products/index');?>"><i class="fa fa-undo"></i> Reset</a>
						</fieldset>
							   </form>
						        <a class="btn btn-primary" data-toggle="tooltip" title="Add New Product" style="font-weight:normal;" href="<?php echo site_url('admin/products/form');?>"><i class="fa fa-plus"></i> Add New Product </a>
						</div>
					    <div class="box-body">
						
						
						
				
					 

							<?php echo form_open('admin/products/bulk_save', array('id'=>'bulk_form'));?>
								<table class="table table-bordered table-hover">
									<thead>
										<tr>
										    <th style="width:50px;">S.No.</th>
											<th><?php echo sort_url('SKU <i class="fa fa-sort" aria-hidden="true"></i>', 'sku', $order_by, $sort_order, $code,'admin');?> </th>
											<th><?php echo sort_url('NAME <i class="fa fa-sort" aria-hidden="true"></i>', 'name', $order_by, $sort_order, $code, 'admin');?> </th>
											<th style="width:100px;"><?php echo sort_url('PRICE <i class="fa fa-sort" aria-hidden="true"></i>', 'price', $order_by, $sort_order, $code,'admin');?> </th>
											<th style="width:100px;"><?php echo sort_url('SALE PRICE <i class="fa fa-sort" aria-hidden="true"></i>', 'saleprice', $order_by, $sort_order, $code, 'admin');?> </th>
											<th style="width:100px;" ><?php echo sort_url('QUANTITY <i class="fa fa-sort" aria-hidden="true"></i>', 'quantity', $order_by, $sort_order, $code, 'admin');?> </th>
											<th ><?php echo sort_url('ENABLED <i class="fa fa-sort" aria-hidden="true"></i>', 'enabled', $order_by, $sort_order, $code, 'admin');?> </th>
											<th>FeatureProduct</th>
											<th style="width:140px;">
												<span class="btn-group pull-right">
													<button class="btn" data-toggle="tooltip" title="Bulk Save"><i class="fa fa-send"></i> Bulk Save </button>
													
												</span>
											</th>
										</tr>
									</thead>
									<tbody>
									<?php echo (count($products) < 1)?'<tr><td style="text-align:center;" colspan="7">There are currently no products.</td></tr>':''?>
									<?php $i=0;foreach ($products as $product): $i++;?>
									<tr>
									    <td><?php echo $i;?>
										</td>
										<td><?php echo form_input(array('name'=>'product['.$product->id.'][sku]','value'=>$product->sku, 'class'=>'form-control'));?></td>
										<td><?php echo form_input(array('name'=>'product['.$product->id.'][name]','value'=>$product->name, 'class'=>'form-control'));?></td>
										<td><?php echo form_input(array('name'=>'product['.$product->id.'][price]', 'value'=>set_value('price', $product->price), 'class'=>'form-control'));?></td>
										<td><?php echo form_input(array('name'=>'product['.$product->id.'][saleprice]', 'value'=>set_value('saleprice', $product->saleprice), 'class'=>'form-control'));?></td>
										<td><?php echo ((bool)$product->track_stock)?form_input(array('name'=>'product['.$product->id.'][quantity]', 'value'=>set_value('quantity', $product->quantity), 'class'=>'form-control')):'N/A';?></td>
										<td>
											<?php
												$options = array(
													  '1'	=> 'Yes',
													  '0'	=>'No'
													);

												echo form_dropdown('product['.$product->id.'][enabled]', $options, set_value('enabled',$product->enabled), 'class="form-control"');
											?>
										</td>
										<td>
											<?php
												$options = array(
													  '1'	=> 'Yes',
													  '0'	=>'No'
													);

												echo form_dropdown('product['.$product->id.'][home_enabled]', $options, set_value('home_enabled',$product->home_enabled), 'class="form-control"');
											?>
										</td>
										<td>
											<span class="btn-group pull-right">
											
												<a class="btn btn-info"  data-toggle="tooltip" title="Edit" href="<?php echo  site_url('admin/products/form/'.$product->id);?>"><i class="fa fa-pencil"></i></a>
												<a class="btn btn-danger" data-toggle="tooltip" title="Delete"  href="<?php echo  site_url('admin/products/delete/'.$product->id);?>" onclick="return areyousure();"><i class="fa fa-trash-o"></i>
												</a>
											</span>
										</td>
									</tr>
								<?php 	endforeach; ?>
									</tbody>
								</table>
						 </form>
					    </div>
						<div class="box-footer">
						    <div class="col-sm-12">
								  <nav class="pull-right">
										<?php echo $this->pagination->create_links();?>	&nbsp;
								  </nav>
							</div>
						</div>
				  </div>
				  </div>
			</div>
		</section>
</div>
<?php
$this->load->view('admin/adminFooter');
?>
<?php
$this->load->view('admin/adminHeader');
?>
<?php
$this->load->view('admin/adminLeftSidebar');
?>
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                       Add Gift Card
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Add Gift Card</li>
                    </ol>
                </section>
				<?php $this->load->view('admin/adminError');?>
					
                <!-- Main content -->
                <section class="content">

<?php echo form_open('admin/giftcards/form/'); ?>

	<label for="to_name">Recipient Name</label>
	<?php
	$data	= array('name'=>'to_name', 'value'=>set_value('code'), 'class'=>'form-control');
	echo form_input($data);
	?>

	<label for="to_email">Recipient Email</label>
	<?php
	$data	= array('name'=>'to_email', 'value'=>set_value('to_email'), 'class'=>'form-control');
	echo form_input($data);
	?>

	<label class="checkbox">
	<?php
	$data	= array('name'=>'send_notification', 'value'=>'true');
	echo form_checkbox($data);
	?>
	Send Email Notification</label>

	<label for="sender_name">Sender Name</label>
	<?php
	$data	= array('name'=>'from', 'value'=>set_value('from'), 'class'=>'form-control');
	echo form_input($data);
	?>

	<label for="personal_message">Personal Message</label>
	<?php
	$data	= array('name'=>'personal_message', 'value'=>set_value('personal_message'), 'class'=>'form-control');
	echo form_textarea($data);
	?>

	<label for="beginning_amount">Amount</label>
	<?php
	$data	= array('name'=>'beginning_amount', 'value'=>set_value('beginning_amount'), 'class'=>'form-control');
	echo form_input($data);
	?>
	
	<div class="form-actions">
		<input class="btn btn-primary" type="submit" value="save"/>
	</div>
	
</form>
</section><!-- /.content -->
            </aside><!-- /.right-side -->
    
<?php
$this->load->view('admin/adminFooter');
?>
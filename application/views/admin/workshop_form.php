<?php $this->load->view('admin/adminHeader');?>
<?php $this->load->view('admin/adminLeftSidebar');?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php if(!empty($id)){echo "Edit";}else{echo "Add";}?> Blog  
			<small>admin panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#"> Catalog</a></li>
			<li class="active"> <?php if(!empty($id)){echo "Edit";}else{echo "Add";}?> Workshop  </li>
		</ol>
	</section>
	<?php $this->load->view('admin/adminError');?>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-body">							         
						<section class="content">
							<!-- Main row -->
							<div class="row">
							<section class="col-sm-12">
									
									
									<!-- Custom tabs (Charts with tabs)--><?php 
									$attributes = array("name" => "workshop_form", "id" => "workshop_form");
									echo form_open_multipart("admin/workshops/form/".$id, $attributes); ?>
										<fieldset>
											<div class="row">
												<div class="col-sm-4">
													<div class="form-group">
														<label for="email"><strong>Date</strong></label><?php
														$data	= array('name'=>'workshop_date', 'value'=>set_value('workshop_date', $workshop_date), 'class'=>'form-control datepicker');
														echo form_input($data); ?>
													</div>
												</div>
												<div class="col-sm-4">
													<div class="form-group bootstrap-timepicker">
														<label for="email"><strong>Time</strong></label><?php
														$data	= array('name'=>'workshop_time', 'value'=>set_value('workshop_time', $workshop_time), 'class'=>'form-control timepicker');
														echo form_input($data); ?>
													</div>
												</div>
												<div class="col-sm-4">
													<div class="form-group">
														<label for="email"><strong>Number</strong></label><?php
														$data	= array('name'=>'number', 'value'=>set_value('number', $number), 'class'=>'form-control');
														echo form_input($data); ?>
													</div>
												</div>
												<input type="hidden" name="status" value="<?php echo set_value('status', $status); ?>">
												<div class="col-sm-4">
													<div class="form-group">
														<label for="workshop_type"><strong>Type</strong></label>
														<select class="form-control" style="min-width:100px;" name="workshop_type" id="workshop_type">
															<option value="" >Select Workshop Type</option>
															<option value="1" <?php if($workshop_type == "1") { echo "selected = 'selected'"; } ?>>Free</option>
															<option value="2" <?php if($workshop_type == "2") { echo "selected = 'selected'"; } ?>>Paid</option>
														</select>
													</div>	
												</div>
												<div class="col-sm-4">
													<div class="form-group">
														<label for="email"><strong>Price</strong></label><?php
														$data	= array('name'=>'workshop_price', 'value'=>set_value('workshop_price', $workshop_price), 'class'=>'form-control');
														echo form_input($data); ?>
													</div>
												</div>	
											</div>	
											<div class="row">
												<div class="form-group pull-left">
													<button type="submit" class="btn btn-primary" style="margin:10px 0px 10px 10px;">Save</button>
												</div>
											</div>	
										</fieldset><?php
									echo form_close();?>
								</section><!-- /.Left col -->
							</div><!-- /.row (main row) -->
						</section><!-- /.content -->
					</div>
					<div class="box-footer"></div>				 
				</div>
			</div>
		</div>
	</section><!-- /.content -->
</div><!-- /.right-side -->

<script type="text/javascript">
$(document).ready(function(){
	 $('.timepicker').timepicker({
      showInputs: false
    })
	$('.datepicker').datepicker({startDate: "1d",format: 'yyyy-mm-dd',todayHighlight: true,autoclose:true});	
});
$('form').submit(function() {
	$('.btn').attr('disabled', true).addClass('disabled');
});
</script>
<?php $this->load->view('admin/adminFooter');?>
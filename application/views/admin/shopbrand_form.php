<?php $this->load->view('admin/adminHeader');?>
<?php $this->load->view('admin/adminLeftSidebar');?>
<link href="<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/uploadify/uploadify.css" rel="stylesheet" type="text/css" />
<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/uploadify/jquery.uploadify.min.js" type="text/javascript"></script>
<div class="content-wrapper">
                <!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
		Shop By Brand Form
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Shop By Brand Form</li>
		</ol>
	</section>
	<?php $this->load->view('admin/adminError');?>
					
     <!-- Main content -->
     <section class="content">
                   
		<!-- Main row -->
		<div class="row">
			<!-- Left col -->
			
			<div class="col-sm-12">
			    <div class="box box-primary">
					<div class="box-header">
						<div class="col-sm-3">
							<label>Brand Image</label>
							<form>
							<div id="queue"></div>
							<input id="brand_image" name="brand_image" type="file" multiple="true">
							</form>
						</div>
						<div class="col-sm-3" id="brand_upload">
							<img alt="Brand Image" class="img-thumbnail" src="<?php echo base_url(); ?>uploads/Brand/small/<?php echo $brand_image; ?>" style="height:100px; width:130px;">			
						</div>
                        
						<div class="col-sm-3">
							<label>Brand Banner Image</label>
							<form>
								<div id="queue"></div>
								<input id="brand_banner_image" name="brand_banner_image" type="file" multiple="true">
							</form>

						</div>
						<div class="col-sm-3" id="brand_banner_upload">
							<img alt="Brand Banner Image" class="img-thumbnail" src="<?php echo base_url(); ?>uploads/Brand/small/<?php echo $brand_banner_image; ?>" style="height:100px; width:130px;">			
						</div>
					</div>
					<?php echo form_open_multipart('admin/shopbrands/form/'.$id); ?>
					  <div class="box-body">
							<!-- Custom tabs (Charts with tabs)-->
							
							<input type="hidden" value="<?php echo $brand_image; ?>" name="brand_add_image" id="brand_add_image">
							<input type="hidden" value="<?php echo $brand_banner_image; ?>" name="brand_add_banner_image" id="brand_add_banner_image">					   
								<div class="form-group">
									<label for="cat_name">Name</label>
									<?php
									$data	= array('name'=>'name', 'value'=>set_value('name', $name), 'class'=>'form-control');
									echo form_input($data);
									?>
								</div>
								<div class="form-group">
									<label for="cat_enabled">Enabled </label>
									<?php echo form_dropdown('enabled', array('0' => 'Disabled', '1' =>'Enabled'), set_value('enabled',$enabled),'class=form-control'); ?>	
								</div>
								  
								
						
							
					  </div>
					  <div class="box-footer">
					       <div class="form-group">
								<div class="pull-left">
									<button type="submit" class="btn btn-primary" >Save</button>
								</div>
								<div class="pull-right">
									<a class="btn btn-primary"  href="<?php echo site_url('admin/shopbrands/'); ?>"><i class="fa fa-eye"></i> view Shop By Brand</a>
								</div>
							</div>
					</div>
					</form>	
				</div>				
			</div><!-- /.Left col -->
                       
					</div><!-- /.row (main row) -->

                </section><!-- /.content -->
</div><!-- /.right-side -->
<script type="text/javascript">
$(document).ready(function(){
	
		$('#brand_image').uploadify({
					
			'formData'     : {
				'flag'      : 'brand_images',
				  'tId'      : '<?php echo $id ?>'
			},
			'onSelect' : function(file) {
				$('#manu_upload').html('<img class="img-circle img-responsive" src="<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/uploadify/ajax-loader1.gif">');
			 },
			'buttonImage' : '<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/uploadify/browse-btn.png',
			'buttonText' : 'Add Brand Image..',
			'multi': false,
			'swf'      : '<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/uploadify/uploadify.swf',
			'uploader' : '<?php echo base_url(); ?>admin/shopbrands/upload_brand_img',
			'onUploadSuccess': function (file, data, response) {
				
					var extension = file.name.replace(/^.*\./, '');
					var a=$.parseJSON(data);
					var imgName=a.imagename;
					
					$('#brand_upload').html('<img alt="Brand Image" class="img-thumbnail" src="<?php echo base_url(); ?>uploads/Brand/small/'+imgName+'" style="height:100px; width:130px;">');
					$("#brand_add_image").val(imgName);
				
				}
			
		});
		
		
		$('#brand_banner_image').uploadify({
			'formData'     : {
				'flag'      : 'brand_banner_image',
				  'tId'      : '<?php echo $id ?>'
			},
			'onSelect' : function(file) {
				$('#manu_upload').html('<img class="img-circle img-responsive" src="<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/uploadify/ajax-loader1.gif">');
			 },
			'buttonImage' : '<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/uploadify/browse-btn.png',
			'buttonText' : 'Add Brand Banner Image..',
			'multi': false,
			'swf'      : '<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/uploadify/uploadify.swf',
			'uploader' : '<?php echo base_url(); ?>admin/shopbrands/upload_brand_img',
			'onUploadSuccess': function (file, data, response) {
					var extension = file.name.replace(/^.*\./, '');
					var a=$.parseJSON(data);
					var imgName=a.imagename;
					
					$('#brand_banner_upload').html('<img alt="Brand Image" class="img-thumbnail" src="<?php echo base_url(); ?>uploads/Brand/small/'+imgName+'" style="height:100px; width:130px;">');
					$("#brand_add_banner_image").val(imgName);
				}
		});
});
</script>
    
<?php
$this->load->view('admin/adminFooter');
?>
 <script type="text/javascript">
$('form').submit(function() {
	$('.btn').attr('disabled', true).addClass('disabled');
});
</script>  
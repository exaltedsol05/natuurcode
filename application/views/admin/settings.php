<?php $this->load->view('admin/adminHeader');
$this->load->view('admin/adminLeftSidebar'); ?>
<div class="content-wrapper">
	<section class="content-header">		<h1>			Admin Configuration			<small>admin panel</small>		</h1>		<ol class="breadcrumb">			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>			<li><a href="#"> Catalog</a></li>			<li class="active">Configuration</li>		</ol>	</section>	<?php $this->load->view('admin/adminError');?>	<!-- Main content -->
    <section class="content">		<?php echo form_open_multipart('admin/settings');?>			<div class="row">				 <div class="col-md-12">						<div class="box box-primary">								<div class="box-header"></div>								<div class="box-body">									<legend>Shop Details</legend>									<div class="col-md-6">										<div class="form-group">											<label>Company Name</label>											<?php echo form_input(array('class'=>'form-control', 'name'=>'company_name', 'value'=>set_value('company_name', $company_name)));?>										</div>									</div>									<div class="col-md-6">										<div class="form-group">											 <label>Cart Email</label>											 <?php echo form_input(array('class'=>'form-control', 'name'=>'email', 'value'=>set_value('email', $email)));?>										</div>									</div>									<legend>Ship From Address</legend>									<div class="col-md-6">										<div class="form-group">											 <label>Country</label>											<?php echo form_dropdown('country_id', $countries_menu, set_value('country_id', $country_id), 'id="country_id" class="form-control"');?>										</div>									</div>									<div class="col-md-6">										<div class="form-group">											<label>Address1</label>											<?php echo form_input(array('name'=>'address1', 'class'=>'form-control','value'=>set_value('address1',$address1)));?>										</div>									</div>																		<div class="col-md-6">										<div class="form-group">											<label>Address2</label>											<?php echo form_input(array('name'=>'address2', 'class'=>'form-control','value'=> set_value('address2',$address2)));?>										</div>									</div>									<div class="col-md-6">										<div class="form-group">											<label>City</label>											<?php echo form_input(array('name'=>'city','class'=>'form-control', 'value'=>set_value('city',$city)));?>										</div>									</div>									<div class="col-md-6">										<div class="form-group">											<label>State</label>											<?php echo form_dropdown('zone_id', $zones_menu, set_value('zone_id', $zone_id), 'id="zone_id" class="form-control"');?>										</div>									</div>									<div class="col-md-6">										<div class="form-group">											<label>Zip</label>											<?php echo form_input(array('maxlength'=>'10', 'class'=>'form-control', 'name'=>'zip', 'value'=> set_value('zip',$zip)));?>										</div>									</div>								</div>								<div class="box-footer">									  <input class="btn btn-primary" type="submit" value="Save"/>								</div>						</div>				 </div>			</div>		</form>
<!--<fieldset>
    <legend>Locale &amp; Currency</legend>

    <div class="row">
        <div class="col-sm-6">
            <label>Locale</label>
            <?php echo form_dropdown('locale', $locales, set_value('locale', $locale), 'class="form-control"');?>
        </div>
        <div class="col-sm-6">
            <label>Currency</label>
            <?php echo form_dropdown('currency_iso', $iso_4217, set_value('currency_iso', $currency_iso), 'class="form-control"');?>
        </div>
        
    </div>

</fieldset>

<fieldset>
    <legend>Security</legend>
   
    <input type="hidden" name="theme" value="default" class="form-control">
    <input type="hidden" name="admin_folder" value="admin" class="form-control">
    <label class="checkbox">
        <?php echo form_checkbox('ssl_support', '1', set_value('ssl_support',$ssl_support));?> Use SSL
    </label>

     <label class="checkbox">
        <?php echo form_checkbox('require_login', '1', set_value('require_login',$require_login));?> Require login to checkout.
     </label>

    <label class="checkbox">
        <?php echo form_checkbox('new_customer_status', '1', set_value('new_customer_status',$new_customer_status));?> New customers are automatically activated.
    </label>

</fieldset>

<fieldset>
    <legend>Package Details</legend>

    <div class="row">
        <div class="col-sm-3">
            <label>Weight Unit (LB, OZ, KG, etc.)</label>
            <?php echo form_input(array('name'=>'weight_unit', 'class'=>'span3','value'=>set_value('weight_unit',$weight_unit)));?>    
        </div>
        <div class="col-sm-3">
            <label>Dimensional Unit (FT, IN, CM, etc.)</label>
            <?php echo form_input(array('name'=>'dimension_unit', 'class'=>'span3','value'=>set_value('dimension_unit',$dimension_unit)));?>
        </div>
    </div>
</fieldset>

<fieldset>
    <legend>Orders &amp; Inventory</legend>

    <table class="table">
        <thead>
            <tr>
                <th>Default Order Status</th>
                <th>Order Status Options</th>
                <th style="text-align:right;">
					<div class="input-group">
						<input type="text" value="" id="new_order_status_field" class="form-control" placeholder="Status Name"/>
						<span class="input-group-btn">
							 <button type="button" class="btn btn-primary" onclick="add_status()"><i class="fa fa-plus"></i></button>
						</span>
					</div>
				  
                </th>
            </tr>
        </thead>
        <tbody id="order_statuses">
        <?php
        $statuses = json_decode($order_statuses, true);
        
        if(!is_array($statuses))
        {
            $statuses = array();
        }
        foreach($statuses as $os):?>
            <tr>
                <td><input type="radio" value="<?php echo htmlentities($os);?>" name="order_status" <?php echo set_radio('order_status', $os, ($os == $order_status)?true:false)?>></td>
                <td><?php echo htmlentities($os);?></td>
                <td style="text-align:right;">
                    <button type="button" onclick="if(confirm('Are you sure you want to remove this order status?')){ delete_status($(this).parent().siblings().first().html()); $(this).parent().parent().remove();}" class="btn btn-danger">
                        <i class="fa fa-remove fa-white"></i>
                    </button>
                </td>
            </tr>
        <?php endforeach;?>
    </table>
    <?php echo form_textarea(array('name'=>'order_statuses', 'value'=>set_value('order_statuses',$order_statuses), 'id'=>'order_statuses_json'));?>   

    <label class="checkbox">
        <?php echo form_checkbox('inventory_enabled', '1', set_value('inventory_enabled',$inventory_enabled));?> Enable Inventory Tracking
    </label>

    <label class="checkbox">
        <?php echo form_checkbox('allow_os_purchase', '1', set_value('allow_os_purchase',$allow_os_purchase));?> Allow customers to buy items that are out of stock
    </label>

</fieldset>

<fieldset>
    <legend>Tax Settings</legend>

    <label>Base sales tax of which address?</label>
    <?php  $address_options = array('ship'=>'Shipping Address', 'bill'=>'Billing Address');?>
    <?php echo form_dropdown('tax_address', $address_options, set_value('tax_address',$tax_address));?>
    
    <label class="checkbox">
        <?php echo form_checkbox('tax_shipping', '1', set_value('tax_shipping',$tax_shipping));?> Charge tax on shipping rates
    </label>

</fieldset>-->
<script>	var order_statuses = <?php echo $order_statuses;?>;	function add_status()	{		var status = $('#new_order_status_field').val();		order_statuses[htmlEntities(status)] = htmlEntities(status);		var os_submission = JSON.stringify(order_statuses);		$('#order_statuses_json').val(os_submission);		var row = '<tr><td><input type="radio" value="' + htmlEntities(status) + '" name="order_status"></td><td>' + htmlEntities(status) + '</td><td style="text-align:right;"><button type="button" onclick="if(confirm(\'Are you sure you want to remove this order status?\')){delete_status($(this).parent().siblings().first().html()); $(this).parent().parent().remove();}" class="btn btn-danger"><i class="icon-remove icon-white"></i></button></td></tr>';		$('#order_statuses').append(row);		$('#new_order_status_field').val('')	}	function delete_status(status)	{		order_statuses[status] = undefined;		var os_submission = JSON.stringify(order_statuses);		$('#order_statuses_json').val(os_submission);	}	$(document).ready(function() {		$('#country_id').change(function() {			$.post('<?php echo site_url('				admin / locations / get_zone_menu ');?>', {					id: $('#country_id').val()				},				function(data) {					$('#zone_id').html(data);				});		});	});	function htmlEntities(str) {		return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');	}
</script>
<style type="text/css">
#order_statuses_json {
   display:none;
}
</style>
	</section><!-- /.content --></div><!-- /.right-side -->
<?php $this->load->view('admin/adminFooter'); ?>
<?php
$this->load->view('admin/adminHeader');
?>
<?php
$this->load->view('admin/adminLeftSidebar');
?>
<aside class="right-side">
	<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
			<?php echo $page;?>
			<small>Control panel</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active"><?php echo $page;?></li>
			</ol>
		</section>
		<section class="content">
		<?php echo form_open('admin/home/updateprofile/'.$id ); 
		
		?>
		<div class="row">
			<br/>
			 <input type="hidden" name="hidRfrm" value="1"/>
			<div class="col-sm-6">					
				<label for="shopname">Shop Name</label>
				<?php
				$data	= array('placeholder'=>'Shop Name', 'name'=>'shopname', 'value'=>set_value('shopname', $row->shopname), 'class'=>'form-control input-sm');
				echo form_input($data);
				?>
			</div>
			<div class="col-sm-6">
                <label for="shopaddress">Shop Address</label>			
				<?php
				$data	= array('placeholder'=>'Shop Address', 'name'=>'shopaddress', 'value'=>set_value('shopaddress', $row->shopaddress), 'class'=>'form-control input-sm');
				echo form_input($data);
				?>
			</div>
					
		</div>
		<div class="row">
			<br/>
			<div class="col-sm-6">					
				 <label for="username">Contact Person</label>	
				<?php
				$data	= array('placeholder'=>'Contact Person', 'name'=>'username', 'value'=>set_value('username', $row->username), 'class'=>'form-control input-sm');
				echo form_input($data);
				?>
			</div>
			<div class="col-sm-6">	
                 <label for="useremail">Contact Email</label>				
				<?php
				$data	= array('placeholder'=>'User Email', 'name'=>'useremail', 'value'=>set_value('useremail', $row->email), 'class'=>'form-control input-sm','readonly'=>'true');
				echo form_input($data);
				?>
			</div>
					
		</div>
		<div class="row">
			<br/>
			<div class="col-sm-6">					
				 <label for="state">State</label>	
				<?php
				$data	= array('placeholder'=>'State', 'name'=>'state', 'value'=>set_value('state', $row->state), 'class'=>'form-control input-sm');
				echo form_input($data);
				?>
			</div>
			<div class="col-sm-6">	
                 <label for="city">City</label>				
				<?php
				$data	= array('placeholder'=>'City', 'name'=>'city', 'value'=>set_value('city', $row->city), 'class'=>'form-control input-sm');
				echo form_input($data);
				?>
			</div>
					
		</div>
		
		<div class="row">
			<br/>
			<div class="col-sm-6">					
				<label for="usermobile">Mobile No.</label>	
				<?php
				$data	= array('placeholder'=>'Mobile No.', 'name'=>'usermobile', 'value'=>set_value('usermobile', $row->mobile), 'class'=>'form-control input-sm');
				echo form_input($data);
				?>
			</div>
			<div class="col-sm-6">
                <label for="landline">LandLine No.</label>			
				<?php
				$data	= array('placeholder'=>'LandLine No.', 'name'=>'landline', 'value'=>set_value('city', $row->landline), 'class'=>'form-control input-sm');
				echo form_input($data);
				?>
			</div>
					
		</div>
		
		<div class="row">
			<br/>
			<div class="col-sm-6">					
				<label for="area">Area/Region</label>
				<?php
				$data	= array('placeholder'=>'Area', 'name'=>'area', 'value'=>set_value('area', $row->area), 'class'=>'form-control input-sm');
				echo form_input($data);
				?>
			</div>
			<div class="col-sm-6">
                <label for="pincode">Area Pin Code</label>			
				<?php
				$data	= array('placeholder'=>'Area Pin Code', 'name'=>'pincode', 'value'=>set_value('pincode', $row->pincode), 'class'=>'form-control input-sm');
				echo form_input($data);
				?>
			</div>
					
		</div>
		<div class="row">
		<br/>
		<div class="col-sm-12">
			<button type="submit" class="btn btn-primary">Save</button>
		</div>
		</div>
		
		</form>
		</section>
</aside>
<?php
$this->load->view('admin/adminFooter');
?>
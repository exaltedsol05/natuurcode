<?php
$this->load->view('admin/adminHeader');
?>
<?php
$this->load->view('admin/adminLeftSidebar');
?>
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Gift Cards
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Gift Cards</li>
                    </ol>
                </section>
				<?php $this->load->view('admin/adminError');?>
					
                <!-- Main content -->
                <section class="content">


<script type="text/javascript">
function areyousure()
{
	return confirm('Are you sure you want to delete this gift card?');
}
</script>

<div class="btn-group" style="float:right">

<?php if ($gift_cards['enabled']):?>

	<a class="btn btn-primary" href="<?php echo site_url('admin/giftcards/form'); ?>"><i class="fa fa-plus"></i> Add Gift Card</a>
	
	<a class="btn btn-warning" href="<?php echo site_url('admin/giftcards/settings'); ?>"><i class="fa fa-cog"></i> Settings</a>
	
	<a class="btn btn-danger" href="<?php echo site_url('admin/giftcards/disable'); ?>"><i class="fa fa-ban"></i> Disable Giftcards</a>
	
<?php else: ?>
	<a class="btn btn-primary" href="<?php echo site_url('admin/giftcards/enable'); ?>"><i class="fa fa-check-square-o"></i> Enable Giftcards</a>
<?php endif; ?>
</div>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Code</th>
			<th>To</th>
			<th>From</th>
			<th>Total</th>
			<th>Used</th>
			<th>Remaining</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php echo (count($cards) < 1)?'<tr><td style="text-align:center;" colspan="7">There are currently no giftcards.</td></tr>':''?>
<?php foreach ($cards as $card):?>
		<tr>
			<td><?php echo  $card['code']; ?></td>
			<td><?php echo  $card['to_name']; ?></td>
			<td><?php echo  $card['from']; ?></td>
			<td><?php echo (float) $card['beginning_amount'];?></td>
			<td><?php echo (float) $card['amount_used']; ?></td>
			<td><?php echo (float) $card['beginning_amount'] - (float) $card['amount_used']; ?></td>
			<td>
				<a class="btn btn-danger" style="float:right;" href="<?php echo site_url('admin/giftcards/delete/'.$card['id']); ?>" onclick="return areyousure();"><i class="icon-trash icon-white"></i> Delete</a>
	  </tr>
<?php endforeach; ?>
	</tbody>
</table>
</section><!-- /.content -->
            </aside><!-- /.right-side -->
    
<?php
$this->load->view('admin/adminFooter');
?>
 
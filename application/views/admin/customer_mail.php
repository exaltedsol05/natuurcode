<?php $this->load->view('admin/adminHeader');?>
<?php $this->load->view('admin/adminLeftSidebar');?>
<div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Customer Mail
                        <small>admin panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#"> Catalog</a></li>
                        <li class="active"> Customer Mail</li>
                    </ol>
                </section>
                <?php $this->load->view('admin/adminError');?>
                <!-- Main content -->
                <section class="content">
					<?php echo form_open('admin/customers/sendMailToCustomer/'.$id); ?>
					 <input type="hidden" name="mail_sent" value="mail_sent" />
					<div class="row">
						 <div class="col-md-12">
								<div class="box box-primary">
										<div class="box-header"></div>
										<div class="box-body">
											<div class="col-md-6">
												<div class="form-group">
													<label>To</label>
													<?php
													$data	= array('name'=>'to', 'value'=>set_value('to', $email), 'class'=>'form-control');
													echo form_input($data); ?>
												</div>
											</div>
										</div>
										<div class="box-body">
										    <div class="col-md-6">
												<div class="form-group">
													<label>Subject</label>
													<?php
													$data	= array('name'=>'subject', 'value'=>set_value('subject', $subject), 'class'=>'form-control');
													echo form_input($data); ?>
												</div>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-6">
												<div class="form-group">
													<label>Content</label>
													<?php
													$data	= array('name'=>'notes', 'value'=>set_value('notes', $notes), 'class'=>'form-control');
													echo form_textarea($data); ?>
												</div>
											</div>
										</div>
										<div class="box-footer">
										      <input class="btn btn-primary" type="submit" value="Send"/>
											  <a href="<?php echo site_url('admin/customers'); ?>" class="btn btn-danger" >Cancel</a>
										</div>
								</div>
						 </div>
					</div>
					

				</form>
			</section><!-- /.content -->
 </div><!-- /.right-side -->
<?php $this->load->view('admin/adminFooter'); ?>
<?php $m	= Array('january','february','march','april','may','june','july','august','september','october','november','december');?>

<table class="table table-striped">
	<thead>
		<tr>
			<?php /*<th>ID</th> uncomment this if you want it*/ ?>
			<th>Date</th>
			<th>Coupon Discounts</th>			
			<th>Products</th>
			<th>Shipping</th>
			<th>Tax</th>
			<th>Grand Total</th>
		</tr>
	</thead>
	<tbody>
<?php foreach($orders as $month):?>
		<tr>
			<td><?php echo $m[intval($month->month)-1].' '.$month->year;?></td>
			<td><?php echo $month->coupon_discounts;?></td>
			<td><?php echo $month->product_totals;?></td>
			<td><?php echo $month->shipping;?></td>
			<td><?php echo $month->tax;?></td>
			<td><?php echo $month->total;?></td>
		</tr>
<?php endforeach;?>
	</tbody>
</table>


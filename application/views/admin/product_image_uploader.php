<?php include('ifram_header.php');?>



<script type="text/javascript">

<?php if( $this->input->post('submit') ):?>
$(window).ready(function(){
    
	//$('#iframe_uploader', window.parent.document).height($('body').height());	
});
<?php endif;?>

<?php if($file_name):?>
	parent.add_product_image('<?php echo $file_name;?>');
<?php endif;?>

</script>

<?php if (!empty($error)): ?>
	<div class="alert alert-danger">
		<a class="close" data-dismiss="alert">×</a>
		<?php echo $error; ?>
	</div>
<?php endif; ?>

<div class="row">
	<div class="col-sm-12">
	    
		<?php echo form_open_multipart('admin/products/product_image_upload/'.$id, 'class="form-inline"');?>
		     <div class="form-group">
			<?php echo form_upload(array('name'=>'userfile', 'id'=>'userfile', 'class'=>'input-file pull-left'));?> 
			 </div>
			<input class="btn btn-default pull-right" name="submit" type="submit" value="Upload" />
		</form>
		<br/>
	</div>
	
</div>


<?php include('ifram_footer.php');
<?php $this->load->view('admin/adminHeader');?>
<?php $this->load->view('admin/adminLeftSidebar');?>
<div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Report
                        <small>admin panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#"> Catalog</a></li>
                        <li class="active"> Report</li>
                    </ol>
                </section>
                <?php $this->load->view('admin/adminError');?>
                <!-- Main content -->
				<section class="content">
					<div class="row">
						<div class="col-md-12">
							<div class="box box-primary">
							    <div class="box-header">
									
							    
							    </div>
								<div class="box-body">
							         
									<div class="row">
										<div class="col-sm-12">
											<h3>Most Bought Customers</h3>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-12" id="most_bought"></div>
									</div>
									
									<div class="row">
										<div class="col-sm-4">
											<h3>Best Sellers</h3>
										</div>
										<div class="col-sm-8">
											<form class="form-inline pull-right">
												<input class="form-control" type="text"  id="best_sellers_start" placeholder="From"/>
												<input type="hidden" name="best_sellers_start" id="best_sellers_start_alt" /> 
												<input class="form-control" type="text" id="best_sellers_end" placeholder="To"/>
												<input type="hidden" name="best_sellers_end" id="best_sellers_end_alt" /> 
												
												<input class="btn btn-primary" type="button" value="Get Best Sellers" onclick="get_best_sellers()"/>
											</form>
										</div>
									</div>


									<div class="row">
										<div class="col-sm-12" id="best_sellers"></div>
									</div>


									<div class="row">
										<div class="col-sm-6">
											<h3>Sales</h3>
										</div>
										<div class="col-sm-6">
											<form class="form-inline pull-right">
												<select name="year" id="sales_year" class="form-control">
													<?php foreach($years as $y):?>
														<option value="<?php echo $y;?>"><?php echo $y;?></option>
													<?php endforeach;?>
												</select>
												<input class="btn btn-primary" type="button" value="Get Monthly Sales" onclick="get_monthly_sales()"/>
											</form>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-12" id="sales_container"></div>
									</div>
				
							    </div>
								<div class="box-footer">
									 
							    </div>
							     
							</div>
						</div>
					</div>
				
				
				
				</section><!-- /.content -->
</div><!-- /.right-side -->
<script type="text/javascript">

$(document).ready(function(){
	get_most_bought();
	get_best_sellers();
	get_monthly_sales();
	$('input:button').button();
	$('#best_sellers_start').datepicker({ dateFormat: 'mm-dd-yy', altField: '#best_sellers_start_alt', altFormat: 'yy-mm-dd' });
	$('#best_sellers_end').datepicker({ dateFormat: 'mm-dd-yy', altField: '#best_sellers_end_alt', altFormat: 'yy-mm-dd' });
});

function get_most_bought()
{
	$.post('<?php echo site_url('admin/reports/most_bought_cust');?>', function(data){
		$('#most_bought').html(data);
		
	});
}
function get_best_sellers()
{
	$.post('<?php echo site_url('admin/reports/best_sellers');?>',{start:$('#best_sellers_start_alt').val(), end:$('#best_sellers_end_alt').val()}, function(data){
		$('#best_sellers').html(data);
		
	});
}

function get_monthly_sales()
{
	
	$.post('<?php echo site_url('admin/reports/sales');?>',{year:$('#sales_year').val()}, function(data){
		$('#sales_container').html(data);
		
	});
}

</script>
<?php $this->load->view('admin/adminFooter');?>
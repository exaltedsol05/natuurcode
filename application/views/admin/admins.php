<?php
$this->load->view('admin/adminHeader');
?>
<?php
$this->load->view('admin/adminLeftSidebar');
?>

<aside class="right-side">
                <!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				Administrators
				<small>admin panel</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li><a href="#"> Catalog</a></li>
				<li class="active"> Administrators</li>
			</ol>
		</section>
                <?php $this->load->view('admin/adminError');?>
                <!-- Main content -->
                <section class="content">
                    <div class="col-sm-12">
                        <div class="col-xs-12">
								<script type="text/javascript">
								function areyousure()
								{
									return confirm('Are you sure you want to delete this administrator?');
								}
								</script>

								<div style="text-align:right;">
									<a class="btn btn-primary" href="<?php echo site_url('admin/home/admin_form'); ?>"><i class="fa fa-plus"></i>  Add New Admin</a>
								</div>

								<table class="table table-striped">
									<thead>
										<tr>
											<th>First Name</th>
											<th>Last Name</th>
											<th>Email</th>
											<th>Username</th>
											<th>Access</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
								<?php foreach ($admins as $admin):?>
										<tr>
											<td><?php echo $admin->firstname; ?></td>
											<td><?php echo $admin->lastname; ?></td>
											<td><a href="mailto:<?php echo $admin->email;?>"><?php echo $admin->email; ?></a></td>
											<td><?php echo $admin->username; ?></td>
											<td><?php echo $admin->access; ?></td>
											<td>
												<div class="btn-group" style="float:right;">
													<a class="btn btn-success" href="<?php echo site_url('admin/home/admin_form/'.$admin->id);?>"><i class="fa fa-pencil"></i> Edit</a>	
													<?php
													$current_admin_id	= $this->session->userdata('id');
													
													if ($current_admin_id != $admin->id): ?>
													<a class="btn btn-danger" href="<?php echo site_url('admin/home/delete/'.$admin->id); ?>" onclick="return areyousure();"><i class="fa fa-trash fa-white"></i> Delete</a>
													<?php endif; ?>
												</div>
											</td>
										</tr>
								<?php endforeach; ?>
									</tbody>
								</table>


						</div>
					</div>
				 </section>

</aside>				 
				 
				 <?php
$this->load->view('admin/adminFooter');
?>
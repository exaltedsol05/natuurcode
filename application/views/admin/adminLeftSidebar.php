<?php $arrmenuSetting=json_decode($this->session->userdata('menuSetting'));?>

  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="<?=HTTP_IMAGES_PATH_ADMIN;?>avatar3.png" class="img-circle" alt="User Image" />
			</div>
			<div class="pull-left info" style="width:75%;">
				<p>Hello, <?php echo ucfirst($this->session->userdata('roll'));?></p>
				<i class="fa fa-circle text-success"></i> Online
			</div>
		</div>

		<div class="clear"></div>
		<!-- /.search form -->
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu">

            <li class="<?php echo $page == 'dash' ? 'active':''; ?>">
				<a href="<?php echo base_url().'admin/home';?>" >
					<i class="fa fa-dashboard"></i> <span>Dashboard</span>
				</a>
			</li>
			<li class="treeview" <?php if(!in_array('Catalog_Top',$arrmenuSetting)){ echo "style='display:none'";}?>>
				<a href="#">
				<i class="fa fa-book"></i>
				<span>Catalog </span>
				<i class="fa fa-angle-left pull-right"></i>
					</a>
					
					<ul class="treeview-menu">                               
						<li <?php if(!in_array('Categories',$arrmenuSetting)){ echo "style='display:none'";}?> class="<?php echo $page == 'categories' ? 'active':''; ?>"><a href="<?php echo base_url(); ?>admin/categories/"><i class="fa fa-angle-double-right"></i> Categories</a></li>	
						
						<li <?php if(!in_array('Products',$arrmenuSetting)){ echo "style='display:none'";}?> class="<?php echo $page == 'products' ? 'active':''; ?>"><a href="<?php echo base_url(); ?>admin/products/"><i class="fa fa-angle-double-right"></i> Products</a></li>
						
						<!--<li <?php if(!in_array('archive_products',$arrmenuSetting)){ echo "style='display:none'";}?> class="<?php echo $page == 'archive_products' ? 'active':''; ?>"><a href="<?php echo base_url(); ?>admin/products/product_archive/"><i class="fa fa-angle-double-right"></i> Archive Products</a></li>	
						
						<li <?php if(!in_array('zero_products',$arrmenuSetting)){ echo "style='display:none'";}?> class="<?php echo $page == 'zero_products' ? 'active':''; ?>"><a href="<?php echo base_url(); ?>admin/products/product_zero_quantity/"><i class="fa fa-angle-double-right"></i> Zero Quantity Products</a></li>-->	
						
						<li <?php if(!in_array('feature_products',$arrmenuSetting)){ echo "style='display:none'";}?> class="<?php echo $page == 'home_products' ? 'active':''; ?>"><a href="<?php echo base_url(); ?>admin/products/home_products"><i class="fa fa-angle-double-right"></i> Feature Products</a></li>
						
						<!--<li <?php if(!in_array('Shop_By_Brand',$arrmenuSetting)){ echo "style='display:none'";}?> class="<?php echo $page == 'shop_brands' ? 'active':''; ?>"><a href="<?php echo base_url(); ?>admin/shopbrands/"><i class="fa fa-angle-double-right"></i> Shop By Brand</a></li>-->                              
					</ul>
            </li>

			<li class="treeview" <?php if(!in_array('Sales_Top',$arrmenuSetting)){ echo "style='display:none'";}?>>
                <a href="#">
					<i class="fa fa-shopping-cart"></i>
					<span>Sales</span>
					<i class="fa fa-angle-left pull-right"></i>
                </a>
                    <ul class="treeview-menu">
						<li <?php if(!in_array('Orders',$arrmenuSetting)){ echo "style='display:none'";}?>><a href="<?php echo base_url(); ?>admin/orders/"><i class="fa fa-angle-double-right"></i> Orders</a></li>

						<li <?php if(!in_array('Customers',$arrmenuSetting)){ echo "style='display:none'";}?>><a href="<?php echo base_url(); ?>admin/customers/"><i class="fa fa-angle-double-right"></i> Customers</a></li>
						
						<!--<li <?php if(!in_array('Customers',$arrmenuSetting)){ echo "style='display:none'";}?>><a href="<?php echo base_url(); ?>admin/customers/groups"><i class="fa fa-angle-double-right"></i> Customers Groups</a></li>-->

						<li <?php if(!in_array('Reports',$arrmenuSetting)){ echo "style='display:none'";}?>><a href="<?php echo base_url(); ?>admin/reports/"><i class="fa fa-angle-double-right"></i> Reports</a></li>

						<li <?php if(!in_array('Coupons',$arrmenuSetting)){ echo "style='display:none'";}?>><a href="<?php echo base_url(); ?>admin/coupons/"><i class="fa fa-angle-double-right"></i>Coupons</a></li>

						<!--<li <?php if(!in_array('Giftcards',$arrmenuSetting)){ echo "style='display:none'";}?>><a href="<?php echo base_url(); ?>admin/giftcards/gift"><i class="fa fa-angle-double-right"></i>Giftcards</a></li>-->
					</ul>
            </li>

			<li <?php if(!in_array('Testimonial_Top',$arrmenuSetting)){ echo "style='display:none'";}?> class="<?php echo $page == 'testimonial' ? 'active':''; ?>">
				<a href="<?php echo base_url(); ?>admin/testimonials/">
					<i class="fa fa-user"></i>
					<span>Testimonial </span>
					<i class="fa fa-angle-left pull-right"></i>
				</a>
			</li>
			
			<li <?php if(!in_array('Testimonial_Top',$arrmenuSetting)){ echo "style='display:none'";}?> class="<?php echo $page == 'testimonial' ? 'active':''; ?>">
				<a href="<?php echo base_url(); ?>admin/customers/getNewsLetter/">
					<i class="fa fa-envelope"></i>
					<span>NewsLetter </span>
					<i class="fa fa-angle-left pull-right"></i>
				</a>
			</li>

			<li <?php if(!in_array('Reviews_Top',$arrmenuSetting)){ echo "style='display:none'";}?> class="treeview">
				<a href="<?php echo base_url(); ?>admin/reviews/">
					<i class="fa fa-pencil-square"></i>
					<span>Reviews </span>
					<i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
					<li class="<?php echo $page == 'categories' ? 'active':''; ?>">
						<a href="<?php echo base_url(); ?>ratenreview_controller/approvedReviews/"><i class="fa fa-angle-double-right"></i>Approved Reviews</a>
					</li> 
					
					<li class="<?php echo $page == 'products' ? 'active':''; ?>">
						<a href="<?php echo base_url(); ?>ratenreview_controller/non_approvedReviews/"><i class="fa fa-angle-double-right"></i>Non-Approved Reviews</a>
					</li>
				</ul>
			</li>

			<li class="treeview" <?php if(!in_array('Content_Top',$arrmenuSetting)){ echo "style='display:none'";}?>>
				<a href="#">
					<i class="fa fa-windows"></i>
					<span>Content </span>
					<i class="fa fa-angle-left pull-right"></i>
				</a>

				<ul class="treeview-menu">                          
					<li <?php if(!in_array('Banners',$arrmenuSetting)){ echo "style='display:none'";}?>><a href="<?php echo base_url(); ?>admin/banners/"><i class="fa fa-angle-double-right"></i> Banners</a></li>
					
					<li <?php if(!in_array('Pages',$arrmenuSetting)){ echo "style='display:none'";}?>><a href="<?php echo base_url(); ?>admin/pages/"><i class="fa fa-angle-double-right"></i> pages</a></li>
					
					<li><a href="<?php echo base_url(); ?>admin/blogs/"><i class="fa fa-angle-double-right"></i> Blogs</a></li>
					
					<li><a href="<?php echo base_url(); ?>admin/workshops/"><i class="fa fa-angle-double-right"></i> Workshops</a></li>
				</ul>                            
			</li>    

			<li class="treeview" <?php if(!in_array('Administrative_Top',$arrmenuSetting)){ echo "style='display:none'";}?>>
				<a href="#"><i class="fa fa-cog"></i><span>Administrative </span><i class="fa fa-angle-left pull-right"></i></a>
				<ul class="treeview-menu">							
					<li <?php if(!in_array('Server_Configuration',$arrmenuSetting)){ echo "style='display:none'";}?>><a href="<?php echo base_url(); ?>admin/settings/"><i class="fa fa-angle-double-right"></i> Server Configuration</a></li>
					
					<li <?php if(!in_array('Messages',$arrmenuSetting)){ echo "style='display:none'";}?>><a href="<?php echo base_url(); ?>admin/settings/canned_messages/"><i class="fa fa-angle-double-right"></i> Messages</a></li>
					
					<!--<li <?php if(!in_array('Administrators',$arrmenuSetting)){ echo "style='display:none'";}?>><a href="<?php echo base_url(); ?>admin/home/admin/"><i class="fa fa-angle-double-right"></i> Administrators</a></li>-->
				</ul>
			</li>
        </ul> 
	</section>
    <!-- /.sidebar -->
 </aside>

  


           
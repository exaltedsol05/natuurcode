<?php $this->load->view('admin/adminHeader');?>
<?php $this->load->view('admin/adminLeftSidebar');?>
<script type="text/javascript">
//<![CDATA[
$(document).ready(function(){
	create_sortable();	
});
// Return a helper with preserved width of cells
var fixHelper = function(e, ui) {
	ui.children().each(function() {
		$(this).width($(this).width());
	});
	return ui;
};
function create_sortable()
{
	$('#banners_sortable').sortable({
		scroll: true,
		helper: fixHelper,
		axis: 'y',
		handle:'.handle',
		update: function(){
			save_sortable();
		}
	});	
	$('#banners_sortable').sortable('enable');
}

function save_sortable()
{
	serial=$('#banners_sortable').sortable('serialize');
			
	$.ajax({
		url:'<?php echo site_url('admin/banners/organize');?>',
		type:'POST',
		data:serial
	});
}
function areyousure()
{
	return confirm('Are you sure you want to delete this banner?');
}
//]]>
</script>


<div class="content-wrapper">
	<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
			<?=$page_title;?>
			<small>Control panel</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active"><?=$page_title;?></li>
			</ol>
		</section>
		<?php $this->load->view('admin/adminError');?>
		<section class="content">
		       
		       <div class="row">
			        <?php //echo $msg;?>
			       <div class="col-sm-12">
				   
				   
				    <div class="box box-primary">
						
						<div class="box-header">
						    <a class="btn btn-primary pull-right" href="<?php echo site_url('admin/banners/banner_form/'.$banner_collection_id); ?>"><i class="fa fa-plus"></i> Add New Banner</a>
							<a class="btn btn-success  pull-right" href="<?php echo site_url('admin/banners/'); ?>"><i class="fa fa-folder-open-o"></i> Banner Collections</a>

							<strong style="float:left;">Banners are sortable! Just drag and drop them in the order you would like for them to appear.</strong>
						</div>
						<div class="box-body">
						       <table class="table table-striped">
							<thead>
								<tr>
								    <th>Sr.No.</th>
									<th>Sort</th>
									<th>Name</th>
									<th>image</th>
									<th>Enable Date</th>
									<th>Disable Date</th>
									<th></th>
								</tr>
							</thead>
							<?php echo (count($banners) < 1)?'<tr><td style="text-align:center;" colspan="5">There are currently no banners</td></tr>':''?>
							<?php if ($banners): ?>
							<tbody id="banners_sortable">
							<?php
                            $i=0;
							foreach ($banners as $banner):
                                $i++;
								//clear the dates out if they're all zeros
								if ($banner->enable_date == '0000-00-00')
								{
									$enable_test	= false;
									$enable			= '';
								}
								else
								{
									$eo			 	= explode('-', $banner->enable_date);
									$enable_test	= $eo[0].$eo[1].$eo[2];
									$enable			= $eo[1].'-'.$eo[2].'-'.$eo[0];
								}

								if ($banner->disable_date == '0000-00-00')
								{
									$disable_test	= false;
									$disable		= '';
								}
								else
								{
									$do			 	= explode('-', $banner->disable_date);
									$disable_test	= $do[0].$do[1].$do[2];
									$disable		= $do[1].'-'.$do[2].'-'.$do[0];
								}


								$disabled_icon	= '';
								$curDate		= date('Ymd');

								if (($enable_test && $enable_test > $curDate) || ($disable_test && $disable_test <= $curDate))
								{
									$disabled_icon	= '<span style="color:#ff0000;">&bull;</span> ';
								}
								?>
								<tr id="banners-<?php echo $banner->banner_id;?>">
								    <td class="handle"><?php echo $i;?></td>
									<td class="handle"><a class="btn" style="cursor:move"><span class="fa fa-align-justify"></span></a></td>
									<td><?php echo $disabled_icon.$banner->name;?></td>
									<td><img style="width:50px;height:50px;" src="<?php echo base_url('uploads/'.$banner->image);?>" alt="current"/></td>
									<td><?php echo $enable;?></td>
									<td><?php echo $disable;?></td>
									<td>
										<div class="btn-group" style="float:right">
											<a class="btn btn-info" href="<?php echo site_url('admin/banners/banner_form/'.$banner_collection_id.'/'.$banner->banner_id);?>"><i class="fa fa-pencil"></i> Edit</a>
											<a class="btn btn-danger" href="<?php echo  site_url('admin/banners/delete_banner/'.$banner->banner_id);?>" onclick="return areyousure();"><i class="fa fa-trash-o"></i> Delete</a>
										</div>
									</td>
								</tr>
							<?php endforeach; ?>
							</tbody>
							<?php endif;?>
						</table>
				     
						</div>
						<div class="box-footer">
						</div>
					</div>
					
				    </div>
			   </div>
			
		</section>

</div>
<?php
$this->load->view('admin/adminFooter');
?>
<?php $this->load->view('admin/adminHeader'); ?><br/><br/><script>
$(document).ready(function(){
	$('.ship_date').datepicker({ 
		startDate: "today",
		format: 'yyyy-mm-dd',
		todayHighlight: true,
		autoclose:true
	});
	
	$("#PickupDetail").change(function(){
		value = $("#PickupDetail").val();	
		if(value == "schedule_time"){
			$('#pickup_shedule').slideDown('slow');
			$('#pickup_shedule1').slideDown('slow');
		}else{
			$('#pickup_shedule').slideUp('slow');
			$('#pickup_shedule1').slideUp('slow');
		}
	})

})

</script>

<div class="container custom-form" style="margin-left:92px;">
	<div class="row">
    	<div class="col-sm-10 col-sm-offset-1">
        <ul class="nav nav-tabs">
            <li class="dropdown active">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Ship <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a data-toggle="tab" href="#createShipment">Create a Shipment</a></li>
                    <li><a data-toggle="tab" href="#createReturnShipment">Create Return Shipment </a></li>
                    <li><a data-toggle="tab" href="#createImportShipment">Create Import Shipment </a></li>
                    <li><a data-toggle="tab" href="#viewPendingShipment">View Pending Shipments </a></li>
                </ul>
            </li>
            <li><a data-toggle="tab" href="#menu1">Ship History</a></li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">My Lists <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a data-toggle="tab" href="#address">Address Book</a></li>
                    <li><a data-toggle="tab" href="#shppro">Shipment Profiles</a></li>
                    <li><a data-toggle="tab" href="#dime">Dimensions</a></li>
                </ul>
            </li>
            <li><a data-toggle="tab" href="#menu3">Reports</a></li>
        </ul>

        <div class="tab-content" style="border:1px solid #ddd; border-top:none; padding:15px;">
            <div id="createShipment" class="tab-pane fade in active">
                <h4>Create a Shipment</h4>
                <ul class="step-block">
                	<li class="active"><span>1</span> Enter shipping information</li>
                	<li><span>2</span> Enter product/commodity information</li>
                    <li><span>3</span> Print label(s) and documents</li>
                </ul>
                <div class="guide-row">
                    <div class="row">
                        <div class="col-sm-6">
                            <p>* Denotes required field</p>
                        </div>
                        <div class="col-sm-6 text-right">
                            <a href="#">Preferences</a> | <a href="#">Clear all fields</a>
                        </div>
                    </div>
                </div>

<div class="custom-form">
	<div class="row">
		<form class="form-horizontal" action="<?php echo base_url()."admin/fedex/fedex_shipment_request";?>" method="post">
			<div class="col-sm-6">
				<!-- start of block 1 -->
				<div class="form-block" style="display:none;">
					<div class="panel-group">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" href="#collapse1">
										<div class="clearfix">
                                            <span class="pull-left">My Shipment Profiles</span>
                                            <span class="pull-right help-icon">
                                                <i class="glyphicon glyphicon-question-sign"></i> Help
                                            </span>
                                        </div>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse in">
								<div class="wrapper">
									
                                     <div class="form-group">
										<label class="control-label col-sm-5" for="fastShip">My shipment profiles (formerly Fast Ship)</label>
                                        <div class="col-sm-7">
                                          <select name="fastShip" id="fastShip" class="form-control" >
                                          	<option value="1">Select</option>
                                          	<option value="2">Name 1</option>
                                          	<option value="3">Name 1</option>
                                          </select>
                                        </div>
									</div>
									<div class="form-group">
										<div class="col-sm-12 text-right">
											<button type="submit" class="btn btn-primary">Ship</button>
										</div>
									</div>
                               </div>
							</div>
						</div>
					</div>
				</div>
				<!-- end of block 1 -->
                
				<!-- end of block 2 -->
				<div class="form-block-1">
					<div class="panel-group">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" href="#from">
										<div class="clearfix">
											<span class="pull-left">1. From</span>
											<span class="pull-right help-icon"><i class="glyphicon glyphicon-question-sign"></i> Help</span>
										</div>
									</a>
								</h4>
							</div>
                            
							<div id="from" class="panel-collapse collapse in">
								<div class="wrapper">
									<div class="form-group">
										<label class="control-label col-sm-4" for="Country"><span>*</span> Country/Location</label>
										<div class="col-sm-8">
										  <select name="from_country" id="Country" class="form-control" >
											<option value="<?php echo $setting_info['country']->setting;?>" selected>India</option>
										  </select>
										</div>
									</div>
										  
									<div class="form-group">
											<label class="control-label col-sm-4" for="Company"><span>&nbsp;</span> Company</label>
											<div class="col-sm-8">
											  <input type="text" name="from_company" value="<?php echo $setting_info['company_name']->setting; ?>" id="Company" class="form-control" >                                              
											</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-sm-4" for="ContactName"><span>*</span> Contact name</label>
										<div class="col-sm-8">
										  <input type="text" name="from_name" value="" id="ContactName" class="form-control" >                  
										</div>
									</div>
										  
									<div class="form-group">
										<label class="control-label col-sm-4" for="Address1"><span>*</span> Address 1</label>
										<div class="col-sm-8">
										<input type="text" name="from_add1" value="<?php echo $setting_info['address1']->setting;?>" id="Address1" class="form-control" >
										</div>
									</div>
									
										  <div class="form-group">
											<label class="control-label col-sm-4" for="Address2"> <span>&nbsp;</span> Address 2</label>
											<div class="col-sm-8">
											  <input type="text" name="from_add2" value="<?php echo $setting_info['address2']->setting;?>" id="Address2" class="form-control" >
											</div>
										  </div>
										  
										  <div class="form-group">
											<label class="control-label col-sm-4" for="PostalCode"><span>*</span> Postal code</label>
											<div class="col-sm-3">
											  <input type="text" name="from_zip" value="<?php echo $setting_info['zip']->setting;?>" class="form-control" >
											</div>
                                            <div class="col-sm-5">
                                              	<a href="#" class="btn-link">Postal code information</a>
                                            </div>
                                          </div>
										  
                                          <div class="form-group">
                                            <label class="control-label col-sm-4" for="City"><span>*</span> City</label>
                                            <div class="col-sm-8">
                                              <input type="text" name="from_city" value="<?php echo $setting_info['city']->setting;?>" id="City" class="form-control">                                              
                                            </div>
                                          </div>
										  
                                          <div class="form-group">
                                            <label class="control-label col-sm-4" for="State"><span>*</span> State</label>
                                            <div class="col-sm-8"><?php
												$from_zone_code =  $setting_info['zone_id']->setting;?>
												<select name="from_zone" id="State" class="form-control" ><?php
													foreach($states_code as $code => $state){
														$checked = '';
														if($from_zone_code == $code){
															$checked = "selected";
														}?>														
														<option value="<?php echo $code; ?>" <?php echo $checked; ?>><?php echo $state; ?></option><?php
													}?> 
                                              </select>
                                            </div>
                                          </div>
										  
                                          <div class="form-group">
                                            <label class="control-label col-sm-4" for="phoneno"><span>*</span> Phone no.</label>
                                            <div class="col-sm-4">
                                              <input type="text" name="from_phone" value="" id="phoneno" class="form-control" >
                                            </div>
                                            <div class="col-sm-1">
                                              	ext.
                                            </div>
                                            <div class="col-sm-3">
                                              	<input type="text" name="ext" id="ext" class="form-control">
                                            </div>
                                          </div>
										  
                                          <div class="form-group">
                                            <label class="control-label col-sm-4" for="VAT"><span>&nbsp;</span> VAT/CST/TIN no.</label>
                                            <div class="col-sm-8">
                                              <input type="text" name="VAT" id="VAT" class="form-control">                                              
                                            </div>
                                          </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                            </div>
							<!-- end of block 2 -->
                            <div class="form-block-1">
                        		<div class="panel-group">
                              <div class="panel panel-default">
                                <div class="panel-heading">
                                  <h4 class="panel-title">
                                    <a data-toggle="collapse" href="#to">
                                    	<div class="clearfix">
                                            <span class="pull-left">2. To</span>
                                            <span class="pull-right help-icon">
                                                <i class="glyphicon glyphicon-question-sign"></i> Help
                                            </span>
                                        </div>
                                    </a>
                                  </h4>
                                </div>
                                <div id="to" class="panel-collapse collapse in">
                                	<div class="wrapper">
                                  		  <div class="form-group">
                                            <label class="control-label col-sm-4" for="Country"><span>*</span> Country/Location</label>
                                            <div class="col-sm-8">
                                              <select name="to_country" id="Country" class="form-control" >
												<option value="<?php echo $order->ship_country;?>" selected>India</option>
											  </select>
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="control-label col-sm-4" for="Company"><span>&nbsp;</span> Company</label>
                                            <div class="col-sm-8">
                                              <input type="text" name="to_company" id="Company" class="form-control">          
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="control-label col-sm-4" for="ContactName"><span>*</span> Contact name</label>
                                            <div class="col-sm-8">
                                              <input type="text" name="to_name" value="<?php echo ucfirst($order->ship_firstname).' '.ucfirst($order->ship_lastname);?>" id="ContactName" class="form-control">
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="control-label col-sm-4" for="Address1"><span>*</span> Address 1</label>
                                            <div class="col-sm-8">
                                              <input type="text" name="to_add1" value="<?php echo $order->ship_address1;?>" id="Address1" class="form-control">                                              
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="control-label col-sm-4" for="Address2"> <span>&nbsp;</span> Address 2</label>
                                            <div class="col-sm-8">
                                              <input type="text" name="to_add2" value="<?php echo $order->ship_address2;?>" id="Address2" class="form-control">                                              
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="control-label col-sm-4" for="PostalCode"><span>*</span> Postal code</label>
                                            <div class="col-sm-3">
                                              <input type="text" name="to_zip" value="<?php echo $order->ship_zip;?>" id="PostalCode" class="form-control">
                                            </div>
                                            <div class="col-sm-5">
                                              	<a href="#" class="btn-link">Postal code information</a>
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="control-label col-sm-4" for="City"><span>*</span> City</label>
                                            <div class="col-sm-8">
                                              <input type="text" name="to_city" value="<?php echo $order->ship_city;?>" id="City" class="form-control">                                              
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="control-label col-sm-4" for="State"><span>*</span> State</label>
                                            <div class="col-sm-8"><?php
												$to_zone_code = $order->ship_zone;;?>
												<select name="to_zone" id="State" class="form-control" ><?php
												foreach($states_code as $code => $state){
													$checked = '';
													if($to_zone_code == $code){
														$checked = "selected";
													}?>
													<option value="<?php echo $code; ?>" <?php echo $checked; ?> ><?php echo $state; ?></option><?php
												}?> 
                                              </select>
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="control-label col-sm-4" for="phoneno"><span>*</span> Phone no.</label>
                                            <div class="col-sm-4">
                                              <input type="text" name="to_phone" value="<?php echo $order->ship_phone;?>" id="phoneno" class="form-control">
                                            </div>
                                            <div class="col-sm-1">
                                              	ext.
                                            </div>
                                            <div class="col-sm-3">
                                              	<input type="text" name="ext" id="ext" class="form-control">
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="control-label col-sm-4" for="VAT"><span>&nbsp;</span> VAT/CST/TIN no.</label>
                                            <div class="col-sm-8">
                                              <input type="text" name="VAT" id="VAT" class="form-control">
                                            </div>
                                          </div>
                                      <div class="form-group"> 
                                        <div class="col-sm-offset-4 col-sm-8">
                                           <div class="checkbox">
                                            <label><input type="checkbox"> This is a residential address</label>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                         </div>
							
							
                        </div>
                    	<div class="col-sm-6">
                        	<!-- start block 3 -->
							
                            <div class="form-block-1">
                        		<div class="panel-group">
                              <div class="panel panel-default">
                                <div class="panel-heading">
                                  <h4 class="panel-title">
                                    <a data-toggle="collapse" href="#package">
                                    	<div class="clearfix">
                                            <span class="pull-left">3. Package & Shipment Details</span>
                                            <span class="pull-right help-icon">
                                                <i class="glyphicon glyphicon-question-sign"></i> Help
                                            </span>
                                        </div>
                                    </a>
                                  </h4>
                                </div>
                                <div id="package" class="panel-collapse collapse in">
                                	<div class="wrapper"><!--
                                  		  <div class="form-group" style="">
                                            <label class="control-label col-sm-4" for="ContactName"><span>*</span> Ship date</label>
                                            <div class="col-sm-8">
                                              <input type="text" name="ship_date" id="ship_date" class="form-control">
                                            </div>
                                          </div>-->
										  <div class="form-group" style="">
                                            <label class="control-label col-sm-4" for="ContactName"><span>*</span> Description</label>
                                            <div class="col-sm-8">
                                              <input type="text" name="Description" id="ship_date" class="form-control">
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="control-label col-sm-4" for="State"><span>*</span> No. of packages</label>
                                            <div class="col-sm-8">
                                              <select name="NumberOfPieces" id="packages" class="form-control" onChange="noOfpackages();"><?php 
												for ($x = 1; $x <= 20; $x++) {	
													echo "<option value='$x'>$x</option>";
												}?>
                                              </select>
                                            </div>
                                          </div>
                                          <div class="form-group" id="arePackagesIdentica" style="display:none;">
                                            <label class="control-label col-sm-4" for="Address1"><span>*</span> Are packages identical</label>
                                            <div class="col-sm-8">
                                            	<div>
                                                    <label class="radio-inline">
														<input type="radio" name="isidentical" value="yes" id="apiYes" onChange="hideTable()">Yes</label>
                                                    <label class="radio-inline">
														<input type="radio" name="identical" value="no" id="apiNo" onChange="showTable()">No</label>
												</div>
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="control-label col-sm-4" for="Address2"> <span>*</span> Weight </label>
                                            <div class="col-sm-5"><?php
												$total_weight = array();
												foreach($order->contents as $orderkey=>$product){
													$total_weight[] = $product['weight'];
												}
												$grant_weight = array_sum($total_weight);?>
												<input type="text" name="Weight[Value]" value="<?php echo $grant_weight; ?>" id="Address2" class="form-control">
                                            </div>
                                            <div class="col-sm-3">KGS</div>
                                          </div>
                                          
                                          <div id="showTable" style="display:none;">
                                              <div class="form-group">
                                                <label class="control-label col-sm-4" for="State"><span>*</span> Weight unit</label>
                                                <div class="col-sm-8">
                                                  <select name="State" id="State" class="form-control" >
                                                    <option value="KG">KG</option>
                                                    <option value="LB">LB</option>
                                                  </select>
                                                </div>
                                              </div>
                                              <div class="row">                                        	
                                              
                                                <div class="col-sm-12">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped">
                                                        <thead>
                                                          <tr>
                                                            <th width="10%"><input type="checkbox" name="qty" id="qty"></th>
                                                            <th width="15%"><span>*</span> Qty</th>
                                                            <th width="18%"><span>*</span> Weight (kgs)</th>
                                                            <th width="42%">Dimensions (cm)</th>
                                                            <th width="15%">Carriage Value (INR)</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <tr>
                                                            <td><input type="checkbox" name="qty" id="qty"></td>
                                                            <td><input type="text" class="form-control"></td>
                                                            <td><input type="text" class="form-control"></td>
                                                            <td>
                                                                <select name="State" id="State" class="form-control" >
                                                                    <option value="1">Select</option>
                                                                    <option value="2">Enter Dimension</option>
                                                                  </select>
                                                            </td>
                                                            <td><input type="text" class="form-control"></td>
                                                          </tr>
                                                          <tr>
                                                            <td><input type="checkbox" name="qty" id="qty"></td>
                                                            <td><input type="text" class="form-control"></td>
                                                            <td><input type="text" class="form-control"></td>
                                                            <td>
                                                                <div class="row gutter-10">
                                                                    <div class="col-sm-4">
                                                                        <input type="text" class="form-control" placeholder="L">
                                                                    </div>
                                                                    <div class="col-sm-4">
                                                                        <input type="text" class="form-control" placeholder="W">
                                                                    </div>
                                                                    <div class="col-sm-4">
                                                                        <input type="text" class="form-control" placeholder="H">
                                                                    </div>
                                                                </div>                                                        	
                                                            </td>
                                                            <td><input type="text" class="form-control"></td>
                                                          </tr>
                                                          <tr>
                                                            <td><input type="checkbox" name="qty" id="qty"></td>
                                                            <td><input type="text" class="form-control"></td>
                                                            <td><input type="text" class="form-control"></td>
                                                            <td>
                                                                <select name="State" id="State" class="form-control" >
                                                                    <option value="1">Select</option>
                                                                    <option value="2">Enter Dimension</option>
                                                                  </select>
                                                            </td>
                                                            <td><input type="text" class="form-control"></td>
                                                          </tr>
                                                          <tr>
                                                            <td><input type="checkbox" name="qty" id="qty"></td>
                                                            <td><input type="text" class="form-control"></td>
                                                            <td><input type="text" class="form-control"></td>
                                                            <td>
                                                                <select name="State" id="State" class="form-control" >
                                                                    <option value="1">Select</option>
                                                                    <option value="2">Enter Dimension</option>
                                                                  </select>
                                                            </td>
                                                            <td><input type="text" class="form-control"></td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
                                                </div>
                                              </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="control-label col-sm-4" for="Address1"><span>*</span> Package contents</label>
                                            <div class="col-sm-8">
                                             	<label class="radio-inline"><input type="radio" name="pcontents" value="DOCUMENTS">Documents</label>
												<label class="radio-inline"><input type="radio" name="pcontents" value="NON_DOCUMENTS" checked>Products/Commodities</label>                                              
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="control-label col-sm-4" for="State"><span>*</span> Shipment purpose</label>
                                            <div class="col-sm-8">
                                              <select name="Purpose" id="State" class="form-control" >
                                                <option value="SOLD" selected>Commercial (sold)</option>
												<option value="PERSONAL">Personal use (not sold)</option>
                                              </select>
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="control-label col-sm-4" for="Address2"> <span>*</span> Total invoice value </label>
                                            <div class="col-sm-4">
                                              <input type="text" name="total_invoice_value" id="Address2" class="form-control"value="<?php echo $order -> total; ?>">         
                                            </div>
                                            <div class="col-sm-4">
                                             Indian Rupees 
                                            </div>                                           
                                            <div class="col-sm-8">
                                              <a href="#">$ Currency Converter</a>
                                            </div>
                                          </div>
                                          
                                          <div class="form-group">
                                            <label class="control-label col-sm-4" for="State"><span>&nbsp;</span> Freight on value</label>
                                            <div class="col-sm-8">
                                              <select name="State" id="State" class="form-control" >
                                                <option value="CR">Carrier risk</option>
                                                <option value="OR">Own risk</option>
                                              </select>
                                            </div>
                                          </div>
                                          
                                          <div class="form-group">
                                            <label class="control-label col-sm-4" for="State"><span>*</span> Service type</label>
                                            <div class="col-sm-8">
                                              <select name="ServiceType" id="State" class="form-control" >
                                                <option value="PRIORITY_OVERNIGHT" >Priority Overnight</option>
                                                <option value="STANDARD_OVERNIGHT" selected>Standard Overnight</option>
                                                <option value="FEDEX_ECONOMY">FedEx Economy</option>
                                              </select>
                                            </div>
                                          </div>
                                          
                                          <div class="form-group">
                                            <label class="control-label col-sm-4" for="State"><span>*</span> Package type</label>
                                            <div class="col-sm-8">
                                              <select name="PackagingType" id="State" class="form-control" >
                                                <option value="FEDEX_BOX" >Fedex Box</option>
                                                <option value="FEDEX_PAK">Fedex Pak</option>
                                                <option value="FEDEX_TUBE">FedEx Tube</option>
                                                <option value="YOUR_PACKAGING" selected>Your Packaging</option>
                                              </select>
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="control-label col-sm-4" for="State"><span>&nbsp;</span> Dimensions</label>
                                            <div class="col-sm-8" id="hideDimen">
                                              <select name="State" id="dimen" class="form-control" onChange="changeDimensions()">
                                                <option value="0">Select</option>
                                                <option value="dimensions">Dimensions</option>
                                              </select>
                                            </div>
                                            <div class="col-sm-8" id="showDimen" style="display:none;">
                                              <div class="row gutter-10">
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" placeholder="L">
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" placeholder="W">
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" placeholder="H">
                                                    </div>
                                                    <div class="col-sm-3">
                                                        cm
                                                    </div>
                                                </div> 
                                            </div>
                                          </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                            </div>                            
							<!-- end of block3 -->
							
							
							
							<div class="form-block-1" style="display:none;">
                        		<div class="panel-group">
                              <div class="panel panel-default">
                                <div class="panel-heading">
                                  <h4 class="panel-title">
                                    <a data-toggle="collapse" href="#billingDetails">
                                    	<div class="clearfix">
                                            <span class="pull-left">4. Billing Details</span>
                                            <span class="pull-right help-icon">
                                                <i class="glyphicon glyphicon-question-sign"></i> Help
                                            </span>
                                        </div>
                                    </a>
                                  </h4>
                                </div>
                                <div id="billingDetails" class="panel-collapse collapse in">
                                	<div class="wrapper">
                                  	  <div class="form-group">
                                        <label class="control-label col-sm-5" for="fastShip"><span>*</span> Bill transportation to</label>
                                        <div class="col-sm-7">
                                          <select name="fastShip" id="fastShip" class="form-control" >
                                          	<option value="1">Select</option>
                                          	<option value="2">Name 1</option>
                                          	<option value="3">Name 1</option>
                                          </select>
                                        </div>
                                      </div>
                                      <div class="form-group">
                                        <label class="control-label col-sm-5" for="fastShip"><span>*</span> Bill duties/taxes/fees to</label>
                                        <div class="col-sm-7">
                                          <select name="fastShip" id="fastShip" class="form-control" >
                                          	<option value="1">Select</option>
                                          	<option value="2">Name 1</option>
                                          	<option value="3">Name 1</option>
                                          </select>
                                        </div>
                                      </div>
                                      <div class="form-group">
                                        <label class="control-label col-sm-5" for="fastShip"><span>&nbsp;</span> Account no.</label>
                                        <div class="col-sm-7">
                                          <input type="text" class="form-control">
                                        </div>
                                      </div>
                                      <div class="form-group">
                                        <label class="control-label col-sm-5" for="fastShip"><span>&nbsp;</span> Your reference</label>
                                        <div class="col-sm-7">
                                          <input type="text" class="form-control">
                                        </div>
                                      </div>
                                      <div class="row">
                                      	<div class="col-sm-6">
                                        	<a class="btn-link" data-toggle="collapse" href="#moreref">More reference fields</a>
                                        </div>
                                      	<div class="col-sm-6 text-right">
                                        	<a class="btn-link" href="#">Add an Account</a>
                                        </div>
                                      </div>
                                      <div class="collapse" id="moreref">
                                          <div class="form-group">
                                            <label class="control-label col-sm-5" for="fastShip"><span>&nbsp;</span> P.O. no.</label>
                                            <div class="col-sm-7">
                                              <input type="text" class="form-control">
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="control-label col-sm-5" for="fastShip"><span>&nbsp;</span> Invoice no.</label>
                                            <div class="col-sm-7">
                                              <input type="text" class="form-control">
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="control-label col-sm-5" for="fastShip"><span>&nbsp;</span> Department no.</label>
                                            <div class="col-sm-7">
                                              <input type="text" class="form-control">
                                            </div>
                                          </div>
                                      </div>
                                      
                                    </div>
                                </div>
                              </div>
                            </div>
                            </div>
							
                            <div class="form-block-1" style="display:none;">
                        		<div class="panel-group">
                              <div class="panel panel-default">
                                <div class="panel-heading">
                                  <h4 class="panel-title">
                                    <a data-toggle="collapse" href="#seroptional">
                                    	<div class="clearfix">
                                            <span class="pull-left">Special Services (optional)</span>
                                            <span class="pull-right help-icon">
                                                <i class="glyphicon glyphicon-question-sign"></i> Help
                                            </span>
                                        </div>
                                    </a>
                                  </h4>
                                </div>
                                <div id="seroptional" class="panel-collapse collapse in">
                                	<div class="wrapper">
                                  		<p>Please enter required shipment information prior to selecting special services.</p>
                                    </div>
                                </div>
                              </div>
                            </div>
                            </div>
                            <div class="form-block-1">
                        		<div class="panel-group">
                              <div class="panel panel-default">
                                <div class="panel-heading">
                                  <h4 class="panel-title">
                                    <a data-toggle="collapse" href="#pickup">
                                    	<div class="clearfix">
                                            <span class="pull-left">4. Pickup/Drop-off</span>
                                            <span class="pull-right help-icon">
                                                <i class="glyphicon glyphicon-question-sign"></i> Help
                                            </span>
                                        </div>
                                    </a>
                                  </h4>
                                </div>
                                
                                <div id="pickup" class="panel-collapse collapse in">
                                    <div class="wrapper">
                                  		<div class="form-group">
                                        <label class="control-label col-sm-4" for="fastShip"><span>*</span> Service Feature</label>
                                        <div class="col-sm-8">
                                          <select name="SpecialServiceTypes" class="form-control" >
                                          	<option value="COD">COD</option>
                                          	<option value="DELIVERY_ON_INVOICE_ACCEPTANCE">DIA</option>
                                          	<option value="COD_AND_DELIVERY_ON_INVOICE_ACCEPTANCE">COD/DIA</option>
                                          </select>
                                          <span style="font-size:9px;">Collect on Delivery(COD) / Delivery on Invoice Acceptance(DIA)</span>	
                                        </div>
                                      </div>
                                      <div class="form-group">
                                        <label class="control-label col-sm-4" for="fastShip"><span>*</span> Drop of Type</label>
                                        <div class="col-sm-8">
                                          <select name="DropoffType" class="form-control">
                                            	<option value="REGULAR_PICKUP" selected>Regular Pickup</option>
                                                <option value="REQUEST_COURIER">Request Courier</option>
                                                <option value="DROP_BOX">Drop Box</option>
                                                <option value="BUSINESS_SERVICE_CENTER">Busines Service Center</option>
                                                <option value="STATION">Station</option>
                                            </select>
                                        </div>
                                      </div>
                                      <div class="form-group" id="pickup_details">
                                        <label class="control-label col-sm-4" for="fastShip"><span>*</span> Pickup Schedule</label>
                                        <div class="col-sm-8">
                                          <select name="PickupDetail" id="PickupDetail" class="form-control">
                                            	<option value="0" selected>Select Pickup Schedule</option>
                                                <option value="schedule_time">Schedule a pickup</option>
                                                <option value="fedex_location">Drop off package at a FedEx location</option>
                                                <option value="pickup_later">Use an already scheduled pickup/Schedule a pickup later</option>
                                            </select>
                                        </div>
                                      </div>
                                      
                                      <div class="form-group" id="pickup_shedule" style="display:none;">
                                      	<label class="control-label col-sm-4" for="fastShip"><span>*</span> Pickup Address</label>
                                        <div class="col-sm-8">DR NARENDRANATHA REDDY,<br/> # 97/B/1/1, KANAKAPURA MNRD, 31ST CRS,7TH BLOCK, JAYANAGAR,<br/> BANGALORE, 560082, INDIA</div>
                                        <label class="control-label col-sm-4" for="fastShip"><span>*</span> Pick Time</label>
                                        <div class="col-sm-4">
                                            <select name="PickupDetail" id="PickupDetail" class="form-control">
                                            	<option value="1000">10:00 a.m</option>
                                                <option value="1030">10:00 a.m</option>
                                                <option value="1100">10:00 a.m</option>
                                                <option value="1130">10:00 a.m</option>
                                                <option value="1200">12:00 p.m.</option>
                                                <option value="1230">12:30 p.m.</option>
                                                <option value="1300">01:00 p.m.</option>
                                                <option value="1330">01:30 p.m.</option>
                                                <option value="1400">02:00 p.m.</option>
                                                <option value="1430">02:30 p.m.</option>
                                                <option value="1500">03:00 p.m.</option>
                                                <option value="1530">03:30 p.m.</option>
                                                <option value="1600">04:00 p.m.</option>
                                                <option value="1630">04:30 p.m.</option>
                                                <option value="1700">05:00 p.m.</option>
                                                <option value="1730">05:30 p.m.</option>
                                                <option value="1800">06:00 p.m.</option>
                                                <option value="1830">06:30 p.m.</option>
                                                <option value="1900">07:00 p.m.</option>
                                            </select>
                                            <span style="font-size:9px;">Ready Time</span>	
                                        </div>
                                        <div class="col-sm-4" style="float:right;">
                                          <select name="PickupDetail" id="PickupDetail" class="form-control">
                                            	<option value="1300">01:00 p.m.</option>
                                                <option value="1330">01:30 p.m.</option>
                                                <option value="1400">02:00 p.m.</option>
                                                <option value="1430">02:30 p.m.</option>
                                                <option value="1500">03:00 p.m.</option>
                                                <option value="1530">03:30 p.m.</option>
                                                <option value="1600">04:00 p.m.</option>
                                                <option value="1630">04:30 p.m.</option>
                                                <option value="1700">05:00 p.m.</option>
                                                <option value="1730">05:30 p.m.</option>
                                                <option value="1800">06:00 p.m.</option>
                                                <option value="1830">06:30 p.m.</option>
                                                <option value="1900">07:00 p.m.</option>
                                                <option value="1930">07:30 p.m.</option>
                                                <option value="2000">08:00 p.m.</option>
                                                <option value="2030">08:30 p.m.</option>
                                                <option value="2100">09:00 p.m.</option>
                                                <option value="2130">09:30 p.m.</option>
                                                <option value="2200">10:00 p.m.</option>
                                                <option value="2230">10:30 p.m.</option>
                                                <option value="2300">11:00 p.m.</option>
                                                <option value="2330">11:30 p.m.</option>
                                                <option value="2400">12:00 a.m.</option>
                                            </select>
                                            <span style="font-size:9px;">Latest Available Time</span>	
                                        </div>
                                      </div>
                                    <div class="form-group" id="pickup_shedule1" style="display:none;">
                                        <label class="control-label col-sm-4" for="fastShip">&nbsp;&nbsp;&nbsp;&nbsp;Any instructions</label>
                                        <div class="col-sm-8">
                                        	<input type="text" name="CourierInstructions" class="form-control" /> 
                                        </div>
                                    </div>
                                    
                                </div>
                              </div>
                            </div>
                            </div>
							
                            <div class="form-block-1">
                        		<div class="panel-group">
                              <div class="panel panel-default">
                                <div class="panel-heading">
                                  <h4 class="panel-title">
                                    <a data-toggle="collapse" href="#complete">
                                    	<div class="clearfix">
                                            <span class="pull-left">5. Complete your Shipment</span>
                                            <span class="pull-right help-icon">
                                                <i class="glyphicon glyphicon-question-sign"></i> Help
                                            </span>
                                        </div>
                                    </a>
                                  </h4>
                                </div>
                                <div id="complete" class="panel-collapse collapse in">
                                	<div class="wrapper">
                                    	<div><label class="checkbox-inline"><input type="checkbox" name="notifyt"> Create a Shipment Profile to store recipient, package and all other details of this shipment for future use.</label></div>
                                        
                                        <div class="form-group" style="margin-top:10px;"> 
                                        <div class="row">
                                        <div class="col-sm-12 text-right">
											<input type="hidden" name="Key" value="VbNYGYmJbrpLRY18<?php //echo $setting_info['key']->setting;?>"  />
											<input type="hidden" name="Password" value="jN1DEXbvYlEuP5AuPFTEnUzMd<?php //echo $setting_info['password']->setting;?>"  />
											<input type="hidden" name="AccountNumber" value="781435546<?php //echo $setting_info['meter']->setting;?>" />
											<input type="hidden" name="MeterNumber" value="109803879<?php //echo $setting_info['shipaccount']->setting;?>"/>
											<input type="hidden" name="PaymentType" value="SENDER" class="form-control input-sm" />
											<input type="hidden" name="CollectionType" value="GUARANTEED_FUNDS" class="form-control input-sm" />
											<input type="hidden" name="Currency" value="INR" class="form-control input-sm" />
											<input type="hidden" name="CustomerReferenceType" value="CUSTOMER_REFERENCE" class="form-control input-sm" />
											<input type="hidden" name="CustomerReferenceValue" value="GR4567892" class="form-control input-sm" />
                                            <button class="btn btn-primary" type="submit">Ship</button>
                                        </div>
                                      </div>
                                      	</div>
                                    </div>
                                </div>
                              </div>
                            </div>
                            </div>
                        </div>
                </form>
            </div>
           </div>
           </div>

<script src="js/jquery-1.12.4.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
function noOfpackages(){
	var target = $('#packages').val();
	//alert(target);
	if(target == 1){
		$("#arePackagesIdentica").hide();
	}else{
		$("#arePackagesIdentica").show();
	}
}

function showTable(){
	var target = $('#apiNo').val();
	if(target == "no"){
		$("#showTable").show();
	}else{
		$("#showTable").hide();
	}
}

function hideTable(){
	var target = $('#apiYes').val();
	if(target == "yes"){
		$("#showTable").hide();
	}else{
		$("#showTable").show();
	}
}

function changeDimensions(){
	var target = $('#dimen').val();
	//alert(target);
	if(target == 'dimensions'){
		$("#showDimen").show();
		$("#hideDimen").hide();
	}
}


</script><?php
$this->load->view('admin/adminFooter'); ?>
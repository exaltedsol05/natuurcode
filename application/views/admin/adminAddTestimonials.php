<?php $this->load->view('admin/adminHeader');?>
<?php $this->load->view('admin/adminLeftSidebar');?>
<div class="content-wrapper"> 
               <!-- Content Header (Page header) -->  

			   <section class="content-header">
			   <h1>                        Add/Edit Testimonial                        <small>
			   admin panel</small>                    </h1>    
			   <ol class="breadcrumb">      
			   <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>                        
			   <li><a href="#"> Catalog</a></li>  
			   <li class="active"> Add/Edit Testimonial</li> 
			   </ol>                </section>               
			   <?php $this->load->view('admin/adminError');?>   
			   <!-- Main content -->				
			   <section class="content">				
			   <div class="row">						
			   <div class="col-md-12">					
			   <div class="box box-primary">		
			   <div class="box-header">
			   </div>								
			   <div class="box-body">							         	
			   <?php 
			   $attributes = array("name" => "testimonial_form", "id" => "testimonial_form");
			   echo form_open_multipart("admin/testimonials/form/$id", $attributes);
			   ?>			
			   <input type="hidden" value="<?php echo $id;?>" name="testimonial_id">																		
			   
			   <fieldset>
				<label for="name">User Image</label>	
				<?php if($image != ""){?>
				<img src="<?php echo base_url().'uploads/testimonial/small/'.$image; ?>" class="img-square img-responsive" style="">	
				<?php }?><br>
				<input id="image" name="image" type="file" multiple="true" class='form-control'>
			   
				
			   <label for="name">User Name</label>										<?php										
			   $data	= array('name'=>'name', 'value'=>set_value('name', $name), 'class'=>'form-control');			
			   echo form_input($data);
			   ?>
			   
			   <label for="email_id">User Email</label>										
			   <?php	
			   $data	= array('name'=>'email_id', 'value'=>set_value('email_id', $email_id), 'class'=>'form-control');			
			   echo form_input($data);				
			   ?>											
			   
			   <label for="company">Company Name</label>										<?php										
			   $data	= array('name'=>'company', 'value'=>set_value('company', $company), 'class'=>'form-control');			
			   echo form_input($data);										
			   ?>
			   
			   <label for="designation">Designation</label>		
			   <?php										
			   $data	= array('name'=>'designation', 'value'=>set_value('designation', $designation), 'class'=>'form-control');	
			   echo form_input($data);						
			   ?>
			   
			   <label for="content">Testimonial Content </label>											
			   <textarea name="content" class="form-control redactor" rows="10" id="content" value="<?php echo $testimonial->content; ?>"><?php echo $content; ?></textarea>						
			   </fieldset>								
			   <div class="pull-left">			
			   <button type="submit" class="btn btn-primary" style="margin:10px 0px 10px 10px;">Save</button>								
			   </div>								 
			   <?php echo form_close();?>												    </div>								
			   <div class="box-footer">									 	
			   </div>							     		
			   </div>						
			   </div>				
			   </div>														
			   </section><!-- /.content --></div><!-- /.right-side -->
<script type="text/javascript">
	$('form').submit(function() {	
		$('.btn').attr('disabled', true).addClass('disabled');
	});
	$('.redactor').redactor({
		//plugins: ['fontcolor'],
		buttons: [
			'bold',
			'italic',
			'underline',
			'formatting',
			'image',
			'link',
			'unorderedlist',
			'orderedlist',
			'alignment',
			'|',
			'html'
		],
		minHeight: 200,
		imageUpload: '<?php echo base_url(); ?>admin/wysiwyg/upload_image',
		fileUpload: '<?php echo base_url(); ?>admin/wysiwyg/upload_file',
		imageGetJson: '<?php echo base_url(); ?>admin/wysiwyg/get_images',
		imageUploadErrorCallback: function(json)
		{
			alert(json.error);
		},
		fileUploadErrorCallback: function(json)
		{
			alert(json.error);
		}
	});
</script>
<?php $this->load->view('admin/adminFooter');?>
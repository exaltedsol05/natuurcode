<?php $this->load->view('admin/adminHeader');?>
<?php $this->load->view('admin/adminLeftSidebar');?>
<?php $this->load->view('admin/adminLeftSidebar');

$arrQuestion_type=array('1'=>'Case based/Clinical','2'=>'Picture type','3'=>'Match the Following','4'=>'Regular MCQs','5'=>'True/False','6'=>'Reason & Assertion');

$arrDiffeculty_type=array('1'=>'Easy','2'=>'Moderate','3'=>'Difficult');

?>
<style>
.status {
		font-size: 30px;
		margin: 2px 2px 0 0;
		display: inline-block;
		vertical-align: middle;
		line-height: 10px;
	}
    .text-success {
        color: #10c469;
    }
    .text-info {
        color: #62c9e8;
    }
    .text-warning {
        color: #FFC107;
    }
    .text-danger {
        color: #ff5b5b;
    }
</style>

<aside class="right-side">
                <!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				Question List
				<small>admin panel</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li><a href="#"> Catalog</a></li>
				<li class="active"> Question List</li>
			</ol>
			
		</section>
                <?php $this->load->view('admin/adminError');?>
                <!-- Main content -->
 <section class="content">
	<div class="col-xs-12">
		<div class="alert alert-info">
		    <h3>Note:</h3>
			<p>formative:core/non_core</p>
			<p>category_name:clinical/memory/concept</p>
			<p>difficulty_level:1 for Easy/2 for Moderate/3 for Difficult</p>
			<p>question_type:1 for Case based Clinical, 2 for Picture type, 3 for Match the Following,4 for Regular MCQs,5 for True/False,6 for Reason & Assertion</p>
			<p>question_option:2,4</p>
			<p><a href="<?php echo site_url('uploads/app/questionData.csv');?>" class="btn btn-danger" ><i class="fa fa-download"></i> Download sample CSV file</a></p>
		</div>
	</div>
    <!-- Display status message -->
    <?php if(!empty($success_msg)){ ?>
    <div class="col-xs-12">
        <div class="alert alert-success"><?php echo $success_msg; ?></div>
    </div>
	 <?php } ?>
    <?php if(!empty($error_msg)){ ?>
    <div class="col-xs-12">
        <div class="alert alert-danger"><?php echo $error_msg; ?></div>
    </div>
    <?php } ?>
	
    <div class="row">
        <!-- Import link -->
        <div class="col-md-12 head">
            <div class="float-right">
                
            </div>
        </div>
		
       
		<div class="col-xs-12">
		 <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Display all questions</h3>
									<div class="pull-right" style="padding: 10px;">
									<a href="javascript:void(0);" class="btn btn-success" onclick="formToggle('importFrm');"><i class="plus"></i> Import</a>
								    </div>
									 <!-- File upload form -->
        <div class="col-md-12" id="importFrm" style="display: none;">
            <form action="<?php echo base_url('admin/products/import'); ?>" method="post" enctype="multipart/form-data">
				<div class="col-sm-3">		
                    <label for="name">File Upload</label>				
					<input type="file" name="file" required />				
				</div>
				<div class="col-sm-3">
				  <br/>
				 
				  <input type="submit" class="btn btn-primary" name="importSubmit" value="IMPORT">
				</div>
               
            </form>
        </div>
		
                                </div><!-- /.box-header -->
        <div class="box-body table-responsive">
        <table id="question_tbl" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sr. No.</th>
												<th style="display:none;">Subject Name</th>
												<th style="display:none;">Chapter Name</th>
												<th>Topic Name</th>
                                                <th>Question</th>
												<th>Question Type</th>
												<th>Difficulty Level</th>
												
                                                <th>Status</th>
												
												<th style="width:60px;">Action</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
										 <?php 
                                                $i=0;
												foreach ($question as $arrquestion){ $i++;?>
												<tr>
												<td><?php echo  $i; ?></td>
												<td style="display:none;"><?php echo  $arrquestion->subject_name; ?></td>
												<td style="display:none;"><?php echo  $arrquestion->chapter_name; ?></td>
												<td><?php echo  $arrquestion->topic_name; ?></td>
												<td><?php echo  $arrquestion->name; ?></td>
												<td><?php echo  $arrQuestion_type[$arrquestion->question_type]; ?></td>
												<td><?php echo  $arrDiffeculty_type[$arrquestion->difficulty_level]; ?></td>
												
												<td><?php echo ($arrquestion->is_active == '1') ? '<span class="status text-success">•</span>Enabled' : '<span class="status text-danger">•</span>Disabled'; ?></td>
												
												
												<td>
												 
												
												<a class="btn btn-sm btn-info" href="<?php echo  site_url('admin/question/form/'.$arrquestion->id);?>"><i class="fa fa-pencil"></i></a>
												<a class="btn btn-sm btn-danger" href="<?php echo  site_url('admin/question/delete/'.$arrquestion->id);?>" onclick="return areyousure();"><i class="fa fa-trash-o"></i></a>
												
												</td>
												</tr>
												<?php
											}
											
											?>
                                            
											
                                        </tbody>
                                        
                                    </table>
         </div><!-- /.box-body -->
         </div><!-- /.box -->                       
         </div>
	</div>
	</section>



</aside>
<script>
function formToggle(ID){
    var element = document.getElementById(ID);
    if(element.style.display === "none"){
        element.style.display = "block";
    }else{
        element.style.display = "none";
    }
}
</script>
<?php  $this->load->view('admin/adminFooter'); ?>
<?php $this->load->view('admin/adminHeader'); ?>
<?php $this->load->view('admin/adminLeftSidebar');?>

	<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	   <!-- Content Header (Page header) -->
		<section class="content-header">
		  <h1>
			Dashboard
			
		  </h1>
		  <ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Dashboard</li>
		  </ol>
		</section>


	<!-- Main content -->
	<section class="content">
		<!-- Info boxes -->
		<div class="row">
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box">
					<span class="info-box-icon bg-aqua"><i class="fa fa-globe"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Total Brands</span>
						<span class="info-box-number"><?php echo count($total_brand); ?></span>
					</div>
					<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
			</div>
			<!-- /.col -->
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box">
					<span class="info-box-icon bg-red"><i class="fa fa-cubes"></i></span>

					<div class="info-box-content">
						<span class="info-box-text">Total Products</span>
						<span class="info-box-number"><?php echo count($total_products); ?></span>
					</div>
					<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
			</div>
			<!-- /.col -->

			<!-- fix for small devices only -->
			<div class="clearfix visible-sm-block"></div>

			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box">
					<span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

					<div class="info-box-content">
						<span class="info-box-text"> Total Sales</span>
						<span class="info-box-number"><?php echo $total_order; ?></span>
					</div>
				<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
			</div>
			<!-- /.col -->
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box">
					<span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

					<div class="info-box-content">
						<span class="info-box-text">Total Members</span>
						<span class="info-box-number"><?php echo count($total_customers); ?></span>
					</div>
				<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->

		<div class="row">
			<div class="col-md-6">
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">Weekly Report</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
							<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
					  <div class="chart">
						<canvas id="weeklyChart" ></canvas>
					  </div>
					</div>
				<!-- /.box-body -->
				</div>
			<!-- /.box -->
			</div>
			
			<div class="col-md-6">
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">Current Year Sale</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
							<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
					  <div class="chart">
						<canvas id="yearChart" ></canvas>
					  </div>
					</div>
				<!-- /.box-body -->
				</div>
			<!-- /.box -->
			</div>


			
		</div>
		<!-- /.row -->

		<!-- Main row -->
		<div class="row">
		<!-- Left col -->
			<div class="col-md-6">
			<!-- TABLE: LATEST ORDERS -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Latest Orders</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
							<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="table-responsive">
							<table class="table no-margin">
								<thead>
									<tr>
									<th>Order ID</th>
									<th>Biil To</th>
									<th>Status</th>
									<th>Total</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									$count = 1;
									foreach($recent_order as $order){?>
										<tr>
											<td><a href="<?php echo base_url()."admin/orders/order/".$order->id; ?>"><?php echo $order->order_number; ?></a></td>
											<td><?php echo ucfirst($order->bill_firstname)." ".ucfirst($order->bill_lastname); ?></td>
											<td><span class="label label-success"><?php echo $order->status; ?></span></td>
											<td>
												<?php 
												$etotal = $order->total;
												if($order->reward_amt>0){
													$etotal = $etotal - ($order->reward_amt/100);
												}
												
												if($order->shipping > 0){
													$etotal = $etotal + $order->shipping;
												}
												echo sprintf("%.2f", $etotal); ?>
											</td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					<!-- /.table-responsive -->
					</div>
					<!-- /.box-body -->
					<div class="box-footer clearfix">
					
						<a href="<?php echo base_url('admin/orders'); ?>" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
					</div>
				<!-- /.box-footer -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->

			<div class="col-md-6">
				<!-- PRODUCT LIST -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Recently Added Products</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
							<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<ul class="products-list product-list-in-box">
							<?php 
								$count = 1;
								foreach($recent_product as $product){
									$image = '';
									foreach(json_decode($product->images) AS $img){
										$image = $img->filename;
									}
							?>
									<li class="item">
										<div class="product-img">
										<img src="<?php echo base_url().'uploads/images/thumbnails/'.$image ?>" alt="Product Image">
										</div>
										<div class="product-info">
											<a href="javascript:void(0)" class="product-title"><?php echo $product->sku ?>
											<span class="label label-warning pull-right"><?php echo $product->saleprice ?></span></a>
											<span class="product-description">
											<?php echo $product->description ?>
											</span>
										</div>
									</li>
							<?php } ?>
							<!-- /.item -->
						</ul>
					</div>
					<!-- /.box-body -->
					<div class="box-footer text-center">
						<a href="<?php echo base_url('admin/products'); ?>" class="uppercase">View All Products</a>
					</div>
					<!-- /.box-footer -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		
		<div class="row">
			<div class="col-md-6">
				<div class="box box-danger">
					<div class="box-header with-border">
						<h3 class="box-title">Latest Members</h3>
						<div class="box-tools pull-right">
							<span class="label label-danger"></span>
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<ul class="users-list clearfix">
							<?php 
							$image = base_url().'uploads/customer_image/default.png';
							foreach($total_customers as $cust){ 
								if($cust->image){
									$image = base_url().'uploads/customer_image/small/'.$cust->image;
								}?>
								<li>
									<img src="<?php echo $image; ?>" alt="User Image">
									<a class="users-list-name" href="#"><?php echo $cust->firstname; ?></a>
								</li>
							<?php } ?>
						</ul>
					<!-- /.users-list -->
					</div>
					<!-- /.box-body -->
					<div class="box-footer text-center">
					<a href="<?php echo base_url('admin/customers'); ?>" class="uppercase">View All Users</a>
					</div>
					<!-- /.box-footer -->
				</div>
			</div>
			
			<div class="col-md-6">
				<!-- LINE CHART -->
				<div class="box box-info">
				<div class="box-header with-border">
				<h3 class="box-title">Total Order</h3>

				<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
				</button>
				<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
				</div>
				<div class="box-body">
				<div class="chart">

				<div id="piechart" style="height: 350px;"></div>
				</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer text-center">
				<a href="<?php echo base_url('admin/orders'); ?>" class="uppercase">View All Orders</a>
				</div>
				</div>
				<!-- /.box -->
			</div>

		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Order Placed',<?php echo $orderbystatus['order_placed']; ?>],
          ['Cancle',<?php echo $orderbystatus['cancelled']; ?>],
          ['Pending',<?php echo $orderbystatus['pending']; ?>],
		  ['Delivered',<?php echo $orderbystatus['delivered']; ?>],
		  ['Processing',<?php echo $orderbystatus['processing']; ?>],
		  ['Shipped',<?php echo $orderbystatus['shipped']; ?>],
		  ['On Hold',<?php echo $orderbystatus['on_hold']; ?>] 
        ]);

        var options = {
          title: ''
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>
<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/chartjs/Chart.min.js"></script>
<script>
 $(function () {
	 //-------------
    //- WEEK CHART -
    //--------------
	var barChartData = {
      labels: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
      datasets: [
        {
          label: "Customer",
          fillColor: "rgba(210, 214, 222, 1)",
          strokeColor: "rgba(210, 214, 222, 1)",
          pointColor: "rgba(210, 214, 222, 1)",
          pointStrokeColor: "#c1c7d1",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: [1000, 2000, 50000, 10000, 2000, 4440, 4450]
        }
      ]
    };
    var barChartCanvas = $("#weeklyChart").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas);
    var barChartData = barChartData;
    barChartData.datasets[0].fillColor = "#00a65a";
    barChartData.datasets[0].strokeColor = "#00a65a";
    barChartData.datasets[0].pointColor = "#00a65a";
    var barChartOptions = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: true,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - If there is a stroke on each bar
      barShowStroke: true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth: 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing: 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing: 1,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to make the chart responsive
      responsive: true,
      maintainAspectRatio: true
    };

    barChartOptions.datasetFill = false;
    barChart.Bar(barChartData, barChartOptions);
	
	
	//-------------
    //- YEAR CHART -
    //--------------
	var barChartData = {
      labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October","November","December"],
      datasets: [
        {
          label: "Customer",
          fillColor: "rgba(210, 214, 222, 1)",
          strokeColor: "rgba(210, 214, 222, 1)",
          pointColor: "rgba(210, 214, 222, 1)",
          pointStrokeColor: "#c1c7d1",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: [1000, 2000, 50000, 10000, 2000, 4440, 4450,6565, 5239, 3480, 34581, 43256]
        }
      ]
    };
    var barChartCanvas = $("#yearChart").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas);
    var barChartData = barChartData;
    barChartData.datasets[0].fillColor = "#00a65a";
    barChartData.datasets[0].strokeColor = "#00a65a";
    barChartData.datasets[0].pointColor = "#00a65a";
    var barChartOptions = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: true,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - If there is a stroke on each bar
      barShowStroke: true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth: 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing: 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing: 1,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to make the chart responsive
      responsive: true,
      maintainAspectRatio: true
    };

    barChartOptions.datasetFill = false;
    barChart.Bar(barChartData, barChartOptions);
 });
</script>

   
<?php $this->load->view('admin/adminFooter');?>
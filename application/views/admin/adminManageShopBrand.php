<?php $this->load->view('admin/adminHeader');?>
<?php $this->load->view('admin/adminLeftSidebar');?>
<script type="text/javascript">
	function areyousure()
	{
		return confirm('Are you sure to delete this Shop Brand?');
	}
</script>
 <div class="content-wrapper">
<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Manage Brand
			<small>admin panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#"> Catalog</a></li>
			<li class="active"> Manage Brand</li>
		</ol>
	</section>
	<?php $this->load->view('admin/adminError');?>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-sm-12">		   
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Display all Shop By Brand</h3>
				</div><!-- /.box-header -->
				<div style="text-align:right">
					<a class="btn btn-primary" href="<?php echo site_url('admin/shopbrands/form'); ?>"><i class="fa fa-plus"></i> Add New Brand</a>
				</div>
				<div class="box-body table-responsive">
					<table id="example1" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>IN</th>
								<th>ShopBrand</th>
								<th>Image</th>
								<th>Status</th>
								<th>Action</th>
								
							</tr>
						</thead>
						<tbody>
						 <?php 
                               $i=0;
								foreach ($shopbrand as $cat){ $i++;?>
								<tr>
								<td><?php echo  $i; ?></td>
								<td><?php echo  $cat->name; ?></td>
								<td><img src="<?php echo base_url(); ?>uploads/Brand/small/<?php echo $cat->brand_image; ?>" style="height:50px; width:50px;"></td>
								<td><?php echo ($cat->is_active == '1') ? 'Enabled' : 'Disabled'; ?></td>
								<td style="width:260px;">
								
								<a class="btn btn-info" href="<?php echo  site_url('admin/shopbrands/form/'.$cat->id);?>"><i class="fa fa-pencil"></i> edit</a>


								<a class="btn btn-danger" href="<?php echo  site_url('admin/shopbrands/delete/'.$cat->id);?>" onclick="return areyousure();"><i class="fa fa-trash-o"></i> delete</a>
								
								</td>
								</tr>
								<?php
							}
							
							?>
							
							
						</tbody>
						
					</table>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div>
	</div>

</section><!-- /.content -->

</div><!-- /.right-side -->
<script>
  $(function () {
	
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": true,
	  "pageLength": 20,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>
<?php
$this->load->view('admin/adminFooter');
?>
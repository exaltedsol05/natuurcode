<?php
$this->load->view('admin/adminHeader');
?>
<?php
$this->load->view('admin/adminLeftSidebar');
?>
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <?php echo $page;?>
                        <small>admin panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#"> Sales</a></li>
                        <li class="active"> <?php echo $page;?></li>
                    </ol>
                </section>
                <?php $this->load->view('admin/adminError');?>
                <!-- Main content -->
                <section class="content">
<?php
	//set "code" for searches
	if(!$code)
	{
		$code = '';
	}
	else
	{
		$code = '/'.$code;
	}
	function sort_url($lang, $by, $sort, $sorder, $code, $admin_folder)
	{
		if ($sort == $by)
		{
			if ($sorder == 'asc')
			{
				$sort	= 'desc';
				$icon	= ' <i class="icon-chevron-up"></i>';
			}
			else
			{
				$sort	= 'asc';
				$icon	= ' <i class="icon-chevron-down"></i>';
			}
		}
		else
		{
			$sort	= 'asc';
			$icon	= '';
		}
			

		$return = site_url('admin/orders/index/'.$by.'/'.$sort.'/'.$code);
		
		echo '<a href="'.$return.'">'.$lang.$icon.'</a>';

	}
	
if ($term):?>

<div class="alert alert-info">
	<?php echo sprintf('Your searched returned %d result(s)', intval($total));?>
</div>
<?php endif;?>

<style type="text/css">
	.pagination {
		margin:0px;
		margin-top:-3px;
	}
</style>
<div class="row">
	<div class="col-sm-12" style="border-bottom:1px solid #f5f5f5;">
		<div class="row">
			<div class="col-sm-3">
				<?php echo $this->pagination->create_links();?>&nbsp;
			</div>
			<div class="col-sm-12">
				<?php echo form_open('admin'.'/giftcards/gift', 'class="form-inline" style="float:right"');?>
					<fieldset>
						<input name="record_no" value="" class="form-control" type="text" placeholder="Total Record" />
						<input type="text" value="" name= "record_amt" class="form-control" placeholder="Total Amount" />
						<select class="form-control" name="record_amt_criteria">
							<option value="0" disabled selected>Select Option</option>
							<option value="up">Greater Then</option>
							<option value="down">Less Then</option>
						</select>
						<select class="form-control" name="record_order">
							<option value="0" disabled selected>Select Option</option>
							<option value="ASC">Asc</option>
							<option value="DESC">Desc</option>
						</select>
						
						<!--
						<input type="text" class="form-control" placeholder="From Date" id="frmdate" />
						<input id="start_top"  value="" class="form-control" type="text" placeholder="From Date"/>
						<input id="end_top" value="" class="form-control" type="text"  placeholder="To Date"/>-->
						<input type="submit" class="btn" name="filter_submit" value="search">
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

<?php //echo form_open('admin'.'/orders/bulk_delete', array('id'=>'delete_form', 'onsubmit'=>'return submit_form();', 'class="form-inline"')); ?>

<table class="table" id="gift_record_table">
    <thead>
		<tr>
			<th><input type="checkbox" id="gc_check_all" /></th>
			<th><?php echo sort_url('order', 'order_number', $sort_by, $sort_order, $code, 'admin'); ?></th>
			<th><?php echo sort_url('bill_to', 'bill_lastname', $sort_by, $sort_order, $code, 'admin'); ?></th>
			<th><?php echo sort_url('ship_to', 'ship_lastname', $sort_by, $sort_order, $code, 'admin'); ?></th>
			<th><?php echo sort_url('ordered_on','ordered_on', $sort_by, $sort_order, $code, 'admin'); ?></th>
			<th><?php echo sort_url('total','total', $sort_by, $sort_order, $code, 'admin'); ?></th>
	    </tr>
	</thead>

    <tbody>
	<?php echo (count($orders) < 1)?'<tr><td style="text-align:center;" colspan="8">There are currently no orders.</td></tr>':''?>
    <?php foreach($orders as $order): ?>
	<tr>
		<td><input name="order[]" type="checkbox" value="<?php echo $order->bill_email; ?>" class="gc_check"/></td>
		<td><?php echo $order->order_number; ?></td>
		<td style="white-space:nowrap"><?php echo $order->bill_lastname.', '.$order->bill_firstname; ?></td>
		<td style="white-space:nowrap"><?php echo $order->ship_lastname.', '.$order->ship_firstname; ?></td>
		<td style="white-space:nowrap"><?php echo date('m/d/y h:i a', strtotime($order->ordered_on)); ?></td>
		<td><div class="MainTableNotes"><?php echo $order->total; ?></div></td>
	</tr>
    <?php endforeach; ?>
	<tr>
		<td colspan="6"><input type="button" value="Ready for Mail" id="gift_mail" class="btn" style="float:right;"/></td>
	<tr>
    </tbody>
</table>
<div id="mail_div" style="display:none;"><br/><br/>
	<input type="text" class="form-control" readonly="true" value="" id="emailids" /><br/>
	<input type="text" placeholder="Enter Email Subject" class="form-control" id="mail_subject"/><br/>
	<textarea name="description" cols="40" rows="10" class="form-control redactor" placeholder="Enter Email Content"id="mail_content"></textarea><br/>
	<input type="button" value="Cancel" id="gift_mail_cancel" class="btn" />
	<input type="button" value="Send Mail" id="gift_mail_done" class="btn" style="float:right;"/><br/><br/><br/>
<div>
</form>
<script type="text/javascript">
$(document).ready(function(){
	$("#gift_mail_done").click(function(){
		useremail = $("#emailids").val();
		mail_subject = $.trim($("#mail_subject").val());
		mail_content = $("#mail_content").val();
		if(mail_subject == ''){
			alert("Please enter mail subject");
			$("#mail_subject").focus();
			return false;
		}
		
		if(mail_content == ''){
			alert("Please enter mail content");
			$("#mail_content").focus();
			return false;
		}
		
		$.ajax({
			url: '<?php echo base_url(); ?>admin/giftcards/mailtouser',
			type: 'post',
			data: {'emails':useremail, 'mail_subject':mail_subject, 'mail_content':mail_content},
			success: function(data){
				alert(data);
			}
		});
	});
	
	$("#gift_mail").click(function(){
		userids = '';
		$($('.gc_check').iCheck('check')).each(function() {
			cemail = $(this).val();
			if (userids.search(cemail)) {
			  userids += cemail+",";
			} 
		});
		
		$("#emailids").val(userids);
		
		$("#gift_record_table").fadeOut( "slow", function() {
			// Animation complete.
		});
		
		$("#mail_div").fadeIn( "slow", function() {
			// Animation complete.
		});
	});
	
	$("#gift_mail_cancel").click(function(){
		$("#gift_record_table").fadeIn( "slow", function() {
			// Animation complete.
		});
		
		$("#mail_div").fadeOut( "slow", function() {
			// Animation complete.
		});
	});
	
	
	$("#frmdate").datepicker();
	
	$('#gc_check_all').on('ifChecked', function(event){
		$('.gc_check').iCheck('check'); 
	});
	
	$('#gc_check_all').on('ifUnchecked', function(event){
		$('.gc_check').iCheck('uncheck'); 
	});
});

function do_search(val){
	$('#search_term').val($('#'+val).val());
	$('#start_date').val($('#start_'+val+'_alt').val());
	$('#end_date').val($('#end_'+val+'_alt').val());
	$('#search_form').submit();
}

function do_export(val){
	$('#export_search_term').val($('#'+val).val());
	$('#export_start_date').val($('#start_'+val+'_alt').val());
	$('#export_end_date').val($('#end_'+val+'_alt').val());
	$('#export_form').submit();
}

function submit_form(){
	if($(".gc_check:checked").length > 0){
		return confirm('Are you sure you want to delete the selected orders?');
	}else{
		alert('You did not select any orders to delete');
		return false;
	}
}

function save_status(id, customer_id, change_amt){
	show_animation();
    select_status = $('#status_form_'+id).val();
	$.post("<?php echo site_url('admin/orders/edit_status'); ?>", {
		change_amt : change_amt, customer_id : customer_id, id: id, status:select_status
	}, function(data){
        //console.log(data);
        setTimeout('hide_animation()', 500);
		location.reload();
	});
	
	$('.redactor').redactor({
		plugins: ['fontcolor'],
		buttons: [
			'bold',
			'italic',
			'underline',
			'formatting',
			'image',
			'link',
			'unorderedlist',
			'orderedlist',
			'alignment',
			'|',
			'html'
		],
		minHeight: 200,
		imageUpload: 'http://localhost/dentkart/admin/wysiwyg/upload_image',
		fileUpload: 'http://localhost/dentkart/admin/wysiwyg/upload_file',
		imageGetJson: 'http://localhost/dentkart/admin/wysiwyg/get_images',
		imageUploadErrorCallback: function(json){
			alert(json.error);
		},
		
		fileUploadErrorCallback: function(json){
			alert(json.error);
		}
	});

}

function show_animation()
{
	$('#saving_container').css('display', 'block');
	$('#saving').css('opacity', '.8');
}

function hide_animation()
{
	$('#saving_container').fadeOut();
}
</script>

<div id="saving_container" style="display:none;">
	<div id="saving" style="background-color:#000; position:fixed; width:100%; height:100%; top:0px; left:0px;z-index:100000"></div>
	<img id="saving_animation" src="<?php echo base_url('assets/admin/img/ajax-loader1.gif');?>" alt="saving" style="z-index:100001; margin-left:-32px; margin-top:-32px; position:fixed; left:50%; top:50%"/>
	<div id="saving_text" style="text-align:center; width:100%; position:fixed; left:0px; top:50%; margin-top:40px; color:#fff; z-index:100001">Saving</div>
</div>
</section><!-- /.content -->
            </aside><!-- /.right-side -->
<?php
$this->load->view('admin/adminFooter');
?>
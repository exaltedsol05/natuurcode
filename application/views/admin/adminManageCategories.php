<?php
$this->load->view('admin/adminHeader');
?>
<?php
$this->load->view('admin/adminLeftSidebar');
?>
 <!-- Right side column. Contains the navbar and content of the page -->

	   
	       <script type="text/javascript">
function areyousure()
{
	return confirm('confirm_delete_category');
}
</script>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
   <!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				Categories
				<small>admin panel</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li><a href="#"> Catalog</a></li>
				<li class="active"> Categories</li>
			</ol>
		</section>
		 <?php $this->load->view('admin/adminError');?>
		
                        
    <!-- Main content -->
    <section class="content">
           <div class="row">

		<div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Display all Categories</h3>
					<div style="text-align:right">
					  <a class="btn btn-primary" href="<?php echo site_url('admin/categories/form'); ?>"><i class="fa fa-plus"></i> Add new category</a>
					</div>
            </div>
			
            <div class="box-body">
               <table id="example1" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>IN</th>
                                                <th>Name</th>
                                                <th>Enable</th>
												<th>Action</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
										 <?php 
											function list_categories($parent_id, $cats, $sub='') {

												foreach ($cats[$parent_id] as $cat):?>
												<tr>
												<td><?php echo  $cat->id; ?></td>
												<td><?php echo  $sub.$cat->name; ?></td>
												<td><?php echo ($cat->enabled == '1') ? 'Enabled' : 'Disabled'; ?></td>
												<td style="width:260px;">
												
												<a class="btn btn-info" href="<?php echo  site_url('admin/categories/form/'.$cat->id);?>"><i class="fa fa-pencil"></i> edit</a>

												<!--<a class="btn btn-success" href="<?php echo  site_url('admin/categories/organize/'.$cat->id);?>"><i class=" fa fa-arrows"></i> organize</a>-->

												<a class="btn btn-danger" href="<?php echo  site_url('admin/categories/delete/'.$cat->id);?>" onclick="return areyousure();"><i class="fa fa-trash-o"></i> delete</a>
												
												</td>
												</tr>
												<?php
												if (isset($cats[$cat->id]) && sizeof($cats[$cat->id]) > 0)
												{
												$sub2 = str_replace('&rarr;&nbsp;', '&nbsp;', $sub);
												$sub2 .=  '&nbsp;&nbsp;&nbsp;&rarr;&nbsp;';
												list_categories($cat->id, $cats, $sub2);
												}
												endforeach;
											}
											if(isset($categories[0]))
											{
												list_categories(0, $categories);
											}

											?>
                                            
											
                                        </tbody>
                                        
                                    </table>
                                

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          
        </div>
        <!-- /.col (right) -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
</div>
<!-- DataTables -->

<script>
  $(function () {
	
    $('#example11').DataTable({
      "paging": true,
      "lengthChange": true,
	  "pageLength": 100,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>
<?php
$this->load->view('admin/adminFooter');
?>
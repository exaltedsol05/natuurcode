<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>AdminLTE | Log in</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo HTTP_CSS_PATH_ADMIN; ?>AdminLTE.css" rel="stylesheet" type="text/css" />

			<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!--[if lt IE 9]>
			<script src="<?php echo HTTP_JS_PATH; ?>html5shiv.js"></script>
			<script src="<?php echo HTTP_JS_PATH; ?>respond.min.js"></script>
			<![endif]-->
    </head>
    <body class="bg-black">
		<div class="form-box" id="login-box">
			<div class="body bg-gray">
				<div class="form-group">
				<?php echo $msg;?>
				</div>
			</div>
		</div>
    </body>
</html>

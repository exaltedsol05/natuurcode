<?php $this->load->view('admin/adminHeader');?>
<?php $this->load->view('admin/adminLeftSidebar');?>
<style>
	.info{border: 1px solid #ccc;border-radius: 5px;padding: 0px;width:32%;margin-left:1%;}
	.info_heading{background-color: #3c8dbc;padding: 5px 10px; margin-top: 0px;color: #fff;}
	.info_p{padding-left: 10px;}
</style>
<style>
	/* status_in_DL */
	.status_in_DL{
		margin-left: auto; margin-right: auto; width: 95px; height: 14px; transition: opacity .2s linear;
		background: url(<?php echo base_url()."uploads/fedex_status.png";?>) -248px -22px;
	}
	
	/* Enroute to Airport Delay */
	.status_in_EA, .status_in_RS, .status_in_DY{
		margin-left: auto; margin-right: auto; width: 95px; height: 14px; transition: opacity .2s linear;
		background: url(<?php echo base_url()."uploads/fedex_status.png";?>) -198px -3px;
	}
	
	/* shipment exception */
	.status_in_HL, .status_in_SE, .status_in_DE, .status_in_CA{
		margin-left: auto; margin-right: auto; width: 95px; height: 14px; transition: opacity .2s linear;
		background: url(<?php echo base_url()."uploads/fedex_status.png";?>) -400px -22px;
	}
	
	/* first stage */
	.status_in_EP, .status_in_OC, .status_in_AF, .status_in_FD, .status_in_AP, .status_in_EP, .status_in_SF, .status_in_DR{
		margin-left: auto; margin-right: auto; width: 95px; height: 14px; transition: opacity .2s linear;
		background: url(<?php echo base_url()."uploads/fedex_status.png";?>) -146px -36px;
	}
	
	/* second stage */
	.status_in_AA, status_in_PU, .status_in_DP, .status_in_LO{
		margin-left:auto; margin-right:auto; width:91px; height:14px; transition:opacity .2s linear;
		background:url(<?php echo base_url()."uploads/fedex_status.png";?>) -46px -37px;
	}
	
	/* third stage */
	.status_in_AD, .status_in_IT, .status_in_EO, status_in_DS, .status_in_PL, .status_in_PF{
		margin-left:auto; margin-right:auto; width:91px; height:14px; transition:opacity .2s linear;
		background:url(<?php echo base_url()."uploads/fedex_status.png";?>) -46px -37px;
	}
	
	/* fourth stage */
	.status_in_TR, .status_in_ED, .status_in_OD{
		margin-left: auto; margin-right: auto; width: 95px; height: 14px; transition: opacity .2s linear;
		background: url(<?php echo base_url()."uploads/fedex_status.png";?>) -248px -22px;
	}
</style><?php
$total_weight = array();
foreach($order->contents as $orderkey=>$product){
	$total_weight[] = $product['weight'];
} 
$grant_weight = array_sum($total_weight);?>
<script type="text/javascript">
	$(document).ready(function(){
		$('#content_editor').redactor({
			minHeight: 200,
			imageUpload: '/admin/wysiwyg/upload_image',
			fileUpload: '/admin/wysiwyg/upload_file',
			imageGetJson: '/admin/wysiwyg/get_images',
			imageUploadErrorCallback: function(json){
				alert(json.error);
			},
			fileUploadErrorCallback: function(json){
				alert(json.error);
			}
		}); 
	});
	// store message content in JS to eliminate the need to do an ajax call with every selection

var messages = <?php
$messages   = array();
foreach($msg_templates as $msg){
	$messages[$msg['id']]= array('subject'=>$msg['subject'], 'content'=>$msg['content']);
}echo json_encode($messages);?>;

var customer_names = <?php 
    echo json_encode(array(
        $order->email=>$order->firstname.' '.$order->lastname,
        $order->ship_email=>$order->ship_firstname.' '.$order->ship_lastname,
        $order->bill_email=>$order->bill_firstname.' '.$order->bill_lastname
    ));
?>;

// use our customer names var to update the customer name in the template
function update_name(){
    if($('#canned_messages').val().length>0){
        set_canned_message($('#canned_messages').val());
    }
}

function set_canned_message(id){
    // update the customer name variable before setting content 
    $('#msg_subject').val(messages[id]['subject'].replace(/{customer_name}/g, customer_names[$('#recipient_name').val()]));
    $('#content_editor').redactor('insertHtml', messages[id]['content'].replace(/{customer_name}/g, customer_names[$('#recipient_name').val()]));
}   
</script>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			View Order
			<small>admin panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#"> Catalog</a></li>
			<li class="active"> View Order</li>
		</ol>
	</section>
	
	<?php $this->load->view('admin/adminError');?>
	
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header">
						<p class="col-md-4 pull-left">
							<a class="btn btn-primary" title="Send Email Notification" onclick="$('#notification_form').slideToggle();"><i class="icon-envelope"></i>Send Email Notification</a>
						</p>	
						<p class="pull-right col-md-8"><label>Please enter fedex track no.</label>&nbsp;&nbsp;
							<input type="hidden" name="fedex_order_no" value="<?php echo $order->id;?>" id="fedex_order_no"/>
							<input type="text" value="<?php echo $order->fedex_track_no;?>" name="fedex_track_no" id="fedex_track_no" class="form-control" style="display:inline;width:50%;padding-top:2px;" placeholder="Enter Fedex Track No."/>&nbsp;&nbsp; 
							<input type="button" class="btn btn-primary" value="Update" id="update_fedex_track"><?php
								if(!empty($order->fedex_track_no)){?>
									<a class="btn btn-primary" url="<?php echo base_url()."admin/fedex/track_shipment/".$order->fedex_track_no;?>" id="trackShipment">Track Shipment</a>
									<div class="row pull-right" id="fedex_response" style="margin-right:40px;"></div><?php
								}?>							
						</p>
						<!--<a class="btn btn-success" href="<?php echo site_url('admin/orders/packing_slip/'.$order->id);?>" target="_blank"><i class="icon-file"></i>Send Email Notification</a><!--

						<a class="btn btn-primary" href="<?php echo base_url()."admin/fedex/create_shipping/".$order->id;?>"><i class="icon-envelope"></i>Create Shipment</a>-->
					</div>
					<div class="box-body">
									<div id="notification_form" class="row" style="display:none;">

										<div class="col-sm-12">

											<?php echo form_open('admin/orders/send_notification/'.$order->id);?>

												<fieldset>

													<label>Message Templates</label>

													<select id="canned_messages" onchange="set_canned_message(this.value)" class="form-control">

														<option>- Select Canned Message -</option>

														<?php foreach($msg_templates as $msg)

														{

															echo '<option value="'.$msg['id'].'">'.$msg['name'].'</option>';

														}

														?>

													</select>



													<label>Recipient</label>

													<select name="recipient" onchange="update_name()" id="recipient_name" class='form-control'>

														<?php 

															if(!empty($order->email))

															{

																echo '<option value="'.$order->email.'">Account Main Email ('.$order->email.')';

															}

															if(!empty($order->ship_email))

															{

																echo '<option value="'.$order->ship_email.'">Shipping Email ('.$order->ship_email.')';

															}

															if($order->bill_email != $order->ship_email)

															{

																echo '<option value="'.$order->bill_email.'">Billing Email ('.$order->bill_email.')';

															}

														?>

													</select>



													<label>Subject</label>

													<input type="text" name="subject" size="40" id="msg_subject" class="form-control"/>



													<label>Message</label>

													<textarea id="content_editor" name="content"></textarea>



													<div class="form-actions">

														<input type="submit" class="btn btn-primary" value="Send Message" />

													</div>

												</fieldset>

											</form>

										</div>

									</div>



									<div class="row" style="margin-top:10px;">

										<div class="col-sm-4 info">

											<h3 class="info_heading">Account Info</h3>

											<p class="info_p">

											Company : <?php echo (!empty($order->company))?$order->company.'<br>':'';?>

											User : <?php echo ucfirst($order->firstname);?> <?php echo ucfirst($order->lastname);?> <br/>

											Email : <?php echo $order->email;?> <br/>

											Contact No. : <?php echo $order->phone;?><br/><br/>

											</p>

										</div>

										<div class="col-sm-4 info">

											<h3 class="info_heading">Billing Address</h3>

											<p class="info_p">

												<?php echo (!empty($order->bill_company))?$order->bill_company.'<br/>':'';?>

												<?php echo ucfirst($order->bill_firstname).' '.ucfirst($order->bill_lastname);?> <br/>

												<?php echo $order->bill_address1;?> <?php echo (!empty($order->bill_address2))?$order->bill_address2.'<br/>':'';?>

												<?php echo $order->bill_city.', '.$order->bill_zone.' '.$order->bill_zip;?>, <?php echo $order->bill_country;?><br/>			

												Email : <?php echo $order->bill_email;?><br/>

												Contact No. : <?php echo $order->bill_phone;?>

											</p>

										</div>

										<div class="col-sm-4 info">

											<h3 class="info_heading">Shipping Address</h3>

											<p class="info_p">

												<?php echo (!empty($order->ship_company))?$order->ship_company.'<br/>':'';?>

												<?php echo ucfirst($order->ship_firstname).' '.ucfirst($order->ship_lastname);?> <br/>

												<?php echo $order->ship_address1;?>, <?php echo (!empty($order->ship_address2))?$order->ship_address2.'<br/>':'';?>

												<?php echo $order->ship_city.', '.$order->ship_zone.' '.$order->ship_zip;?>, <?php echo $order->ship_country;?><br/>			

												Email : <?php echo $order->ship_email;?><br/>

												Contact No. : <?php echo $order->ship_phone;?>

											</p>

										</div>

									</div>



									<div class="row" style="margin-top:20px;">

										<div class="col-sm-4 info">

											<h3 class="info_heading">Order Details</h3>

											<p class="info_p">

												Order No. : <?php

													echo $order -> order_number;



												if(!empty($order->referral)):?>

													<strong>Referral: </strong><?php echo $order->referral;?><br/>

												<?php endif;?>

												<?php if(!empty($order->is_gift)):?>

													<strong>This is a gift.</strong>

												<?php endif;?>

												

												<?php if(!empty($order->gift_message)):?>

													<strong>Gift Note</strong><br/>

													<?php echo $order->gift_message;?>

												<?php endif;?>

											</p>

										</div>

										<div class="col-sm-4 info">
											<h3 class="info_heading">Payment Method</h3>
											<p class="info_p"><?php echo $order->payment_info; ?></p>
										</div>
										<div class="col-sm-4 info">
											<h3 class="info_heading">Shipping Details</h3>
											<p class="info_p">
												<?php if(empty($order->shipping_method)){
													echo "Calculated Shipping Charges";
												}else{
													echo $order->shipping_method;
												} ?>
												<?php if(!empty($order->shipping_notes)):?><div style="margin-top:10px;"><?php echo $order->shipping_notes;?></div><?php endif;?>
											</p>
										</div>
									</div>

									<?php echo form_open('admin/orders/order/'.$order->id, 'class="form-inline"');?>

									<fieldset>
										<div class="row" style="margin-top:20px;">
											<div class="col-md-8 info" style="width:98%;">
												<h3 class="info_heading">Admin Notes</h3>
												<p class="info_p" style="float:left;width:80%;">
													<textarea name="notes" class="form-control" rows="6" cols="135" style="border-radius:5px;" placeholder="Please, Enter notes" id="admin_txt"></textarea>
													<input type="hidden" name="toemail"  value="<?php echo $order->email; ?>" />
													<input type="hidden" name="order_no" value="<?php echo $order->order_number; ?>" />
													<input type="hidden" name="name" value="<?php echo ucfirst($order->bill_firstname)." ".ucfirst($order->bill_lastname); ?>" />
													<input type="hidden" name="phone" value="<?php echo $order->bill_phone; ?>" /><?php
													$etotal = $order->total;
													if($order->reward_amt>0){
														$etotal = $etotal - ($order->reward_amt/100);
													}
													if($order->shipping > 0){
														$etotal = $etotal + $order->shipping;
													} ?>
													<input type="hidden" name="euser_id" value="<?php echo $order->customer_id; ?>" />
													<input type="hidden" name="eecash" value="<?php echo $etotal; ?>" />
												</p>
												<p class="info_p" style="float:right;width:20%;">	
													Status : <?php echo form_dropdown('status', json_decode($this->config->item('order_statuses')), $order->status, 'class="form-control"');?><br/><br/>
													Send Mail to User : <input type="checkbox" name="mailtouser" /> <br/><br/>
													<input type="button" class="btn btn-primary" value="Update Order" id="admin_notes"/>
												</p>
												<div  class="col-md-12"><hr/>
													<h3 style="text-align:center;">Order Status History</h3>
													<table class="table table-striped">

														<thead>

															<tr>

																<th style="width:10%;">Serial No.</th>

																<th style="width:60%;">Notes</th>

																<th style="width:10%;">Status</th>

																<th style="width:20%;">Date</th>

															</tr>

														</thead>

														<tbody><?php

															$count = 1;
															if(count($order_history) > 0){				
																foreach($order_history as $history){?>

																	<tr>

																		<td>

																			<?php echo $count; ?>.

																		</td>

																		<td>

																			<?php echo $history -> notes; ?>

																		</td>

																		<td>

																			<?php echo $history -> status; ?>

																		</td>

																		<td>

																			<?php echo $history -> date; ?>

																		</td>

																	</tr><?php

																	$count = $count+1;	

																}

															}else{?>

																<tr>

																	<td colspan="4">

																		There is no update for this record.

																	</td>

																</tr><?php							

															}?>

															

														</tbody>	

													</table>

												</div>

											</div>

										</div>

									</fieldset>

									</form><hr/>





									<table class="table table-striped">

										<thead>

											<tr>

												<th>NAME</th>

												<th>Description</th>

												<th>Price</th>

												<th>Quantity</th>

												<th>Total</th>

											</tr>

										</thead>

										<tbody>

											<?php
											if($order->order_currency=='1'){
												$symbol = 'fa fa-rupee';
											}else if($order->order_currency=='2'){
												$symbol = 'fa fa-usd';
											}else if($order->order_currency=='3'){
												$symbol = 'fa fa-eur';
											}else if($order->order_currency=='4'){
												$symbol = 'fa fa-gbp';
											}else if($order->order_currency=='5'){
												$symbol = 'fa fa-usd';
											}else if($order->order_currency=='6'){
												$symbol = 'aed_gcc_image';
											} 
											foreach($order->contents as $orderkey=>$product):
											?>

											<tr>

												<td>

													<?php echo $product['name'];?>

													<?php echo (trim($product['sku']) != '')?'<br/><small>SKU: '.$product['sku'].'</small>':'';?>

													

												</td>

												<td>

													<?php //echo $product['excerpt'];?>

													<?php

													

													// Print options

													if(isset($product['options']))

													{

														foreach($product['options'] as $name=>$value)

														{

															$name = explode('-', $name);

															$name = trim($name[0]);

															if(is_array($value))

															{

																echo '<div>'.$name.':<br/>';

																foreach($value as $item)

																{

																	echo '- '.$item.'<br/>';

																}   

																echo "</div>";

															}

															else

															{

																echo '<div>'.$name.': '.$value.'</div>';

															}

														}

													}

													

													if(isset($product['gc_status'])) echo $product['gc_status']; ?>

												</td>

												<td><span class="<?php echo $symbol;?>"></span> <?php echo $product['product_currency_price'];?></td>

												<td><?php echo $product['quantity'];?></td>

												<td><span class="<?php echo $symbol;?>"></span> <?php $rowtotal =  $product['product_currency_price']*$product['quantity'];
													echo sprintf("%.2f", $rowtotal);
												?></td>

											</tr>

											<?php endforeach;?>
											</tbody>
											<tfoot>

											<?php if($order->coupon_discount > 0):?>
												<tr>
													<td><strong>Coupon Discount</strong></td>
													<td colspan="3"></td>
													<td><?php echo format_currency(0-$order->coupon_discount); ?></td>
												</tr>
											<?php endif;?>

											<tr>
												<td><strong>Subtotal</strong></td>
												<td colspan="3"></td>
												<td><span class="<?php echo $symbol;?>"></span> <?php echo $order->subtotal; ?></td>
											</tr><?php 
											
											$gtotal = $order->subtotal;
											
											$charges = @$order->custom_charges;
											if(!empty($charges)){
												foreach($charges as $name=>$price) : ?>
													<tr>
														<td><strong><?php echo $name?></strong></td>
														<td colspan="3"></td>
														<td><?php echo format_currency($price); ?></td>
													</tr><?php 
												endforeach;
											}?>

											<tr>
												<td><strong>Shipping</strong></td>
												<td colspan="3"><?php 
													if(empty($order->shipping_method)){
														echo "Calculated Shipping Charges";
													}else{
														echo $order->shipping_method;
													} ?></td>
												<td><span class="<?php echo $symbol;?>"></span> <?php echo $order->shipping; 
													$gtotal = $gtotal + $order->shipping;?>
												</td>
											</tr>
											
											<?php if($order->tax > 0):?>
											<tr>
												<td><strong>Tax</strong></td>
												<td colspan="3"></td>
												<td><?php echo $order->tax; 
													//$gtotal = $gtotal+ $order->tax; ;?></td>
											</tr>
											<?php endif;?>
											
											<!--<?php if($order->gift_card_discount > 0):?>
											<tr>
												<td><strong><?php echo lang('giftcard_discount');?></strong></td>
												<td colspan="3"></td>
												<td><?php echo format_currency(0-$order->gift_card_discount); ?></td>
											</tr>
											<?php endif;?>-->
											
											<?php if($order->reward_amt > 0):?>
											<tr>
												<td><strong>Withdraw eCash</strong></td>
												<td colspan="3"></td>
												<td>-<?php 
													echo sprintf("%.2f", $order->reward_amt/100);
													$gtotal = $gtotal - ($order->reward_amt/100); ?>
												</td>
												<td>
											</tr>
											<?php endif;?>
											<tr>
												<td><h3>Total</h3></td>
												<td colspan="3"></td>
												<td><strong><span class="<?php echo $symbol;?>"></span> <?php echo sprintf("%.2f", $gtotal); ?></strong></td>
											</tr>
										</tfoot>

									</table>
				
							    </div>
								<div class="box-footer">
									 
							    </div>
							     
							</div>
						</div>
					</div>
				
				
				
				</section><!-- /.content -->
</div><!-- /.right-side -->
<script>
$(document).ready(function(){
	$("#admin_notes").click(function(){
		var admin_text = $.trim($('#admin_txt').val());
		if(admin_text == ''){
			alert("Please, enter text first.");
			admin_text = $.trim($('#admin_txt').val(""));
			$('#admin_txt').focus();
			return false;
		}else{
			$(".form-inline").submit();
		}
	})
	
	$("#update_fedex_track").click(function(){
		var fedex_track = $.trim($('#fedex_track_no').val());
		if(fedex_track == ''){
			alert("Please, Enter Fedex Track No.");
			admin_text = $('#fedex_track_no').val("");
			$('#fedex_track_no').focus();
			return false;
		}else{
			fedex_order_no = $("#fedex_order_no").val();
			$.ajax({
				type: "POST",  					
				url: "<?php echo base_url();?>admin/orders/update_fedex_track",
				data: {track_no:fedex_track, order_id:fedex_order_no},
				beforeSend:function(){},
				success: function(msg){
					if(msg)
						alert("Track Number Updated Successfully.");
					else
						alert("Track Number Could Not Update, Please Try Again.");
				}
			});
		}
	});
	
	$("#trackShipment").click(function(){
		url = $("#trackShipment").attr('url');
		$.ajax({
			type: "POST",  					
			url: url,
			//data: {id:id, status:status},
			beforeSend:function() {
				$("#fedex_response").html("<strong>Wait for a moment...</strong>");
			},
			success: function(msg){
				$("#fedex_response").html(msg);
			}
		});	
	})
})
</script>
<?php $this->load->view('admin/adminFooter');?>
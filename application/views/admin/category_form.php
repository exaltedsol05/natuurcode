<?php $this->load->view('admin/adminHeader');?>
<?php $this->load->view('admin/adminLeftSidebar');?>
            <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Category Form
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Category Form</li>
                    </ol>
                </section>
				<?php $this->load->view('admin/adminError');?>
					
                <!-- Main content -->
                <section class="content">

                   
                    <!-- Main row -->
                    <div class="row">
                        <!-- Left col -->
                        <section class="col-sm-12">
                            <!-- Custom tabs (Charts with tabs)-->
							<?php echo form_open_multipart('admin/categories/form/'.$id); ?>
                            <div class="nav-tabs-custom">
							     
                                <!-- Tabs within a box -->                              
										

									
										<ul class="nav nav-tabs">
											<li class="active"><a href="#description_tab" data-toggle="tab">Description</a></li>
											<li><a href="#attributes_tab" data-toggle="tab">Attributes</a></li>
											<li><a href="#seo_tab" data-toggle="tab">SEO</a></li>
										</ul>
											<div class="tab-content">										
												<div class="tab-pane fade in active" id="description_tab">
													<fieldset>
														<label for="cat_name">Name</label>
														<?php
														$data	= array('name'=>'name', 'value'=>set_value('name', $name), 'class'=>'form-control');
														echo form_input($data);
														?>				
														<label for="cat_description">Description</label>
														<?php
														$data	= array('name'=>'description', 'class'=>'form-control', 'value'=>set_value('description', $description));
														echo form_textarea($data);
														?>
														<label for="cat_enabled">Enabled </label>
														<?php echo form_dropdown('enabled', array('0' => 'Disabled', '1' =>'Enabled'), set_value('enabled',$enabled),'class=form-control'); ?>	
													</fieldset>
												</div>

												<div class="tab-pane fade" id="attributes_tab">
													<fieldset>
														<label for="cat_slug">Slug </label>
														<?php
														$data	= array('name'=>'slug', 'class'=>'form-control', 'value'=>set_value('slug', $slug));
														echo form_input($data);
														?>				
														<label for="cat_sequence">Sequence </label>
														<?php
															$data	= array('name'=>'sequence','class'=>'form-control',  'value'=>set_value('sequence', $sequence));
															echo form_input($data);
														?>				
														<label for="parent_id">Parent </label>
														<?php
														$data	= array(0 => 'Top Level Category');
														foreach($categories as $parent)
														{
															if($parent->id != $id)
															{
															$data[$parent->id] = $parent->name;
															}
														}
														echo form_dropdown('parent_id', $data, $parent_id,'class=form-control');
														?>				
														<label for="excerpt">Excerpt </label>
														<?php
														$data	= array('name'=>'excerpt', 'value'=>set_value('excerpt', $excerpt), 'class'=>'form-control', 'rows'=>3);
														echo form_textarea($data);
														?>
														<label for="image">Banner Image(1920*1080) </label>
														<div class="input-append">
														<?php echo form_upload(array('name'=>'image'));?><span class="add-on"> Max File Size <?php echo  $this->config->item('size_limit')/1024; ?>kb</span>
														</div>
														<?php if($id && $image != ''):?>

														<div style="text-align:center; padding:5px; border:1px solid #ddd;"><img src="<?php echo base_url('uploads/images/small/'.$image);?>" alt="current"/><br/> Current File </div>

														<?php endif;?>
														
														
														<label for="image">Home Image(400*400) </label>
														<div class="input-append">
														<?php echo form_upload(array('name'=>'image1'));?><span class="add-on"> Max File Size <?php echo  $this->config->item('size_limit')/1024; ?>kb</span>
														</div>
														<?php if($id && $image1 != ''):?>

														<div style="text-align:center; padding:5px; border:1px solid #ddd;"><img src="<?php echo base_url('uploads/images/small/'.$image1);?>" alt="current"/><br/> Current File </div>

														<?php endif;?>
													</fieldset>
												</div>

												<div class="tab-pane fade" id="seo_tab">
													<fieldset>
														<label for="seo_title">SEO Title </label>
														<?php
														$data	= array('name'=>'seo_title', 'value'=>set_value('seo_title', $seo_title), 'class'=>'form-control');
														echo form_input($data);
														?>				
														<label>Meta Data</label> 
														<?php
														$data	= array('rows'=>3, 'name'=>'meta', 'value'=>set_value('meta', html_entity_decode($meta)), 'class'=>'form-control');
														echo form_textarea($data);
														?>
														<p class="help-block">ex. &lt;meta name="description" content="We sell products that help you" /&gt;</p>
													</fieldset>
												</div>
	
											</div>

									
									<div class="pull-left">
									<button type="submit" class="btn btn-primary" style="margin:10px 0px 10px 10px;">Save</button>
									</div>
									<div class="pull-right">

										<a class="btn btn-primary"  style="margin:10px 0px 10px 10px;" href="<?php echo site_url('admin/categories/'); ?>"><i class="fa fa-eye"></i> view category</a>
								   </div>
								
                            </div><!-- /.nav-tabs-custom -->
                            </form>							
                        </section><!-- /.Left col -->
                       
					</div><!-- /.row (main row) -->

                </section><!-- /.content -->
   </div>        
    
<?php
$this->load->view('admin/adminFooter');
?>
 <script type="text/javascript">
$('form').submit(function() {
	$('.btn').attr('disabled', true).addClass('disabled');
});
</script>  
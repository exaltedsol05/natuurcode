<?php $this->load->view('admin/adminHeader');?>
<?php $this->load->view('admin/adminLeftSidebar');?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
			<?=$page_title;?>
			<small>Control panel</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active"><?=$page_title;?></li>
			</ol>
		</section>
		<?php $this->load->view('admin/adminError');?>
		<section class="content">		       
		       <div class="row">			       
			       <div class="col-sm-12">
						 <?php echo form_open_multipart('admin/banners/banner_collection_form/'.$banner_collection_id); ?>
							<div class="box box-primary">						
								<div class="box-header">
								</div>
								<div class="box-body">
								  <?php $name = array('name'=>'name', 'value' => set_value('name', $name),'class'=>'form-control');
								   ?>
									<div class="form-group">
								   
										<label for="title">Name</label>
										<?php echo form_input($name); ?>
									</div>
								</div>
								<div class="box-footer">
									<input class="btn btn-primary" type="submit" value="Save"/>
								</div>												
							</div>
						</form>
				    </div>
			</div>
		</section>
</div>
<?php
$this->load->view('admin/adminFooter');
?>
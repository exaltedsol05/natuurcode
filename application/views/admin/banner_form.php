<?php $this->load->view('admin/adminHeader');?>
<?php $this->load->view('admin/adminLeftSidebar');?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
            <h1>
			<?php echo $page_title;?>
			<small>Control panel</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active"><?php echo $page_title;?></li>
			</ol>
		</section>
		<?php $this->load->view('admin/adminError');?>
		<section class="content">
		      <div class="row">
			      
			       <div class="col-sm-12">
				   <?php echo form_open_multipart('admin/banners/banner_form/'.$banner_collection_id.'/'.$banner_id); ?>
					<div class="box box-primary">						
						<div class="box-header">
						</div>
						<div class="box-body">
						        <?php
							$name			= array('name'=>'name', 'value' => set_value('name', $name),'class'=>'form-control');
							$enable_date	= array('name'=>'enable_date', 'id'=>'enable_date', 'value'=>set_value('enable_on', set_value('enable_date', $enable_date)),'class'=>'form-control');
							$disable_date	= array('name'=>'disable_date', 'id'=>'disable_date', 'value'=>set_value('disable_on', set_value('disable_date', $disable_date)),'class'=>'form-control');
							$f_image		= array('name'=>'image', 'id'=>'image');
							$link			= array('name'=>'link', 'value' => set_value('link', $link),'class'=>'form-control');	
							$new_window		= array('name'=>'new_window', 'value'=>1, 'checked'=>set_checkbox('new_window', 1, $new_window));
						?>

					
					<div class="form-group">
						<label for="name">Name </label>
						<?php echo form_input($name); ?>
					</div>
					<div class="form-group">
						<label for="link">Link </label>
						<?php echo form_input($link); ?>
					</div>
					<div class="form-group">
						<label for="enable_date">Enable Date </label>
						<?php echo form_input($enable_date); ?>
					</div>
					<div class="form-group">
						<label for="disable_date">Disable Date </label>
						<?php echo form_input($disable_date); ?>
					</div>
					<div class="form-group">
						<label class="checkbox">
							<?php echo form_checkbox($new_window); ?> <?php echo 'New Window';?>
						</label>
					</div>
					<div class="form-group">
						<label for="image">Image </label>
						<?php echo form_upload($f_image); ?>
					</div>

					<?php if($banner_id && $image != ''):?>
					<div style="text-align:center; padding:5px; border:1px solid #ccc;"><img src="<?php echo base_url('uploads/'.$image);?>" alt="current"/><br/>Current File</div>
					<?php endif;?>

					

					
						</div>
						<div class="box-footer">
						      <input class="btn btn-primary" type="submit" value="Save"/>
						</div>
					</div>
					</form>
				</div>
					
              </div>					
		</section>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$("#enable_date").datepicker({ dateFormat: 'mm-dd-yy'});
		$("#disable_date").datepicker({ dateFormat: 'mm-dd-yy'});
	});
	
	$('form').submit(function() {
		$('.btn').attr('disabled', true).addClass('disabled');
	});
</script>
<?php $this->load->view('admin/adminFooter');?>
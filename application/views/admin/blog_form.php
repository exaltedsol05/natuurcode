<?php $this->load->view('admin/adminHeader');?>
<?php $this->load->view('admin/adminLeftSidebar');?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php if(!empty($id)){echo "Edit";}else{echo "Add";}?> Blog  
			<small>admin panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#"> Catalog</a></li>
			<li class="active"> <?php if(!empty($id)){echo "Edit";}else{echo "Add";}?> Blog  </li>
		</ol>
	</section>
	<?php $this->load->view('admin/adminError');?>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header"></div>
					<div class="box-body">							         
						<section class="content">
							<!-- Main row -->
							<div class="row">
							<section class="col-sm-12">
									
									
									<!-- Custom tabs (Charts with tabs)--><?php 
									$attributes = array("name" => "blog_form", "id" => "blog_form");
									echo form_open_multipart("admin/blogs/form/".$id, $attributes); ?>
									
																	
										<fieldset>
											<div class="row"><br/>
												<div class="form-group">
													<label for="test_content">Blog Banner Image</label>
													<?php if($blog_image != ""){?>
													<img src="<?php echo base_url().'uploads/blog/'.$blog_image; ?>" class="img-square img-responsive" style="">	
													<?php }?><br>
													<input id="blog_image" name="blog_image" type="file" class='form-control '>
													<p>Image size:1140*255</p>
												</div>
											</div>
											<div class="row"><br/>
												<div class="form-group">
													<label for="test_content">Blog Thumb Image</label>
													<?php if($blog_image != ""){?>
													<img src="<?php echo base_url().'uploads/blog/'.$blog_image; ?>" class="img-square img-responsive" style="">	
													<?php }?><br>
													<input id="blog_thumb_image" name="blog_thumb_image" type="file" class='form-control '>
													<p>Image size:540*255</p>
												</div>
											</div>
											<div class="row"><br/>
												<div class="form-group">
													<label for="email"><strong>Blog Title</strong></label><?php
													$data	= array('name'=>'title', 'value'=>set_value('title', $title), 'class'=>'form-control');
													echo form_input($data); ?>
													</div>
											
											</div>
											<div class="row"><br/>
												<div class="form-group">
													<label for="email"><strong>Product Ids</strong></label><?php
													$data	= array('name'=>'product_id', 'value'=>set_value('product_id', $product_id), 'class'=>'form-control');
													echo form_input($data); ?>
													</div>
											<p class="help-text">Example:1,2,3</p>
											</div>
											<input type="hidden" name="status" value="<?php echo set_value('status', $status); ?>">
											<div class="row">
												<div class="form-group">
													<label for="blog_cat"><strong>Category</strong></label>
													<select class="form-control" style="min-width:100px;" name="blog_cat" id="blog_cat">
														<option value="" >Select Blog Category</option>
														<option value="1" <?php if($blog_cat == "1") { echo "selected = 'selected'"; } ?>>Natuur Knowledge</option>
														<option value="2" <?php if($blog_cat == "2") { echo "selected = 'selected'"; } ?>>Daily Wisdom</option>
														<option value="3" <?php if($blog_cat == "3") { echo "selected = 'selected'"; } ?>>DIY Workshop</option>
														<option value="4" <?php if($blog_cat == "4") { echo "selected = 'selected'"; } ?>>Chemicals in Daily Life</option>
														<option value="5" <?php if($blog_cat == "5") { echo "selected = 'selected'"; } ?>>Health & Wellness</option>
														<option value="6" <?php if($blog_cat == "6") { echo "selected = 'selected'"; } ?>>Beauty</option>
													</select>
												</div>	
											</div>
											<div class="row">
												<div class="form-group">
													<input type="checkbox" name="is_popular_blog" value="1" <?php echo($is_popular_blog == "1")?'checked="checked"':'';?>/> Popular Articles&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="checkbox" name="is_new" value="1" <?php echo($is_new == "1")?'checked="checked"':'';?>/> What's New
												</div>
											</div>	
											<div class="row">
												<div class="form-group">
													<label for="test_content"><strong>Blog Content</strong></label><?php 
													$data	= array('name'=>'content', 'class'=>'form-control redactor', 'value'=>set_value('content', $content));
													echo form_textarea($data); ?>
											</div>	
											<div class="row">
												<div class="form-group pull-left">
													<button type="submit" class="btn btn-primary" style="margin:10px 0px 10px 10px;">Save</button>
												</div>
											</div>	
										</fieldset><?php
									echo form_close();?>
								</section><!-- /.Left col -->
							</div><!-- /.row (main row) -->
						</section><!-- /.content -->
					</div>
					<div class="box-footer"></div>				 
				</div>
			</div>
		</div>
	</section><!-- /.content -->
</div><!-- /.right-side -->

<script type="text/javascript">
$(document).ready(function(){
    $('.redactor').redactor({
		//plugins: ['fontcolor'],
		buttons: [
			'bold',
			'italic',
			'underline',
			'formatting',
			'image',
			'link',
			'unorderedlist',
			'orderedlist',
			'alignment',
			'|',
			'html'
		],
		minHeight: 200,
		imageUpload: '<?php echo base_url(); ?>admin/wysiwyg/upload_image',
		fileUpload: '<?php echo base_url(); ?>admin/wysiwyg/upload_file',
		imageGetJson: '<?php echo base_url(); ?>admin/wysiwyg/get_images',
		imageUploadErrorCallback: function(json)
		{
			alert(json.error);
		},
		fileUploadErrorCallback: function(json)
		{
			alert(json.error);
		}
	});
});

$('form').submit(function() {
	$('.btn').attr('disabled', true).addClass('disabled');
});
</script>
<?php $this->load->view('admin/adminFooter');?>
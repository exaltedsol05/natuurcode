<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>

<link href="<?php echo HTTP_CSS_PATH_ADMIN; ?>bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>jquery.min.js"></script>
<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>jquery-ui.min.js" type="text/javascript"></script>

<style type="text/css">

/* fix some bootstrap problems */
.btn-mini [class^="icon-"] {
	margin-top: -1px;
}
.navbar-form .input-append .btn {
	margin-top:0px;
	padding-left:2px;
	padding-right:5px;
}

</style>

<body>
	
	<div class="container">
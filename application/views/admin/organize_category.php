<?php
$this->load->view('admin/adminHeader');
?>
<?php
$this->load->view('admin/adminLeftSidebar');
?>

<script type="text/javascript">
//<![CDATA[
$(document).ready(function(){
	create_sortable();
});

// Return a helper with preserved width of cells
var fixHelper = function(e, ui) {
	ui.children().each(function() {
		$(this).width($(this).width());
	});
	return ui;
};

function create_sortable()
{
	$('#category_contents').sortable({
		scroll: true,
		helper: fixHelper,
		axis: 'y',
		update: function(){
			save_sortable();
		}
	});	
	$('#category_contents').sortable('enable');
}

function save_sortable()
{
	serial=$('#category_contents').sortable('serialize');
			
	$.ajax({
		url:'<?php echo site_url('admin/categories/process_organization/'.$category->id);?>',
		type:'POST',
		data:serial
	});
}
//]]>
</script>

	<aside class="right-side">
	<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
			<?php echo $page_title;?>
			<small>Control panel</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active"><?php echo $page_title;?></li>
			</ol>
		</section>
		<?php $this->load->view('admin/adminError');?>
		<section class="content">
		       
		       <div class="row">
			        
			       <section class="col-sm-12">
				      <div class="row">
					       <div class="col-sm-12">
										   <div class="box-body no-padding">
												<table class="table table-striped">
													<thead>
														<tr>
															<th>Sku</th>
															<th>Name</th>
															<th>Price</th>
															<th>Sale</th>
														</tr>
													</thead>
													<tbody id="category_contents">
												<?php foreach ($category_products as $product){ ?>
														<tr id="product-<?php echo $product->id;?>">
															<td><?php echo $product->sku;?></td>
															<td><?php echo $product->name;?></td>
															<td><?php echo $product->price;?></td>
															<td><?php echo $product->saleprice;?></td>
														</tr>
												<?php } ?>
													</tbody>
												</table>
										   </div>
                        </div>
						</div>
 
				   </section>
				</div>
		</section>
	</aside>
<?php
$this->load->view('admin/adminFooter');
?>
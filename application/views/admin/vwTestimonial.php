<?php $this->load->view('admin/adminHeader');?>
<?php $this->load->view('admin/adminLeftSidebar');?>
<script type="text/javascript">
function areyousure(){	
	return confirm('Are you sure to delete this type?');
}
</script>
<div class="content-wrapper">                
<!-- Content Header (Page header) -->                
<section class="content-header">                  
  <h1>                        Testimonials                        <small>admin panel</small>                    </h1>                   
  <ol class="breadcrumb">                      
  <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> 
  <li><a href="#"> Catalog</a></li>                        
  <li class="active"> Testimonials</li>                  
  </ol>               
  </section>                
  <?php $this->load->view('admin/adminError');?>    
  <!-- Main content -->				<section class="content">
  <div class="row">					
  <div class="col-md-12">							
  <div class="box box-primary">						
  <div class="box-header">								
  <h3 class="box-title" style="width: 100%;">All Testimonials <a class="btn btn-primary pull-right" href="<?php echo base_url('admin/testimonials/form').$id; ?>" style="margin-right:10px;color: #FFF;"><i class="fa fa-plus"></i> Add Testimonial</a></h3>							    							   
  </div>								
  <div class="box-body">					
  <table id="example1" class="table table-bordered table-striped">
  <thead>                                        
  <tr>                                            
  <th>S.No.</th>                                   
  <th>Email Id</th>                                
  <th>Name</th>                                    
  <th>Image</th>                                       
  <th style="width: 80%;">Content</th>                  
  <th>status</th>                                       
  <th style="width: 80%;"> Action</th>
  </tr>                                        
  </thead>                                        
  <tbody>										
  <?php 											
  $i=0;
  
foreach ($arrTestimonial as $cat){ ?>			
									<tr>		
									<td><?php echo  ++$i; ?></td>
									<td><?php echo  $cat->email_id; ?></td>
									<td><?php echo  ucwords($cat->name); ?></td>
									<td><img src="<?php echo base_url().'uploads/testimonial/small/'.$cat->image; ?>" style="width:50px;height:50px;"></td>
									<td><?php echo  $cat->content; ?></td>
									<td>                                     
									<select onchange="change_status(<?php echo $cat->id; ?>,this.value)">
									<option value="1" <?php if($cat->status == "1") { echo "selected = 'selected'"; } ?>>Active</option>
									<option value="0" <?php if($cat->status == "0") { echo "selected = 'selected'"; } ?>>Inactive</option>
									</select>                                                
									<br/><span id="st<?php echo $cat->id; ?>" style="color:green"> </span>                                          
									</td>                                                    <td>													
									<a class="btn btn-info" href="<?php echo  site_url('admin/testimonials/form/'.$cat->id);?>"><i class="fa fa-pencil"></i> edit</a>										
									<a href="javascript:void(0);" class="btn btn-danger"  onclick="delete_testimonial('<?php echo $cat->id;?>');"><i class="fa fa-trash-o"></i> delete</a>													
									</td>											
									</tr>										   
									<?php } ?>
									</tbody>                
									</table>			
								    </div>			
									<div class="box-footer">
									</div>
									</div>				
									</div>					</div>
									</section><!-- /.content --></div><!-- /.right-side -->
									<script type="text/javascript"> 	
									function change_status(id,status) {	
									if(confirm("Do you want to change status?"))   
										{         
									$.ajax({           
									type: "POST",            
									url: "<?php echo base_url();?>
									admin/testimonials/change_testimonial_status",               data:{id:id,status:status},       
									success: function(msg){         
									if(msg==1)                
										{                    
									$('#st'+id).html("Status changed.");      
									}                                }        
									});     
									}
									}
									function delete_testimonial(id) {      
									if(confirm("Warning : This will permanently delete this Testimonial and  all the related records, once deleted those records can't be restored!!!\n\nSo, are you absolutely sure about deleting this Testimonial?"))  
										
										{         
										
									window.location='<?php echo base_url(); ?>admin/testimonials/delete_testimonial/'+id; }   
									} 
									</script>
									<?php $this->load->view('admin/adminFooter');?>
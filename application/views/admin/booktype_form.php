<?php
$this->load->view('admin/adminHeader');
?>
<?php
$this->load->view('admin/adminLeftSidebar');
?>
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Book Type Form
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Book Type Form</li>
                    </ol>
                </section>
				<?php $this->load->view('admin/adminError');?>
					
                <!-- Main content -->
                <section class="content">

                   
                    <!-- Main row -->
                    <div class="row">
                        <!-- Left col -->
                        <section class="col-sm-12">
                            <!-- Custom tabs (Charts with tabs)-->
							<?php echo form_open_multipart('admin/booktypes/form/'.$id); ?>
                            <div class="nav-tabs-custom">
							     
                                <!-- Tabs within a box -->
									
											<div class="tab-content">
												<div class="tab-pane fade in active" id="description_tab">
													<fieldset>
														<label for="cat_name">Name</label>
														<?php
														$data	= array('name'=>'name', 'value'=>set_value('name', $name), 'class'=>'form-control');
														echo form_input($data);
														?>
														<label for="cat_enabled">Enabled </label>
														<?php echo form_dropdown('enabled', array('0' => 'Disabled', '1' =>'Enabled'), set_value('enabled',$enabled),'class=form-control'); ?>	
													</fieldset>
												</div>

											</div>

									
									<div class="pull-left">
									<button type="submit" class="btn btn-primary" style="margin:10px 0px 10px 10px;">Save</button>
									</div>
									<div class="pull-right">

										<a class="btn btn-primary"  style="margin:10px 0px 10px 10px;" href="<?php echo site_url('admin/booktypes/'); ?>"><i class="fa fa-eye"></i> view Book Type</a>
								   </div>
								
                            </div><!-- /.nav-tabs-custom -->
                            </form>							
                        </section><!-- /.Left col -->
                       
					</div><!-- /.row (main row) -->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
    
<?php
$this->load->view('admin/adminFooter');
?>
 <script type="text/javascript">
$('form').submit(function() {
	$('.btn').attr('disabled', true).addClass('disabled');
});
</script>  
<?php $this->load->view('admin/adminHeader');?>
    <?php $this->load->view('admin/adminLeftSidebar');?>
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>                        Orders                        <small>admin panel</small>                    </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="#"> Catalog</a></li>
                    <li class="active"> Orders</li>
                </ol>
            </section>
            <?php $this->load->view('admin/adminError');?>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-header">
                                    <?php																				if(!$code)										{											$code = '';										}										else										{											$code = '/'.$code;										}										function sort_url($lang, $by, $sort, $sorder, $code, $admin_folder)										{											if ($sort == $by)											{												if ($sorder == 'asc')												{													$sort	= 'desc';													$icon	= ' <i class="icon-chevron-up"></i>';												}												else												{													$sort	= 'asc';													$icon	= ' <i class="icon-chevron-down"></i>';												}											}											else											{												$sort	= 'asc';												$icon	= '';											}																							$return = site_url('admin/orders/index/'.$by.'/'.$sort.'/'.$code);																						echo '<a href="'.$return.'">'.$lang.$icon.'</a>';										}																			if($term){ ?>
                                        <div class="alert alert-info">
                                            <?php echo sprintf('Your searched returned %d result(s)', intval($total));?>
                                        </div>
                                        <?php } ?>
                                            <style type="text/css">
                                                .pagination {
                                                    margin: 0px;
                                                    margin-top: -3px;
                                                }
                                            </style>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-sm-12" style="border-bottom:1px solid #f5f5f5;">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <?php echo $this->pagination->create_links(); ?>&nbsp; </div>
                                                <div class="col-sm-8">
                                                    <?php echo form_open('admin'.'/orders/index', 'class="form-inline" style="float:right"');?>
                                                        <fieldset>
                                                            <input id="start_top" value="" class="form-control" type="text" placeholder="Start Date" />
                                                            <input id="start_top_alt" type="hidden" name="start_date" />
                                                            <input id="end_top" value="" class="form-control" type="text" placeholder="End Date" />
                                                            <input id="end_top_alt" type="hidden" name="end_date" />
                                                            <input id="top" type="text" class="form-control" name="term" placeholder="Term" />
                                                            <button class="btn" name="submit" value="search">Search</button>
                                                            <button class="btn" name="submit" value="export" style="display:none;">xml Export</button>
                                                        </fieldset>
                                                        </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php echo form_open('admin'.'/orders/bulk_delete', array('id'=>'delete_form', 'onsubmit'=>'return submit_form();', 'class="form-inline"')); ?>
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <input type="checkbox" id="gc_check_all" />
                                                        <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i></button>
                                                    </th>
                                                    <th>
                                                        <?php echo sort_url('Order Number', 'order_number', $sort_by, $sort_order, $code, 'admin'); ?>
                                                    </th>
                                                    <th>
                                                        <?php echo sort_url('Bill To', 'bill_lastname', $sort_by, $sort_order, $code, 'admin'); ?>
                                                    </th>
                                                    <th>
                                                        <?php echo sort_url('Ship To', 'ship_lastname', $sort_by, $sort_order, $code, 'admin'); ?>
                                                    </th>
                                                    <th>
                                                        <?php echo sort_url('Ordered On','ordered_on', $sort_by, $sort_order, $code, 'admin'); ?>
                                                    </th>
                                                    <th>
                                                        <?php echo sort_url('Status','status', $sort_by, $sort_order, $code, 'admin'); ?>
                                                    </th>
                                                    <th>
                                                        <?php echo sort_url('Total','total', $sort_by, $sort_order, $code, 'admin'); ?>
                                                    </th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 																														echo (count($orders) < 1)?'<tr><td style="text-align:center;" colspan="8">There are currently no orders.</td></tr>':''?>
                                                    <?php foreach($orders as $order): 
														if($order->order_currency=='1'){
															$symbol = 'fa fa-rupee';
														}else if($order->order_currency=='2'){
															$symbol = 'fa fa-usd';
														}else if($order->order_currency=='3'){
															$symbol = 'fa fa-eur';
														}else if($order->order_currency=='4'){
															$symbol = 'fa fa-gbp';
														}else if($order->order_currency=='5'){
															$symbol = 'fa fa-usd';
														}else if($order->order_currency=='6'){
															$symbol = 'aed_gcc_image';
														}
													?>
                                                        <tr>
                                                            <td>
                                                                <input name="order[]" type="checkbox" value="<?php echo $order->id; ?>" class="gc_check" />
                                                            </td>
                                                            <td>
                                                                <?php echo $order->order_number; ?>
                                                            </td>
                                                            <td style="white-space:nowrap">
                                                                <?php echo $order->bill_firstname.' '.$order->bill_lastname; ?>
                                                            </td>
                                                            <td style="white-space:nowrap">
                                                                <?php echo $order->ship_firstname.' '.$order->ship_lastname; ?>
                                                            </td>
                                                            <td style="white-space:nowrap">
                                                                <?php echo date('d/m/Y h:i a', strtotime($order->ordered_on)); ?>
                                                            </td>
                                                            <td style="span2">
                                                                <?php 	$gtotal = $order->total;
																if($order->shipping > 0){													$gtotal = $gtotal + $order->shipping;
																}
																if($order->reward_amt > 0){														$gtotal = round($gtotal - ($order->reward_amt/100));
																}	
																echo form_dropdown('status', json_decode($this->config->item('order_statuses')), $order->status, 'id="status_form_'.$order->order_number.'" class="form-control" style="float:left;width: 130px;"');?>
                                                                    <!--<button type="button" class="btn btn-success" onClick="save_status('<?php echo $order->order_number; ?>', <?php echo $order->customer_id; ?>, <?php echo round($gtotal); ?>)" style="float:left;margin-left:4px;">Save</button>-->
                                                                   <a href="<?php echo base_url()."view/order/invoice/"; echo $order->order_number; ?>" class="btn btn-success" style="float:left;margin-left:4px;" target="_blank">Invoice</a>
                                                                   
                                                            </td>
                                                            <td>
                                                                <div class="MainTableNotes"><span class="<?php echo $symbol;?>"></span>
                                                                    <?php 												echo sprintf ("%.2f", $gtotal); ?>
                                                                </div>
                                                            </td>
                                                            <td> <a class="btn btn-info btn-sm" style="float:right;" href="<?php echo site_url('admin'.'/orders/order/'.$order->id);?>"><i class="fa fa-search"></i> Form View</a> </td>
                                                        </tr>
                                                        <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                        </form>
                                </div>
                                <div class="box-footer"> </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- /.content -->
        </div>
        <!-- /.right-side -->
        <script type="text/javascript">
            $(document).ready(function() {
                $('#gc_check_all').click(function() {
                    if (this.checked) {
                        $('.gc_check').attr('checked', 'checked');
                    } else {
                        $(".gc_check").removeAttr("checked");
                    }
                });
                $('#start_top').datepicker({
                    dateFormat: 'mm-dd-yy',
                    altField: '#start_top_alt',
                    altFormat: 'yy-mm-dd'
                });
                $('#start_bottom').datepicker({
                    dateFormat: 'mm-dd-yy',
                    altField: '#start_bottom_alt',
                    altFormat: 'yy-mm-dd'
                });
                $('#end_top').datepicker({
                    dateFormat: 'mm-dd-yy',
                    altField: '#end_top_alt',
                    altFormat: 'yy-mm-dd'
                });
                $('#end_bottom').datepicker({
                    dateFormat: 'mm-dd-yy',
                    altField: '#end_bottom_alt',
                    altFormat: 'yy-mm-dd'
                });
            });

            function do_search(val) {
                $('#search_term').val($('#' + val).val());
                $('#start_date').val($('#start_' + val + '_alt').val());
                $('#end_date').val($('#end_' + val + '_alt').val());
                $('#search_form').submit();
            }

            function do_export(val) {
                $('#export_search_term').val($('#' + val).val());
                $('#export_start_date').val($('#start_' + val + '_alt').val());
                $('#export_end_date').val($('#end_' + val + '_alt').val());
                $('#export_form').submit();
            }

            function submit_form() {
                if ($(".gc_check:checked").length > 0) {
                    return confirm('Are you sure you want to delete the selected orders?');
                } else {
                    alert('You did not select any orders to delete');
                    return false;
                }
            }

            function save_status(id, customer_id, change_amt) {
                show_animation();
                select_status = $('#status_form_' + id).val();
                $.post("<?php echo site_url('admin/orders/edit_status'); ?>", {
                    change_amt: change_amt,
                    customer_id: customer_id,
                    id: id,
                    status: select_status
                }, function() {
                    setTimeout(function() {
                        location.reload();
                    }, 500);
                })
            }

            function show_animation() {
                $('#saving_container').css('display', 'block');
                $('#saving').css('opacity', '.8');
            }

            function hide_animation() {
                $('#saving_container').fadeOut();
            }
        </script>
        <div id="saving_container" style="display:none;">
            <div id="saving" style="background-color:#000; position:fixed; width:100%; height:100%; top:0px; left:0px;z-index:100000"></div> <img id="saving_animation" src="<?php echo base_url('assets/admin/img/ajax-loader1.gif');?>" alt="saving" style="z-index:100001; margin-left:-32px; margin-top:-32px; position:fixed; left:50%; top:50%" />
            <div id="saving_text" style="text-align:center; width:100%; position:fixed; left:0px; top:50%; margin-top:40px; color:#fff; z-index:100001">Saving</div>
        </div>
        <?php $this->load->view('admin/adminFooter');?>
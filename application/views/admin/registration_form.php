<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>dentkart::<?php echo (isset($page))?' :: '.$page:''; ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo HTTP_CSS_PATH_ADMIN; ?>AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="<?php echo HTTP_JS_PATH; ?>html5shiv.js"></script>
			<script src="<?php echo HTTP_JS_PATH; ?>respond.min.js"></script>
        <![endif]-->
		<style>
		.input-group.error {
		border: 1px solid #a94442 !important;
		}
		</style>
    </head>
    <body class="bg-black">
         

        <div class="form-box" id="login-box">
            <div class="header bg-light-blue">New Seller Registration
			
			
			</div>
			
            <form action="" method="post" name="frmRegistration" id="frmRegistration">
			     <input type="hidden" name="hidRfrm" value="yes"/>
                <div class="body bg-gray">
				    <div id='msg'></div>
					<div class="row">
					    <div class="col-sm-6">
							<div class="form-group">							  		  
									<input type="text" name="shopname" id="shopname" class="form-control" placeholder="Shop Name"/>							  
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							  		<input type="text" name="shopaddress" id="shopaddress" class="form-control" placeholder="Shop Address"/>																  
							</div>
						</div>
					</div>
					<div class="row">
					    <div class="col-sm-6">
							<div class="form-group">
							   <div class="input-group">
									<div class="input-group-addon"><i class="fa fa-user"></i></div>					  
										  <input type="text" name="username" id="username" class="form-control" placeholder="Contact Person"/>
							   </div><!-- /.input group -->					   
						    </div>
					    </div>
						<div class="col-sm-6">
							<div class="form-group">
							    <div class="input-group">
                                  <div class="input-group-addon"><i class="fa fa-at"></i></div>					         
									<input type="email" name="useremail" id="useremail" class="form-control" placeholder="User Email"/>
								</div>							
															  				   
						    </div>
					    </div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
												        
									<input type="text" name="state" id="state" class="form-control" placeholder="State"/>
								
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
													        
									<input type="text" name="city" id="city" class="form-control" placeholder="City"/>
								
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
								<div class="form-group">
									<div class="input-group">
										<div class="input-group-addon">+91</div>
										<input type="text" name="usermobile" id="usermobile" class="form-control" placeholder="User MobileNo."/>
									</div><!-- /.input group -->
								</div><!-- /.form group -->
						</div>
						<div class="col-sm-6">
							<div class="form-group">
									<div class="input-group">
										<div class="input-group-addon"><i class="fa fa-phone"></i></div>			        
									<input type="text" name="landline" id="landline" class="form-control" placeholder="LandLine No."/>
								    </div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
								<div class="form-group">
									<div class="input-group">
										<div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
										<input type="text" name="area" id="area" class="form-control" placeholder="Area"/>
									</div><!-- /.input group -->
								</div><!-- /.form group -->
						</div>
						<div class="col-sm-6">
							<div class="form-group">
									<div class="input-group">
										<div class="input-group-addon"><i class="fa fa-th"></i></div>			        
									<input type="text" name="pincode" id="pincode" class="form-control" placeholder="Area Pin Code"/>
								    </div>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<div class="input-group">
									<div class="input-group-addon"><i class="fa fa-key"></i></div>					     
									<input type="password" name="userpassword" id="userpassword" class="form-control" placeholder="Password"/>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<div class="input-group">
									<div class="input-group-addon"><i class="fa fa-key"></i></div>					     
									<input type="password" name="userpassword2" id="userpassword2" class="form-control" placeholder="Retype password"/>
								</div>
							</div>
						</div>
				   </div>
                 </div>
				<div class="footer">

                    <button type="submit" name="btnRegister" id="btnRegister" class="btn bg-blue btn-block">Sign me up</button>

                    <a href="<?=site_url('admin/home');?>" class="text-center">I already have a membership</a>
                </div>
            </form>
            <div class="overlay"></div>
			<div class="loading-img"></div>
           
        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js" type="text/javascript"></script>
		 <script src="<?php echo HTTP_JS_PATH_ADMIN; ?>AdminLTE/jquery.validate.min.js" type="text/javascript"></script>
		 <script>
		 $("#frmRegistration").validate({
		        focusInvalid: false, 
                ignore: "",
				rules: {
					username: "required",
					userpassword: {required:true,minlength:6},
					userpassword2:{required: true,minlength:6,equalTo: "#userpassword"},
					useremail:{required:true,email:true},
					usermobile:{required: true,digits: true,rangelength: [10, 10]}
				},
				errorPlacement: function (label, element) { // render error placement for each input type
                    //alert($(element).parent().attr("class"));				
					$('<span class="error"></span>').insertAfter($(element).parent()).append(label)
                    //var parent = $(element).parent('.input-with-icon');
                    //parent.removeClass('success-control').addClass('error-control');  
                },
				submitHandler: function(form) {
				       $( ".form-box" ).addClass("box");
						$.ajax({
							url: '<?php echo base_url(); ?>admin/home/do_register',
							type: 'post',
							dataType: 'json',
							data: $('form#frmRegistration').serialize(),
							success: function(data) {
							  if(data.auth=='true')
							  {
								$('#msg').html('<div class="alert alert-success" style="margin-left: 0px;"><a class="close" data-dismiss="alert">×</a>'+data.msg+'</div>');
								$( ".form-box" ).removeClass( "box" );
							  }
							  else
							  {
							      $('#msg').html('<div class="alert alert-danger" style="margin-left: 0px;"><a class="close" data-dismiss="alert">×</a>'+data.msg+'</div>');
								  $( ".form-box" ).removeClass( "box" );
							  }
							}
						});
				}
			});
		</script>

    </body>
</html>

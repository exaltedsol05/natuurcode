<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<link rel="shortcut icon" href="<?php echo HTTP_IMAGES_PATH; ?>favicon.png" type="images/png">
    <title>Natuur::<?php echo (isset($page_title))?' :: '.$page_title:''; ?></title>
	
	<!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo HTTP_CSS_PATH_ADMIN; ?>bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo HTTP_CSS_PATH_ADMIN; ?>AdminLTE.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <!--<link rel="stylesheet" href="<?php echo HTTP_CSS_PATH_ADMIN; ?>skins/_all-skins.min.css">-->
	<link rel="stylesheet" href="<?php echo HTTP_CSS_PATH_ADMIN; ?>skins/skin-green-light.css">
	
    <link href="<?php echo HTTP_CSS_PATH_ADMIN; ?>jquery-ui.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo HTTP_CSS_PATH_ADMIN; ?>redactor.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo HTTP_CSS_PATH_ADMIN; ?>main.css" rel="stylesheet" type="text/css" />
	<!-- DataTables -->
    <link rel="stylesheet" href="<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/datatables/dataTables.bootstrap.css">
	<!-- bootstrap timepicker -->
	<link rel="stylesheet" href="<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/timepicker/bootstrap-timepicker.min.css">
	<!-- iCheck for checkboxes and radio inputs -->
   <link rel="stylesheet" href="<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/iCheck/all.css">
	<!-- jQuery 2.2.3 -->
	<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>jquery-ui.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>bootstrap.min.js"></script>
	
	<!-- bootstrap timepicker -->
	<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/timepicker/bootstrap-timepicker.min.js"></script>
	<!-- bootstrap datepicker -->
	<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- PreLoader -->
	<link href="<?php echo HTTP_CSS_PATH_ADMIN; ?>main.css" rel="stylesheet" type="text/css" />
	<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>modernizr-2.6.2.min.js"></script>
	<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>redactor.min.js" type="text/javascript"></script>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
    
  </head>
 <body class="skin-green-light sidebar-mini">
   
  <div class="wrapper">
       <div id="loader-wrapper" style="display:none;">
			<div id="loader"></div>

			<div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>

       </div>
        <?php
			$pg = isset($page) && $page != '' ?  $page :'dash'  ;    
		?>
		
		 <header class="main-header">

    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>N</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Natuur</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
		  <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo HTTP_IMAGES_PATH_ADMIN; ?>user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"> <?php echo ucfirst($this->session->userdata('admin_name'));?> </span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo HTTP_IMAGES_PATH_ADMIN; ?>user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                   <?php echo ucfirst($this->session->userdata('admin_name'));?> - <?php echo ucfirst($this->session->userdata('roll'));?>
                    <small>Member since <?php echo ucfirst($this->session->userdata('create_date'));?></small>
                </p>
              </li>
              <!-- Menu Body -->
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo base_url(); ?>admin/home/profile/<?php echo $this->session->userdata('id');?>" class="btn btn-primary btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url(); ?>admin/home/logout" class="btn btn-danger btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          
        </ul>
      </div>

    </nav>
  </header>
 
 <!-- Content Wrapper. Contains page content -->
  
<?php $this->load->view('admin/adminHeader');?>
<?php $this->load->view('admin/adminLeftSidebar');?>
<script type="text/javascript">
function areyousure(){	return confirm('Are you sure to delete this?');}
function delete_newsletter(id) {
	if(confirm("Warning : This will permanently delete this NewsLetter and  all the related records, once deleted those records can't be restored!!!\n\nSo, are you absolutely sure about deleting this NewsLetter?")){
		window.location='<?php echo base_url(); ?>admin/customers/delete_newsletter/'+id;         
		}
	} 
</script>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>                        NewsLetter                        <small>admin panel</small>                    </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="#"> Catalog</a></li>
         <li class="active"> NewsLetter</li>
      </ol>
   </section>
   <?php $this->load->view('admin/adminError');?>                <!-- Main content -->				
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <div class="box-header">
                  <h3 class="box-title" style="width: 100%;">All NewsLetter </h3>
               </div>
               <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th>S.No.</th>
                           <th>Email Id</th>
                           <th>Status</th>
                           <th>Delete</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
							$i=1;
							foreach ($arrNewsLetter as $NewsLetter){ ?>												
                        <tr>
                           <td><?php echo  $i++; ?></td>
                           <td><?php echo  $NewsLetter->email; ?></td>
                           <td><?php echo  ($NewsLetter->enable==1)?'Enabled':'Disabled'; ?></td>
						   <td><a href="javascript:void(0);" class="btn btn-danger"  onclick="delete_newsletter('<?php echo $NewsLetter->id;?>');"><i class="fa fa-trash-o"></i> Delete</a></td>
                        </tr>
                        <?php } ?>                                            											                                        
                     </tbody>
                  </table>
               </div>
               <div class="box-footer">									 							    </div>
            </div>
         </div>
      </div>
   </section>
   <!-- /.content -->
</div>
<!-- /.right-side -->
<?php $this->load->view('admin/adminFooter');?>
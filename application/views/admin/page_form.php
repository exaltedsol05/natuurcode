<?php
$this->load->view('admin/adminHeader');
?>
<?php
$this->load->view('admin/adminLeftSidebar');
?>
<aside class="right-side">
	<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
			Page Form
			<small>Control panel</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Page Form</li>
			</ol>
		</section>
		<?php $this->load->view('admin/adminError');?>
		<section class="content">

<?php echo form_open('admin/pages/form/'.$id); ?>

<div class="tabbable">
	
	<ul class="nav nav-tabs">
		<li class="active"><a href="#content_tab" data-toggle="tab">Content</a></li>
		<li><a href="#attributes_tab" data-toggle="tab">Attributes</a></li>
		<li><a href="#seo_tab" data-toggle="tab">SEO</a></li>
	</ul>
	
	<div class="tab-content">
		<div class="tab-pane active" id="content_tab">
			<fieldset>
				<label for="title">Title</label>
				<?php
				$data	= array('name'=>'title', 'value'=>set_value('title', $title), 'class'=>'form-control');
				echo form_input($data);
				?>
				
				<label for="content">Content</label>
				<?php 
				$data	= array('name'=>'content', 'class'=>'form-control redactor', 'value'=>set_value('content', $content));
				echo form_textarea($data);
				?>
			</fieldset>
		</div>

		<div class="tab-pane" id="attributes_tab">
			<fieldset>
				<label for="menu_title">Menu Title</label>
				<?php
				$data	= array('name'=>'menu_title', 'value'=>set_value('menu_title', $menu_title), 'class'=>'form-control');
				echo form_input($data);
				?>
			
				<label for="slug">Slug</label>
				<?php
				$data	= array('name'=>'slug', 'value'=>set_value('slug', $slug), 'class'=>'form-control');
				echo form_input($data);
				?>
			
				<label for="sequence">Parent</label>
				<?php
				$options	= array();
				$options[0]	= 'Top Level';
				function page_loop($pages, $dash = '', $id=0)
				{
					$options	= array();
					foreach($pages as $page)
					{
						//this is to stop the whole tree of a particular link from showing up while editing it
						if($id != $page->id)
						{
							$options[$page->id]	= $dash.' '.$page->title;
							$options			= $options + page_loop($page->children, $dash.'-', $id);
						}
					}
					return $options;
				}
				$options	= $options + page_loop($pages, '', $id);
				echo form_dropdown('parent_id', $options,  set_value('parent_id', $parent_id), 'class="form-control input-sm"');
				?>
			
				<label for="sequence">Sequence</label>
				<?php
				$data	= array('name'=>'sequence', 'value'=>set_value('sequence', $sequence), 'class'=>'form-control');
				echo form_input($data);
				?>
			</fieldset>
		</div>
	
		<div class="tab-pane" id="seo_tab">
			<fieldset>
				<label for="code">SEO Title</label>
				<?php
				$data	= array('name'=>'seo_title', 'value'=>set_value('seo_title', $seo_title), 'class'=>'form-control');
				echo form_input($data);
				?>
			
				<label>Meta</label>
				<?php
				$data	= array('rows'=>'3', 'name'=>'meta', 'value'=>set_value('meta', html_entity_decode($meta)), 'class'=>'form-control');
				echo form_textarea($data);
				?>
				
				<p class="help-block">ex. &lt;meta name="description" content="We sell products that help you" /&gt;</p>
			</fieldset>
		</div>
	</div>
</div>

<div class="form-actions">
	<button type="submit" class="btn btn-primary">Save</button>
</div>	
</form>
</section>
</aside>
<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/redactor-plugins-master/fontcolor.js" type="text/javascript"></script>

<script type="text/javascript">

$(document).ready(function(){

    
    $('.redactor').redactor({
			plugins: ['fontcolor'],
			buttons: [
				'bold',
				'italic',
				'underline',
				'formatting',
				'image',
				'link',
				'unorderedlist',
				'orderedlist',
				'alignment',
				'|',
				'html'
			],
            minHeight: 200,
            imageUpload: '<?php echo base_url(); ?>admin/wysiwyg/upload_image',
            fileUpload: '<?php echo base_url(); ?>admin/wysiwyg/upload_file',
            imageGetJson: '<?php echo base_url(); ?>admin/wysiwyg/get_images',
            imageUploadErrorCallback: function(json)
            {
                alert(json.error);
            },
            fileUploadErrorCallback: function(json)
            {
                alert(json.error);
            }
      });

});
</script>
<?php
$this->load->view('admin/adminFooter');
?>
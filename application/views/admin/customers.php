<?php $this->load->view('admin/adminHeader');?>
<?php $this->load->view('admin/adminLeftSidebar');?>
<script type="text/javascript">
function areyousure()
{
	return confirm('Are you sure you want to delete this customer?');
}
</script>
<div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Customers
                        <small>admin panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#"> Catalog</a></li>
                        <li class="active"> Customers</li>
                    </ol>
                </section>
                <?php $this->load->view('admin/adminError');?>
                <!-- Main content -->
				<section class="content">
					<div class="row">
						<div class="col-md-12">
							<div class="box box-primary">
							    <div class="box-header">
									<div class="btn-group1 pull-right1" style="float:left;width: 100%;">
										<div style="float:left;">
											<?php  $page_links	= $this->pagination->create_links();
											if($page_links != ''):
												echo $page_links;
											endif;?>
										</div>
										<div style="float:right;">
											<!--<a class="btn btn-primary" href="<?php echo site_url('admin/customers/export_xml');?>"><i class="fa fa-download"></i> Export Customers (XML)</a>
											<a class="btn btn-primary" href="<?php echo site_url('admin/customers/get_subscriber_list');?>"><i class="fa fa-download"></i> Opt-in Emails (CSV)</a>-->
											<a class="btn btn-primary" href="<?php echo site_url('admin/customers/form'); ?>"><i class="fa fa-plus"></i> Add New Customer</a>
										</div>
									</div>
							    
							    </div>
								<div class="box-body">
							         <table class="table table-bordered table-hover">
											<thead>
												<tr><?php if($by=='ASC'){ $by='DESC';}else{$by='ASC';}?>
													<th>Sr.No.</th>
													<th>
													    <a href="<?php echo site_url('admin/customers/index/firstname/');?>/<?php echo ($field == 'firstname')?$by:'';?>">First Name
													    <?php if($field == 'firstname'){ echo ($by == 'ASC')?'<i class="fa fa-sort-alpha-asc"></i>':'<i class="fa fa-sort-alpha-desc"></i>';} ?></a>
												    </th>
													
													<th>
														<a href="<?php echo site_url('admin/customers/index/lastname/');?>/<?php echo ($field == 'lastname')?$by:'';?>">Last Name
														<?php if($field == 'lastname'){ echo ($by == 'ASC')?'<i class="fa fa-sort-alpha-asc"></i>':'<i class="fa fa-sort-alpha-desc"></i>';} ?> </a>
													</th>
												
													<th>
														<a href="<?php echo site_url('admin/customers/index/email/');?>/<?php echo ($field == 'email')?$by:'';?>">Email
														<?php if($field == 'email'){ echo ($by == 'ASC')?'<i class="fa fa-sort-alpha-asc"></i>':'<i class="fa fa-sort-alpha-desc"></i>';} ?></a>
													</th>
												
												<th>Active</th>
												<th></th>
												</tr>
											</thead>
												<tbody>
												<?php echo (count($customers) < 1)?'<tr><td style="text-align:left;" colspan="5">There are currently no customer.</td></tr>':''?>
												<?php $i=0;foreach ($customers as $customer): $i++;?>
												<tr>
												
												    <td><?php echo $i; ?></td>
													
													<td><?php echo  $customer->firstname; ?></td>
													<td><?php echo  $customer->lastname; ?></td>
													
													<td><a href="mailto:<?php echo  $customer->email;?>"><?php echo  $customer->email; ?></a></td>
													 
													<td><?php 
														if($customer->active == 1){
															echo 'Yes';
														}else{
															echo 'No';
														}?>
													</td>
													<td>
														<div class="btn-group" style="float:right">
														<a class="btn btn-info" href="<?php echo site_url('admin/customers/form/'.$customer->id); ?>"><i class="fa fa-pencil"></i>Edit</a>

														<a class="btn btn-success" href="<?php echo site_url('admin/customers/addresses/'.$customer->id); ?>"><i class="fa fa-envelope"></i> Addresses</a>
														
														<a class="btn btn-primary" href="<?php echo site_url('admin/customers/sendMailToCustomer/'.$customer->id); ?>"><i class="fa fa-paper-plane"></i>Send Mail</a>

														<a class="btn btn-danger" href="<?php echo site_url('admin/customers/delete/'.$customer->id); ?>" onclick="return areyousure();"><i class="fa fa-trash-o"></i> Delete</a>
														</div>
													</td>
												</tr>
												<?php endforeach;?>
										</tbody>
							        </table>
				
				
							    </div>
								<div class="box-footer">
									<div style="float:left;">
										<?php  
											if($page_links != ''):
												echo $page_links;
											endif;
										?>
									</div>   
							    </div>
							     
							</div>
						</div>
					</div>
				
				
				
				</section><!-- /.content -->
</div><!-- /.right-side -->
<?php $this->load->view('admin/adminFooter');?>
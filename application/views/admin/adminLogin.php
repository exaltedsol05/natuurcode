<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Natuur::<?php echo (isset($page))?' :: '.$page:''; ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
      

			<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!--[if lt IE 9]>
			<script src="<?php echo HTTP_JS_PATH; ?>html5shiv.js"></script>
			<script src="<?php echo HTTP_JS_PATH; ?>respond.min.js"></script>
			<![endif]-->
    </head>
    <body >
<style>
body{ 
	background: url(<?php echo HTTP_IMAGES_PATH_ADMIN; ?>admin_bg.jpg) no-repeat center center fixed; 
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
}

.panel-default {
opacity: 0.9;
margin-top:30px;
}
.form-group.last { margin-bottom:0px; }
</style>
    <div class="container">
			<div class="row">
				<div class="col-md-4 col-md-offset-4" style="margin-top:8%;">
					<div class="panel panel-default">
						<div class="panel-heading" align="center">
							<p><h3>Admin Panel Login</h3></p>
							<?php
							if(isset($error)){    
							?>
							<div class="alert alert-dismissible alert-danger" style="margin-left:0px;">
								<button type="button" class="close" data-dismiss="alert">×</button>
								<strong>Oh snap!</strong> <a href="<?php echo base_url(); ?>admin" class="alert-link"><?=$error?></a>
							</div>
							<?php } ?>
						</div>
						<div class="panel-body">
						
							<form class="form-horizontal" role="form" action="<?php echo base_url(); ?>admin/home/do_login" method="post" name="frmLogin" id="frmLogin">
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-3 control-label">UserName</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" name="username" id="username" placeholder="User Name" required>
								</div>
							</div>
							<div class="form-group">
								<label for="inputPassword3" class="col-sm-3 control-label">Password</label>
								<div class="col-sm-9">
									<input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<div class="checkbox">
										<label><input type="checkbox"/>Remember me</label>
									</div>
								</div>
							</div>
							<div class="form-group last">
								<div class="col-sm-offset-3 col-sm-9">
									<button type="submit" class="btn btn-success btn-sm">
										Sign in</button>
										 <button type="reset" class="btn btn-default btn-sm">
										Reset</button>
								</div>
							</div>
							</form>
						
						
						
						</div>
						<div class="panel-footer">
						   Admin panel Login
						</div>
					</div>
				</div>
			</div>
	</div>
		
		
		
    </body>
</html>

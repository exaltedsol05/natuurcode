<?php $this->load->view('admin/adminHeader');?>
<?php $this->load->view('admin/adminLeftSidebar');?>
<div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Customer Form
                        <small>admin panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#"> Catalog</a></li>
                        <li class="active"> Customer Form</li>
                    </ol>
                </section>
                <?php $this->load->view('admin/adminError');?>
                <!-- Main content -->
                <section class="content">
					<?php echo form_open('admin/customers/form/'.$id); ?>
					 <input type="hidden" name="auth_type" value="manual" />
					<div class="row">
						 <div class="col-md-12">
								<div class="box box-primary">
										<div class="box-header"></div>
										<div class="box-body">
										    <div class="col-md-6">
												<div class="form-group">
													<label>Company</label>
													<?php
													$data	= array('name'=>'company', 'value'=>set_value('company', $company), 'class'=>'form-control');
													echo form_input($data); ?>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
														<label>First Name</label>
														<?php
														$data	= array('name'=>'firstname', 'value'=>set_value('firstname', $firstname), 'class'=>'form-control');
														echo form_input($data); ?>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Last Name</label>
													<?php
													$data	= array('name'=>'lastname', 'value'=>set_value('lastname', $lastname), 'class'=>'form-control');
													echo form_input($data); ?>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Email</label>
													<?php
													$data	= array('name'=>'email', 'value'=>set_value('email', $email), 'class'=>'form-control');
													echo form_input($data); ?>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Phone</label>
													<?php
													$data	= array('name'=>'phone', 'value'=>set_value('phone', $phone), 'class'=>'form-control');
													echo form_input($data); ?>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Pincode </label><?php
													$data	= array('name'=>'pincode', 'value'=>set_value('pincode', $pincode), 'class'=>'form-control');
													echo form_input($data); ?>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Password</label>
													<?php
													$data	= array('name'=>'password', 'class'=>'form-control');
													echo form_password($data); ?>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Confirm</label>
													<?php
													$data	= array('name'=>'confirm', 'class'=>'form-control');
													echo form_password($data); ?>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="checkbox">
													<?php $data	= array('name'=>'email_subscribe', 'value'=>1, 'checked'=>(bool)$email_subscribe);
													echo form_checkbox($data).' Email Subscribed';?>
												   </label>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="checkbox">
													<?php
													$data	= array('name'=>'active', 'value'=>1, 'checked'=>$active);
													echo form_checkbox($data).' Active'; 
													?>
													</label>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Group</label>
													<?php echo form_dropdown('group_id', $group_list, set_value('group_id',$group_id), 'class="form-control"'); ?>
												</div>
											</div>
										</div>
										<div class="box-footer">
										      <input class="btn btn-primary" type="submit" value="Save"/>
										</div>
								</div>
						 </div>
					</div>
					

				</form>
			</section><!-- /.content -->
 </div><!-- /.right-side -->
<?php $this->load->view('admin/adminFooter'); ?>
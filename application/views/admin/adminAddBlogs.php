<?php $this->load->view('admin/adminHeader');?>
<?php $this->load->view('admin/adminLeftSidebar');?>
<link href="<?php echo HTTP_UPLOADIFY_PATH; ?>uploadify.css" rel="stylesheet" type="text/css" media="screen">
<script src="<?php echo HTTP_UPLOADIFY_PATH; ?>jquery.uploadify.min.js"></script>
<div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Add/Edit Blog
                        <small>admin panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#"> Catalog</a></li>
                        <li class="active"> Add/Edit Blog</li>
                    </ol>
                </section>
                <?php $this->load->view('admin/adminError');?>
                <!-- Main content -->
				<section class="content">
					<div class="row">
						<div class="col-md-12">
							<div class="box box-primary">
							    <div class="box-header">
									
							    
							    </div>
								<div class="box-body">
							         
									<section class="content">
										<!-- Main row -->
										<div class="row">
										<section class="col-sm-12">
													<label for="test_content">Blog Image</label>
													<div class="form-control" id="imgProfile_upload" style="border: 1px solid #ddd;width:180px; height:140px;margin-top:5px;">
														<img src="<?php echo base_url().'uploads/blog/small/'.$blog->blog_image; ?>" class="img-square img-responsive" style="">
														
													</div>
													<div class="" style="padding:5px; ">
														<form>
															<div id="queue"></div>
															<input id="profileimage_upload" name="profileimage_upload" type="file" multiple="true">
														</form>
													</div>
										
											
												<!-- Custom tabs (Charts with tabs)-->
												<?php 
													$attributes = array("name" => "blog_form", "id" => "blog_form");
													echo form_open_multipart("admin/blogs/insertNewBlog", $attributes);
												?>
												<input type="hidden" value="<?php echo $blog->id;?>" name="blog_id">
												<input type="hidden" id="blog_image" name="blog_image" value="<?php echo $blog->blog_image;?>">
												
												<fieldset>
													<label for="email">Blog Title</label>
													<?php
													$data	= array('name'=>'title', 'value'=>set_value('title', $blog->title), 'class'=>'form-control');
													echo form_input($data);
													?>	

													<label for="test_content">Blog Content</label>
													<?php 
													$data	= array('name'=>'content', 'class'=>'form-control redactor', 'value'=>set_value('content', $blog->content));
													echo form_textarea($data);
													?>
													
												</fieldset>
												<div class="pull-left">
												<button type="submit" class="btn btn-primary" style="margin:10px 0px 10px 10px;">Save</button>
												</div>
											 <?php echo form_close();?>	
											 
											</section><!-- /.Left col -->
										   
										</div><!-- /.row (main row) -->

									</section><!-- /.content -->
				
							    </div>
								<div class="box-footer">
									 
							    </div>
							     
							</div>
						</div>
					</div>
				
				
				
				</section><!-- /.content -->
</div><!-- /.right-side -->
<?php $this->load->view('admin/adminFooter');?>
<script type="text/javascript">
$(document).ready(function(){

    
    $('.redactor').redactor({
			//plugins: ['fontcolor'],
			buttons: [
				'bold',
				'italic',
				'underline',
				'formatting',
				'image',
				'link',
				'unorderedlist',
				'orderedlist',
				'alignment',
				'|',
				'html'
			],
            minHeight: 200,
            imageUpload: '<?php echo base_url(); ?>admin/wysiwyg/upload_image',
            fileUpload: '<?php echo base_url(); ?>admin/wysiwyg/upload_file',
            imageGetJson: '<?php echo base_url(); ?>admin/wysiwyg/get_images',
            imageUploadErrorCallback: function(json)
            {
                alert(json.error);
            },
            fileUploadErrorCallback: function(json)
            {
                alert(json.error);
            }
      });
	  
	  
	$('#profileimage_upload').uploadify({
				
		'formData'     : {
			'flag'      : 'profileimage_upload',
			'tId'      : "<?php echo $customer['id']; ?>"
		},
		'onSelect' : function(file) {
							
			$('#imgProfile_upload').html('<img class="img-circle img-responsive" src="<?php echo HTTP_UPLOADIFY_PATH; ?>ajax-loader1.gif">');
		 },
		'buttonImage' : '<?php echo HTTP_UPLOADIFY_PATH; ?>browse-btn.png',
		'buttonText' : 'Add Event Image..',
		'multi': false,
		'swf'      : '<?php echo HTTP_UPLOADIFY_PATH; ?>uploadify.swf',
		'uploader' : '<?php echo base_url(); ?>admin/blogs/uploadBlogImg',
		'onUploadSuccess': function (file, data, response) {
			
			var extension = file.name.replace(/^.*\./, '');
			var a=$.parseJSON(data);
			var imgName=a.imagename;
			//alert(imgName);
			$('#imgProfile_upload').html('<img alt="profile image" style="width:180px; height:125px;" class="img-square" src="<?php echo base_url(); ?>uploads/blog/small/'+imgName+'">');
			
			$("#blog_image").val(imgName);
		}
		
	});

});
$('form').submit(function() {
	$('.btn').attr('disabled', true).addClass('disabled');
});
</script>
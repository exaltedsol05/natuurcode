<?php $this->load->view('admin/adminHeader'); 
$this->load->view('admin/adminLeftSidebar');?><?php
 ?>
<aside class="right-side">

	<!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Customer eCash
        <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Customer eCash</li>
        </ol>
    </section><?php 
	$this->load->view('admin/adminError');?>
	<section class="content"> <?php 
		if(isset($msg)){?>
			<div class="row" style="text-align:center;color:green;font-size:18px;">
				<?php echo $msg;?>
			</div><?php 
		}?>
		<script>					
			$('.datepicker').datepicker({ 
				startDate: "today",
				format: 'yyyy-mm-dd',
				todayHighlight: true,
				autoclose:true
			});
			
			function slide_view(){
				$('#add_ecash_customer').slideToggle('slow');
				$('#show_ecash_trans').slideToggle('slow');
				$('#add_cash_text').slideToggle('slow');
				$('#calcle_cash_text').slideToggle('slow');
			}
			
			function validate_add_ecash(){
				
			}
			
			function brand_discount_validation(){
				validateioncond = true;
				
				var discount = $("#brand_discount_rate").val();	
				if(isNaN(discount)){
					$("#frm_errors").text("Discount rate must be numeric.");
					$("#brand_discount_rate").focus();
					validateioncond = false;
					return false;
				}
			}					
		</script>
        <section ><?php
			if($isshowform){
				$display_view = 'display:none;"';
				$display_form  = '';
			}else{
				$display_view = '';
				$display_form = "display:none;";
			}?>
			<a class="btn btn-primary" onclick="slide_view();" id="add_cash_text" style="float:right;<?php echo $display_view; ?>" ><i class="fa fa-plus"></i> Add eCash</a>
                <div id="add_ecash_customer" style="<?php echo $display_form; ?>" >
                	<table class="table" class="col-sm-12">
						<thead>
							<tr>
                              	<th>#</th>
                                <th>Activity</th>
                              	<th>eCash</th>
                              	<th></th>
                        	</tr>
						</thead>
						<tbody><?php
						$count = 1;
						foreach($ecash_setting as $setting){?>
                        	<tr>   
                            	<td class="col-sm-1"><?php echo $count; ?>.</td> 
                        		<td class="col-sm-9"><?php echo $setting->label; ?></td>
                                <td colspan="1" class="col-sm-2"><?php
                                	echo form_open('admin/customers/customer_ecash/'.$userid); ?>
                                        <input type="text" name="ecash_amt" value="0" class="form-control" style="width:80px;float:left;"/> &nbsp;&nbsp;
                                        <input type="hidden" name="user_id" value="<?php echo $userid; ?>" class="form-control" style="width:80px;float:left;"/>
                                        <input type="hidden" name="ecash_label" value="<?php echo $setting->label; ?>" class="form-control" style="width:80px;float:left;"/>
                                        <input type="submit" value="Save" class="btn btn-primary" style="float:right;" onclick="validate_add_ecash();">
                                    </form>
                                </td>
                            </tr><?php
							$count++;
                        }?>        
                    	</tbody>
                    </table>
                    <a class="btn btn-primary" style="float:right;" href="#" onclick="slide_view();" id="add_cash_text">Cancle</a><br/>
                    <hr/>
                </div>                
                <table class="table" id="show_ecash_trans" class="col-sm-12" style="<?php echo $display_view; ?>">
					<thead>
						<tr>
						  <th>#</th>
						  <th>Activity</th>
						  <th>eCash</th>
                          <th>Action</th>
                          <th>Date</th>
						</tr>
					</thead>
					<tbody><?php
						$count = 1;
						$total_rows = sizeof($ecash_records);
						if($total_rows >= 1){
							$count = 1;
							foreach($ecash_records as $ecash){ ?>
								<tr>
									<td class="col-sm-1"><?php echo $count."."; ?></td>
                                    <td class="col-sm-8"><?php echo $ecash ->action; ?></td>
                                    <td class="col-sm-1"><?php echo $ecash ->ecash; ?></td>
                                    <td class="col-sm-1"><?php 
                                        if($ecash ->tranction)
                                            echo "earn";
                                        else
                                            echo "Less"; ?>
                                    </td>
                                    <td class="col-sm-1"><?php 
                                        $strdate = strtotime($ecash ->date);
                                        echo date("d/m/Y", $strdate); ?>
                                    </td>
								</tr><?php
								$count++;
							}
						}else{?>
							<tr>
								<td colspan="6">No eCash transaction found.</td>
							</tr><?php
						}?>
					</tbody>
				</table>
			</section>
	    </section>
</aside><?php
$this->load->view('admin/adminFooter'); ?>
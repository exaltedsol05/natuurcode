<?php $this->load->view('admin/adminHeader');?>
<?php $this->load->view('admin/adminLeftSidebar');?>



<div class="content-wrapper">
	<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
			<?=$page_title;?>
			<small>Control panel</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active"><?=$page_title;?></li>
			</ol>
		</section>
		<?php $this->load->view('admin/adminError');?>
		<section class="content">
		       
		       <div class="row">
			        <div class="col-sm-12">
			        <div class="box box-primary">
						
						<div class="box-header"> <a style="float:right;" class="btn btn-primary pull-right" href="<?php echo site_url('admin/banners/banner_collection_form'); ?>"><i class="fa fa-plus"></i> Add New Banner Collection</a></div>
						<div class="box-body">
						      <table class="table table-striped">
							<thead>
								<tr>
								    <th>Sr.No.</th>
									<th>Name</th>
									<th ></th>
								</tr>
							</thead>
							<?php echo (count($banner_collections) < 1)?'<tr><td style="text-align:center;" colspan="5">There are currently no banner collections</td></tr>':''?>
							<?php if ($banner_collections): ?>
							<tbody>
							<?php
                            $i=0;
							foreach ($banner_collections as $banner_collection): $i++;?>
								<tr>
								     
									<td><?php echo $i;?></td>
									<td><?php echo $banner_collection->name;?></td>
									<td>
										<div class="btn-group" style="float:right">
											<a class="btn btn-success" href="<?php echo base_url('admin/banners/banner_collection/'.$banner_collection->banner_collection_id);?>"><i class="fa fa-file-image-o"></i> Banners</a>
											
											<a class="btn btn-info" href="<?php echo site_url('admin/banners/banner_collection_form/'.$banner_collection->banner_collection_id);?>"><i class="fa fa-pencil"></i> Edit</a>
											
											<a class="btn btn-danger" href="<?php echo site_url('admin/banners/delete_banner_collection/'.$banner_collection->banner_collection_id);?>" onclick="return areyousure();"><i class="fa fa-trash-o"></i> Delete</a>
										</div>
									</td>
								</tr>
							<?php endforeach; ?>
							</tbody>
					<?php endif;?>
					</table>					  					   
					
						</div>
						<div class="box-footer"></div>
					</div>														    
			
			        </div>
			</div>
		</section>

</div>
<script type="text/javascript">
function areyousure(){
	return confirm('Are you sure you want to delete this banner collection and all it\\\'s contents?');
}
</script>
<?php
$this->load->view('admin/adminFooter');
?>
<?php
$this->load->view('admin/adminHeader');
?>
<?php
$this->load->view('admin/adminLeftSidebar');
?>
<link href="<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/uploadify/uploadify.css" rel="stylesheet" type="text/css" />
<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/uploadify/jquery.uploadify.min.js" type="text/javascript"></script>
            <aside class="right-side">
                <!-- Content Header (Page header) -->

                <section class="content-header">
                    <h1>
                        Manufacturers Form
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Manufacturers Form</li>
                    </ol>
                </section>
				<?php $this->load->view('admin/adminError');?>
					
                <!-- Main content -->
                <section class="content">

                   
                    <!-- Main row -->
                    <div class="row">
                        <!-- Left col -->
                        <div class="col-sm-3">
							<label style="padding-left: 20px;">Manufacturer Image</label>
							<form>
								<div id="queue"></div>
								<input id="manufacturer_image" name="manufacturer_image" type="file" multiple="true">
							</form>

						</div>
						<div class="col-sm-6" id="manu_upload">
							<img alt="Manufacturer Image" class="img-thumbnail" src="<?php echo base_url(); ?>uploads/Manufacturer/small/<?php echo $manu_image; ?>" style="height:100px; width:130px;">
							
						</div>
                        <section class="col-sm-12">
                            <!-- Custom tabs (Charts with tabs)-->
							<?php echo form_open_multipart('admin/manufacturers/form/'.$id); ?>
							<input type="hidden" value="<?php echo $manu_image; ?>" name="manu_add_image" id="manu_add_image">

                            <div class="nav-tabs-custom">
							     
                                <!-- Tabs within a box -->
									
											<div class="tab-content">
												<div class="tab-pane fade in active" id="description_tab">
													<fieldset>
														<label for="cat_name">Name</label>
														<?php
														$data	= array('name'=>'name', 'value'=>set_value('name', $name), 'class'=>'form-control');
														echo form_input($data);
														?>
														<label for="cat_enabled">Enabled </label>
														<?php echo form_dropdown('enabled', array('0' => 'Disabled', '1' =>'Enabled'), set_value('enabled',$enabled),'class=form-control'); ?>	
													</fieldset>
												</div>

											</div>

									
									<div class="pull-left">
									<button type="submit" class="btn btn-primary" style="margin:10px 0px 10px 10px;">Save</button>
									</div>
									<div class="pull-right">

										<a class="btn btn-primary"  style="margin:10px 0px 10px 10px;" href="<?php echo site_url('admin/manufacturers/'); ?>"><i class="fa fa-eye"></i> view Manufacturers</a>
								   </div>
								
                            </div><!-- /.nav-tabs-custom -->
                            </form>							
                        </section><!-- /.Left col -->
                       
					</div><!-- /.row (main row) -->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
<script type="text/javascript">
$(document).ready(function(){
	
		$('#manufacturer_image').uploadify({
					
			'formData'     : {
				'flag'      : 'manufacturer_image',
				  'tId'      : '<?php echo $id ?>'
			},
			'onSelect' : function(file) {
				$('#manu_upload').html('<img class="img-circle img-responsive" src="<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/uploadify/ajax-loader1.gif">');
			 },
			'buttonImage' : '<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/uploadify/browse-btn.png',
			'buttonText' : 'Add Manufacturer Image..',
			'multi': false,
			'swf'      : '<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/uploadify/uploadify.swf',
			'uploader' : '<?php echo base_url(); ?>admin/manufacturers/upload_manu_img',
			'onUploadSuccess': function (file, data, response) {
				
					var extension = file.name.replace(/^.*\./, '');
					var a=$.parseJSON(data);
					var imgName=a.imagename;
					
					$('#manu_upload').html('<img alt="Manufacturer Image" class="img-thumbnail" src="<?php echo base_url(); ?>uploads/Manufacturer/small/'+imgName+'" style="height:100px; width:130px;">');
					$("#manu_add_image").val(imgName);
				
				}
			
		});
});
</script>
    
<?php
$this->load->view('admin/adminFooter');
?>
 <script type="text/javascript">
$('form').submit(function() {
	$('.btn').attr('disabled', true).addClass('disabled');
});
</script>  
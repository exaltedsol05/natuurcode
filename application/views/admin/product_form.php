<?php $this->load->view('admin/adminHeader');
      $this->load->view('admin/adminLeftSidebar'); ?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
			Product Form
			<small>Control panel</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Product Form</li>
			</ol>
		</section>
		<section class="content">
		<?php $this->load->view('admin/adminError');?>
<?php $GLOBALS['option_value_count'] = 0;?>
<style type="text/css">
	.sortable { list-style-type: none; margin: 0; padding: 0; width: 100%; }
	.sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; height: 18px; }
	.sortable li>span { position: absolute; margin-left: -1.3em; margin-top:.4em; }
</style>

<script type="text/javascript">
//<![CDATA[

$(document).on('click',".delet_tier",function(){
	if(confirm('Are you sure you want to remove this Tier?')){
		row = $(this).attr('delete_tier');
		$("#"+row).remove();
	}
});

$(document).ready(function() {
	$(".sortable").sortable();
	$(".sortable > span").disableSelection();
	//if the image already exists (phpcheck) enable the selector

	<?php if($id) : ?>
	//options related
	var ct	= $('#option_list').children().size();
	// set initial count
	option_count = <?php echo count($product_options); ?>;
	<?php endif; ?>

	photos_sortable();
});

function isNumeric(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}



function add_product_image(data)
{

	p	= data.split('.');

	var photo = '<?php add_image("'+p[0]+'", "'+p[0]+'.'+p[1]+'", '', '', '', base_url('uploads/images/thumbnails'));?>';
	$('#gc_photos').append(photo);
	$('#gc_photos').sortable('destroy');
	photos_sortable();
}

function remove_image(img)
{
	if(confirm('Are you sure you want to remove this image?'))
	{
		var id	= img.attr('rel')
		$('#gc_photo_'+id).remove();
	}
}

function photos_sortable()
{
	$('#gc_photos').sortable({
		handle : '.gc_thumbnail',
		items: '.gc_photo',
		axis: 'y',
		scroll: true
	});
}

function remove_option(id)
{
	if(confirm('Are you sure you want to remove this option?'))
	{
		$('#option-'+id).remove();
	}
}

//]]>
</script>


<?php echo form_open('admin/products/form/'.$id , array('class' => 'save_form')); ?>
<div class="row">
	<div class="col-sm-12">
	     <div class="box box-primary">
			<div class="box-header">
			</div>
			<div class="box-body">
			<div class="tabbable">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#product_info" data-toggle="tab">Details</a></li>
					
					<li><a href="#product_categories" data-toggle="tab">Categories</a></li>
					<!--<li ><a href="#product_options" data-toggle="tab">Options</a></li>-->
					
					<li><a href="#product_related" data-toggle="tab">Related Products</a></li>
					
					<li><a href="#product_photos"     data-toggle="tab">Images</a></li>
					<li><a href="#product_inventory"     data-toggle="tab">Inventory</a></li>
					<li><a href="#product_more_information"  data-toggle="tab">Product More Information</a></li>
					<li><a href="#product_seo"     data-toggle="tab">SEO Information</a></li>
					
											
					
					
				</ul>
			</div>
			<div class="tab-content">
				<div class="tab-pane active" id="product_info">
				
					
					<div class="form-group">
						   <label>Product Name</label>
							<?php
							$data	= array('placeholder'=>'Name', 'name'=>'name', 'value'=>set_value('name', $name), 'class'=>'form-control input-sm');
							echo form_input($data);
							?>
							<span class="text-danger"><?php echo form_error('name'); ?></span>
						
					</div>
					<div class="form-group">
							<label>Excerpt(Short Description)</label>
							<?php
							$data	= array('name'=>'excerpt', 'value'=>set_value('excerpt', $excerpt), 'class'=>'form-control input-sm', 'rows'=>5);
							echo form_textarea($data);
							?>
						
					</div>
					<div class="form-group">
							<label>Description</label>
							<?php
							$data	= array('name'=>'description', 'class'=>'form-control redactor', 'value'=>set_value('description', $description));
							echo form_textarea($data);
							?>
					</div>
					<div class="form-group">
							<label>How To Use</label>
							<?php
							$data	= array('name'=>'description2', 'class'=>'form-control redactor', 'value'=>set_value('description2', $description2));
							echo form_textarea($data);
							?>
					</div>
					<div class="form-group">
							<label>Ask Natuur</label>
							<?php
							$data	= array('name'=>'description3', 'class'=>'form-control redactor', 'value'=>set_value('description3', $description3));
							echo form_textarea($data);
							?>
					</div>
					<div class="form-group">
							<label>Blog</label>
							<?php
							$data	= array('name'=>'description4', 'class'=>'form-control redactor', 'value'=>set_value('description4', $description4));
							echo form_textarea($data);
							?>
					</div>
					<div class="form-group">
							<label>Video</label>
							<?php
							$data	= array('name'=>'description5', 'class'=>'form-control redactor', 'value'=>set_value('description5', $description5));
							echo form_textarea($data);
							?>
					</div>
					
				</div>
										
				<div class="tab-pane" id="product_categories">
						
						<div class="form-group">
							<?php if(isset($categories[0])):?>
								<label><strong>Select one or more categories</strong></label>
								<table class="table table-striped">
									<thead>
										<tr>
											<th colspan="2">Name</th>
										</tr>
									</thead>
								<?php
								function list_categories($parent_id, $cats, $sub='', $product_categories) {
				
									foreach ($cats[$parent_id] as $cat):?>
									<tr>
										<td><?php echo  $sub.$cat->name; ?></td>
										<td>
											<input type="checkbox" name="categories[]" value="<?php echo $cat->id;?>" <?php echo(in_array($cat->id, $product_categories))?'checked="checked"':'';?>/>
										</td>
									</tr>
									<?php
									if (isset($cats[$cat->id]) && sizeof($cats[$cat->id]) > 0)
									{
										$sub2 = str_replace('&rarr;&nbsp;', '&nbsp;', $sub);
											$sub2 .=  '&nbsp;&nbsp;&nbsp;&rarr;&nbsp;';
										list_categories($cat->id, $cats, $sub2, $product_categories);
									}
									endforeach;
								}
							
							
								list_categories(0, $categories, '', $product_categories);
							
								?>
							</table>
						<?php else:?>
							<div class="alert">There are no available categories.</div>
						<?php endif;?>
						</div>
					
				</div>
				
				<div class="tab-pane" id="product_options">
					<div class="form-group">
						
								<select id="option_options" class="inline form-control pull-left" style="width:200px;">
									<option value="">Select Option Type</option>
									<option value="checklist">Checklist</option>
									<option value="radiolist">Radiolist</option>
									<option value="droplist">Droplist</option>
									<option value="textfield">Textfield</option>
									<option value="textarea">Textarea</option>
								</select>
								<input id="add_option" class="btn btn-primary pull-left" type="button" value="Add Option" style="margin:0px;"/>
							
						
					</div>
					
					<script type="text/javascript">
					
					$( "#add_option" ).click(function(){
						if($('#option_options').val() != '')
						{
							add_option($('#option_options').val());
							$('#option_options').val('');
						}
					});
					
					function add_option(type)
					{
						//increase option_count by 1
						option_count++;
						
						<?php
						$value			= array(array('name'=>'', 'value'=>'', 'weight'=>'', 'price'=>'', 'limit'=>''));
						$js_textfield	= (object)array('name'=>'', 'type'=>'textfield', 'required'=>false, 'values'=>$value);
						$js_textarea	= (object)array('name'=>'', 'type'=>'textarea', 'required'=>false, 'values'=>$value);
						$js_radiolist	= (object)array('name'=>'', 'type'=>'radiolist', 'required'=>false, 'values'=>$value);
						$js_checklist	= (object)array('name'=>'', 'type'=>'checklist', 'required'=>false, 'values'=>$value);
						$js_droplist	= (object)array('name'=>'', 'type'=>'droplist', 'required'=>false, 'values'=>$value);
						?>
						if(type == 'textfield')
						{
							
							
							$('#options_container').append('<?php add_option($js_textfield, "'+option_count+'");?>');
						}
						else if(type == 'textarea')
						{
							$('#options_container').append('<?php add_option($js_textarea, "'+option_count+'");?>');
						}
						else if(type == 'radiolist')
						{
							$('#options_container').append('<?php add_option($js_radiolist, "'+option_count+'");?>');
						}
						else if(type == 'checklist')
						{
							$('#options_container').append('<?php add_option($js_checklist, "'+option_count+'");?>');
						}
						else if(type == 'droplist')
						{
							$('#options_container').append('<?php add_option($js_droplist, "'+option_count+'");?>');
						}
					}
					
					function add_option_value(option)
					{
						
						option_value_count++;
						<?php
						$js_po	= (object)array('type'=>'radiolist');
						$value	= (object)array('name'=>'', 'value'=>'', 'weight'=>'', 'price'=>'');
						?>
						$('#option-items-'+option).append('<?php add_option_value($js_po, "'+option+'", "'+option_value_count+'", $value);?>');
					}
					
					$(document).ready(function(){
						$('body').on('click', '.option_title', function(){
							$($(this).attr('href')).slideToggle();
							return false;
						});
						
						$('body').on('click', '.delete-option-value', function(){
							if(confirm('Are you sure you want to remove this value?'))
							{
								$(this).closest('.option-values-form').remove();
							}
						});
						
						
						
						$('#options_container').sortable({
							axis: "y",
							items:'tr',
							handle:'.handle',
							forceHelperSize: true,
							forcePlaceholderSize: true
						});
						
						$('.option-items').sortable({
							axis: "y",
							handle:'.handle',
							forceHelperSize: true,
							forcePlaceholderSize: true
						});
					});
					</script>
					<style type="text/css">
						.option-form {
							display:none;
							margin-top:10px;
						}
						.option-values-form
						{
							background-color:#fff;
							padding:6px 3px 6px 6px;
							-webkit-border-radius: 3px;
							-moz-border-radius: 3px;
							border-radius: 3px;
							margin-bottom:5px;
							border:1px solid #ddd;
						}
						
						.option-values-form input {
							margin:0px;
						}
						.option-values-form a {
							margin-top:3px;
						}
					</style>
					<div class="form-group">
							<table class="table table-striped"  id="options_container">
								<?php
								$counter	= 0;
								if(!empty($product_options))
								
								{
									foreach($product_options as $po)
									{
										$po	= (object)$po;
										if(empty($po->required)){$po->required = false;}

										add_option($po, $counter);
										$counter++;
									}
								}?>
									
							</table>
						
					</div>
				</div>

				
				
				<div class="tab-pane" id="product_related">
					<div class="row">
						<div class="col-sm-12">
							<label><strong>Search for a related product.</strong></label>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6" style="text-align:center">
							<div class="row">
								<div class="col-sm-12">
									<input class="form-control" type="text" id="related_product_search" />
									<script type="text/javascript">
									$('#related_product_search').keyup(function(){
										$('#related_product_list').html('');
										run_related_product_query();
									});
							
									function run_related_product_query()
									{
										$.post("<?php echo site_url('admin/products/product_autocomplete/');?>", { name: $('#related_product_search').val(), limit:10},
											function(data) {
										
												$('#related_product_list').html('');
										
												$.each(data, function(index, value){
										
													if($('#related_product_'+index).length == 0)
													{
														$('#related_product_list').append('<option id="related_product_item_'+index+'" value="'+index+'">'+value+'</option>');
													}
												});
										
										}, 'json');
									}
									</script>
								</div>
							</div>
							<div class="row">
								<br/>
								<div class="col-sm-12">
									<select class="form-control" id="related_product_list" size="5" style="margin:0px;"></select>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12" style="margin-top:8px;">
									<a href="#" onclick="add_related_product();return false;" class="btn btn-primary pull-right" title="Add Related Product">Add Related Product</a>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<table class="table table-striped" style="margin-top:10px;">
								<tbody id="related_product_items_container">
								<?php
								
								foreach($related_products as $group)
								{
									echo related_items($group->id, $group->name);
								}
								?>
								</tbody>
							</table>
						</div>
					</div>
				
				
				</div>
				
				
				
				<div class="tab-pane" id="product_photos">
				     <br>
				    <p style="font-weight:bold;">Product Upload size: 800*800px</p>
					<div class="row">
						<br>
						<iframe id="iframe_uploader" src="<?php echo site_url('admin/products/product_image_form/'.$id);?>" class="col-sm-12" style="height:75px; border:0px;width:100%;"></iframe>
					</div>
					<div class="row">
						<div class="col-sm-12">
							
							<div id="gc_photos">
								
							<?php
							foreach($images as $photo_id=>$photo_obj)
							{
								if(!empty($photo_obj))
								{
									$photo = (array)$photo_obj;
									add_image($photo_id, $photo['filename'], $photo['alt'], $photo['caption'], isset($photo['primary']));
								}

							}
							?>
							</div>
						</div>
					</div>
				</div>
				 
				 <div class="tab-pane" id="product_inventory">			 			 
					<div class="row">
						<br/>
						
									<div class="col-sm-3">
										<label for="track_stock">Track Stock</label>
										<?php
										$options = array(	 '1'	=> 'Yes'
															,'0'	=> 'No'
															);
										echo form_dropdown('track_stock', $options, set_value('track_stock',$track_stock), 'class="form-control input-sm"');
										?>
									</div>
									<div class="col-sm-3">
										<label for="fixed_quantity">Fixed Quantity</label>
										<?php
										$options = array(	 '0'	=> 'No'
															,'1'	=> 'Yes'
															);
										echo form_dropdown('fixed_quantity', $options, set_value('fixed_quantity','0'), 'class="form-control input-sm" disabled="disabled"');
										?>
									</div>
									<div class="col-sm-3">
										<label for="quantity">Quantity </label>
										<?php
										$data	= array('name'=>'quantity', 'value'=>set_value('quantity', $quantity), 'class'=>'form-control input-sm');
										echo form_input($data);
										?>
									</div>
									<div class="col-sm-3">
										<label for="basket">Basket </label>
										<?php
										$options = array(
												'0'   	=> 'Normal',
												'1'     => 'Basket',
										);

										$shirts_on_sale = array('small', 'large');
										echo form_dropdown('is_basket', $options, $is_basket, 'class = form-control input-sm');
										?>
									</div>																	
					</div>
					
				 </div>
				 
				<div class="tab-pane" id="product_more_information"><br>
					<div class="col-sm-12">
						<div class="row">
							<div class="col-sm-4">
								<label for="product_status">Product Status</label><?php
								$options = array('0'=>'Disabled','1'	=> 'Enabled');
								echo form_dropdown('enabled', $options, set_value('enabled',$enabled), 'class="form-control input-sm"');?>
							</div>
							<div class="col-sm-4">
								<label for="product_status">Shippable Status</label><?php
								$options = array('1'	=> 'Shippable','0'	=> 'Not Shippable');
								echo form_dropdown('shippable', $options, set_value('shippable',$shippable), 'class="form-control input-sm" id="shippable"');?>
							</div>
							<div class="col-sm-4">
								<label for="product_status">Taxable Status</label><?php
								$options = array('1'=> 'Taxable','0'=> 'Not Taxable');
								echo form_dropdown('taxable', $options, set_value('taxable',$taxable), 'class="form-control input-sm" id="taxable"');?>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-4"><br/>
								<label for="home_product">Feature Product</label><?php
								$options = array('0'=>'Disabled','1'=> 'Enabled');
								echo form_dropdown('home_enabled', $options, set_value('home_enabled',$home_enabled), 'class="form-control input-sm"');?>
							</div>
							<div class="col-sm-4"><br/>
								<label for="sku">SKU</label><?php
								$data	= array('name'=>'sku', 'value'=>set_value('sku', $sku), 'class'=>'form-control input-sm' ,'id'=>'sku');
								echo form_input($data);?>
								<span class="text-danger"><?php echo form_error('sku'); ?></span>
							</div>
							
							
							<div class="col-sm-2">
								 <br/>
								<label for="price">Price</label>
								<?php
								$data	= array('name'=>'price', 'value'=>set_value('price', $price), 'class'=>'form-control input-sm','id'=>'price');
								echo form_input($data);?>
								<span class="text-danger"><?php echo form_error('price'); ?></span>
							</div>
							
							<div class="col-sm-2">
								<br/>
								<label for="saleprice">Sale Price</label>
								<?php
								$data	= array('name'=>'saleprice', 'value'=>set_value('saleprice', $saleprice), 'class'=>'form-control input-sm','id'=>'saleprice');
								echo form_input($data);?>
								
							</div>
							
							
							<div class="col-sm-2"><br/>
								<label for="weight">Weight</label><?php
								$data	= array('name'=>'weight', 'value'=>set_value('weight', $weight), 'class'=>'form-control input-sm','id'=>'weight');
								echo form_input($data);?>
								<span class="text-danger"><?php echo form_error('weight'); ?></span>
							</div>
														<div class="col-sm-2">								<br/>								<label for="saleprice">Hot Deals Start Date</label>								<?php								$data	= array('name'=>'hotdeals_from', 'value'=>set_value('hotdeals_from', $hotdeals_from), 'class'=>'form-control input-sm datepicker','id'=>'hotdeals_from');								echo form_input($data);?>															</div>							<div class="col-sm-2">								<br/>								<label for="saleprice">Hot Deals End Date</label>								<?php								$data	= array('name'=>'hotdeals_to', 'value'=>set_value('hotdeals_to', $hotdeals_to), 'class'=>'form-control input-sm datepicker','id'=>'hotdeals_to');								echo form_input($data);?>															</div>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="product_seo">
					<div class="row">
						<div class="col-sm-12">
								<div style="padding-top:10px;">
									<div class="form-group">
										<label for="slug">URL Keyword</label><?php
										$data	= array('name'=>'slug', 'value'=>set_value('slug', $slug), 'class'=>'form-control');
										echo form_input($data);?>
										<br/>
										<label for="seo_title">Title Tag</label>
										<?php
										$data	= array('name'=>'seo_title', 'value'=>set_value('seo_title', $seo_title), 'class'=>'form-control');
										echo form_input($data);
										?>
										<br/>
										<label for="meta">Meta Tags <i>ex. &lt;meta name="description" content="We sell products that help you" /&gt;</i></label>
										<?php
										$data	= array('name'=>'meta', 'value'=>set_value('meta', html_entity_decode($meta)), 'class'=>'form-control');
										echo form_textarea($data);
										?>
									</div>
								</div>
							</div>
						</div>
					</div>

				 
		
		</div>
			</div>
		
			 <div class="box-footer">
				 <button type="submit" class="btn btn-primary" />Save</button>
			 </div>
		 </div>
	</div>
	
</form>

<?php
function add_image($photo_id, $filename, $alt, $caption, $primary=false)
{

	ob_start();
	?>
	<div class="row gc_photo" id="gc_photo_<?php echo $photo_id;?>" style="background-color:#fff; border-bottom:1px solid #ddd; padding-bottom:20px; margin-bottom:20px;margin-left: 0px;">
		<div class="col-sm-3">
			<input type="hidden" name="images[<?php echo $photo_id;?>][filename]" value="<?php echo $filename;?>"/>
			<img class="gc_thumbnail" src="<?php echo base_url('uploads/images/thumbnails/'.$filename);?>" style="padding:5px; border:1px solid #ddd;"/>
		</div>
		<div class="col-sm-9">
			<div class="row">
				<div class="col-sm-4">
					<input name="images[<?php echo $photo_id;?>][alt]" value="<?php echo $alt;?>" class="span2" placeholder="Alt Tag"/>
				</div>
				<div class="col-sm-4">
					<input type="radio" name="primary_image" value="<?php echo $photo_id;?>" <?php if($primary) echo 'checked="checked"';?>/> Main Image
				</div>
				<div class="col-sm-4">
					<a onclick="return remove_image($(this));" rel="<?php echo $photo_id;?>" class="btn btn-xs btn-danger pull-right" ><i class="fa fa-trash-o"></i> Remove</a>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<label>Caption</label>
					<textarea name="images[<?php echo $photo_id;?>][caption]" class="form-control" rows="2"><?php echo $caption;?></textarea>
				</div>
			</div>
		</div>
	</div>

	<?php
	$stuff = ob_get_contents();

	ob_end_clean();
	
	echo replace_newline($stuff);
}


function add_option($po, $count)
{
	ob_start();
	?>
	<tr id="option-<?php echo $count;?>">
		<td>
			<a class="handle btn btn-xs btn-primary"><i class="fa fa-align-justify"></i></a>
			<strong><a class="option_title" href="#option-form-<?php echo $count;?>"><?php echo $po->type;?> <?php echo (!empty($po->name))?' : '.$po->name:'';?></a></strong>
			<button type="button" class="btn btn-xs btn-danger pull-right" onclick="remove_option(<?php echo $count ?>);"><i class="fa fa-trash-o"></i></button>
			<input type="hidden" name="option[<?php echo $count;?>][type]" value="<?php echo $po->type;?>" />
			<div class="option-form" id="option-form-<?php echo $count;?>">
				<div class="row">
				
					<div class="col-sm-9">
						<input type="text" class="form-control" placeholder="Option Name" name="option[<?php echo $count;?>][name]" value="<?php echo $po->name;?>"/>
					</div>
					
					<div class="col-sm-3" style="text-align:right;">
						<input type="checkbox" name="option[<?php echo $count;?>][required]" value="1" <?php echo ($po->required)?'checked="checked"':'';?>/>Required
					</div>
				</div>
				<?php if($po->type!='textarea' && $po->type!='textfield'):?>
				<div class="row">
					<div class="col-sm-12" style="margin-top:10px;">
						<a class="btn btn-primary" onclick="add_option_value(<?php echo $count;?>);">Add Option Item</a>
					</div>
				</div>
				<?php endif;?>
				<div style="margin-top:10px;">

					<div class="row">
						<?php if($po->type!='textarea' && $po->type!='textfield'):?>
						<div class="col-sm-1">&nbsp;</div>
						<?php endif;?>
						<div class="col-sm-3"><strong>&nbsp;&nbsp;Name</strong></div>
						<div class="col-sm-2"><strong>&nbsp;Value</strong></div>
						<div class="col-sm-2"><strong>&nbsp;Weight</strong></div>
						<div class="col-sm-2"><strong>&nbsp;Price</strong></div>
						<div class="col-sm-2"><strong>&nbsp;<?php echo ($po->type=='textfield')?'Limit':'';?></strong></div>
					</div>
					<div class="option-items" id="option-items-<?php echo $count;?>">
					<?php if($po->values):?>
						<?php
						foreach($po->values as $value)
						{
							$value = (object)$value;
							add_option_value($po, $count, $GLOBALS['option_value_count'], $value);
							$GLOBALS['option_value_count']++;
						}?>
					<?php endif;?>
					</div>
				</div>
			</div>
		</td>
	</tr>
	
	<?php
	$stuff = ob_get_contents();

	ob_end_clean();
	
	echo replace_newline($stuff);
}

function add_option_value($po, $count, $valcount, $value)
{
	ob_start();
	?>
	<div class="option-values-form">
		<div class="row">
			<?php if($po->type!='textarea' && $po->type!='textfield'):?><div class="col-sm-1"><a class="handle btn btn-primary btn-xs" style="float:left;"><i class="fa fa-align-justify"></i></a></div><?php endif;?>
			<div class="col-sm-3"><input type="text" class="form-control" name="option[<?php echo $count;?>][values][<?php echo $valcount ?>][name]" value="<?php echo $value->name ?>" /></div>
			<div class="col-sm-2"><input type="text" class="form-control" name="option[<?php echo $count;?>][values][<?php echo $valcount ?>][value]" value="<?php echo $value->value ?>" /></div>
			<div class="col-sm-2"><input type="text" class="form-control" name="option[<?php echo $count;?>][values][<?php echo $valcount ?>][weight]" value="<?php echo $value->weight ?>" /></div>
			<div class="col-sm-2"><input type="text" class="form-control" name="option[<?php echo $count;?>][values][<?php echo $valcount ?>][price]" value="<?php echo $value->price ?>" /></div>
			<div class="col-sm-2">
			<?php if($po->type=='textfield'):?><input class="form-control" type="text" name="option[<?php echo $count;?>][values][<?php echo $valcount ?>][limit]" value="<?php echo $value->limit ?>" />
			<?php elseif($po->type!='textarea' && $po->type!='textfield'):?>
				<a class="delete-option-value btn btn-danger btn-xs pull-right"><i class="fa fa-trash-o"></i></a>
			<?php endif;?>
			</div>
		</div>
	</div>
	<?php
	$stuff = ob_get_contents();

	ob_end_clean();

	echo replace_newline($stuff);
}
//this makes it easy to use the same code for initial generation of the form as well as javascript additions
function replace_newline($string) {
  return trim((string)str_replace(array("\r", "\r\n", "\n", "\t"), ' ', $string));
}
?>
<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/redactor-plugins-master/fontcolor.js" type="text/javascript"></script>
<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>plugins/redactor-plugins-master/video.js" type="text/javascript"></script>

<script type="text/javascript">

$(document).ready(function(){
$('.datepicker').datepicker({startDate: "-1d",format: 'yyyy-mm-dd',todayHighlight: true,autoclose:true});	
    $('.redactor').redactor({
			plugins: ['fontcolor'],
			buttons: [
				'bold',
				'italic',
				'underline',
				'formatting',
				'image',
				'link',
				'unorderedlist',
				'orderedlist',
				'alignment',
				'table',
				'video',
				'|',
				'html'
			],
            minHeight: 200,
            imageUpload: '<?php echo base_url(); ?>admin/wysiwyg/upload_image',
            fileUpload: '<?php echo base_url(); ?>admin/wysiwyg/upload_file',
            imageGetJson: '<?php echo base_url(); ?>admin/wysiwyg/get_images',
            imageUploadErrorCallback: function(json)
            {
                alert(json.error);
            },
            fileUploadErrorCallback: function(json)
            {
                alert(json.error);
            }
      });

});

//<![CDATA[
var option_count		= <?php echo $counter?>;
var option_value_count	= <?php echo $GLOBALS['option_value_count'];?>

function add_related_product()
{
	//if the related product is not already a related product, add it
	if($('#related_product_'+$('#related_product_list').val()).length == 0 && $('#related_product_list').val() != null)
	{
		<?php $new_item	 = str_replace(array("\n", "\t", "\r"),'',related_items("'+$('#related_product_list').val()+'", "'+$('#related_product_item_'+$('#related_product_list').val()).html()+'"));?>
		var related_product = '<?php echo $new_item;?>';
		$('#related_product_items_container').append(related_product);
		run_related_product_query();
	}
	else
	{
		if($('#related_product_list').val() == null)
		{
			alert('Please select a product to add first.');
		}
		else
		{
			alert('This product is already related.');
		}
	}
}

function remove_related_product(id)
{
	if(confirm('Are you sure you want to remove this related item?'))
	{
		$('#related_product_'+id).remove();
		run_related_product_query();
	}
}

function photos_sortable()
{
	$('#gc_photos').sortable({	
		handle : '.gc_thumbnail',
		items: '.gc_photo',
		axis: 'y',
		scroll: true
	});
}

//]]>
</script>
<?php
	function related_items($id, $name) {
		return '
				<tr id="related_product_'.$id.'">
					<td>
						<input type="hidden" name="related_products[]" value="'.$id.'"/>
						'.$name.'</td>
					<td>
						<a class="btn btn-danger pull-right btn-xs" href="#" onclick="remove_related_product('.$id.'); return false;"><i class="fa fa-trash-o"></i> Remove</a>
					</td>
				</tr>
			';
	}
?>
</section>
</div>
<?php
$this->load->view('admin/adminFooter');
?>
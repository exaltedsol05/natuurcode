<?php $this->load->view('admin/adminHeader'); ?>
<?php $this->load->view('admin/adminLeftSidebar');?>
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                       Gift Card Settings
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Gift Card Settings</li>
                    </ol>
                </section>
				<?php $this->load->view('admin/adminError');?>
					
                <!-- Main content -->
                <section class="content">
					<?php echo form_open('admin/giftcards/settings'); ?>


								<label for="predefined_card_amounts">Predefined Card Amounts</label>
								<?php 
								$data	= array('name'=>'predefined_card_amounts', 'value'=>set_value('predefined_card_amounts', $predefined_card_amounts), 'class'=>'form-control');
								echo form_input($data);
								 ?>
								<span class="help-inline">(ex. 10,20,30)</span>
								
								<label class="checkbox">
								<?php
								$data	= array('name'=>'allow_custom_amount', 'value'=>'1', 'checked'=>(bool)$allow_custom_amount);
								echo form_checkbox($data);
								?>Allow Custom Amounts</label>
								
						<div class="form-actions">
							<input class="btn btn-primary" type="submit" value="save"/>
						</div>
					</form>
				</section><!-- /.content -->
			</aside><!-- /.right-side -->
    
<?php $this->load->view('admin/adminFooter');?>
 
<?php $this->load->view('admin/adminHeader');
$this->load->view('admin/adminLeftSidebar'); ?>
<style>
.list{
    list-style: none;
}
</style>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
		<?php echo $page;?>
			<small>Admin Access Level</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#"> Admin Access Level</a></li>
			<li class="active"> <?php echo $page;?></li>
		</ol>
	</section>
		<?php $this->load->view('admin/adminError');?>
	<!-- Main content -->
	<section class="content">
	<?php echo form_open('admin/home/admin_form/'.$id); 
		$arrmenuSetting=json_decode($menuSetting);
	?>

		<label>First Name</label>
		<?php
		$data	= array('name'=>'firstname', 'value'=>set_value('firstname', $firstname),'class'=>'form-control input-xlarge');
		echo form_input($data);
?>

	<label>Last Name</label>
	<?php
	$data	= array('name'=>'lastname', 'value'=>set_value('lastname', $lastname) ,'class'=>'form-control input-xlarge');
	echo form_input($data);
	?>

	<label>Username</label>
	<?php
	$data	= array('name'=>'username', 'value'=>set_value('username', $username),'class'=>'form-control input-xlarge');
	echo form_input($data);
	?>

	<label>Email</label>
	<?php
	$data	= array('name'=>'email', 'value'=>set_value('email', $email),'class'=>'form-control input-xlarge');
	echo form_input($data);
	?>

	<label>Access</label>
	<?php
	$options = array(	'Admin'		=> 'Admin',
	'Staff'	=> 'Staff'
	);
	echo form_dropdown('access', $options, set_value('phone', $access),'class="form-control input-xlarge"');
	?>
	<p><label>Page Access</label></p>
<div class="row">
<div class="col-sm-4">
	<ul class="list">
		<li><input type="checkbox" name="menuSetting[]" value="Bulk_Discount_Top" <?php if(in_array('Bulk_Discount_Top',$arrmenuSetting)){ echo "checked";}?>> Bulk_Discount 				
		<ul class="list">
			<li><input type="checkbox" name="menuSetting[]" value="Brand_Discount" <?php if(in_array('Brand_Discount',$arrmenuSetting)){ echo "checked";}?>> Brand_Discount </li>
			<li><input type="checkbox" name="menuSetting[]" value="Category_Discount" <?php if(in_array('Category_Discount',$arrmenuSetting)){ echo "checked";}?>> Category_Discount </li>
			<li><input type="checkbox" name="menuSetting[]" value="Bulk_Discount_Setting" <?php if(in_array('Bulk_Discount_Setting',$arrmenuSetting)){ echo "checked";}?>> Bulk Discount Setting </li>						
		</ul>				
	</li>
	</ul>
	</div>
<div class="col-sm-4">
<ul class="list">
<li><input type="checkbox" name="menuSetting[]" value="Catalog_Top" <?php if(in_array('Catalog_Top',$arrmenuSetting)){ echo "checked";}?>> Catalog_Top 
<ul class="list">
<li><input type="checkbox" name="menuSetting[]" value="Categories" <?php if(in_array('Categories',$arrmenuSetting)){ echo "checked";}?>> Categories </li>
<li><input type="checkbox" name="menuSetting[]" value="Products" <?php if(in_array('Products',$arrmenuSetting)){ echo "checked";}?>> Products </li>
<li><input type="checkbox" name="menuSetting[]" value="archive_products"  <?php if(in_array('archive_products',$arrmenuSetting)){ echo "checked";}?>> Archive Products </li>
<li><input type="checkbox" name="menuSetting[]" value="zero_products"  <?php if(in_array('zero_products',$arrmenuSetting)){ echo "checked";}?>> Zero Quantity Products </li>
<li><input type="checkbox" name="menuSetting[]" value="feature_products"  <?php if(in_array('feature_products',$arrmenuSetting)){ echo "checked";}?>> Feature Products </li>
<li><input type="checkbox" name="menuSetting[]" value="Shop_By_Brand" <?php if(in_array('Shop_By_Brand',$arrmenuSetting)){ echo "checked";}?>> Shop_By_Brand </li>
<li><input type="checkbox" name="menuSetting[]" value="Special_Banners" <?php if(in_array('Special_Banners',$arrmenuSetting)){ echo "checked";}?>> Special_Banners </li>

</ul>
</li>
</ul>
</div>

<div class="col-sm-4">
<ul class="list">
<li class="list"><input type="checkbox" name="menuSetting[]" value="Sales_Top" <?php if(in_array('Sales_Top',$arrmenuSetting)){ echo "checked";}?>> Sales_Top
<ul class="list">
<li><input type="checkbox" name="menuSetting[]" value="Orders" <?php if(in_array('Orders',$arrmenuSetting)){ echo "checked";}?>> Orders </li>
<li><input type="checkbox" name="menuSetting[]" value="Customers" <?php if(in_array('Customers',$arrmenuSetting)){ echo "checked";}?>> Customers </li>
<li><input type="checkbox" name="menuSetting[]" value="Customers_group" <?php if(in_array('Customers',$arrmenuSetting)){ echo "checked";}?>> Customers Groups</li>
<li><input type="checkbox" name="menuSetting[]" value="Reports" <?php if(in_array('Reports',$arrmenuSetting)){ echo "checked";}?>> Reports </li>
<li><input type="checkbox" name="menuSetting[]" value="Coupons" <?php if(in_array('Coupons',$arrmenuSetting)){ echo "checked";}?>> Coupons </li>
<li><input type="checkbox" name="menuSetting[]" value="Giftcards" <?php if(in_array('Giftcards',$arrmenuSetting)){ echo "checked";}?>> Giftcards </li>
</ul>
</li>
</ul>
</div>
</div>
<div class="row">
<div class="col-sm-4">
<ul class="list">
<li><input type="checkbox" name="menuSetting[]" value="Testimonial_Top" <?php if(in_array('Testimonial_Top',$arrmenuSetting)){ echo "checked";}?>> Testimonial_Top</li>
</ul>
</div>

<div class="col-sm-4">
<ul class="list">
<li><input type="checkbox" name="menuSetting[]" value="Reviews_Top" <?php if(in_array('Reviews_Top',$arrmenuSetting)){ echo "checked";}?>> Reviews_Top</li>
</ul>
</div>


</div>
<div class="row">
<div class="col-sm-4">
<ul class="list">
<li><input type="checkbox" name="menuSetting[]" value="Content_Top"  <?php if(in_array('Content_Top',$arrmenuSetting)){ echo "checked";}?>> Content_Top
<ul class="list">
<li><input type="checkbox" name="menuSetting[]" value="Banners" <?php if(in_array('Banners',$arrmenuSetting)){ echo "checked";}?>> Banners </li>
<li><input type="checkbox" name="menuSetting[]" value="Pages" <?php if(in_array('Pages',$arrmenuSetting)){ echo "checked";}?>> Pages </li>
</ul>

</li>
</ul>
</div>

<div class="col-sm-4">
<ul class="list">
<li><input type="checkbox" name="menuSetting[]" value="Product_Request" <?php if(in_array('Product_Request',$arrmenuSetting)){ echo "checked";}?>> Product_Request</li>
</ul>
</div>
<div class="col-sm-4">
<ul class="list">
<li><input type="checkbox" name="menuSetting[]" value="Administrative_Top" <?php if(in_array('Administrative_Top',$arrmenuSetting)){ echo "checked";}?>> Administrative_Top
<ul class="list">
<li><input type="checkbox" name="menuSetting[]" value="Server_Configuration" <?php if(in_array('Server_Configuration',$arrmenuSetting)){ echo "checked";}?>> Server_Configuration </li>
<li><input type="checkbox" name="menuSetting[]" value="Messages" <?php if(in_array('Messages',$arrmenuSetting)){ echo "checked";}?>> Messages </li>
<li><input type="checkbox" name="menuSetting[]" value="Administrators" <?php if(in_array('Administrators',$arrmenuSetting)){ echo "checked";}?>> Administrators </li>
</ul>

</li>
</ul>
</div>
</div>

<label>Password</label>
<?php
$data	= array('name'=>'password','class'=>'form-control input-xlarge');
echo form_password($data);
?>

<label>Confirm Password</label>
<?php
$data	= array('name'=>'confirm','class'=>'form-control input-xlarge');
echo form_password($data);
?>

<div class="form-actions">
<input class="btn btn-primary" type="submit" value="Save"/>
</div>

</form>
<script type="text/javascript">
$('form').submit(function() {
$('.btn').attr('disabled', true).addClass('disabled');
});
</script>


</section><!-- /.content -->
</div><!-- /.right-side -->
<?php $this->load->view('admin/adminFooter'); ?>
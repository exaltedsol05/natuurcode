<?php $this->load->view('vwHeader');?>
<style>#showMenu{display:none;}</style>
<script>
$(document).ready(function(){
	$(".change_password").click(function(){
    $("#change_password").slideToggle(300);
    });
	$('.delete_address').click(function(){
		if($('.delete_address').length > 1)
		{
			if(confirm('Are you sure you want to delete this address?'))
			{
				$.post("<?php echo site_url('secure/delete_address');?>", { id: $(this).attr('rel') },
					function(data){
						$('#address_'+data).remove();
						$('#address_list .my_account_address').removeClass('address_bg');
						$('#address_list .my_account_address:even').addClass('address_bg');
					});
			}
		}
		else
		{
			alert('You Must leave at least 1 address in the Address Manager.');
		}	
	});
});


function set_default(address_id, type)
{
	$.post('<?php echo site_url('secure/set_default_address') ?>/',{id:address_id, type:type});
}


</script>
<link href="<?php echo HTTP_UPLOADIFY_PATH; ?>uploadify.css" rel="stylesheet" type="text/css" media="screen">
<script src="<?php echo HTTP_UPLOADIFY_PATH; ?>jquery.uploadify.min.js"></script>

<?php
$company	= array('id'=>'company', 'class'=>'form-control', 'name'=>'company', 'value'=> set_value('company', $customer['company']));
$first		= array('id'=>'firstname', 'class'=>'form-control', 'name'=>'firstname', 'value'=> set_value('firstname', $customer['firstname']));
$last		= array('id'=>'lastname', 'class'=>'form-control', 'name'=>'lastname', 'value'=> set_value('lastname', $customer['lastname']));
$email		= array('id'=>'email', 'class'=>'form-control', 'name'=>'email', 'value'=> set_value('email', $customer['email']));
$phone		= array('id'=>'phone', 'class'=>'form-control', 'name'=>'phone', 'value'=> set_value('phone', $customer['phone']));

$password	= array('id'=>'password', 'class'=>'form-control', 'name'=>'password', 'value'=>'');
$confirm	= array('id'=>'confirm', 'class'=>'form-control', 'name'=>'confirm', 'value'=>'');
?>
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner myAccount_breadcum">
			<ul class="list-inline list-unstyled">
				<li><a href="<?php echo base_url();?>">Home</a></li>
				<li><a href="#" class="active">My Account</a></li>
			</ul>
		</div>
	</div>
</div>
<div class="body-content outer-top-xs">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php if ($this->session->flashdata('message')):?>
				<div class="alert alert-info">
					<a class="close" data-dismiss="alert">×</a>
					<?php echo $this->session->flashdata('message');?>
				</div>
				<?php endif;?>
			
				<?php if ($this->session->flashdata('error')):?>
				<div class="alert alert-danger">
					<a class="close" data-dismiss="alert">×</a>
					<?php echo $this->session->flashdata('error');?>
				</div>
				<?php endif;?>
			
				<?php if (!empty($error)):?>
				<div class="alert alert-danger">
					<a class="close" data-dismiss="alert">×</a>
					<?php echo $error;?>
				</div>
				<?php endif;?>
			</div>
			<div class="col-md-3 myaccount_left">
				<?php $this->load->view('user_account_sidebar');?>
			</div>			
			<div class="col-md-9 myaccount_right">
				<div class="row">
					<div class="col-md-12 myDashboard">
						<h3>MY DASHBOARD</h3><?php 
							$name = "";
							if(!empty($customer['firstname'])){
								$name =  $customer['firstname'].' '.$customer['lastname'];
							}else{
								$name = $customer['email'];
							}?>								
						<p class="myaccount_email"><b>Hello, <?php echo $name;?>!</b></p>
						<p>From your My Account Dashboard you have the ability to view a snapshot of your recent account activity and update your account information. Select a link below to view or edit information.</p>
					</div>
					<div class="col-md-12" id="imgProfile_upload">
						<?php 
							$photo = "";
							if($this->session->userdata('social_login_image'))
							{
								$photo = $this->session->userdata('social_login_image');
							}
							else
							{
								if($customer['image'] != '' && file_exists('uploads/customer_image/small/'.$customer['image']))
								{
									$photo =  base_url().'uploads/customer_image/small/'.$customer['image'];
								}else{
									$photo = base_url().'uploads/customer_image/default_user.jpg';
								}
							}
						?>						
						<img src="<?php echo $photo; ?>" class="img-square img-responsive">
						<div class="updated_image">
							<label>Update Image</label>
							<form>
								<div id="queue"></div>
								<input id="profileimage_upload" name="profileimage_upload" type="file" multiple="true">
							</form>
						</div>
					</div>
				</div> 
				<hr>
				<div class="row">
					<div class="col-sm-8">
						<h5><b>CONTACT INFORMATION</b>&nbsp;&nbsp;<a class="btn green_btn" href="<?php echo base_url().'secure/my_information'; ?>">EDIT</a></h5>
						<?php
							if(!empty($customer['firstname'])){?>
								<p><i><?php echo $customer['firstname'].' '.$customer['lastname'];?></i></p><?php
							}?>
						<p><i><?php echo $customer['email'];?></i></p>
						<p><i><?php echo $customer['phone'];?></i></p>
						<a style="display:none;" href="javascript:;" class="change_password"><i>Change Password</i></a>
					</div>
					<div class="col-sm-4" style="display:none;">
						<h5><b>NEWSLETTERS</b></h5>
						<p><a href="#"><?php if((bool)$customer['email_subscribe']) { echo "EDIT"; }else { echo "ADD";} ?></a></p>
						<?php if(!(bool)$customer['email_subscribe']) { ?>
							<p><i>You are currently not subscribed to any newsletter.</i></p>
						<?php  } ?>                  
					</div>
					<div class="col-sm-12" id="change_password" style="display:none;">
						<div class="my-account-box">
							<?php echo form_open('secure/my_account'); ?>
							<fieldset>
								<h2>Change Password</h2>
								
								<div class="row" style="display:none;">
									<div class="col-sm-12">
										<label for="company">Company</label>
										<?php echo form_input($company);?>
									</div>
								</div>
								<div class="row" style="display:none;">	
									<div class="col-sm-6">
										<label for="account_firstname">First Name</label>
										<?php echo form_input($first);?>
									</div>
								
									<div class="col-sm-6">
										<label for="account_lastname">Last Name</label>
										<?php echo form_input($last);?>
									</div>
								</div>
							
								<div class="row" style="display:none;">
									<div class="col-sm-6">
										<label for="account_email">Email</label>
										<?php echo form_input($email);?>
									</div>
								
									<div class="col-sm-6">
										<label for="account_phone">Phone</label>
										<?php echo form_input($phone);?>
									</div>
								</div>
							
								<div class="row" style="display:none;">
									<div class="col-sm-12" style="padding-left: 35px;">
										<label class="checkbox">
											<input type="checkbox" name="email_subscribe" value="1" <?php if((bool)$customer['email_subscribe']) { ?> checked="checked" <?php } ?>/> Subscribe to our email list
										</label>
									</div>
								</div>
							
								<div class="row" style="display:none;">
									<div class="col-sm-12">
										<div style="margin:30px 0px 10px; text-align:center;">
											<strong>If you do not wish to change your<br/>password, leave both fields blank.</strong>
										</div>
									</div>
								</div>
							
								<div class="row">	
									<div class="col-sm-6">
										<label for="account_password">Password</label>
										<?php echo form_password($password);?>
									</div>

									<div class="col-sm-6">
										<label for="account_confirm">Confirm Password</label>
										<?php echo form_password($confirm);?>
									</div>
								</div>
								<br>
								<input type="submit" value="Submit" class="btn btn-primary" />

							</fieldset>
							</form>
						</div>
					</div>
				</div>
				<hr>
				
				<div class="row">
					<div class="col-sm-8">
						<h4>Address Manager</h4>
					</div>
					<div class="col-sm-4 text-right">
						<input type="button" class="btn btn-sm edit_address btn-primary" rel="0" value="Add Address"/>
					</div>
				</div>
				<hr> 
				<div class="row">
					<div class="col-sm-12" id='address_list'>
					<?php if(count($addresses) > 0):?>
						<table class="table table-bordered table-striped">
					<?php
					$c = 1;
						foreach($addresses as $a):?>
							<tr id="address_<?php echo $a['id'];?>">
								<td>
									<?php
									$b	= $a['field_data'];
									echo format_address($b, true);
									?>
								</td>
								<td>
									<div class="row">
										<div class="col-sm-12">
											<div class="btn-group pull-right myBtn">
												<input type="button" class="btn btn-sm edit_address green_btn" rel="<?php echo $a['id'];?>" value="Edit" />
												<input type="button" class="btn btn-sm btn-danger delete_address" rel="<?php echo $a['id'];?>" value="Delete" />
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="billing_shipping_section">
												<span><input type="radio" id="default_billing<?php echo $a['id'] ?>" name="bill_chk" onclick="set_default(<?php echo $a['id'] ?>, 'bill')" <?php if($customer['default_billing_address']==$a['id']) echo 'checked="checked"'?> /> <label for="default_billing<?php echo $a['id'] ?>">Default Billing</label></span><span><input type="radio" id="default_shipping<?php echo $a['id'] ?>" name="ship_chk" onclick="set_default(<?php echo $a['id'] ?>,'ship')" <?php if($customer['default_shipping_address']==$a['id']) echo 'checked="checked"'?>/> <label for="default_shipping<?php echo $a['id'] ?>">Default Shipping</label></span>
											</div>
										</div>
									</div>
								</td>
							</tr>
						<?php endforeach;?>
						</table>
					<?php else:?>
					<p>No address in your account.</p>
					<?php endif;?>
					</div>
				</div>
			</div>
		</div> 
	</div>
</div>
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog default_form">
		<!-- Modal content-->
		<div class="modal-content" id="address-form-container">Try this out...</div>
	</div>
</div>

<link href="<?php echo HTTP_UPLOADIFY_PATH; ?>uploadify.css" rel="stylesheet" type="text/css" media="screen">
<script src="<?php echo HTTP_UPLOADIFY_PATH; ?>jquery.uploadify.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#profileimage_upload').uploadify({
				
		'formData'     : {
			'flag'      : 'profileimage_upload',
			'tId'      : "<?php echo $customer['id']; ?>"
		},
		'onSelect' : function(file) {
							
			$('#imgProfile_upload').html('<img class="img-circle img-responsive" src="<?php echo HTTP_UPLOADIFY_PATH; ?>ajax-loader1.gif">');
		 },
		'buttonImage' : '<?php echo HTTP_UPLOADIFY_PATH; ?>browse-btn.png',
		'buttonText' : 'Add Event Image..',
		'multi': false,
		'swf'      : '<?php echo HTTP_UPLOADIFY_PATH; ?>uploadify.swf',
		'uploader' : '<?php echo base_url(); ?>secure/uploadCustomerImg',
		'onUploadSuccess': function (file, data, response) {
			
			var extension = file.name.replace(/^.*\./, '');
			var a=$.parseJSON(data);
			var imgName=a.imagename;
			//alert(imgName);
			$('#imgProfile_upload').html('<img alt="profile image" style="width:180px; height:120px;" class="img-square" src="<?php echo base_url(); ?>uploads/customer_image/small/'+imgName+'">');
		}
		
	});
});
</script>


<?php 
$this->load->view('vwFooter');
?>
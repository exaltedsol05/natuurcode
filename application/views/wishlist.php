<?php $this->load->view('vwHeader');?>
<style>
.span1 {cursor:pointer; }

.minus, .plus{
	width: 45px;
    height: 51px;
    background: #f2f2f2;
    /* border-radius: 4px; */
    font-size: 16px;
    font-weight: bold;
    padding: 13px 0px 0px 0px;
    border: 1px solid #ddd;
    display: inline-block;
    vertical-align: middle;
    text-align: center;
}
.quantity1{
height: 52px;
    width: 76px;
    text-align: center;
    font-size: 26px;
    border: 1px solid #ddd;
    /* border-radius: 4px; */
    display: inline-block;
    vertical-align: middle;
}			
</style>
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner myAccount_breadcum">
			<ul class="list-inline list-unstyled">
				<li><a href="<?php echo base_url();?>">Home</a></li>
				<li><a href="#" class="active">My Account</a></li>
			</ul>
		</div>
	</div>
</div>
<div class="body-content outer-top-xs">
	<div class="container">
		<div class="row">
			<div class="col-md-3 myaccount_left">
				<?php $this->load->view('user_account_sidebar');?>
			</div>			
			<div class="col-md-9 myaccount_right">
				<div class="row">
					<div class="col-md-12 myDashboard">
						<h3>MY WISHLIST</h3>
					</div>
				</div>
				<div class="my-wishlist-page">
					<div class="row">
						<div class="col-md-12 my-wishlist">
						<?php include('checkout/wishlist_summary.php');?>
						</div>		
					</div><!-- /.row -->
				</div>
			</div>
		</div> 
	</div>
</div>
<?php $this->load->view('vwFooter');?>
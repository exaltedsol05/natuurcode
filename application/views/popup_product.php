<div class="closeBtnModal">
	<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>
</div>
<div class="container nopadding_container">
	<div class="product_container_inner">
		<div class="product_details">
			<div class="row">
				<div class="col-lg-6 col-md-6">
				   <div class="product-details-tab" style="text-align: center;">
						<div id="img-1" class="zoomWrapper single-zoom">
							<?php
							$photo  = theme_img('no_picture.png', 'No Image Available');
							$product->images    = array_values($product->images);
							$count = count($product->images);
								if(($count) > 1){
									for($j=0; $j<$count; $j++){	
										if($product->images[$j]->primary){?>	
											
												<a 	data-lightbox="image-1"
												data-title="Gallery" href="<?php echo base_url('uploads/images/medium/'.$photo->filename);?>">
												<img id="zoom1" src="<?php echo base_url('uploads/images/medium/'.$product->images[$j]->filename);?>" data-zoom-image="<?php echo base_url('uploads/images/full/'.$product->images[$j]->filename);?>" /> 
												</a>
											
										<?php	
										}
									}
								}
							?>
						</div>
						<div class="single-zoom-thumb">
							<ul class="s-tab-zoom owl-carousel single-product-active" id="gallery_01">
							<?php
							if(count($product->images) > 1):
							$i=0; 
							foreach($product->images as $image):
							?>
							<li>
							<a href="#" class="elevatezoom-gallery <?php echo ($i==0)?'active':'';?>" data-update="" data-image="<?php echo base_url('uploads/images/medium/'.$image->filename);?>" data-zoom-image="<?php echo base_url('uploads/images/full/'.$image->filename);?>">
								<img src="<?php echo base_url('uploads/images/thumbnails/'.$image->filename);?>"  />
							</a>
							 </li>
							<?php  $i++; endforeach;?>
							<?php endif;?>
								
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6">
					<div class="product_d_right">
					   <?php echo form_open('cart/add_to_cart_ajax', 'class="" id="add-to-cart_'.$product->id.'" name="add-to-cart_'.$prdid.'" accept-charset="utf-8"');?>
						<input type="hidden" name="cartkey" value="<?php echo $this->session->flashdata('cartkey');?>" />
						<input type="hidden" name="id" value="<?php echo $product->id?>"/>
						<input type="hidden" name="rating_val" id="rating_val" value=""/>

							<h1><?php echo $product->name;?>(<?php if(!empty($product->sku)):?><?php echo $product->sku; ?><?php endif;?>)</h1>
							
							<div class="product_rating">
								<ul>
									<?php
							   $all_ret = $this->natuur->product_rating($product -> id);
							   $product_rating = explode("*-*", $all_ret);
							   $avg_rating = explode("*-*", $all_ret);
							   //print_r($product_rating);
							   $star1 	=$product_rating[0];
							   $star2 	=$product_rating[1];
							   $star3 	=$product_rating[2];
							   $star4 	=$product_rating[3];
							   $star5 	=$product_rating[4];
							   $starall 	=$product_rating[5];
							   $avt_rating = (1*($star1) + 2*($star2) +3*($star3) +4*($star4) + 5*($star5))/($starall);
							   $avt_rating = number_format((float)$avt_rating, 1, '.', '');
							   if(empty($avt_rating)){
								$avt_rating = 0;
							   }
							   for($i = 0; $i < $avt_rating; $i++){
								echo '<li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>';
							   }
							   $dact = 5-$avt_rating;
							   for($j = 0; $j < $dact; $j++){
								echo ' <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>';
							   }?>
							   <li class="rc">(<?php echo $starall; ?> Rating)</li>
								   
								</ul>
							</div>
							<div class="product_meta">
							<div class="row">
								<div class="col-sm-3">
									<div class="stock-box">
										<span class="label">Availability :</span>
									</div>
								</div>
								<div class="col-sm-9">
									<div class="stock-box">
									<?php if($product->quantity < 1):?>
										<span class="value">Out of Stock</span>
									<?php else:?>
										<span class="value">In Stock</span>
									<?php endif;?>
									</div>
								</div>
							</div>
							<!-- /.row -->	
							</div>
							<div class="price_box">
								<?php 
									$price_design = $this->natuur->get_natuur_currency_function($product->price,$product->saleprice);
									echo $price_design['price_design'];
								?>
							</div>
							<div class="product_desc">
								<p><?php echo $product->excerpt; ?></p>
							</div>                                    
							<div class="product_variant quantity">
								<label>quantity</label>
							   <div class="number number_quantity"><span class="minus span1" >-</span><input type="text" value="1" id="quantity1" class="quantity1" name="quantity"/><span class="plus span1">+</span></div>
							   <div class="cartWishlist">
									<button class="button add_to_cart_button" type="button" onclick="addToCart(<?php echo $product->id;?>);" title="add to cart"><i class="zmdi zmdi-shopping-cart-plus"></i>Add To Bag</button><?php if($this->Customer_model->is_logged_in(false, false)){?><button class="button wishlist_button" type="button" onclick="add_to_wishlist(<?php echo $product->id;?>);" title="add to wishlist"><i class="fa fa-heart-o" aria-hidden="true"></i>Wishlist</button><?php
									}else{?><button class="button wishlist_button" type="button" title="Wishlist" data-toggle="modal" data-target="#myLoginModal" title="add to wishlist"><i class="fa fa-heart-o" aria-hidden="true"></i>Wishlist</button>
									<?php
									}?>
								</div>
							</div>
						</form>
						<div class="priduct_social" style="display:none;">
							<ul>
								<li><a class="facebook" href="#" title="facebook"><i class="fa fa-facebook"></i> Like</a></li>           
								<li><a class="twitter" href="#" title="twitter"><i class="fa fa-twitter"></i> tweet</a></li>           
								<li><a class="pinterest" href="#" title="pinterest"><i class="fa fa-pinterest"></i> save</a></li>           
								<li><a class="google-plus" href="#" title="google +"><i class="fa fa-google-plus"></i> share</a></li>        
								<li><a class="linkedin" href="#" title="linkedin"><i class="fa fa-linkedin"></i> linked</a></li>        
							</ul>      
						</div>

					</div>
				</div>
			</div>  
		</div>
	</div>
</div>
<?php
   function theme_img($uri, $tag=false)
   {
   	if($tag)
   	{
   		return '<img src="'.theme_url('assets/assets/images/'.$uri).'" alt="'.$tag.'">';
   	}
   	else
   	{
   		return theme_url('assets/assets/images/'.$uri);
   	}
   }
   function theme_url($uri)
   {
   	$CI =& get_instance();
   	return $CI->config->base_url('/'.$uri);
   }
   ?>
 
<script src="<?php echo HTTP_JS_PATH; ?>plugins.js"></script>
<!-- Main JS -->
<script src="<?php echo HTTP_JS_PATH; ?>main.js?v10"></script>
<script src="<?php echo HTTP_JS_PATH; ?>equal_heights.js"></script>
<script src="<?php echo HTTP_JS_PATH; ?>jquery.blockUI.js"></script>
<script src="<?php echo HTTP_JS_PATH; ?>jquery.validate.min.js"></script>
<script src="<?php echo HTTP_JS_PATH; ?>star-rating.min.js"></script>
<script src="<?php echo HTTP_JS_PATH; ?>jquery.cookieMessage.min.js"></script>  
<script>
jQuery(document).ready(function(){
		$("#img1").elevateZoom({ gallery: 'gallery_01', cursor: 'pointer', galleryActiveClass: "active" });
		$("#img1").bind("click", function(e) {
			var ez = $('#img1').data('elevateZoom');
			ez.closeAll();
	   //$.fancybox(ez.getGalleryList());
	   return false;
	   });
	$('.minus').click(function () {
		var $input = $(this).parent().find('input');
		var count = parseInt($input.val()) - 1;
		count = count < 1 ? 1 : count;
		$input.val(count);
		$input.change();
		return false;
	});
	$('.plus').click(function () {
		var $input = $(this).parent().find('input');
		$input.val(parseInt($input.val()) + 1);
		$input.change();
		return false;
	});   
})
</script>  
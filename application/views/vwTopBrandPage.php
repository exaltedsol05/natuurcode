<form method="post" action="<?php echo base_url().'cart/brand';?>" id="brand_form">
<input type="hidden" name="term" id="brand_name" value="" >
<input type="hidden" name="top_brand_id" id="brand_id" value="" >

<div class="yamm-content" style="max-height: 500px;overflow-y: scroll;">
	<div class="row">
		<div class="col-xs-12 col-menu">
			<ul class="links">
				<?php foreach($arrshopByBrand as $brand){ 
					$image = base_url().'uploads/Brand/full/'.$brand->brand_image;
					if(!empty($brand->brand_image)){
				?>
					<li>
						<a href="javascript:void(0);" onclick="myBrandFun('<?php echo $brand->name;?>','<?php echo $brand->id;?>');">
						<span><?php echo $brand->name;?></span><img src="<?php echo $image; ?>" alt="Brand Name" class="img-responsive" style="height:50px;width:120px;padding:10px;border:1px solid #ccc;">
						</a>
					</li>
				<?php } }?> 
			</ul>
		</div>
	</div>
</div>
</form>

<script type="text/javascript">
function myBrandFun(brand_name,brand_id){
	$("#brand_name").val(brand_name);
	$("#brand_id").val(brand_id);
	$("#brand_form").submit();
}
</script>
<?php $this->load->view('vwHeader');?>
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner myAccount_breadcum">
			<ul class="list-inline list-unstyled">
				<li><a href="<?php echo base_url();?>">Home</a></li>
				<li><a href="#" class="active">My Account</a></li>
			</ul>
		</div>
	</div>
</div>
<div class="body-content outer-top-xs">
	<div class="container">
		<div class="row">
			<div class="col-md-3 myaccount_left">
				<?php $this->load->view('user_account_sidebar');?>
			</div>			
			<div class="col-md-9 myaccount_right">
				<div class="row">
					<div class="col-md-12 myDashboard">
						<h3>MY DASHBOARD</h3>
					</div>
				</div>
				<div class="row">
					
				</div>
			</div>
		</div> 
	</div>
</div>
<?php 
$this->load->view('vwFooter');
?>
<?php 
foreach($banners as $banner):?>
			<?php
				$image=base_url('uploads/'.$banner->image);	
                $target=false;
				if($banner->new_window)
				{
					$target=' target="_blank"';
				}?>				
			<a href="<?php echo $banner->link;?>" <?php echo $target;?>	>
			<div class="single_slider d-flex align-items-center" data-bgimg="<?php echo $image;?>">
                <div class="container" style="text-align: center;">
                    <div class="row">
                        <div class="col-12">
                            <div class="slider_content slider_c_three">
                                <h1><?php echo $banner->name;?></h1>
                                <!--<p>We’ve been squeezing oranges in Alista for over 20 years, and we are proud.</p>-->
                                <!--<a href="<?php //echo $banner->link;?>" <?php //echo $target;?> >shop now</a>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			</a>	
		<?php endforeach;?>

<?php
$this->load->view('vwHeader');
?>
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="<?php echo base_url();?>">Home</a></li>
				<li><a href="#"><?php echo $page_title;?></a></li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->
<section class="container">
<div class="row">
	<div class="col-sm-12">
		<div class="contact-page">
		<h1 class="text-center"><?php echo $arrContact->title;?></h1>
		<hr>
			<div class="row">
				<div class="col-md-12 contact-map outer-bottom-vs">
					<iframe width="600" height="450" frameborder="0" style="border:0"src="https://www.google.com/maps/embed/v1/place?q=635%2C%20Block%20C%201%2C%20Sector%203%2C%20Gurugram%2C%20Haryana%20122017&key=AIzaSyCs7-DFU-bnracAz0jij9TefFmpBZ9HS2I" allowfullscreen></iframe>
				</div>
				<div class="col-md-9 contact-form">
					<div class="col-md-12 contact-title">
						<h4>Contact Form</h4>
					</div>
					<form class="register-form default_form" role="form" id="contact_form">
						<div class="col-md-4 ">
								<div class="form-group">
								<label class="info-title form_label" for="exampleInputName">Your Name <span>*</span></label>
								<input type="text" name="exampleInputName" class="form-control unicase-form-control text-input" id="exampleInputName" placeholder="">
							  </div>
						</div>
						<div class="col-md-4">
								<div class="form-group">
								<label class="info-title form_label" for="exampleInputEmail">Email Address <span>*</span></label>
								<input type="email" name="exampleInputEmail" class="form-control unicase-form-control text-input" id="exampleInputEmail" placeholder="">
							  </div>
						</div>
						<div class="col-md-12">
								<div class="form-group">
								<label class="info-title form_label" for="exampleInputComments">Your Comments <span>*</span></label>
								<textarea class="form-control unicase-form-control" name="exampleInputComments" id="exampleInputComments"></textarea>
							  </div>							
						</div>
						<div class="col-md-12 outer-bottom-small m-t-20">
							<button id="request" type="submit" class="btn-upper btn checkout-page-button green_btn">Send Message</button>
							<div id="alertt_msg"></div>
						</div>
						
					</form>
				</div>
				<?php echo $arrContact->content; ?>		
			</div><!-- /.contact-page -->
		</div>
    </div>
</div>
</section>
<script>
$(document).ready(function(){
	$("#contact_form").validate({
		rules:{
			exampleInputName:"required",
			exampleInputEmail:"required",
			exampleInputComments:"required"
		},
        submitHandler: function (form){
     		$.ajax({
                type: "POST",
                datatype:"json",
                data: $("#contact_form").serialize(),
                url: "<?php echo base_url(); ?>cms/contact_us_request", 
				beforeSend: function() { 
				      $("#alertt_msg").html('<span style="color:green;">sending....</span>');
				      $("#request").prop('disabled', true); // disable button
				},
                success: function(msg){
                	if(msg){
						$("#alertt_msg").html('<span style="color:green;">request sent sucessfully.</span>');
						$("#request").prop('disabled', false); // enable button

						$('#exampleInputName').val('');
						$('#exampleInputEmail').val('');
						$('#exampleInputComments').val('');
                	}
                }
            });
        }
    });
});
</script>
<?php
$this->load->view('vwFooter');
?>

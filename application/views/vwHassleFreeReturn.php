<?php
$this->load->view('vwHeader');
?>
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="<?php echo base_url();?>">Home</a></li>
				<li><a href="#"><?php echo $page_title;?></a></li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->
<section class="container">
<div class="row">
	<div class="col-sm-12">
		<div style="background-color: #fff;box-shadow: 0 2px 4px 0 rgba(0,0,0,.08);padding: 20px;overflow: hidden;">
			<h1 class="text-center"><?php echo $arrHassleFreeReturn->title;?></h1>
			<hr>
			<div class="abou-page">
				 <div class="row">
					<div class="col-md-8 col-md-offset-2">
					  <div class="single-img-add sidebar-add-slider">
						<div> 
						  
						  <!-- Wrapper for slides -->
						   <!--<div class="" role="listbox" style="overflow:hidden;">
							<div class="item"> 
								<img style="max-height:250px;width:720px;" src="<?php echo base_url().'assets/assets/images/about_us_slide2.jpg'; ?>" alt="slide1"> 
							</div>
						  </div> -->
						</div>
					  </div>
					</div>
					<div class="col-sm-12">
						<?php echo $arrHassleFreeReturn->content; ?>
					</div>
				</div><!-- /.row -->
			</div><!-- /.sigin-in-->
		</div>
    </div>
</div>
</section>
<?php
$this->load->view('vwFooter');
?>
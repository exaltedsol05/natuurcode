<?php $this->load->view('vwHeader');?>

<style type="text/css">
	p {
    font-size: 16px;
}
.container_body a{color: #0056b3!important;}
</style>
<section class="container container_body">
<div class="row">
	<div class="col-sm-12">
		<div class="page-header 1">
			<h1 style="font-size:28px;margin-top:30px;">PRIVACY POLICY:</h1>
		</div>

		<p>Natuur Privacy Policy<br> This Privacy Policy was last changed May, 2021</p>
	</div>
	<div class="col-sm-12">
		<h2 style="font-size: 22px;font-weight: 600;">Overview</h2>
		
		<p>Natuur (a unit of Natuur Manufacturing Pvt Ltd) is the owner of <a href="https://www.natuur.in/">www.natuur.in</a> (“Website”).
		The protection and security of your personal information is one of Natuur’s top priorities. The
		following Privacy Policy explains natuur’s information practices for this Website and subscriber
		/ membership based services (“Services”) including the type of information being collected,
		method of such information collection, use of such personal information that is provided by our
		customers and sharing of such information with third parties.
		</p>

		<p>Natuur respects and is committed to safeguarding your privacy. We do not rent or sell our customer database or newsletter subscriber list to third parties. We do not share customer information in ways different from what is disclosed in this statement. Other than as described in this Policy, we will not give any information about you to others without your express permission. We will respond to your request for access to modify or delete your information within thirty (30) days.
		</p>

		<p>Our Privacy Policy may change from time to time and Natuur reserves the right to change or modify our Privacy Policy at any time with or without notice. To make sure you are aware of any changes, please review this policy periodically.
		</p>

		<p>By using this Website you agree to accept the terms of this Privacy Policy as well as the Website’s Terms of Use. By accessing or using this Website you expressly consent to our use and disclosure of your personal information in any manner described in this Privacy Policy. This Privacy Policy extends to both, users who visit the Website but do not transact business on the Website (“Users”) as well as users who are registered and are authorized by the Website to transact business on the Website (“Members”).
		</p>

		<p>If you have any questions or concerns over the Aloe Veda Privacy Policy, please feel free to contact <a href="mailto:support@natuur.in" title="Support">support@natuur.in</a>
		</p>

		<p>PLEASE READ THE FOLLOWING TERMS OF OUR PRIVACY POLICY</p>

		<p>What information we collect from you</p>
		<p>“Personal Information” refers to any information that identifies or can be used to identify, contact or locate the person, to whom such information pertains including, but not limited to, name, address, phone number, fax number, email address, financial profiles, identification number and credit card information. </p>

		<p>Our Web site uses registration forms in which you give us contact and demographic information (including your name, address, telephone number, CV and email address) so you can place orders, request information and support, and make product suggestions. For certain services, we may also request a credit card number, identification number, national identification number or tax identification number, or other financial information. We will then create an account number for you. We use or may use your contact information (such as email address) from the registration form to send you newsletters and occasional information about our services and partners, inform you if you are the potential winner of a contest or promotion, personalize your experience with Natuur, or investigate cases of suspected fraud or abuse. We use your information to contact you regarding functionality changes to our products, our Web site, new Natuur products and services, and special offers we think you’ll find valuable. If you would rather not receive this information, please see the “Updating Your Information” section below on how to change your preferences.</p>

		<p>To better serve you, we receive and may also store information you present to us on our Web site or give us in any other way via email, telephone or other communications with Natuur’s Customer Service Department. This information is or may be used in the same manner as information directly collected using Natuur’s registration form. We may also use information you provide us via email or telephone to pursue the matter for which you contacted us. Natuur also may obtain information from you in our online forums. This information may not be disclosed except as consistent with Terms of Use. If you contact us for support, we will keep an internal record of what support was given.</p>

		<p>Automatic Information: We automatically collect some information about your computer when you visit <a href="https://www.natuur.in/">www.natuur.in</a>  For example, we will collect your IP address, and information relating to your Web browser software (such as Netscape Navigator or Internet Explorer, Firefox, Google Chrome etc.) and referring Web site. We also may collect information about your online activity, such as pages visited or products viewed. One of our goals in collecting this information is to help customize your user experience. We do not use this information to identify you.</p>

		<p>Cookies and Other Web Technologies: A cookie is a small piece of data that a web server places on a user’s computer. Natuur employs the use of cookies and web beacons to ensure that you do not see the same ad repeatedly, to track marketing promotions, to estimate and report audience size and traffic, and to customize and personalize your experience at <a href="https://www.natuur.in/">www.natuur.in</a> </p>

		<p>When you visit Natuur website, we may assign your computer a “cookie” (a small, unique identifier text file). You can always choose not to receive a cookie file by enabling your Web browser to refuse cookies or to prompt you before accepting a cookie. Please note that if you refuse to accept cookies from Natuur, you may not be able to access many of the tools offered by Natuur. </p>

		<p>Furthermore, Natuur  may, from time to time, use outside media companies to display advertisements on our site. These ads may contain cookies and web beacons, which are collected and tracked by the outside media company. Natuur has no access to or control over these cookies. Most web browsers provide users the ability to limit the use of cookies and web beacons by allowing users to adjust privacy settings. If a user has their cookies disabled, Natuur  will still be accessible, however some sections of the web site will not perform as well as they normally would with cookies enabled.</p>

		<p>Our site also uses cookies to keep track of your shopping cart and receipts. We use cookies to identify you so you don’t need to login each time you visit our site. They are linked to your customer number which is associated with the information in your account.</p>


		<p>Information Collected Through Web Analytics: Natuur uses a Web analytics company to track and generate reports on visitor use of our site. We use the Web analytics company to analyze visitor activity so that we may better understand how visitors make use of Natuur, and so that we may better tailor content to meet our visitors’ needs. This data may include anonymized, individual visitor click behavior. No personally identifiable information is obtained or shared with the Web analytics company. Although our Web analytics company logs the information coming from Natuur on our behalf, we control how that data may and may not be used. Visitors who wish to opt-out of this Web site analysis tool may do so by writing to us.</p>

		<p>In the future, in order to customize your experience with our email newsletters, we may receive a confirmation when you open email from Natuur if your computer supports such capabilities. Clicking on a link in Natuur  newsletter may also be tracked on a per user basis to allow us to present content, which closely matches your interests. Demographic and profile data (such as state, postal code, age, or gender) may be used to customize your experience at our site or in our email newsletters, showing you content and advertising that may best suit your needs. This information is shared with advertisers and other third parties on an aggregate basis only. Individually identifiable user click behavior is never shared with advertisers and partners. </p>

		<p>How we use this information</p>

		<p>Natuur  uses information we collect for the following general purposes: to measure interest in and improve our products, services, and Web site; to resolve disputes or troubleshoot problems; to prevent potentially prohibited or illegal activities; to enforce our Terms of Use; and as otherwise described to you at the point of collection. We use information gathered about you from our site statistics (for example your IP address) to analyze trends, to help diagnose problems with our server, and to administer our Web site. This is not linked to any personally identifiable information, except as necessary to prevent fraud or abuse on our system, and to investigate and take actions in the case of suspected fraud or abuse. We also gather broad demographic information from this data to help us improve our site and make your browsing and purchasing experience more enjoyable.</p>

		<p>If you chose to receive communications from Natuur about special promotions when registering as a Natuur member, we may use your email address to send you communications about surveys, contests, sweepstakes, promotions, health and personal care deals or other health news and opportunities on Natuur and its marketing partners. If you choose to receive our Newsletters, you may receive communications such as “Wellness Now” and periodic announcements about changes to our features at your email address. You can discontinue your subscription to Natuur promotional email services at any time by clicking on the unsubscribe link in the email received or by going to Natuur and clicking on the ‘Change Subscriptions’ link on that page. You can also unsubscribe to e-mail communications at any time by forwarding the email to <a href="mailto:support@natuur.in" title="Support">support@natuur.in</a>  with the word “unsubscribe” in the subject line.</p>

		<p>We may also use your information to present a co-branded offer with our partners or affiliates. If we collect information from you in connection with a co-branded offer, it will be clear at the point of collection who is collecting the information and whose privacy statement applies.</p>

		<p>With whom we share this information</p>

		<p>Natuur may share the information it collects with the following entities:</p>

		<p>a) Third-party vendors who provide services or functions on our behalf, including package delivery, business analytics, customer service, marketing, distribution of surveys or sweepstakes programs, and fraud prevention. These companies have access to information needed to perform their functions but are not permitted to share or use the information for any other purpose. </p>

		<p>b) Business partners who may offer products or services through our site. You can tell when a third party is involved in a product or service you have requested because their name will be listed. Please note that we do not control the privacy practices of these third-party business partners and recommend you evaluate their practices before deciding to provide them with any personal information.</p>

		<p>c) In response to subpoenas, court orders, or other legal process; to establish or exercise our legal rights; to defend against legal claims; or as otherwise required by law. In such cases we reserve the right to raise or waive any legal objection or right available to us. When we believe it is appropriate to investigate, prevent, or take action regarding illegal or suspected illegal activities; to protect and defend the rights, property, or safety of Natuur, our customers, or others; and in connection with our Terms of Service and other agreements.</p>

		<p>d) In connection with a corporate transaction, such as a divestiture, merger, consolidation, or asset sale, or in the unlikely event of bankruptcy. </p>

		<p>e) We also may share aggregate or anonymous information with third parties, including advertisers and investors. For example, we may tell our advertisers the number of visitors our Web site receives or what are the most popular personal care products. This information does not contain any personal information and is used to develop content and services we hope you will find of interest. </p>

		<p>f) Occasionally, Natuur may participate in reward programs. In order to properly credit you for your reward, we may share basic customer information with the reward-giver. </p>

		<p>g) In the future, Natuur may host content on behalf of certain other web site partners. Information collected on these pages may be shared with the partner site. These pages, and any information shared with the partner site, are subject to the Privacy Policy of the partner site. </p>

		<p>Other than as set out above, you will be notified when information about you will be shared with third parties, and you will have an opportunity to choose not to have us share such information.</p>

		<p>External Links to Other Sites</p>

		<p>Natuur links you to other Web sites. Please note that those Web sites may not operate under this Privacy Policy, and we are not responsible for the privacy practices or the content of such web sites. We encourage you to be aware when you leave our site and to read and examine the privacy statements posted on those other Web sites to understand their procedures for collecting, using, and disclosing personally identifiable information. </p>

		<p>You may provide personal information to third parties when you register at a third party web site and specifically request that your information be forwarded to us so that we can send you information about our product and service offerings. Natuur does or may in the future use this information in the same manner as information collected directly by Natuur. This Privacy Policy does not bind sites that collect information on behalf of Natuur, and Natuur is not responsible for the privacy practices or the content of such web sites. </p>

		<p>If, in the future, you enter Natuur via a reward or incentive program, Natuur may share your email address with the program through which you entered Natuur to ensure you are credited with your incentive. In order to better communicate with registered users about their registration status, Natuur may acquire information about users from third-party sources, including but not limited to a users mailing address. The information collected will only be shared, if at all, on an aggregate basis only with third parties. </p>

		<p>Data collected by business partners and ad networks to serve you with relevant advertising </p>

		<p>Many of the advertisements you see on the Natuur website are served by <a href="https://www.natuur.in/">www.natuur.in</a>  or its service providers. But we also allow third parties to collect information about your online activities through cookies and other technologies. These third parties include (1) business partners, who collect information when you view or interact with one of their advertisements on our sites, and (2) advertising networks, which collect information about your interests when you view or interact with one of the advertisements they place on many different web sites on the Internet. The information gathered by these third parties is used to make predictions about your characteristics, interests or preferences and to display advertisements on our sites and across the Internet tailored to your apparent interests. </p>

		<p>Please note that we do not have access to or control over cookies or other technologies these third parties may use to collect information about your interests, and the information practices of these third parties are not covered by this Privacy Policy. </p>

		<p>Data collected by companies that operate cookie-based exchanges to serve you with relevant advertising. </p>

		<p>Like other companies operating online, Natuur participates in cookie-based exchanges where anonymous information is collected about your browsing behavior through cookies and other technologies and segmented into different topics of interest (such as wellness). These topics of interest are then shared with third parties, including advertisers and ad networks, so they can tailor advertisements to your apparent interests. We do not share personal information (such as your email address) with these companies and we do not permit these companies to collect any such information about you on our site. </p>

		<p>Data collected to serve you with relevant advertising </p>

		<p>We do not share your personal information (such as email addresses) with unaffiliated third parties so they can serve you with advertisements. You can choose not to receive relevant online advertising on other Web sites, based on your usage of Natuur. </p>

		<p>Children Under 16 Years Old</p>

		<p>Natuur does not knowingly collect, maintain or use personal information from our website about children under age 16. Children under the age of 16 may not participate in any online activities on our web sites. If a child whom we know to be under age 16 sends personal information to us online, we will only use that information to respond directly to that child, notify parents, or seek parental consent.</p>

		<p>How we protect your information</p>

		<p>We want you to feel confident about using Natuur’s Web Portal. While no Web site can guarantee security, we have implemented appropriate administrative, technical, and physical security procedures to help protect the information you provide to us.</p>

		<p>Security, Fraud, and Abuse</p>

		<p>This site has security measures in place to protect against the loss, misuse or alteration of the information under our control. Natuur regularly reviews these measures to better protect users. Occasionally information may be provided to Natuur fraudulently or by an individual unauthorized to provide such information. Natuur may use all information it collects for reasonable investigation and prosecution of suspected fraud and/or abuse. Natuur may provide all information it collects to third parties as necessary to comply with law enforcement requests or other legal requirements. Natuur does not ensure or warrant the security of any information transmitted to us over the Internet.</p>

		<p>Compliance with Laws and Law Enforcement</p>

		<p>We cooperate with government and law enforcement officials and private parties to enforce and comply with the law. We will disclose any information about you to government or law enforcement officials or private parties as we, in our sole discretion, believe necessary or appropriate to respond to claims and legal process (including without limitation subpoenas), to protect our property and rights or the property and rights of a third party, to protect the safety of the public or any person, or to prevent or stop activity we consider to be illegal or unethical. We will also share your information to the extent necessary to comply with ICANN’s rules, regulations and policies.</p>

		<p>Opting Out of Further Communications and Updating Your Information</p>

		<p>You may update / alter your personal account information or opt out of receiving communications from us and our partners at any time. We have the following options for changing and modifying your account information or contact preferences.</p>

		<p>Website: http://natuur.in Email: <a href="mailto:support@natuur.in" title="Support">support@natuur.in</a>
you may write to Natuur Personal Care at the following postal address: C1/635, Palam Vihar<br> Gurgaon - 112017,<br> India.
You may call us at: + (91) – 124 - 4205699
</p>

		<p>To opt out of future communications from Natuur’s partners, please contact the partner directly. For assistance with this, please contact Natuur’s Customer Service: <a href="mailto:support@natuur.in" title="Support">support@natuur.in</a> </p>

		<p>Sending Emails</p>

		<p>We use emails to communicate with you, to confirm your placed orders, and to send information that you have requested. We also provide email links, as on our “Contact Us” page, to allow you to contact us directly. We strive to promptly reply to your messages.</p>

		<p>The information you send to us may be stored and used to improve this site and our products, or it may be reviewed and discarded.</p>

		<p>Third Party Service Providers</p>

		<p>We may at times provide information about you to third parties to provide various services on our behalf, such as processing credit card payments, serving advertisements, conducting contests or surveys, performing analyses of our products or customer demographics, shipping of goods or services, and customer relationship management. We will only share information about you that is necessary for the third party to provide the requested service. These companies are prohibited from retaining, sharing, storing or using your personally identifiable information for any secondary purposes.</p>

		<p>In the event that we use third party advertising companies to serve ads on our behalf, these companies may employ cookies and action tags (also known as single pixel gift or web beacons) to measure advertising effectiveness. Any information that these third parties collect via cookies and action tags is completely anonymous.</p>

		<p>Supplementation of Information</p>

		<p>In order to provide certain services to you, we may on occasion supplement the personal information you submit to us with information from third party sources (e.g., information from our strategic partners, service providers, etc). We do this to enhance our ability to serve you, to tailor our products and services to you, and to offer you opportunities to purchase products or services that we believe may be of interest to you.</p>

		<p>Sweepstakes, Contests/Online Surveys and Order Forms</p>

		<p>Natuur occasionally runs sweepstakes, may provide you with the opportunity to participate in contests or online surveys, or uses order forms on our web site in which we ask for contact and certain personally identifiable demographic information. Participation in sweepstakes, contests and surveys and usage of order forms is completely voluntary and the user therefore has a choice whether or not to disclose the requested information. The requested information typically includes contact information (such as name and address), and demographic information (such as gender, postal code and age level – note that you must be 18 or above to enter). We use or may use your contact information (such as email address) from your sweepstakes entry, survey registration, or order form to inform you if you are the potential winner of a contest or promotion, to monitor site traffic, personalize your experience with Natuur, send you newsletters (should you opt-in for these newsletters) and occasional information about our services or partners, showing you content and advertising that may best suit your needs or investigate cases of suspected fraud. </p>

		<p>This information is shared with advertisers and other third parties on an aggregate basis only. We may use a third party service provider to conduct these surveys or contests. In the event that Natuur partners with a third party to run a sweepstakes, we may share demographic and profile data with such third party only as indicated in the official sweepstakes rules. When we do, that company will be prohibited from using our users’ personally identifiable information for any other purpose. We will not share the personally identifiable information you provide through a contest or survey with other third parties unless we give you prior notice and choice.</p>

		<p>Tell-a-Friend and Email This Deal</p>

		<p>Natuur may, from time to time, offer services to allow our users to send information about our site to their friends. If a user elects to use such referral service for informing a friend about our site, or elects to use our other services to inform a friend about information on our site, we will ask them for the friend’s name and email address. We will automatically send the friend a one-time email inviting them to visit our Web site. We store this information for the purpose of sending this one-time email and investigating suspected fraud. The friend may contact Natuur  at <a href="mailto:support@natuur.in" title="Support">support@natuur.in</a> to request the removal of this information from our database.</p>

		<p>What Happens to my Personal Information if I Terminate my Natuur Account?</p>

		<p>When your Natuur Personal Care account is cancelled (either voluntarily or involuntarily) all of your personally identifiable information is placed in “deactivated” status on our relevant Natuur   databases. However, you should know that deactivation of your account does not mean your personally identifiable information has been deleted from our database entirely. We will retain and use your personally identifiable information if necessary in order to comply with our legal obligations, resolve disputes, or enforce our agreements.</p>

		<p>Business Assets </p>

		<p>As part of operations, Natuur may sell, transfer or merge particular businesses and/or other assets to a third party. As part of such a transaction, personal information users have provided to Natuur may be given to a third party. Natuur attempts to comply with all legislative and judicial orders. These orders will take precedence over the details stated in the policy above. If you have any questions about this Privacy Policy or your dealings with Natuur, please contact us using the contact information below.</p>

		<p>Transfer of Data Abroad</p>

		<p>If you are visiting this web site from a country other than the country in which our servers are located (<strong>currently in the US</strong>), your communications with us will necessarily result in the transfer of information across international boundaries and your information may be stored, and processed in the country where our servers are located and our central database is operated. The data protection and other laws of the United States and other countries might not be as comprehensive as those in your country, but please be assured that Natuur does take steps pursuant to local laws to ensure that your privacy is protected. By using our services, you understand that your information may be transferred to our facilities and those third parties with whom we share it as described in this Policy. By visiting this web site and communicating electronically with us, you consent to such transfers.</p>

		<p>Changes to this Privacy Policy</p>

		<p>We reserve the right to modify this Privacy Policy at any time. If we decide to change our Privacy Policy, we will post those changes to this privacy statement, our home page, and other places we deem appropriate, so that you are aware of what information we collect, how we use it, and under what circumstances, if any, we disclose it.</p>

		<p>If we make material changes to this Policy, we will notify you here, by email, or by means of a notice on our home page, at least 30 days prior to the implementation of the changes.</p>

		<p>Contacting the Web Site</p>

		<p>If you have questions about this Privacy Policy, the practices of this Web site, or your dealings with this site, please contact us at: </p>

		<p>Email: <a href="mailto:support@natuur.in" title="Support">support@natuur.in</a> </p>

		<p>Natuur Personal Care & Home Care<br>
Attn: Web Service Team<br>
PLOT NO 49, HSIIDC, UDYOG KUNJ ALIPUR, GURGAON, HARYANA, INDIA, 122102.
</p>

	 


 </div>
</section>

<?php $this->load->view('vwFooter');?>

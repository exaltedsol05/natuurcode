<?php $this->load->view('vwHeader');?>
<style>
.span1 {cursor:pointer; }

.minus, .plus{
	width: 45px;
    height: 51px;
    background: #f2f2f2;
    /* border-radius: 4px; */
    font-size: 16px;
    font-weight: bold;
    padding: 13px 0px 0px 0px;
    border: 1px solid #ddd;
    display: inline-block;
    vertical-align: middle;
    text-align: center;
}
.quantity1{
height: 52px;
    width: 76px;
    text-align: center;
    font-size: 26px;
    border: 1px solid #ddd;
    /* border-radius: 4px; */
    display: inline-block;
    vertical-align: middle;
}			
</style>  
    <!--shop  area start-->
    <div class="shop_area shop_reverse">
        <div class="container">
            <div class="row">
                

			   <div class="col-lg-12 col-md-12">
                    <!--shop wrapper start-->
                    <!--shop toolbar start-->
                    <div class="shop_title">
                        <h1 style="text-align:center;">Best Sellers</h1>
                    </div>
                    
                    <div class="shop_toolbar_wrapper">
                        <div class="shop_toolbar_btn">

                            <button data-role="grid_3" type="button" class=" btn-grid-3" data-toggle="tooltip" title="3"></button>

                            <button data-role="grid_4" type="button"  class=" btn-grid-4" data-toggle="tooltip" title="4"></button>

                            <button data-role="grid_list" type="button"  class="active btn-list" data-toggle="tooltip" title="List"></button>
                        </div>
                        <div class=" niceselect_option">

                           
                                <select name="orderby" id="sort_products" onchange="window.location='<?php echo current_full_url();?>'+$(this).val();" >

                                   <option value=''><?php echo 'Default';?></option>
								<option <?php echo(!empty($_GET['by']) && $_GET['by']=='name/asc')?' selected="selected"':'';?> value="&by=name/asc"><?php echo 'Sort by name A to Z';?></option>
								<option <?php echo(!empty($_GET['by']) && $_GET['by']=='name/desc')?' selected="selected"':'';?>  value="&by=name/desc"><?php echo 'Sort by name Z to A';?></option>
								<option <?php echo(!empty($_GET['by']) && $_GET['by']=='price/asc')?' selected="selected"':'';?>  value="&by=price/asc"><?php echo 'Sort by price Low to High';?></option>
								<option <?php echo(!empty($_GET['by']) && $_GET['by']=='price/desc')?' selected="selected"':'';?>  value="&by=price/desc"><?php echo 'Sort by price High to Low';?></option>
                                </select>
                          


                        </div>
                        <div class="page_amount">
						    <div class="pagination">
                            <?php echo $this->pagination->create_links();?>&nbsp;
							</div>
                        </div>
                    </div>
                    <!--shop toolbar end-->

                     <div class="row shop_wrapper grid_list">
					    <?php foreach($products as $product){
							
							$discount=round(100*($product->price-$product->saleprice)/$product->price);
							$photo  = theme_img('no_picture.png', 'No Image Available');
							
                           	$product->images    = array_values($product->images);
                          
                           	if(!empty($product->images[0])){
                           		$primary    = $product->images[0];
                           		foreach($product->images as $photo){
                           			if(isset($photo->primary)){
                           				$primary    = $photo;
                           			}
                           		}
                           
                           		$photo  = '<img src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'"/>';
                           }
						   if(!empty($product->images[1])){
                           		$secondry    = $product->images[1];
                           		
                           
                           		$photo1  = '<img src="'.base_url('uploads/images/medium/'.$secondry->filename).'" alt="'.$product->seo_title.'"/>';
                           }
						   
						   ?>
                        <div class="col-12 ">
                            <div class="single_product cat">
                                <div class="product_thumb">
                                    <a class="primary_img" href="<?php echo base_url($product->slug); ?>"><?php echo $photo;?></a>
                                    <a class="secondary_img" href="<?php echo base_url($product->slug); ?>"><?php echo $photo1;?></a>
                                    <div class="label_product">
                                        <span class="label_sale">new</span>
                                    </div>
                                    
                                </div>	
                                <div class="product_content grid_content">
                                    <div class="product_name">
                                        <h4><a href="<?php echo base_url($product->slug); ?>"><?php echo $product->name;?></a></h4>
                                    </div>
                                    <div class="product_rating" product_size="<?php echo $product->product_size;?>">
                                        <ul>
                                            <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                                            <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                                            <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                                            <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                                            <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price-container">
                                        <div class="price_box">
											<?php 
												$price_design = $this->natuur->get_natuur_currency_function($product->price,$product->saleprice);
												echo $price_design['price_design'];
											?>										
                                        </div>
                                       <!--<div class="wishlist_btn">
                                            <a href="wishlist.html" title="wishlist"><i class="ion-android-favorite-outline"></i></a>
                                        </div>-->
										<div class="action_links">
											<ul>
												<li><div class="number">
													<span class="minus span1" >-</span>
														<input type="text" value="1" id="quantity<?php echo $product->id;?>" class="quantity1" name="quantity"/>
													<span class="plus span1">+</span>
											</div></li>
												<li class="add_to_cart"><a title="add to cart"href="javascript:void(0);" onclick="addToCartProduct(<?php echo $product->id;?>);">Add to Cart</a></li>
											   
												<li>
												<?php if($this->Customer_model->is_logged_in(false, false)){?>
														<a onclick="add_to_wishlist(<?php echo $product->id;?>);" href="javascript:;" title="Wishlist">
														<i class="ion-android-favorite-outline"></i></a>
												<?php
													}else{
												?>
														<a  href="#" title="Wishlist" aria-hidden="true" data-toggle="modal" data-target="#myLoginModal">
														<i class="ion-android-favorite-outline"></i></a>
												<?php } ?>
												</li>
												<li class="quick_view"><a href="#" data-toggle="modal" data-target="#modal_box" title="Quick View"><i class="ion-eye"></i></a></li>
											</ul>
										</div>
                                    </div>
                                </div>
                                <div class="product_content list_content">
                                    <div class="product_name">
                                       <h4><a href="<?php echo base_url($product->slug); ?>"><?php echo $product->name;?></a></h4>
                                    </div>
                                    <div class="product_rating">
                                        <ul>
                                            <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                                            <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                                            <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                                            <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                                            <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <?php 
											$price_design = $this->natuur->get_natuur_currency_function($product->price,$product->saleprice);
											echo $price_design['price_design'];
										?>										
                                    </div>
                                    <div class="product_desc">
                                        <p><?php echo $product->excerpt;?></p>
		

                                    </div>
									
                                    <div class="action_links">
                                        <ul>
										    <li><div class="number">
												<span class="minus span1" >-</span>
													<input type="text" value="1" id="quantity<?php echo $product->id;?>" class="quantity1" name="quantity"/>
												<span class="plus span1">+</span>
										</div></li>
                                            <li class="add_to_cart"><a title="add to cart"href="javascript:void(0);" onclick="addToCartProduct(<?php echo $product->id;?>);">Add to Cart</a></li>
                                           
                                            <li>
												<?php if($this->Customer_model->is_logged_in(false, false)){?>
														<a onclick="add_to_wishlist(<?php echo $product->id;?>);" href="javascript:;" title="Wishlist">
														<i class="ion-android-favorite-outline"></i></a>
												<?php
													}else{
												?>
														<a  href="#" title="Wishlist" aria-hidden="true" data-toggle="modal" data-target="#myLoginModal">
														<i class="ion-android-favorite-outline"></i></a>
												<?php } ?>
											</li>
											<li class="quick_view" rel="<?php echo $product->id;?>"><a href="javascript:void(0)" title="Quick View"><i class="ion-eye"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
						<?php } ?>
					</div>

                    <div class="shop_toolbar t_bottom">
                        <div class="pagination">
                            <!--<ul>
                                <li class="current">1</li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li class="next"><a href="#">next</a></li>
                                <li><a href="#">>></a></li>
                            </ul>-->
							<?php echo $this->pagination->create_links();?>
                        </div>
                    </div>
                    <!--shop toolbar end-->
                    <!--shop wrapper end-->
                </div>
            </div>
        </div>
    </div>
	 <!-- modal area start-->
    <div class="modal fade" id="modal_box" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal_body" id="quick_product_details">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-5 col-md-5 col-sm-12">
                                <div class="modal_tab">  
                                    <div class="tab-content product-details-large">
                                        <div class="tab-pane fade show active" id="tab1" role="tabpanel" >
                                            <div class="modal_tab_img">
                                                <a href="#"><img src="assets/img/product/product1.jpg" alt=""></a>    
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="tab2" role="tabpanel">
                                            <div class="modal_tab_img">
                                                <a href="#"><img src="assets/img/product/product5.jpg" alt=""></a>    
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="tab3" role="tabpanel">
                                            <div class="modal_tab_img">
                                                <a href="#"><img src="assets/img/product/product8.jpg" alt=""></a>    
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="tab4" role="tabpanel">
                                            <div class="modal_tab_img">
                                                <a href="#"><img src="assets/img/product/product6.jpg" alt=""></a>    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal_tab_button">    
                                        <ul class="nav product_navactive owl-carousel" role="tablist">
                                            <li >
                                                <a class="nav-link active" data-toggle="tab" href="#tab1" role="tab" aria-controls="tab1" aria-selected="false"><img src="assets/img/s-product/product.jpg" alt=""></a>
                                            </li>
                                            <li>
                                                 <a class="nav-link" data-toggle="tab" href="#tab2" role="tab" aria-controls="tab2" aria-selected="false"><img src="assets/img/s-product/product2.jpg" alt=""></a>
                                            </li>
                                            <li>
                                               <a class="nav-link button_three" data-toggle="tab" href="#tab3" role="tab" aria-controls="tab3" aria-selected="false"><img src="assets/img/s-product/product3.jpg" alt=""></a>
                                            </li>
                                            <li>
                                               <a class="nav-link" data-toggle="tab" href="#tab4" role="tab" aria-controls="tab4" aria-selected="false"><img src="assets/img/s-product/product6.jpg" alt=""></a>
                                            </li>

                                        </ul>
                                    </div>    
                                </div>  
                            </div> 
                            <div class="col-lg-7 col-md-7 col-sm-12">
                                <div class="modal_right">
                                    <div class="modal_title mb-10">
                                        <h2>Handbag feugiat</h2> 
                                    </div>
                                    <div class="modal_price mb-10">
                                        <span class="new_price">$64.99</span>    
                                        <span class="old_price" >$78.99</span>    
                                    </div>
                                    <div class="modal_description mb-15">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia iste laborum ad impedit pariatur esse optio tempora sint ullam autem deleniti nam in quos qui nemo ipsum numquam, reiciendis maiores quidem aperiam, rerum vel recusandae </p>    
                                    </div> 
                                    <div class="variants_selects">
                                        <div class="variants_size">
                                           <h2>size</h2>
                                           <select class="select_option">
                                               <option selected value="1">s</option>
                                               <option value="1">m</option>
                                               <option value="1">l</option>
                                               <option value="1">xl</option>
                                               <option value="1">xxl</option>
                                           </select>
                                        </div>
                                        <div class="variants_color">
                                           <h2>color</h2>
                                           <select class="select_option">
                                               <option selected value="1">purple</option>
                                               <option value="1">violet</option>
                                               <option value="1">black</option>
                                               <option value="1">pink</option>
                                               <option value="1">orange</option>
                                           </select>
                                        </div>
                                        <div class="modal_add_to_cart">
                                            <form action="#">
                                                <input min="0" max="100" step="2" value="1" type="number">
                                                <button type="submit">add to cart</button>
                                            </form>
                                        </div>   
                                    </div>
                                    <div class="modal_social">
                                        <h2>Share this product</h2>
                                        <ul>
                                            <li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li class="pinterest"><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                            <li class="google-plus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                            <li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        </ul>    
                                    </div>      
                                </div>    
                            </div>    
                        </div>     
                    </div>
                </div>    
            </div>
        </div>
    </div>
    <!-- modal area end-->
    <!--shop  area end-->
    <?php 
   function theme_img($uri, $tag=false){
   	if($tag){
   		return '<img src="'.theme_url('assets/assets/images/'.$uri).'" alt="'.$tag.'">';
   	}else{
   		return theme_url('assets/assets/images/'.$uri);
   	}
   	
   }
   function theme_url($uri){
   	$CI =& get_instance();
   	return $CI->config->base_url('/'.$uri);
   }
   ?>
<?php $this->load->view('vwFooter');?>
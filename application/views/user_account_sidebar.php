<div class="filter-box col-md-12">
	<div class="filter-col my_account_leftbar">
        <h3>MY ACCOUNT</h3>
        <div class="show-filter">
            <div class="filter">
                <ul>
                    <li <?php echo  $page =='my_account' ? 'class="active"' : 'class=""' ; ?>>
                    	<a href="<?php echo site_url('secure/my_account');?>">ACCOUNT DASHBOARD</a>
                    </li>
                    <li <?php echo  $page =='my_info' ? 'class="active"' : 'class=""' ; ?>>    
                    	<a href="<?php echo site_url('secure/my_information');?>">ACCOUNT INFORMATION</a>
                    </li>
                    <li <?php echo  $page =='my_add' ? 'class="active"' : 'class=""' ; ?>>
                    	<a href="<?php echo site_url('secure/my_address');?>">ADDRESS BOOK</a>
                    </li>
                    <li <?php echo  $page =='my_order' ? 'class="active"' : 'class=""' ; ?>>
                    	<a href="<?php echo site_url('secure/my_orders');?>">MY ORDERS</a>
                    </li>
                    <li <?php echo  $page =='my_wishlist' ? 'class="active"' : 'class=""' ; ?>>
                    	<a href="<?php echo base_url().'secure/my_wishlist'; ?>">MY WISHLIST</a>
                    </li>
                </ul>
            </div>
        </div>                   
    </div>
</div>
 <style>
.error {
	color:#FF0000;  /* red */
}

</style>
 
 <!--footer area start-->
    <footer class="footer_widgets footer_three">
        <div class="container">  
            <div class="footer_top">
                <div class="row">
                    <div class="col-lg-4 col-md-4 fFirst">
                        <div class="widgets_container contact_us">
                            <h3>CONTACT US</h3>
                            <div class="footer_contact">
                                <ul>
                                    <li><i class="ion-ios-location"></i><span>Addresss:</span> J - 30 (Basement), Mayfield Garden,
										Sector - 51, Gurgaon, Haryana - 122018 (India)</li>
                                    <li><i class="ion-ios-telephone"></i><span>Call Us:</span>9718500111</li>
                                    <li><i class="ion-android-mail"></i><span>Email:</span>management@natuur.in</li>
                                </ul>
                            </div>
							<h3>NEWSLETTER SIGNUP!</h3>
							<div class="newsletter_form">
                                <form action="#">
                                    <input placeholder="NEWSLETTER SIGNUP!" type="text">
                                    <button type="submit"><i class="ion-android-mail"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-3 fSecond">
                        <div class="widgets_container widget_menu">
                            <h3>NATUUR</h3>
                            <div class="footer_menu">
                                <ul>

                                    <li><a href="<?php echo site_url('contact-us');?>">Contact Us</a></li>
                                    <li><a href="<?php echo site_url('about-us');?>">About Us</a></li>
                                    <li><a href="<?php echo site_url('blog');?>">Blog</a></li>
                                    <li><a href="<?php echo site_url('careers');?>">Careers</a></li>
                                    <li><a href="<?php echo site_url('ingredients');?>">Ingredients</a></li>
                                   
                                </ul>
                            </div>

                        </div>
                    </div>
					<div class="col-lg-3 col-md-2 fThird">
                        <div class="widgets_container widget_menu">
                            <h3>HELP</h3>
                            <div class="footer_menu">
                                <ul>
                                    <li><a href="<?php echo site_url('contact-us');?>">Register / Login</a></li>
                                    <li><a href="<?php echo site_url('about-us');?>">Payments & Saved Cards</a></li>
                                    <li><a href="<?php echo site_url('blog');?>">Shipping & Delivery</a></li>
                                    <li><a href="<?php echo site_url('careers');?>">Cancellation & Return</a></li>
                                    <li><a href="<?php echo site_url('faqs');?>">FAQ's</a></li>
                                    <li><a href="<?php echo site_url('privacy-policy');?>">Privacy Policy</a></li>
                                </ul>
                            </div>

                        </div>
                    </div>					
                    <div class="col-lg-3 col-md-3 fFourth">
                       <div class="widgets_container widget_menu">
                            <h3>WHY BUY FROM NATUUR</h3>
                            <div class="footer_menu">
                                <ul>

                                    <li><a href="<?php echo site_url('secure-shopping');?>">Secure Shopping</a></li>
                                    <li><a href="<?php echo site_url('easy-buy');?>">Easy Buy</a></li>
                                    <li><a href="<?php echo site_url('hassle-free-return-policy');?>">Hassle Free Return Policy</a></li>
                                    <li><a href="<?php echo site_url('gift-natuur');?>">Gift Natuur</a></li>
                                    <li><a href="<?php echo site_url('naturr-loyalty-points');?>">Natuur Loyalty Points</a></li>                                   
                                </ul>
                            </div>
                        </div>
						<div class="footer_social">
							<ul>
								<li><a href="https://www.facebook.com/NaturalMama2016/" target="_blank"><i class="ion-social-facebook"></i></a></li>
								<li><a href="https://twitter.com/NaturlaMama" target="_blank"><i class="ion-social-twitter"></i></a></li>
								<li><a href="https://www.instagram.com/natuur.in"><i class="ion-social-instagram" target="_blank"></i></a></li>
								<li><a href="https://www.linkedin.com/company/natural-mama-india" target="_blank"><i class="ion-social-linkedin"></i></a></li>
							</ul>
						</div>
                    </div>                   
                </div>
            </div>
        </div>
         <div class="footer_bottom">      
            <div class="footer_adjust">
               <div class="row">
                    <div class="col-lg-6 col-md-5">
                        <div class="footer_payment">
							<h5>Payment Methods</h5>
                            <!-- <a><img src="<?php echo base_url('assets/img/icon/papyel.png'); ?>" alt=""></a> -->
                            <a><img src="<?php echo base_url('uploads/home_static_image/payment-method.png'); ?>" alt=""></a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-7">
                        <div class="copyright_area">
                            <p>Copyright &copy; <?php echo date('Y');?><a href="https://natuur.in/">Natuur</a>All Right Reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>     
    </footer>
    <!--footer area end-->
<!-- Plugins JS -->
<script src="<?php echo HTTP_JS_PATH; ?>plugins.js"></script>
<!-- Main JS -->
<script src="<?php echo HTTP_JS_PATH; ?>main.js"></script>
<script src="<?php echo HTTP_JS_PATH; ?>equal_heights.js"></script>
<script src="<?php echo HTTP_JS_PATH; ?>jquery.blockUI.js"></script>
<script src="<?php echo HTTP_JS_PATH; ?>jquery.validate.min.js"></script>
<script src="<?php echo HTTP_JS_PATH; ?>star-rating.min.js"></script>
<script src="<?php echo HTTP_JS_PATH; ?>jquery.cookieMessage.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
});
</script>
<script type="text/javascript">
$.cookieMessage({
    'mainMessage': 'This website uses cookies. By using this website you consent to our use of these cookies. For more information visit our <a href="<?php echo site_url('privacy-policy');?>">Privacy Policy</a>. ',
    'acceptButton': 'Got It!',
    'fontSize': '16px',
    'backgroundColor': '#222',
});
</script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5e202b9b27773e0d832dd285/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

<script type="text/javascript">

$(window).on('load', function() { // makes sure the whole site is loaded 
  $('#status').fadeOut(); // will first fade out the loading animation 
  $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website. 
  $('body').delay(350).css({'overflow':'visible'});
});
	$(document).ready(function(){
		
		//if we support placeholder text, remove all the labels
		if(!supports_placeholder()){
			$('.placeholder').show();
		}<?php
		// Restore previous selection, if we are on a validation page reload
		$zone_id = set_value('zone_id');
		echo "\$('#zone_id').val($zone_id);\n";
		$zone_id1 = set_value('zone_id1');
		echo "\$('#zone_id1').val($zone_id1);\n"; ?>
	});
	function supports_placeholder()
	{
		return 'placeholder' in document.createElement('input');
	}


$(document).ready(function() {
	$('#country_id').change(function(){
		populate_zone_menu();
	});	

	$('#country_id1').change(function(){
		populate_zone_menu1();
	});	
});

// context is ship or bill

function populate_zone_menu(value)
{
	$.post('<?php echo site_url('locations/get_zone_menu');?>',{id:$('#country_id').val()}, function(data) {
		$('#zone_id').html(data);
	});
}
function populate_zone_menu1(value)
{
	$.post('<?php echo site_url('locations/get_zone_menu');?>',{id:$('#country_id1').val()}, function(data) {
		$('#zone_id1').html(data);
	});
}	
$(document).ready(function(){
		/* 
		$(".payment_mode").change(function(){
		var zipcode = '';
		if(!document.getElementById("use_shipping").checked){
			var str_zip = document.forms["checkoutform"]["zip1"].value;
			if (str_zip == null || str_zip == "") {
				alert("Shipping address city must be filled out");
				document.forms["checkoutform"]["zip1"].focus();
				$(".payment_mode").prop('checked', false);
				return false;
			}else{
				zipcode = str_zip; 
			}
		}else{
			var str_zip = document.forms["checkoutform"]["zip"].value;
			if (str_zip == null || str_zip == "") {
				alert("Billing address city must be filled out");
				document.forms["checkoutform"]["zip"].focus();
				$(".payment_mode").prop('checked', false);
				return false;
			}else{
				zipcode = str_zip; 
			}
		}		
		//change button text and show bank details
		var selectedVal = "";
		var selected = $(".radio input[class='payment_mode']:checked");
		if (selected.length > 0) {
			selectedVal = selected.val();
		}		
		if(selectedVal == 'payu_money'){
			$("#confirm_btn").val('Proceed to payment');
			$("#bank_transfer_details").slideUp('fast');
		}else if(selectedVal == 'bank_transfer_payment'){
			$("#confirm_btn").val('Place Order');	
			$("#bank_transfer_details").slideDown('fast');
		}else{
			$("#bank_transfer_details").slideUp('fast');
			$("#confirm_btn").val('Place Order');	
		}
		$.ajax({
			url: '<?php echo base_url(); ?>cart/checkzip',
			type: 'post',
			data: {'zipcode':zipcode},
			dataType:"json",
			success: function(data){
				if(data.color == 'green'){
					$("#chckpinmsg-red").html('');
					grant_total = $("#order_grant_total").val();
					if(data.message == 'Only Prepaid Delivery Available'){
						$("#cod_mode").prop('checked', false);
						$("#chckpinmsg-green").html(data.message+'<br/> Expected Time : '+data.estimate_time);
					}else if(grant_total > 10000){
						grant_total = $("#order_grant_total").val();
						$("#cod_mode").prop('checked', false);
						$("#chckpinmsg-green").html("Only Prepaid Delivery Available if order total is greater then <i class='fa fa-inr'></i>10,000'<br/> Expected Time : "+data.estimate_time);
					}else{
						$("#chckpinmsg-green").html(data.message+'<br/> Expected Time : '+data.estimate_time);
						$("#chckpinmsg-red").html('');					
					}					
				}else{
					$(".payment_mode").prop('checked', false);
					$("#chckpinmsg-red").html(data.message);
					$("#chckpinmsg-green").html('');
				}
			}
		});		
	}); */
    $('#use_shipping').change(function(){
        if(this.checked){
			$(".shiping_fa  ").addClass("fa-check");
		}else{
			$(".shiping_fa  ").removeClass("fa-check");
		}     
    });	
	$(".ishipping_charges").click(function(){
		var charge_shipped = $(this).attr('ship_charge');
		if($(".ishipping_charges").is(':checked')){ 
			$(".shippingtr").css("display", "table-row");
			$(".antishippingtr").css("display", "none");
		}else{
			$(".shippingtr").css("display", "none");
			$(".antishippingtr").css("display", "table-row");
		}
	});
	$("#confirm_btn").click(function(){
		
		var strF_name = document.forms["checkoutform"]["firstname"].value;
		if (strF_name == null || strF_name == "") {
			alert("Billing address First Name must be filled out");
			document.forms["checkoutform"]["firstname"].focus();
			return false;
		}

		var strL_name = document.forms["checkoutform"]["lastname"].value;
		if (strL_name == null || strL_name == "") {
			alert("Billing address Last Name must be filled out");
			document.forms["checkoutform"]["lastname"].focus();
			return false;
		}
		var str_email = document.forms["checkoutform"]["email"].value;
		if (str_email == null || str_email == "") {
			alert("Billing address email must be filled out");
			document.forms["checkoutform"]["email"].focus();
			return false;
		}
		var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;  
		if(!str_email.match(mailformat)){  
			alert("You have entered Billing address an invalid email address!");
			document.forms["checkoutform"]["email"].focus();			
			return false;   
		}
		var str_address1 = document.forms["checkoutform"]["address1"].value;
		if (str_address1 == null || str_address1 == "") {
			alert("Billing address must be filled out");
			document.forms["checkoutform"]["address1"].focus();
			return false;
		}
		var str_city = document.forms["checkoutform"]["city"].value;
		if (str_city == null || str_city == "") {
			alert("Billing address city must be filled out");
			document.forms["checkoutform"]["city"].focus();
			return false;
		}
		var str_zip = document.forms["checkoutform"]["zip"].value;
		if (str_zip == null || str_zip == "") {
			alert("Billing address Pin Code must be filled out");
			document.forms["checkoutform"]["zip"].focus();
			return false;
		}else if(!isValidzip(str_zip)){
			alert("Billing address Pin Code must be correct");
			document.forms["checkoutform"]["zip"].focus();
			return false;
		}
		var str_phone = document.forms["checkoutform"]["phone"].value;
		if (str_phone == null || str_phone == "") {
			alert("Billing address phone must be filled out");
			document.forms["checkoutform"]["phone"].focus();
			return false;
		}else if(!isValidcontact(str_phone)){
			alert("Billing address phone must be correct mobile no");
			document.forms["checkoutform"]["phone"].focus();
			return false;
		}
		if(!document.getElementById("use_shipping").checked){
			var strF_name = document.forms["checkoutform"]["firstname1"].value;
			if (strF_name == null || strF_name == "") {
				alert("Shipping address First Name must be filled out");
				document.forms["checkoutform"]["firstname1"].focus();
				return false;
			}
			var strL_name = document.forms["checkoutform"]["lastname1"].value;
			if (strL_name == null || strL_name == "") {
				alert("Shipping address Last Name must be filled out");
				document.forms["checkoutform"]["lastname1"].focus();
				return false;
			}
			var str_email = document.forms["checkoutform"]["email1"].value;
			if (str_email == null || str_email == "") {
				alert("Shipping address email must be filled out");
				document.forms["checkoutform"]["email1"].focus();
				return false;
			}
			var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;  
			if(!str_email.match(mailformat)) {  
				alert("You have entered Shipping address an invalid email address!");
                document.forms["checkoutform"]["email1"].focus();				
				return false;   
			}
			var str_address1 = document.forms["checkoutform"]["address11"].value;
			if (str_address1 == null || str_address1 == "") {
				alert("Shipping address must be filled out");
				document.forms["checkoutform"]["address11"].focus();
				return false;
			}
			var str_city = document.forms["checkoutform"]["city1"].value;
			if (str_city == null || str_city == "") {
				alert("Shipping address city must be filled out");
				document.forms["checkoutform"]["city1"].focus();
				return false;
			}
			var str_zip = document.forms["checkoutform"]["zip1"].value;
			if (str_zip == null || str_zip == "") {
				alert("Shipping address Pin Code must be filled out");
				document.forms["checkoutform"]["zip1"].focus();
				return false;
			}else if(!isValidzip(str_zip)){
				alert("Shipping address Pin Code must be correct");
				document.forms["checkoutform"]["zip1"].focus();
				return false;
			}
			var str_phone = document.forms["checkoutform"]["phone1"].value;
			if (str_phone == null || str_phone == "") {				
				alert("Shipping address phone must be filled out");
				document.forms["checkoutform"]["phone1"].focus();
				return false;
			}else if(!isValidcontact(str_phone)){
				alert("Shipping address phone must be correct mobile no");
				document.forms["checkoutform"]["phone1"].focus();
				return false;
			}
		}
        var use_shipping = $('input[name="use_shipping"]');
		if(use_shipping.prop("checked") == true){
			var pin = $('input[name="zip"]').val();
		}else{
			var pin = $('#ship_zip').val();
		}		
		if(pin !='' && pin !=null){
			var total_weight = $("#weight").attr('value');
			$.ajax({
				type     : "POST",
				data     : {'pin':pin,'total_weight':total_weight},
				url      : "<?=base_url('cart/checkpin')?>",
				success  : function(response){
					$('.ship_amount').text(response);
					$('input[name="shippingtr"]').val(response);
					var sub_tot = parseInt($('.sub_total').attr('value'));
					var grand_total = sub_tot + parseInt(response);
					$('.grand_total').text(grand_total);
					$('#order_grant_total').val(grand_total);
					$('input[name="subtotalamt"]').val(grand_total);
					}
			});
		}
		if (!$('input[name=shipping_method]:checked').length) {
           // do something here
			alert("Please select shipping method");
			document.forms["checkoutform"]["shipping_method"].focus();
			return false;
        }

		if(!document.getElementById("term").checked){
			alert("Please accept all term and conditions");
			document.forms["checkoutform"]["term"].focus();
			return false;
		}
		
		$("#checkoutform").submit();
	})
});
window.onload = function(){
	$('.product').equalHeights();
}
</script>
<script>
$(document).ready(function(){	
	$(".edit_address").click(function(){
		$.ajax({
			type: "POST",
			url: '<?php echo site_url('secure/address_form'); ?>/'+$(this).attr('rel'),
			//data: {id:id, status:status},
			beforeSend:function(){},
			success: function(msg){
				$('#address-form-container').html(msg);
	            $('#myModal').modal('show');
			}
		});	
	});	
	$('#search_form_submit').click(function(){	 
	    $('#search_frm').submit();
	});
	
	$('.open_register_model').click(function(){
		$('#authentication').modal('hide');
	});
	$('.open_login_model').click(function(){
		$('#signup').modal('hide');
	});	
	$("#login_form").validate({
		rules:{
			email_id:"required",
			password:"required"
		},
        submitHandler: function (form){
     		$.ajax({
                type: "POST",
                datatype:"json",
                data: $("#login_form").serialize(),
                url: "<?php echo base_url(); ?>secure/login", 
                success: function(msg){
                	if(msg.status == false){
                		$("#alert_msg").html(msg.msg);
                	}else if(msg.status == true){
						//alert("hello");
                		window.location='<?php echo base_url(); ?>checkout';
                	}
                }
            });
        }
    });
	$("#reg_form").validate({
		rules:{
			reg_email:{
				required: true,
				email: true,
				remote: {
					type: 'post',
					url: '<?php echo base_url()."secure/checkemail"; ?>',
					data: {
						user_email: function() {
							return $( "#reg_email" ).val();
						}
					},
				}
			},
			reg_phone: {
				required: true,
				number: true,
				remote: {
					type: 'post',
					url: '<?php echo base_url()."secure/checkphone"; ?>',
					data: {
						user_phone: function() {
							return $( "#reg_phone" ).val();
						}
					},
				}
			},
			reg_password:
			{
				required:true,
				minlength:6,
			}
		},
		messages:
		{
			 reg_email:
			 {
				required: "Please enter your email address.",
				email: "Please enter a valid email address.",
				remote: jQuery.validator.format("{0} is already taken.")
			 },
			 reg_phone:
			 {
				required: "Please enter your phone number.",
				number: "Please enter a valid phone number.",
				remote: jQuery.validator.format("{0} is already taken.")
			 },
		},
        submitHandler: function (form) 
        {
			form.submit();
        }
    });	
	$("#newsletter_form").validate({
        rules: {
            exampleInputEmail1  : "required"
        },
        submitHandler: function(form) {
            var email = $('#exampleInputEmail1').val();
            $.ajax({
                url: '<?php echo base_url(); ?>secure/newsletterEmail',
                type: 'post',
                dataType: 'json',
                data: {email:email},
                success: function(data) {
                   
                       if(data==1)
                       {
                            $("#alt_msg").html('<p style="color:green;font-weight:300;padding:5px;">You have successfuly subscribed.</p>');
                       }
                       else
                       {
                            $("#alt_msg").html('<p style="color:red;font-weight:300;padding:5px;">You have already subscribed.</p>');
                       }                  
                }
            });
            
        }
    });
 
});

$('#rating-input').on('rating.change', function(event, value, caption) {
	$('#rating_val').val(value);
});
	
	//add review form
	function post_review_form(){
		product_id = $("#prdid").val();
		pop_review = $("#pop_review").val();
		pop_review_title = $("#review_title").val();
		rating_val = $("#rating_val").val();
		
		if(pop_review_title.length < 10){
			$("#pop_err_msg").html('<div class="alert alert-danger">Please make sure your title contains at least 10 characters.</div>');
			return false;
		}
		
		if(pop_review.length < 20){
			$("#pop_err_msg").html('<div class="alert alert-danger">Please make sure your review contains at least 20 characters.</div>');
			return false;
		}
		$.ajax({
			url: '<?php echo base_url(); ?>ratenreview_controller/add_review',
			type: 'post',
			data: {'pop_product_id':product_id, 'review':pop_review, 'title' : pop_review_title,'rating':rating_val},
			success: function(data) {
				if(data == 'logout'){
					url = "<?php echo base_url(); ?>secure/login";
					$( location ).attr("href", url);
				}else if(data == 'no'){
					$.blockUI({ 
						css: { 
							border: 'none', 
							padding: '15px',
							message: data.message, 				
							backgroundColor: '#000', 
							'-webkit-border-radius': '10px', 
							'-moz-border-radius': '10px', 
							opacity: .5, 
							color: '#fff' 
						},
						message:'<strong style="font-size:18px;">Your already review this product.</strong>'
					}); 

					setTimeout($.unblockUI, 2000); 
				}else if(data == 'yes'){
					$.blockUI({ 
						css: { 
							border: 'none', 
							padding: '15px',
							message: data.message, 				
							backgroundColor: '#000', 
							'-webkit-border-radius': '10px', 
							'-moz-border-radius': '10px', 
							opacity: .5, 
							color: '#fff' 
						},
						message:'<strong style="font-size:18px;">Your review add for this product successfully.</strong>'
					}); 

					setTimeout($.unblockUI, 2000); 
					location.reload();
				}	
			}
		});
	}


	function addToCart(id){
        var cart = $('#add-to-cart_'+id);
        $.post(cart.attr('action'), cart.serialize(), function(data){
			// return false;
			if(data.message != undefined){
				$.blockUI({ 
					css: { 
						border: 'none', 
						padding: '15px',
						message: data.message, 				
						backgroundColor: '#000', 
						'-webkit-border-radius': '10px', 
						'-moz-border-radius': '10px', 
						opacity: .5, 
						color: '#fff' 
					},
					message:'<strong style="font-size:18px;">'+data.message+'</strong>'
				}); 
				setTimeout($.unblockUI, 2000); 
			}else if(data.error != undefined){
				$.blockUI({ 
					css: { 
						border: 'none', 
						padding: '15px',
						message: data.message, 				
						backgroundColor: '#000', 
						'-webkit-border-radius': '10px', 
						'-moz-border-radius': '10px', 
						opacity: .5, 
						color: '#fff' 
					},
					message:'<strong style="font-size:18px;">'+data.error+'</strong>'
				}); 
				setTimeout($.unblockUI, 2000);
			}			
        }, 'json');
		
		prval = 0;
		if(parseInt($(".current_cart_value").text())){
			prval = parseInt($(".current_cart_value").text());
		}
		quantval = parseInt($("#add-to-cart_"+id+" input[name=quantity]").val());
		
		if(isNaN(quantval)){
			//return nan in case of grouped product list in product details page
			quantval = parseInt($("#nan_"+id).val());
		}
		
		if(quantval > 0){
			newtiems = prval+quantval;
			$(".current_cart_value").html(newtiems + " Items");
		}
		setTimeout(function(){ location.reload(); }, 2000);
		
    }
	
	
	//add to wishlist functions
	function add_to_wishlist(product_id = 0){
		$.ajax({
			url: '<?php echo base_url(); ?>ratenreview_controller/add_wishlist',
			type: 'post',
			data: {'product_id':product_id},
			success: function(data) {
				if(data == 'logout'){
					url = "<?php echo base_url(); ?>secure/login";
					$( location ).attr("href", url);
				}else{
					preval = $("#header_wishlist").text();
					msg = '';
					if(data == 'no'){
						msg = 'Your product has been already added.';
						$.blockUI({ 
							css: { 
								border: 'none', 
								padding: '15px',
								message: data.message, 				
								backgroundColor: '#000', 
								'-webkit-border-radius': '10px', 
								'-moz-border-radius': '10px', 
								opacity: .5, 
								color: '#fff' 
							},
							message:'<strong style="font-size:18px;">'+msg+'</strong>'
						}); 
						setTimeout($.unblockUI, 2000);
					}else{
						msg = 'You have addedd successfully product in Wishlist.';
						$.blockUI({ 
							css: { 
								border: 'none', 
								padding: '15px',
								message: data.message, 				
								backgroundColor: '#000', 
								'-webkit-border-radius': '10px', 
								'-moz-border-radius': '10px', 
								opacity: .5, 
								color: '#fff' 
							},
							message:'<strong style="font-size:18px;">'+msg+'</strong>'
						}); 
						setTimeout($.unblockUI, 2000);
					}
				}	
			}
		});
	}

	//add to compare functions
	function add_to_compare(product_id = 0){
		$.ajax({
			url: '<?php echo base_url(); ?>ratenreview_controller/add_compare',
			type: 'post',
			data: {'product_id':product_id},
			success: function(data) {
				//if(data == 'logout'){
				if(false){	
					url = "<?php echo base_url(); ?>secure/login";
					$( location ).attr("href", url);
				}else{
					preval = $("#header_compare").text();
					msg = '';
					if(data == "full"){
						msg = 'You can add maximum 4 product in Compare List';
					}if(data=='found'){
						msg = 'Your product has been already added to Compare List';
					}else if(data=='add'){
						msg = 'You have addedd successfully product in Compare List.';
					}else{
						// no query
					}
					$.blockUI({ 
						css: { 
							border: 'none', 
							padding: '15px',
							message: data.message, 				
							backgroundColor: '#000', 
							'-webkit-border-radius': '10px', 
							'-moz-border-radius': '10px', 
							opacity: .5, 
							color: '#fff' 
						},
						message:'<strong style="font-size:18px;">'+msg+'</strong>'
					}); 
					setTimeout($.unblockUI, 2000);
				}	
			}
		});
	}
	
	function checkphone()
    {
	    var phone =document.getElementById("reg_phone").value;
	    var pattern = /^\d{10}$/;
	    if (pattern.test(phone)) 
	    {
	        $.ajax({
			    type: 'post',
			    url: '<?php echo base_url()."secure/checkphone"; ?>',
			    data: {user_phone:phone},
			    success: function (response) {

			        if(response=="OK"){
	               		$('#reg_phone').css('border','3px solid green');
	                    return true;	
	                }else{
	                	$('#reg_phone').css('border','3px solid red');
	                    return false;	
	                }
             	}
		   });
	    }else{
		   $('#reg_phone').css('border','3px solid red');
		   return false;
	    }
	}

    function checkemail()
    {
	    var email =document.getElementById("reg_email").value;
	    var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    if(pattern.test(email)) 
	    {
	        $.ajax({
			    type: 'post',
			    url: '<?php echo base_url()."secure/checkemail"; ?>',
			    data: {user_email:email},
			    success: function (response) {

			        if(response=="OK")	
	                {
	               		$('#reg_email').css('border','3px solid green');
	                    return true;	
	                }
	                else
	                {
	                	$('#reg_email').css('border','3px solid red');
	                    return false;	
	                }
             	}
		   });
	    }
	    else
	    {
		   $('#reg_email').css('border','3px solid red');
		   return false;
	    }
	}
	
	function checkpassword()
	{
		var password =document.getElementById("reg_password").value;
	    if(password.length > 5) 
	    {
	        $('#reg_password').css('border','3px solid green');
	        return true;              
	    }
	    else
	    {
		   $('#reg_password').css('border','3px solid red');
		   return false;
	    }
	}
	
	function addToCartProduct(id){
		var quantity=$('#quantity'+id).val();
		$.ajax({
			type: "POST",  					
			url: "<?php echo base_url()."cart/add_to_cart_ajax";?>",
			data: {id:id, quantity:quantity},
			beforeSend:function() {},
			dataType: 'json',	
			success: function(data){
				$.blockUI({ 
					css: { 
						border: 'none', 
						padding: '15px',
						message: data.message, 				
						backgroundColor: '#000', 
						'-webkit-border-radius': '10px', 
						'-moz-border-radius': '10px', 
						opacity: .5, 
						color: '#fff' 
					},
					message:'<strong style="font-size:18px;">Product add to cart successfull.</strong>'
				}); 

				setTimeout($.unblockUI, 2000); 
				location.reload();
			}
		});		
	}


</script>

<script>
   $("#ask_question").validate({
   	rules:{
   		nickname	: "required",
   		email		: "required",
   		question	: "required"
   	},
   	submitHandler: function (form){
   		$.ajax({
   			type: "POST",
   			datatype:"json",
   			data: $("#ask_question").serialize(),
   			url: "<?php echo base_url();?>cart/ask_question", 
   			success: function(msg){
   				alert("Thank you for your question"); 
   				setTimeout(function(){ 
   					window.location.reload();
   				}, 2000);
   			}
   		});
   	}
   });
   jQuery(document).ready(function(){
   	$('#save_question_off').click(function(){
   		alert("Please login before ask question");
   	});
   	$("#img1").elevateZoom({ gallery: 'gallery_01', cursor: 'pointer', galleryActiveClass: "active" });
   	$("#img1").bind("click", function(e) {
   		var ez = $('#img1').data('elevateZoom');
   		ez.closeAll();
   //$.fancybox(ez.getGalleryList());
   return false;
   });
   // This button will increment the value
   $('.qtyplus').click(function(e){
	   
	 
	   // Stop acting like a button
	   e.preventDefault();
	   // Get the field name
	   fieldName = $(this).attr('field');
	   // Get its current value
	   var currentVal = parseInt($('input[id='+fieldName+']').val());
	   // If is not undefined
	   if (!isNaN(currentVal)) {
	   // Increment
	   $('input[id='+fieldName+']').val(currentVal + 1);
	   } else {
	   // Otherwise put a 0 there
	   $('input[id='+fieldName+']').val(0);
	   }
   });
   // This button will decrement the value till 0
   $(".qtyminus").click(function(e) {
	   // Stop acting like a button
	   e.preventDefault();
	   // Get the field name
	   fieldName = $(this).attr('field');
	   // Get its current value
	   var currentVal = parseInt($('input[id='+fieldName+']').val());
	   // If it isn't undefined or its greater than 0
	   if (!isNaN(currentVal) && currentVal > 1) {
	   // Decrement one
	   $('input[id='+fieldName+']').val(currentVal - 1);
	   } else {
	   // Otherwise put a 0 there
	   $('input[id='+fieldName+']').val(1);
	   }
   });
   
   
   $('.minus').click(function () {
				var $input = $(this).parent().find('input');
				var count = parseInt($input.val()) - 1;
				count = count < 1 ? 1 : count;
				$input.val(count);
				$input.change();
				return false;
			});
			$('.plus').click(function () {
				var $input = $(this).parent().find('input');
				$input.val(parseInt($input.val()) + 1);
				$input.change();
				return false;
			});
   
   })
   if($(window).width() > 768){
   // Hide all but first tab content on larger viewports
   $('.accordion__content:not(:first)').hide();
   // Activate first tab
   $('.accordion__title:first-child').addClass('active');
   } else {
   // Hide all content items on narrow viewports
   $('.accordion__content').hide();
   };
   // Wrap a div around content to create a scrolling container which we're going to use on narrow viewports
   //$( ".accordion__content" ).wrapInner( "<div class='overflow-scrolling'></div>" );
   // The clicking action
   $('.accordion__title').on('click', function() {
   $('.accordion__content').hide();
   $(this).next().show().prev().addClass('active').siblings().removeClass('active');
   });
   function checkzipcode(){
   var zip = $.trim($("#pincode").val());
   var zipRegex = /^\d{6}$/;
   var msg = '';
   if (!zipRegex.test(zip)){
   	$("#chckpinmsg-green").html('');
   	$("#chckpinmsg-red").html("Please enter valid zip code.");
   }else{
   	$.ajax({
   		url: '<?php echo base_url(); ?>cart/checkzip',
   		type: 'post',
   		data: {'zipcode':zip},
   		dataType:"json",
   		success: function(data) {
   			if(data != '0'){
   				var zip_details = eval(data);
   //{"id":"43","postcode":"110043","isship":"1","iscod":"1","daystodeliver":"2 to 4"}
   if(zip_details['isship'] == '1'){
   $("#addcartbut").prop("disabled", false);
   if(zip_details['iscod'] == '1'){
   	msg = 'Yes, Cash on Delivery available in your area and We can deliver product in '+zip_details['iscod']+' Days';
   	$("#chckpinmsg-green").html(msg);
   	$("#chckpinmsg-red").html('')
   }else{
   	msg = 'Sorry, Cash on Delivery not available in your area and We can deliver product in '+zip_details['iscod']+' Days';
   	$("#chckpinmsg-green").html('');
   	$("#chckpinmsg-red").html(msg);
   }
   }else{
   $("#addcartbut").prop("disabled", true);
   $("#chckpinmsg-green").html('');
   $("#chckpinmsg-red").html("Sorry, We could not provide shipping service in your area.");
   }
   }else{
   $("#addcartbut").prop("disabled", true);
   $("#chckpinmsg-green").html('');
   $("#chckpinmsg-red").html("Sorry, We could not provide shipping service in your area.");
   }
   }
   });
   }			
   }
</script>
<script>
$(document).ready(function(){
$('#rating-input').rating({
update: $('#user_rating').val(),
min: 1,
max: 5,
step: 1,
size: 'x',
showClear: false,
starCaptions: {
1:'Disappointing',
2:'Bad',
3:'Average',
4:'Good',
5:'Perfect'
}
});
});
</script>
<!--<script type="text/javascript">
$(document).ready(function(){
              
            $("#reg_form").validate({
                rules: {
                    firstname: "required",
                    lastname: 'required',
                    email: {
                    		required: true,
                    		email: true
                    },
                    phone:{
                                required: true,
                                number: true,
                                minlength:10
                    },
                   	password: {
                   		required:true,
                   		minlength:6
                   	},
					confirm: {
					      equalTo: "#password"
					}
                },
                messages: {
                    firstname: "Please enter firstname.",
                   	lastname: "Please enter lastname.",
                   	email:{
                    		required: "Please enter email.",
                    		email: "Please enter valid email id."
                    },
                    phone:{
                            required: "Please enter mobile number.",
                            number: "only numbers allowed."
                    }
                },
                submitHandler: function (form) {
                	
                	var redirect = $("#redirect").val();
        
                	if(redirect == '')
                	{

                		$('#myModal').modal({backdrop: 'static', keyboard: false});
                		getOtp();
                	}
                	else
                	{
                		form.submit();
                	}
             
                }

            });

});

		function check_otp()
		{
			var hidden_otp = $("#hidden_otp").val();
			var enter_otp = $("#enter_otp").val();

			
				if(hidden_otp == enter_otp)
				{
					document.getElementById("modal_message").innerHTML = "<font color='green'>Thank you for verified the mobile number and please wait until page redirect.</font>";

					
					setTimeout(function(){
						
						$('#myModal').modal('hide');
						
						$("#subBtn").click();
					}, 3000);		
				}
				else
				{
					document.getElementById("modal_message").innerHTML = "<font color='#FF0000'>Incorrect Code</font>";
				}

		}

        function getOtp()
        {
        	// var mobile = document.getElementById("bill_phone").value;
	        // var pattern = /^\d{10}$/;
	        // if (pattern.test(mobile)) 
	        // {
           		var phone = $("#phone").val();
	            $.ajax({
	                type: "POST",
	                datatype:"json",
	                data: {phone:phone},
	                url: "<?php echo base_url(); ?>secure/generateOtp", 
	                success: function(otp) 
	                {
	                	if(otp !="not_ok")
	                	{
	                		$("#new_mob").html(phone);
	                		$("#hidden_otp").val(otp);
	                		$("#redirect").val(1);
	                	}
	                }
	            });
	          
	        // }
	        // alert("It is not valid mobile number.input 10 digits number!");
	        // return false;
  
        }
</script>-->
</body>
</html>
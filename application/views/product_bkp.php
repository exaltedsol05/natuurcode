<?php $this->load->view('vwHeader');?>
<style>#showMenu{display:none;}</style>
<script>
window.onload = function(){
	$('.product').equalHeights();
}
</script>
<link rel="stylesheet" href="<?php echo HTTP_CSS_PATH; ?>multizoom.css" type="text/css" />
<section class="container">
    <div class="product-details">
        <div class="row">
        	<div class="col-lg-12"><?php
				if(!empty($base_url) && is_array($base_url)):?>
					<div class="row">
						<div class="col-sm-12">
							<ul class="breadcrumb">
								<li><a href="<?php echo base_url();?>" alt="Dentkart">Home</a></li><?php
								$url_path	= '';
								$count	 	= 1;
								foreach($base_url as $bc):
									$url_path .= '/'.$bc;
									if($count == count($base_url)):?>
										<li class="active"><?php echo $bc;?></li>
									<?php else:?>
										<li><a href="<?php echo site_url($url_path);?>"><?php echo $bc;?></a></li> <span class="divider">/</span>
									<?php endif;
									$count++;
								endforeach;?>
							</ul>
						</div>
					</div>
				<?php endif;?>

				<?php if ($this->session->flashdata('message')):?>
					<div class="alert alert-info">
						<a class="close" data-dismiss="alert">×</a>
						<?php echo $this->session->flashdata('message');?>
					</div>
				<?php endif;?>

				<?php if ($this->session->flashdata('error')):?>
					<div class="alert alert-danger">
						<a class="close" data-dismiss="alert">×</a>
						<?php echo $this->session->flashdata('error');?>
					</div>
				<?php endif;?>

				<?php if (!empty($error)):?>
					<div class="alert alert-danger">
						<a class="close" data-dismiss="alert">×</a>
						<?php echo $error;?>
					</div>
				<?php endif;?>

				<div class="row">
            		<div class="col-sm-4 col-lg-4">
                    	<div class="product-gallery"><?php
							$sales_price = $this -> dentkart -> product_price($product);
							$discount=round(100*($product->price-$sales_price)/$product->price);?>
							<?php if($discount){?>
								<div class="off-col"><?php echo $discount;?>% OFF</div>
							<?php } ?>
							<div class="targetarea"><?php
								$photo  = theme_img('no_picture.png', 'No Image Available');
								$product->images    = array_values($product->images);

								if(!empty($product->images[0])){
									$primary    = $product->images[0];

									foreach($product->images as $photo){
										if(isset($photo->primary)){
											$primary    = $photo;
										}
									}

									$photo  = '<img id="multizoom1" src="'.base_url('uploads/images/small/'.$primary->filename).'" alt="'.$product->seo_title.'"/>';
								}
								echo $photo ?>
							</div>
						  <?php if(count($product->images) > 1):?>
							<div class="multizoom1 thumbs">
								<?php foreach($product->images as $image):?>
									<a href="<?php echo base_url('uploads/images/small/'.$image->filename);?>" data-lens="false" data-large="<?php echo base_url('uploads/images/medium/'.$image->filename);?>" data-title="Saleen S7 Twin Turbo">
										<img src="<?php echo base_url('uploads/images/thumbnails/'.$image->filename);?>" alt="Saleen" title="" style="width:72px;" />
									</a>
								<?php endforeach;?>
							</div>
							<?php endif;?>
						</div>
					</div>

                    <div class="col-sm-8 col-lg-8">
                    	<div class="product-info">
                            <div class="product-head"><?php echo $product->name;?></div>
                            <h5><?php echo $product->excerpt;?></h5>
                            <div class="row rr-box">
                            	<div class="col-sm-3">
                                	<div class="rating-box">
										<ul class="clearfix"><?php
											$all_ret = $this->dentkart->product_rating($product -> id);
											$product_rating = explode("*-*", $all_ret);
											$avg_rating = explode("*-*", $all_ret);
											//print_r($product_rating);

											$star1 	=$product_rating[0];
											$star2 	=$product_rating[1];
											$star3 	=$product_rating[2];
											$star4 	=$product_rating[3];
											$star5 	=$product_rating[4];
											$starall 	=$product_rating[5];
											$avt_rating = (1*($star1) + 2*($star2) +3*($star3) +4*($star4) + 5*($star5))/($starall);
											$avt_rating = number_format((float)$avt_rating, 1, '.', '');
											if(empty($avt_rating)){
												$avt_rating = 0;
											}
											for($i = 0; $i < $avt_rating; $i++){
												echo '<li class="active"><a href="#"></a></li>';
											}

											$dact = 5-$avt_rating;
											for($j = 0; $j < $dact; $j++){
												echo '<li><a href="#"></a></li>';
											}?>
											<li class="rc">(<?php echo $starall; ?> Rating)</li>
										</ul>
									</div>
								</div>
                                <div class="col-sm-2"><?php
									if($total_review > 0){?>
										<a href="#all_review"><?php echo $total_review; ?> Review </a><?php
									}else{?>
										<a><?php echo $total_review; ?> Review </a><?php
									}?>	
                                </div>
                                <div class="col-sm-5">
                                	<?php if($product->reward_points_earned)
									{?>
										<a href="#">You will earn <?php echo $product->reward_points_earned;?> E-Cash. (<i class="fa fa-inr"></i><?php echo (round($product->reward_points_earned/100)); ?>)</a>
									<?php  } ?>
                                </div>
                            </div>

                            <div class="compare-botton"><!--
								<a onclick="add_to_compare(<?php echo $product->id;?>);" style="cursor:pointer;">Add to Compare</a>-->
							</div>

							<div class="row">
								<div class="col-md-7">
									<div class="product-code">
										<span>Product Code :</span> <?php
										if(!empty($product->sku)):?><?php
											echo $product->sku; ?><?php
										endif;?>
									</div><?php

									if($product->publisher){?>
										<div class="product-brand">
											<span>Publisher :</span> <?php echo $product->publisher;?>
										</div><?php
									} ?><?php

									if($product->shop_by_brand){?>
										<div class="product-brand">
											<span>Brand :</span> <?php echo $product->shop_by_brand;?>
										</div><?php
									} ?><?php

									if($product->manufacturer){?>
										<div class="product-brand">
											<span>Manufacturer :</span> <?php echo $product->manufacturer;?>
										</div><?php
									} ?>

									<div class="availability">
										<span>Availability :</span><?php
										//if((bool)$product->track_stock && $product->quantity < 1 && config_item('inventory_enabled')):
										if($product->quantity < 1):?>
											<em class="color-red">Out of Stock</em><?php
										else:?>
											 <em class="color-green">In Stock</em><?php
										endif;?>
									</div><?php
									
									$total_gruped_product =  count($product->grouped_products);
									if($total_gruped_product <= 1){?>
										<div class="amount-box clearfix">
											<div class="row"><?php
												$discount = $product->price-$sales_price;
												if($discount>0){?>
													<div class="col-sm-12 col-md-3">
														<div class="mrp">
															<small>₹ <span id="porgprice"><?php echo round($product->price); ?></span></small>
															<input type="hidden" value="<?php echo $product->price; ?>" id="prd_org_prc" />
														</div>
													</div>
													<div class="col-sm-12 col-md-8">
														<div class="price">₹ <span id="psaleprice"><?php echo round($sales_price); ?></span>
															<small>(Save ₹ <span id="pdiscprice"><?php echo round($product->price-$sales_price);?></span>)
															</small>
															<input type="hidden" value="<?php echo $product->$sales_price;?>" id="prd_sale_prc" />
														</div>
													</div><?php
												}else if($discount==0){?>
													<!-- price or sale price is same -->
													<div class="col-sm-12 col-md-8">
														<div class="price">₹ <span id="psaleprice"><?php echo round($sales_price); ?></span>
															<input type="hidden" value="<?php echo $product->price; ?>" id="prd_org_prc" />
															<input type="hidden" value="<?php echo $sales_price;?>" id="prd_sale_prc" />
														</div>
													</div><?php
												}else{ ?>
													<div class="col-sm-12 col-md-11">
														<div class="mrp">
															<small>₹ <span><?php echo $product->price; ?></span>
																<input type="hidden" value="<?php echo $product->price;?>" id="prd_sale_prc" />
																<input type="hidden" value="<?php echo $product->price; ?>" id="prd_org_prc" />
																<span id="pdiscprice">
																	<?php echo 0;?>
																</span>
															</small>
														</div>
													</div><?php
												} ?>
											</div>
											<div class="selling-col"></div>
										</div><?php
									}?><?php 
									$total_gruped_product =  count($product->grouped_products);
									if($total_gruped_product <= 1){
										$prdid = $product->id;?>
										<?php echo form_open('cart/add_to_cart_ajax', 'class="form-horizontal" id="add-to-cart_'.$product->id.'" name="add-to-cart_'.$prdid.'" accept-charset="utf-8"');?>
											<input type="hidden" name="cartkey" value="<?php echo $this->session->flashdata('cartkey');?>" />
											<input type="hidden" name="id" value="<?php echo $product->id?>"/>
											<input type="hidden" name="saleprice_pricetier" value="<?php echo $sales_price;?>" id="saleprice_pricetier"/><?php
											if($product->quantity >= 1){?>												
												<div class="quant-control" style="margin:0px;">
													<a value="-" class="qtyminus btn-sm" field="quantity" ><i class="fa fa-minus"></i></a>
													<span class="quantity" style="position: relative;top:-3px;">
														<input type="text" name="quantity" value="1" id="cart_quantity" style="padding: 4px 5px;">
													</span>
													<a type="button" value="+" class="qtyplus btn-sm change" field="quantity"><i class="fa fa-plus"></i></a>
												</div>
												<div class="cart-button pull-left" id="val_zip">
													<button class="btn btn-sm btn-warning cart-button cart-button" type="button" value="submit" title="Add to Cart" id="aaddcartbut" style="margin:0px 15px;" onclick="addToCart(<?php echo $product->id;?>);"><i class="fa fa-shopping-cart"></i> Add To Cart</button>
												</div><?php
											}?>	
											
										</form><?php 
										if($this->Customer_model->is_logged_in(false, false)){?>
											<a onclick="add_to_compare(<?php echo $product->id;?>);" class="btn btn-sm btn-default pull-left"><span class="fa fa-refresh"></span></a>
											<a onclick="add_to_wishlist(<?php echo $product->id;?>);" class="btn btn-sm btn-default pull-left" style="margin-left:10px;"><span class="fa fa-heart-o"></span></a><?php
										}else{?>
											<a href="#" data-toggle="modal" data-target="#authentication" class="btn btn-sm btn-default pull-left"><span class="fa fa-refresh"></span></a>
											<a href="#" data-toggle="modal" data-target="#authentication" class="btn btn-sm btn-default pull-left" style="margin-left:10px;"><span class="fa fa-heart-o"></span></a><?php
										}?>	
									<?php
									}?>
									<div class="clearfix"></div>
									<div class="social-box">
										<div class="row">
											<div class="col-sm-12"><?php
												$title = $product -> name;
												$desc = $product -> description;
												$url = site_url($relate->slug);
												$photo  = theme_img('no_picture.png', 'No Image Available');
												$product->images   = array_values($product->images);
												
												if(!empty($product->images[0])){
													$primary    = $product->images[0];
													foreach($product->images as $photo){
														if(isset($photo->primary)){
															$primary    = $photo;
														}
													}

													$photo  = base_url('uploads/images/medium/'.$primary->filename);
												}
												$social_image =  $photo; ?>
												<a><strong>Share: </strong></a>
												<a title="Facebook" class="facebook share-click" style="cursor:pointer;">
													<span class="fa fa-facebook-square"></span>
												</a>

												<a class="twitter share-click" title="Twitter" style="cursor:pointer;">
													<span class="fa fa-twitter-square">
												</a>

												<a  title="google plus" class="google share-click" style="cursor:pointer;">
													<span class="fa fa-google-plus-square"></span>
												</a>

												<a  class="linkedin share-click" style="cursor:pointer;">
													<span class="fa fa-linkedin-square"></span>
												</a>
											</div>
										</div>
									</div><?php
									if(!empty($product->product_desc)){ ?>
										<div class="terms-box">
											<ul><?php
												if($product->product_desc){ ?>
													   <li><?php echo $product->product_desc;?></li><?php
												} ?>
											</ul>
										</div><?php
									}	?>
									</div>
									<div class="col-md-5"><?php
										if(!empty($product->price_tier)){?>
											<div class="offer_section">
												<h2>Offer</h2><?php
												$price_tier_array = json_decode($product->price_tier, true);
												if(!empty($price_tier_array)){
													$product->price_tier;
													$price_tiers = array();
													foreach($price_tier_array as $tier){
														$tier_quantity = $tier['quantity'];
														$tier_discount = $tier['discount_value'];
														$tier_type = $tier['discount_type'];
														$price_tiers[$tier_quantity]['quantity'] = $tier_quantity;
														$price_tiers[$tier_quantity]['discount'] = $tier_discount;
														$price_tiers[$tier_quantity]['type'] = $tier_type;
													}
													ksort($price_tiers);?>
													<p><?php
														foreach($price_tiers as $tier){
															$discount_value = '';

															if($tier['type'] == 'Flat Amount'){
																$discount_value = "Rs.".$tier['discount'];
															}else{
																$discount_value = $tier['discount']."%";
															}

															echo "Buy ".$tier['quantity']." and Above get ".$discount_value." OFF<br/>";
															echo "<input type='hidden' quantity='".$tier['quantity']."' tier_type='".$tier['type']."' discount_value='".$tier['discount']."' class='tier_price_hidden_value' id='pt".$tier['quantity']."'/>";
														}?>
													</p><?php
												}?>
											</div><?php
										}else if($product->is_pricetire_or_buyget == 2){?>
											<div class="offer_section">
												<h2>Free Product</h2><?php
												$buyconditions = json_decode($product->buy_get_condition);
												$pids = $buyconditions->p_id;
												$buys = $buyconditions->buy;
												$gets = $buyconditions->get;
												$froms = $buyconditions->from;
												$tos = $buyconditions->to;
												$total = sizeof($pids);
												$total = sizeof($pids);
												$conditionpid = array();
												
												for($i = 0; $i <= $total-1; $i++){
													$conditionpid[] = array(
														'free_product' 	=> $pids[$i],
														'buy_quantity' => $gets[$i],
														'free_quantity' 	=> $buys[$i],
														'from' 			=> $froms[$i],
														'to' 			=> $tos[$i]
													);
												}?>
												<p><?php
													foreach($conditionpid as $tier){
														$freeproduct = $this->Product_model->get_cart_ready_product($tier['free_product'], 1);
														$link = "<a href=".site_url(implode('/', $base_url).'/'.$freeproduct['slug']).">".$freeproduct['name']."</a>";
														echo "Buy ".$tier['buy_quantity']." and Get ".$tier['free_quantity']." ".$link." Free<br/>";
													}?>
												</p>
											</div><?php
										}else{?>
											<div style="min-height:120px;visibility:hidden;">sdfsd</div><?php	
										} ?>
										<div class="pin-box">
											<h4>Shipping, Delivery &amp; Payment options</h4>
											<div class="pin-check">
												<input type="text" name="pin" id="pincode" placeholder="Enter PIN Code">
												<input type="button" value="Check" onclick="checkzipcode();">
											</div>
											<div class="alert-box">
												<span class="color-green" id="chckpinmsg-green"></span>
												<span class="color-red" id="chckpinmsg-red"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				<!--group product -->
				<?php if(!empty($product->grouped_products)):?>
					<div class="product_tables" style="width:100%;">
						<table class="table table-bordered" style="width:150%;">
							<thead>
								<tr>
									<th class="col_1">#</th>
									<th class="col_1">P. Code</th>
									<th class="col_2">Product</th>
									<th class="col_3">Name</th>
									<th class="col_4" id="p_content">Package Contents</th>
									<th class="col_5" id="p_shade">Shade/ Type/Series</th>
									<th class="col_6" id="p_unit">Units</th>
									<th class="col_7" id="p_refill">Refill Type</th>
									<th class="col_8" id="p_color">Color</th>
									<th class="col_9" id="p_length">Length</th>
									<th class="col_10" id="p_size">Size</th>
									<th class="col_11" id="p_taper">Taper</th>
									<th class="col_12" id="p_part">Working Part</th>
									<th class="col_13">Price</th>
									<th class="col_14" >QTY</th>
									<th class="col_15" >Add to Cart</th>
							</tr>
						</thead>
						<tbody><?php
							$i         = 0;
							$p_content = 0;
							$p_shade   = 0;
							$p_unit    = 0;
							$p_refill  = 0;
							$p_color 	= 0;
							$p_length   = 0;
							$p_size    = 0;
							$p_taper  = 0;
							$p_part  = 0;
							foreach($product->grouped_products as $grouped):?><?php
								$photo  = theme_img('no_picture.png', 'No Image Available');
								$grouped->images = array_values((array)json_decode($grouped->images));
								if(!empty($grouped->images[0])){
										$primary    = $grouped->images[0];
										foreach($grouped->images as $photo){
											if(isset($photo->primary)){
												$primary    = $photo;
											}
										}

									$photo  = '<img src="'.base_url('uploads/images/thumbnails/'.$primary->filename).'" alt="'.$grouped->seo_title.'" style="width:50px;"/>';
								}
								$i++;?>
								<form action="<?php echo base_url().'cart/add_to_cart_ajax'?>" id="add-to-cart_<?php echo $grouped->id;?>" name="add-to-cart_<?php echo $grouped->id;?>" class="form-horizontal" method="post" accept-charset="utf-8" >
									<tr>
										<td ><?php echo $i;?></th>
										<td style="min-width:220px;"><?php echo $grouped->sku;?></th>
										<td class="col_3"><?php echo $photo;?></td>
										<td><?php echo $grouped->name;?></td><?php 
										if($grouped->package_contents){
											 $p_content=1;?>
											<td ><?php echo $grouped->package_contents;?></td><?php 
										}?>
										<?php if($grouped->shade){
											 $p_shade=1; ?>
											<td><?php echo $grouped->shade;?></td>
										<?php }?>

										<?php if($grouped->units){ $p_unit=1;?>
											<td><?php echo $grouped->units;?></td>
										<?php }?>
										<?php if($grouped->refill_type){ $p_refill=1;?>
											  <td><?php echo $grouped->refill_type;?></td>
										<?php }?>

										
										<?php
											if($grouped->color){?>
											<td><?php 
												$p_color=1;
												echo $grouped->color;?>
											</td> <?php } ?>
										<?php
											if($grouped->length){?>
											<td>
												<?php $p_length=1;
												echo $grouped->length;?>
												</td>
											<?php }?>
										
										<?php
											if($grouped->size){?>
												<td> <?php $p_size=1;
												echo $grouped->size;?>
												</td>
											<?php }?>
										<?php
											if($grouped->taper){?>
											<td>
												<?php $p_taper=1;
												echo $grouped->taper;?>
											</td>
											<?php }?>
										<?php
											if($grouped->working_part){?>
											<td>
										<?php	$p_part=1;
												echo $grouped->working_part;?>
											</td>
										<?php }?>
										<td class="col_6" style="width:130px;"><?php 
											$salesprice = $this -> dentkart -> product_price($grouped);
											$disc = $grouped->price - $salesprice;
											if($disc > 0):?>
												<span class="normal-price" style="float:left;">₹ <?php echo $grouped->price; ?></span>
												<span class="special-price" style="float:right;color:#EC7A08;">₹ <?php echo $salesprice; ?></span><br/><br/>
												<span class="discount">(Save ₹ <?php echo $grouped->price-$salesprice;?>)</span><?php 
											else: ?>
												<span class="special-price" style="color:#EC7A08;">₹ <?php echo $grouped->price; ?></span><?php 
											endif; ?>
										</td>
										<td>
											<input type="hidden"  name="id" value="<?php echo $grouped->id;?>">
											<input type="text"  name="quantity" maxlength="12" value="1" title="Qty" id="nan_<?php echo $grouped->id;?>" >
										</td>
										<td>
										<div class="add-button"><!--
											<a class="add_to_cart" type="button" href="<?php echo site_url(implode('/', $base_url).'/'.$grouped->slug); ?>" style="width:110px;border-radius:0px;">Add to Cart</a>-->
											<button class="add_to_cart" type="button" onclick="addToCart(<?php echo $grouped->id;?>);" style="width:110px;">Add to Cart</button>
										</div>
										</td>
									</tr>
								</form>
							<?php endforeach;?>
						</tbody><?php						
					   if(!$p_content){
					   		echo '<script>document.getElementById("p_content").style.display = "none";</script>';
					   }
					   if(!$p_color){
					   		echo '<script>document.getElementById("p_color").style.display = "none";</script>';
					   }
					   if(!$p_length){
					   		echo '<script>document.getElementById("p_length").style.display = "none";</script>';
					   }
					   if(!$p_size){
					   		echo '<script>document.getElementById("p_size").style.display = "none";</script>';
					   }
					   if(!$p_taper){
					   		echo '<script>document.getElementById("p_taper").style.display = "none";</script>';
					   }
					   if(!$p_part){
					   echo '<script>document.getElementById("p_part").style.display = "none";</script>';
					   }
					   ;
						if(!$p_shade){

							echo '<script>document.getElementById("p_shade").style.display = "none";</script>';
						}
						if(!$p_unit){ echo '<script>document.getElementById("p_unit").style.display = "none";</script>';}
						if(!$p_refill){

							echo '<script>document.getElementById("p_refill").style.display = "none";</script>';
						}

							?>
					</table>
				</div>

				<?php endif;?>
                <!--Combo start here--><?php
				if(!empty($product->attech_combo_products)):?>
					<div class="row">
                        <div class="col-lg-12">
                            <hr class="hr-line">
                        </div>
                    </div>
                	<h4>RECOMMENDED COMBOS FOR <span style="color:#EC7A08;"><?php echo $product -> name; ?></span></h4>
					<div class="row">
                        <div class="col-lg-12">
                            <div class="product-description">
                                <ul class="nav nav-tabs"><?php
									$j=0;
									foreach($product->attech_combo_products as $attech_combo): $j++;?>
										<li <?php if($j==1){ echo 'class="active"';}?> >
											<a data-toggle="tab" href="#<?php echo $attech_combo->id;?>"><?php echo $attech_combo->name;?></a>
										</li><?php
									endforeach;?>
                                </ul>

                                <div class="tab-content"><?php
									$k=0;
									foreach($attech_combo_product as $combo_product_key=>$combo_product_value):
										$k++;?>
										<div id="<?php echo $combo_product_key;?>" class="tab-pane fade in <?php if($k==1){ echo 'active';}?>">
											<div class="content-inner">
												<div class="combo-wrapper">
													<div class="row">
														<div class="col-md-9 col-lg-8">
															<div class="row">
																<form action="<?php echo base_url().'cart/add_to_cart_ajax'?>" id="add-to-cart_<?php echo $combo_product_key;?>" name="add-to-cart_<?php echo $combo_product_key;?>" class="form-horizontal" method="post" accept-charset="utf-8"> 
																	<input type="hidden"  name="quantity" maxlength="12" value="3" title=" Qty" class="form-control qty" style="width:50px;"><?php
																	$total_combo_price=0;
																	$i=0;
																	foreach($combo_product_value->combo_products as $arrCombo){
																		$i++;
																		$photo  = theme_img('no_picture.png', 'No Image Available');

																		$arrCombo->images = array_values((array)json_decode($arrCombo->images));

																		if(!empty($arrCombo->images[0])){
																			$primary    = $arrCombo->images[0];
																			foreach($arrCombo->images as $photo){
																				if(isset($photo->primary)){
																					$primary    = $photo;
																				}
																			}

																			$photo  = '<img src="'.base_url('uploads/images/thumbnails/'.$primary->filename).'" alt="'.$arrCombo->seo_title.'" style="height: 130px;"/>';
																		}
																		$total_combo_price = $total_combo_price+$arrCombo->saleprice;?>
																		<div class="col-xs-6 col-sm-3 col-lg-3">
																			<div class="book-col">
																				<div class="boughttogether">
																					<input type="checkbox" name="id[]" value="<?php echo $arrCombo->id;?>" rel="<?php echo $arrCombo->saleprice;?>" checked onclick="combo_price(<?php echo $combo_product_key;?>);">
																					<div class="book-image">
																						<div class="book-inner"><?php
																							echo $photo;?>
																						</div>

																					</div>
																					<div class="book-title">
																						
																						<label for="book11"><?php echo $arrCombo->name;?></label>
																					</div>
																					<div class="book-price">₹ <?php echo $arrCombo->saleprice;?></div>
																				</div>
																				<div class="plus-icon" <?php if($i<count($combo_product_value->combo_products)){echo "style='display:block;'"; }else{ echo "style='display:none;'";}?>>+</div>
																			</div>
																		</div><?php
																	} ?>
																</form>
															</div>
														</div>
														<div class="col-md-3 col-lg-3">
															<div class="boughttogether-cart">
																<div class="price" id="psaleprice">
																	<span style="display:none;">Rs. 765.00</span>
																	₹ <span id="add-to-cart_combo_<?php echo $combo_product_key;?>"><?php echo $total_combo_price;?></span>
																</div>
																<div class="disc" style="display:none;">
																	<p>20% discount.</p>
																	<p>₹ 153.00 discount on buying this offer.</p>
																</div>
																<div class="clearfix"></div>
																<div class="add-button">
																	<a id="add-to-cart_combo_btn_<?php echo $combo_product_key;?>" type="button" onclick="addToCart(<?php echo $combo_product_key;?>);" style="cursor:pointer;">Add to Cart</a>
																</div>
																<div class="clearfix"></div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div><?php
									endforeach;?>
                                </div>
                            </div>
                        </div>
                    </div>
                <!--Combo end here--><?php
				endif;?>

				<!--product description tab -->
                <div class="row" id="product_details_tabbed">
                	<div class="col-lg-12">
                		<div class="product-description">
                            <div class="wrapper">
                            	<ul class="nav nav-tabs">
									<li class="active"><a data-toggle="tab" href="#menu1">Product Details </a></li>
									<li><a data-toggle="tab" href="#menu2">Package Contents</a></li>
									<li><a data-toggle="tab" href="#menu3">Aditional information</a></li>
									<li><a data-toggle="tab" href="#menu4">Downloads</a></li>
                                </ul>

                                <div class="tab-content">
									<div id="menu1" class="tab-pane fade in active">
										<div class="content-inner">
											<?php echo $product->description;
											$discription =$product->description;?>
										</div>
									</div>
									<div id="menu2" class="tab-pane fade">
										<div class="content-inner">
											<?php echo $product->package_contents_desc; ?>
										</div>
									</div>
									<div id="menu3" class="tab-pane fade">
										<div class="content-inner">
											<?php echo $product->aditional_information; ?>
										</div>
									</div>
									<div id="menu4" class="tab-pane fade">
										<div class="content-inner">
											<a href="<?php echo base_url('uploads/digital_uploads/'.$digital_product->filename);?>"><?php echo $digital_product->filename;?></a>
										</div>
									</div>
                                </div>
                            </div>
                    	</div>
                    </div>
                </div>

                <!--New Review Block -->
                <div class="top-rating-box">
                    <div class="row">
                        <div class="col-lg-12">
                            <hr class="hr-line">
                            <h4>Rating And Reviews</h4>
                        </div>
                    </div>

                    <div class="row"><?php
						$all_ret = $this->dentkart->product_rating($product -> id);
						$product_rating = explode("*-*", $all_ret);
						$avg_rating = explode("*-*", $all_ret);
						//print_r($product_rating);

						$star1 	=$product_rating[0];
						$star2 	=$product_rating[1];
						$star3 	=$product_rating[2];
						$star4 	=$product_rating[3];
						$star5 	=$product_rating[4];
						$starall 	=$product_rating[5];
						$avt_rating = (1*($star1) + 2*($star2) +3*($star3) +4*($star4) + 5*($star5))/($starall);
						$avt_rating = number_format((float)$avt_rating, 1, '.', '');
						if(empty($avt_rating)){
							$avt_rating = 0;
						}

						$bar5 = $star5 * 100 / $starall;
						$bar5 = number_format((float)$bar5, 1, '.', '');
						$bar4 = $star4 * 100 / $starall;
						$bar4 = number_format((float)$bar4, 1, '.', '');
						$bar3 = $star3 * 100 / $starall;
						$bar3 = number_format((float)$bar3, 1, '.', '');
						$bar2 = $star2 * 100 / $starall;
						$bar2 = number_format((float)$bar2, 1, '.', '');
						$bar1 = $star1 * 100 / $starall;
						$bar1 = number_format((float)$bar1, 1, '.', ''); ?>

                        <div class="col-sm-4 col-lg-4">
                            <div class="displayRating">
                                <div id="displayRating" class="yellow">
                                    <h2 style="position: absolute;top: 12%;left: 31%;z-index: 2;color: #333;width: 50px;text-align: center;"><?php echo $avt_rating; ?></h2>
                                </div>
                            </div>
							<script>
							$("#displayRating").percircle({
								text:"out of 5",
								percent: <?php echo $avt_rating * 20; ?>
							});
							</script>
                            <div class="clearfix"></div>
                            <div class="rating-info">
                                <h4>Based on <strong><span><?php echo $starall;?></span></strong> ratings</h4>
                                <div class="row gutter-10">
                                    <div class="col-xs-2 col-sm-2 col-lg-2">
                                        <p>5 Star</p>
                                    </div>
                                    <div class="col-xs-7 col-sm-7 col-lg-7">
                                        <div class="rating-bar" style="width:<?php echo $bar5; ?>%;"></div>
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-lg-3">
                                        <p><?php echo $star5;?></p>
                                    </div>
                                </div>
								<div class="row gutter-10">
                                    <div class="col-xs-2 col-sm-2 col-lg-2">
                                        <p>4 Star</p>
                                    </div>
                                    <div class="col-xs-7 col-sm-7 col-lg-7">
                                        <div class="rating-bar" style="width:<?php echo $bar4; ?>%;"></div>
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-lg-3">
                                        <p><?php echo $star4;?></p>
                                    </div>
                                </div>
                                <div class="row gutter-10">
                                    <div class="col-xs-2 col-sm-2 col-lg-2">
                                        <p>3 Star</p>
                                    </div>
                                    <div class="col-xs-7 col-sm-7 col-lg-7">
                                        <div class="rating-bar" style="width:<?php echo $bar3; ?>%;"></div>
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-lg-3">
                                        <p><?php echo $star3;?></p>
                                    </div>
                                </div>
                                <div class="row gutter-10">
                                    <div class="col-xs-2 col-sm-2 col-lg-2">
                                        <p>2 Star</p>
                                    </div>
                                    <div class="col-xs-7 col-sm-7 col-lg-7">
                                        <div class="rating-bar" style="width:<?php echo $bar2; ?>%;"></div>
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-lg-3">
                                        <p><?php echo $star2;?></p>
                                    </div>
                                </div>
                                <div class="row gutter-10">
                                    <div class="col-xs-2 col-sm-2 col-lg-2">
                                        <p>1 Star</p>
                                    </div>
                                    <div class="col-xs-7 col-sm-7 col-lg-7">
                                        <div class="rating-bar" style="width:<?php echo $bar1; ?>%;"></div>
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-lg-3">
                                        <p><?php echo $star1;?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-lg-4"><?php
							$all_rec = $this->dentkart->recommend_items($product -> id);
							$recomendation_array = explode("*-*", $all_rec);
							$yes_rec 	=$recomendation_array[0];
							$no_rec 	=$recomendation_array[1];
							$all_rec 	=$recomendation_array[2];
							$perc = ($yes_rec / $all_rec) * 100;
							$perc = (int)$perc;?>
							<div class="displayRating">
								<div id="recommendRating" class="green" >
									<h2 class="recommendations_val" style="position: absolute;top: 12%;left: 7%;z-index: 2;color: #333;width: 100px;text-align: center;"><?php echo $perc; ?>%</h2>
								</div>
                            </div>
							<script>
								$("#recommendRating").percircle({
									text:"RECOMMEND",
									percent: <?php echo $perc; ?>
								});
							</script>
                            <div class="clearfix"></div>
                            <div class="rating-info">
                                <h5>Based on <strong><span class="recommendations_val"><?php echo $all_rec; ?></span></strong> recommendations.</h5>
                                <h4>Would you recommend this item?</h4>
                                <div class="text-center"><?php
									$this->load->library('session');
									$cust_id = $this->customer['id'];
									if(empty($cust_id)){?>
										<a href="#" data-toggle="modal" data-target="#authentication" class="btn btn-success btn-sm">YES</a>
										<a href="#" data-toggle="modal" data-target="#authentication" class="btn btn-success btn-sm">No</a><?php
									}else{?>
										<a onclick="recommendation(1,<?php echo $product -> id; ?>)" class="btn btn-success btn-sm">YES</a>
										<a onclick="recommendation(0,<?php echo $product -> id; ?>)" class="btn btn-success btn-sm">No</a><?php
									}?>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-lg-4"><?php
							$user_rate = $this->dentkart->user_rating($product -> id);
							if(empty($user_rate)){
								$user_rate = 0;
							} ?>
                            <div class="displayRating">
                                <div id="rateIt" class="blue">
                                    <h2 style="position: absolute;top: 12%;left: 42% !important;z-index: 2;color: #333;"><?php echo $user_rate; ?></h2>
                                </div>
                            </div>
							<script>
							$("#rateIt").percircle({
								text:"Star",
								percent: <?php echo $user_rate * 20; ?>
							});
							</script>
                            <div class="clearfix"></div>
							<div class="rating-info">
                                <input id="rating-input" />
								<input type="hidden" name="prdid" id="prdid" value="<?php echo $product -> id; ?>" />
                                <h4>Have you purchased this item?</h4>
                                <div class="text-center"><?php
								if(empty($cust_id)){?>
									<a href="#" data-toggle="modal" data-target="#authentication" class="btn btn-warning	btn-sm">WRITE A REVIEW</a><?php
								}else{?>
									<a onclick="review_form();" class="btn btn-warning	btn-sm">WRITE A REVIEW</a><?php
								}?>

                                </div>
                            </div><script>
								$(document).ready(function(){
									$("#addcartbut").click(function(){
										$("#addcartform").submit();
									}); 
									
									$('#rating-input').rating({
										update: <?php echo $user_rate; ?>,
										min: 1,
										max: 5,
										step: 1,
										size: 'xs',
										showClear: false,
										starCaptions: {
											1:'Disappointing',
											2:'Bad',
											3:'Average',
											4:'Good',
											5:'Perfect'
										}
									});

									$('#rating-input').rating({
										update: <?php echo $user_rate; ?>,
										size: 'xs',
										showClear: false,
										starCaptions: {
											3:'Average'
										}
									});
								});


								//var ratingValue = $('#rating-input').rating('update', 3).val();
								//alert(ratingValue);
								</script>
                        </div>
                    </div><?php
					if(count($total_review) > 0){?>
						<div class="review-display" id="all_review">
							<div class="row">
								<div class="col-sm-5">
									<h5><?php
										echo 'Displaying Reviews 1-10 of '.$total_review; ?>
									</h5>
								</div>
								<div class="col-sm-4">
									<ul class="sort-by-box">
										<li>Sort by:</li>
										<li class="active"><a data-toggle="tab" href="#helpfull">Most Helpfull</a></li>
										<li><a data-toggle="tab" href="#recent">Most Recent</a></li>
									</ul>
								</div>
								<div class="col-sm-3">
									<ul class="sort-by-box pull-right">
										<li>Filter by:</li>
										<li>
											<select class="form-control" id="sort_products" onchange="window.location='<?php echo base_url().$product -> slug; ?>?'+$(this).val();" style="height:26px;">
												<option value=''><?php echo 'All Stars';?></option>
												<option <?php echo(!empty($_GET['star']) && $_GET['star']=='1')?' selected="selected"':'';?> value="&star=1"><?php echo '1 Star';?></option>
												<option <?php echo(!empty($_GET['star']) && $_GET['star']=='2')?' selected="selected"':'';?>  value="&star=2"><?php echo '2 Star';?></option>
												<option <?php echo(!empty($_GET['star']) && $_GET['star']=='3')?' selected="selected"':'';?>  value="&star=3"><?php echo '3 Star';?></option>
												<option <?php echo(!empty($_GET['star']) && $_GET['star']=='4')?' selected="selected"':'';?>  value="&star=4"><?php echo '4 Star';?></option>
												<option <?php echo(!empty($_GET['star']) && $_GET['star']=='5')?' selected="selected"':'';?>  value="&star=5"><?php echo '5 Star';?></option>
											</select>
										</li>
									</ul>
								</div>
							</div>

							<div class="tab-content">
								<div id="helpfull" class="tab-pane fade in active"><?php
									if(true){
										foreach($product_review_helpful as $prd_reviews){?>
											<div class="user-comment-box"><?php
												$user_product_data = $this->dentkart->user_product_data($prd_reviews->user_id);
												$fname = $user_product_data[0] -> firstname;?>
												<div class="row">
													<div class="col-xs-3 col-sm-2">
														<div class="user-col">
															<div class="user-photo"><?php
																if(empty($user_product_data[0] -> image)){
																	if(empty($fname)){
																		echo "H";
																	}else{
																		echo substr($fname, 0, 1);
																	}
																}else{
																	echo '<img src="'.base_url().'uploads/customer_image/small/'.$user_product_data[0] -> image.'" alt="Customer Images" style="width:70px;height:70px;" class="img-circle">';
																}?>
															</div>
															<h6><?php echo ucfirst($fname);?></h6>
															<div class="buyer"><span class="fa fa-shield"></span> Verified By Admin</div>
														</div>
													</div>
													<div class="col-xs-9 col-sm-10">
														<div class="user-article">
															<div class="row">
																<div class="col-sm-6">
																	<div class="rating-box pull-left">
																		<ul class="clearfix"><?php
																			$user_review_rate = $this->dentkart->review_user_rating($prd_reviews -> product_id, $prd_reviews -> user_id);

																			if(empty($user_review_rate)){
																				$user_review_rate = 0;
																			}

																			$active = $user_review_rate;
																			$nonactive = 5-$user_review_rate;
																			for ($x = 1; $x <= $active; $x++){
																				echo '<li class="active"><a ></a></li>';
																			}

																			for ($x = 1; $x <= $nonactive; $x++){
																				echo '<li ><a ></a></li>';
																			}?>
																		</ul>
																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="date-show pull-right"><?php
																		$date = strtotime($prd_reviews->reviewdate);
																		echo date("F jS, Y", $date);?></div>
																	</div>
																</div>
															<div class="clearfix"></div>
															<h5><?php echo ucfirst($prd_reviews->title);?></h5>
															<p><?php echo ucfirst($prd_reviews->review);?></p>
															<p class="help-question">Was this review helpful? &nbsp; <a class="btn btn-sm btn-default" href="<?php echo base_url(); ?>ratenreview_controller/ishelpful_review/?id=<?php echo $prd_reviews->id;?>">Yes</a>
															<span><?php echo $prd_reviews->is_helpful;?></span></p>
														</div>
													</div>
												</div>
											</div><?php
										}
									}?>
								</div>
								<div id="recent" class="tab-pane fade in"><?php
										if(true){
											foreach($product_review_recent as $prd_reviews){?>
												<div class="user-comment-box"><?php
												$user_product_data = $this->dentkart->user_product_data($prd_reviews->user_id);
												$fname = $user_product_data[0] -> firstname;?>
												<div class="row">
													<div class="col-xs-3 col-sm-2">
														<div class="user-col">
															<div class="user-photo"><?php
																if(empty($user_product_data[0] -> image)){
																	if(empty($fname)){
																		echo "H";
																	}else{
																		echo substr($fname, 0, 1);
																	}
																}else{
																	echo '<img src="'.base_url().'uploads/customer_image/small/'.$user_product_data[0] -> image.'" alt="Customer Images" style="width:70px;height:70px;" class="img-circle">';
																}?>
															</div>
															<h6><?php echo ucfirst($fname);?></h6>
															<div class="buyer"><span class="fa fa-shield"></span> Verified Buyer</div>
														</div>
													</div>
													<div class="col-xs-9 col-sm-10">
														<div class="user-article">
															<div class="row">
																<div class="col-sm-6">
																	<div class="rating-box pull-left">
																		<ul class="clearfix"><?php
																			$user_review_rate = $this->dentkart->review_user_rating($prd_reviews -> product_id, $prd_reviews -> user_id);

																			if(empty($user_review_rate)){
																				$user_review_rate = 0;
																			}

																			$active = $user_review_rate;
																			$nonactive = 5-$user_review_rate;
																			for ($x = 1; $x <= $active; $x++){
																				echo '<li class="active"><a ></a></li>';
																			}

																			for ($x = 1; $x <= $nonactive; $x++){
																				echo '<li ><a ></a></li>';
																			}?>
																		</ul>
																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="date-show pull-right"><?php
																		$date = strtotime($prd_reviews->reviewdate);
																		echo date("F jS, Y", $date);?></div>
																	</div>
																</div>
															<div class="clearfix"></div>
															<h5><?php echo ucfirst($prd_reviews->title);?></h5>
															<p><?php echo ucfirst($prd_reviews->review);?></p>
															<p class="help-question">Was this review helpful? &nbsp; <a class="btn btn-sm btn-default" href="<?php echo base_url(); ?>ratenreview_controller/ishelpful_review/?id=<?php echo $prd_reviews->id;?>">Yes</a>
															<span><?php echo $prd_reviews->is_helpful;?></span></p>
														</div>
													</div>
												</div>
											</div><?php
										}
									}?>
								</div>
							</div>
						</div><?php
					}else{
						echo "<br/>Become first Customer for review this product ";
						$this->load->library('session');
						$cust_id = $this->customer['id'];
						if(empty($cust_id)){?>
							<a href="#" data-toggle="modal" data-target="#authentication" class="btn btn-warning	btn-sm">WRITE A REVIEW</a><?php
						}else{?>
							<a onclick="review_form();" class="btn btn-warning	btn-sm">WRITE A REVIEW</a><?php
						}
					} ?>
				</div>
				<!--/New Review Block --><?php
				if($product->related_products){ ?>
					<div class="bought-block">
						<div class="row">
							<div class="col-lg-12">
								<hr class="hr-line">
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-lg-10">
								<h4>RELATED PRODUCTS</h4>
							</div>
							<div class="col-lg-2">
							   <div id="bookPager" class="pull-right">
									<span id="book-prev"></span>
									<span id="book-next"></span>
								</div>
							</div>
						</div>
						<div class="medical-books">
							<div class="row gutter-0" id="booksSlider1"><?php
								foreach($product->related_products as $relate1):?><?php
									$photo1  = theme_img('no_picture.png', 'No Image Available');
									$arrimages = array_values((array)json_decode($relate1->images));
									if(!empty($arrimages[0])){
										$primary1    = $arrimages[0];
										foreach($arrimages as $photo1){
											if(isset($photo->primary)){
												$primary1    = $photo1;
											}
										}

										$photo1  = '<img src="'.base_url('uploads/images/thumbnails/'.$primary1->filename).'" alt="'.$relate1->seo_title.'"/>';
									} ?>
									<div class="col-sm-3" >
										<div class="product-block"><?php
											$salesprice = $this -> dentkart -> product_price($relate1); ?>
											<div class="off-box"><?php echo $discount=round(100*($relate1->price-$salesprice)/$relate1->price);?>% OFF</div>
											<div class="image-box">
												<a href="<?php echo site_url($relate1->slug); ?>"><?php echo $photo1; ?></a>
											</div>
											<div class="product-title">
												<a href="<?php echo site_url($relate1->slug); ?>"><?php echo $relate1->name;?></a>
											</div>
											<div class="price-box clearfix"><?php
												if($relate1->saleprice > 0):?>
													 <div class="mrp-box">₹ <?php echo $relate1->price; ?></div>
													 <div class="total-box">₹ <?php echo $salesprice; ?></div><?php
												else: ?>
													<div class="total-box">₹ <?php echo $relate1->price; ?></div><?php
												endif; ?>
											</div>
										</div>
									</div><?php
								endforeach;?>
							</div>
						</div>
					</div><?php
				} ?>
				<!--CUSTOMERS WHO BOUGHT THIS PRODUCT ALSO BOUGHT -->
			</div>
		</div>
	</div>

	<!-- recent product started -->
	<div class="row">
        <div class="col-lg-12">
            <hr class="hr-line">
        </div>
    </div>

	<div class="row">
    	<div class="col-lg-12">
        	<div class="medical-books">
            	<h4>YOU RECENTLY VIEWED </h4>
                <div class="row gutter-0" id="booksSlider3"><?php
                    foreach($product_recent as $prd_recent){?>
                        <div class="col-lg-6">
							<div class="product-block"><?php
								$sales_price = $this -> dentkart -> product_price($prd_recent);
								$discount=round(100*($prd_recent->price-$sales_price )/$prd_recent->price);?>
								<?php if($discount){?>
								<div class="off-box"><?php echo $discount;?>% off</div>
								<?php } ?>
									<div class="image-box"><?php
									$images = json_decode($prd_recent -> images, true);
									$images = array_values($images);
									echo $filename = $images[0]['filename'];?>
									<a href="<?php echo base_url().$prd_recent->slug;?>">
										<img src="<?php echo base_url().'uploads/images/small/'.$filename;?>" alt="<?php echo $prd_recent->name;?>">
									</a>
								</div>
								<div class="product-title">
									<a href="<?php echo base_url().$prd_recent->slug;?>"><?php echo $prd_recent->name;?></a>
								</div>
								
								<div class="price-box clearfix">
									<?php if($discount > 0):?>
											<div class="mrp-box">₹ <?php echo $prd_recent->price; ?></div>
											<div class="total-box">₹ <?php echo $sales_price ; ?></div><?php
										else: ?>
											<div class="total-box">₹ <?php echo $prd_recent->price; ?></div><?php
										endif; ?>
								</div>
							</div>
						</div><?php
					}	?>
					<div class="slider-pager-1 text-center">
						<span id="book-prev1"></span>
						<span id="book-next1"></span>
					</div>
                </div>
            </div>
        </div>
    </div>
</section>


<script src="<?php echo HTTP_JS_PATH; ?>jquery.tabbedcontent.min.js"></script>
<script src="<?php echo HTTP_JS_PATH; ?>multizoom.js"></script>
<script src="<?php echo HTTP_JS_PATH; ?>jquery.spin.js"></script>
<script src="<?php echo HTTP_JS_PATH; ?>jquery.blockUI.js"></script>

<script>
function new_tier_price(currentVal){
	if(currentVal < 1){
		salseval = $("#prd_sale_prc").val();
		single_prd_price = salseval;
		$("#saleprice_pricetier").val(Math.round(single_prd_price));
		return false;
	}else{
		if($('.tier_price_hidden_value').length > 0){
			fval = ''
			$('.tier_price_hidden_value').each(function(){
				fval = $(this).attr('quantity');
				salseval = $("#prd_sale_prc").val();
				single_prd_price = salseval;
				$("#saleprice_pricetier").val(Math.round(single_prd_price));
				return false;
			});
			if(currentVal < fval){
				orgval = $("#prd_org_prc").val();
				salseval = $("#prd_sale_prc").val();
				single_prd_price = salseval;
				$("#psaleprice").text(Math.round(single_prd_price));
				totaldiscount = orgval-single_prd_price;
				$("#pdiscprice").text(Math.round(totaldiscount));
				$("#saleprice_pricetier").val(single_prd_price);
			}else{
				$('.tier_price_hidden_value').each(function(){
					quantity = $(this).attr('quantity');
					if(quantity > currentVal){
						return false;
					}else{
						last_index = quantity;
					}
				});

				quantity = $("#pt"+last_index).attr('quantity');
				dsc_value = $("#pt"+last_index).attr('discount_value');
				dsc_type = $("#pt"+last_index).attr('tier_type');

				orgval = $("#prd_org_prc").val();
				salseval = $("#prd_sale_prc").val();

				temp = salseval * currentVal;

				if(dsc_type == 'Discount Amount'){
					discamt = temp * dsc_value / 100;
					newtemp = temp - discamt;
				}else if(dsc_type == 'Flat Amount'){
					newtemp = temp - dsc_value;
				}
				single_prd_price = newtemp / currentVal;
				$("#psaleprice").text(Math.round(single_prd_price));
				totaldiscount = orgval-single_prd_price;
				$("#pdiscprice").text(Math.round(totaldiscount));
				$("#saleprice_pricetier").val(Math.round(single_prd_price));
				//pdiscprice
			}
		}
	}
}


jQuery(document).ready(function(){

	$("#cart_quantity").change(function(){
		currentVal = $(this).val();
		if (!isNaN(currentVal)) {
			new_tier_price(currentVal);
		}
	});

    // This button will increment the value
    $('.qtyplus').click(function(e){
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('input[name='+fieldName+']').val());
		ncurrentVal = currentVal;
        // If is not undefined
        if (!isNaN(currentVal)) {
            // Increment
			ncurrentVal = currentVal + 1;
            $('input[name='+fieldName+']').val(currentVal + 1);
        } else {
            // Otherwise put a 0 there
            $('input[name='+fieldName+']').val(0);
			currentVal = 0;
        }

		new_tier_price(ncurrentVal);
    });
    // This button will decrement the value till 0
    $(".qtyminus").click(function(e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('input[name='+fieldName+']').val());
        ncurrentVal = currentVal;
		// If it isn't undefined or its greater than 0
        if (!isNaN(currentVal) && currentVal > 0) {
            // Decrement one
			ncurrentVal = currentVal - 1;
            $('input[name='+fieldName+']').val(currentVal - 1);
        } else {
			ncurrentVal = 0;
            // Otherwise put a 0 there
            $('input[name='+fieldName+']').val(0);
        }
		new_tier_price(ncurrentVal);
    });


	/*
	$("#displayRating").percircle({
		text:"out of 5",
		percent: 68
	});
	$("#recommendRating").percircle({
		text:"RECOMMEND",
		percent: 80
	});
	$("#rateIt").percircle({
		text:"Star",
		percent: 20
	});

	$('#rating-input').rating({
	  min: 1,
	  max: 5,
	  step: 1,
	  size: 'xs',
	  showClear: false,
	  starCaptions: {1:'Disappointing', 2:'Bad', 3:'Average', 4:'Good', 5:'Perfect'}
   });*/
	$('#image1').addimagezoom({ // single image zoom
		zoomrange: [3, 10],
		magnifiersize: [300,300],
		magnifierpos: 'right',
		cursorshade: true,
		largeimage: 'hayden.jpg' //<-- No comma after last option!
	})


	$('#image2').addimagezoom() // single image zoom with default options

	$('#multizoom1').addimagezoom({ // multi-zoom: options same as for previous Featured Image Zoomer's addimagezoom unless noted as '- new'
		descArea: '#description', // description selector (optional - but required if descriptions are used) - new
		speed: 1500, // duration of fade in for new zoomable images (in milliseconds, optional) - new
		descpos: true, // if set to true - description position follows image position at a set distance, defaults to false (optional) - new
		imagevertcenter: true, // zoomable image centers vertically in its container (optional) - new
		magvertcenter: true, // magnified area centers vertically in relation to the zoomable image (optional) - new
		zoomrange: [3, 10],
		magnifiersize: [300,300],
		magnifierpos: 'right',
		cursorshade: true //<-- No comma after last option!
	});

	$('#multizoom2').addimagezoom({ // multi-zoom: options same as for previous Featured Image Zoomer's addimagezoom unless noted as '- new'
		descArea: '#description2', // description selector (optional - but required if descriptions are used) - new
		disablewheel: true // even without variable zoom, mousewheel will not shift image position while mouse is over image (optional) - new
				//^-- No comma after last option!
	});

	$('#booksSlider1').bxSlider({
	  //auto: true,
	  //mode: 'vertical',
	  minSlides: 2,
	  maxSlides: 6,
	  slideWidth: 410,
	  slideMargin: 15,
	  pager: false,
	  controls: true,
	  nextSelector: '#book-next',
	  prevSelector: '#book-prev',
	  nextText: 'Onward →',
	  prevText: '← Go back'
	});
	$('#booksSlider2').bxSlider({
	  //auto: true,
	  minSlides: 2,
	  maxSlides: 5,
	  slideWidth: 220,
	  slideMargin: 15,
	  pager: false,
	  controls: true,
	});
	$('#booksSlider3').bxSlider({
	  //auto: true,
	  //mode: 'vertical',
	  minSlides: 2,
	  maxSlides: 6,
	  slideWidth: 175,
	  slideMargin: 15,
	  pager: false,
	  controls: true,
	  nextSelector: '#book-next1',
	  prevSelector: '#book-prev1',
	  nextText: 'Onward →',
	  prevText: '← Go back'
	});

})
</script>


<script>
if($(window).width() > 768){
	// Hide all but first tab content on larger viewports
	$('.accordion__content:not(:first)').hide();

	// Activate first tab
	$('.accordion__title:first-child').addClass('active');
} else {
	// Hide all content items on narrow viewports
	$('.accordion__content').hide();
};

// Wrap a div around content to create a scrolling container which we're going to use on narrow viewports
//$( ".accordion__content" ).wrapInner( "<div class='overflow-scrolling'></div>" );
// The clicking action
$('.accordion__title').on('click', function() {
	$('.accordion__content').hide();
	$(this).next().show().prev().addClass('active').siblings().removeClass('active');
});


</script>

<script>
	var tabs;
	jQuery(function($) {
		tabs = $('.tabscontent').tabbedContent({loop: true}).data('api');
		// switch to tab...
		/*$('a[href=#click-to-switch]').on('click', function(e) {
			var tab = prompt('Tab to switch to (number or id)?');
			if (!tabs.switchTab(tab)) {
				alert('That tab does not exist :\\');
			}
			e.preventDefault();
		});*/

		// Next and prev actions
		$('.controls a').on('click', function(e) {
			var action = $(this).attr('href').replace('#', '');
			tabs[action]();
			e.preventDefault();
		});
	});

	function combo_price(id){
		//alert("add-to-cart_"+id);
		var total_combo=0;
		var number_of_checked = 0;
		$("#add-to-cart_"+id+" input:checkbox:checked").each(function() {
			 total_combo=total_combo+parseInt($(this).attr('rel')); // do your staff with each checkbox
			 number_of_checked += 1;
		});
		$('#add-to-cart_combo_'+id).html(total_combo);
		//change quantity of combo box
		$("#add-to-cart_"+id+" input[name=quantity]").val(number_of_checked);
		
		if(total_combo==0){
			$('#add-to-cart_combo_btn_'+id).attr('disabled','disabled');
		}else{
			$('#add-to-cart_combo_btn_'+id).removeAttr('disabled');
		}
	}

	var product_url = "<?php echo site_url(implode('/', $base_url).'/'.$product->slug);?>";
	var image_url = "<?php echo $social_image;?>";
	var title = "<?php echo $product->name;?>";
	var descpp = '<?php echo $product->excerpt; ?>';
	var ecash_prd_id = '<?php echo $product->id; ?>';
	var is_user_login = '<?php if($this->Customer_model->is_logged_in(false, false)){ echo 'yes';}else{ echo 'no';}?>'
	$('.share-click').click(function(event) {
		var shareName = $(this).attr('class').split(' ')[0]; //get the first class name of clicked element
		if(shareName == 'facebook'){
			/*var openLink = "https://www.facebook.com/dialog/feed?app_id=1608683706088621&link="+product_url+"&picture="+image_url+"&description="+descpp+"&title="+title+"&redirect_uri=http://www.facebook.com/";
			winWidth    = 650;
			winHeight   = 450;
			winLeft     = ($(window).width()  - winWidth)  / 2,
			winTop      = ($(window).height() - winHeight) / 2,
			winOptions   = 'width='  + winWidth  + ',height=' + winHeight + ',top='    + winTop    + ',left='   +winLeft;

			window.open(openLink,'Share',winOptions);*/
			
			$.ajaxSetup({ cache: true });
				$.getScript('//connect.facebook.net/en_US/sdk.js', function(){
				FB.init({
				  appId: '1608683706088621', //replace with your app ID
				  version: 'v2.3'
				});
				FB.ui({
					method: 'share',
					title: title,
					href: product_url,
					picture: image_url,
					caption: "Dentkart",
					description: descpp
				  },
				  function(response) {
					if (response && !response.error_code) {
						if(is_user_login == 'yes'){
							console.log('start');
							$.ajax({  					
								type: "POST",  					
								url: "<?php echo base_url().'ratenreview_controller/ecash_facebook_share' ?>",
								data: {'product_id':ecash_prd_id},
								beforeSend:function() {},					
								success: function(msg){
									//alert('msg');
								}
							});
							console.log('end');
						}
					}
				});
			});
			return false;
		}else if(shareName == 'twitter'){
			var openLink = 'http://twitter.com/home?status=' + encodeURIComponent(title+'@Dentkart');
			winWidth    = 650;
			winHeight   = 450;
			winLeft     = ($(window).width()  - winWidth)  / 2,
			winTop      = ($(window).height() - winHeight) / 2,
			winOptions   = 'width='  + winWidth  + ',height=' + winHeight + ',top='    + winTop    + ',left='   +winLeft;
			window.open(openLink,'Share',winOptions);
			return false;
		}else if(shareName == 'google'){
			var openLink = 'https://plus.google.com/share?url=' + encodeURIComponent(product_url+'@Dentkart'+title);
			winWidth    = 650;
			winHeight   = 450;
			winLeft     = ($(window).width()  - winWidth)  / 2,
			winTop      = ($(window).height() - winHeight) / 2,
			winOptions   = 'width='  + winWidth  + ',height=' + winHeight + ',top='    + winTop    + ',left='   +winLeft;
			window.open(openLink,'Share',winOptions);
			return false;
		}else if(shareName == 'linkedin'){
			var openLink = 'http://www.linkedin.com/shareArticle?mini=true&url='+encodeURIComponent(product_url+'@Dentkart'+title);
			winWidth    = 650;
			winHeight   = 450;
			winLeft     = ($(window).width()  - winWidth)  / 2,
			winTop      = ($(window).height() - winHeight) / 2,
			winOptions   = 'width='  + winWidth  + ',height=' + winHeight + ',top='    + winTop    + ',left='   +winLeft;
			window.open(openLink,'Share',winOptions);
			return false;
		}						
	});
</script><?php

function theme_img($uri, $tag=false){
	if($tag){
		return '<img src="'.theme_url('assets/assets/images/'.$uri).'" alt="'.$tag.'">';
	}else{
		return theme_url('assets/assets/images/'.$uri);
	}
}

function theme_url($uri){
	$CI =& get_instance();
	return $CI->config->base_url('/'.$uri);
}

$this->load->view('vwFooter'); ?>
<div class="col-md-12" style="padding:0px;">
	<div style="text-align:center;padding:5px;background-color:#f1f1f1;border-bottom:1px solid #ddd;font-size:18px;">
		<span style="font-weight:600;padding-left:0px;height:auto;float:none;padding-top:0px;">View Cart Details</span>
	</div>
	<?php
	$cartarray =  $this->natuur->contents();
	if(count($cartarray) > 0){
		$total = 0;
		foreach ($this->natuur->contents() as $cartkey=>$product):  $i++;?><?php
			$prdimag = json_decode($product['images'], true);
			$prdimag = json_decode(json_encode($prdimag), true);
			$filename = "";
			foreach($prdimag as $prdimgrow){
				$filename = $prdimgrow['filename'];
			}
			
			$prdtotal = $product['quantity'] *$product['saleprice'];
			$total += $prdtotal;?>
			<div class="col-md-12" style="padding:0px;margin-top:10px;border-bottom:1px solid #ececec;" id="prd<?php echo $cartkey; ?>">
				<div class="col-md-3" style="float:left;padding-left:0px;">
					<img src="<?php echo base_url().'uploads/images/small/'.$filename; ?>" alt="product name" style="max-height:78px;padding:5px;max-width:78px;">
				</div>
				<div class="col-md-9" style="padding:0px;padding: 0px 10px;">
					<small style="font-size:14px;"><b><?php echo $product['name']; ?></b></small>
					<p style="margin:3px 0px;"><?php echo $product['quantity']; ?> X  <?php echo $product['saleprice'];?>= Rs.<?php echo $prdtotal?>.00</small></p>
					<p style="cursor:pointer; margin:3px 0px; text-align:right; font-size:14px;float:right;padding-top:0px;height:auto;">
						<a onclick="remove_product('<?php echo site_url('cart/ajax_remove_item/'.$cartkey);?>');" style="height:auto;padding-top:0px;font-size:18px;float:right;padding-left:0px;min-width:auto;color:#337ab7;">&nbsp;<i class="fa fa-times" aria-hidden="true"></i></a>
						<a href="<?php echo site_url(implode('/', $base_url).'/'.$product['slug']); ?>" style="height:auto;padding-top:0px;font-size:18px;float:right;padding-left:0px;min-width:auto;color:#337ab7;"><small><i class="fa fa-pencil-square-o" aria-hidden="true"></i></small>&nbsp;|</a>
					</p>
				</div>
			</div><?php
		endforeach;?>	
		<!--second product end--->
		<div class="col-md-12" style="padding-top:5px; margin-top:10px; border-top:1px solid #fff;">
			<span style="float:left;padding-left:0px;height:auto;font-size:16px;">Total: Rs <strong> <?php echo $total; ?>.00</strong></span><a href="<?php echo base_url()."cart/view_cart";  ?>" class="btn-info" style="background: #511E3E;border-radius:0px;border:0px;padding:8px 15px;float:right;color:#fff;text-align:center;">View Cart</a><?php
			if($this->Customer_model->is_logged_in(false, false)){?>
				<a href="<?php echo base_url()."cart/update_cart/checkout";  ?>" class="col-md-12" style="font-size:18px;text-align:center;color:#337ab7;">Proceed to Checkout</a><?php
			}else{?>
				<a href="#" data-toggle="modal" data-target="#authentication" class="col-md-12" style="font-size:18px;text-align:center;color:#337ab7;" onclick="$('#red-box').addClass('hide').removeClass('in');">Proceed to Checkout</a><?php
			}?>
		</div><?php
	}else{?>
		<div class="col-md-12" style="padding:10px;text-align:center;">
			<span style="font-size:14px;margin-bottom:20px;">Sorry, No product add to your cart.</span>
			<a href="<?php echo base_url();?>" class="btn-info" style="display:inline;float:none;color:#fff;background: #511E3E;border-radius: 0px;border: 0px;padding: 8px 15px;">Continue Shopping</a>
		</div><?php
	}?>
</div>
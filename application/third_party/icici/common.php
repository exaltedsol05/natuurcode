<?php
//Change below accesskey, secretkey to test 
$accesskey = 'e77d0dd0-f32b-11ea-af57-6bd6a40b1a92';
$secretkey = 'be288e83b4c926433d527beaf1372a50f983c25c';

//Changing environment to live requires remote_script also to be used for live and change accesskey,secretkey too
$environment = 'live';

//Sample data

//$remote_script = "https://sandbox-payments.open.money/layer";
//for production
$remote_script = "https://payments.open.money/layer";

//Hash functions requried in both request and response
function create_hash($data,$accesskey,$secretkey){
    ksort($data);
    $hash_string = $accesskey;
    foreach ($data as $key=>$value){
        $hash_string .= '|'.$value;
    }
    return hash_hmac("sha256",$hash_string,$secretkey);
}

function verify_hash($data,$rec_hash,$accesskey,$secretkey){
    $gen_hash = create_hash($data,$accesskey,$secretkey);
    if($gen_hash === $rec_hash){
        return true;
    }
    return false;
}
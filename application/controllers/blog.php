<?php
class Blog extends CI_Controller {

    
	public function __construct() {
        parent::__construct();
		$this->load->library('Banners');
		$this->load->library('Menus');
		$this->load->library('natuur');
		$this->load->model('Customer_model');
		$//this->load->model('cms_model');
		$this->load->model('Testimonial_model');
		$this->load->model('admin_model');		
		$this->load->model('Option_model');		
    }
	
	public function index(){

		error_reporting(E_ALL); 
        ini_set('display_errors', '1');

		$data['page'] ='home';
		$data['cat_blogs'] = $this->admin_model->get_all_cat_blogso();
		$data['popular_blogs'] = $this->admin_model->get_popular_blogs();
		$data['new_blogs'] = $this->admin_model->get_new_blogs();
        $this->load->view('vwBlog', $data);
	}
	
	function cms_faq(){
		$data['page_title'] = 'FAQs';
		$data['arrFaq'] = $this->cms_model->getFaq();
		$this->load->view('vwFaq',$data);
	}
	
	function cms_about(){
		$data['page_title'] = 'About Us';
		$data['page'] = 'about';
		$data['arrAbout'] = $this->cms_model->getAboutUs();
		$this->load->view('vwAboutus',$data);
	}
	
	function cms_contactus(){
		$data['page_title'] = 'Contact Us';
		$data['page'] = 'contact';
		$data['arrContact'] = $this->cms_model->getContactUs();
		$this->load->view('vwContactus',$data);
	}
	
	function blogs(){
		$data['page_title'] = 'Blog';
		$data['arrBlog'] = $this -> admin_model -> getBlogList();
		$this->load->view('vwBlog',$data);
	}
	
	function cms_blog_read($blog_id){
		$data['page_title'] = 'Blog';
		$data['blogDetails'] = $this->Testimonial_model->getBlogDetails($blog_id);
		$this->load->view('vwFullBlog',$data);
	}
	
	function cms_term_conditions(){
		$data['page_title'] = 'Term & Conditions';
		$data['arrTerm'] = $this->cms_model->getTerm();
		$this->load->view('vwTerm',$data);
	}
	function cms_privacy_policy()
	{
		$data['page_title'] = 'Privacy Policy';
		$data['arrPrivacy'] = $this->cms_model->getPrivacy();
		$this->load->view('vwPrivacy',$data);
	}
	function cms_cancel_policy()
	{
		$data['page_title'] = 'Cancellation Policy';
		$data['arrCancel'] = $this->cms_model->getCancel();
		$this->load->view('vwCancel',$data);
	}
	function cms_shipping_policy()
	{
		$data['page_title'] = 'Shipping Policy';
		$data['arrShipping'] = $this->cms_model->getShipping();
		$this->load->view('vwShipping',$data);
	}
	
	function contact_us_request()
	{
		extract($_POST);

		$data = array(
			'name' => $exampleInputName,
			'email' => $exampleInputEmail,
			'msg' => $exampleInputComments,
			);

		if($this->cms_model->saveContactUsDta($data))
		{
			$d['status'] = 'yes';
			$d['msg'] = 'Work in progress';
			echo json_encode($d);
			die;
		}
	}
	
	function blog_page($blog_id){
		$data['blog'] = $this -> admin_model -> blogDetails($blog_id);
		$this->load->view('blog', $data);
	}
	
	function category($cat){
		$blog_category = array("natuur_knowledge" => "1", "daily_wisdom" => "2", "diy_workshop" => "3", "chemicals_in_daily_life" => "4", "health_wellness" => "5", "beauty" => "6");		
		
		$category = array("1" => "Natuur Knowledge", "2"=>"Daily Wisdom", "3" => "DIY Workshop", "4"=> "Chemicals in Daily Life", "5" => "Health & Wellness", "6" => "Beauty");
		
		$data['title'] = $category[$blog_category[$cat]];
		$data['cat_blogs'] = $this->admin_model->get_all_cat_blogso($blog_category[$cat]);
		$this->load->view('category_blog', $data);
	}
}
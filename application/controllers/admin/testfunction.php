<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Testfunction extends CI_Controller {
    var $admin_id		= false;
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
		$this->load->helper('form');
        $this->load->library('form_validation');
		$this->load->model('Admin_model');
		
    }

    public function index()
	{
        $city = 1; //1=Gurgaon,2=Delhi NCR,3=Tier1,4=Tier2,5=Tier3
		$weight = 2501 ; //gm
		
		if($city == 1){
			if($weight <= 500){
				$charge = 35; //upto 500gm
			}else if($weight > 500 && $weight <= 1000){
				$charge = 35+25; //501gm-1000gm
			}else if($weight > 1000 && $weight <= 1500){
				$charge = 35+25+15; //1001gm-1500gm
			}else if($weight > 1500){
				$findWeight = $weight/500;
				//echo $findWeight;
				$charge = 35+25+15; //basic charge
				for($i=3; $i < $findWeight; $i++){
					$charge = $charge+10;
				}
			}
		}
		echo $charge; //total charges
		if($city == 2){
			$findWeight = $weight/500; //total weight divided by your weight
			echo $findWeight;
			if($findWeight > 1){
				$charge = 10; //basic charge
				for($i=1; $i < $findWeight; $i++){
					$charge = $charge+5;
				}
			}
			else
			{
				$charge = 10;
			}
			echo "$".$charge; //total charges
		}
    }   
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
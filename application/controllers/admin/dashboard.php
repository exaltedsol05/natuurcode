<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
		$this->load->model('admin_model');
        $this->load->library('form_validation');
         if (!$this->session->userdata('is_admin_login')) {
            redirect('admin/home');
        }
    }

    public function index() {
        $arr['page']='dash';
		$admin_email 	= $this->session->userdata['admin_email'];
		$admin_id 		= $this->session->userdata['id'];
		$this->db->select('*');
		$this->db->from('admin');
		$this->db->where(array('id' => $admin_id, 'email' => $admin_email));
		$result	= $this->db->get()->row();

        $arr['admin']					= $result;
		$arr['page'] 					= 'dash';
       	$arr['tody_order'] 	       		= $this->admin_model->today_order();
		$arr['total_order']	      	 	= $this->admin_model->total_order();
		$arr['total_customers']			= $this->admin_model->total_customers();
		//$arr['total_product_request']	= $this->admin_model->total_product_request();
		$arr['recent_order']			= $this->admin_model->recent_order();
		$arr['recent_product']			= $this->admin_model->recent_product();
		$arr['recent_customers']		= $this->admin_model->recent_customers();
		$arr['weekly_order']			= $this->admin_model->weekly_order();
		$arr['yearly_order'] 			= $this->admin_model->yearly_order();
		$arr['total_brand'] 			= 0;
		$arr['total_products'] 			= $this->admin_model->get_all_products();
		
		$arr['orderbystatus'] 			= $this->admin_model->get_order_by_status();
		//print_r($arr['weekly_order'] );exit;
        $this->load->view('admin/adminDashboard',$arr);
    }
	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
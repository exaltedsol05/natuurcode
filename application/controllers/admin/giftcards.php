<?php

class Giftcards extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->model('Settings_model');
		$this->load->model('Gift_card_model');
		$this->load->model('Order_model');
		$this->load->model('Search_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
	}
	
	function index(){
		
		$data['page_title'] = 'Gift Cards';
		$data['cards'] = $this->Gift_card_model->get_all_new();
		
		$gc_settings = $this->Settings_model->get_settings('gift_cards');
		if(isset($gc_settings['enabled']))
		{
			$data['gift_cards']['enabled'] = $gc_settings['enabled'];
		}
		else
		{
			$data['gift_cards']['enabled'] = false;
		}
		
		$this->load->view('admin/giftcards', $data);
		
	}
	
	function form()
	{
		
		$this->form_validation->set_rules('to_email', 'Recipient Email', 'trim|required');
		$this->form_validation->set_rules('to_name', 'Recipient Name', 'trim|required');
		$this->form_validation->set_rules('from', 'Sender Name', 'trim|required');
		$this->form_validation->set_rules('personal_message', 'Personal Message', 'trim');
		$this->form_validation->set_rules('beginning_amount', 'Amount', 'trim|required|numeric');
		
		$data['page_title'] = 'Add Gift Card';
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/giftcard_form', $data);
		}
		else
		{
			$save['code'] = generate_code(); // from the string helper
			$save['to_email'] = $this->input->post('to_email');
			$save['to_name'] = $this->input->post('to_name');
			$save['from'] = $this->input->post('from');
			$save['personal_message'] = $this->input->post('personal_message');
			$save['beginning_amount'] = $this->input->post('beginning_amount');
			$save['activated'] = 1;
			
			$this->Gift_card_model->save_card($save);
			
			if($this->input->post('send_notification'))
			{
				//get the canned message for gift cards
				$row = $this->db->where('id', '1')->get('canned_messages')->row_array();

				// set replacement values for subject & body
				$row['subject']	= str_replace('{from}', $save['from'], $row['subject']);
				$row['subject']	= str_replace('{site_name}', $this->config->item('company_name'), $row['subject']);
				
				$row['content']	= str_replace('{code}', $save['code'], $row['content']);
				$row['content']	= str_replace('{amount}', $save['beginning_amount'], $row['content']);
				$row['content']	= str_replace('{from}', $save['from'], $row['content']);
				$row['content']	= str_replace('{personal_message}', nl2br($save['personal_message']), $row['content']);
				$row['content']	= str_replace('{url}', $this->config->item('base_url'), $row['content']);
				$row['content']	= str_replace('{site_name}', $this->config->item('company_name'), $row['content']);

				$this->load->library('email');

				$config['mailtype'] = 'html';
				$this->email->initialize($config);

				$this->email->from($this->config->item('email'));
				$this->email->to($save['to_email']);

				$this->email->subject($row['subject']);
				$this->email->message($row['content']);

				$this->email->send();
			}
			
			$this->session->set_flashdata('message', 'The gift card was saved.');
			
			redirect('admin/giftcards');
		}
		
	}
	
	function activate($code)
	{
		$this->Gift_card_model->activate($code);
		$this->Gift_card_model->send_notification($code);
		$this->session->set_flashdata('message','The gift card was activated.');
		redirect('admin/giftcards');
	}
	
	function delete($id)
	{
		$this->Gift_card_model->delete($id);
		
		$this->session->set_flashdata('message', 'The gift card was deleted.');
		redirect('admin/giftcards');
	}
	
	// Gift card functionality 
	function enable()
	{
		
		$config['predefined_card_amounts'] = "10,20,25,50,100";
		$config['allow_custom_amount'] = "1";
		$config['enabled'] = '1';
		$this->Settings_model->save_settings('gift_cards', $config);
		redirect('admin/giftcards');
	}
	
	function disable() 
	{
		$config['enabled'] = '0';
		$this->Settings_model->save_settings('gift_cards', $config);
		redirect('admin/giftcards');
	}
	
	function settings()
	{		
		$gc_settings = $this->Settings_model->get_settings('gift_cards');
				
		$data['predefined_card_amounts']	= $gc_settings['predefined_card_amounts'];
		$data['allow_custom_amount'] 		= $gc_settings['allow_custom_amount'];
		
		$this->form_validation->set_rules('predefined_card_amounts', 'Predefined Card Amounts', 'trim');
		$this->form_validation->set_rules('allow_custom_amount', 'Allow Custom Amounts', 'trim');
		
		$data['page_title']	= 'Gift Card Settings';
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/giftcards_settings', $data);
		}
		else
		{
		
			$save['predefined_card_amounts']	= $this->input->post('predefined_card_amounts');
			$save['allow_custom_amount']		= $this->input->post('allow_custom_amount');
			
			$this->Settings_model->save_settings('gift_cards', $save);
			
			$this->session->set_flashdata('message', 'The gift card settings were saved.');
			
			redirect('admin/giftcards');
			
		}
	}
	
	function gift($sort_by='order_number',$sort_order='desc', $code=0, $page=0, $rows=50){
		//if they submitted an export form do the export
		$this->load->helper('form');
		$this->load->helper('date');
		$data['message']	= $this->session->flashdata('message');
		$data['page']		= 'Giftcards';	
		
 		$data['orders']	= $this->Gift_card_model->get_orders($this->input->post());
		$data['total']	= count($data['orders']);//$this->Order_model->get_orders_count($term);
		
		$this->load->library('pagination');
		$config['base_url']			= site_url('admin/orders/index/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
		$config['total_rows']		= $data['total'];
		$config['per_page']			= $rows;
		$config['uri_segment']		= 7;
		$config['first_link']		= 'First';
		$config['first_tag_open']	= '<li>';
		$config['first_tag_close']	= '</li>';
		$config['last_link']		= 'Last';
		$config['last_tag_open']	= '<li>';
		$config['last_tag_close']	= '</li>';

		$config['full_tag_open']	= '<nav><ul  class="pagination">';
		$config['full_tag_close']	= '</ul></nav>';
		$config['cur_tag_open']		= '<li class="active"><a href="#">';
		$config['cur_tag_close']	= '</a></li>';
		
		$config['num_tag_open']		= '<li>';
		$config['num_tag_close']	= '</li>';
		
		$config['prev_link']		= '&laquo;';
		$config['prev_tag_open']	= '<li>';
		$config['prev_tag_close']	= '</li>';

		$config['next_link']		= '&raquo;';
		$config['next_tag_open']	= '<li>';
		$config['next_tag_close']	= '</li>';
		
		$this->pagination->initialize($config);
	
		$data['sort_by']	= $sort_by;
		$data['sort_order']	= $sort_order;
				
		$this->load->view('admin/gift', $data);
	}
	
	function mailtouser(){	
		$this->load->library('email');
		$emails = $this -> input -> post('emails');
		$count_email = substr_count($emails,",");
		$email_array = array();
		if($count_email <= 1){
			$email_array[] = rtrim($emails, ",");
		}else{
			$email_array[] = explode(",", $emails);
		}
		
		$mail_subject =$this->input->post('mail_subject');
		$mail_content =$this->input->post('mail_content');
		$fromemail = "drpnnreddy@natuur.in";
		
		foreach($email_array as $tomail){
			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			$this->email->from('drpnnreddy@natuur.in');
			$this->email->to('shokeen.eweblabs@gmail.com');
			$this->email->subject("mail subject");
			$this->email->message("mail message");
			$this->email->attach("<?php echo HTTP_CSS_PATH; ?>jquery.bxslider.css");
			$this->email->attach("<?php echo HTTP_CSS_PATH; ?>star-rating.min.css");
			$this->email->attach("<?php echo HTTP_CSS_PATH; ?>font-awesome.min.css");
			
			if($this->email->send()){
				echo 'Email send to '.$tomail.' successfully.';
			}else{
				echo 'Oops, Email Not send to '.$tomail;
				show_error($this->email->print_debugger());
			}
		}
	}
}
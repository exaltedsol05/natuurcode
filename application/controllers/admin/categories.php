<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Categories extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
         if (!$this->session->userdata('is_admin_login')) {
            redirect('admin/home');
        }
		if ($this->session->userdata('roll')!='admin') {
            redirect('admin/home');
        }
		$this->load->model('Category_model');
    }

    public function index() {
        $arr['page'] = 'categories';
        $arr['categories'] = $this->Category_model->get_categories_tiered(true);
        $this->load->view('admin/adminManageCategories',$arr);
    }

    function form($id = false)
    {
        
        $config['upload_path']      = 'uploads/images/full';
        $config['allowed_types']    = 'gif|jpg|png';
        $config['max_size']         = $this->config->item('size_limit');
        $config['max_width']        = '10240';
        $config['max_height']       = '7680';
        $config['encrypt_name']     = true;
        $this->load->library('upload', $config);
        
        
        $this->category_id  = $id;
        $this->load->helper('form');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        
        $arr['categories']     = $this->Category_model->get_categories();
        $arr['page']     = 'categories';
        
        //default values are empty if the customer is new
        $arr['id']             = '';
        $arr['name']           = '';
        $arr['slug']           = '';
        $arr['description']    = '';
        $arr['excerpt']        = '';
        $arr['sequence']       = '';
        $arr['image']          = '';
		$arr['image1']          = '';
		
        $arr['seo_title']      = '';
        $arr['meta']           = '';
        $arr['parent_id']      = 0;
        $arr['enabled']        = '';
        $arr['error']          = '';
        
        //create the photos array for later use
        $arr['photos']     = array();
        
        if ($id)
        {   
            $category       = $this->Category_model->get_category($id);

            //if the category does not exist, redirect them to the category list with an error
            if (!$category)
            {
                $this->session->set_flashdata('error','The requested category could not be found');
                redirect('admin/categories');
            }
            
            //helps us with the slug generation
            $this->category_name    = $this->input->post('slug', $category->slug);
            
            //set values to db values
            $arr['id']             = $category->id;
            $arr['name']           = $category->name;
            $arr['slug']           = $category->slug;
            $arr['description']    = $category->description;
            $arr['excerpt']        = $category->excerpt;
            $arr['sequence']       = $category->sequence;
            $arr['parent_id']      = $category->parent_id;
            $arr['image']          = $category->image;
            $arr['seo_title']      = $category->seo_title;
			$arr['image1']         = $category->image1;
			
            $arr['meta']           = $category->meta;
            $arr['enabled']        = $category->enabled;
            
        }
        
        $this->form_validation->set_rules('name', 'name', 'trim|required|max_length[64]');
        $this->form_validation->set_rules('slug', 'slug', 'trim');
        $this->form_validation->set_rules('description', 'description', 'trim');
        $this->form_validation->set_rules('excerpt', 'excerpt', 'trim');
        $this->form_validation->set_rules('sequence', 'sequence', 'trim|integer');
        $this->form_validation->set_rules('parent_id', 'parent_id', 'trim');
        $this->form_validation->set_rules('image', 'Banner image', 'trim');
		$this->form_validation->set_rules('image1', 'Home image', 'trim');
		
        $this->form_validation->set_rules('seo_title', 'seo_title', 'trim');
        $this->form_validation->set_rules('meta', 'meta', 'trim');
        $this->form_validation->set_rules('enabled', 'enabled', 'trim|numeric');
        
        
        // validate the form
        if ($this->form_validation->run() == FALSE)
        {
		    
            $this->load->view('admin/category_form', $arr);
			
        }
        else
        {
            
            if($_FILES['image']["name"])
			{
            $uploaded   = $this->upload->do_upload('image');
            
            if ($id)
            {
                //delete the original file if another is uploaded
                if($uploaded)
                {
                    
                    if($arr['image'] != '')
                    {
                        $file = array();
                        $file[] = 'uploads/images/full/'.$arr['image'];
                        $file[] = 'uploads/images/medium/'.$arr['image'];
                        $file[] = 'uploads/images/small/'.$arr['image'];
                        $file[] = 'uploads/images/thumbnails/'.$arr['image'];
                        
                        foreach($file as $f)
                        {
                            //delete the existing file if needed
                            if(file_exists($f))
                            {
                                unlink($f);
                            }
                        }
                    }
                }
                
            }
            
            if(!$uploaded)
            {
                //$arr['error']  = $this->upload->display_errors();
				//echo $this->upload->display_errors();
				
                if($_FILES['image']['error'] != 4)
                {
                    $arr['error']  .= $this->upload->display_errors();
					
                    $this->load->view('admin/category_form', $arr);
                    return; //end script here if there is an error
                }
            }
            else
            {
                $image          = $this->upload->data();
                $save['image']  = $image['file_name'];
                
                $this->load->library('image_lib');
                
                //this is the larger image
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'uploads/images/full/'.$save['image'];
                $config['new_image']    = 'uploads/images/medium/'.$save['image'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 600;
                $config['height'] = 500;
                $this->image_lib->initialize($config);
                $this->image_lib->resize();
                $this->image_lib->clear();

                //small image
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'uploads/images/medium/'.$save['image'];
                $config['new_image']    = 'uploads/images/small/'.$save['image'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 300;
                $config['height'] = 300;
                $this->image_lib->initialize($config); 
                $this->image_lib->resize();
                $this->image_lib->clear();

                //cropped thumbnail
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'uploads/images/small/'.$save['image'];
                $config['new_image']    = 'uploads/images/thumbnails/'.$save['image'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 150;
                $config['height'] = 150;
                $this->image_lib->initialize($config);  
                $this->image_lib->resize(); 
                $this->image_lib->clear();
            }
			}
			
			
			
			
			if($_FILES['image1']["name"])
			{
			 $uploaded1   = $this->upload->do_upload('image1');
            
            if ($id)
            {
                //delete the original file if another is uploaded
                if($uploaded1)
                {
                    
                    if($arr['image1'] != '')
                    {
                        $file = array();
                        $file[] = 'uploads/images/full/'.$arr['image1'];
                        $file[] = 'uploads/images/medium/'.$arr['image1'];
                        $file[] = 'uploads/images/small/'.$arr['image1'];
                        $file[] = 'uploads/images/thumbnails/'.$arr['image1'];
                        
                        foreach($file as $f)
                        {
                            //delete the existing file if needed
                            if(file_exists($f))
                            {
                                unlink($f);
                            }
                        }
                    }
                }
                
            }
            
            if(!$uploaded)
            {
                //$arr['error']  = $this->upload->display_errors();
				//echo $this->upload->display_errors();
				
                if($_FILES['image1']['error'] != 4)
                {
                    $arr['error']  .= $this->upload->display_errors();
					
                    $this->load->view('admin/category_form', $arr);
                    return; //end script here if there is an error
                }
            }
            else
            {
                $image          = $this->upload->data();
                $save['image1']  = $image['file_name'];
                
                $this->load->library('image_lib');
                
                //this is the larger image
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'uploads/images/full/'.$save['image1'];
                $config['new_image']    = 'uploads/images/medium/'.$save['image1'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 600;
                $config['height'] = 500;
                $this->image_lib->initialize($config);
                $this->image_lib->resize();
                $this->image_lib->clear();

                //small image
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'uploads/images/medium/'.$save['image1'];
                $config['new_image']    = 'uploads/images/small/'.$save['image1'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 300;
                $config['height'] = 300;
                $this->image_lib->initialize($config); 
                $this->image_lib->resize();
                $this->image_lib->clear();

                //cropped thumbnail
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'uploads/images/small/'.$save['image1'];
                $config['new_image']    = 'uploads/images/thumbnails/'.$save['image1'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 150;
                $config['height'] = 150;
                $this->image_lib->initialize($config);  
                $this->image_lib->resize(); 
                $this->image_lib->clear();
            }
            
			}
			
			
            
            $this->load->helper('text');
            
            //first check the slug field
            $slug = $this->input->post('slug');
            
            //if it's empty assign the name field
            if(empty($slug) || $slug=='')
            {
                $slug = $this->input->post('name');
            }
            
            $slug   = url_title(convert_accented_characters($slug), 'dash', TRUE);
            
            //validate the slug
            $this->load->model('Routes_model');
            if($id)
            {
                $slug   = $this->Routes_model->validate_slug($slug, $category->route_id);
                $route_id   = $category->route_id;
            }
            else
            {
                $slug   = $this->Routes_model->validate_slug($slug);
                
                $route['slug']  = $slug;    
                $route_id   = $this->Routes_model->save($route);
            }
            
            $save['id']             = $id;
            $save['name']           = $this->input->post('name');
            $save['description']    = $this->input->post('description');
            $save['excerpt']        = $this->input->post('excerpt');
            $save['parent_id']      = intval($this->input->post('parent_id'));
            $save['sequence']       = intval($this->input->post('sequence'));
            $save['seo_title']      = $this->input->post('seo_title');
            $save['meta']           = $this->input->post('meta');
            $save['enabled']        = $this->input->post('enabled');
            $save['route_id']       = intval($route_id);
            $save['slug']           = $slug;
            
            $category_id    = $this->Category_model->save($save);
            
            //save the route
            $route['id']    = $route_id;
            $route['slug']  = $slug;
            $route['route'] = 'cart/category/'.$category_id.'';
            
            $this->Routes_model->save($route);
            
            $this->session->set_flashdata('message', 'The category has been saved!');
            
            //go back to the category list
            redirect('admin/categories');
        }
    }

     function delete($id)
    {
        
        $category   = $this->Category_model->get_category($id);
        //if the category does not exist, redirect them to the customer list with an error
        if ($category)
        {
            $this->load->model('Routes_model');
            
            $this->Routes_model->delete($category->route_id);
            $this->Category_model->delete($id);
            
            $this->session->set_flashdata('message', 'The category has been deleted.');
            redirect('admin/categories');
        }
        else
        {
            $this->session->set_flashdata('error','The requested category could not be found.');
        }
    }
    function organize($id = false)
    {
      
        $data['page']	= 'Category organize';
        if (!$id)
        {
            $this->session->set_flashdata('error', 'You must select a category to organize');
            redirect('admin/categories');
        }
        
        $data['category']       = $this->Category_model->get_category($id);
        //if the category does not exist, redirect them to the category list with an error
        if (!$data['category'])
        {
            $this->session->set_flashdata('error', 'The requested category could not be found.');
            redirect('admin/categories');
        }
            
        $data['page_title']     = sprintf('Organize "%s" Category', $data['category']->name);
        
        $data['category_products']  = $this->Category_model->get_category_products_admin($id);
        
        $this->load->view('admin/organize_category', $data);
    }
    
    
	
	function process_organization($id)
    {
        $products   = $this->input->post('product');
        $this->Category_model->organize_contents($id, $products);
    }
    
    
    

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
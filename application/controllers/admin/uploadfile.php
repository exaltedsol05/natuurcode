<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Uploadfile extends CI_Controller{

public function index(){
//$this->load->view('include/header');
$file = file_get_contents ($_FILES['upload']['tmp_name']);

$temp_folder = getcwd().'/temp/';

if(!is_dir($temp_folder)){

mkdir($temp_folder);
}

if((!empty($_FILES["upload"])) && ($_FILES['upload']['error'] == 0)) {

$file_category = $_POST['file_category'];

$filename = basename($_FILES['upload']['name']);

$this->$file_category($_FILES['upload']['tmp_name']);

$ext = substr($filename, strpos($filename,'.')+1);

if($ext=='csv'){

$csvfile_path = $temp_folder.$filename;

if(!file_exists($csvfile_path)){
if(move_uploaded_file($_FILES['upload']['tmp_name'],$csvfile_path)){

$this->$file_category($csvfile_path);

}
else {
echo "Error: A problem occurred during file upload!";
}
}
else {
echo "Error: File ".$_FILES["uploaded_file"]["name"]." already exists";
}

}
else{
echo "Please upload file in CSV format";
}

}
$this->load->view('uploadfile');
//$this->load->view('include/footer');
}
function readCSV($csvFile){
	$file_handle = fopen($csvFile, 'r');
	while (!feof($file_handle) ) {
		$line_of_text[] = fgetcsv($file_handle, 51200);
	}
	fclose($file_handle);
	return $line_of_text;
}
public function addcsv_infibeam()
{
	//$this->Product_model->deletethirdparty();
	$THE_PATTERN=$_SERVER["DOCUMENT_ROOT"]."/natuur/uploads/infibeam/*.csv";

	$TheFilesList = @glob($THE_PATTERN);
	$TheFilesTotal = @count($TheFilesList);
	$TheFilesTotal = $TheFilesTotal - 1;
	$TheFileTemp = "";
	$this->load->model('Product_model');
	$this->Product_model->deleteinfibeam();
	
	for ($TheFilex=0; $TheFilex<=$TheFilesTotal; $TheFilex++)
	{
	    $csvFile = $TheFilesList[$TheFilex];
		
		$data = $this->readCSV($csvFile);
		
		$team_array = array();
		$i = 0;

		
		foreach ($data as $key => $val) {
			if($key > 0){

				$SiteName 		= $val[0];
				$ListingId 		= $val[1];
				$Make 			= $val[2];
				$Model 			= $val[3];
				$Variant 		= $val[4];
				$Category 		= $val[5];
				$SubCategory 	= $val[6];
				$Title 			= $val[7];
				$ProductCategory = $val[8];
				$BasePrice 		 = $val[9];
				$OurPrice 		 = $val[10];
				$Discount 		 = $val[11];
				$ShippingCharges = $val[12];
				$Status 		 = $val[13];
				$URL 		     = $val[14];
				$ImageURL 		 = $val[15];
				$Attribute 		 = $val[16];
				$Offer 		     = $val[19];
				if($SiteName)
				{
					$team = array(
						'SiteName' 		=> $SiteName,
						'ListingId' 	=> $ListingId,
						'Make' 			=> $Make,
						'Model' 		=> $Model,
						'Variant' 		=> $Variant,
						'Category' 		=> $Category,
						'SubCategory' 	=> $SubCategory,
						'Title' 		=> $Title,
						'ProductCategory' => $ProductCategory,
						'BasePrice' 	=> $BasePrice,
						'OurPrice' 		=> $OurPrice,
						'Discount' 		=> $Discount,
						'ShippingCharges'=> $ShippingCharges ,
						'Status'		=> $Status,
						'URL'			=> $URL, 	
						'ImageURL'		=> $ImageURL, 	
						'Attribute'		=> $Attribute, 	
						'Offer'			=> $Offer 
					);

					array_push($team_array,$team);
				}
			}
		}
		
		if(! empty($team_array) ){
				$this->Product_model->saveinfibeam($team_array);
				echo "Inserted Successfully.";
				//unlink($filepath);
		}
		else{
			echo "No new data found to insert.";
		}
	}
}

public function addcsv_filipkart()
{
	//$this->Product_model->deletethirdparty();
	$THE_PATTERN=$_SERVER["DOCUMENT_ROOT"]."/natuur/uploads/filipkart/*.csv";

	$TheFilesList = @glob($THE_PATTERN);
	$TheFilesTotal = @count($TheFilesList);
	$TheFilesTotal = $TheFilesTotal - 1;
	$TheFileTemp = "";
	$this->load->model('Product_model');
	$this->Product_model->deletefilipkart();
	
	for ($TheFilex=0; $TheFilex<=$TheFilesTotal; $TheFilex++)
	{
	    $csvFile = $TheFilesList[$TheFilex];
		
		$data = $this->readCSV($csvFile);
		
		$team_array = array();
		$i = 0;

		
		foreach ($data as $key => $val) {
			if($key > 0){

					$productId 	    = $val[0];
					$title 		    = $val[1];
					$description 	= $val[2];
					$imageUrlStr 	= $val[3];
					$mrp 			= $val[4];
					$price 			= $val[5];
					$productUrl 	= $val[6];
					$categories 	= $val[7];					
					$productBrand 	= $val[8];
					$deliveryTime 	= $val[9];
					$inStock 		= $val[10];
					$codAvailable 	= $val[11];
					$emiAvailable 	= $val[12];
					$offers 		= $val[13];
					$discount 		= $val[14];
					$cashBack 		= $val[15];
					$size 		    = $val[16];
					$color			= $val[17];
					$sizeUnit		= $val[18];
					$sizeVariants   = $val[19];
					$colorVariants  = $val[20];
					$styleCode      = $val[21];
				
				if($productId)
				{
					$team = array(
						'productId' 	=> $productId,
						'title' 		=> $title,
						'description' 	=> $description,
						'imageUrlStr' 	=> $imageUrlStr,
						'mrp' 			=> $mrp,
						'price' 		=> $price,
						'productUrl' 	=> $productUrl,
						'categories' 	=> $categories,
						'productBrand'  => $productBrand,
						'deliveryTime' 	=> $deliveryTime,
						'inStock' 		=> $inStock,
						'codAvailable' 	=> $codAvailable,
						'emiAvailable'  => $emiAvailable ,
						'offers'		=> $offers,
						'discount'		=> $discount, 	
						'size'			=> $size, 	
						'color'			=> $color, 	
						'sizeUnit'		=> $sizeUnit,
						'sizeVariants'	=> $sizeVariants,
						'colorVariants'	=> $colorVariants,
						'styleCode'		=> $styleCode
						
						
						
					);

					array_push($team_array,$team);
				}
			}
		}
		
		if(! empty($team_array) ){
				$this->Product_model->savefilipkart($team_array);
				echo "Inserted Successfully.";
				//unlink($filepath);
		}
		else{
			echo "No new data found to insert.";
		}
	}
}

}
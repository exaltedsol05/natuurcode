<?php

class Reports extends CI_Controller {

	//this is used when editing or adding a customer
	var $customer_id	= false;	

	function __construct()
	{		
		parent::__construct();

		//$this->auth->check_access('Admin', true);
		
		$this->load->model('Order_model');
		$this->load->model('Search_model');
		$this->load->helper(array('formatting'));
		
		//$this->lang->load('report');
	}
	
	function index()
	{
		$data['page']	= 'Reports';
		$data['years']		= $this->Order_model->get_sales_years();
		$this->load->view('admin/reports', $data);
	}
	
	function best_sellers()
	{
		$start	= $this->input->post('start');
		$end	= $this->input->post('end');
		$data['best_sellers']	= $this->Order_model->get_best_sellers($start, $end);
		
		$this->load->view('admin/reports/best_sellers', $data);	
	}
	
	function sales()
	{
		$year			= $this->input->post('year');
		$data['orders']	= $this->Order_model->get_gross_monthly_sales($year);
		$this->load->view('admin/reports/sales', $data);	
	}
	function most_bought_cust()
	{
		$data['most_bought_list']		= $this->Order_model->get_most_bought_customer_list();
		//echo "<pre>";print_r($data['most_bought_list']);exit;
		$this->load->view('admin/reports/most_bought', $data);
	}

}
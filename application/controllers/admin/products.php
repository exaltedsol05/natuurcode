<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Products extends CI_Controller {	
	
	private $use_inventory = false;
	
	function __construct()
	{		
        parent::__construct();
        $this->load->library('form_validation');
        if (!$this->session->userdata('is_admin_login')) {
            redirect('admin/home');
        }
		if ($this->session->userdata('roll')!='admin') {
            redirect('admin/home');
        }
		
		$this->load->model('Product_model');
		$this->load->model('Category_model');
		$this->load->helper('form');
		 $this->load->library('excel');

	}

	function index($order_by="name", $sort_order="ASC", $code=0, $page=0, $rows=20){
		
		$arr['page']	= 'products';
		
		$arr['code']		= $code;
		$term				= false;
		$category_id		= false;

		//get the category list for the drop menu
		$arr['categories']	= $this->Category_model->get_categories_tiered();
		
		$post				= $this->input->post(null, false);
		$this->load->model('Search_model');
		
		if($post){
			$term			= json_encode($post);
			$code			= $this->Search_model->record_term($term);
			$arr['code']	= $code;
		}elseif ($code){
			$term			= $this->Search_model->get_term($code);
		}
		
		//store the search term
		$arr['term']		= $term;
		$arr['order_by']	= $order_by;
		$arr['sort_order']	= $sort_order;
		
		$arr['products']	= $this->Product_model->products(array('term'=>$term, 'order_by'=>$order_by, 'sort_order'=>$sort_order, 'rows'=>$rows, 'page'=>$page));

		//total number of products
		$arr['total']		= $this->Product_model->products(array('term'=>$term, 'order_by'=>$order_by, 'sort_order'=>$sort_order), true);

		
		$this->load->library('pagination');
		
		$config['base_url']			= site_url('admin/products/index/'.$order_by.'/'.$sort_order.'/'.$code.'/');
		$config['total_rows']		= $arr['total'];
		$config['per_page']			= $rows;
		$config['uri_segment']		= 7;
		$config['first_link']		= 'First';
		$config['first_tag_open']	= '<li>';
		$config['first_tag_close']	= '</li>';
		$config['last_link']		= 'Last';
		$config['last_tag_open']	= '<li>';
		$config['last_tag_close']	= '</li>';

		$config['full_tag_open']	= '<ul class="pagination">';
		$config['full_tag_close']	= '</ul>';
		$config['cur_tag_open']		= '<li class="active"><a href="#">';
		$config['cur_tag_close']	= '</a></li>';
		
		$config['num_tag_open']		= '<li>';
		$config['num_tag_close']	= '</li>';
		
		$config['prev_link']		= '&laquo;';
		$config['prev_tag_open']	= '<li>';
		$config['prev_tag_close']	= '</li>';

		$config['next_link']		= '&raquo;';
		$config['next_tag_open']	= '<li>';
		$config['next_tag_close']	= '</li>';
		
		$this->pagination->initialize($config);
		//echo $this->config->item('admin');
		
		$this->load->view('admin/products', $arr);
	}
	
	
	function product_archive($order_by="name", $sort_order="ASC", $code=0, $page=0, $rows=20){
		
		$arr['page']	    = 'archive_products';
		$arr['page_title']	= 'Archive Products';
		$arr['code']		= $code;
		$term				= false;
		$category_id		= false;

		//get the category list for the drop menu
		$arr['categories']	= $this->Category_model->get_categories_tiered();
		
		$post				= $this->input->post(null, false);
		$this->load->model('Search_model');
		
		if($post){
			$term			= json_encode($post);
			$code			= $this->Search_model->record_term($term);
			$arr['code']	= $code;
		}elseif ($code){
			$term			= $this->Search_model->get_term($code);
		}
		
		//store the search term
		$arr['term']		= $term;
		$arr['order_by']	= $order_by;
		$arr['sort_order']	= $sort_order;
		
		$arr['products']	= $this->Product_model->products_archive(array('term'=>$term, 'order_by'=>$order_by, 'sort_order'=>$sort_order, 'rows'=>$rows, 'page'=>$page));

		//total number of products
		$arr['total']		= $this->Product_model->products_archive(array('term'=>$term, 'order_by'=>$order_by, 'sort_order'=>$sort_order), true);

		
		$this->load->library('pagination');
		
		$config['base_url']			= site_url('admin/products/products_archive/'.$order_by.'/'.$sort_order.'/'.$code.'/');
		$config['total_rows']		= $arr['total'];
		$config['per_page']			= $rows;
		$config['uri_segment']		= 7;
		$config['first_link']		= 'First';
		$config['first_tag_open']	= '<li>';
		$config['first_tag_close']	= '</li>';
		$config['last_link']		= 'Last';
		$config['last_tag_open']	= '<li>';
		$config['last_tag_close']	= '</li>';

		$config['full_tag_open']	= '<ul class="pagination">';
		$config['full_tag_close']	= '</ul>';
		$config['cur_tag_open']		= '<li class="active"><a href="#">';
		$config['cur_tag_close']	= '</a></li>';
		
		$config['num_tag_open']		= '<li>';
		$config['num_tag_close']	= '</li>';
		
		$config['prev_link']		= '&laquo;';
		$config['prev_tag_open']	= '<li>';
		$config['prev_tag_close']	= '</li>';

		$config['next_link']		= '&raquo;';
		$config['next_tag_open']	= '<li>';
		$config['next_tag_close']	= '</li>';
		
		$this->pagination->initialize($config);
		//echo $this->config->item('admin');
		
		$this->load->view('admin/product_archive', $arr);
	}
	
	
	
	//basic category search
	function product_autocomplete()
	{
		$name	= trim($this->input->post('name'));
		$limit	= $this->input->post('limit');
		
		if(empty($name))
		{
			echo json_encode(array());
		}
		else
		{
			$results	= $this->Product_model->product_autocomplete($name, $limit);
			
			$return		= array();
			
			foreach($results as $r)
			{
				$return[$r->id]	= $r->name;
			}
			echo json_encode($return);
		}
		
	}
	//basic category search
	function product_autocomplete_combo()
	{
		$name	= trim($this->input->post('name'));
		$limit	= $this->input->post('limit');
		
		if(empty($name))
		{
			echo json_encode(array());
		}
		else
		{
			$results	= $this->Product_model->product_autocomplete_combo($name, $limit);
			
			$return		= array();
			
			foreach($results as $r)
			{
				$return[$r->id]	= $r->name;
			}
			echo json_encode($return);
		}
		
	}
	function bulk_save()
	{
		$products	= $this->input->post('product');
		
		if(!$products)
		{
			$this->session->set_flashdata('error', 'There are no products selected to bulk update');
			redirect('admin/products');
		}
				
		foreach($products as $id=>$product)
		{
			$product['id']	= $id;
			$this->Product_model->save($product);
		}
		
		$this->session->set_flashdata('message', 'Your products have been updated.');
		redirect('admin/products');
	}
	
	function form($id = false, $duplicate = false){
		
		
		
		$this->product_id = $id;
		
		$this->load->model(array('Option_model'));
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		
		$arr['categories']		= $this->Category_model->get_categories_tiered();
		
		$arr['page']		= 'products';
		//default values are empty if the product is new
		
		
		$arr['id']					    = '';
		$arr['sku']				        = '';
		$arr['name']				    = '';
		$arr['slug']				    = '';
		$arr['description']		        = '';
		$arr['description2']		    = '';
		$arr['description3']		    = '';
		$arr['description4']		    = '';
		$arr['description5']		    = '';
		$arr['excerpt']			        = '';
		$arr['price']				    = '';
		$arr['saleprice']			    = '';
		$arr['weight']				    = '';
		$arr['track_stock'] 		    = '';
		$arr['seo_title']			    = '';
		$arr['meta']				    = '';
		$arr['shippable']				= '';
		$arr['taxable']			    	= '';
		$arr['fixed_quantity']			= '';
		$arr['quantity']				= '';
		$arr['is_basket']				= '';
		$arr['enabled']			    	= '';
		$arr['home_enabled']			= '';
		$arr['related_products']		= array();
		$arr['product_categories']		= array();
		$arr['images']					= array();
		
		$arr['hotdeals_from']			= '';
		$arr['hotdeals_to']				= '';
	    //create the photos array for later use
		

		if ($id){	
			
			
			// get product & options data
			$arr['product_options']	    = $this->Option_model->get_product_options($id);
			$product					= $this->Product_model->get_product($id);
			
			//if the product does not exist, redirect them to the product list with an error
			if (!$product)
			{
				$this->session->set_flashdata('error','The requested product could not be found.');
				redirect($this->config->item('admin').'/products');
			}
			
			//helps us with the slug generation
			$this->product_name	= $this->input->post('slug', $product->slug);
			
			//set values to db values
			
		
			$arr['id']						= $id;
			$arr['sku']				    	= $product->sku;
			$arr['name']					= $product->name;
			$arr['seo_title']				= $product->seo_title;
			$arr['meta']					= $product->meta;
			$arr['slug']					= $product->slug;
			$arr['description']				= $product->description;
			$arr['description2']			= $product->description2;
			$arr['description3']			= $product->description3;
			$arr['description4']			= $product->description4;
			$arr['description5']			= $product->description5;
			$arr['excerpt']					= $product->excerpt;
			$arr['price']					= $product->price;
			$arr['saleprice']				= $product->saleprice;
			$arr['weight']					= $product->weight;
			$arr['track_stock'] 			= $product->track_stock;
			$arr['shippable']				= $product->shippable;
			$arr['quantity']				= $product->quantity;
			$arr['is_basket']				= $product->is_basket;
			$arr['taxable']					= $product->taxable;
			$arr['fixed_quantity']			= $product->fixed_quantity;
			$arr['enabled']					= $product->enabled;
			$arr['home_enabled']			= $product->home_enabled;
			$arr['hotdeals_from']			= $product->hotdeals_from;
			$arr['hotdeals_to']				= $product->hotdeals_to;
			
			//make sure we haven't submitted the form yet before we pull in the images/related products from the database
			if(!$this->input->post('submit')){
				
				$arr['product_categories']	= array();
				foreach($product->categories as $product_category)
				{
					$arr['product_categories'][] = $product_category->id;
				}
				
				$arr['related_products']	= $product->related_products;
				$arr['images']				= (array)json_decode($product->images);
			}
		}
		$exists_image = (array)json_decode($product->images, true);
		//json_decode( json_encode($array), true);
		//echo '<pre>';print_r($exists_image);echo '</pre>';
		//if $arr['related_products'] is not an array, make it one.
		if(!is_array($arr['related_products']))
		{
			$arr['related_products']	= array();
		}
		

		
		//no error checking on these
		
		
		
		
		
		
		
		$this->form_validation->set_rules('caption', 'Caption');
		$this->form_validation->set_rules('primary_photo', 'Primary');
		if($id){
			   $this->form_validation->set_rules('sku', 'SKU', 'trim|required');
		}else{
			   $this->form_validation->set_rules('sku', 'SKU', 'trim|is_unique[products.sku]|required');
		}
		
		$this->form_validation->set_rules('seo_title', 'Title Tag', 'trim');
		$this->form_validation->set_rules('meta', 'Meta Data', 'trim');
		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('slug', 'URL Keyword', 'trim');
		$this->form_validation->set_rules('description', 'Description', 'trim');
		$this->form_validation->set_rules('description2',  'How To Use', 'trim');
		$this->form_validation->set_rules('description3', 'Ask Natuur', 'trim');
		$this->form_validation->set_rules('description4', 'Blog', 'trim');
		$this->form_validation->set_rules('description5', 'Video', 'trim');
		$this->form_validation->set_rules('excerpt', 'Excerpt', 'trim');
		$this->form_validation->set_rules('price', 'Price', 'trim|numeric|floatval');
		$this->form_validation->set_rules('saleprice', 'Sale Price', 'trim|numeric|floatval');
		$this->form_validation->set_rules('weight', 'Weight', 'trim');
		$this->form_validation->set_rules('track_stock', 'Track Stock', 'trim|numeric');
		$this->form_validation->set_rules('quantity', 'Available Quantity', 'trim|numeric');
		$this->form_validation->set_rules('shippable', 'Shippable', 'trim|numeric');
		$this->form_validation->set_rules('taxable', 'Taxable', 'trim|numeric');
		$this->form_validation->set_rules('fixed_quantity', 'Fixed Quantity', 'trim|numeric');
		$this->form_validation->set_rules('enabled', 'Enabled', 'trim|numeric');
		/*
		if we've posted already, get the photo stuff and organize it
		if validation comes back negative, we feed this info back into the system
		if it comes back good, then we send it with the save item
		
		submit button has a value, so we can see when it's posted
		*/
		
		if($duplicate)
		{
			$arr['id']	= false;
		}
		if($this->input->post('submit'))
		{
			//reset the product options that were submitted in the post
			$arr['product_options']	    = $this->input->post('option');
			$arr['related_products']	= $this->input->post('related_products');
			$arr['product_categories']	= $this->input->post('categories');
			$arr['images']				= $this->input->post('images');
		}
		
		
		
		
		if ($this->form_validation->run() == FALSE){
			$this->load->view('admin/product_form', $arr);
		}else{
			
			
			$this->load->helper('text');
			
			//first check the slug field
			$slug = $this->input->post('slug');
			
			//if it's empty assign the name field
			if(empty($slug) || $slug=='')
			{
				$slug = $this->input->post('name');
			}
			
			$slug	= url_title(convert_accented_characters($slug), 'dash', TRUE);
			
			//validate the slug
			$this->load->model('Routes_model');

			if($id)
			{
				$slug		= $this->Routes_model->validate_slug($slug, $product->route_id);
				$route_id	= $product->route_id;
			}
			else
			{
				$slug	= $this->Routes_model->validate_slug($slug);
				
				$route['slug']	= $slug;	
				$route_id	= $this->Routes_model->save($route);
			}
			
			
           
		
			$save['id']					= $id;
			$save['sku']				= $this->input->post('sku');
			$save['name']				= $this->input->post('name');
			$save['seo_title']			= $this->input->post('seo_title');
			$save['meta']				= $this->input->post('meta');
			$save['description']		= $this->input->post('description');
			$save['description2']		= $this->input->post('description2');
			$save['description3']		= $this->input->post('description3');
			$save['description4']		= $this->input->post('description4');
			$save['description5']		= $this->input->post('description5');
			
			$save['excerpt']			= $this->input->post('excerpt');
			$save['price']				= $this->input->post('price');
			$save['saleprice']			= $this->input->post('saleprice');
			$save['weight']				= $this->input->post('weight');
			$save['track_stock']		= $this->input->post('track_stock');
			$save['fixed_quantity']		= $this->input->post('fixed_quantity');
			$save['quantity']			= $this->input->post('quantity');
			$save['is_basket']			= $this->input->post('is_basket');
			$save['shippable']			= $this->input->post('shippable');
			$save['taxable']			= $this->input->post('taxable');
			$save['enabled']			= $this->input->post('enabled');
			$save['home_enabled']		= $this->input->post('home_enabled');
			$post_images				= $this->input->post('images');
			
			$save['hotdeals_from']		= $this->input->post('hotdeals_from');
			$save['hotdeals_to']		= $this->input->post('hotdeals_to');
			
			$save['slug']				= $slug;
			$save['route_id']			= $route_id;
			
			if($primary	= $this->input->post('primary_image'))
			{
				if($post_images)
				{
					$post_images_arr = array();
					foreach($post_images as $key => &$pi)
					{
						$post_images_arr[] = $pi['filename'];
						if($primary == $key)
						{
							$pi['primary']	= true;
							continue;
						}
					}	
				}
				
			}
			
			$save['images']	= json_encode($post_images);
			
			//echo '<pre>';print_r($exists_image);echo '</pre>';
			//echo '<pre>';print_r($post_images_arr);echo '</pre>';
			foreach($exists_image as $exists_image_val){
				//echo $exists_image_val['filename'].'1<br>';
				if(!in_array($exists_image_val['filename'], $post_images_arr)) {
					$file_chk = array();
					$file_chk[] = 'uploads/images/full/'.$exists_image_val['filename'];
					$file_chk[] = 'uploads/images/medium/'.$exists_image_val['filename'];
					$file_chk[] = 'uploads/images/small/'.$exists_image_val['filename'];
					$file_chk[] = 'uploads/images/thumbnails/'.$exists_image_val['filename'];
					//echo $exists_image_val->filename.'<br>';
					foreach($file_chk as $f)
					{
						//delete the existing file if needed
						if(file_exists($f))
						{
							unlink($f);
							
						}
					}
				}
			}
			//echo '<pre>';print_r($post_images_arr);echo '</pre>';
			
			if($this->input->post('related_products'))
			{
				
				$save['related_products'] = json_encode($this->input->post('related_products'));
			}
			else
			{
				$save['related_products'] = '';
			}
			
			//save categories
			$categories			= $this->input->post('categories');
			if(!$categories)
			{
				$categories	= array();
			}
			
			
			// format options
			$options	= array();
			if($this->input->post('option'))
			{
				foreach ($this->input->post('option') as $option)
				{
					$options[]	= $option;
				}

			}	
			
			// save product 
			$product_id	= $this->Product_model->save($save, $options, $categories);
			
			//save the route
			$route['id']	= $route_id;
			$route['slug']	= $slug;
			$route['route']	= 'cart/product/'.$product_id;
			$this->Routes_model->save($route);			
			$this->session->set_flashdata('message','The product has been saved.');

			//go back to the product list
			redirect('admin/products');
		}
	}
	
	function product_image_form($id=null)
	{
		$arr['file_name'] = false;
		$arr['error']	= false;
		$arr['id']	= $id;
		$this->load->view('admin/product_image_uploader', $arr);
	}
	
	function product_image_upload($id=null)
	{
		$product					= $this->Product_model->get_product($id);
		$exists_image = (array)json_decode($product->images, true);
		
		$arr['file_name'] = false;
		$arr['error']	= false;
		
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['max_size']	= $this->config->item('size_limit');
		$config['upload_path'] = 'uploads/images/full';
		$config['encrypt_name'] = true;
		$config['remove_spaces'] = true;

		$this->load->library('upload', $config);
		
		if ( $this->upload->do_upload())
		{
			$upload_data	= $this->upload->data();
			
			$this->load->library('image_lib');
			/*
			
			I find that ImageMagick is more efficient that GD2 but not everyone has it
			if your server has ImageMagick then you can change out the line
			
			$config['image_library'] = 'gd2';
			
			with
			
			$config['library_path']		= '/usr/bin/convert'; //make sure you use the correct path to ImageMagic
			$config['image_library']	= 'ImageMagick';
			*/			
			
			//this is the larger image
			$config['image_library'] = 'gd2';
			$config['source_image'] = 'uploads/images/full/'.$upload_data['file_name'];
			$config['new_image']	= 'uploads/images/medium/'.$upload_data['file_name'];
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 400;
			$config['height'] = 300;
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
			$this->image_lib->clear();

			//small image
			$config['image_library'] = 'gd2';
			$config['source_image'] = 'uploads/images/full/'.$upload_data['file_name'];
			$config['new_image']	= 'uploads/images/small/'.$upload_data['file_name'];
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 235;
			$config['height'] = 235;
			$this->image_lib->initialize($config); 
			$this->image_lib->resize();
			$this->image_lib->clear();

			//cropped thumbnail
			$config['image_library'] = 'gd2';
			$config['source_image'] = 'uploads/images/full/'.$upload_data['file_name'];
			$config['new_image']	= 'uploads/images/thumbnails/'.$upload_data['file_name'];
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 150;
			$config['height'] = 150;
			$this->image_lib->initialize($config); 	
			$this->image_lib->resize();	
			$this->image_lib->clear();

			$arr['file_name']	= $upload_data['file_name'];
			
			if($id!=''){
				$create_images = array();
				$create_images[$upload_data['raw_name']] = array(
					"filename" => $upload_data['file_name'], 
					"alt" => "", 
					"caption" => ""
				);
				$final_images = array_merge($exists_image,$create_images);
				$save['id']					= $id;
				$save['images']				= json_encode($final_images);
				$product_id	= $this->Product_model->save($save, '', '');
			}
		}
		
		if($this->upload->display_errors() != '')
		{
			$arr['error'] = $this->upload->display_errors();
		}
		$arr['id']	= $id;
		$this->load->view('admin/product_image_uploader', $arr);
	}
	
	
	
	function delete_archive($id = false)
	{
	   
		if ($id)
		{	
			$product	= $this->Product_model->get_product($id);
			//$product='';
			//if the product does not exist, redirect them to the customer list with an error
			if (!$product)
			{
				$this->session->set_flashdata('error', 'The requested product could not be found.');
				redirect('admin/products');
				
			}
			else
			{

				//if the product is legit, delete them
				$this->Product_model->delete_product_archive($id);

				$this->session->set_flashdata('message','The product has been deleted.');
				redirect('admin/products');
			}
		}
		else
		{
			//if they do not provide an id send them to the product list page with an error
			$this->session->set_flashdata('error', 'The requested product could not be found.');
			redirect('admin/products');
		}
	}
	
	function delete($id = false)
	{
	   
		if ($id)
		{	
			$product	= $this->Product_model->get_product($id);
			
			//if the product does not exist, redirect them to the customer list with an error
			if (!$product)
			{
				$this->session->set_flashdata('error', 'The requested product could not be found.');
				redirect('admin/products/products');
				
			}
			else
			{
              
				// remove the slug
				$this->load->model('Routes_model');
				$this->Routes_model->delete($product->route_id);

				//if the product is legit, delete them
				$this->Product_model->delete_product($id);

				$this->session->set_flashdata('message','The product has been deleted.');
				redirect('admin/products/products');
			}
		}
		else
		{
			//if they do not provide an id send them to the product list page with an error
			$this->session->set_flashdata('error', 'The requested product could not be found.');
			redirect('admin/products/products');
		}
	}
	
	
	function home_products()
	{
		$arr['page']	= 'feature_products';
		$arr['page_title']	= 'Feature Products';
		$arr['arrHomeProducts']	= $this->Product_model->getHomeProducts();
		//print_r($arr['arrHomeProducts']);exit;
		$this->load->view('admin/adminManageHomeProducts', $arr);
	}
	
	function product_zero_quantity($order_by="name", $sort_order="ASC", $code=0, $page=0, $rows=20){
		
		$arr['page']	    = 'zero_products';
		$arr['page_title']	= 'Zero Products';
		$arr['code']		= $code;
		$term				= false;
		$category_id		= false;

		//get the category list for the drop menu
		$arr['categories']	= $this->Category_model->get_categories_tiered();
		
		$post				= $this->input->post(null, false);
		$this->load->model('Search_model');
		
		if($post){
			$term			= json_encode($post);
			$code			= $this->Search_model->record_term($term);
			$arr['code']	= $code;
		}elseif ($code){
			$term			= $this->Search_model->get_term($code);
		}
		
		//store the search term
		$arr['term']		= $term;
		$arr['order_by']	= $order_by;
		$arr['sort_order']	= $sort_order;
		
		$arr['products']	= $this->Product_model->products_zero_quantity(array('term'=>$term, 'order_by'=>$order_by, 'sort_order'=>$sort_order, 'rows'=>$rows, 'page'=>$page));

		//total number of products
		$arr['total']		= $this->Product_model->products_zero_quantity(array('term'=>$term, 'order_by'=>$order_by, 'sort_order'=>$sort_order), true);

		
		$this->load->library('pagination');
		
		$config['base_url']			= site_url('admin/products/products_archive/'.$order_by.'/'.$sort_order.'/'.$code.'/');
		$config['total_rows']		= $arr['total'];
		$config['per_page']			= $rows;
		$config['uri_segment']		= 7;
		$config['first_link']		= 'First';
		$config['first_tag_open']	= '<li>';
		$config['first_tag_close']	= '</li>';
		$config['last_link']		= 'Last';
		$config['last_tag_open']	= '<li>';
		$config['last_tag_close']	= '</li>';

		$config['full_tag_open']	= '<ul class="pagination">';
		$config['full_tag_close']	= '</ul>';
		$config['cur_tag_open']		= '<li class="active"><a href="#">';
		$config['cur_tag_close']	= '</a></li>';
		
		$config['num_tag_open']		= '<li>';
		$config['num_tag_close']	= '</li>';
		
		$config['prev_link']		= '&laquo;';
		$config['prev_tag_open']	= '<li>';
		$config['prev_tag_close']	= '</li>';

		$config['next_link']		= '&raquo;';
		$config['next_tag_open']	= '<li>';
		$config['next_tag_close']	= '</li>';
		
		$this->pagination->initialize($config);
		//echo $this->config->item('admin');
		
		$this->load->view('admin/product_zero_quantity', $arr);
	}
	   public function import(){
		
        if ($this->input->post('importSubmit')) {
			
            $path = 'uploads/app/';

            $config['upload_path'] = $path;
            $config['allowed_types'] = '*';
            $config['remove_spaces'] = TRUE;
			
			$this->load->library('upload', $config);
            $this->upload->initialize($config);
            $uploaded	= $this->upload->do_upload('file');
			// Form field validation rules
            $this->form_validation->set_rules('file', 'XLS file');
            
            // Validate submitted form data
            if($this->form_validation->run() == true){
			
				
		   
            if (!$this->upload->do_upload('file')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $data = array('upload_data' => $this->upload->data());
            }
            
            if (!empty($data['upload_data']['file_name'])) {
                $import_xls_file = $data['upload_data']['file_name'];
            } else {
                $import_xls_file = 0;
            }
            $inputFileName = $path . $import_xls_file;
            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch (Exception $e) {
                die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                        . '": ' . $e->getMessage());
            }
            $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
		
            $arrayCount = count($allDataInSheet);
            $flag = 0;
            $createArray = array(
			'sku',
			'hsn_code',
			'courier_weight',
			'category_id',
            'product_name',
            'excerpt',
            'product_size',
            'price',
			'keywords',
            'description'
			);
            $SheetDataKey = array();
            foreach ($allDataInSheet as $dataInSheet) {
                foreach ($dataInSheet as $key => $value) {
                    if (in_array(trim($value), $createArray)) {
                        $value = preg_replace('/\s+/', '', $value);
                        $SheetDataKey[trim($value)] = $key;
                    } else {
                        
                    }
                }
            }
			
           $data = array_diff_key($makeArray, $SheetDataKey);
		  
            if (empty($data)) {
                $flag = 1;
            }
            if ($flag == 1) {
				
				$insertCount = $updateCount = $rowCount = $notAddCount = 0;
                for ($i = 2; $i <= $arrayCount; $i++) {
                    $addresses = array();
					$sku     = $SheetDataKey['sku'];
					$hsn_code     = $SheetDataKey['hsn_code'];
					$courier_weight     = $SheetDataKey['courier_weight'];
					$category_id     = $SheetDataKey['category_id'];
					
					$product_name       = $SheetDataKey['product_name'];
                    $excerpt          = $SheetDataKey['excerpt'];			
                    $is_active        = '1';
                    $product_size = $SheetDataKey['product_size'];
                    $price    = $SheetDataKey['price'];
					$keywords  = $SheetDataKey['keywords'];
					
					$description      = $SheetDataKey['description'];
					
					$sku = filter_var(trim($allDataInSheet[$i][$sku]), FILTER_SANITIZE_STRING);
                    $hsn_code = filter_var(trim($allDataInSheet[$i][$hsn_code]), FILTER_SANITIZE_STRING);
					$courier_weight = filter_var(trim($allDataInSheet[$i][$courier_weight]), FILTER_SANITIZE_STRING);
					$category_id = filter_var(trim($allDataInSheet[$i][$category_id]), FILTER_SANITIZE_STRING);
					
                    $product_name = filter_var(trim($allDataInSheet[$i][$product_name]), FILTER_SANITIZE_STRING);
                    $is_active = filter_var(trim($allDataInSheet[$i][$is_active]), FILTER_SANITIZE_EMAIL);
                    $excerpt = filter_var(trim($allDataInSheet[$i][$excerpt]), FILTER_SANITIZE_STRING);
                    $product_size = filter_var(trim($allDataInSheet[$i][$product_size]), FILTER_SANITIZE_STRING);
					$price = filter_var(trim($allDataInSheet[$i][$price]), FILTER_SANITIZE_STRING);
					$keywords = filter_var(trim($allDataInSheet[$i][$keywords]), FILTER_SANITIZE_STRING);
					$description = filter_var(trim($allDataInSheet[$i][$description]), FILTER_SANITIZE_STRING);
					
					$rowCount++;
                            $quesData = array(
							    'sku'=>$sku,
								'hsn_code'=>$hsn_code,
								'courier_weight'=>$courier_weight,
								'category_id'=>$category_id,
								'product_name' => $product_name,
								'excerpt' => $excerpt,
								'product_size' => $product_size,
                                'price' => $price,                     
								'is_active' => '1',
                                'keywords' => $keywords,              
								'description' => $description
                            );
							
                                $this->load->helper('text');
			
			if($sku!='' && $product_name!='')
			{
			$slug = $product_name;
			$slug	= url_title(convert_accented_characters($slug), 'dash', TRUE);
			
			
			//save categories
			$categories			= explode('-',$category_id);
			if(!$categories)
			{
				$categories	= array();
			}
			//validate the slug
			$this->load->model('Routes_model');
			$slug	= $this->Routes_model->validate_slug($slug);

			$route['slug']	            = $slug;	
			$route_id	                = $this->Routes_model->save($route);

			$save['sku']				= $sku;
			$save['name']				= $product_name;
			$save['hsn_code']			= $hsn_code;
			$save['courier_weight']		= $courier_weight;
            $save['product_size']		= $product_size;
			$save['excerpt']			= $excerpt;
			$save['price']				= $price;
			$save['seo_title']			= $keywords;
			$save['description']		= $description;
			$save['slug']				= $slug;
			$save['track_stock']	    = 1;
			$save['quantity']			= 1000;
			

			// save product 
			$product_id	= $this->Product_model->save($save,'',$categories);

			//save the route
			$route1['id']	= $route_id;
			$route1['slug']	= $slug;
			$route1['route']	= 'cart/product/'.$product_id;
			$this->Routes_model->save($route1);			
					unset($route_id);			
					unset($product_id);		 
                         $insertCount++;
			}               
                           
                }              
               
				 // Status message with imported data count
                        $notAddCount = ($rowCount - ($insertCount + $updateCount));
                        $successMsg = 'Questions have imported successfully. Total Rows ('.$rowCount.') | Inserted ('.$insertCount.') | Updated ('.$updateCount.') | Not Inserted ('.$notAddCount.')';
                        $this->session->set_userdata('success_msg', $successMsg);
				redirect('admin/products');
            } else {
               $this->session->set_userdata('error_msg', 'Error on file upload,column is not matching, please try again.');
			   redirect('admin/questionupload');
            }
			}else{
				
                $this->session->set_userdata('error_msg', 'Invalid file, please select only xls file.');
				redirect('admin/questionupload');
            
			}
        }
        
        $this->load->view('admin/questionupload', $data);
    
	}
    
	
    /*
     * Callback function to check file value and type during validation
     */
    public function file_check($str){
        $allowed_mime_types = array('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        if(isset($_FILES['file']['name']) && $_FILES['file']['name'] != ""){
            $mime = get_mime_by_extension($_FILES['file']['name']);
            $fileAr = explode('.', $_FILES['file']['name']);
            $ext = end($fileAr);
			
            if(($ext == 'xlsx') && in_array($mime, $allowed_mime_types)){
                return true;
            }else{
                $this->form_validation->set_message('file_check', 'Please select only xlsx file to upload.');
                return false;
            }
        }else{
            $this->form_validation->set_message('file_check', 'Please select a xlsx file to upload.');
            return false;
        }
    }

	
}
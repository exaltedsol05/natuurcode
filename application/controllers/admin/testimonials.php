<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Testimonials extends CI_Controller {	
	
	private $use_inventory = false;
	
	function __construct()
	{		
		
        parent::__construct();
        $this->load->library('form_validation');
		
        if (!$this->session->userdata('is_admin_login')) {
            redirect('admin/home');
        }
		if ($this->session->userdata('roll')!='admin') {
            redirect('admin/home');
        }
		
		$this->load->model('Product_model');
		$this->load->model('Category_model');
		$this->load->model('admin_model');
		
		$this->load->helper('form');
	}

	function index()
	{
		$arr['page']  = 'testimonial';
		$arr['arrTestimonial'] = $this->admin_model->getTestimonialList();
		//echo "<pre>";print_r($arr['arrTestimonial']);exit;
		$this->load->view('admin/vwTestimonial',$arr);
	}
	function change_testimonial_status()
    {
        $id = $this->input->post('id');
        $status = $this->input->post('status');
        
        if ($this->admin_model->changeTestimonialStatus($id,$status))
        {
            echo "1";
            exit;
        }
    }
    public function addtestimonials()
    {
        $arr['page']='testimonial';
       
        $this->load->view('admin/adminAddTestimonials',$arr);
    }
    function insertNewTestimonial()
    {
        $data = array( 
			'email_id'        	=> $this->input->post('email'),
			'content'         	=> $this->input->post('test_content'),
			'company'   		=> $this->input->post('c_name'),
			'designation'   	=> $this->input->post('designation'),
			'status'         	=> 1
        );

        $testimonial_id = $this->input->post('testimonial_id');

        if(isset($testimonial_id) && !empty($testimonial_id))
        {
            if( $this->admin_model->updateTesimonial($data,$testimonial_id))
            {
                $this->session->set_flashdata('message', 'Testimonial updated sucessfully.');
                redirect('admin/testimonials');
            }
        }
        else
        {
            if( $this->admin_model->inserTestimonial($data))
            {
                $this->session->set_flashdata('message', 'New testimonial added sucessfully.');
                redirect('admin/testimonials');
            }
        }
    }
    public function delete_testimonial($id)
    {
        if($this->admin_model->delete_testimonial($id))
        {
            $this->session->set_flashdata('message', 'Testimonial deleted sucessfully.');
            redirect('admin/testimonials');
        }
    }
    function editTestimonial()
    {
		$arr['page'] = 'testimonial';
		$test_id = $this->uri->segment('4');

       $arr['testimonial'] = $this->admin_model->testimonialDetails($test_id);

        //print_r($data);

       $this->load->view('admin/adminAddTestimonials',$arr);

    }
}
<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Workshops extends CI_Controller {
	private $use_inventory = false;
	
	function __construct(){
        parent::__construct();
        $this->load->library('form_validation');
		
        if (!$this->session->userdata('is_admin_login')) {
            redirect('admin/home');
        }
		
		if ($this->session->userdata('roll')!='admin') {
            redirect('admin/home');
        }
		
		$this->load->model('Product_model');
		$this->load->model('Category_model');
		$this->load->model('workshop_model');
		
		$this->load->helper('form');
	}

	function index(){
		$arr['page']  = 'workshops';
		$arr['arrWorkshop'] = $this->workshop_model->getWorkshopList();
		$this->load->view('admin/vwWorkshop',$arr);
	}
	
	function change_workshop_status(){
        $id = $this->input->post('id');
        $status = $this->input->post('status');
        
        if ($this->workshop_model->changeWorkshopStatus($id,$status)){
            echo "1";
            exit;
        }
    }
	
    public function addworkshops(){
        $arr['page']='workshops';
        $this->load->view('admin/adminAddWorkshops',$arr);
    }
	
	
	function form($id = false){
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		//set the default values
		$data['id']			= '';
		$data['workshop_date']		= '';
		$data['workshop_time']	= '';
		$data['number']	= '';
		$data['workshop_type']		= '';
		$data['workshop_price']	= '';
		$data['status']	= '';
		$data['page']	= 'Workshop Form';
		
		if($id){
			$workshop			= $this->workshop_model->workshopDetails($id);
			
			if(!$workshop){
				//workshop does not exist
				$this->session->set_flashdata('error', 'The request workshop could not be found.');
				redirect('admin/workshops');
			}
			
			//set values to db values
			$data['id']			= $workshop->id;
			$data['workshop_date']	= $workshop->workshop_date;
			$data['workshop_time']	= $workshop->workshop_time;
			$data['number']	= $workshop->number;
			$data['workshop_type']		= $workshop->workshop_type;
			$data['workshop_price']	= $workshop->workshop_price;
		}
		
		$this->form_validation->set_rules('workshop_date', 'Date', 'trim|required');
		$this->form_validation->set_rules('workshop_time', 'Time', 'trim|required');
		$this->form_validation->set_rules('workshop_type', 'Type', 'trim|required');
		
		// Validate the form
		if($this->form_validation->run() == false){
			$this->load->view('admin/workshop_form', $data);
		}else{
			$this->load->helper('text');
			
			//validate the slug
			$save = array();
			$save['id']			= $id;;
			$save['workshop_date']		= $this->input->post('workshop_date');
			$save['workshop_time']	= $this->input->post('workshop_time'); 
			$save['number']	= $this->input->post('number');
			$save['workshop_type']		= $this->input->post('workshop_type');
			$save['workshop_price']	= $this->input->post('workshop_price');
			//save the page
			$page_id	= $this->workshop_model->save_workshop($save);
			$this->session->set_flashdata('message', 'The Workshop has been saved.');
			
			//go back to the page list
			redirect('admin/workshops');
		}
	}
	
	
	
    public function delete_workshop($id)
    {
        if($this->workshop_model->delete_workshop($id))
        {
            $this->session->set_flashdata('message', 'Workshops deleted sucessfully.');
            redirect('admin/workshops');
        }
    }
}
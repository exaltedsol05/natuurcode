<?php
//error_reporting(E_ALL);

class Fedex extends CI_Controller {	
	function __construct(){
		parent::__construct();
		$this->load->model('Order_model');
		$this->load->model('Search_model');
		$this->load->model('location_model');
		$this->load->helper(array('formatting'));		
	}

	function objToArray($obj, &$arr){
		if(!is_object($obj) && !is_array($obj)){
			$arr = $obj;
			return $arr;
		}

		foreach ($obj as $key => $value){
			if (!empty($value)){
				$arr[$key] = array();
				objToArray($value, $arr[$key]);
			}else{
				$arr[$key] = $value;
			}
		}
		return $arr;
	}	

	function track_shipment($trackid){
		$request = array();
		$request['WebAuthenticationDetail'] = array(
			'ParentCredential' =>array(
				'Key' => 'VbNYGYmJbrpLRY18', 
				'Password' => 'jN1DEXbvYlEuP5AuPFTEnUzMd'
			),
			'UserCredential' =>array(
				'Key' => 'VbNYGYmJbrpLRY18', 
				'Password' => 'jN1DEXbvYlEuP5AuPFTEnUzMd'
			)
		);
		
		$request['ClientDetail'] = array(
			'AccountNumber' => '781435546', 
			'MeterNumber' => '109803879',
			'Localization' => array(
				'LanguageCode'	=> 'EN',
				'LocaleCode'	=> 'us'
			)
		);
		
		$request['TransactionDetail'] = array(
			'CustomerTransactionId' => 'Ground Track By Number',
			'Localization' => array(
				'LanguageCode'	=> 'EN',
				'LocaleCode'	=> 'us'
			)	
		);
		
		$request['Version'] = array(
			'ServiceId' => 'trck', 
			'Major' => '12', 
			'Intermediate' => '0', 
			'Minor' => '0'
		);
		
		$request['TrackRequestProcessingOptionType']=array(
			'INCLUDE_DETAILED_SCANS' => true 
		);
		
		$request['SelectionDetails'] = array(
			'CarrierCode' => 'FDXE',
			'PackageIdentifier' => array(
				'Type' => 'TRACKING_NUMBER_OR_DOORTAG',
				'Value' => $trackid // Replace 'XXX' with a valid tracking identifier
			)
		);
		
		$this->load->library('createshipmentfedex');	
		$data['response'] = $this -> createshipmentfedex -> track_order($request);
		
		if($data['response']->HighestSeverity == "SUCCESS"){
			$trackDetails = $data['response']->CompletedTrackDetails->TrackDetails;
			$ShipperAddress = $trackDetails->ShipperAddress;
			$DestinationAddress = $trackDetails->DestinationAddress;
			$DestinationAddress = $trackDetails->DestinationAddress;
			
			$eventDetails = $data['response']->CompletedTrackDetails->TrackDetails->Events;
			$EventType = $eventDetails->EventType;
			$EventDescription = $eventDetails->EventDescription;
			$DatesOrTimes = $trackDetails->DatesOrTimes;
			
			$CreationTime = "";
			$AnticipatedTender = "";
			
			foreach($DatesOrTimes as $key => $value){
				if($value->Type == "ACTUAL_TENDER"){
					$CreationTime = $value->DateOrTimestamp;
				}else if($value->Type == "ANTICIPATED_TENDER"){
					$CreationTime = $value->DateOrTimestamp;
				}else{
					$CreationTime = $trackDetails->StatusDetail->CreationTime;
				}
				
				if($value->Type == "ESTIMATED_DELIVERY"){
					$AnticipatedTender = $value->DateOrTimestamp;
				}else if($value -> Type == "ACTUAL_DELIVERY"){
					$AnticipatedTender = $value->DateOrTimestamp;
				}
			}
			
			if(empty($CreationTime)){
				$CreationTime = "Not found";
			}else{
				$CreationTime = date("M d, Y", strtotime($CreationTime));
			}	
			
			if(empty($AnticipatedTender)){
				$AnticipatedTender = "Pending";
			}else{
				$AnticipatedTender = date("M d, Y", strtotime($AnticipatedTender));
			}

			$response = '<div class="col-sm-12">';
			$response .= '<div class="pull-right" style="height:80px; border:1px solid #ddd; padding:10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px; width:475px;>';
			$response .= '<div style="">';
			$response .= '<div class="pull-left col-md-3">';
			$response .= '<p>Ship Date</p>';
			$response .= '<span style="font-size:14px;"><strong>'.$CreationTime.'</strong></span>';
			$response .= '</div>';
			$response .= '<div class="col-md-6 text-center">';
			$response .= '<div class="status_in_'.$EventType.'" style=""></div>';
			$response .= '<span style="font-size:14px;"><strong>'.$EventDescription.'</strong></span>';
			$response .= '</div>';
			$response .= '<div class="pull-right col-md-3">';
			$response .= '<p>Delivery Date</p>';
			$response .= '<span style="font-size:14px;"><strong>'.$AnticipatedTender.'</strong></span>';
			$response .= '</div>';
			$response .= '</div>';	
			$response .= '</div>';
			echo $response .= '</div>';
		}else{
			echo "Error : Please try again.";
		}
	}	
}
<?php
class Orders extends CI_Controller {	
	function __construct(){		
		parent::__construct();
		$this->
		load->model('Order_model');
		$this->load->library('natuur');
		$this->load->model('Search_model');
		$this->load->model('location_model');
		$this->load->helper(array('formatting'));
		
		if (!$this->session->userdata('is_admin_login')) {
            $this->load->view('admin/adminLogin');
        }
	}

	function index($sort_by='order_number',$sort_order='desc', $code=0, $page=0, $rows=15){
		//if they submitted an export form do the export
		if($this->input->post('submit') == 'export'){
			$this->load->model('customer_model');
			$this->load->helper('download_helper');
			$post	= $this->input->post(null, false);
			$term	= (object)$post;
			$data['orders']	= $this->Order_model->get_orders($term);		

			foreach($data['orders'] as &$o){
				$o->items	= $this->Order_model->get_items($o->id);
			}
			force_download_content('orders.xml', $this->load->view('admin/orders_xml', $data, true));
			die;
		}
		
		$this->load->helper('form');
		$this->load->helper('date');
		$data['message']	= $this->session->flashdata('message');
		$data['page']	= 'Orders';
		$data['code']		= $code;
		$term				= false;
		$post	= $this->input->post(null, false);
		
		if($post){
			//if the term is in post, save it to the db and give me a reference
			$term			= json_encode($post);
			$code			= $this->Search_model->record_term($term);
			$data['code']	= $code;
			//reset the term to an object for use
			$term	= (object)$post;
		}elseif ($code){
			$term	= $this->Search_model->get_term($code);
			$term	= json_decode($term);
		} 
 		
		$data['term']	= $term;
 		$data['orders']	= $this->Order_model->get_orders($term, $sort_by, $sort_order, $rows, $page);
		$data['total']	= $this->Order_model->get_orders_count($term);
		$this->load->library('pagination');
		$config['base_url']			= site_url('admin/orders/index/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
		$config['total_rows']		= $data['total'];
		$config['per_page']			= $rows;
		$config['uri_segment']		= 7;
		$config['first_link']		= 'First';
		$config['first_tag_open']	= '<li>';
		$config['first_tag_close']	= '</li>';
		$config['last_link']		= 'Last';
		$config['last_tag_open']	= '<li>';
		$config['last_tag_close']	= '</li>';
		$config['full_tag_open']	= '<nav><ul  class="pagination">';
		$config['full_tag_close']	= '</ul></nav>';
		$config['cur_tag_open']		= '<li class="active"><a href="#">';
		$config['cur_tag_close']	= '</a></li>';
		$config['num_tag_open']		= '<li>';
		$config['num_tag_close']	= '</li>';
		$config['prev_link']		= '&laquo;';
		$config['prev_tag_open']	= '<li>';
		$config['prev_tag_close']	= '</li>';
		$config['next_link']		= '&raquo;';
		$config['next_tag_open']	= '<li>';
		$config['next_tag_close']	= '</li>';
		$this->pagination->initialize($config);
		$data['sort_by']	= $sort_by;
		$data['sort_order']	= $sort_order;				
		$this->load->view('admin/orders', $data);
	}
	
	function export(){
		$this->load->model('customer_model');
		$this->load->helper('download_helper');
		$post	= $this->input->post(null, false);
		$term	= (object)$post;
		$data['orders']	= $this->Order_model->get_orders($term);
		
		foreach($data['orders'] as &$o){
			$o->items	= $this->Order_model->get_items($o->id);
		}
		
		force_download_content('orders.xml', $this->load->view('admin/orders_xml', $data, true));
	}

	function order($id){
		$this->load->helper(array('form', 'date'));
		$this->load->library('form_validation');
		
		//$this->load->model('Gift_card_model');
		$this->form_validation->set_rules('notes', 'Notes', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$message	= $this->session->flashdata('message');
		
		if ($this->form_validation->run() == TRUE){			
			$save			= array();
			$save['id']		= $id;
			$save['notes']	= $this->input->post('notes');
			$save['status']	= $this->input->post('status');
			$data['message']	= 'The order has been updated.';
			
			$this->Order_model->save_order($save);
			$this->session->set_flashdata('message', 'Record Saved has been updated!');
						
			$mailsent = $this->input->post('mailtouser');
			if(!empty($mailsent)){
				$this->load->helper('string');
				$this->load->library('email');
				
				$tomail = $this->input->post('toemail');
				$this->email->from("akgupta.ch@gmail.com");
				$this->email->to($tomail);
				$this->email->subject("Naturalmama: Order Status Change");
				$msg = "Dear Customer,\r\n\r\n";
				$msg .= "Your Order Status has been change to ". $save['status'] .".";
				$msg .= "\r\n\r\n".$save['notes'];
				$msg .= "\r\n\r\nFrom\r\nNaturalmama";
				
				$this->email->message($msg);
				if($this->email->send()){
					$this->session->set_flashdata('message', 'Record update and Email has been sent!');
				}				
			}			
			
			
			
			//sent sms
			//$sms['name']		= $this->input->post('name');
	    	//	$sms['phone'] 		= $this->input->post('phone');
			//$sms['order_no'] 	= $this->input->post('order_no');
			//$this->natuur->sendsms($sms, $save['status']);
		}

		//get the order information, this way if something was posted before the new one gets queried here
		$data['page']	= 'View Order';
		$data['order']	= $this->Order_model->get_order($id);
		
		$this->load->model('Messages_model');
    	$msg_templates = $this->Messages_model->get_list('order');
		// replace template variables

    	foreach($msg_templates as $msg){
 			// fix html
 			$msg['content'] = str_replace("\n", '', html_entity_decode($msg['content']));

 			// {order_number}
 			$msg['subject'] = str_replace('{order_number}', $data['order']->order_number, $msg['subject']);
			$msg['content'] = str_replace('{order_number}', $data['order']->order_number, $msg['content']);

    		// {url}
			$msg['subject'] = str_replace('{url}', $this->config->item('base_url'), $msg['subject']);
			$msg['content'] = str_replace('{url}', $this->config->item('base_url'), $msg['content']);
			// {site_name}

			$msg['subject'] = str_replace('{site_name}', $this->config->item('company_name'), $msg['subject']);
			$msg['content'] = str_replace('{site_name}', $this->config->item('company_name'), $msg['content']);
			$data['msg_templates'][]	= $msg;
    	}
		
		// we need to see if any items are gift cards, so we can generate an activation link
		$data['order_history'] = $this->Order_model->get_order_status($id);
		$data['setting_info'] = $this->Order_model->get_setting_info();
		//echo "<pre>";print_r($data);exit;
		$this->load->view('admin/order', $data);
	}

	

	function packing_slip($order_id){
		$this->load->helper('date');
		$data['order']		= $this->Order_model->get_order($order_id);
		$this->load->view('admin/packing_slip.php', $data);
	}

	function edit_status(){
    	//$this->auth->is_logged_in();
    	$order['id']		= $this->input->post('id');
    	$order['status']	= $this->input->post('status');
		
		$old_order_details = $this->Order_model->find_old_status($order['id']);
		
		$save['id'] = $old_order_details->id;
		$save['status'] = $order['status'];
		$save['notes'] = 'Order status change by admin to '.$order['status'];
		$old_status = $old_order_details->status;
        
		if($old_status != $order['status']){
			$this->Order_model->save_order($save);
			if($old_status != "Delivered" && $order['status'] == "Delivered"){
				$order['customerid']	= $this->input->post('customer_id');
				$order['change_amt']	= $this->input->post('change_amt');
			}
		}
    }

    
    function send_notification($order_id=''){
			// send the message
   		$this->load->library('email');
		$config['mailtype'] = 'html';
		$this->email->initialize($config);
		$this->email->from('orders@natuur.in', 'Natuur');
		$this->email->to($this->input->post('recipient'));
		$this->email->subject($this->input->post('subject'));
		$this->email->message(html_entity_decode($this->input->post('content')));
		$this->email->send();
		$this->session->set_flashdata('message', 'Email notification has been sent!');
		redirect('admin/orders/order/'.$order_id);
	}

	function bulk_delete(){
    	$orders	= $this->input->post('order');
		if($orders){
			foreach($orders as $order){
	   			$this->Order_model->delete($order);
	   		}
			$this->session->set_flashdata('message', 'The selected orders have been deleted');
		}else{
			$this->session->set_flashdata('error', 'You did not select any orders to delete');

		}

   		//redirect as to change the url

		redirect('admin/orders');	

    }

	
}
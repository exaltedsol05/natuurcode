<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Home extends CI_Controller {
    var $admin_id		= false;
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
		$this->load->helper('form');
        $this->load->library('form_validation');
		$this->load->model('Admin_model');
		
    }

    public function index() {
         if ($this->session->userdata('is_admin_login')) {
            redirect('admin/dashboard');
        } else {
        $this->load->view('admin/adminLogin');
        }
    }
	public function registration() {
         if ($this->session->userdata('is_admin_login')) {
            redirect('admin/dashboard');
        } else {
            $this->load->view('admin/registration_form');
		
        }
    }
	
	public function updateprofile($id = false) {
	     $arr['page'] = 'Manage Profile';
		
         $submitted 			= $this->input->post('hidRfrm');
		if ($submitted)
		{
			$save['id']		= $id;
			$save['username']		= $this->input->post('username');
			$save['shopname']		= $this->input->post('shopname');
			$save['shopaddress']	= $this->input->post('shopaddress');
			$save['state']			= $this->input->post('state');
			$save['city']		    = $this->input->post('city');
			$save['landline']		= $this->input->post('landline');
			$save['area']		    = $this->input->post('area');
			$save['pincode']		= $this->input->post('pincode');
			$save['mobile']	    = $this->input->post('usermobile');

			$this->Admin_model->save($save);
		}
		$arr['id'] = $id;
		$arr['row'] = $this->Admin_model->get_admin($id);
		$this->load->view('admin/adminEditUser',$arr);
		
	}
	
    public function do_forgot() {
			if ($this->session->userdata('is_admin_login')) {
				redirect('admin/dashboard');
			} else {
					@$email = $_POST['email'];
					$this->form_validation->set_rules('email', 'Email', 'required');
					if ($this->form_validation->run() == FALSE) {
					    
						$this->load->view('admin/adminLogin');
					} else {
							//$password='AAB1234AAB';
							
							
							
							$sql = "SELECT * FROM admin WHERE email= ? ";
							$val = $this->db->query($sql,array($email));
								if ($val->num_rows) {
								$password=random_string('alnum', 8);
								$salt = '5&JDDlwz%Rwh!t2Yg-Igae@QxPzFTSId';
								$enc_pass  = md5($salt.$password);
								$sql = "UPDATE admin SET password=? WHERE email=?";
								$this->db->query($sql,array($enc_pass,$email));
								$subject="Password reset";
								$message="Your password has been reset to ". $password .".";
								$this->sendMail($subject,$email,$message);
								$data['messages']='<div class="alert alert-success">Your password has been sent your email id . Please login your account <a href="'.base_url().'admin/home">click here.</a></div>';
							}
							else{
								$data['messages']='<div class="alert alert-danger">Your email id is not match in our database .Please login your account <a href="'.base_url().'admin/home">click here.</a></div>';
							}
                            echo json_encode($data['messages']);							
							exit;
					}
			}
			
	}
     public function do_login() {

        if ($this->session->userdata('is_admin_login')) {
            redirect('admin/dashboard');
        } else {
            @$user = $_POST['username'];
            @$password = $_POST['password'];

            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('admin/adminLogin');
            } else {
                $salt = '5&JDDlwz%Rwh!t2Yg-Igae@QxPzFTSId';                $enc_pass  = md5($salt.$password);
                $sql = "SELECT * FROM admin WHERE email = ? AND password = ?";
                $val = $this->db->query($sql,array($user ,$enc_pass ));

                if ($val->num_rows) {								
                    foreach ($val->result_array() as $recs => $res) {
                        $this->session->set_userdata(array(
                            'id' => $res['id'],
                            'admin_name' => $res['username'],
                            'admin_email' => $res['email'],
                            'roll' => strtolower($res['access']),							'menuSetting' => $res['menuSetting'],
							'create_date' => $res['create_date'],                           
                            'is_admin_login' => true                           
                                )
                        );
                    }
                    redirect('admin/dashboard');
                } else {
                    $err['error'] = 'Username or Password incorrect';
				
                    $this->load->view('admin/adminLogin', $err);
                }
            }
        }
           }

	public function do_reset(){
	      
					@$email       = $_POST['adminemail'];
					@$oldpassword = $_POST['oldpassword'];
					@$newpassword = $_POST['newpassword'];
					$this->form_validation->set_rules('adminemail', 'Email', 'required');
					$this->form_validation->set_rules('oldpassword', 'Old Password', 'required');
					$this->form_validation->set_rules('newpassword', 'New Password', 'required');
					if ($this->form_validation->run() == FALSE) {
					    $err['error']='please fill all fields';
						$this->load->view('admin/adminFooter',$err);
					} else {
							
							$salt = '5&JDDlwz%Rwh!t2Yg-Igae@QxPzFTSId';
							$enc_pass  = md5($salt.$newpassword);
							$sql = "UPDATE admin SET password=? WHERE email=?";
							
							$this->db->query($sql,array($enc_pass,$email));							
                            $data['messages']='Successfully update your password.';
                            echo json_encode($data['messages']);							
							exit;
					}
			
	}
			
    public function logout() {
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('is_admin_login');   
        $this->session->sess_destroy();
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
        redirect('admin/home', 'refresh');
    }
    function admin(){
		$data['page']	= 'Administrators';		
		$data['admins']		= $this->get_admin_list();		
		$this->load->view('admin/admins', $data);	
	}	    
	function get_admin_list()    
	{        
		$this->db->select('*');        
		$this->db->order_by('lastname', 'ASC');        
		$this->db->order_by('firstname', 'ASC');        
		$this->db->order_by('email', 'ASC');        
		$this->db->order_by('username', 'ASC');        
		$result = $this->db->get('admin');        
		$result = $result->result();                
		return $result;    
	}		    
	function get_admin($id)   
	{        
		$this->db->select('*');        
		$this->db->where('id', $id);        
		$result = $this->db->get('admin');        
		$result = $result->row();        
		return $result;    
	}         
	public function admin_form($id)	{				
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');		
		$data['page']		= 'Assign access level';						
		$data['id']		    = '';		
		$data['firstname']	= '';		
		$data['lastname']	= '';		
		$data['email']		= '';		
		$data['username']	= '';		
		$data['access']		= '';		
		$data['menuSetting']= '';				
		if($id){				
			$this->admin_id		= $id;			
			$admin	= $this->get_admin($id);						
			if (!$admin){				
				$this->session->set_flashdata('message', 'admin_not_found');				
				redirect('admin/home/admin');			
			}					
			$data['id']			= $admin->id;			
			$data['firstname']	= $admin->firstname;			
			$data['lastname']	= $admin->lastname;			
			$data['email']		= $admin->email;			
			$data['username']	= $admin->username;			
			$data['access']		= $admin->access;			
			$data['menuSetting']		= $admin->menuSetting;		
		}				
		$this->form_validation->set_rules('firstname', 'firstname', 'trim|max_length[32]');		$this->form_validation->set_rules('lastname', 'lastname', 'trim|max_length[32]');		$this->form_validation->set_rules('email', 'email','trim|required|valid_email|max_length[128]');		
		$this->form_validation->set_rules('username', 'username', 'trim|required|max_length[128]|callback_check_username');		
		$this->form_validation->set_rules('access', 'access', 'trim|required');						
		if ($this->input->post('password') != '' || $this->input->post('confirm') != '' || !$id)		
		{			
			$this->form_validation->set_rules('password', 'password', 'required|min_length[6]');			
			$this->form_validation->set_rules('confirm', 'confirm_password', 'required|matches[password]');		
		}				
		if ($this->form_validation->run() == FALSE)		
		{						
			$this->load->view('admin/adminRoleSetting', $data);		
		}		
		else		
		{			
			$save['id']		= $id;			
			$save['firstname']	= $this->input->post('firstname');			
			$save['lastname']	= $this->input->post('lastname');			
			$save['email']		= $this->input->post('email');			
			$save['username']	= $this->input->post('username');			
			$save['access']		= $this->input->post('access');			
			$save['menuSetting']	= json_encode($this->input->post('menuSetting'));			if ($this->input->post('password') != '' || !$id)			
			{				 
				$password  = $this->input->post('password');				 
				$salt              = '5&JDDlwz%Rwh!t2Yg-Igae@QxPzFTSId';                 $enc_pass          = md5($salt.$password);				 
				$save['password']	= $enc_pass;			
			}		
			$this->save($save);						
			$this->session->set_flashdata('message', 'admin has been saved!');				
			redirect('admin/home/admin');		
		}    
	}	
	function save($admin)    {        
		if ($admin['id'])        
		{            
			$this->db->where('id', $admin['id']);            
			$this->db->update('admin', $admin);        
		}        
		else        
		{            
			$this->db->insert('admin', $admin);        
		}    
	}	
	public function delete($id)	{				
		if ($this->session->userdata('id') == $id)		
		{			
			$this->session->set_flashdata('message','You cannot delete yourself');			
				redirect('admin/home/admin');			
		}						
		$this->admin_delete($id);		
		$this->session->set_flashdata('message', 'The administrator has been deleted.');		redirect('admin/home/admin');	
	}	
	function admin_delete($id){        
		if ($this->check_id($id))        
		{            
			$admin  = $this->get_admin($id);            
			$this->db->where('id', $id);            
			$this->db->limit(1);            
			$this->db->delete('admin');            
			return $admin->firstname.' '.$admin->lastname.' has been removed.';        
		}        
		else        
		{            
			return 'The admin could not be found.';        
		}    
	}	
	function check_id($str){        
		$this->db->select('id');        
		$this->db->from('admin');        
		$this->db->where('id', $str);        
		$count = $this->db->count_all_results();                
		if ($count > 0)        
		{            
			return true;        
		}        
		else        
		{            
			return false;        
		}       
	}			
	function check_username($str){				
		$email = $this->admin_check_username($str, $this->admin_id);		
		if ($email)		
		{			
			$this->form_validation->set_message('check_username', 'The requested username is already in use.');			
			return FALSE;		
		}		
		else		
		{			
			return TRUE;		
		}	
	}	
	function admin_check_username($str, $id=false)	{		
		$this->db->select('username');        
		$this->db->from('admin');        
		$this->db->where('username', $str);		        
		if ($id)        
		{            
			$this->db->where('id !=', $id);        
		}       
		$count = $this->db->count_all_results();              
		if ($count > 0)        
		{            
			return true;        
		}        
		else        
		{            
			return false;        
		}	
	}    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
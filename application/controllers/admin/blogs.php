<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Blogs extends CI_Controller {
	private $use_inventory = false;
	
	function __construct(){
        parent::__construct();
        $this->load->library('form_validation');
		
        if (!$this->session->userdata('is_admin_login')) {
            redirect('admin/home');
        }
		
		if ($this->session->userdata('roll')!='admin') {
            redirect('admin/home');
        }
		
		$this->load->model('Product_model');
		$this->load->model('Category_model');
		$this->load->model('admin_model');
		
		$this->load->helper('form');
	}

	function index(){
		$arr['page']  = 'blog';
		$arr['arrBlog'] = $this->admin_model->getBlogList();
		$this->load->view('admin/vwBlog',$arr);
	}
	
	function change_blog_status(){
        $id = $this->input->post('id');
        $status = $this->input->post('status');
        
        if ($this->admin_model->changeBlogStatus($id,$status)){
            echo "1";
            exit;
        }
    }
	
    public function addblogs(){
        $arr['page']='blog';
        $this->load->view('admin/adminAddBlogs',$arr);
    }
	
	
	function form($id = false){
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		//set the default values
		$data['id']			= '';
		$data['title']		= '';
		$data['content']	= '';
		$data['blog_image']	= '';
		$data['status']		= '';
		$data['blog_cat']	= '';
		$data['is_popular_blog']	= '';
		$data['is_new']	= '';
		$data['page']	= 'Blog Form';
		
		if($id){
			$blog			= $this->admin_model->blogDetails($id);
			
			if(!$blog){
				//blog does not exist
				$this->session->set_flashdata('error', 'The request blog could not be found.');
				redirect('admin/blogs');
			}
			
			//set values to db values
			$data['id']			= $blog->id;
			$data['title']		= $blog->title;
			$data['content']	= $blog->content;
			$data['blog_image']	= $blog->blog_image;
			$data['status']		= $blog->status;
			$data['blog_cat']	= $blog->blog_cat;
			$data['is_popular_blog']	= $blog->is_popular_blog;
			$data['is_new']	= $blog->is_new;
		}
		
		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		$this->form_validation->set_rules('content', 'Content', 'trim|required');
		$this->form_validation->set_rules('blog_cat', 'Category', 'trim|required');
		
		// Validate the form
		if($this->form_validation->run() == false){
			$this->load->view('admin/blog_form', $data);
		}else{
			$this->load->helper('text');
			
			//validate the slug
			$save = array();
			$save['id']			= $id;;
			$save['title']		= $this->input->post('title');
			$save['content']	= $this->input->post('content'); 
			$save['blog_image']	= $this->input->post('blog_image');
			$save['status']		= $this->input->post('status');
			$save['blog_cat']	= $this->input->post('blog_cat');
			if(!empty($this->input->post('is_popular_blog')))
				$save['is_popular_blog'] = $this->input->post('is_popular_blog');
			else
				$save['is_popular_blog'] = '2';
			
			if(!empty($this->input->post('is_new')))
				$save['is_new'] = $this->input->post('is_new');
			else
				$save['is_new'] = '2';
			
			//save the page
			$page_id	= $this->admin_model->save_blog($save);
			$this->session->set_flashdata('message', 'The Blog has been saved.');
			
			//go back to the page list
			redirect('admin/blogs');
		}
	}
	
    function insertNewBlog(){
        $data = array( 
			'title'         	=> $this->input->post('title'),
			'content'   		=> $this->input->post('content'),
			'blog_image'   		=> $this->input->post('blog_image'),
        );

        $blog_id = $this->input->post('blog_id');

        if(isset($blog_id) && !empty($blog_id))
        {
            if( $this->admin_model->updateBlog($data,$blog_id))
            {
                $this->session->set_flashdata('message', 'Blog details updated sucessfully.');
                redirect('admin/blogs');
            }
        }
        else
        {
            if( $this->admin_model->insertBlog($data))
            {
                $this->session->set_flashdata('message', 'New Blog added sucessfully.');
                redirect('admin/blogs');
            }
        }
    }
	
	
    public function delete_blog($id)
    {
        if($this->admin_model->delete_blog($id))
        {
            $this->session->set_flashdata('message', 'Blog deleted sucessfully.');
            redirect('admin/blogs');
        }
    }
    function editBlog()
    {
		$arr['page'] = 'blog';
		$test_id = $this->uri->segment('4');
        $arr['blog'] = $this->admin_model->blogDetails($test_id);
        $this->load->view('admin/adminAddBlogs',$arr);

    }
	
	function uploadBlogImg(){
		//check whether submit button was clicked or not
        $arr['file_name'] = false;
		$arr['error']	= false;
		$config['allowed_types'] = '*';
	
		$config['upload_path'] = 'uploads/blog/full';
		$config['encrypt_name'] = true;
		$config['remove_spaces'] = true;
              
		$this->load->library('upload', $config);
		$field_name = "Filedata";
          
		if ( $this->upload->do_upload($field_name)){
			$upload_data	= $this->upload->data();
			$this->load->library('image_lib');
			/*
			
			I find that ImageMagick is more efficient that GD2 but not everyone has it
			if your server has ImageMagick then you can change out the line
			
			$config['image_library'] = 'gd2';
			
			with
			
			$config['library_path']		= '/usr/bin/convert'; //make sure you use the correct path to ImageMagic
			$config['image_library']	= 'ImageMagick';
			*/

			//small image
			$config['image_library'] = 'gd2';
			$config['source_image'] = 'uploads/blog/full/'.$upload_data['file_name'];
			$config['new_image']	= 'uploads/blog/small/'.$upload_data['file_name'];
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 235;
			$config['height'] = 235;
			$this->image_lib->initialize($config); 
			$this->image_lib->resize();
			$this->image_lib->clear();

			$arr['file_name']	= $upload_data['file_name'];
			$filename           = $upload_data['file_name'];
			
			if(!empty($this->input->post('tId')))
			{
				$id=$this->input->post('tId');
				$this->Customer_model->updateProfileImage($id,$filename);
			}
			
			$msg='true';

            //@unlink($custPrevImageFull);
            //@unlink($custPrevImageSmall);
		}
		
		if($this->upload->display_errors() != ''){
            echo '4';
			$arr['error'] = $this->upload->display_errors();
			$msg='false';
		}
		
        $result = json_encode(array('data'=>$msg,'imagename'=>$filename));
       	echo  $result;
		die;
	}
}
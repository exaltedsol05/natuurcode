<?php


/* Single page checkout controller*/


class Checkout extends CI_Controller {


	function __construct(){


		parent::__construct();

		$this->load->model('Category_model');

		$this->load->model('Option_model');

		$this->load->model('Search_model');

		$this->load->model('Product_model');		

		$this->load->model('Order_model');		

		$this->load->model('ratenreview_model');

		//load natuur library

		$this->load->library('Banners');
		$this->load->library('Menus');
		$this->load->model('Location_model');
		$this->load->model('Customer_model');
		$this->load->model('Settings_model');
		$this->load->library('natuur');

		//load helpers
		$this->load->helper(array('formatting_helper'));
		$this->customer = $this->natuur->customer();

		/*make sure the cart isnt empty*/

		if($this->natuur->total_items()==0){

			redirect('cart/view_cart');

		}


		/*is the user required to be logged in?*/


		if (config_item('require_login')){

			$this->Customer_model->is_logged_in('checkout');

		}


		if(!config_item('allow_os_purchase') && config_item('inventory_enabled')){


			/*double check the inventory of each item before proceeding to checkout*/


			$inventory_check	= $this->natuur->check_inventory();

			if($inventory_check){


				/*


				OOPS we have an error. someone else has gotten the scoop on our customer and bought products out from under them!


				we need to redirect them to the view cart page and let them know that the inventory is no longer there.


				*/


				$this->session->set_flashdata('error', $inventory_check);

				redirect('cart/view_cart');

			}

		}


		/* Set no caching

		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");


		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");


		header("Cache-Control: no-store, no-cache, must-revalidate");


		header("Cache-Control: post-check=0, pre-check=0", false);


		header("Pragma: no-cache");

		*/


		$this->load->library('form_validation');


	}

	function index(){

		/*show address first*/

		$this->step_1();

	}


	function step_1(){

		$data['customer']	= $this->natuur->customer();

		if(isset($data['customer']['id'])){
			$data['customer_addresses'] = $this->Customer_model->get_address_list($data['customer']['id']);
		}
//echo '<pre>';print_r($this->Customer_model->get_address_list($data['customer']['id']));echo '</pre>';
		/*require a billing address*/
		$this->form_validation->set_rules('address_id', 'Billing Address ID', 'numeric');
		$this->form_validation->set_rules('firstname', 'First Name', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[128]');
		$this->form_validation->set_rules('phone', 'Phone', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('company', 'Company', 'trim|max_length[128]');
		$this->form_validation->set_rules('address1', 'Address1', 'trim|required|max_length[128]');
		$this->form_validation->set_rules('address2', 'Aaddress2', 'trim|max_length[128]');
		$this->form_validation->set_rules('city', 'City', 'trim|required|max_length[128]');
		//$this->form_validation->set_rules('country_id', 'Country', 'trim|required|numeric');
		$this->form_validation->set_rules('use_shipping', 'Ship To Address', '');
		$this->form_validation->set_rules('term', 'Terms and Conditions', 'trim|required');
        $this->form_validation->set_rules('shipping_notes', 'Shipping Information', 'trim|xss_clean');
		$this->form_validation->set_rules('shipping_method', 'Shipping Method', 'trim');

		/* require that a payment method is selected */
		$this->form_validation->set_rules('payment_mode', 'Payment Method', 'trim|required');


		// Relax the requirement for countries without zones
		if($this->Location_model->has_zones($this->input->post('country_id'))){
			$this->form_validation->set_rules('zone_id', 'State', 'trim|required|numeric');
		} else {
			$this->form_validation->set_rules('zone_id', 'State'); // will be empty
		}


		/*if there is post data, get the country info and see if the zip code is required*/
		if($this->input->post('country_id')){

			$country = $this->Location_model->get_country($this->input->post('country_id'));
			if((bool)$country->zip_required){
				$this->form_validation->set_rules('zip', 'Zip', 'trim|required|max_length[10]');
			}
		}else{
			$this->form_validation->set_rules('zip', 'Zip', 'trim|max_length[10]');
		}

		/*require a shipping address*/
		if($this->input->post('use_shipping')!='yes'){

			$this->form_validation->set_rules('address_id1', 'Shipping Address ID', 'numeric');

			$this->form_validation->set_rules('firstname1', 'First Name', 'trim|required|max_length[32]');

			$this->form_validation->set_rules('lastname1', 'Last Name', 'trim|required|max_length[32]');

			$this->form_validation->set_rules('email1', 'Email', 'trim|required|valid_email|max_length[128]');

			$this->form_validation->set_rules('phone1', 'Phone', 'trim|required|max_length[32]');

			$this->form_validation->set_rules('company1', 'Company', 'trim|max_length[128]');

			$this->form_validation->set_rules('address11', 'Address1', 'trim|required|max_length[128]');

			$this->form_validation->set_rules('address21', 'Aaddress2', 'trim|max_length[128]');

			$this->form_validation->set_rules('city1', 'City', 'trim|required|max_length[128]');

			$this->form_validation->set_rules('country_id1', 'Country', 'trim|required|numeric');


			// Relax the requirement for countries without zones
			if($this->Location_model->has_zones($this->input->post('country_id1')))
			{

				$this->form_validation->set_rules('zone_id1', 'State', 'trim|required|numeric');

			} else {

				$this->form_validation->set_rules('zone_id1', 'State'); // will be empty

			}


			/*if there is post data, get the country info and see if the zip code is required*/
			if($this->input->post('country_id1'))
			{

				$country = $this->Location_model->get_country($this->input->post('country_id1'));

				if((bool)$country->zip_required)
				{

					$this->form_validation->set_rules('zip1', 'Zip', 'trim|required|max_length[10]');

				}

			}
			else
			{

				$this->form_validation->set_rules('zip1', 'Zip', 'trim|max_length[10]');

			}

		}


		if ($this->form_validation->run() == false){

			/* where to next? Shipping? */
			$shipping_methods           = $this->_get_shipping_methods();

			$data['total_weight']		= $this -> _calculate_shipping_methods();

			$shipping					= $this->natuur->shipping_method();

			$data['shipping_code']		= $shipping['code'];

			$data['shipping_methods']	= $shipping_methods;

			/* are the being bounced back? */
			$module = $this->input->post('payment_mode');

			if($this->input->post('payment_mode') == "PayU_Money"){

				$description = "PayU Money";

			}else if($this->input->post('payment_mode') == "Icici"){
				$description = "Icici";
			}else{

				$description = "Cash On Delivery";
			}

			$this->natuur->set_payment($module, $description);

			$data['payment_method']		= $this->natuur->payment_method();


			if($this->input->post('use_shipping')===false && isset($data['customer']['bill_address'])){

				//$data['use_shipping'] = ($data['customer']['bill_address'] == @$data['customer']['ship_address']);

				$data['use_shipping']= false;


			} else if($this->input->post('use_shipping')=='yes') {

				 $data['use_shipping'] = true;

			} else {

				 $data['use_shipping'] = false;
			}

			//$this->load->view('checkout/address_form', $data);
			$this->load->view('checkout/checkout_form', $data);


		}else{


			/*load any customer data to get their ID (if logged in)*/


			$customer				= $this->natuur->customer();			


			$customer['bill_address']['company']		= $this->input->post('company');


			$customer['bill_address']['firstname']		= $this->input->post('firstname');


			$customer['bill_address']['lastname']		= $this->input->post('lastname');


			$customer['bill_address']['email']			= $this->input->post('email');


			$customer['bill_address']['phone']			= $this->input->post('phone');


			$customer['bill_address']['address1']		= $this->input->post('address1');


			$customer['bill_address']['address2']		= $this->input->post('address2');


			$customer['bill_address']['city']			= $this->input->post('city');


			$customer['bill_address']['zip']			= $this->input->post('zip');


			/* get zone / country data using the zone id submitted as state*/


			$country								= $this->Location_model->get_country(set_value('country_id'));


			if($this->Location_model->has_zones($country->id))


			{


				$zone									= $this->Location_model->get_zone(set_value('zone_id'));


				$customer['bill_address']['zone']		= $zone->code;  /*  save the state for output formatted addresses */


			} else {


				$customer['bill_address']['zone'] 		= '';


			}


			$customer['bill_address']['country']		= $country->name; /*  some shipping libraries require country name */


			$customer['bill_address']['country_code']   = $country->iso_code_2; /*  some shipping libraries require the code */


			$customer['bill_address']['zone_id']		= $this->input->post('zone_id');  /*  use the zone id to populate address state field value */


			$customer['bill_address']['country_id']		= $this->input->post('country_id');


			/* for guest customers, load the billing address data as their base info as well */			


			if(empty($customer['id']))
			{


				$customer['company']	= $customer['bill_address']['company'];


				$customer['firstname']	= $customer['bill_address']['firstname'];


				$customer['lastname']	= $customer['bill_address']['lastname'];


				$customer['phone']		= $customer['bill_address']['phone'];


				$customer['email']		= $customer['bill_address']['email'];


			}else{				$customer['firstname']	= $customer['bill_address']['firstname'];				$customer['lastname']	= $customer['bill_address']['lastname'];			}


			


			if(!isset($customer['group_id']))


			{


				$customer['group_id'] = 1; /* default group */


			}





			// Use as shipping address


			if($this->input->post('use_shipping')=='yes')


			{


				$customer['ship_address']	= $customer['bill_address'];


			}





			if($this->input->post('use_shipping')!='yes')


			{


					/* load any customer data to get their ID (if logged in) */


				$customer['ship_address']['company']		= $this->input->post('company1');


				$customer['ship_address']['firstname']		= $this->input->post('firstname1');


				$customer['ship_address']['lastname']		= $this->input->post('lastname1');


				$customer['ship_address']['email']			= $this->input->post('email1');


				$customer['ship_address']['phone']			= $this->input->post('phone1');


				$customer['ship_address']['address1']		= $this->input->post('address11');


				$customer['ship_address']['address2']		= $this->input->post('address21');


				$customer['ship_address']['city']			= $this->input->post('city1');


				$customer['ship_address']['zip']			= $this->input->post('zip1');





				/* get zone / country data using the zone id submitted as state*/


				$country								= $this->Location_model->get_country(set_value('country_id1'));


				if($this->Location_model->has_zones($country->id))


				{


					$zone									= $this->Location_model->get_zone(set_value('zone_id1'));


					$customer['ship_address']['zone']			= $zone->code;  /*  save the state for output formatted addresses */


				} else {


					$customer['ship_address']['zone'] 		= '';


				}


				$customer['ship_address']['country']		= $country->name;


				$customer['ship_address']['country_code']   = $country->iso_code_2;


				$customer['ship_address']['zone_id']		= $this->input->post('zone_id1');


				$customer['ship_address']['country_id']		= $this->input->post('country_id1');


			}            			


			


			$this->session->set_flashdata('shipping_cost', $this->input->post('shippingtr'));
          $this->session->set_userdata('shippingCost', $this->input->post('shippingtr'));


			//echo '<pre>'; print_r($_POST); echo '</pre>'; die;


			/* save customer details*/
			$this->natuur->save_customer($customer);


            /* we have shipping details! */
			$this->natuur->set_additional_detail('shipping_notes', $this->input->post('shipping_notes'));

			/* parse out the shipping information */

			$shipping_method	= json_decode($this->input->post('shipping_method'));

			$shipping_code		= md5($this->input->post('shipping_method'));
       

			/* set shipping info */

			$this->natuur->set_shipping($shipping_method[0], $shipping_method[1]->num, $shipping_code);

			$module = $this->input->post('payment_mode');


			if($module == "PayU_Money"){


				$des = "PayU Money";


			}elseif($module == "Icici"){
				$des = "Icici";
			}else{


				$des = "Cash On Delivery";


			}


			$this->natuur->set_payment( $module, $des);


			redirect('checkout/place_order');


		}


	}





	function shipping_address(){


		$data['customer']	= $this->natuur->customer();





		if(isset($data['customer']['id']))


		{


			$data['customer_addresses'] = $this->Customer_model->get_address_list($data['customer']['id']);


		}





		/*require a shipping address*/


		$this->form_validation->set_rules('address_id', 'Billing Address ID', 'numeric');


		$this->form_validation->set_rules('firstname', 'First Name', 'trim|required|max_length[32]');


		$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required|max_length[32]');


		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[128]');


		$this->form_validation->set_rules('phone', 'Phone', 'trim|required|max_length[32]');


		$this->form_validation->set_rules('company', 'Company', 'trim|max_length[128]');


		$this->form_validation->set_rules('address1', 'Address1', 'trim|required|max_length[128]');


		$this->form_validation->set_rules('address2', 'Address2', 'trim|max_length[128]');


		$this->form_validation->set_rules('city', 'City', 'trim|required|max_length[128]');


		$this->form_validation->set_rules('country_id', 'Country', 'trim|required|numeric');


		$this->form_validation->set_rules('zone_id', 'State', 'trim|required|numeric');





		// Relax the requirement for countries without zones


		if($this->Location_model->has_zones($this->input->post('country_id')))


		{


			$this->form_validation->set_rules('zone_id', 'State', 'trim|required|numeric');


		} else {


			$this->form_validation->set_rules('zone_id', 'State'); // will be empty


		}





		/* if there is post data, get the country info and see if the zip code is required */


		if($this->input->post('country_id'))


		{


			$country = $this->Location_model->get_country($this->input->post('country_id'));


			if((bool)$country->zip_required)


			{


				$this->form_validation->set_rules('zip', 'Zip', 'trim|required|max_length[10]');


			}


		}


		else


		{


			$this->form_validation->set_rules('zip', 'Zip', 'trim|max_length[10]');


		}





		if ($this->form_validation->run() == false)


		{


			/* show the address form but change it to be for shipping */


			$data['address_form_prefix']	= 'ship';


			$this->load->view('checkout/address_form', $data);


		}


		else


		{


			/* load any customer data to get their ID (if logged in) */


			$customer				= $this->natuur->customer();





			$customer['ship_address']['company']		= $this->input->post('company');


			$customer['ship_address']['firstname']		= $this->input->post('firstname');


			$customer['ship_address']['lastname']		= $this->input->post('lastname');


			$customer['ship_address']['email']			= $this->input->post('email');


			$customer['ship_address']['phone']			= $this->input->post('phone');


			$customer['ship_address']['address1']		= $this->input->post('address1');


			$customer['ship_address']['address2']		= $this->input->post('address2');


			$customer['ship_address']['city']			= $this->input->post('city');


			$customer['ship_address']['zip']			= $this->input->post('zip');





			/* get zone / country data using the zone id submitted as state*/


			$country								= $this->Location_model->get_country(set_value('country_id'));


			if($this->Location_model->has_zones($country->id))


			{


				$zone									= $this->Location_model->get_zone(set_value('zone_id'));





				$customer['ship_address']['zone']			= $zone->code;  /*  save the state for output formatted addresses */


			} else {


				$customer['ship_address']['zone'] 		= '';


			}


			$customer['ship_address']['country']		= $country->name;


			$customer['ship_address']['country_code']   = $country->iso_code_2;


			$customer['ship_address']['zone_id']		= $this->input->post('zone_id');


			$customer['ship_address']['country_id']		= $this->input->post('country_id');





			/* for guest customers, load the shipping address data as their base info as well */


			if(empty($customer['id']))


			{


				$customer['company']	= $customer['ship_address']['company'];


				$customer['firstname']	= $customer['ship_address']['firstname'];


				$customer['lastname']	= $customer['ship_address']['lastname'];


				$customer['phone']		= $customer['ship_address']['phone'];


				$customer['email']		= $customer['ship_address']['email'];


			}





			if(!isset($customer['group_id']))


			{


				$customer['group_id'] = 1; /* default group */


			}





			/*  save customer details */


			$this->natuur->save_customer($customer);





			/* send to the next form */


			redirect('checkout/step_2');


		}


	}





	function step_2()


	{


		


		


		/* where to next? Shipping? */


		$shipping_methods = $this->_get_shipping_methods();





		if($shipping_methods)


		{


			$this->shipping_form($shipping_methods);


		}


		/* now where? continue to step 3 */


		else


		{


			$this->step_3();


		}


	}





	protected function shipping_form($shipping_methods)
	{

		$data['customer']			= $this->natuur->customer();

		/* do we have a selected shipping method already? */


		$shipping					= $this->natuur->shipping_method();

		$data['shipping_code']		= $shipping['code'];

		$data['shipping_methods']	= $shipping_methods;

		$this->form_validation->set_rules('shipping_notes', 'Shipping Information', 'trim|xss_clean');

		$this->form_validation->set_rules('shipping_method', 'Shipping Method', 'trim|callback_validate_shipping_option');

		if($this->form_validation->run() == false)
		{

			$this->load->view('checkout/shipping_form', $data);

		}
		else
		{

		/* we have shipping details! */

			$this->natuur->set_additional_detail('shipping_notes', $this->input->post('shipping_notes'));

			/* parse out the shipping information */


			$shipping_method	= json_decode($this->input->post('shipping_method'));


			$shipping_code		= md5($this->input->post('shipping_method'));


			/* set shipping info */


			$this->natuur->set_shipping($shipping_method[0], $shipping_method[1]->num, $shipping_code);

			redirect('checkout/step_3');

		}


	}



	/*


		callback for shipping form 


		if callback is true then it's being called for form_Validation


		In that case, set the message otherwise just return true or false


	*/

	function validate_shipping_option($str, $callback=true)
	{

		$shipping_methods	= $this->_get_shipping_methods();

		if($shipping_methods)
		{

			foreach($shipping_methods as $key=>$val)
			{
		    	$check	= json_encode(array($key, $val));

				if($str	== md5($check))
				{

					return $check;

				}

			}


		}


		/* if we get there there is no match and they have submitted an invalid option */


		$this->form_validation->set_message('validate_shipping_option', 'Please select a valid shipping method.');

		return FALSE;

	}



	private function _calculate_shipping_methods(){


		$subtotal = $this->natuur->subtotal();

		if($this->session->userdata('redim_amt')){


			$reedmamt = $this->session->userdata('redim_amt');


			$subtotal = $subtotal-$reedmamt;


		}


		


		$total_weight = 0;


		$freeshipping = array();


		$chargeshipping = array();


		


		foreach ($this->natuur->contents() as $cartkey=>$product){


			$isfree = $product['shippable'];


			if(!$isfree){


				//echo "free";


				$freeshipping[] = array('weight' => $product['weight'],'total_weight' => $total_weight + $product['weight'],'quantity' => $product['quantity']);	


			}else{


				//echo "paid";


				$chargeshipping[] = array('weight' => $product['weight'],'total_weight' => $total_weight + $product['weight'],'quantity' => $product['quantity']);	


			}


			


			$weight = $product['weight'] * $product['quantity']; 


			$total_weight = $total_weight + $weight;


			$quantity = $product['quantity'];


		}


		/* if($subtotal > 2000){


			if(!empty($freeshipping) && empty($chargeshipping)) {


				 // only free shipping product in cart


				 $total_shipping_charge = 0;


			}else if(empty($freeshipping) && !empty($chargeshipping)) {


				 // only chargeble shipping product in cart


				 $total_shipping_charge = $total_weight * 30;


			}else{


				//mix product in cart


				$total_shipping_charge = $total_weight * 30;


			}			


		}else{


			if(!empty($freeshipping) && empty($chargeshipping)) {


				 // only free shipping product in cart


				 $total_shipping_charge = 50;


			}else if(empty($freeshipping) && !empty($chargeshipping)) {


				 // only chargeble shipping product in cart


				 $total_shipping_charge = ($total_weight * 30)+50;


			}else{


				//mix product in cart


				$total_shipping_charge = ($total_weight * 30)+50;


			}


		}  */


		$total_weight = $total_weight;


		$this->load->library('session');


		$this->session->set_userdata(array('total_weight' => $total_weight));


		return $total_weight;


	}


	


	private function _get_shipping_methods(){


		$shipping_methods	= array();

		/* do we need shipping? */

		if(config_item('require_shipping'))
		{

			/* do the cart contents require shipping? */

			if($this->natuur->requires_shipping())
			{

				/* ok so lets grab some shipping methods. If none exists, then we know that shipping isn't going to happen! */


				foreach ($this->Settings_model->get_settings('shipping_modules') as $shipping_method=>$order)
				{

					$this->load->add_package_path(APPPATH.'packages/shipping/'.$shipping_method.'/');

					/* eventually, we will sort by order, but I'm not concerned with that at the moment */

					$this->load->library($shipping_method);

					$shipping_methods	= array_merge($shipping_methods, $this->$shipping_method->rates());

				}



				/*  Free shipping coupon applied ? */


				if($this->natuur->is_free_shipping()) 
				{

					/*  add free shipping as an option, but leave other options in case they want to upgrade */

					$shipping_methods['Free Shipping (basic)'] = "0.00";

				}

				/*  format the values for currency display */


				foreach($shipping_methods as &$method)
				{

					/*  convert numeric values into an array containing numeric & formatted values */

					$method = array('num'=>$method,'str'=>$method);

				}


			}

		}


		if(!empty($shipping_methods))
		{

			/* everything says that shipping is required! */

			return $shipping_methods;

		}
		else
		{
			return false;
		}


	}





	function step_3()
	{
		/*


		Some error checking


		see if we have the billing address


		*/


		$customer	= $this->natuur->customer();

		if(empty($customer['bill_address']))
		{

			redirect('checkout/step_1');

		}


		/* see if shipping is required and set. */


		if(config_item('require_shipping') && $this->natuur->requires_shipping() && $this->_get_shipping_methods())
		{

			$code	= $this->validate_shipping_option($this->natuur->shipping_code());

			if(!$code)
			{
				redirect('checkout/step_2');

			}


		}


		if($payment_methods = $this->_get_payment_methods())
		{

			$this->payment_form($payment_methods);

		}
		/* now where? continue to step 4 */
		else
		{

			$this->step_4();

		}

	}

	protected function payment_form($payment_methods)
	{

		/* find out if we need to display the shipping */

		$data['customer']			= $this->natuur->customer();

		$data['shipping_method']	= $this->natuur->shipping_method();

		/* are the being bounced back? */

		$data['payment_method']		= $this->natuur->payment_method();

		/* pass in the payment methods */

		$data['payment_methods']	= $payment_methods;

		/* require that a payment method is selected */

		$this->form_validation->set_rules('module', 'Payment Method', 'trim|required|xss_clean|callback_check_payment');

		$module = $this->input->post('module');

		if($module)

		{
			$this->load->add_package_path(APPPATH.'packages/payment/'.$module.'/');

			$this->load->library($module);

		}

		if($this->form_validation->run() == false)
		{

			$this->load->view('checkout/payment_form', $data);

		}
		else
		{
			$this->natuur->set_payment( $module, $this->$module->description() );

			redirect('checkout/step_4');

		}

	}


	/* callback that lets the payment method return an error if invalid */


	function check_payment($module)
	{

		$check	= $this->$module->checkout_check();

		if(!$check)
		{
			return true;
		}
		else
		{

	    	$this->form_validation->set_message('check_payment', $check);

			return false;

		}

	}


	private function _get_payment_methods(){


		$payment_methods	= array();

		if($this->natuur->total() != 0){

			foreach ($this->Settings_model->get_settings('payment_modules') as $payment_method=>$order){


				if(!empty($payment_form)){


					$payment_methods[$payment_method] = $payment_form;


				}


			}


		}



		if(!empty($payment_methods)){

			return $payment_methods;

		}else{

			return false;

		}


	}


	function step_4(){

		/* get addresses */

		$data['customer']		= $this->natuur->customer();

		$data['shipping_method']	= $this->natuur->shipping_method();

		$data['payment_method']		= $this->natuur->payment_method();

		/* Confirm the sale */

		$this->load->view('checkout/confirm', $data);

	}


	function login()
	{
		$this->Customer_model->is_logged_in('checkout');
	}


	function register(){

		$this->Customer_model->is_logged_in('checkout', 'secure/register');

	}


	
	function place_order(){
 
		$this->session->unset_userdata('shipping_price');

		// retrieve the payment method
		$payment 			= $this->natuur->payment_method();				
		$payment_methods	= $this->_get_payment_methods();


		//make sure they're logged in if the config file requires it
		if($this->config->item('require_login')){

			$this->Customer_model->is_logged_in();

		}

		// are we processing an empty cart?
		$contents = $this->natuur->contents();

		if(empty($contents)){

			redirect('cart/view_cart');

		}else{

			//  - check to see if we have a payment method set, if we need one
			if(empty($payment) && $this->natuur->total() > 0 && (bool)$payment_methods == true){

				redirect('cart/view_cart');

			}

		}


		$shipping_cost = $this->session->flashdata('shipping_cost');
		$amt = $this->session->userdata('redim_amt');
        // save the order
		$order_id = $this->natuur->save_order();
		//get full row of order

		$order_data                 = $this -> Product_model -> order_data($order_id);
		$data['order_id']			= $order_id;
		$data['order_number']		= $order_data->order_number;
		$data['order_date']			= $order_data->format_data;
		$data['user_notes']			= $order_data->shipping_notes;
		$data['shipping']			= $this->natuur->shipping_method();
		$data['payment']			= $this->natuur->payment_method();
		$data['customer']			= $this->natuur->customer();
		$data['shipping_notes']		= $this->natuur->get_additional_detail('shipping_notes');
		$data['hide_menu']			= true;
		
		// run the complete payment module method once order has been saved
		if(!empty($payment)){
            if(!$payment['module'])
			{
				$payment['module']='PayU_Money';
				$data['payment']['module']='Icici';
			}
		}


        $data['natuur']['subtotal']            = $this->natuur->subtotal();
		$data['natuur']['coupon_discount']     = $this->natuur->coupon_discount();
		$data['natuur']['order_tax']           = $this->natuur->order_tax();
		$data['natuur']['discounted_subtotal'] = $this->natuur->discounted_subtotal();
		$data['natuur']['shipping_cost']       = $order_data->shipping;
		//$data['natuur']['gift_card_discount']  = $this->natuur->gift_card_discount();
        $data['natuur']['total']               = $this->natuur->total() + $shipping_cost; 
		$data['natuur']['contents']            = $this->natuur->contents();

		if($data['payment']['module'] == "PayU_Money"){
			$this->load->view('payu_form', $data);
		}elseif($data['payment']['module'] == "Icici"){
			$this->load->view('payment/icici/icici_form', $data);
		}else{

			//payment mode cash on delivery
			// - get the email template

			$this->load->model('messages_model');
			$row = $this->messages_model->get_message(7);
			$row['content'] = html_entity_decode($row['content']);
			// set replacement values for subject & body
			// {customer_name}

			$row['subject'] = str_replace('{url}', $this->config->item('base_url'), $row['subject'])." (#Order : ".$data['order_number'].")";

			$row['content'] = str_replace('{customer_name}', $data['customer']['firstname'].' '.$data['customer']['lastname'], $row['content']);

			// {url}

			$row['subject'] = str_replace('{url}', $this->config->item('base_url'), $row['subject']);
			$row['content'] = str_replace('{url}', $this->config->item('base_url'), $row['content']);

			// {site_name}
			$row['subject'] = str_replace('{site_name}', 'Natuur', $row['subject']);
			$row['content'] = str_replace('{site_name}', 'Natuur', $row['content']);


			// {order_summary}
			$row['content'] = str_replace('{order_summary}', $this->load->view('invoice_summary', $data, true), $row['content']);
			$this->load->library('email');
			$config['mailtype'] = 'html';
			$this->email->initialize($config);
			$this->email->from('care@natuur.com', 'Natuur');
			
			if($this->Customer_model->is_logged_in(false, false)){

				$this->email->to($data['customer']['email']);

			}else{

				$this->email->to($data['customer']['ship_address']['email']);

			}


			//email the admin

			$this->email->bcc('mukesh.itsoft@gmail.com');
			$this->email->subject($row['subject']);
			$this->email->message($row['content']);
			$this->email->send();
			$data['page_title'] = 'Thanks for shopping with '.$this->config->item('company_name');

			$this->natuur->destroy();

			/*  show final confirmation page */
			$this->load->view('invoice', $data);
		}	
	}


	function payu_response(){

		$post = $this->input->post();		

		if($post['status'] == "success"){

			$order_data  = $this->db->get_where('orders', array('order_number'=>$post['txnid']))->row();		

			$date =  $order_data -> ordered_on;

			$date = strtotime($date);

			$order_data->format_data = date("F d, Y", $date);

			$data['natuur']['subtotal']            = $this->natuur->subtotal();

			$data['natuur']['coupon_discount']     = $this->natuur->coupon_discount();

			$data['natuur']['order_tax']           = $this->natuur->order_tax();


			$data['natuur']['discounted_subtotal'] = $this->natuur->discounted_subtotal();


			$data['natuur']['shipping_cost']       = $order_data->shipping;


			//$data['natuur']['gift_card_discount']  = $this->natuur->gift_card_discount();


			$data['natuur']['reward_amt']          = $amt;


			$data['natuur']['total']               = $this->natuur->total();


			$data['natuur']['contents']            = $this->natuur->contents();$data['natuur']['shipping_cost']        = $this->session->userdata('shippingCost');

			$data['order_id']			= $order_data->order_number;


			$data['order_number']		= $order_data->order_number;


			$data['order_date']			= $order_data->format_data;


			$data['user_notes']			= $order_data -> shipping_notes;


			$data['shipping']			= $this->natuur->shipping_method();


			$data['payment']			= $this->natuur->payment_method();


			$data['customer']			= $this->natuur->customer();


			$data['shipping_notes']		= $this->natuur->get_additional_detail('shipping_notes');


			$data['hide_menu']			= true;


			$payu_response = array('status' => $post['status'],'mihpayid' => $post['mihpayid'], 'mode' => $post['mode'], 'addedon' => $post['addedon'], 'encryptedPaymentId' => $post['encryptedPaymentId'], 'bank_ref_num' => $post['bank_ref_num'], 'name_on_card' => $post['name_on_card'], 'cardnum' => $post['cardnum'], 'payuMoneyId' => $post['payuMoneyId'], 'net_amount_debit' => $post['net_amount_debit']);



			$update = array();

			$update['payment_status'] = $post['status'];

			$update['status']         = 'Order Placed';
			$update['shipping']         = $this->session->userdata('shippingCost');
			


			$update['payment_credentials'] = serialize($payu_response);


			$this->db->where('order_number', $post['txnid']);


			$this->db->update('orders', $update);





			$data->page_title = 'Thanks for shopping with '.$this->config->item('company_name');


			


			// - get the email template


			$this->load->model('messages_model');


			$row = $this->messages_model->get_message(7);


			


			$row['content'] = html_entity_decode($row['content']);





			// set replacement values for subject & body


			// {customer_name}


			$row['subject'] = str_replace('{url}', $this->config->item('base_url'), $row['subject'])." (#Order : ".$data['order_id'].")";


			$row['content'] = str_replace('{customer_name}', $data['customer']['firstname'].' '.$data['customer']['lastname'], $row['content']);





			// {url}


			$row['subject'] = str_replace('{url}', $this->config->item('base_url'), $row['subject']);


			$row['content'] = str_replace('{url}', $this->config->item('base_url'), $row['content']);





			// {site_name}


			$row['subject'] = str_replace('{site_name}', 'Natuur', $row['subject']);


			$row['content'] = str_replace('{site_name}', 'Natuur', $row['content']);





			// {order_summary}


			$row['content'] = str_replace('{order_summary}', $this->load->view('invoice_summary', $data, true), $row['content']);


			$this->load->library('email');


			$config['mailtype'] = 'html';


			$this->email->initialize($config);


			$this->email->from('care@natuur.com', 'Natuur');


            $this->email->bcc('mukesh.itsoft@gmail.com');


			if($this->Customer_model->is_logged_in(false, false)){


				$this->email->to($data['customer']['email']);


			}else{


				$this->email->to($data['customer']['ship_address']['email']);


			}


			


			//email the admin


			


			$this->email->subject($row['subject']);


			$this->email->message($row['content']);


			$this->email->send();





			$this->natuur->destroy();


			/* show final confirmation page */


			$this->load->view('invoice', $data);


		}else{


			$payu_response = array('status' => $post['status'],'mihpayid' => $post['mihpayid'], 'unmappedstatus' => $post['unmappedstatus'], 'addedon' => $post['addedon'], 'bank_ref_num' => $post['bank_ref_num'], 'payuMoneyId' => $post['payuMoneyId'], 'amount' => $post['amount']);


			


			$update = array();


			$update['payment_status'] = $post['status'];


			$update['payment_credentials'] = serialize($payu_response);


			$this->db->where('order_number', $post['txnid']);


			$this->db->update('orders', $update);


			redirect('home/');


		}


	}

	
	function icici_response(){
		include APPPATH . 'third_party/icici/layer_api.php';
		include APPPATH . 'third_party/icici/common.php';
		//Change below accesskey, secretkey to test 
$accesskey = 'e77d0dd0-f32b-11ea-af57-6bd6a40b1a92';
$secretkey = 'be288e83b4c926433d527beaf1372a50f983c25c';
		//$post = $this->input->post();
		
		$error = "";
$status = "";

if(!isset($_POST['layer_payment_id']) || empty($_POST['layer_payment_id'])){
	$error = "Invalid response.";    
}
try {
    $data = array(
        'layer_pay_token_id'    => $_POST['layer_pay_token_id'],
        'layer_order_amount'    => $_POST['layer_order_amount'],
        'tranid'     			=> $_POST['tranid'],
    );

    if(empty($error) && verify_hash($data,$_POST['hash'],$accesskey,$secretkey) && !empty($data['tranid'])){
        $layer_api = new LayerApi($environment,$accesskey,$secretkey);
        $payment_data = $layer_api->get_payment_details($_POST['layer_payment_id']);


        if(isset($payment_data['error'])){
            $error = "Layer: an error occurred E14".$payment_data['error'];
        }

        if(empty($error) && isset($payment_data['id']) && !empty($payment_data)){
            if($payment_data['payment_token']['id'] != $data['layer_pay_token_id']){
                $error = "Layer: received layer_pay_token_id and collected layer_pay_token_id doesnt match";
            }
            elseif($data['layer_order_amount'] != $payment_data['amount']){
                $error = "Layer: received amount and collected amount doesnt match";
            }
            else {
                switch ($payment_data['status']){
                    case 'authorized':
                    case 'captured':
                        $status = "Payment captured: Payment ID ". $payment_data['id'];
                        break;
                    case 'failed':								    
                    case 'cancelled':
                        $status = "Payment cancelled/failed: Payment ID ". $payment_data['id'];                        
                        break;
                    default:
                        $status = "Payment pending: Payment ID ". $payment_data['id'];
                        exit;
                    break;
                }
            }
        } else {
            $error = "invalid payment data received E98";
        }
    } else {
        $error = "hash validation failed";
    }

} catch (Throwable $exception){

   $error =  "Layer: an error occurred " . $exception->getMessage();
    
}
		
//if(!empty($error))
    //echo $error;
        //else
    //echo $status;		
	//die;	
		if(empty($error))
		{
			
			$post['ORDERID']=$payment_data['payment_token']['mtx'];
			
			$order_data  = $this->db->get_where('orders', array('order_number'=>$post['ORDERID']))->row();		
			$date =  $order_data -> ordered_on;
			$date = strtotime($date);
			$order_data->format_data = date("F d, Y", $date);
					
			$data['natuur']['subtotal']            = $this->natuur->subtotal();
			$data['natuur']['coupon_discount']     = $this->natuur->coupon_discount();
			$data['natuur']['order_tax']           = $this->natuur->order_tax();
			$data['natuur']['discounted_subtotal'] = $this->natuur->discounted_subtotal();
			$data['natuur']['shipping_cost']       = $order_data->shipping;
			
			$data['natuur']['total']               = $this->natuur->total();
			$data['natuur']['contents']            = $this->natuur->contents();
		
		
			$data['order_id']			= $order_data->order_number;
			$data['order_number']		= $order_data->order_number;
			$data['order_date']			= $order_data->format_data;
			$data['user_notes']			= $order_data -> shipping_notes;
			$data['shipping']			= $this->natuur->shipping_method();
			$data['payment']			= $this->natuur->payment_method();
			$data['customer']			= $this->natuur->customer();
			$data['shipping_notes']		= $this->natuur->get_additional_detail('shipping_notes');
			$data['hide_menu']			= true;
		
			
			$icici_response = $payment_data;
			
			$update = array();
			$update['payment_status'] = $payment_data['status'];
			$update['payment_credentials'] = serialize($icici_response);
			$this->db->where('order_number', $post['ORDERID']);
			$this->db->update('orders', $update);

			$data->page_title = 'Thanks for shopping with '.$this->config->item('company_name');
			
			// - get the email template
			$this->load->model('messages_model');
			$row = $this->messages_model->get_message(7);
			
			$row['content'] = html_entity_decode($row['content']);

			// set replacement values for subject & body
			// {customer_name}
			$row['subject'] = str_replace('{url}', $this->config->item('base_url'), $row['subject'])." (#Order : ".$data['order_id'].")";
			$row['content'] = str_replace('{customer_name}', $data['customer']['firstname'].' '.$data['customer']['lastname'], $row['content']);

			// {url}
			$row['subject'] = str_replace('{url}', $this->config->item('base_url'), $row['subject']);
			$row['content'] = str_replace('{url}', $this->config->item('base_url'), $row['content']);

			// {site_name}
			$row['subject'] = str_replace('{site_name}', 'natuur', $row['subject']);
			$row['content'] = str_replace('{site_name}', 'natuur', $row['content']);

			// {order_summary}
			$row['content'] = str_replace('{order_summary}', $this->load->view('invoice_summary', $data, true), $row['content']);
			$this->load->library('email');
			$config['mailtype'] = 'html';
			$this->email->initialize($config);
			$this->email->from('support@natuur.com', 'natuur');

			if($this->Customer_model->is_logged_in(false, false)){
				$this->email->to($data['customer']['email']);
			}else{
				$this->email->to($data['customer']['ship_address']['email']);
			}
			
			//email the admin
			
			$this->email->subject($row['subject']);
			$this->email->message($row['content']);
			$this->email->send();

			$this->natuur->destroy();
			/* show final confirmation page */
			$this->load->view('invoice', $data);
		}else{
			$this->natuur->destroy();
			$post['ORDERID']=$post['ORDERID'];
			$icici_response = $payment_data;
			$update = array();
			$update['payment_status']      = $payment_data['status'];
			$update['payment_credentials'] = serialize($icici_response);
			$this->db->where('order_number', $post['ORDERID']);
			$this->db->update('orders', $update);
			redirect('home/');
		}
	}



}
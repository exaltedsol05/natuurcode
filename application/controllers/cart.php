<?php
class Cart extends CI_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->model('Category_model');
		$this->load->model('Option_model');
		$this->load->model('Search_model');
		$this->load->model('Product_model');
		$this->load->model('Page_model');
		
		//load natuur library
		$this->load->library('Banners');
		$this->load->library('Breadcrumbs');
		$this->load->library('Menus');
		$this->load->model(array('location_model'));
		$this->load->model('Customer_model');
		
		$this->load->library('natuur');
		
		//load helpers
		$this->load->helper(array('formatting_helper'));
		// $this->load->helper(array('MY_date_helper'));
		//$this->load->helper('MY_url_helper');
		$this->customer = $this->natuur->customer();
    }
	
	function index()
	{
		
		//$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		$data['homepage']			= true;
		
		$this->view('homepage', $data);
		
	}
    
	function page($id = false)
	{
		$this->session->unset_userdata('shipping_price');
		//if there is no page id provided redirect to the homepage.
		$data['page']	= $this->Page_model->get_page($id);
		if(!$data['page'])
		{
			show_404();
		}
		$this->load->model('Page_model');
		$data['base_url']			= $this->uri->segment_array();
		
		$data['fb_like']			= true;

		$data['page_title']			= $data['page']->title;
		
		$data['meta']				= $data['page']->meta;
		$data['seo_title']			= (!empty($data['page']->seo_title))?$data['page']->seo_title:$data['page']->title;
		
		//$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		
		$this->load->view('page', $data);
	}
	
	
	function search($code=false, $page = 0)
	{
		$data['taxtype'] = rtrim($_GET['taxtype'],',');
		$taxtype         = explode('-', $data['taxtype']);
		$minTax          = $taxtype[0];
		$maxTax          = $taxtype[1];
		
		$data['pricetype'] 		= rtrim($_GET['pricetype'],',');
		$pricetype         		= explode('-', $data['pricetype']);
		$data['minPrice']  		= $minPrice = $pricetype[0];
		$data['maxPrice']  		= $maxPrice = $pricetype[1];
		//check to see if we have a search term
		if(!$code)
		{
			//if the term is in post, save it to the db and give me a reference
			$term		= $this->input->post('term', true);
			$code		= $this->Search_model->record_term($term);
			
			// no code? redirect so we can have the code in place for the sorting.
			// I know this isn't the best way...
			redirect('cart/search/'.$code.'/'.$page);
		}
		else
		{
			//if we have the md5 string, get the term
			$term	= $this->Search_model->get_term($code);
		}
		
		if(empty($term))
		{
			//if there is still no search term throw an error
			$this->session->set_flashdata('error', 'You did not supply a search term');
			redirect('cart');
		}

		$data['page_title']			= 'Search';
		//$data['gift_cards_enabled']	= $this->gift_cards_enabled;
		
		//fix for the category view page.
		$data['base_url']			= array();
		
		$sort_array = array(
							'name/asc' => array('by' => 'products.name', 'sort'=>'ASC'),
							'name/desc' => array('by' => 'products.name', 'sort'=>'DESC'),
							'price/asc' => array('by' => 'sort_price', 'sort'=>'ASC'),
							'price/desc' => array('by' => 'sort_price', 'sort'=>'DESC'),
							);
		$sort_by	= array('by'=>'products.id', 'sort'=>'DESC');
	
		if(isset($_GET['by']))
		{
			if(isset($sort_array[$_GET['by']]))
			{
				$sort_by	= $sort_array[$_GET['by']];
			}
		}
		
			$data['page_title']	= 'Search';
			//$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		
			//set up pagination
			$this->load->library('pagination');
			$config['base_url']		= base_url().'cart/search/'.$code.'/';
			$config['uri_segment']	= 4;
			$config['per_page']		= 12;
			
			$config['first_link'] = 'First';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_link'] = 'Last';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';

			$config['full_tag_open'] = '<ul class="pagination">';
			$config['full_tag_close'] = '</ul>';
			$config['cur_tag_open'] = '<li class="current"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';

			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';

			$config['prev_link'] = '&laquo;';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';

			$config['next_link'] = '&raquo;';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			
			$result	= $this->Product_model->search_products($term, $config['per_page'], $page, $sort_by['by'], $sort_by['sort'],$minTax,$maxTax,$minPrice,$maxPrice);

			$config['total_rows']	= $result['count'];
			$this->pagination->initialize($config);
			$data['products'] = $result['products'];
			
			$data['categories'] = $this->Category_model->get_categories_tiered(true);
			
			foreach ($data['products'] as &$p)
			{
			    
				   $p->images	= (array)json_decode($p->images);
				   $p->options	= $this->Option_model->get_product_options($p->id);
				
			}
			$this->load->view('category', $data);
	}
	
	function bestsellers($id=null)
	{ 
		$id = '106';
		$data['page'] = 'category';
		$data['brands']    = $brand    = rtrim($_GET['brand'],',');

		$data['taxtype'] = rtrim($_GET['taxtype'],',');
		$taxtype = explode('-', $data['taxtype']);
		$minTax = $taxtype[0];
		$maxTax = $taxtype[1];

		$data['pricetype'] = rtrim($_GET['pricetype'],',');
		$pricetype = explode('-', $data['pricetype']);
		$data['minPrice'] = $minPrice = $pricetype[0];
		$data['maxPrice'] = $maxPrice = $pricetype[1];
		
		
		
		//get the category
		/*$data['category'] = $this->Category_model->get_category($id);
		
		$data['categories'] = $this->Category_model->get_categories_tiered(true);	
		if (!$data['category'] || $data['category']->enabled==0)
		{
			show_404();
		}*/
				
		$product_count = $this->Product_model->count_bestseller_products();
		//echo $product_count;
		//set up pagination
		$segments	= $this->uri->total_segments();
		$base_url	= $this->uri->segment_array();
		
		if('best-sellers' == $base_url[count($base_url)])
		{
			$page	= 0;
			$segments++;
		}
		else
		{
			$page	= array_splice($base_url, -1, 1);
			$page	= $page[0];
		}
		
		$data['base_url']	= $base_url;
		$base_url			= implode('/', $base_url);
		
		$data['product_columns']	= $this->config->item('product_columns');
		//$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		
		//$data['meta']		= 'Best sellers';
		$data['seo_title']	= 'Best sellers';
		//$data['page_title']	= 'Best sellers';
		
		$sort_array = array(
							'name/asc' => array('by' => 'products.name', 'sort'=>'ASC'),
							'name/desc' => array('by' => 'products.name', 'sort'=>'DESC'),
							'price/asc' => array('by' => 'sort_price', 'sort'=>'ASC'),
							'price/desc' => array('by' => 'sort_price', 'sort'=>'DESC'),
							);
		$sort_by	= array('by'=>'products.id', 'sort'=>'DESC');
		
		if(isset($_GET['by']))
		{
			if(isset($sort_array[$_GET['by']]))
			{
				$sort_by	= $sort_array[$_GET['by']];
			}
		}
		
		//set up pagination
		$this->load->library('pagination');
		$config['base_url']		= site_url($base_url);
		
		$config['uri_segment']	= $segments;
		$config['per_page']		= 12;
		$config['total_rows']	= $product_count;
		
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['full_tag_open'] = '<ul class="list-inline list-unstyled">';
		$config['full_tag_close'] = '</ul>';
		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';

		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
				
		$this->pagination->initialize($config);
		
		$category_ids = array(127,106,128);
		$data['products']	= $this->Product_model->get_bestseller_products($category_ids, $config['per_page'], $page, $sort_by['by'], $sort_by['sort'],$new_tax,$minPrice,$maxPrice);
		foreach ($data['products'] as &$p)
		{
			
			$p->images	= (array)json_decode($p->images);
			$p->options	= $this->Option_model->get_product_options($p->id);
		}
		
		$this->load->view('vwBestsellers', $data);
	}
	
	
	function category($id)
	{ 
		$data['page'] = 'category';
	
		$data['brands']    = $brand    = rtrim($_GET['brand'],',');

		$data['taxtype'] = rtrim($_GET['taxtype'],',');
		$taxtype = explode('-', $data['taxtype']);
		$minTax = $taxtype[0];
		$maxTax = $taxtype[1];

		$data['pricetype'] = rtrim($_GET['pricetype'],',');
		$pricetype = explode('-', $data['pricetype']);
		$data['minPrice'] = $minPrice = $pricetype[0];
		$data['maxPrice'] = $maxPrice = $pricetype[1];
		
		
		
		//get the category
		$data['category'] = $this->Category_model->get_category($id);
		
		$data['categories'] = $this->Category_model->get_categories_tiered(true);	
		if (!$data['category'] || $data['category']->enabled==0)
		{
			show_404();
		}
				
		$product_count = $this->Product_model->count_products($data['category']->id);
		
		//set up pagination
		$segments	= $this->uri->total_segments();
		$base_url	= $this->uri->segment_array();
		
		if($data['category']->slug == $base_url[count($base_url)])
		{
			$page	= 0;
			$segments++;
		}
		else
		{
			$page	= array_splice($base_url, -1, 1);
			$page	= $page[0];
		}
		
		$data['base_url']	= $base_url;
		$base_url			= implode('/', $base_url);
		
		$data['product_columns']	= $this->config->item('product_columns');
		//$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		
		$data['meta']		= $data['category']->meta;
		$data['seo_title']	= (!empty($data['category']->seo_title))?$data['category']->seo_title:$data['category']->name;
		$data['page_title']	= $data['category']->name;
		
		$sort_array = array(
							'name/asc' => array('by' => 'products.name', 'sort'=>'ASC'),
							'name/desc' => array('by' => 'products.name', 'sort'=>'DESC'),
							'price/asc' => array('by' => 'sort_price', 'sort'=>'ASC'),
							'price/desc' => array('by' => 'sort_price', 'sort'=>'DESC'),
							);
		$sort_by	= array('by'=>'products.id', 'sort'=>'DESC');
	
		if(isset($_GET['by']))
		{
			if(isset($sort_array[$_GET['by']]))
			{
				$sort_by	= $sort_array[$_GET['by']];
			}
		}
		
		//set up pagination
		$this->load->library('pagination');
		$config['base_url']		= site_url($base_url);
		
		$config['uri_segment']	= $segments;
		$config['per_page']		= 12;
		$config['total_rows']	= $product_count;
		
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['full_tag_open'] = '<ul class="list-inline list-unstyled">';
		$config['full_tag_close'] = '</ul>';
		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';

		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
				
		$this->pagination->initialize($config);
		

		$data['products']	= $this->Product_model->get_products($data['category']->id, $config['per_page'], $page, $sort_by['by'], $sort_by['sort'],$new_tax,$minPrice,$maxPrice);
		foreach ($data['products'] as &$p)
		{
			
			$p->images	= (array)json_decode($p->images);
			$p->options	= $this->Option_model->get_product_options($p->id);
		}
		
		
		$this->load->view('category', $data);
	}
	
	function product($id)
	{
		
		$data['customer']			= (array)$this->Customer_model->get_customer($this->customer['id']);
		
		$data['customers']			= $this->Customer_model->get_customers();
		
		//get hot deals product
		$data['hotproducts'] = $this->Product_model->get_hot_deals_products();
		// $this->db->query("TRUNCATE TABLE ask_question");
		
		$data['arrQuestion'] = $this->db->query("SELECT * FROM ask_question")->result();
		// echo "<pre>";print_r($data['arrQuestion']); echo "</pre>";
		foreach ($data['hotproducts'] as &$p)
		{
			$p->images	= (array)json_decode($p->images);
			$p->options	= $this->Option_model->get_product_options($p->id);
		}
		
		//get the product
		$data['product']	= $this->Product_model->get_product($id);
		
		if(!$data['product'] || $data['product']->enabled==0)
		{
			show_404();
		}
		
		$data['base_url']			= $this->uri->segment_array();
		
		$data['options']				= $this->Option_model->get_product_options($data['product']->id);
		
		
		
		$related			= $data['product']->related_products;
		$grouped			= $data['product']->grouped_products;
		
		$data['related']	= $related;
		$data['grouped']	= array();
        //$data['attech_combo_product']	    = $c;
				
		$data['posted_options']	= $this->session->flashdata('option_values');
		$data['page_title']			= $data['product']->name;
		$data['meta']				= $data['product']->meta;
		$data['seo_title']			= (!empty($data['product']->seo_title))?$data['product']->seo_title:$data['product']->name;
			
		if($data['product']->images == 'false')
		{
			$data['product']->images = array();
		}
		else
		{
			$data['product']->images	= array_values((array)json_decode($data['product']->images));
		}

		//$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		//set rate n review
		$this->load->model('ratenreview_model');
		$star = $this->input->get('star');
		if(empty($star)){
			$star = '';
		}

        //recent product
		$product_recent = $this->ratenreview_model->get_recent_product();
        $data['product_recent'] = 	$product_recent;

        //rate and review
		$product_review_recent = $this->ratenreview_model->get_product_approved_reviews_recent($id, $star);
		$product_review_helpful = $this->ratenreview_model->get_product_approved_reviews_helpful($id, $star);
		$product_review_count = $this->ratenreview_model->count_product_reviews($id, $star);
		$data['total_review'] = $product_review_count;
		
		$data['product_review_recent'] 	= 	$product_review_recent;
		
		$data['product_review_helpful'] = 	$product_review_helpful;
		//end set rate n review

		//echo "<pre>";print_r($data['related']);exit;		
		$this->load->view('product', $data);
	}
	
	
	function add_to_cart()
	{
		
		
		// Get our inputs
		
		$product_id		= $this->input->post('id');
		$quantity 		= $this->input->post('quantity');
		$post_options 	= $this->input->post('option');
		
		// Get a cart-ready product array
		$product = $this->Product_model->get_cart_ready_product($product_id, $quantity);
		
		//if out of stock purchase is disabled, check to make sure there is inventory to support the cart.
		if(!$this->config->item('allow_os_purchase') && (bool)$product['track_stock'])
		{
			$stock	= $this->Product_model->get_product($product_id);
			
			//loop through the products in the cart and make sure we don't have this in there already. If we do get those quantities as well
			$items		= $this->natuur->contents();
			$qty_count	= $quantity;
			foreach($items as $item)
			{
				if(intval($item['id']) == intval($product_id))
				{
					$qty_count = $qty_count + $item['quantity'];
				}
			}
			
			if($stock->quantity < $qty_count)
			{
				//we don't have this much in stock
				$this->session->set_flashdata('error', sprintf('The quantity requested for the "%s" product could not be added. We only have %d item(s) in stock.', $stock->name, $stock->quantity));
				$this->session->set_flashdata('quantity', $quantity);
				$this->session->set_flashdata('option_values', $post_options);

				redirect($this->Product_model->get_slug($product_id));
			}
		}

		// Validate Options 
		// this returns a status array, with product item array automatically modified and options added
		//  Warning: this method receives the product by reference
		$status = $this->Option_model->validate_product_options($product, $post_options);
		
		// don't add the product if we are missing required option values
		if( ! $status['validated'])
		{
			$this->session->set_flashdata('quantity', $quantity);
			$this->session->set_flashdata('error', $status['message']);
			$this->session->set_flashdata('option_values', $post_options);
		
			redirect($this->Product_model->get_slug($product_id));
		
		} else {
		
			//Add the original option vars to the array so we can edit it later
			$product['post_options']	= $post_options;
			
			//is giftcard
			$product['is_gc']			= false;
			
			// Add the product item to the cart, also updates coupon discounts automatically
			$this->natuur->insert($product);
		
			// go go gadget cart!
			redirect('cart/view_cart');
		}
	}
	
	
	function add_to_cart_free_product($product){
		
		//$arrIds       = $post['id'];
		//$arrQuantitys = $post['quantity'];
		if(!empty($product['buy_get_products'])){
			$buyconditions = json_decode($product['buy_get_products']);
			$pids = $buyconditions->p_id;
			$buys = $buyconditions->buy;
			$gets = $buyconditions->get;
			$froms = $buyconditions->from;
			$tos = $buyconditions->to;
			$total = sizeof($pids);
			$conditionpid = array();
			
			for($i = 0; $i <= $total-1; $i++){
				$conditionpid[$gets[$i]] = array(
					'free_product' 	=> $pids[$i],
					'free_quantity' => $gets[$i],
					'buy_quantity' 	=> $buys[$i],
					'from' 			=> $froms[$i],
					'to' 			=> $tos[$i]
				);
			}
			
			$carts = array();
			$i = 1;
			
			foreach ($this->natuur->contents() as $cartkey=>$cartproduct){
				
				$cartid = $cartproduct['id'];
				$cartq = $cartproduct['quantity'];
				$temp = array("id" => $cartproduct['id'], "quantity" => $cartq);
				$carts[$cartid] = $temp;
			}
			
			ksort($conditionpid);
			$current_cart_quantity = $carts[$product['id']]['quantity'];
			
			$needkey = 0;
			foreach($conditionpid as $key => $checkrage){
				if($current_cart_quantity >= $key){
					$needkey = $key;
				}else{
					break;
				}
			}
			
			$cdate = time();
			$fromd = strtotime($conditionpid[$needkey]['from']);
			$endd  = strtotime($conditionpid[$needkey]['to']);
			$free_quantity = $conditionpid[$needkey]['buy_quantity'];
			
			if($cdate > $fromd && $endd > $cdate){
				// start here -->
				$product = $this->Product_model->get_cart_ready_product($conditionpid[$needkey]['free_product'], $free_quantity);
				$product['saleprice'] = 0;
				$product['quantity'] = $free_quantity;
				
				if(!$this->config->item('allow_os_purchase') && (bool)$product['track_stock']){
					$stock	= $this->Product_model->get_product($product_id);
					$items		= $this->natuur->contents();
					$qty_count	= $quantity;
					foreach($items as $item){
						if(intval($item['id']) == intval($product_id)){
							$qty_count = $qty_count + $item['quantity'];
						}
					}
					
					if($stock->quantity < $qty_count){
						$this->session->set_flashdata('error', sprintf('The quantity requested for the "%s" product could not be added. We only have %d item(s) in stock.', $stock->name, $stock->quantity));
						$this->session->set_flashdata('quantity', $quantity);
						redirect($this->Product_model->get_slug($product_id));
					}
				}
				
				// Add the product item to the cart, also updates coupon discounts automatically
				$this->natuur->insert($product);
			}
		}
	}	
	
	
	function add_to_cart_ajax(){
		$arrIds       = $this->input->post('id');
		$arrQuantitys = $this->input->post('quantity');
		
		if(is_array($arrIds)){
			// Get our inputs
			$i=0;
		    foreach($arrIds as $ids){
				$product_id		= $ids;
				$quantity 		= 1;
				//$post_options 	= $this->input->post('option');
		
				// Get a cart-ready product array
				$product = $this->Product_model->get_cart_ready_product($product_id, $quantity);
				//$salesprice = $this -> natuur -> product_price($product);
				//$product['price'] = $salesprice;
				//$product['saleprice'] = $salesprice;
				//if out of stock purchase is disabled, check to make sure there is inventory to support the cart.
				if(!$this->config->item('allow_os_purchase') && (bool)$product['track_stock']){
					$stock	= $this->Product_model->get_product($product_id);
					
					//loop through the products in the cart and make sure we don't have this in there already. If we do get those quantities as well
					$items		= $this->natuur->contents();
					$qty_count	= $quantity;
					foreach($items as $item){
						if(intval($item['id']) == intval($product_id)){
							$qty_count = $qty_count + $item['quantity'];
						}
					}
					
					if($stock->quantity < $qty_count){
						//we don't have this much in stock
						$this->session->set_flashdata('error', sprintf('The quantity requested for the "%s" product could not be added. We only have %d item(s) in stock.', $stock->name, $stock->quantity));
						$this->session->set_flashdata('quantity', $quantity);
						//$this->session->set_flashdata('option_values', $post_options);

						redirect($this->Product_model->get_slug($product_id));
					}
				}
				
				// Add the product item to the cart, also updates coupon discounts automatically
				$this->natuur->insert($product);
					
				$i++;
			}
			
		    die(json_encode(array('message'=>'This item was successfully added to your cart!')));
		}else{			
			// Get our inputs
			$product_id		= $this->input->post('id');
			$quantity 		= $this->input->post('quantity');
			//$post_options 	= $this->input->post('option');
	
			// Get a cart-ready product array
			$product = $this->Product_model->get_cart_ready_product($product_id, $quantity);
			//$salesprice = $this -> natuur -> product_price($product);
			//$product['price'] = $salesprice;
			//$product['saleprice'] = $salesprice;
				
			//if out of stock purchase is disabled, check to make sure there is inventory to support the cart.
			if(!$this->config->item('allow_os_purchase') && (bool)$product['track_stock']){
				$stock	= $this->Product_model->get_product($product_id);
				
				//loop through the products in the cart and make sure we don't have this in there already. If we do get those quantities as well
				$items		= $this->natuur->contents();
				$qty_count	= $quantity;
				foreach($items as $item){
					if(intval($item['id']) == intval($product_id)){
						$qty_count = $qty_count + $item['quantity'];
					}
				}
				
				if($stock->quantity < $qty_count){
					$this->session->set_flashdata('error', sprintf('The quantity requested for the "%s" product could not be added. We only have %d item(s) in stock.', $stock->name, $stock->quantity));
					$this->session->set_flashdata('quantity', $quantity);
					$this->session->set_flashdata('option_values', $post_options);
					$msg = sprintf('The quantity requested for the "%s" product could not be added. We only have %d item(s) in stock.', $stock->name, $stock->quantity);
					die(json_encode(array('error'=>$msg)));
				}
			}
			
			$this->natuur->insert($product);
			
			die(json_encode(array('message'=>'This item was successfully added to your cart!')));
		} 
	}
	
	
	function view_cart()
	{
		$this->session->unset_userdata('shipping_price');
		$data['page_title']	= 'View Cart';
		//$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		$this->load->view('view_cart', $data);
	}
	
	function remove_item($key)
	{
		//drop quantity to 0
		$this->natuur->update_cart(array($key=>0));
		redirect('cart/view_cart');
	}
	
	function checkzip(){
		$zip =$this->input->post('zipcode');
		$zip_output	= $this->Page_model->get_zip_status($zip);
		$zip_data = array('zipcode' => $zip);
		
		if($zip_output){
			$temp_value = $zip_output->isship;
			if($temp_value == 'No Delivery Available'){
				$zip_data['color'] 	= 'red';
				$zip_data['message']	= $zip_output->isship;
				$zip_data['estimate_time'] = "";
			}else{
				$zip_data['color'] = 'green';
				$zip_data['message']	= $zip_output->isship;
				$zip_data['estimate_time'] = $zip_output->daystodeliver;
			}
		}else{
			$zip_data['color'] 	= 'red';
			$zip_data['message']	= 'Please enter valid zip code';
			$zip_data['estimate_time'] = "";
		}
		
		echo json_encode($zip_data);
		exit();
	}	
	function checkpin(){
		$pincode = $this->input->post('pin');
		$total_weight = $this->input->post('total_weight');
			
				if(!empty($pincode)){
					  $code = $this->Page_model->get_zip_ship_amount($pincode);
				
						if($code == '0'){
							echo 0 ;
						}else{

							    $x = 35; //rs for delhi ncr
                                // system will add 10 % on this for packing & wrapping
							    $total_weight_with_packing = $total_weight+ ceil((float)($total_weight/10));

							    //1 grm shipping charge = (35/500) for NCR;
							    $shipping_price = $total_weight_with_packing*(0.07);
   
								if($code == 'A'){
									if($total_weight >0 && $total_weight <=50){
										$shipping_price = 15;
									}
									if($total_weight >50 && $total_weight <=200){
										$shipping_price = 25;
									}
									if($total_weight >200 && $total_weight <=500){
										$shipping_price = 30;
									}
									if($total_weight >500){
										
										$tempMod = ceil((float)($total_weight / 500));
										$shipping_price = 30 + ($tempMod-1)*10;	
									}
								}
								if($code == 'B'){
									if($total_weight >0 && $total_weight <=50){
										$shipping_price = 35;
									}
									if($total_weight >50 && $total_weight <=200){
										$shipping_price = 35;
									}
									if($total_weight >200 && $total_weight <=500){
										$shipping_price = 50;	
									}
									if($total_weight >500){
										
										$tempMod = ceil((float)($total_weight / 500));
										$shipping_price = 50 + ($tempMod-1)*15;	
									}
								}
								if($code == 'C'){
									if($total_weight >0 && $total_weight <=50){
										$shipping_price = 35;
									}
									if($total_weight >50 && $total_weight <=200){
										$shipping_price = 40;
									}
									if($total_weight >200 && $total_weight <=500){
										$shipping_price = 60;	
									}
									if($total_weight >500){
										
										$tempMod = ceil((float)($total_weight / 500));
										$shipping_price = 60 + ($tempMod-1)*30;	
									}
								}if($code == 'D'){
									if($total_weight >0 && $total_weight <=50){
										$shipping_price = 35;	
									}
									if($total_weight >50 && $total_weight <=200){
										$shipping_price = 60;	
									}
									if($total_weight >200 && $total_weight <=500){
										$shipping_price = 80;	
									}
									if($total_weight >500){
											
										$tempMod = ceil((float)($total_weight / 500));
										$shipping_price = 80 + ($tempMod-1)*40;	
									}
								}if($code == 'E'){
									if($total_weight >0 && $total_weight <=50){
										$shipping_price = 35;	
									}
									if($total_weight >50 && $total_weight <=200){
										$shipping_price = 70;
									}
									if($total_weight >200 && $total_weight <=500){
										$shipping_price = 90;	
									}
									if($total_weight >500){
										
										$tempMod = ceil((float)($total_weight / 500));
										$shipping_price = 90 + ($tempMod-1)*50;	
									}
								}
								$this->load->library('session');
								$this->session->set_userdata('shipping_price',$shipping_price);
								echo $shipping_price;	
					} //end else
				}
	}
	function update_cart($redirect = false)
	{
		//if redirect isn't provided in the URL check for it in a form field
		if(!$redirect)
		{
			$redirect = $this->input->post('redirect');
		}
		
		// see if we have an update for the cart
		$item_keys		= $this->input->post('cartkey');
		$coupon_code	= $this->input->post('coupon_code');
		$gc_code		= $this->input->post('gc_code');
		
		if($coupon_code)
		{
			$coupon_code = strtolower($coupon_code);
		}
			
		//get the items in the cart and test their quantities
		$items			= $this->natuur->contents();
		$new_key_list	= array();
		//first find out if we're deleting any products
		foreach($item_keys as $key=>$quantity)
		{
			if(intval($quantity) === 0)
			{
				//this item is being removed we can remove it before processing quantities.
				//this will ensure that any items out of order will not throw errors based on the incorrect values of another item in the cart
				$this->natuur->update_cart(array($key=>$quantity));
			}
			else
			{
				//create a new list of relevant items
				$new_key_list[$key]	= $quantity;
			}
		}
		$response	= array();
		foreach($new_key_list as $key=>$quantity)
		{
			$product	= $this->natuur->item($key);
			//if out of stock purchase is disabled, check to make sure there is inventory to support the cart.
			if(!$this->config->item('allow_os_purchase') && (bool)$product['track_stock'])
			{
				$stock	= $this->Product_model->get_product($product['id']);
			
				//loop through the new quantities and tabluate any products with the same product id
				$qty_count	= $quantity;
				foreach($new_key_list as $item_key=>$item_quantity)
				{
					if($key != $item_key)
					{
						$item	= $this->natuur->item($item_key);
						//look for other instances of the same product (this can occur if they have different options) and tabulate the total quantity
						if($item['id'] == $stock->id)
						{
							$qty_count = $qty_count + $item_quantity;
						}
					}
				}
				if($stock->quantity < $qty_count)
				{
					if(isset($response['error']))
					{
						$response['error'] .= '<p>'.sprintf('The quantity requested for the "%s" product could not be added. We only have %d item(s) in stock.', $stock->name, $stock->quantity).'</p>';
					}
					else
					{
						$response['error'] = '<p>'.sprintf('The quantity requested for the "%s" product could not be added. We only have %d item(s) in stock.', $stock->name, $stock->quantity).'</p>';
					}
				}
				else
				{
					//this one works, we can update it!
					//don't update the coupons yet
					$this->natuur->update_cart(array($key=>$quantity));
				}
			}
			else
			{
				$this->natuur->update_cart(array($key=>$quantity));
			}
		}
		
		//if we don't have a quantity error, run the update
		if(!isset($response['error']))
		{
			//update the coupons and gift card code
			$response = $this->natuur->update_cart(false, $coupon_code, $gc_code);
			// set any messages that need to be displayed
			$response['message'] = '<p>Successfully Updated your cart!</p>';
		}
		else
		{
			$response['error'] = '<p>There was an error updating the cart!</p>'.$response['error'];
		}
		
		
		//check for errors again, there could have been a new error from the update cart function
		if(isset($response['error']))
		{
			$this->session->set_flashdata('error', $response['error']);
		}
		
		if(isset($response['message']))
		{
			
			$this->session->set_flashdata('message', $response['message']);
		}
		
		if($redirect)
		{
			redirect($redirect);
		}
		else
		{
			redirect('cart/view_cart');
		}
	}

	
	//wishlist functions
	function add_wishlist(){
		$product_id = $this->input->post('product_id');
		
		if(!$this->Customer_model->is_logged_in(false,false)){
			echo "logout";
		}else{
			//if user logged in
			echo $total_records = $this -> Product_model -> add_wishlist_table($product_id);
		}
	}
	
	function wishlist(){
		$wish_ids = $this -> Product_model -> get_user_wishlist();
		$wish_product_ids = '0,';
		
		foreach($wish_ids as $ids){
			$wish_product_ids .= $ids -> product_id.",";
		}
		$wish_product_ids = rtrim($wish_product_ids, ',');
		$data['product_records'] = $this -> Product_model -> get_wish_product_row($wish_product_ids);
		$this->load->view('wishlist', $data);
	}
	
	function delete_wishlist($id){
		$this -> Product_model -> delete_from_wishlist($id);
		redirect('cart/wishlist');
	}
	
	//compare functions
	function add_compare(){
		$product_id = $this->input->post('product_id');
		if(!$this->Customer_model->is_logged_in(false,false)){
			echo "logout";
		}else{
			//if user logged in
			echo $total_records = $this -> Product_model -> add_compare_table($product_id);
		}
	}
	
	function compare(){
		$comp_ids = $this -> Product_model -> get_user_compare();
		$comp_product_ids = '0,';
		
		foreach($comp_ids as $ids){
			$comp_product_ids .= $ids -> product_id.",";
		}
		$comp_product_ids = rtrim($comp_product_ids, ',');
		$data['product_records'] = $this -> Product_model -> get_comp_product_row($comp_product_ids);
		$this->load->view('compare', $data);
	}
	
	function delete_compr($id){
		$this -> Product_model -> delete_from_compr($id);
		redirect('ratenreview_controller/compare');
	}
	
	//end june 23 work
	function product_search_by_ajax(){
		$post = $this->input->post();
		$result	= $this->Product_model->search_products_by_ajax($post);
		echo json_encode($result);
	}
	function mini_cart(){
		$this->load->view('mini_cart');
	}
	function ajax_remove_item($key){
		$this->session->set_userdata('redim_amt', 0);
		//$this->session->unset_userdata('redim_amt');
		$tempkey = $key;
		//drop quantity to 0
		$this->natuur->update_cart(array($key=>0));
		echo $tempkey;
	}
	function ajax_remove_cart_item($key){
		if($this->natuur->update_cart(array($key=>0))){
			return true;
		}else{
			return false;
		}
	}
	
	//for feature product click from home page
	function feature($code=false, $page = 0){
		$data['page_title'] = 'Feature';
		$data['page'] = 'feature';
		
		$data['top_brand'] = true;
		//fix for the category view page.
		$data['base_url']			= array();
		$sort_array = array(
							'name/asc' => array('by' => 'products.name', 'sort'=>'ASC'),
							'name/desc' => array('by' => 'products.name', 'sort'=>'DESC'),
							'price/asc' => array('by' => 'sort_price', 'sort'=>'ASC'),
							'price/desc' => array('by' => 'sort_price', 'sort'=>'DESC'),
							);
		$sort_by	= array('by'=>'products.id', 'sort'=>'DESC');
	
		if(isset($_GET['by'])){
			if(isset($sort_array[$_GET['by']])){
				$sort_by	= $sort_array[$_GET['by']];
			}
		}
		
		
		//set up pagination
		$this->load->library('pagination');
		
		$config['base_url']		= base_url().'cart/feature/';
		$config['uri_segment']	= 5;
		$config['per_page']		= 36;
		
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['cur_tag_open'] = '<li class="current"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';

		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$catid = '';
		if($_GET['search_cat']){
			$catid = $_GET['search_cat'];
		}
				
		$result	= $this->Product_model->feture_products_from_top($config['per_page'], $page, $sort_by['by'], $sort_by['sort'],$new_tax,$minPrice,$maxPrice);
				
		$config['total_rows']	= $result['count'];
		$this->pagination->initialize($config);
		$data['products'] = $result['products'];
		
		foreach ($data['products'] as &$p){
			   $p->images	= (array)json_decode($p->images);
			   $p->options	= $this->Option_model->get_product_options($p->id);				
		}
		$this->load->view('category', $data);	
	}
	
	//for new product click from home page
	function newproduct($code=false, $page = 0){
		$data['page_title'] = 'New';
		$data['page'] = 'new';
		
		$data['top_brand'] = true;
		//fix for the category view page.
		$data['base_url']			= array();
		$sort_array = array(
							'name/asc' => array('by' => 'products.name', 'sort'=>'ASC'),
							'name/desc' => array('by' => 'products.name', 'sort'=>'DESC'),
							'price/asc' => array('by' => 'sort_price', 'sort'=>'ASC'),
							'price/desc' => array('by' => 'sort_price', 'sort'=>'DESC'),
							);
		$sort_by	= array('by'=>'products.id', 'sort'=>'DESC');
	
		if(isset($_GET['by'])){
			if(isset($sort_array[$_GET['by']])){
				$sort_by	= $sort_array[$_GET['by']];
			}
		}
		
		
		//set up pagination
		$this->load->library('pagination');
		
		$config['base_url']		= base_url().'cart/newproduct/';
		$config['uri_segment']	= 5;
		$config['per_page']		= 36;
		
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['cur_tag_open'] = '<li class="current"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';

		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$catid = '';
		if($_GET['search_cat']){
			$catid = $_GET['search_cat'];
		}
				
		$result	= $this->Product_model->new_products_from_top($config['per_page'], $page, $sort_by['by'], $sort_by['sort'],$new_tax,$minPrice,$maxPrice);
				
		$config['total_rows']	= $result['count'];
		$this->pagination->initialize($config);
		$data['products'] = $result['products'];
		
		foreach ($data['products'] as &$p){
			   $p->images	= (array)json_decode($p->images);
			   $p->options	= $this->Option_model->get_product_options($p->id);				
		}
		$this->load->view('category', $data);	
	}	
	
	function ask_question(){
		date_default_timezone_set("Asia/Kolkata");
		$data = array();
		//echo $this->customer['id'];
		$data['name'] = $this->input->post('nickname');
		$data['email'] = $this->input->post('email');
		$data['question'] = $this->input->post('question');
		$data['product_id'] = $this->input->post('pid');
		$data['user_id'] = $this->customer['id'];
		$data['create_date'] = date('Y-m-d H:i:s');
		
		 // print_r($data); die;
		$this->session->set_flashdata('message', 'Your query succssfully save');
		echo $insert_id = $this->Product_model->save_ask_question($data);
		die;
		
	}

	function contact_us_request(){
		date_default_timezone_set("Asia/Kolkata");
		$user_name  = strip_tags($this->input->post('exampleInputName'));
		$user_email = strip_tags($this->input->post('exampleInputEmail'));
		$comments   = strip_tags($this->input->post('exampleInputComments'));

        $message  = '';
		$message .= "Name: ".strip_tags($user_name).", ";
		$message .= "Email: ".strip_tags($user_email).", ";
		$message .= "Message: ".strip_tags($comments);
		
		$headers  = "From: " . $user_email . "\r\n";
		$headers .= "Reply-To: ". $user_email . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		$email_unique_id =  rand(1000, 99999);
        
        $to = 'management@natuur.in';
		$subject = "Natuur user Feedback";

		if(mail($to, $subject, $message, $headers ))
		{
			echo 1;
		}else{
			echo 0;
		}

		die;
	}

	
}
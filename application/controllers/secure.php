<?php
use Razorpay\Api\Api;
class Secure extends CI_Controller {
	
	var $customer;
	
	function __construct()
	{
		
		parent::__construct();
		
		$this->load->model(array('location_model'));
		$this->load->model('Customer_model');
		$this->load->model('Category_model');
		$this->load->model('ratenreview_model');
		
		$this->load->library('natuur');
		//$this->load->library('Session');
		$this->load->library('Auth');
		$this->load->library('Menus');
		$this->load->helper(array('url', 'file', 'string', 'html'));
		//load helpers
		$this->load->helper(array('formatting_helper'));
		$this->customer = $this->natuur->customer();
		
		$this->load->model('Settings_model');
		//load in config items from the database
		$settings = $this->Settings_model->get_settings('naturalmama');
		
		
		foreach($settings as $key=>$setting)
		{
			//special for the order status settings
			if($key == 'order_statuses')
			{
				$setting = json_decode($setting, true);
			}
			$this->config->set_item($key, $setting);
		}

		
        
        //if SSL is enabled in config force it here.
        if (config_item('ssl_support') && (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == 'off'))
		{
			$CI =& get_instance();
			$CI->config->config['base_url'] = str_replace('http://', 'https://', $CI->config->config['base_url']);
			redirect($CI->uri->uri_string());
		}
		
		
		
		
	}
	
	function index()
	{
		show_404();
		
	}
	
	public function socialLogin($provider)
	{
		try
		{
			$this->load->library('HybridAuthLib');
			if ($this->hybridauthlib->providerEnabled($provider))
			{
				$service = $this->hybridauthlib->authenticate($provider);
				if ($service->isUserConnected())
				{
					$user_profile = $service->getUserProfile();
					//echo "<pre>";print_r($user_profile);exit;
					if($user_profile->email == '' && $user_profile->phone=='')
					{
						echo "<script>alert('For social signup, please make your phone or email public from your social account.');
							setTimeout(function(){ 
								window.location.href='/naturalmama';
							}, 1000);
						</script>";
					}
					else
					{
						//set social image in session after login--available in all pages as logged in user image
						$this->session->set_userdata(array('social_login_image'=>$user_profile->photoURL));
						//load customer model
						$this->load->model('Customer_model');
					
						$customer['id']='';
						$customer['auth_type']		= $this->uri->segment('3');
						$customer['auth_id'] 		= $user_profile->identifier;
						
						$name = explode(' ', $user_profile->displayName);
						$customer['firstname'] 		= $name['0'];
						$customer['lastname'] 		= $name['1'];
						$customer['email'] 			= $user_profile->email;
						$customer['phone'] 			= $user_profile->phone;
						$customer['active'] 		= 1;
						
						if(!empty($user_profile->email))
						{
							//echo "server email hai";exit;
							$query1 = "SELECT * FROM customers WHERE email = '".$user_profile->email."'";
							$val1   = $this->db->query($query1);
							$data1 = $val1->row_array();
							if($data1)
							{	
								//echo "db email hai";exit;
								$customer['id'] = $data1['id'];
								//print_r($customer);exit;
								$customer_id = $this->Customer_model->save($customer);
								if($customer_id)
								{
									//echo "update karne k baad server email se login hoga";exit;
									$login	= $this->Customer_model->socialLogin($customer['email'],$customer['phone']);
									if ($login)
									{	
										redirect('secure/my_account');
									}
								}
							}
							else
							{
								//echo "server email hai lekin db email nhi hai, pehle db me insert fir login hoga";exit;
								$customer_id = $this->Customer_model->save($customer);
								if($customer_id)
								{
									$login	= $this->Customer_model->socialLogin($customer['email'],$customer['phone']);
									if ($login)
									{	
										redirect('secure/my_account');
									}
								}
							}
						}
						else
						{
							//echo "server phone hai email nhi hai";exit;
							$query2 = "SELECT * FROM customers WHERE phone = '".$user_profile->phone."'";
							$val2   = $this->db->query($query2);
							$data2 = $val2->row_array();
							if($data2)
							{
								//echo "db phone hai";exit;
								$customer['id'] = $data2['id'];
								$customer_id = $this->Customer_model->save($customer);
								if($customer_id)
								{
									//echo "pehle db me update hoga fir login";exit;
									$login	= $this->Customer_model->socialLogin($customer['email'],$customer['phone']);
									if ($login)
									{	
										redirect('secure/my_account');
									}
								}
							}
							else
							{
								//echo "server phone hai lekin db phone nhi hai, pehle db me insert fir login hoga";exit;
								$customer_id = $this->Customer_model->save($customer);
								if($customer_id)
								{
									$login	= $this->Customer_model->socialLogin($customer['email'],$customer['phone']);
									if ($login)
									{	
										redirect('secure/my_account');
									}
								}
							}

						}
					}
				}
				else // Cannot authenticate user
				{
					show_error('Cannot authenticate user');
				}
			}
			else // This service is not enabled.
			{
				log_message('error', 'controllers.HAuth.login: This provider is not enabled ('.$provider.')');
				show_404($_SERVER['REQUEST_URI']);
			}
		}
		catch(Exception $e)
		{
			$error = 'Unexpected error';
			switch($e->getCode())
			{
				case 0 : $error = 'Unspecified error.'; break;
				case 1 : $error = 'Hybriauth configuration error.'; break;
				case 2 : $error = 'Provider not properly configured.'; break;
				case 3 : $error = 'Unknown or disabled provider.'; break;
				case 4 : $error = 'Missing provider application credentials.'; break;
				case 5 : log_message('debug', 'controllers.HAuth.login: Authentification failed. The user has canceled the authentication or the provider refused the connection.');
				         //redirect();
				         if (isset($service))
				         {
				         	log_message('debug', 'controllers.HAuth.login: logging out from service.');
				         	$service->logout();
				         }
				         show_error('User has cancelled the authentication or the provider refused the connection.');
				         break;
				case 6 : $error = 'User profile request failed. Most likely the user is not connected to the provider and he should to authenticate again.';
				         break;
				case 7 : $error = 'User not connected to the provider.';
				         break;
			}

			if (isset($service))
			{
				$service->logout();
			}

			log_message('error', 'controllers.HAuth.login: '.$error);
			show_error('Error authenticating user.');
		}
	}
	public function socialEndpoint()
	{

		log_message('debug', 'controllers.HAuth.endpoint called.');
		log_message('info', 'controllers.HAuth.endpoint: $_REQUEST: '.print_r($_REQUEST, TRUE));

		if ($_SERVER['REQUEST_METHOD'] === 'GET')
		{
			log_message('debug', 'controllers.HAuth.endpoint: the request method is GET, copying REQUEST array into GET array.');
			$_GET = $_REQUEST;
		}

		log_message('debug', 'controllers.HAuth.endpoint: loading the original HybridAuth endpoint script.');
		require_once APPPATH.'/third_party/hybridauth/index.php';

	}
	
	
	function ajax_login($ajax = false){	
		$usermail	= $this->input->post('usermail');
		$userpass	= $this->input->post('userpass');
		$ajax_login		= $this->Customer_model->ajax_login($usermail, $userpass);
		if ($ajax_login){
			echo 1;
		}else{
			echo 2;
		}
	}	
	function login($ajax = false){		

		//find out if they're already logged in, if they are redirect them to the my account page

		

		$redirect = $this->Customer_model->is_logged_in(false, false);
		//if they are logged in, we send them back to the my_account by default, if they are not logging in
		if ($redirect){	
			redirect('secure/my_account/');
		}

		

		$data['email']		    = '';
		$data['password']		= '';
		$data['page_title']	= 'Login';
		$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		$this->load->helper('form');

		$data['redirect']	= $this->session->flashdata('redirect');
		$submitted 		    = $this->input->post('submitted');
		if ($submitted){

			$email		= $this->input->post('email');
			$password	= $this->input->post('password');
			$remember   = $this->input->post('remember');
			$redirect	= $this->input->post('redirect');
			$login		= $this->Customer_model->login($email, $password, $remember);
			if ($login)
			{
				if ($redirect == '')
				{
					//if there is not a redirect link, send them to the my account page
					//$redirect = 'secure/my_account';
					$redirect = 'checkout';
				}

				//to login via ajax

				if($ajax)
				{
					die(json_encode(array('result'=>true)));
				}
				else
				{
					redirect($redirect);
				}
			}
			else
			{
				//this adds the redirect back to flash data if they provide an incorrect credentials

				if($ajax)
				{
					die(json_encode(array('result'=>false)));
				}
				else
				{
					$this->session->set_flashdata('redirect', $redirect);
					$this->session->set_flashdata('error', 'Email or Password incorrect.');
					redirect('secure/login');
				}
			}
		}
		show_404();
		//$this->load->view('login', $data);

	}
	function chk_user()
	{
		$userVal = $this->input->post('userVal');
		$textForm = $this->input->post('textForm');
		$userExists		= $this->Customer_model->chk_user($userVal);
		if($userExists){
			echo 1; //exists
		}else{
			// $otp = rand(100000,999999);
			$otp = '123456';
			$save['id']		= false;
			if($textForm=='mail'){
				$save['email']				= $userVal;
			}
			if($textForm=='phone'){
				$save['phone']				= $userVal;
			}
			$save['active']				= '1';
			$save['forgot_password_otp']				= $otp;

			$id = $this->Customer_model->save($save);
			if($textForm=='mail'){
				$this->load->library('email');
				$config['mailtype'] = 'html';		
				$this->email->initialize($config);

				$this->email->from('info@natuur.com', $this->config->item('company_name'));
				$this->email->to($userVal);
				$this->email->subject('Natuur');
				$this->email->message('Your otp is'.$otp);
				
				$this->email->send();
			}else{
				$this->load->library('email');
				$config['mailtype'] = 'html';		
				$this->email->initialize($config);

				$this->email->from('info@natuur.com', $this->config->item('company_name'));
				$this->email->to('rajeshbabai0001@gmail.com');
				$this->email->subject('Natuur');
				$this->email->message('Your otp is '.$otp);
				
				$this->email->send();
			}
			echo 2; //not exists
		}
	}
	function ajax_sent_otp()
	{
		$usermail = $this->input->post('usermail');
		$textForm = $this->input->post('textForm');
		$userExists		= $this->Customer_model->chk_user($usermail);
		if($userExists){
			// $otp = rand(100000,999999);
			$otp = '123456';
			$save['id']		= $userExists->id;
			if($textForm=='mail'){
				$save['email']				= $usermail;
			}
			if($textForm=='phone'){
				$save['phone']				= $usermail;
			}
			$save['active']				= '1';
			$save['forgot_password_otp']				= $otp;

			$id = $this->Customer_model->save($save);
			if($textForm=='mail'){
				$this->load->library('email');
				$config['mailtype'] = 'html';		
				$this->email->initialize($config);

				$this->email->from('info@natuur.com', $this->config->item('company_name'));
				$this->email->to($usermail);
				$this->email->subject('Natuur');
				$this->email->message('Your otp is'.$otp);
				
				$this->email->send();
			}else{
				$this->load->library('email');
				$config['mailtype'] = 'html';		
				$this->email->initialize($config);

				$this->email->from('info@natuur.com', $this->config->item('company_name'));
				$this->email->to($usermail);
				$this->email->subject('Natuur');
				$this->email->message('Your otp is'.$otp);
				
				$this->email->send();
			}
			echo 1; //exists
		}
	}
	function ajax_otp_login()
	{
		$usermail = $this->input->post('usermail');
		$userotp = $this->input->post('userotp');
		$userpw = $this->input->post('userpw');
		$textForm = $this->input->post('textForm');
		$userExists		= $this->Customer_model->chk_user_with_otp($usermail,$userotp);
		if($userExists){
			$save['id']		= $userExists->id;
			$save['password']			= sha1($userpw);
			$save['active']				= '1';
			$save['forgot_password_otp']				= '';

			$id = $this->Customer_model->save($save);
			$ajax_login		= $this->Customer_model->ajax_login($usermail, $userpw);
			echo 1; //exists
		}else{
			echo 2;
		}
	}
	
	function login1($ajax = false){
		
		//find out if they're already logged in, if they are redirect them to the my account page
		$redirect	= $this->Customer_model->is_logged_in(false, false);
		
		//if they are logged in, we send them back to the my_account by default, if they are not logging in
		if ($redirect){	
			redirect('secure/my_account/');
		}
		$data['page_title']	= 'Login';
		
		
		$this->load->helper('form');
		$data['redirect']	= $this->session->flashdata('redirect');
		$submitted 		= $this->input->post('submitted');
		
		if ($submitted){
			$email		= $this->input->post('email_id');
			$password	= $this->input->post('password');
			$remember   = $this->input->post('remember');
			$redirect	= $this->input->post('redirect');
			$login		= $this->Customer_model->login($email, $password, $remember);
			
			if ($login){
				$this->load->model('ratenreview_model');
				if ($redirect == ''){
					//if there is not a redirect link, send them to the my account page
					header('Content-Type: application/x-json; charset=utf-8');
					$data['status'] = true;
					echo json_encode($data);						
					die;
				}
				else
				{
					redirect($redirect);
				}				
			}else{
				
				header('Content-Type: application/x-json; charset=utf-8');
				$data['msg'] = '<div class="alert alert-danger">Email Id or Password incorrect.</div>';
				$data['status'] = false;
				echo json_encode($data);						
				die;
			}
			
		}
		
	}
	
	function logout()
	{
		$this->load->library('HybridAuthLib');
		$service = $this->hybridauthlib->logoutAllProviders();
		
		$this->Customer_model->logout();
		redirect('home');
	}

	function register1(){
		$redirect	= $this->Customer_model->is_logged_in(false, false);
		//if they are logged in, we send them back to the my_account by default
		if ($redirect){
			redirect('secure/my_account');
		}
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		/*
		we're going to set this up early.
		we can set a redirect on this, if a customer is checking out, they need an account.
		this will allow them to register and then complete their checkout, or it will allow them
		to register at anytime and by default, redirect them to the homepage.
		*/
		$data['redirect']	= $this->session->flashdata('redirect');		
		$data['page_title']	= 'Account Registration';
		$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		$data['countries_menu']	= $this->location_model->get_countries_menu();
		//default values are empty if the customer is new

		$data['salutation']				= '';
		$data['firstname']				= '';
		$data['lastname']				= '';
		$data['email']					= '';
		$data['email_subscribe']		= '';
		$data['dob']					= '';
		$data['phone']					= '';
		$data['company']				= '';
		$data['street_add1']			= '';
		$data['street_add2']			= '';
		$data['city']					= '';
		$data['state']					= '';
		$data['country']				= '';
		$data['pincode']				= '';
		$data['marrg_aniversary']		= '';
		$data['clinic_anniversary']		= '';
		$data['password']				= '';
		if($this->input->post('firstname')==0){
		   $firstname = '';	
		}
		
		if($this->input->post('lastname')==0){
		   $lastname = '';	
		}
		
		$save['id']		            = false;
		$save['salutation']			= $this->input->post('salutation');			
		$save['firstname']			= $firstname;
		$save['lastname']			= $lastname;
		$save['email']				= $this->input->post('reg_email');
		$save['email_subscribe']	= intval((bool)$this->input->post('email_subscribe'));
		$save['dob']				= $this->input->post('date').'-'.$this->input->post('month').'-'.$this->input->post('year');
		$save['phone']				= $this->input->post('reg_phone');
		$save['company']			= $this->input->post('company');
		$save['street_add1']		= $this->input->post('address1');		
		$save['street_add2']		= $this->input->post('address2');		
		$save['city']				= $this->input->post('city');		
		$save['state']				= $this->input->post('state');		
		$save['country']			= $this->input->post('country_id');		
		$save['pincode']			= $this->input->post('pin');		
		$save['marrg_aniversary']	= $this->input->post('mrg_date').'-'.$this->input->post('mrg_month').'-'.$this->input->post('mrg_year');		
		$save['clinic_anniversary']	= $this->input->post('clinic_date').'-'.$this->input->post('clinic_month').'-'.$this->input->post('clinic_year');
		$pass 						= $this->input->post('reg_password');
		$save['password']			= sha1($pass);
		$save['active']				= $this->config->item('new_customer_status');
		$redirect					= $this->input->post('redirect');
		
		//if we don't have a value for redirect
		if ($redirect == ''){
			$redirect = 'secure/my_account';
		}
			
		// save the customer info and get their new id
		$id = $this->Customer_model->save($save);

		// get the email template
		$res = $this->db->where('id', '6')->get('canned_messages');
		$row = $res->row_array();
		
		if($firstname=='')
		{
			// set replacement values for subject & body		
		$row['subject'] = str_replace('{customer_name}', 'sir/madam', $row['subject']);
		$row['content'] = str_replace('{customer_name}', 'sir/madam', $row['content']);
		}else{
			// set replacement values for subject & body		
		    $row['subject'] = str_replace('{customer_name}', $this->input->post('firstname').' '. $this->input->post('lastname'), $row['subject']);
		    $row['content'] = str_replace('{customer_name}', $this->input->post('firstname').' '. $this->input->post('lastname'), $row['content']);
		}
		
		
		// {url}
		$row['subject'] = str_replace('{url}', $this->config->item('base_url'), $row['subject']);
		$row['content'] = str_replace('{url}', $this->config->item('base_url'), $row['content']);
		
		// {site_name}
		$row['subject'] = str_replace('{site_name}', $this->config->item('company_name'), $row['subject']);
		$row['content'] = str_replace('{site_name}', $this->config->item('company_name'), $row['content']);
		
		$this->load->library('email');
		$config['mailtype'] = 'html';		
		$this->email->initialize($config);

		$this->email->from($this->config->item('email'), $this->config->item('company_name'));
		$this->email->to($save['email']);
		$this->email->bcc($this->config->item('email'));
		$this->email->subject($row['subject']);
		$this->email->message(html_entity_decode($row['content']));
		
		$this->email->send();
        if($firstname=='')
		{		
			$this->session->set_flashdata('message', sprintf( 'Thanks for registering %s!', 'sir/madam' ) );
		}else{
			$this->session->set_flashdata('message', sprintf( 'Thanks for registering %s!', $this->input->post('firstname') ) );
		}
			
		//lets automatically log them in
		
		$this->Customer_model->login($save['email'], $pass);	
		//we're just going to make this secure regardless, because we don't know if they are
		//wanting to redirect to an insecure location, if it needs to be secured then we can use the secure redirect in the controller
		//to redirect them, if there is no redirect, the it should redirect to the homepage.
		
		redirect($redirect);
	}
	
	
	 function register(){

		$data['page']='register';

		$redirect	= $this->Customer_model->is_logged_in(false, false);

		//if they are logged in, we send them back to the my_account by default

		if ($redirect)
		{
			redirect('secure/my_account');
		}

		

		$this->load->library('form_validation');

		$this->form_validation->set_error_delimiters('<div>', '</div>');

		

		/*

		we're going to set this up early.

		we can set a redirect on this, if a customer is checking out, they need an account.

		this will allow them to register and then complete their checkout, or it will allow them

		to register at anytime and by default, redirect them to the homepage.

		*/

		

		$data['redirect']	= $this->session->flashdata('redirect');

		

		$data['page_title']	= 'romanfox:Account - Register';

		//$data['gift_cards_enabled'] = $this->gift_cards_enabled;

		

		//default values are empty if the customer is new


		$data['firstname']	= '';

		$data['lastname']	= '';

		$data['email']		= '';

		$data['phone']		= '';

		



		$this->form_validation->set_rules('firstname', 'First Name', 'trim|required|max_length[32]');

		$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required|max_length[32]');

		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email|max_length[128]|callback_check_email');

		$this->form_validation->set_rules('phone', 'Mobile Number', 'trim|required|max_length[32]');

		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|sha1');

		$this->form_validation->set_rules('confirm', 'Confirm Password', 'required|matches[password]');

		$this->form_validation->set_rules('email_subscribe', 'Account Newsletter Subscribe', 'trim|numeric|max_length[1]');



		

		if ($this->form_validation->run() == FALSE)
		{

			//if they have submitted the form already and it has returned with errors, reset the redirect

			if ($this->input->post('submitted'))
			{
				$data['redirect']	= $this->input->post('redirect');				
			}

			

			// load other page content 

			//$this->load->model('banner_model');

			$this->load->helper('directory');
			if($this->input->post('register')){
				$data['error'] = validation_errors();
			}
			if(!empty($this->input->post('login_redirect'))){
				$data['redirect'] = $this->input->post('login_redirect');
			}
			show_404();
			//$this->load->view('register', $data);
		}
		else
		{
			$save['id']		= false;

			$save['firstname']			= $this->input->post('firstname');
			$save['lastname']			= $this->input->post('lastname');
			$save['email']				= $this->input->post('email');
			$save['phone']				= $this->input->post('phone');
			$save['active']				= '1';
			$save['email_subscribe']	= intval((bool)$this->input->post('email_subscribe'));
			$save['password']			= $this->input->post('password');
			$redirect					= $this->input->post('redirect');

			
			//if we don't have a value for redirect

			if ($redirect == '')
			{
				//$redirect = 'secure/my_account';
				$redirect = 'checkout';
			}

			

			// save the customer info and get their new id

			$id = $this->Customer_model->save($save);
			// set replacement values for subject & body

			$ip = $_SERVER['REMOTE_ADDR'];
			if($ip=='127.0.0.1' || $ip=='::1')
			{

			}else{

				/* send an email */

				// get the email template

				$res = $this->db->where('id', '6')->get('canned_messages');
				$row = $res->row_array();

				// {customer_name}

				$row['subject'] = str_replace('{customer_name}', $this->input->post('firstname').' '. $this->input->post('lastname'), $row['subject']);
				$row['content'] = str_replace('{customer_name}', $this->input->post('firstname').' '. $this->input->post('lastname'), $row['content']);

				

				// {url}

				$row['subject'] = str_replace('{url}', $this->config->item('base_url'), $row['subject']);
				$row['content'] = str_replace('{url}', $this->config->item('base_url'), $row['content']);

				// {site_name}

				$row['subject'] = str_replace('{site_name}', $this->config->item('company_name'), $row['subject']);
				$row['content'] = str_replace('{site_name}', $this->config->item('company_name'), $row['content']);
				$this->load->library('email');
				$config['mailtype'] = 'html';
				$this->email->initialize($config);
				$this->email->from($this->config->item('email'), $this->config->item('company_name'));
				$this->email->to($save['email']);
				$this->email->bcc($this->config->item('email'));
				$this->email->subject($row['subject']);
				$this->email->message(html_entity_decode($row['content']));

				

				$this->email->send();

			}

			$this->session->set_flashdata('message', sprintf('Thanks for registering %s', $this->input->post('firstname') ) );

			

			//lets automatically log them in

			$this->Customer_model->login($save['email'], $this->input->post('confirm'));

			

			//we're just going to make this secure regardless, because we don't know if they are

			//wanting to redirect to an insecure location, if it needs to be secured then we can use the secure redirect in the controller

			//to redirect them, if there is no redirect, the it should redirect to the homepage.

			

			redirect($redirect);

		}
		show_404();
	}

	function check_email($str)
	{
		if(!empty($this->customer['id']))
		{
			$email = $this->Customer_model->check_email($str, $this->customer['id']);
		}
		else
		{
			$email = $this->Customer_model->check_email($str);
		}
		
        if ($email)
       	{
			$this->form_validation->set_message('check_email', 'The requested email is already in use.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	function forgot_password()
	{
		$data['page_title']	= 'Forgot Password';
		//$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		$submitted = $this->input->post('submitted');
		if ($submitted)
		{
			$this->load->helper('string');
			$email = $this->input->post('email');
			
			$reset = $this->Customer_model->reset_password($email);
			
			if ($reset)
			{						
				$this->session->set_flashdata('message', 'A mail has been sent to your account, please click on link and create your new password.');
			}
			else
			{
				$this->session->set_flashdata('error', 'Please enter a valid email Id.');
			}
			redirect('secure/forgot_password');
		}
		
		// load other page content 
		//$this->load->model('banner_model');
		$this->load->helper('directory');
	
		//if they want to limit to the top 5 banners and use the enable/disable on dates, add true to the get_banners function
		//$data['banners']	= $this->banner_model->get_banners();
		//$data['ads']		= $this->banner_model->get_banners(true);
		$data['categories']	= $this->Category_model->get_categories_tiered();
		
		
		$this->load->view('forgot_password', $data);
	}
	
	function my_orders($offset=0)
	{
		$data['page'] = 'my_order';
		
		//make sure they're logged in
		$this->Customer_model->is_logged_in('secure/my_orders/');
		
		//$data['gift_cards_enabled']	= $this->gift_cards_enabled;
		
		$data['customer']			= (array)$this->Customer_model->get_customer($this->customer['id']);
			
		$data['addresses'] 			= $this->Customer_model->get_address_list($this->customer['id']);
		
		$data['page_title']			= 'Welcome '.$data['customer']['firstname'].' '.$data['customer']['lastname'];
		//$data['customer_addresses']	= $this->Customer_model->get_address_list($data['customer']['id']);
		
		// load other page content 
		//$this->load->model('banner_model');
		$this->load->model('order_model');
		$this->load->helper('directory');
		$this->load->helper('date');
		
		//if they want to limit to the top 5 banners and use the enable/disable on dates, add true to the get_banners function
	//	$data['banners']	= $this->banner_model->get_banners();
	//	$data['ads']		= $this->banner_model->get_banners(true);
		$data['categories']	= $this->Category_model->get_categories_tiered(0);
		
		
		// paginate the orders
		$this->load->library('pagination');

		$config['base_url'] = site_url('secure/my_account');
		$config['total_rows'] = $this->order_model->count_customer_orders($this->customer['id']);
		$config['per_page'] = '15'; 
	
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['full_tag_open'] = '<div class="pagination"><ul>';
		$config['full_tag_close'] = '</ul></div>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';

		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		
		$this->pagination->initialize($config); 
		
		$data['orders_pagination'] = $this->pagination->create_links();

		$data['orders']		= $this->order_model->get_customer_orders($this->customer['id'], $offset);
        
		
		//if they're logged in, then we have all their acct. info in the cookie.
		
		
		/*
		This is for the customers to be able to edit their account information
		*/

		/*$this->load->library('form_validation');
		$this->form_validation->set_rules('company', 'Company', 'trim|max_length[128]');
		$this->form_validation->set_rules('firstname', 'First Name', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[128]|callback_check_email');
		$this->form_validation->set_rules('phone', 'Phone', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('email_subscribe', 'Subscribe to our email list', 'trim|numeric|max_length[1]');
		
		if($this->input->post('password') != '' || $this->input->post('confirm') != '')
		{
			$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|sha1');
			$this->form_validation->set_rules('confirm', 'Confirm Password', 'required|matches[password]');
		}
		else
		{
			$this->form_validation->set_rules('password', 'Password');
			$this->form_validation->set_rules('confirm', 'Confirm Password');
		}


		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('my_account', $data);
		}
		else
		{
			$customer = array();
			$customer['id']						= $this->customer['id'];
			$customer['company']				= $this->input->post('company');
			$customer['firstname']				= $this->input->post('firstname');
			$customer['lastname']				= $this->input->post('lastname');
			$customer['email']					= $this->input->post('email');
			$customer['phone']					= $this->input->post('phone');
			$customer['email_subscribe']		= intval((bool)$this->input->post('email_subscribe'));
			if($this->input->post('password') != '')
			{
				$customer['password']			= $this->input->post('password');
			}
						
			$this->natuur->save_customer($this->customer);
			$this->Customer_model->save($customer);
			
			$this->session->set_flashdata('message', 'Your account has been updated');
			
			redirect('secure/my_account');
		}*/
	    $this->load->view('my_orders', $data);
	}
	
	
	
	
	function my_address($offset=0)
	{
		$data['page'] = 'my_add';
		
		//make sure they're logged in
		$this->Customer_model->is_logged_in('secure/my_address/');
		
		//$data['gift_cards_enabled']	= $this->gift_cards_enabled;
		
		$data['customer']			= (array)$this->Customer_model->get_customer($this->customer['id']);
			
		$data['addresses'] 			= $this->Customer_model->get_address_list($this->customer['id']);
		
		$data['page_title']			= 'Welcome '.$data['customer']['firstname'].' '.$data['customer']['lastname'];
		//$data['customer_addresses']	= $this->Customer_model->get_address_list($data['customer']['id']);
		
		// load other page content 
		//$this->load->model('banner_model');
		//$this->load->model('order_model');
		//$this->load->helper('directory');
		//$this->load->helper('date');
		
		//if they want to limit to the top 5 banners and use the enable/disable on dates, add true to the get_banners function
	//	$data['banners']	= $this->banner_model->get_banners();
	//	$data['ads']		= $this->banner_model->get_banners(true);
		$data['categories']	= $this->Category_model->get_categories_tiered(0);
		
		
		// paginate the orders
		/*$this->load->library('pagination');

		$config['base_url'] = site_url('secure/my_account');
		$config['total_rows'] = $this->order_model->count_customer_orders($this->customer['id']);
		$config['per_page'] = '15'; 
	
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['full_tag_open'] = '<div class="pagination"><ul>';
		$config['full_tag_close'] = '</ul></div>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';

		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		
		$this->pagination->initialize($config); 
		
		$data['orders_pagination'] = $this->pagination->create_links();

		$data['orders']		= $this->order_model->get_customer_orders($this->customer['id'], $offset);
        */
		
		//if they're logged in, then we have all their acct. info in the cookie.
		
		
		/*
		This is for the customers to be able to edit their account information
		*/

		/*$this->load->library('form_validation');
		$this->form_validation->set_rules('company', 'Company', 'trim|max_length[128]');
		$this->form_validation->set_rules('firstname', 'First Name', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[128]|callback_check_email');
		$this->form_validation->set_rules('phone', 'Phone', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('email_subscribe', 'Subscribe to our email list', 'trim|numeric|max_length[1]');
		
		if($this->input->post('password') != '' || $this->input->post('confirm') != '')
		{
			$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|sha1');
			$this->form_validation->set_rules('confirm', 'Confirm Password', 'required|matches[password]');
		}
		else
		{
			$this->form_validation->set_rules('password', 'Password');
			$this->form_validation->set_rules('confirm', 'Confirm Password');
		}


		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('my_account', $data);
		}
		else
		{
			$customer = array();
			$customer['id']						= $this->customer['id'];
			$customer['company']				= $this->input->post('company');
			$customer['firstname']				= $this->input->post('firstname');
			$customer['lastname']				= $this->input->post('lastname');
			$customer['email']					= $this->input->post('email');
			$customer['phone']					= $this->input->post('phone');
			$customer['email_subscribe']		= intval((bool)$this->input->post('email_subscribe'));
			if($this->input->post('password') != '')
			{
				$customer['password']			= $this->input->post('password');
			}
						
			$this->natuur->save_customer($this->customer);
			$this->Customer_model->save($customer);
			
			$this->session->set_flashdata('message', 'Your account has been updated');
			
			redirect('secure/my_account');
		}*/
	    $this->load->view('my_address', $data);
	}
	
	
	
	
	function my_account($offset=0)
	{
		$data['page'] = 'my_account';
		
		//make sure they're logged in
		$this->Customer_model->is_logged_in('secure/my_account/');
		
		//$data['gift_cards_enabled']	= $this->gift_cards_enabled;
		
		$data['customer']			= (array)$this->Customer_model->get_customer($this->customer['id']);
			
		$data['addresses'] 			= $this->Customer_model->get_address_list($this->customer['id']);
		
		$data['page_title']			= 'Welcome '.$data['customer']['firstname'].' '.$data['customer']['lastname'];
		$data['customer_addresses']	= $this->Customer_model->get_address_list($data['customer']['id']);
		
		// load other page content 
		//$this->load->model('banner_model');
		$this->load->model('order_model');
		$this->load->helper('directory');
		$this->load->helper('date');
		
		//if they want to limit to the top 5 banners and use the enable/disable on dates, add true to the get_banners function
	//	$data['banners']	= $this->banner_model->get_banners();
	//	$data['ads']		= $this->banner_model->get_banners(true);
		$data['categories']	= $this->Category_model->get_categories_tiered(0);
		
		
		// paginate the orders
		$this->load->library('pagination');

		$config['base_url'] = site_url('secure/my_account');
		$config['total_rows'] = $this->order_model->count_customer_orders($this->customer['id']);
		$config['per_page'] = '15'; 
	
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['full_tag_open'] = '<div class="pagination"><ul>';
		$config['full_tag_close'] = '</ul></div>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';

		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		
		$this->pagination->initialize($config); 
		
		$data['orders_pagination'] = $this->pagination->create_links();

		$data['orders']		= $this->order_model->get_customer_orders($this->customer['id'], $offset);

		
		//if they're logged in, then we have all their acct. info in the cookie.
		
		
		/*
		This is for the customers to be able to edit their account information
		*/

		$this->load->library('form_validation');
		$this->form_validation->set_rules('company', 'Company', 'trim|max_length[128]');
		$this->form_validation->set_rules('firstname', 'First Name', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[128]|callback_check_email');
		$this->form_validation->set_rules('phone', 'Phone', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('email_subscribe', 'Subscribe to our email list', 'trim|numeric|max_length[1]');
		
		if($this->input->post('password') != '' || $this->input->post('confirm') != '')
		{
			$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|sha1');
			$this->form_validation->set_rules('confirm', 'Confirm Password', 'required|matches[password]');
		}
		else
		{
			$this->form_validation->set_rules('password', 'Password');
			$this->form_validation->set_rules('confirm', 'Confirm Password');
		}


		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('my_account', $data);
		}
		else
		{
			$customer = array();
			$customer['id']						= $this->customer['id'];
			$customer['company']				= $this->input->post('company');
			$customer['firstname']				= $this->input->post('firstname');
			$customer['lastname']				= $this->input->post('lastname');
			$customer['email']					= $this->input->post('email');
			$customer['phone']					= $this->input->post('phone');
			$customer['email_subscribe']		= intval((bool)$this->input->post('email_subscribe'));
			if($this->input->post('password') != '')
			{
				$customer['password']			= $this->input->post('password');
			}
						
			$this->natuur->save_customer($this->customer);
			$this->Customer_model->save($customer);
			
			$this->session->set_flashdata('message', 'Your account has been updated');
			
			redirect('secure/my_account');
		}
	
	}
	
	
	function my_information()
	{
		$data['page'] = 'my_info';
		
		//make sure they're logged in
		$this->Customer_model->is_logged_in('secure/my_information/');
		
		
		
		$data['customer']			= (array)$this->Customer_model->get_customer($this->customer['id']);
			
		
		
		$data['page_title']			= 'Welcome '.$data['customer']['firstname'].' '.$data['customer']['lastname'];
		
		$data['categories']	= $this->Category_model->get_categories_tiered(0);
		
		//if they're logged in, then we have all their acct. info in the cookie.
		
		
		/*
		This is for the customers to be able to edit their account information
		*/

		$this->load->library('form_validation');
		//$this->form_validation->set_rules('company', 'Company', 'trim|max_length[128]');
		$this->form_validation->set_rules('firstname', 'First Name', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required|max_length[32]');
		//$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[128]|callback_check_email');
		//$this->form_validation->set_rules('phone', 'Phone', 'trim|required|max_length[32]');
		//$this->form_validation->set_rules('email_subscribe', 'Subscribe to our email list', 'trim|numeric|max_length[1]');
		
		if($this->input->post('password') != '' || $this->input->post('confirm') != '')
		{
			$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|sha1');
			$this->form_validation->set_rules('confirm', 'Confirm Password', 'required|matches[password]');
		}
		else
		{
			
			$this->form_validation->set_rules('password', 'Password');
			$this->form_validation->set_rules('confirm', 'Confirm Password');
		}


		if ($this->form_validation->run() == FALSE)
		{
			
			$this->load->view('my_information', $data);
			
		}
		else
		{ 
			$customer = array();
			$customer['id']						= $this->customer['id'];
			$customer['company']				= $this->input->post('company');
			$customer['firstname']				= $this->input->post('firstname');
			$customer['lastname']				= $this->input->post('lastname');
			$customer['dob']					= $this->input->post('dob');
			$customer['email']					= $this->input->post('email');
			$customer['phone']					= $this->input->post('phone');
			$customer['email_subscribe']		= intval((bool)$this->input->post('email_subscribe'));
			if($this->input->post('password') != '')
			{
				$customer['password']			= $this->input->post('password');
			}
						
			$this->natuur->save_customer($this->customer);
			$this->Customer_model->save($customer);
			
			$this->session->set_flashdata('message', 'Your account has been updated');
			//echo $this->session->flashdata('message');
			//exit;
			redirect('secure/my_information');
			
		}
	   
	}
	
	
	
	
	function set_default_address()
	{
		$id = $this->input->post('id');
		$type = $this->input->post('type');
	
		$customer = $this->natuur->customer();
		$save['id'] = $customer['id'];
		
		if($type=='bill')
		{
			$save['default_billing_address'] = $id;

			$customer['bill_address'] = $this->Customer_model->get_address($id);
			$customer['default_billing_address'] = $id;
		} else {

			$save['default_shipping_address'] = $id;

			$customer['ship_address'] = $this->Customer_model->get_address($id);
			$customer['default_shipping_address'] = $id;
		} 
		
		//update customer db record
		$this->Customer_model->save($save);
		
		//update customer session info
		$this->natuur->save_customer($customer);
		
		echo "1";
	}
	
	function partial($view, $vars = array(), $string=false)
	{
		
		if($string)
		{
			return $this->load->view($view, $vars, true);
		}
		else
		{
			$this->load->view($view, $vars);
		}
	}
	function select_ship_address($id = 0)
	{
		$customer = $this->natuur->customer();
		if($id != 0)
		{
			$a	= $this->Customer_model->get_address($id);
			if($a['customer_id'] == $this->customer['id'])
			{
				$this->load->model('Settings_model');
				$settings = $this->Settings_model->get_settings('naturalmama');
				//notice that this is replacing all of the data array
				//if anything beyond this form data needs to be added to
				//the data array, do so after this portion of code
				$chk_pin['pincode'] = $a['zipcode'];
				$get_available_pin	= $this->pin_code_serviceability($chk_pin);
				//$get_available_pin	= $this->location_model->get_available_pin($a['zipcode']);
				if($get_available_pin){
					$session_currency = $this->natuur->get_currncy();
					$products_weight = $this->natuur->products_weight()+($this->natuur->products_weight()*10/100);
					$get_shipping_fees = $this->get_shipping_fees($get_available_pin['city_category'],$products_weight);
					$data		= $a['field_data'];
					$data['id']	= $id;
					$data['cart_subtotal']	= round($this->natuur->subtotal()+$get_shipping_fees,2);
					$data['get_shipping_fees']	= round($get_shipping_fees,2);
					$data['currency_symbol']	= $session_currency['currency_symbol'];
				}else{
					$data['error']	= $settings['address_not_available_message'];
					$data['id']	= '';
				}
			} else {
				redirect('/'); // don't allow cross-customer editing
			}
		}
		echo json_encode($data);
	}
	function select_ship_address_cart()
	{
		$id = $this->input->post('address_checked');
		$customer = $this->natuur->customer();
		if($id != 0)
		{
			$a	= $this->Customer_model->get_address($id);
			if($a['customer_id'] == $this->customer['id'])
			{
				$this->load->model('Settings_model');
				$settings = $this->Settings_model->get_settings('naturalmama');
				//notice that this is replacing all of the data array
				//if anything beyond this form data needs to be added to
				//the data array, do so after this portion of code
				$chk_pin['pincode'] = $a['zipcode'];
				$get_available_pin	= $this->pin_code_serviceability($chk_pin);
				//$get_available_pin	= $this->location_model->get_available_pin($a['zipcode']);
				if($get_available_pin){
					$session_currency = $this->natuur->get_currncy();
					$products_weight = $this->natuur->products_weight()+($this->natuur->products_weight()*10/100);
					$get_shipping_fees = $this->get_shipping_fees($get_available_pin['city_category'],$products_weight);
					$data		= $a['field_data'];
					$data['id']	= $id;
					$data['cart_subtotal']	= round($this->natuur->subtotal()+$get_shipping_fees,2);
					$data['get_shipping_fees']	= round($get_shipping_fees,2);
					$data['currency_symbol']	= $session_currency['currency_symbol'];
				}else{
					$data['error']	= $settings['address_not_available_message'];
					$data['id']	= '';
				}
			} else {
				redirect('/'); // don't allow cross-customer editing
			}
		}
		echo json_encode($data);
	}
	function step_45(){
		$module = $this->input->post('payment_mode');
		if($module == "PayU_Money"){
			$des = "PayU Money";
		}elseif($module == "Icici"){
			$des = "Icici";
		}else{
			$des = "Cash On Delivery";
		}
		$data['razorpay_key'] = [
			'keyId'=> 'rzp_test_ivUcuQUoTZDjkt',
			'secretKey'=> 'kEUhGa1UmoFw3bGMUzWCnWEV',
		];

		$this->natuur->set_payment( $module, $des);
		$this->load->view('razorpay/Razorpay', $data);
	}
	public function pin_code_serviceability($chk_pin)
	{
		//$token = '11f30b579cf37b0eb84cab44ba80a335d134acd3';//test
		$token = '4d10012c6a5654ebc416c1a8fea646a953d784c8';//live
		$pin = $chk_pin['pincode'];
		//test
		//$url = 'https://staging-express.delhivery.com/c/api/pin-codes/json/?token='.$token.'&filter_codes='.$pin;
		//live
		$url = 'https://track.delhivery.com/c/api/pin-codes/json/?token='.$token.'&filter_codes='.$pin;
	    $method = 'GET';
	    $headers = array(
	        "content-type: application/json"
	    );
	
	    $curl = curl_init();
	
	    curl_setopt_array($curl, array(
	        CURLOPT_RETURNTRANSFER => true,
	        CURLOPT_URL => $url,
	        CURLOPT_CUSTOMREQUEST => $method,
	        CURLOPT_HTTPHEADER => $headers,
	    ));
	
	    $response = curl_exec($curl);
	    $err = curl_error($curl);
	
	    curl_close($curl);
	
	    if ($err) {
	    	echo "cURL Error #:" . $err;
	    } else {
	    	$data = json_decode($response,true);
			//echo '<pre>'; print_r($data); echo'</pre>';
			//echo $data['delivery_codes'][0]['postal_code']['district'];exit;
			if(!empty($data) && ($data['delivery_codes'][0]['postal_code']['pin'])!=''){
				$tier_one_cities = $this->config->item('tier_one_cities');
				//echo '<pre>'; print_r($tier_one_cities); echo'</pre>';
				if(array_key_exists($data['delivery_codes'][0]['postal_code']['state_code'],$tier_one_cities)){
					$return_data['city_category'] = 3;
				}else{
					$return_data['city_category'] = 4;
				}
				return $return_data;
			}else{
				return false;
			}
	    }
	}
	public function get_shipping_fees($city,$weight)
	{	
		$price = $this->natuur->subtotal();
		$maxWeight = 20000;
		
		$totPercel = $weight/$maxWeight;
		for($j=0; $j < $totPercel; $j++){
			if($j == 0){
				if($totPercel <= 1){
					$remainWeight = $totPercel*$maxWeight;
				}else{
					$remainWeight = $maxWeight;
				}
			}
			if($j > 0){
				$remainWeight = $weight-($j*$maxWeight);
			}
			if($city == 1 || $city == 2){
				if($price < 1000){
					if($remainWeight <= 500){
						$charge += 35; //upto 500gm
					}else if($remainWeight > 500 && $remainWeight <= 1000){
						$charge += 35+25; //501gm-1000gm
					}else if($remainWeight > 1000 && $remainWeight <= 1500){
						$charge += 35+25+15; //1001gm-1500gm
					}else if($remainWeight > 1500){
						$findWeight = $remainWeight/500;
						$charge += 35+25+15; //basic charge
						for($i=3; $i < $findWeight; $i++){
							$charge += 10;
						}
					}
				}else{
					$charge = 0;
				}
			}
			if($city == 3){
				if($price < 2000){
					if($remainWeight <= 500){
						$charge += 40; //upto 500gm
					}else if($remainWeight > 500 && $remainWeight <= 1000){
						$charge += 40+30; //501gm-1000gm
					}else if($remainWeight > 1000 && $remainWeight <= 1500){
						$charge += 40+30+30; //1001gm-1500gm
					}else if($remainWeight > 1500){
						$findWeight = $remainWeight/500;
						$charge += 40+30+30; //basic charge
						for($i=3; $i < $findWeight; $i++){
							$charge += 15;
						}
					}
				}else{
					$charge = 0;
				}
			}
			if($city == 4 || $city == 5){
				if($price < 2000){
					if($remainWeight <= 500){
						$charge += 50; //upto 500gm
					}else if($remainWeight > 500 && $remainWeight <= 1000){
						$charge += 50+30; //501gm-1000gm
					}else if($remainWeight > 1000 && $remainWeight <= 1500){
						$charge += 50+30+20; //1001gm-1500gm
					}else if($remainWeight > 1500){
						$findWeight = $remainWeight/500;
						$charge += 50+30+20; //basic charge
						for($i=3; $i < $findWeight; $i++){
							$charge += 20;
						}
					}
				}else{
					$charge = 0;
				}
			}
		}
		$price = $this->natuur->get_natuur_currency_function($charge,$charge);
		return $price['return_price']; //total charges
    }
	public function get_shipping_fees_calc($city,$weight) //For test purpose
	{		
		$price = $this->natuur->subtotal();
		$maxWeight = 20000;
		
		$totPercel = $weight/$maxWeight;
		for($j=0; $j < $totPercel; $j++){
			if($j == 0){
				if($totPercel <= 1){
					$remainWeight = $totPercel*$maxWeight;
				}else{
					$remainWeight = $maxWeight;
				}
			}
			if($j > 0){
				$remainWeight = $weight-($j*$maxWeight);
			}
			if($city == 1 || $city == 2){
				if($price < 1000){
					if($remainWeight <= 500){
						$charge += 35; //upto 500gm
					}else if($remainWeight > 500 && $remainWeight <= 1000){
						$charge += 35+25; //501gm-1000gm
					}else if($remainWeight > 1000 && $remainWeight <= 1500){
						$charge += 35+25+15; //1001gm-1500gm
					}else if($remainWeight > 1500){
						$findWeight = $remainWeight/500;
						$charge += 35+25+15; //basic charge
						for($i=3; $i < $findWeight; $i++){
							$charge += 10;
						}
					}
				}else{
					$charge = 0;
				}
			}
			if($city == 3){
				if($price < 2000){
					if($remainWeight <= 500){
						$charge += 40; //upto 500gm
					}else if($remainWeight > 500 && $remainWeight <= 1000){
						$charge += 40+30; //501gm-1000gm
					}else if($remainWeight > 1000 && $remainWeight <= 1500){
						$charge += 40+30+30; //1001gm-1500gm
					}else if($remainWeight > 1500){
						$findWeight = $remainWeight/500;
						$charge += 40+30+30; //basic charge
						for($i=3; $i < $findWeight; $i++){
							$charge += 15;
						}
					}
				}else{
					$charge = 0;
				}
			}
			if($city == 4 || $city == 5){
				if($price < 2000){
					if($remainWeight <= 500){
						$charge += 50; //upto 500gm
					}else if($remainWeight > 500 && $remainWeight <= 1000){
						$charge += 50+30; //501gm-1000gm
					}else if($remainWeight > 1000 && $remainWeight <= 1500){
						$charge += 50+30+20; //1001gm-1500gm
					}else if($remainWeight > 1500){
						$findWeight = $remainWeight/500;
						$charge += 50+30+20; //basic charge
						for($i=3; $i < $findWeight; $i++){
							$charge += 20;
						}
					}
				}else{
					$charge = 0;
				}
			}
		}
		echo 'Shipping fees is <b>'.$charge.'</b>'; //total charges
    }
	function payment(){
		$this->load->library('form_validation');
		
		$data['customer']	= $this->natuur->customer();
		
		if(isset($data['customer']['id'])){
			$data['customer_addresses'] = $this->Customer_model->get_address_list($data['customer']['id']);
		}
		
		$a	= $this->Customer_model->get_address($this->input->post('address_checked'));
		
		/* require that a payment method is selected */
		$this->form_validation->set_rules('payment_mode', 'Payment Method', 'trim|required');
		$this->form_validation->set_rules('term', 'Please accept terms and condition', 'trim|required');
		
		if ($this->form_validation->run() == false){

			/* where to next? Shipping? */
			$shipping_methods           = $this->_get_shipping_methods();
			$data['total_weight']		= $this -> _calculate_shipping_methods();
			$shipping					= $this->natuur->shipping_method();
			$data['shipping_code']		= $shipping['code'];
			$data['shipping_methods']	= $shipping_methods;

			/* are the being bounced back? */
			$module = $this->input->post('payment_mode');

			if($this->input->post('payment_mode') == "PayU_Money"){
				$description = "PayU Money";
			}else if($this->input->post('payment_mode') == "Icici"){
				$description = "Icici";
			}else if($this->input->post('payment_mode') == "Razorpay"){
				$description = "Razorpay";
			}else{
				$description = "Cash On Delivery";
			}
			$this->natuur->set_payment($module, $description);
			$data['payment_method']		= $this->natuur->payment_method();
			if($this->input->post('use_shipping')===false && isset($data['customer']['bill_address'])){
				//$data['use_shipping'] = ($data['customer']['bill_address'] == @$data['customer']['ship_address']);
				$data['use_shipping']= false;
			} else if($this->input->post('use_shipping')=='yes') {
				 $data['use_shipping'] = true;
			} else {
				 $data['use_shipping'] = false;
			}
			
			$data['error'] = validation_errors();
			//$this->load->view('checkout/address_form', $data);
			$this->load->view('checkout/checkout_form', $data);
		}else{
			$customer				= $this->natuur->customer();
			
			$customer['bill_address']['company']		= '';
			$customer['bill_address']['firstname']		= $a['field_data']['firstname'];
			$customer['bill_address']['lastname']		= '';
			$customer['bill_address']['email']			= '';
			$customer['bill_address']['phone']			= $a['field_data']['phone'];
			$customer['bill_address']['address1']		= $a['field_data']['address1'];
			$customer['bill_address']['address2']		= $a['field_data']['landmark'];
			$customer['bill_address']['city']			= $a['field_data']['city'];
			$customer['bill_address']['zip']			= $a['field_data']['zip'];
			$customer['bill_address']['zone']			= $a['field_data']['zone_name'];
			$customer['bill_address']['country']		= $a['field_data']['country'];
			$customer['bill_address']['country_code']	= $a['field_data']['country_code'];
			$customer['bill_address']['zone_id']		= $a['field_data']['zone_id'];
			$customer['bill_address']['country_id']		= $a['field_data']['country_id'];
			
			if(empty($customer['id']))
			{
				$customer['company']	= $customer['company'];
				$customer['firstname']	= $customer['firstname'];
				$customer['lastname']	= '';
				$customer['phone']		= $customer['phone'];
				$customer['email']		= $customer['email'];
			}else{
				$customer['firstname']	= $customer['firstname'];				$customer['lastname']	= '';			
			}
			
			if(!isset($customer['group_id'])){
				$customer['group_id'] = 1; /* default group */
			}
			if($this->input->post('use_shipping')!='yes'){
				$customer['ship_address']	= $customer['bill_address'];
			}else{
				$customer['ship_address']['company']		= '';
				$customer['ship_address']['firstname']		= $this->input->post('bill_firstname');
				$customer['ship_address']['lastname']		= '';
				$customer['ship_address']['email']			= '';
				$customer['ship_address']['phone']			= $this->input->post('bill_phone');
				$customer['ship_address']['address1']		= $this->input->post('bill_address');
				$customer['ship_address']['address2']		= $this->input->post('bill_landmark');
				$customer['ship_address']['city']			= $this->input->post('bill_city');
				$customer['ship_address']['zip']			= $this->input->post('bill_zip');
				/* get zone / country data using the zone id submitted as state*/

				/*$country								= $this->Location_model->get_country($this->input->post('bill_country_id'));
				if($this->Location_model->has_zones($country->id))
				{
					$zone									= $this->Location_model->get_zone($this->input->post('bill_zone_id'));
					$customer['ship_address']['zone']			= $zone->code;
				} else {
					$customer['ship_address']['zone'] 		= '';
				}
				$customer['ship_address']['country']		= $country->name;
				$customer['ship_address']['country_code']   = $country->iso_code_2;*/
				$customer['ship_address']['zone_id']		= $this->input->post('bill_zone_id');
				$customer['ship_address']['country_id']		= $this->input->post('bill_country_id');
			}
			
			$module = $this->input->post('payment_mode');
			if($module == "PayU_Money"){
				$des = "PayU Money";
			}elseif($module == "Icici"){
				$des = "Icici";
			}elseif($module == "Razorpay"){
				$des = "Razorpay";
			}else{
				$des = "Cash On Delivery";
			}
			
			
			$this->load->model('Settings_model');
			$settings = $this->Settings_model->get_settings('naturalmama');
			$chk_pin['pincode'] = $a['zipcode'];
			$get_available_pin	= $this->pin_code_serviceability($chk_pin);
			//$get_available_pin	= $this->location_model->get_available_pin($a['zipcode']);
			if($get_available_pin){
				$products_weight = $this->natuur->products_weight()+($this->natuur->products_weight()*10/100);
				$get_shipping_fees = $this->get_shipping_fees($get_available_pin['city_category'],$products_weight);
				$cur =  $this->natuur->get_currncy();
				$curr = $cur['currency_code'];
				$razor['razorpay_key'] = [
					'keyId'=> 'rzp_test_ivUcuQUoTZDjkt',
					'secretKey'=> 'kEUhGa1UmoFw3bGMUzWCnWEV',
					'amount'=> $this->natuur->subtotal()+$get_shipping_fees,
					'currency'=> $curr,
					'name'=> $settings['company_name'],
					'email'=> $settings['email'],
					'mobile'=> $settings['mobile'],
				];

				$this->natuur->set_payment( $module, $des);
				$this->load->view('razorpay/Razorpay', $razor);
			}else{
				redirect('/');
			}
			$customer['ship_address']['get_shipping_fees']			= $get_shipping_fees;
			/* save customer details*/
			$this->natuur->save_customer($customer);
			//$this->natuur->set_payment( $module, $des);
			//redirect('checkout/place_order');
		}		
	}
	function payment_success(){
		$data['response'] = $this->input->post();
		$data['razorpay_key'] = [
			'keyId'=> 'rzp_test_ivUcuQUoTZDjkt',
			'secretKey'=> 'kEUhGa1UmoFw3bGMUzWCnWEV',
		];
		$this->load->view('razorpay/Razorpay_fetch_payment', $data);
	}
	
	function product_quick_view($id)
	{
		$data['customer']			= (array)$this->Customer_model->get_customer($this->customer['id']);
		
		$data['customers']			= $this->Customer_model->get_customers();
		
		//get the product
		$data['product']	= $this->Product_model->get_product($id);
		
		if(!$data['product'] || $data['product']->enabled==0)
		{
			show_404();
		}
		
		$data['base_url']			= $this->uri->segment_array();
		
		$data['page_title']			= $data['product']->name;
		$data['meta']				= $data['product']->meta;
		$data['seo_title']			= (!empty($data['product']->seo_title))?$data['product']->seo_title:$data['product']->name;
			
		if($data['product']->images == 'false')
		{
			$data['product']->images = array();
		}
		else
		{
			$data['product']->images	= array_values((array)json_decode($data['product']->images));
		}
		return $this->load->view('popup_product', $data);
	}
	function address_form($id = 0)
	{
		
		$customer = $this->natuur->customer();
		//echo $this->customer['id'];
		//grab the address if it's available
		$data['id']			= false;
		$data['firstname']	= '';
		$data['phone']	= '';
		$data['alt_phone']	= '';
		$data['zip']		= '';
		$data['address1']	= '';
		$data['landmark']		= '';
		$data['country_id']		= '';
		$data['city']	= '';
		$data['zone_id']	= '';
		

		if($id != 0)
		{
			$a	= $this->Customer_model->get_address($id);
			if($a['customer_id'] == $this->customer['id'])
			{
				//notice that this is replacing all of the data array
				//if anything beyond this form data needs to be added to
				//the data array, do so after this portion of code
				$data		= $a['field_data'];
				$data['id']	= $id;
			} else {
				redirect('/'); // don't allow cross-customer editing
			}
			
			$data['zones_menu']	= $this->location_model->get_zones_menu($data['country_id']);
		}
		
		//get the countries list for the dropdown
		$data['countries_menu']	= $this->location_model->get_countries_menu();
		
		if($id==0)
		{
			//if there is no set ID, the get the zones of the first country in the countries menu
			$data['zones_menu']	= $this->location_model->get_zones_menu(0);
		} else {
			$data['zones_menu']	= $this->location_model->get_zones_menu($data['country_id']);
		}

		$this->load->library('form_validation');
		$this->form_validation->set_rules('firstname', 'Name', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('phone', 'Phone no', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('zip', 'Pincode', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('address1', 'Address', 'trim|required|max_length[128]');
		$this->form_validation->set_rules('city', 'City', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('country_id', 'Country', 'trim|required|numeric');
		$this->form_validation->set_rules('zone_id', 'State', 'trim|required|numeric');
		
		
		if ($this->form_validation->run() == FALSE)
		{
			if(validation_errors() != '')
			{
				echo validation_errors();
			}
			else
			{
				$this->partial('address_form', $data);
				
			}
		}
		else
		{
			$a = array();
			$a['id']						= ($id==0) ? '' : $id;
			$a['customer_id']				= $this->customer['id'];
			$a['zipcode']				= $this->input->post('zip');
			$a['field_data']['firstname']	= $this->input->post('firstname');
			$a['field_data']['phone']		= $this->input->post('phone');
			$a['field_data']['alt_phone']		= $this->input->post('alt_phone');
			$a['field_data']['zip']			= $this->input->post('zip');
			$a['field_data']['address1']	= $this->input->post('address1');
			$a['field_data']['landmark']	= $this->input->post('landmark');
			$a['field_data']['city']		= $this->input->post('city');
			
			// get zone / country data using the zone id submitted as state
			$country = $this->location_model->get_country(set_value('country_id'));	
			$zone    = $this->location_model->get_zone(set_value('zone_id'));		
			if(!empty($country))
			{
				$a['field_data']['zone']		= $zone->code;  // save the state for output formatted addresses
				$a['field_data']['zone_name']		= $zone->name;  // save the state for output formatted addresses
				$a['field_data']['country']		= $country->name; // some shipping libraries require country name
				$a['field_data']['country_code']   = $country->iso_code_2; // some shipping libraries require the code 
				$a['field_data']['country_id']  = $this->input->post('country_id');
				$a['field_data']['zone_id']		= $this->input->post('zone_id');  
			}
			
			$this->Customer_model->save_address($a);
			$this->session->set_flashdata('message', 'Your address has been saved!');
			echo 1;
		}
	}
	function address_user_form($id = 0)
	{
		
		$customer = $this->natuur->customer();
		//echo $this->customer['id'];
		//grab the address if it's available
		$data['id']			= false;
		$data['firstname']	= '';
		$data['phone']	= '';
		$data['alt_phone']	= '';
		$data['zip']		= '';
		$data['address1']	= '';
		$data['landmark']		= '';
		$data['country_id']		= '';
		$data['city']	= '';
		$data['zone_id']	= '';
		

		if($id != 0)
		{
			$a	= $this->Customer_model->get_address($id);
			if($a['customer_id'] == $this->customer['id'])
			{
				//notice that this is replacing all of the data array
				//if anything beyond this form data needs to be added to
				//the data array, do so after this portion of code
				$data		= $a['field_data'];
				$data['id']	= $id;
			} else {
				redirect('/'); // don't allow cross-customer editing
			}
			
			$data['zones_menu']	= $this->location_model->get_zones_menu($data['country_id']);
		}
		
		//get the countries list for the dropdown
		$data['countries_menu']	= $this->location_model->get_countries_menu();
		
		if($id==0)
		{
			//if there is no set ID, the get the zones of the first country in the countries menu
			$data['zones_menu']	= $this->location_model->get_zones_menu(0);
		} else {
			$data['zones_menu']	= $this->location_model->get_zones_menu($data['country_id']);
		}

		$this->load->library('form_validation');
		$this->form_validation->set_rules('firstname', 'Name', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('phone', 'Phone no', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('zip', 'Pincode', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('address1', 'Address', 'trim|required|max_length[128]');
		$this->form_validation->set_rules('city', 'City', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('country_id', 'Country', 'trim|required|numeric');
		$this->form_validation->set_rules('zone_id', 'State', 'trim|required|numeric');
		
		
		if ($this->form_validation->run() == FALSE)
		{
			if(validation_errors() != '')
			{
				echo validation_errors();
			}
			else
			{
				$this->get_address_form('address_user_form', $data);
				
			}
		}
		else
		{
			$a = array();
			$a['id']						= ($id==0) ? '' : $id;
			$a['customer_id']				= $this->customer['id'];
			$a['zipcode']				= $this->input->post('zip');
			$a['field_data']['firstname']	= $this->input->post('firstname');
			$a['field_data']['phone']		= $this->input->post('phone');
			$a['field_data']['alt_phone']		= $this->input->post('alt_phone');
			$a['field_data']['zip']			= $this->input->post('zip');
			$a['field_data']['address1']	= $this->input->post('address1');
			$a['field_data']['landmark']	= $this->input->post('landmark');
			$a['field_data']['city']		= $this->input->post('city');
			
			// get zone / country data using the zone id submitted as state
			$country = $this->location_model->get_country(set_value('country_id'));	
			$zone    = $this->location_model->get_zone(set_value('zone_id'));		
			if(!empty($country))
			{
				$a['field_data']['zone']		= $zone->code;  // save the state for output formatted addresses
				$a['field_data']['zone_name']		= $zone->name;  // save the state for output formatted addresses
				$a['field_data']['country']		= $country->name; // some shipping libraries require country name
				$a['field_data']['country_code']   = $country->iso_code_2; // some shipping libraries require the code 
				$a['field_data']['country_id']  = $this->input->post('country_id');
				$a['field_data']['zone_id']		= $this->input->post('zone_id');  
			}
			
			$this->Customer_model->save_address($a);
			//$this->session->set_flashdata('message', 'Your address has been saved!');
			echo 1;
		}
	}
	function get_address_form($view, $vars = array(), $string=false)
	{
		
		if($string)
		{
			return $this->load->view($view, $vars, true);
		}
		else
		{
			$this->load->view($view, $vars);
		}
	}
	function delete_address()
	{
		$id = $this->input->post('id');
		// use the customer id with the addr id to prevent a random number from being sent in and deleting an address
		$customer = $this->natuur->customer();
		$this->Customer_model->delete_address($id, $customer['id']);
		echo $id;
	}
	function newsletterEmail()
	{
		//save user email those are subscribe for newsletter
		$name = $this->input->post('name');
		$email = $this->input->post('email');

		$check_email = $this->Customer_model->check_newsletter($email);

		if($check_email)
		{
			echo "2";
		}
		else
		{
			$data = array('email' => $email, 'name' => $name,);
			if($this->Customer_model->saveNewsLetter($data))
			{
				$this->load->library('email');
				$config['mailtype'] = 'html';		
				$this->email->initialize($config);

				$this->email->from('info@natuur.com', $this->config->item('company_name'));
				$this->email->to($data['email']);
				$this->email->bcc($this->config->item('email'));
				$this->email->subject('Natuur || NewsLetter');
				// $this->email->message(html_entity_decode($row['content']));
				$this->email->message('Thank you for subscribe newsletter');
				
				$this->email->send();
				echo "1";
			}
		}
	}
	function uploadCustomerImg()
	{
		//$customerInfo = $this->user_model->selectCustomer();
        //$custPrevImageFull = './uploads/customer/full/'.$customerInfo['0']->profile_image;
        //$custPrevImageSmall = './uploads/customer/small/'.$customerInfo['0']->profile_image;
        
		//check whether submit button was clicked or not
        $arr['file_name'] = false;
		$arr['error']	= false;
		$config['allowed_types'] = '*';
		//$config['max_size']	= $this->config->item('size_limit');
		$config['upload_path'] = 'uploads/customer_image/full';
		$config['encrypt_name'] = true;
		$config['remove_spaces'] = true;
              
		$this->load->library('upload', $config);
		$field_name = "Filedata";
          
		if ( $this->upload->do_upload($field_name))
		{
			$upload_data	= $this->upload->data();
			$this->load->library('image_lib');
			/*
			
			I find that ImageMagick is more efficient that GD2 but not everyone has it
			if your server has ImageMagick then you can change out the line
			
			$config['image_library'] = 'gd2';
			
			with
			
			$config['library_path']		= '/usr/bin/convert'; //make sure you use the correct path to ImageMagic
			$config['image_library']	= 'ImageMagick';
			*/

			//small image
			$config['image_library'] = 'gd2';
			$config['source_image'] = 'uploads/customer_image/full/'.$upload_data['file_name'];
			$config['new_image']	= 'uploads/customer_image/small/'.$upload_data['file_name'];
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 235;
			$config['height'] = 235;
			$this->image_lib->initialize($config); 
			$this->image_lib->resize();
			$this->image_lib->clear();

			$arr['file_name']	= $upload_data['file_name'];
			$filename           = $upload_data['file_name'];
			$id=$this->input->post('tId');
			$this->Customer_model->updateProfileImage($id,$filename);
			$msg='true';

            //@unlink($custPrevImageFull);
            //@unlink($custPrevImageSmall);
		}
		
		if($this->upload->display_errors() != '')
		{
            echo '4';
			$arr['error'] = $this->upload->display_errors();
			$msg='false';
		}
		
        $result = json_encode(array('data'=>$msg,'imagename'=>$filename));
       	echo  $result;
		die;
	}
	function checkemail()
	{
			$emailId=$_POST['user_email'];

			$checkdata="SELECT id FROM customers WHERE email='$emailId' ";

			$query=mysql_query($checkdata);

			if(mysql_num_rows($query) == 0)
			{
				echo 'true';
			}
			else
			{
			   echo 'false';
			}
		exit();
	}
	function checkphone()
	{
			$phone=$_POST['user_phone'];

			$checkdata="SELECT id FROM customers WHERE phone='$phone' ";

			$query=mysql_query($checkdata);

			if(mysql_num_rows($query) == 0)
			{
				echo 'true';
			}
			else
			{
				echo 'false';
			}
		exit();
	}
	function updateAddcheckout()
	{
		$cust_id = $_POST['id'];
		$my_data = explode(',',rtrim($_POST['str'],','));
		$final_data = array();
		foreach($my_data as $data)
		{
			$newdata = explode('$#$',$data);
			$final_data[$newdata['0']] = $newdata['1'];
		}
		//print_r($final_data);exit;
		if($this->Customer_model->updateCustomerAdd($cust_id,$final_data)){
			
			$data['msg'] = 'updated successfully';
			
		}
		else{
			$data['msg'] = 'not updated';
		}
		
		echo json_encode($data);
	}
	
	function update_password()
	{
		$data['flag'] = '';
		$data['email1'] = $email = base64_decode($_GET['email']);
		$temp_pass = sha1($_GET['pass']);

		$query1 = "SELECT * FROM customers WHERE email = '".$email."' AND password = '".$temp_pass."' ";
		$val1   = $this->db->query($query1);
		//echo $this->db->last_query();
		$data1 = $val1->row_array();
		//print_r($data1);exit;
		if(count($data1) > 0)
		{
			$data['flag'] = true;
		}
		else
		{
			$data['msg'] = 'Link expire or not valid.';
			$data['flag'] = false;
		}
		
		if($this->input->post('password') != '')
		{
			$password			= sha1($this->input->post('password'));
			$user_mail			= $this->input->post('email1');
			$sql = "update customers SET password='".$password."' WHERE email='".$user_mail."'";
			//$this->db->query($sql);
			//echo $this->db->last_query();die;
			if($this->db->query($sql))
			{
				$data['msg'] = 'New password created successfully,</br> please login to continue.';
			} 
		}
		
		$this->load->view('change_forget_password',$data);
	}

    function my_wishlist(){
		if($this->Customer_model->is_logged_in(false, false)){
			$wish_ids = $this -> ratenreview_model -> get_user_wishlist();
			$wish_product_ids = '0,';
			
			foreach($wish_ids as $ids){
				$wish_product_ids .= $ids -> product_id.",";
			}
			$wish_product_ids = rtrim($wish_product_ids, ',');
			$data['product_records'] = $this -> ratenreview_model -> get_wish_product_row($wish_product_ids);
			$data['page'] = "my_wishlist";
			$this->load->view('wishlist', $data);
		}else{
			return redirect('/');
		}
	}
	
	function delete_wishlist($id){
		$this -> ratenreview_model -> delete_from_wishlist($id);
		redirect('secure/my_wishlist');
	}
	
	function currency($currency){
		if($currency=='1'){
			$symbol = 'fa fa-inr';
			$currency_code = 'INR';
		}else if($currency=='2'){
			$symbol = 'fa fa-usd';
			$currency_code = 'USD';
		}else if($currency=='3'){
			$symbol = 'fa fa-eur';
			$currency_code = 'EUR';
		}else if($currency=='4'){
			$symbol = 'fa fa-gbp';
			$currency_code = 'GBP';
		}else if($currency=='5'){
			$symbol = 'fa fa-usd';
			$currency_code = 'AUD';
		}else if($currency=='6'){
			$symbol = 'aed_gcc_image';
			$currency_code = 'AED';
		}
		$save_currency['currency'] = $currency;
		$save_currency['currency_symbol'] = $symbol;
		$save_currency['currency_code'] = $currency_code;
		$this->natuur->save_currency($save_currency);
		// redirect($CI->uri->uri_string());
		redirect($_SERVER['HTTP_REFERER']);
	}
	function convertor(){
		$url = 'https://api.exchangerate-api.com/v4/latest/USD';
		$json = file_get_contents($url);
		$exp = json_decode($json);

		$convert = $exp->rates->INR;

		echo $convert;
		//echo $this->natuur->_cart_contents['currency']['currency'];
		/*$fromCurrency = urlencode('USD');
		$toCurrency = urlencode('INR');	
		$encode_amount = 1;
		$url  = "https://www.google.com/search?q=".$fromCurrency."+to+".$toCurrency;
		$get = file_get_contents($url);
		//var_dump($get);
		$data = preg_split('/\D\s(.*?)\s=\s/',$get);
		var_dump($data);
		$exhangeRate = (float) substr($data[1],0,7);*/
		
		//echo $exhangeRate;
		
		/*$convertedAmount = $amount*$exhangeRate;
		$data = array( 'exhangeRate' = > $exhangeRate, 'convertedAmount' = >$convertedAmount, 'fromCurrency' = > strtoupper($fromCurrency), 'toCurrency' = > strtoupper($toCurrency));
		echo json_encode( $data );*/	
	}


}
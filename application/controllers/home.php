<?php
use Razorpay\Api\Api;
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
		$this->load->model('Category_model');
		$this->load->model('Product_model');
		$this->load->model('Option_model');
		$this->load->model('workshop_model');
		
		//load natuur library
		$this->load->library('Banners');
		$this->load->library('Menus');
		$this->load->model(array('location_model'));
		$this->load->model('Customer_model');
        $this->load->model('Testimonial_model');
		
		$this->load->library('natuur');
		
		//load helpers
		$this->load->helper(array('formatting_helper'));
		$this->customer = $this->natuur->customer();
		
    }

    public function index() {
        $data['page'] ='home';
		//$data['category'] =$this->Category_model->get_categorsies(0);
        $data['category'] =$this->Category_model->get_category(0);
			$data['hotproducts'] = $this->Product_model->get_hot_deals_products();
			foreach ($data['hotproducts'] as &$p)
			{
				$p->images	= (array)json_decode($p->images);
				$p->options	= $this->Option_model->get_product_options($p->id);
			}

       
        $data['arrHomeProducts'] = $this->Product_model->getHomeProducts();
		$data['arrFeaturedProducts'] = $this->Product_model->getFeaturedProducts();
        $data['arrNewProducts'] = $this->Product_model->getLatestProducts();
		$data['arrBestSellerProducts'] = $this->Product_model->getBestSellerProducts();
        $data['arrBestSellerProducts_P'] = $this->Product_model->getBestSellerProducts_P();
        $data['arrSpecialOfferProducts'] = $this->Product_model->getSpecialOfferProducts();
        $data['arrDealProducts'] = $this->Product_model->getSpecialDealProducts();

        $data['arrTestimonial'] = $this->Testimonial_model->getTestimonialList();
		$data['product1'] =$this->Product_model->get_category_product('127');//PERSONAL CARE
		$data['product2'] =$this->Product_model->get_category_product('106');//HOME CARE
		$data['product3'] =$this->Product_model->get_category_product('128');//SPECIALITY FOODS
		$data['product4'] =$this->Product_model->get_category_product('142');//Gifts
        $data['discover_categories'] = $this->Category_model->get_categories_tiered(false);

        $this->load->view('vwHome',$data);
    }

     public function do_login() {

        if ($this->session->userdata('is_client_login')) {
            redirect('home/loggedin');
        } else {
            $user = $_POST['username'];
            $password = $_POST['password'];

            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == FALSE) {
 /*
         * Code By Abhishek R. Kaushik
         * Sr. Software Developer 
         */
                $this->load->view('login');
            } else {
                $sql = "SELECT * FROM users WHERE user_name = '" . $user . "' AND user_hash = '" . md5($password) . "'";
                $val = $this->db->query($sql);


                if ($val->num_rows) {
                    foreach ($val->result_array() as $recs => $res) {

                        $this->session->set_userdata(array(
                            'id' => $res['id'],
                            'user_name' => $res['user_name'],
                            'email' => $res['email'],                            
                            'is_client_login' => true
                                )
                        );
                    }
                    redirect('calls/call');
                } else {
                    $err['error'] = 'Username or Password incorrect';
                    $this->load->view('login', $err);
                }
            }
        }
           }

        
    public function logout() {
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('title');
         $this->session->unset_userdata('ag_country');
        
        $this->session->unset_userdata('is_client_login');
        $this->session->sess_destroy();
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
        redirect('home', 'refresh');
    }
	public function workshop_calender() {
		$getUrl[0] = $this->input->post('month');
		$getUrl[1] = $this->input->post('year');
		/* date settings */
		$month = (int) ($getUrl[0]!='' ? $getUrl[0] : date('m'));
		$year = (int)  ($getUrl[1]!='' ? $getUrl[1] : date('Y'));
		if(!($month <13 && $month >0)){
			$month =date("m");  // Current month as default month
		}
		$no_of_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);//calculate number of days in a month

		$j= date('w',mktime(0,0,0,$month,1,$year)); // This will calculate the week day of the first day of the month
		$adj=str_repeat("<td bgcolor='#e3e3e3' class=\"noCursor\"></td>",$j);  // Blank starting cells of the calendar 
		$blank_at_end=42-$j-$no_of_days; // Days left after the last day of the month
		if($blank_at_end >= 7){
			$blank_at_end = $blank_at_end - 7 ;
		} 
		$adj2=str_repeat("<td bgcolor='#e3e3e3' class=\"noCursor\"></td>",$blank_at_end); // Blank ending cells of the calendar

		/* select month control */
		$select_month_control= date('F',mktime(0,0,0,$month,1,$year));

		/* select year control */
		$select_year_control= $year;
		
		$var .= '<tbody>
			<tr>';
					$prevUrl = ($month != 1 ? $month - 1 : 12)."#".($month != 1 ? $year : $year - 1);
					$nextUrl = ($month != 12 ? $month + 1 : 1)."#".($month != 12 ? $year : $year + 1);
				$var .= '<td colspan=1 data-url="'.$prevUrl.'" class="calender_change"><a href="javascript:void(0)" class="control"><i class="fa fa-angle-double-left"></i></a></td>
				<td colspan=5 class="monthName">'.$select_month_control." ".$select_year_control.'</td>
				<td colspan=1 data-url="'.$nextUrl.'" class="calender_change"><a href="javascript:void(0)" class="control"><i class="fa fa-angle-double-right"></i></a></td>
			</tr>
			<tr><th>Sun</th><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th></tr>
			</tbody><tbody class="dateWise">
			<tr>';
			$sMonth = sprintf("%02d", $month);
			
			$getWorkshopList = $this->workshop_model->getWorkshopListByMonthYear($sMonth,$year);
			//echo '<pre>';print_r($getWorkshopList);echo '</pre>';
			$workshopArray = array();
			foreach($getWorkshopList as $getWorkshopListVal){
				$workshopArray[] = $getWorkshopListVal->workshop_date;
			}
			//echo '<pre>';print_r($workshopArray);echo '</pre>';
			for($i=1;$i<=$no_of_days;$i++){
				if($i<10){
					$i = "0".$i;
				}
				$pv = "'".$i."','".$sMonth."','".$year."'";
				$dateFormat = $year."-".$sMonth."-".$i;
				$isFunction = '';
				if($dateFormat > date('Y-m-d')){
					if(in_array($dateFormat, $workshopArray)){
						$chk_workshop = $this->workshop_model->workshop_chk_details($dateFormat);
						$count_booked_workshop = $this->workshop_model->count_booked_workshop($chk_workshop->id);
						if($count_booked_workshop < $chk_workshop->number){
							$isFunction = 'onclick="dateFunction('.$pv.')"';
							$isAvailable = 'slots_available';
						}else{
							$isAvailable = 'no_slots_available';
						}
					}else{
						$isAvailable = 'previous_future';
					}
				}else{
					$isAvailable = 'previous_future';
				}
				$var .= $adj.'<td '.$isFunction.'><table class="bookingStatus"><td class="bookRight '.$isAvailable.'">'.$i.'</td></table></td>'; // This will display the date inside the calendar cell
				$adj='';
				$j ++;
				if($j==7){
					$var .= '</tr><tr>'; // start a new row
					$j=0;
				}
			}
			$var .= $adj2.'</tr></tbody>';   // Blank the balance cell of calendar at the end	
			echo $var; 
	}
	public function workshop_chk_details() {
		$date = $this->input->post('bDate');
		$chk_workshop = $this->workshop_model->workshop_chk_details($date);
		$var = '';
		//print_r($chk_workshop);
		if($chk_workshop){
			$count_booked_workshop = $this->workshop_model->count_booked_workshop($chk_workshop->id);
			if($count_booked_workshop < $chk_workshop->number){
			$var .= '<h6>Lorem Ipsum Text. Lorem Ipsum Text. Lorem Ipsum Text.</h6>';   // Description
			$var .= '<p>Workshop Date : '.$chk_workshop->workshop_date.'</p>';   // Blank the balance cell of calendar at the end
			$var .= '<p>Workshop Time : '.$chk_workshop->workshop_time.'</p>';   // Blank the balance cell of calendar at the end
			$var .= '<div class="razorpay_payment"><input id="payment_defult2" name="payment_mode" value="Razorpay" checked="" type="radio" data-target="createp_account">Razorpay</div>';   // Razorpay
			$var .= '<div class="accept_condition"><input id="term" name="term" value="yes" type="checkbox" checked="" data-target="createp_account"><label for="term"> I accept the <a href="#" target="_blank">Terms and Conditions</a></label></div>';   // Checkbox
			$var .= '<input type="hidden" name="date" value="'.$chk_workshop->workshop_date.'" /><div class="workshopBtn"><button type="submit" class="btn green_btn">Payment</button></div>';   // Blank the balance cell of calendar at the end
			//echo json_encode($chk_workshop);
			}else{
				$var .= '<div class="workshop_noslot">
						<h3><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>No Slot Available.</h3>
					</div>';
			}
		} 
		echo $var; 
	}
	public function workshop_payment() {
		$date = $this->input->post('date');		
		$chk_workshop = $this->workshop_model->workshop_chk_details($date);
		$count_booked_workshop = $this->workshop_model->count_booked_workshop($chk_workshop->id);
		if($count_booked_workshop < $chk_workshop->number){
			if($chk_workshop){
				$this->load->model('Settings_model');
				$settings = $this->Settings_model->get_settings('naturalmama');
				$cur =  $this->natuur->get_currncy();
				if($cur['currency_code']!=''){
					$curr = $cur['currency_code'];
				}else{
					$curr = 'INR';
				}
				$price = $this->natuur->get_natuur_currency_function($chk_workshop->workshop_price,$chk_workshop->workshop_price);
				$razor['razorpay_key'] = [
					'keyId'=> 'rzp_test_ivUcuQUoTZDjkt',
					'secretKey'=> 'kEUhGa1UmoFw3bGMUzWCnWEV',
					'amount'=> $price['return_price'],
					'currency'=> $curr,
					'name'=> $settings['company_name'],
					'email'=> $settings['email'],
					'mobile'=> $settings['mobile'],
					'workshop_id'=> $chk_workshop->id,
				];
				$this->load->view('razorpay/Razorpay-workshop', $razor);
			}
		}else{
			redirect('/');
		}
	}
	function workshop_payment_success(){
		$data['response'] = $this->input->post();
		$data['razorpay_key'] = [
			'keyId'=> 'rzp_test_ivUcuQUoTZDjkt',
			'secretKey'=> 'kEUhGa1UmoFw3bGMUzWCnWEV',
		];
		$this->load->view('razorpay/Razorpay_workshop_fetch_payment', $data);
	}
	function book_workshop($payment_status,$payment_id,$workshop_id){
		$customer	= $this->natuur->customer();
		$customer_id	= $customer['id'];
		
		//validate the slug
		$save = array();
		$save['id']		= false;
		$save['customer_id']		= $customer_id;
		$save['workshop_id']		= $workshop_id;
		$save['payment_status']	= $payment_status; 
		$save['payment_id']	= $payment_id;
		$save['order_currency']			= $this->natuur->_cart_contents['currency']['currency'];
		//save the page
		$book_workshop	= $this->workshop_model->book_workshop($save);
		
		$data['booking_details']	= $this->workshop_model->get_book_workshop($book_workshop);
		$data['workshop_details']	= $this->workshop_model->workshopDetails($workshop_id);
		
		$this->load->view('workshop_invoice', $data);
	}
    

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Temp_home extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
		$this->load->model('Category_model');
		$this->load->model('Product_model');
		$this->load->model('Option_model');
		
		//load natuur library
		$this->load->library('Banners');
		$this->load->library('Menus');
		$this->load->model(array('location_model'));
		$this->load->model('Customer_model');
        $this->load->model('Testimonial_model');
		
		$this->load->library('natuur');
		
		//load helpers
		$this->load->helper(array('formatting_helper'));
		$this->customer = $this->natuur->customer();
		
    }


 public function phpinfo() {

echo phpinfo();
 }
    public function index() {

        ini_set('error_reporting', E_ALL);
        ini_set('display_errors', '1');

        $data['page'] ='home';
		//$data['category'] =$this->Category_model->get_categorsies(0);
        $data['category'] =$this->Category_model->get_category(0);
			$data['hotproducts'] = $this->Product_model->get_hot_deals_products();
			foreach ($data['hotproducts'] as &$p)
			{
				$p->images	= (array)json_decode($p->images);
				$p->options	= $this->Option_model->get_product_options($p->id);
			}

       
        $data['arrHomeProducts'] = $this->Product_model->getHomeProducts();
		$data['arrFeaturedProducts'] = $this->Product_model->getFeaturedProducts();
        $data['arrNewProducts'] = $this->Product_model->getLatestProducts();
		$data['arrBestSellerProducts'] = $this->Product_model->getBestSellerProducts();
        $data['arrBestSellerProducts_P'] = $this->Product_model->getBestSellerProducts_P();

        $data['arrSpecialOfferProducts'] = $this->Product_model->getSpecialOfferProducts();
        $data['arrDealProducts'] = $this->Product_model->getSpecialDealProducts();

        $data['arrTestimonial'] = $this->Testimonial_model->getTestimonialList();
		$data['product1'] =$this->Product_model->get_category_product('127');
		$data['product2'] =$this->Product_model->get_category_product('106');
		$data['product3'] =$this->Product_model->get_category_product('128');
		$data['product4'] =$this->Product_model->get_category_product('142');

        $data['discover_categories'] = $this->Category_model->get_categories_tiered(false);
       
        $this->load->view('temp_vwHome',$data);
    }

     public function do_login() {

        if ($this->session->userdata('is_client_login')) {
            redirect('home/loggedin');
        } else {
            $user = $_POST['username'];
            $password = $_POST['password'];

            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == FALSE) {
 /*
         * Code By Abhishek R. Kaushik
         * Sr. Software Developer 
         */
                $this->load->view('login');
            } else {
                $sql = "SELECT * FROM users WHERE user_name = '" . $user . "' AND user_hash = '" . md5($password) . "'";
                $val = $this->db->query($sql);


                if ($val->num_rows) {
                    foreach ($val->result_array() as $recs => $res) {

                        $this->session->set_userdata(array(
                            'id' => $res['id'],
                            'user_name' => $res['user_name'],
                            'email' => $res['email'],                            
                            'is_client_login' => true
                                )
                        );
                    }
                    redirect('calls/call');
                } else {
                    $err['error'] = 'Username or Password incorrect';
                    $this->load->view('login', $err);
                }
            }
        }
           }

        
    public function logout() {
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('title');
         $this->session->unset_userdata('ag_country');
        
        $this->session->unset_userdata('is_client_login');
        $this->session->sess_destroy();
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
        redirect('home', 'refresh');
    }

    

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
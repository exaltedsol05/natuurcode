<?php
class Cms extends CI_Controller {

    
	public function __construct() {
        parent::__construct();
		/*
		$this->load->model('Category_model');
		$this->load->model('Option_model');
		$this->load->model('Search_model');
		$this->load->model('Product_model');
		$this->load->model('Page_model');
		$this->load->model('Digital_product_model');
		//load Dentkart library
		
		$this->load->model(array('location_model'));
		
		
		//$this->load->model('ratenreview_model');
		
		
		//load helpers
		$this->load->helper(array('formatting_helper'));
		//$this->load->helper('MY_url_helper');
		//$this->customer = $this->dentkart->customer();
		*/
		$this->load->library('Banners');
		$this->load->library('Menus');
		//$this->load->library('dentkart');
		$this->load->model('Customer_model');
		$this->load->model('cms_model');
		$this->load->model('Testimonial_model');
		
    }
	function cms_faq()
	{
		$data['page_title'] = 'FAQs';
		$data['arrFaq'] = $this->cms_model->getFaq();
		$this->load->view('vwFaq',$data);
	}
	function cms_about()
	{
		
		$data['page_title'] = 'About Us';
		$data['page'] = 'about';
		$data['arrAbout'] = $this->cms_model->getAboutUs();
		$this->load->view('vwAboutus',$data);
	}
	
	function cms_contactus()
	{
		$data['page_title'] = 'Contact Us';
		$data['page'] = 'contact';
		$data['arrContact'] = $this->cms_model->getContactUs();
		$this->load->view('vwContactus',$data);
	}
	function cms_blog()
	{
		$data['page_title'] = 'Blog';
		//$data['arrBlog'] = $this->cms_model->getBlog();
		$data['arrBlog'] = $this->Testimonial_model->getBlogList();
		$this->load->view('vwBlog',$data);
	}
	function cms_blog_read($blog_id)
	{
		$data['page_title'] = 'Blog';
		$data['blogDetails'] = $this->Testimonial_model->getBlogDetails($blog_id);
		$this->load->view('vwFullBlog',$data);
	}
	function cms_term_conditions()
	{
		$data['page_title'] = 'Term & Conditions';
		$data['arrTerm'] = $this->cms_model->getTerm();
		$this->load->view('vwTerm',$data);
	}
	function cms_cancellation_returns()
	{
		$data['page_title'] = 'Cancellation & Returns';
		$data['arrCancellationReturns'] = $this->cms_model->getCancellationReturns();
		$this->load->view('vwCancellationReturns',$data);
	}
	function cms_privacy_policy()
	{
		$data['page_title'] = 'Privacy Policy';
		$data['arrPrivacy'] = $this->cms_model->getPrivacy();
		$this->load->view('vwPrivacy',$data);
	}
	function cms_cancel_policy()
	{
		$data['page_title'] = 'Cancellation Policy';
		$data['arrCancel'] = $this->cms_model->getCancel();
		$this->load->view('vwCancel',$data);
	}
	function cms_shipping_policy()
	{
		$data['page_title'] = 'Shipping Policy';
		$data['arrShipping'] = $this->cms_model->getShipping();
		$this->load->view('vwShipping',$data);
	}
	function cms_secure_shipping()
	{
		$data['page_title'] = 'SECURE SHOPPING';
		$data['arrSecureShipping'] = $this->cms_model->getSecureShipping();
		$this->load->view('vwSecureShipping',$data);
	}
	function cms_easy_buy()
	{
		$data['page_title'] = 'Easy Buy';
		$data['arrEasyBuy'] = $this->cms_model->getEasyBuy();
		$this->load->view('vwEasyBuy',$data);
	}
	function cms_hassle_free_return()
	{
		$data['page_title'] = 'Hassle Free Return';
		$data['arrHassleFreeReturn'] = $this->cms_model->getHassleFreeReturn();
		$this->load->view('vwHassleFreeReturn',$data);
	}
	// function cms_gift_natuur()
	// {
		// $data['page_title'] = 'Gift Natuur';
		// $data['arrGiftNatuur'] = $this->cms_model->getGiftNatuur();
		// $this->load->view('vwGiftNatuur',$data);
	// }
	
	function contact_us_request()
	{
		extract($_POST);

		$data = array(
			'name' => $exampleInputName,
			'email' => $exampleInputEmail,
			'msg' => $exampleInputComments,
			);

		if($this->cms_model->saveContactUsDta($data))
		{
			$d['status'] = 'yes';
			$d['msg'] = 'Work in progress';
			echo json_encode($d);
			die;
		}
	}
}
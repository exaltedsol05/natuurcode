<?php
class Ratenreview_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
		$this->load->model('Category_model');
		$this->load->model('Option_model');
		$this->load->model('Search_model');
		$this->load->model('Product_model');
		$this->load->model('Page_model');
		
		//load natuur library
		$this->load->library('Banners');
		$this->load->library('Menus');
		$this->load->model(array('location_model'));
		$this->load->model('Customer_model');
		$this->load->model('ratenreview_model');
		
		$this->load->library('natuur');
		
		//load helpers
		$this->load->helper(array('formatting_helper'));
		//$this->load->helper('MY_url_helper');
		$this->customer = $this->natuur->customer();
		
    }
	
	function index()
	{
		//$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		$data['homepage']			= true;
		
		$this->view('homepage', $data);
		
	}

	//wishlist functions
	function add_wishlist(){
		$product_id = $this->input->post('product_id');
		if(!$this->Customer_model->is_logged_in(false,false)){
			echo "logout";
		}else{
			//if user logged in
			$isInsert = $this -> ratenreview_model -> add_wishlist_table($product_id);
			if($isInsert > 0){
				echo "yes";
			}else{
				echo "no";
			}
		}
	}
	
	
	
	//compare functions
	function add_compare(){
		$product_id = $this->input->post('product_id');
		if(isset($this->session->userdata['comp'])){
			$comp_product_ids = $this->session->userdata['comp'];
			
			//Check Already Added or not
			if (strpos($comp_product_ids, $product_id) !== false) {	
				echo 'found';
			}else{
				$totalproduct = explode(",", $comp_product_ids);
				if(count($totalproduct) > 3){
					echo "full";
				}else{
					$new_comp = $comp_product_ids.",".$product_id;
					$this->session->set_userdata('comp', $new_comp);
					echo "add";
				}	
			}
		}else{
			$this->session->set_userdata('comp', $product_id);
			echo "add";
		}
	}	
	
	function compare(){
		//$this->session->unset_userdata('comp');
		if(isset($this->session->userdata['comp'])){
			if (empty($this->session->userdata['comp'])) {
				$this->session->unset_userdata('comp');
				$comp_product_ids = '0';
			}else{
				$comp_product_ids = $this->session->userdata['comp'];
			}			
		}else{
			$comp_product_ids = 0;
		}
		
		$data['product_records'] = $this -> ratenreview_model -> get_comp_product_row($comp_product_ids);
		$this->load->view('compare', $data);
	}
	
	
	function delete_compr($id){
		$comp_product_ids = $this->session->userdata['comp'];
		$product_id = $id.',';
		
		if (strpos($comp_product_ids, $product_id) !== false) {
			$comp_product_ids = str_replace($product_id, '', $comp_product_ids);
		}else{
			$comp_product_ids = str_replace($id, '', $comp_product_ids);
		}
		
		$comp_product_ids = rtrim($comp_product_ids,",");
		
		$this->session->set_userdata('comp', $comp_product_ids);
		redirect('ratenreview_controller/compare');
	}

	//recommend functions
	function add_recommend(){
		if(!$this->Customer_model->is_logged_in(false,false)){
			echo "logout";
		}else{
			$product_id = $this->input->post('product_id');
			$recommendation_for = $this->input->post('recommendation_for');
			$isreccond = $this -> ratenreview_model -> check_is_recommend($product_id);
			if($isreccond){
				echo 'no';
			}else{
				$this -> ratenreview_model -> add_recommend($product_id, $recommendation_for);
				echo 'yes';
				$all_recommend = $this -> ratenreview_model -> count_recommend();
				//$yes_recommend = $this -> ratenreview_model -> yes_recommend();
				//$no_recommend - $this -> ratenreview_model -> no_recommend();
			}
		}
	}
	
	//add rating
	function add_rating(){
		if(!$this->Customer_model->is_logged_in(false,false)){
			echo "logout";
		}else{
			$product_id = $this->input->post('product_id');
			$rating = $this->input->post('rating');
			$isreccond = $this -> ratenreview_model -> check_is_rate($product_id);
			if($isreccond){
				echo 'no';
			}else{
				$this -> ratenreview_model -> add_rating($product_id, $rating);
				echo 'yes';
			}
		}
	}
	
	//review functions
	function add_review(){
		if(!$this->Customer_model->is_logged_in(false,false)){
			echo "logout";
		}else{
			$review = $this->input->post('review');
			$product_id = $this->input->post('pop_product_id');
			$review_title = $this->input->post('title');
			$rating = $this->input->post('rating');
			$isreccond = $this -> ratenreview_model -> check_is_rate($product_id);
			$isreview = $this -> ratenreview_model -> check_is_review($product_id);
			if($isreview && $isreccond){
				echo 'no';
			}else{
				$this -> ratenreview_model -> add_review($product_id, $review, $review_title);
				$this -> ratenreview_model -> add_rating($product_id, $rating);
				echo 'yes';
			}
		}
	}
	
	//show approved review page
	function approvedReviews(){
		$arr['page']	= 'Approved Reviews';
		$arr['reviews']	= $this->ratenreview_model->get_approved_reviews();
		$this->load->view('admin/approvedReviews', $arr);
	}
	
	//shw non approved review page
	function non_approvedReviews(){
		$arr['page']	= 'Non-Approved Reviews';
		$arr['reviews']	= $this->ratenreview_model->get_non_approved_reviews();
		$this->load->view('admin/non_approvedReviews', $arr);
	}
	
	//approved review
	function review_approve($id){
		$this -> ratenreview_model -> approve_from_review($id);
		$this->session->set_flashdata('message', 'You Approved review successfull.' );
		redirect('ratenreview_controller/non_approvedReviews');
	}
	
	//no approvied review
	function review_non_approve($id){
		$this -> ratenreview_model -> non_approve_from_review($id);
		$this->session->set_flashdata('message', 'You Un-approved review successfull.' );
		redirect('ratenreview_controller/approvedReviews');
	}
	
	//delete reviewe
	function review_non_delete($id){
		$this -> ratenreview_model -> delete_from_review($id);
		$this->session->set_flashdata('message', 'You Delete review successfull.' );
		redirect('ratenreview_controller/non_approvedReviews');
	}
	
	function review_delete($id){
		$this -> ratenreview_model -> delete_from_review($id);
		$this->session->set_flashdata('message', 'You Delete review successfull.' );
		redirect('ratenreview_controller/approvedReviews');
	}
	
	function ishelpful_review(){
		$review_id = $this->input->get('id');
		$product_slug = $this -> ratenreview_model -> update_ishelpfule($review_id);
		$this->session->set_flashdata('message', 'Review was mark as useful review.' );
		redirect($product_slug);
	}
	
	function recent_review(){
		$product_id = $this->input->get('id');
		$product_slug = $this -> ratenreview_model -> product_uri($product_id);
		$this->session->set_flashdata('message', 'Review are sort by most recent review.' );
		redirect($product_slug);
	}
	
	function invoice($order_id){
		$order = $this -> ratenreview_model -> get_order_details($order_id);
		$order_items = $this -> ratenreview_model -> get_order_items($order->id);
		
		$data['order'] = $order;
		$data['order_items'] = $order_items;
		$this->load->view('my_invoice', $data);
	}
	
	function my_ecash(){
		$result = $this -> ratenreview_model -> get_reward_details();
		$data = array();
		$data['page'] = "my_ecash";
		$data['earn'] = $result['earn'];
		$data['pending'] = $result['pending'];
		
		$data['ecash_records'] = $this -> ratenreview_model -> get_user_ecash_track();
		$this->load->view('my_ecash', $data);
	}
	
	function ecash_facebook_share(){
		$product_id = $this->input->post('product_id');
		$this -> ratenreview_model ->set_facebook_share_ecash($product_id);
	}
	
	function ecash_facebook_like(){
		if($this->Customer_model->is_logged_in(false, false)){
			$output = $this -> ratenreview_model ->set_facebook_like_ecash();	
			echo $output;	
		}else{
			echo false;
		}
	}
}
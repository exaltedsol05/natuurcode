<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class atompaynetz
{
	var $CI;
	
	//this can be used in several places
	var	$method_name;
	var $url = "https://payment.atomtech.in/paynetz/epi/fts";
	var $Login="14290";
	var $Password="DENT@123";
	var $MerchantName="DENTKART";
	var $TxnCurr="INR";
	var $TxnScAmt="0";
	
	function __construct()
	{
		$this->CI =& get_instance();
		$this->method_name	= 'Credit Card Through atompaynetz Solutions';
	}
	
	//these are the front end form and check functions
	function checkout_form($post = false)
	{
		$settings	= $this->CI->Settings_model->get_settings('atompaynetz');
		$enabled	= $settings['enabled'];

		$cc_data = $this->CI->session->userdata('cc_data');
		
		$form			= array();
		if($enabled == 1)
		{
			$form['name']	= $this->method_name;
			$form['form']	= $this->CI->load->view('atompaynetz_checkout', array('cc_data'=>$cc_data), true);
		}
		
		return $form;
	}
	function checkout_check()
	{
		// Nothing to check in this module
		return false;
	}
	
	
	function description()
	{
		return 'Atompaynetz';
	}
	
	
	//payment processor
	function process_payment($shipping_cost,$rmt_point)
	{
		
		$customer = $this->CI->dentkart->customer();
	
		if ( $settings = $this->CI->Settings_model->get_settings('atompaynetz') ) 
		{
			/*$args = array();
			$args['sid'] 		   		= $settings['sid'];
			$args['cart_order_id'] 		= $this->CI->config->item('company_name').' order';
			$args['purchase_step'] 		= 'payment-method';
			$args['currency_code'] 		= $settings['currency'];
			$args['total'] 				= $this->CI->dentkart->total();
			$args['return_url'] 		= site_url($settings['cancel_url']);
			$args['x_receipt_link_url'] = site_url($settings['return_url']);
			$args['card_holder_name'] 	= $customer['bill_address']["firstname"] . ' ' . $customer['bill_address']["lastname"];
			$args['street_address'] 	= $customer['bill_address']["address1"];
			$args['street_address2'] 	= $customer['bill_address']["address2"];
			$args['city'] 				= $customer['bill_address']["city"];

			// Pass state for US and Canada
			if( $customer['bill_address']["country_id"] == "223" || $customer['bill_address']["country_id"] == "38" )
			{
				$args['state'] = $customer['bill_address']["zone"];
			}
			else
			{
				$args['state'] = 'XX';
			}
			
			$args['zip'] = $customer['bill_address']["zip"];
			$args['country'] = $customer['bill_address']["country_code"];
			$args['phone'] = $customer['bill_address']["phone"];
			$args['email'] = $customer['bill_address']["email"]; 
			
			$url = 'https://www.2checkout.com/checkout/purchase?'.http_build_query($args, '', '&amp;');
			redirect($url);*/
			
			
			
		$total=$this->CI->dentkart->total()+$shipping_cost-$rmt_point;
			
		$datenow = date("d/m/Y h:m:s");
		$modifiedDate = str_replace(" ", "%20", $datenow);
		//$this->url = $this->paymentConfig->Url;
		$postFields  = "";
		$postFields .= "&login=".$this->Login;
		$postFields .= "&pass=".$this->Password;
		//$postFields .= "&ttype=".$_POST['TType'];
		$postFields .= "&ttype=NBFundTransfer";
		
		//$postFields .= "&prodid=".$_POST['product'];
		$postFields .= "&prodid=DENTKART";
		//$postFields .= "&amt=".$_POST['amount'];
		$postFields .= "&amt=".$total;
		$postFields .= "&txncurr=".$this->TxnCurr;
		$postFields .= "&txnscamt=".$this->TxnScAmt;
		//$postFields .= "&clientcode=".urlencode(base64_encode($_POST['clientcode']));
		$postFields .= "&clientcode=".urlencode(base64_encode($customer['id']));
		$postFields .= "&txnid=".rand(0,999999);
		$postFields .= "&date=".$modifiedDate;
		//$postFields .= "&custacc=".$_POST['AccountNo'];
		$postFields .= "&custacc=1234567890";
		$postFields .= "&udf1=".$customer['bill_address']["firstname"] . ' ' . $customer['bill_address']["lastname"];
		$postFields .= "&udf2=".$customer['bill_address']["email"];
		$postFields .= "&udf3=".$customer['bill_address']["phone"];
		$postFields .= "&udf4=".$customer['bill_address']["address1"].' '.$customer['bill_address']["address2"].' '.$customer['bill_address']["city"].' '.$customer['bill_address']["zip"].' '.$customer['bill_address']["country_code"];	
		
		//$postFields .= "&ru=".$_POST['ru'];
		$postFields .= "&ru=https://dentkart.in/checkout/atompaynetz_status";
		// Not required for merchant
		//$postFields .= "&bankid=".$_POST['bankid'];

		$sendUrl = $this->url."?".substr($postFields,1)."\n";

		$this->writeLog($sendUrl);
		
		$returnData = $this->sendInfo($postFields);
	
		$this->writeLog($returnData."\n");
		$xmlObjArray     = $this->xmltoarray($returnData);

		$url = $xmlObjArray['url'];
		$postFields  = "";
		//$postFields .= "&ttype=".$_POST['TType'];
		$postFields .= "&ttype=NBFundTransfer";
		
		$postFields .= "&tempTxnId=".$xmlObjArray['tempTxnId'];
		$postFields .= "&token=".$xmlObjArray['token'];
		$postFields .= "&txnStage=1";
		$url = $this->url."?".$postFields;
		$this->writeLog($url."\n");
		redirect($url);
		//header("Location: ".$url);
			
			
			
		}
		else
		{
			return 'There was an error processing your payment through atompaynetz';
		}	
	}
	
	
	
	
	
	function sendInfo($data){
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $this->url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_PORT , 443); 
		curl_setopt($ch, CURLOPT_SSLVERSION,1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

		$returnData = curl_exec($ch);

		curl_close($ch);
			return $returnData;
	}
	
	function requestMerchant(){
		
		$datenow = date("d/m/Y h:m:s");
		$modifiedDate = str_replace(" ", "%20", $datenow);
		//$this->url = $this->paymentConfig->Url;
		$postFields  = "";
		$postFields .= "&login=".$this->Login;
		$postFields .= "&pass=".$this->Password;
		$postFields .= "&ttype=".$_POST['TType'];
		$postFields .= "&prodid=".$_POST['product'];
		$postFields .= "&amt=".$_POST['amount'];
		$postFields .= "&txncurr=".$this->TxnCurr;
		$postFields .= "&txnscamt=".$this->TxnScAmt;
		$postFields .= "&clientcode=".urlencode(base64_encode($_POST['clientcode']));
		$postFields .= "&txnid=".rand(0,999999);
		$postFields .= "&date=".$modifiedDate;
		$postFields .= "&custacc=".$_POST['AccountNo'];
		$postFields .= "&ru=".$_POST['ru'];
		// Not required for merchant
		//$postFields .= "&bankid=".$_POST['bankid'];

		$sendUrl = $this->url."?".substr($postFields,1)."\n";

		$this->writeLog($sendUrl);
		
		$returnData = $this->sendInfo($postFields);
		$this->writeLog($returnData."\n");
		$xmlObjArray     = $this->xmltoarray($returnData);

		$url = $xmlObjArray['url'];
		$postFields  = "";
		$postFields .= "&ttype=".$_POST['TType'];
		$postFields .= "&tempTxnId=".$xmlObjArray['tempTxnId'];
		$postFields .= "&token=".$xmlObjArray['token'];
		$postFields .= "&txnStage=1";
		$url = $this->url."?".$postFields;
		$this->writeLog($url."\n");
		header("Location: ".$url);
		
	}

	function writeLog($data){
		$fileName = date("Y-m-d").".txt";
		$fp = fopen("log/".$fileName, 'a+');
		$data = date("Y-m-d H:i:s")." - ".$data;
		fwrite($fp,$data);
		fclose($fp);
	}

	function xmltoarray($data){
		$parser = xml_parser_create('');
		xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8"); 
		xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
		xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
		xml_parse_into_struct($parser, trim($data), $xml_values);
		xml_parser_free($parser);
		
		$returnArray = array();
		$returnArray['url'] = $xml_values[3]['value'];
		$returnArray['tempTxnId'] = $xml_values[5]['value'];
		$returnArray['token'] = $xml_values[6]['value'];

		return $returnArray;
	}

	
	
	}

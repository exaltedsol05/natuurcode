<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cod
{
	var $CI;
	
	//this can be used in several places
	var	$method_name;
	
	function __construct()
	{
		$this->CI =& get_instance();
		$this->method_name	= 'Cash on Delivery';
	}
	
	/*
	checkout_form()
	this function returns an array, the first part being the name of the payment type
	that will show up beside the radio button the next value will be the actual form if there is no form, then it should equal false
	there is also the posibility that this payment method is not approved for this purchase. in that case, it should return a blank array 
	*/
	
	//these are the front end form and check functions
	function checkout_form($post = false)
	{
		$settings	= $this->CI->Settings_model->get_settings('cod');
		$enabled	= $settings['enabled'];
		
		$form			= array();
		if($enabled == 1)
		{
			$form['name']	= $this->method_name;
			$form['form']	= false;
		}
		
		return $form;
	}
	function checkout_check()
	{
		//this is where we would normally check the $_POST info
		
		//if all is well, return false, otherwise, return an error message
		return false;
	}
	
	function description()
	{
		//create a description from the session which we can store in the database
		//this will be added to the database upon order confirmation
		
		/*
		access the payment information with the  $_POST variable since this is called
		from the same place as the checkout_check above.
		*/
		
		return 'Cash on Delivery';
		
		
	}
	
	
	
	//payment processor
	function process_payment()
	{
		$process	= false;
		//process the payment here, if it goes through return false if it breaks down, return an error message
		if($process)
		{
			return 'There was an error processing your payment';
		}
		else
		{
			return false;
		}
	}
	
	
	
	}

<?php
	
class Menus {
	
	var $CI;
	function __construct(){
		$this->CI =& get_instance();
		$this->CI->load->model('category_model');
		$this->CI->load->model('Product_model');
	}

	function show_menu(){
		$data['categories']	= $this->CI->category_model->get_categories_tiered(false);
		$this->CI->load->view('vwTopMenu', $data);
	}
	
	function return_category_tier(){
		return $data['categories']	= $this->CI->category_model->get_categories_tiered(false);
	}

	function get_search_menu($parent_id){
		$search_categories	= $this->CI->category_model->get__search_categories($parent_id);
		return $search_categories;
	}
	
}
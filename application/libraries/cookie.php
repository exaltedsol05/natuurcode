<?php

class Cookie{
	var $CI;	
	function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->database();
	}

	function cookie_permission_allowed(){
		
		$header = array('REMOTE_ADDR');
		
		foreach ($header as $key){
			if (array_key_exists($key, $_SERVER) === true){
				foreach (explode(',', $_SERVER[$key]) as $ip){
					$ip = trim($ip); // just to be safe
					if($ip == '127.0.0.1' || $ip == '::1'){	//localhost 
						$query = $this->CI->db->get_where('cookie_permission', array('allowed_ip' => $ip));
					}else{
						if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
							// return $ip;
							$query = $this->CI->db->get_where('cookie_permission', array('allowed_ip' => $ip));
						}
					}
				}
			}
		}
		
		if ($query->num_rows() > 0){
			return true;
		}else{
			return false;
		}
	}	
}
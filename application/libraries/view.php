<?php

class View extends CI_Controller {	

	function __construct()
	{		
		parent::__construct();
		$this->load->model('Category_model');
		$this->load->model('Option_model');
		$this->load->model('Search_model');
		$this->load->model('Product_model');
		$this->load->model('Page_model');
		
		//load natuur library
		$this->load->library('Banners');
		$this->load->library('Menus');
		$this->load->model(array('location_model'));
		$this->load->model('Customer_model');
		$this->load->model('ratenreview_model');
		
		$this->load->library('natuur');
		
		//load helpers
		$this->load->helper(array('formatting_helper'));
		//$this->load->helper('MY_url_helper');
		$this->customer = $this->natuur->customer();
	}
	
	function order($type, $order_id){
		$server = $this->input->server(index);
		
		$order = $this -> ratenreview_model -> get_order_details($order_id);
		$order_items = $this -> ratenreview_model -> get_order_items($order->id);
		
		$data['page_title'] = "Order Detail";
		if($type == 'invoice'){
			$data['page_title'] = "Invoice";
			$data['invoice_no'] = "#DK-".$order->order_number;	
			$data['invoice'] = true;	
			$invoice_date = $this -> ratenreview_model -> get_invoice_items($order->id);
			$data['invoice_no_date'] = date("F d, Y", strtotime($invoice_date));
		}
		
		$data['order'] = $order;
		$data['order_items'] = $order_items;
		$this->load->view('my_invoice', $data);
	}
}
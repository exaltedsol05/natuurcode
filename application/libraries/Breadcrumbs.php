<?php
/**
 * Breadcrumbs Class
 *
 * @package     GoCart
 * @subpackage  Libraries
 * @category    Breadcrumbs
 * @author      Clear Sky Designs
 * @link        http://gocartdv.com
 */

class Breadcrumbs
{
    var $breadcrumbs;
    var $CI;
	function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->helper('url');
		$this->CI->load->model('category_model');
		$this->breadcrumbs = [];
		
		
	}
	
	public function generate(){
		$slug   = $this->CI->uri->segment(1);
		$type='category';
        if(!$type || !$slug){
            return; //return blank
        }
		
        if($type == 'category'){
            $category =  $this->CI->Category_model->slug($slug);
            if(!$category){
                return;
            }
            $this->trace_categories($category->id);
        }
		 
		$data['breadcrumbs']=$this->breadcrumbs;
		$this->CI->load->view('breadcrumbs', $data);
	}
	
	public function trace_categories($id){
		$category = $this->CI->Category_model->find($id);
		
		if($category){
			array_unshift($this->breadcrumbs, ['link'=>site_url($category->slug), 'name'=>$category->name]);
			$this->trace_categories($category->parent_id);
		}		
	}
	
    /*public function __construct()
    {
		
        //$this->breadcrumbs = [];
		$this->CI =& get_instance();
		$this->CI->load->model('category_model');
    }

    public function trace_categories($id)
    {
        $category = $this->Categories()->find($id);
        if($category)
        {
            array_unshift($this->breadcrumbs, ['link'=>site_url('category/'.$category->slug), 'name'=>$category->name]);
            $this->trace_categories($category->parent_id);
        }
    }

    public function trace_pages($id)
    {
        if(isset($this->CI->pages['all'][$id]))
        {
            $page = $this->CI->pages['all'][$id];
            array_unshift($this->breadcrumbs, ['link'=>site_url('page/'.$page->slug), 'name'=>$page->title]);
            $this->trace_pages($page->parent_id);
        }
    }

    function generate()
    {
		
        //$type   = $this->uri()->segment(1);
        $slug   = $this->uri()->segment(1);
		$type='category';
        if(!$type || !$slug)
        {
            return; //return blank
        }
        if($type == 'category')
        {
            $category = $this->Categories()->slug($slug);
            if(!$category)
            {
                return;
            }
            $this->trace_categories($category->id);
			die;
        }
        elseif($type == 'product')
        {
            $product = $this->Products()->slug($slug);
            if(!$product)
            {
                return;
            }
            array_unshift($this->breadcrumbs, ['link'=>site_url('product/'.$product->slug), 'name'=>$product->name]);
            $this->trace_categories($product->primary_category);
        }
        elseif($type == 'page')
        {
            $page = $this->Pages()->slug($slug);
            if(!$page)
            {
                return;
            }
            $this->trace_pages($page->id);
        }

        echo GoCart\Libraries\View::getInstance()->get('breadcrumbs', ['breadcrumbs'=>$this->breadcrumbs]);
    }*/
}
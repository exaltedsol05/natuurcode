<?php 
date_default_timezone_set("Asia/Kolkata");
function format_address($fields, $br=false)
{
	
	
	if(empty($fields))
	{
		return ;
	}
	
	// Default format
	$default = '<div class="address_name"><span>{firstname}<small><i class="fa fa-phone" aria-hidden="true"></i>{phone}</small></span></div><div class="address_details">{address1}, {landmark}, {city}, {country}, {zone_name} - <b>{zip}</b></div>';
	
	// Fetch country record to determine which format to use
	$CI = &get_instance();
	$CI->load->model('location_model');
	$c_data = $CI->location_model->get_country($fields['country_id']);
	
	if(empty($c_data->address_format))
	{
		$formatted	= $default;
	} else {
		$formatted	= $c_data->address_format;
	}

	$keys = preg_split("/[\s,{}]+/", $formatted);
	foreach ($keys as $id=>$key)
	{
		
		$formatted = array_key_exists($key, $fields) ? str_replace('{'.$key.'}', $fields[$key], $formatted) : str_replace('{'.$key.'}', '', $formatted);
	}
	
	// remove any extra new lines resulting from blank company or address line
	$formatted		= preg_replace('`[\r\n]+`',"\n",$formatted);
	if($br)
	{
		$formatted	= nl2br($formatted);
	}
	return $formatted;
	
}
									function timeAgo($date) {
	$timestamp = strtotime($date);	
	$strTime = array("second", "minute", "hour", "day", "month", "year");
	$length = array("60","60","24","30","12","10");
	$currentTime = time();
	if($currentTime >= $timestamp) {
		$diff     = time()- $timestamp;
		for($i = 0; $diff >= $length[$i] && $i < count($length)-1; $i++) {
			$diff = $diff / $length[$i];
		}
		$diff = round($diff);
		return $diff . " " . $strTime[$i] . "(s) ago ";
	}
}
/*function format_currency($value, $symbol=true)
{
	$fmt = numfmt_create( config_item('locale'), NumberFormatter::CURRENCY );
	
	return numfmt_format_currency($fmt, $value, config_item('currency_iso'));
}*/
<?php
Class Customer_model extends CI_Model
{

    var $CI;

    function __construct()
    {
        parent::__construct();
       
        $this->CI =& get_instance();
        $this->CI->load->database(); 
        $this->CI->load->helper('url');
		
		
    }
    
    function get_customers($limit=0, $offset=0, $order_by='id', $direction='DESC')
    {
        $this->db->order_by($order_by, $direction);
        if($limit>0)
        {
            $this->db->limit($limit, $offset);
        }

        $result = $this->db->get('customers');
        return $result->result();
    }
    
    function count_customers()
    {
		
        return $this->db->count_all_results('customers');
    }
    
    function get_customer($id)
    {
        
        $result = $this->db->get_where('customers', array('id'=>$id));
        return $result->row();
    }
    
    function get_subscribers()
    {
        $this->db->where('email_subscribe','1');
        $res = $this->db->get('customers');
        return $res->result_array();
    }
    
    function get_address_list($id)
    {
        $addresses = $this->db->where('customer_id', $id)->get('customers_address_bank')->result_array();
        // unserialize the field data
        if($addresses)
        {
            foreach($addresses as &$add)
            {
                $add['field_data'] = unserialize($add['field_data']);
            }
        }
        
        return $addresses;
    }
    
    function get_address($address_id)
    {
        $address= $this->db->where('id', $address_id)->get('customers_address_bank')->row_array();
        if($address)
        {
            $address_info           = unserialize($address['field_data']);
            $address['field_data']  = $address_info;
            $address                = array_merge($address, $address_info);
        }
        return $address;
    }
    
    function save_address($data)
    {
        // prepare fields for db insertion
        $data['field_data'] = serialize($data['field_data']);
        // update or insert
        if(!empty($data['id'])){
            $this->db->where('id', $data['id']);
            $this->db->update('customers_address_bank', $data);
            return $data['id'];
        } else {
            $this->db->insert('customers_address_bank', $data);
            return $this->db->insert_id();
        }
    }
    
    function delete_address($id, $customer_id){
        $this->db->where(array('id'=>$id, 'customer_id'=>$customer_id))->delete('customers_address_bank');
        return $id;
    }
    
    function save($customer){
        if ($customer['id']){
            $this->db->where('id', $customer['id']);
            $this->db->update('customers', $customer);
            return $customer['id'];
        }
        else{
            $this->db->insert('customers', $customer);
            return $this->db->insert_id();
        }
    }
    
    function deactivate($id){
        $customer   = array('id'=>$id, 'active'=>0);
        $this->save_customer($customer);
    }
    
    function delete($id){
        /*
        deleting a customer will remove all their orders from the system
        this will alter any report numbers that reflect total sales
        deleting a customer is not recommended, deactivation is preferred
        */
        
        //this deletes the customers record
        $this->db->where('id', $id);
        $this->db->delete('customers');
        
        // Delete Address records
        $this->db->where('customer_id', $id);
        $this->db->delete('customers_address_bank');
        
        //get all the orders the customer has made and delete the items from them
        $this->db->select('id');
        $result = $this->db->get_where('orders', array('customer_id'=>$id));
        $result = $result->result();
        foreach ($result as $order){
            $this->db->where('order_id', $order->id);
            $this->db->delete('order_items');
        }
        
        //delete the orders after the items have already been deleted
        $this->db->where('customer_id', $id);
        $this->db->delete('orders');
    }
    
    function check_email($str, $id=false)
    {
        $this->db->select('email');
        $this->db->from('customers');
        $this->db->where('email', $str);
        if ($id)
        {
            $this->db->where('id !=', $id);
        }
        $count = $this->db->count_all_results();
        
        if ($count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    
    /*
    these functions handle logging in and out
    */
    function logout()
    {
        $this->CI->session->unset_userdata('customer');
        //force expire the cookie
        $this->generateCookie('[]', time()-3600);
        $this->natuur->destroy(false);
    }

    private function generateCookie($data, $expire)
    {
        setcookie('natuurCustomer', $data, $expire, '/', $_SERVER['HTTP_HOST']);
    }
    
    function chk_user($userVal){
        $sql="SELECT * FROM customers 
            WHERE (email ='$userVal' OR phone='$userVal') AND active=1 ";
        $result = $this->db->query($sql);
        return $result->row();
	}
    function chk_user_with_otp($userVal,$userotp){
        $sql="SELECT * FROM customers 
            WHERE (email ='$userVal' OR phone='$userVal') AND forgot_password_otp='$userotp' AND active=1 ";
        $result = $this->db->query($sql);
        return $result->row();
	}
	function login($email, $password, $remember=false){
        $sql="SELECT * FROM customers 
            WHERE (email ='$email' OR phone='$email') AND password='".sha1($password)."'  AND active=1 ";
        $result = $this->db->query($sql);
        $customer   = $result->row_array();
	  
        if ($customer){
			$custid = $customer['id'];
			
            // Retrieve customer addresses
            $this->db->where(array('customer_id'=>$customer['id'], 'id'=>$customer['default_billing_address']));
            $address = $this->db->get('customers_address_bank')->row_array();
            if($address){
                $fields = unserialize($address['field_data']);
                $customer['bill_address'] = $fields;
                $customer['bill_address']['id'] = $address['id']; // save the addres id for future reference
            }
            
            $this->db->where(array('customer_id'=>$customer['id'], 'id'=>$customer['default_shipping_address']));
            $address = $this->db->get('customers_address_bank')->row_array();
            if($address){
                $fields = unserialize($address['field_data']);
                $customer['ship_address'] = $fields;
                $customer['ship_address']['id'] = $address['id'];
            } else {
                 $customer['ship_to_bill_address'] = 'true';
            }
            
            if($remember){
                $loginCred = json_encode(array('email'=>$email, 'password'=>$password));
                $loginCred = base64_encode($this->aes256Encrypt($loginCred));
                //remember the user for 6 months
                $this->generateCookie($loginCred, strtotime('+6 months'));
            }
            
            // put our customer in the cart
            $this->natuur->save_customer($customer);
            return true;
        }else{
            return false;
        }
    }
	function ajax_login($email, $password){
        $sql="SELECT * FROM customers 
            WHERE (email ='$email' OR phone='$email') AND password='".sha1($password)."'  AND active=1 ";
        $result = $this->db->query($sql);
        $customer   = $result->row_array();
	  
        if ($customer){
			$custid = $customer['id'];
			
            // Retrieve customer addresses
            $this->db->where(array('customer_id'=>$customer['id'], 'id'=>$customer['default_billing_address']));
            $address = $this->db->get('customers_address_bank')->row_array();
            if($address){
                $fields = unserialize($address['field_data']);
                $customer['bill_address'] = $fields;
                $customer['bill_address']['id'] = $address['id']; // save the addres id for future reference
            }
            
            $this->db->where(array('customer_id'=>$customer['id'], 'id'=>$customer['default_shipping_address']));
            $address = $this->db->get('customers_address_bank')->row_array();
            if($address){
                $fields = unserialize($address['field_data']);
                $customer['ship_address'] = $fields;
                $customer['ship_address']['id'] = $address['id'];
            } else {
                 $customer['ship_to_bill_address'] = 'true';
            }
            
            if($remember){
                $loginCred = json_encode(array('email'=>$email, 'password'=>$password));
                $loginCred = base64_encode($this->aes256Encrypt($loginCred));
                //remember the user for 6 months
                $this->generateCookie($loginCred, strtotime('+6 months'));
            }
            
            // put our customer in the cart
            $this->natuur->save_customer($customer);
            return true;
        }else{
            return false;
        }
    }
	
    function socialLogin($social_email=false,$social_phone=false)
    {
        
        $this->db->select('*');
        $this->db->from('customers');
        if($social_email)
        {
            $this->db->where('email',$social_email);
        }
        if($social_phone)
        {
            $this->db->where('phone',$social_phone);
        }
        $result = $this->db->get();
        $customer   = $result->row_array();
        if ($customer)
        {
            // Retrieve customer addresses
            $this->db->where(array('customer_id'=>$customer['id'], 'id'=>$customer['default_billing_address']));
            $address = $this->db->get('customers_address_bank')->row_array();
            if($address)
            {
                $fields = unserialize($address['field_data']);
                $customer['bill_address'] = $fields;
                $customer['bill_address']['id'] = $address['id']; // save the addres id for future reference
            }
            
            $this->db->where(array('customer_id'=>$customer['id'], 'id'=>$customer['default_shipping_address']));
            $address = $this->db->get('customers_address_bank')->row_array();
            if($address)
            {
                $fields = unserialize($address['field_data']);
                $customer['ship_address'] = $fields;
                $customer['ship_address']['id'] = $address['id'];
            } else {
                 $customer['ship_to_bill_address'] = 'true';
            }
            
            
            // Set up any group discount 
            /*if($customer['group_id']!=0) 
            {
                $group = $this->get_group($customer['group_id']);
                if($group) // group might not exist
                {
                    $customer['group'] = $group;
                }
            }*/
            
            if($remember)
            {
                $loginCred = json_encode(array('email'=>$email, 'password'=>$password));
                $loginCred = base64_encode($this->aes256Encrypt($loginCred));
                //remember the user for 6 months
                $this->generateCookie($loginCred, strtotime('+6 months'));
            }
            
            
            
            // put our customer in the cart
            
            $this->natuur->save_customer($customer);

       
            return true;
        }
        else
        {
            
            return false;
            
        }
    }
    
    function is_logged_in($redirect = false, $default_redirect = 'secure/login/')
    {
        
        //$redirect allows us to choose where a customer will get redirected to after they login
        //$default_redirect points is to the login page, if you do not want this, you can set it to false and then redirect wherever you wish.
       
         $customer = $this->natuur->customer();
		//print_r($customer);
        if (!isset($customer['id']))
        {
			//echo "asas123";
			//die;
            //check the cookie
            if(isset($_COOKIE['natuurCustomer']))
            {
				
                //the cookie is there, lets log the customer back in.
                $info = $this->aes256Decrypt(base64_decode($_COOKIE['natuurCustomer']));
                $cred = json_decode($info, true);

                if(is_array($cred))
                {
                    if( $this->login($cred['email'], $cred['password']) )
                    {
                        return $this->is_logged_in($redirect, $default_redirect);
                    }
                }
            }

            //this tells gocart where to go once logged in
            if ($redirect)
            {
                $this->session->set_flashdata('redirect', $redirect);
            }
            
            if ($default_redirect)
            {   
                redirect($default_redirect);
            }
            
            return false;
        }
        else
        {
			//echo "asas";
			//die;
            return true;
        }
    }
    
    function reset_password($email)
    {
        $this->load->library('encrypt');
        $customer = $this->get_customer_by_email($email);
        if ($customer)
        {
            $this->load->helper('string');
            $this->load->library('email');
			$config['mailtype'] = 'html';		
			$this->email->initialize($config);
            
            $new_password       = random_string('alnum', 8);
            $customer['password']   = sha1($new_password);
            //$this->save($customer);
			$sql = "update customers set password='".sha1($new_password)."' WHERE email='".$email."'";
			$this->db->query($sql);
						
			$message = '<p>To create your new password, please click on given link below:<br>
					<a href="'.base_url().'secure/update_password?email='.base64_encode($email).'&pass='.$new_password.'" target="_blank">'.base_url().'secure/update_password?email='. base64_encode($email) .'</a></p>';
            
			
            $this->email->from($this->config->item('email'), $this->config->item('company_name'));
            $this->email->to($email);
			$this->email->bcc($this->config->item('email'));
            $this->email->subject($this->config->item('company_name').': Password Reset');
            $this->email->message(html_entity_decode($message));
            $this->email->send();
            
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function get_customer_by_email($email)
    {
        $result = $this->db->get_where('customers', array('email'=>$email));
        return $result->row_array();
    }

    private function aes256Encrypt($data)
    {
        $key = config_item('encryption_key');
        if(32 !== strlen($key))
        {
            $key = hash('SHA256', $key, true);
        }
        $padding = 16 - (strlen($data) % 16);
        $data .= str_repeat(chr($padding), $padding);
        return mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_CBC, str_repeat("\0", 16));
    }
    private function aes256Decrypt($data)
    {
        $key = config_item('encryption_key');
        if(32 !== strlen($key))
        {
            $key = hash('SHA256', $key, true);
        }
        $data = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_CBC, str_repeat("\0", 16));
        $padding = ord($data[strlen($data) - 1]); 
        return substr($data, 0, -$padding); 
    }

    // Customer groups functions
    function get_groups()
    {
        return $this->db->get('customer_groups')->result();     
    }
    
    function get_group($id)
    {
        return $this->db->where('id', $id)->get('customer_groups')->row();      
    }
    
    function delete_group($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('customer_groups');
    }
    
    function save_group($data)
    {
        if(!empty($data['id'])) 
        {
            $this->db->where('id', $data['id'])->update('customer_groups', $data);
            return $data['id'];
        } else {
            $this->db->insert('customer_groups', $data);
            return $this->db->insert_id();
        }
    }
    function saveNewsLetter($data)
    {
        //calling from
        //controller(function)-->secure(newsletterEmail)
        return $this->db->insert('newsletter_email', $data);
    }
	function getNewsLetter()
    {
        $this->db->select('*');
		$this->db->where('enable','1');
		$results =  $this->db->get('newsletter_email')->result();
		return $results;
    }
	function deleteNewsLetter($id)
    {
		$this->db->where('id', $id);
		return $this->db->delete('newsletter_email');
    }
    
	function check_newsletter($email)
    {
        //calling from
        //controller(function)-->secure(newsletterEmail)
        $this->db->select('email');
        $this->db->from('newsletter_email');
        $this->db->where('email', $email);
        $count = $this->db->count_all_results();
 
        if ($count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    //update customer profile image
    function updateProfileImage($id,$filename)
    {
        $data = array('image' => $filename);
        $this->db->where('id', $id);
        return $this->db->update('customers', $data);
    }
    //save product request in database
    function saveProductRequest($product)
    {
       
        $this->db->insert('product_request', $product);
        return $this->db->insert_id();
    }
	function updateCustomerAdd($cust_id,$my_data)
	{
        $this->db->where('id', $cust_id);
        $this->db->update('customers', $my_data);
		echo $this->db->last_query();
	}
}
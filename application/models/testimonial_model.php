<?php
Class Testimonial_model extends CI_Model
{	

	function getTestimonialList()
	{
		$this->db->select('testimonial.*');
		$this->db->from('testimonial');
		//$this->db->join('customers','customers.email = testimonial.email_id');
		$this->db->where('testimonial.status','1');
		$result = $this->db->get();
		return $result->result();
	}
	function getBlogList()
	{
		$this->db->select('*');
		$this->db->from('blog');
		$this->db->where('blog.status','1');
		$result = $this->db->get();
		return $result->result();
	}
	function getBlogDetails($blog_id)
	{
		$this->db->select('*');
		$this->db->from('blog');
		$this->db->where('blog.status','1');
		$this->db->where('blog.id',$blog_id);
		$result = $this->db->get();
		return $result->row();
	}
	
}
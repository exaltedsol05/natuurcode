<?php
Class order_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	function get_gross_monthly_sales($year)
	{
		$this->db->select('SUM(coupon_discount) as coupon_discounts');
		$this->db->select('SUM(gift_card_discount) as gift_card_discounts');
		$this->db->select('SUM(subtotal) as product_totals');
		$this->db->select('SUM(shipping) as shipping');
		$this->db->select('SUM(tax) as tax');
		$this->db->select('SUM(total) as total');
		$this->db->select('YEAR(ordered_on) as year');
		$this->db->select('MONTH(ordered_on) as month');
		$this->db->group_by(array('MONTH(ordered_on)'));
		$this->db->order_by("ordered_on", "desc");
		$this->db->where('YEAR(ordered_on)', $year);
		
		return $this->db->get('orders')->result();
	}
	
	function get_sales_years()
	{
		$this->db->order_by("ordered_on", "desc");
		$this->db->select('YEAR(ordered_on) as year');
		$this->db->group_by('YEAR(ordered_on)');
		$records	= $this->db->get('orders')->result();
		$years		= array();
		foreach($records as $r)
		{
			$years[]	= $r->year;
		}
		return $years;
	}
	
	function get_orders($search=false, $sort_by='', $sort_order='DESC', $limit=0, $offset=0)
	{			
		if ($search)
		{
			if(!empty($search->term))
			{
				//support multiple words
				$term = explode(' ', $search->term);

				foreach($term as $t)
				{
					$not		= '';
					$operator	= 'OR';
					if(substr($t,0,1) == '-')
					{
						$not		= 'NOT ';
						$operator	= 'AND';
						//trim the - sign off
						$t		= substr($t,1,strlen($t));
					}

					$like	= '';
					$like	.= "( `order_number` ".$not."LIKE '%".$t."%' " ;
					$like	.= $operator." `bill_firstname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `bill_lastname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `ship_firstname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `ship_lastname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `status` ".$not."LIKE '%".$t."%' ";
					$like	.= $operator." `notes` ".$not."LIKE '%".$t."%' )";

					$this->db->where($like);
				}	
			}
			if(!empty($search->start_date))
			{
				$this->db->where('ordered_on >=',$search->start_date);
			}
			if(!empty($search->end_date))
			{
				//increase by 1 day to make this include the final day
				//I tried <= but it did not function. Any ideas why?
				$search->end_date = date('Y-m-d', strtotime($search->end_date)+86400);
				$this->db->where('ordered_on <',$search->end_date);
			}
			
		}
		
		if($limit>0)
		{
			$this->db->limit($limit, $offset);
		}
		if(!empty($sort_by))
		{
			$this->db->order_by($sort_by, $sort_order);
		}
		
		return $this->db->get('orders')->result();
	}
	
	function get_orders_count($search=false)
	{			
		if ($search)
		{
			if(!empty($search->term))
			{
				//support multiple words
				$term = explode(' ', $search->term);

				foreach($term as $t)
				{
					$not		= '';
					$operator	= 'OR';
					if(substr($t,0,1) == '-')
					{
						$not		= 'NOT ';
						$operator	= 'AND';
						//trim the - sign off
						$t		= substr($t,1,strlen($t));
					}

					$like	= '';
					$like	.= "( `order_number` ".$not."LIKE '%".$t."%' " ;
					$like	.= $operator." `bill_firstname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `bill_lastname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `ship_firstname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `ship_lastname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `status` ".$not."LIKE '%".$t."%' ";
					$like	.= $operator." `notes` ".$not."LIKE '%".$t."%' )";

					$this->db->where($like);
				}	
			}
			if(!empty($search->start_date))
			{
				$this->db->where('ordered_on >=',$search->start_date);
			}
			if(!empty($search->end_date))
			{
				$this->db->where('ordered_on <',$search->end_date);
			}
			
		}
		
		return $this->db->count_all_results('orders');
	}

	
	
	//get an individual customers orders
	function get_customer_orders($id, $offset=0)
	{
		$this->db->join('order_items', 'orders.id = order_items.order_id');
		$this->db->order_by('ordered_on', 'DESC');
		return $this->db->get_where('orders', array('customer_id'=>$id), 15, $offset)->result();
	}
	
	function count_customer_orders($id)
	{
		$this->db->where(array('customer_id'=>$id));
		return $this->db->count_all_results('orders');
	}
	
	function get_order($id)
	{
		$this->db->where('id', $id);
		$result 			= $this->db->get('orders');
		
		$order				= $result->row();
		$order->contents	= $this->get_items($order->id);
		
		return $order;
	}
	
	function get_items($id)
	{
		$this->db->select('order_id, contents');
		$this->db->where('order_id', $id);
		$result	= $this->db->get('order_items');
		
		$items	= $result->result_array();
		
		$return	= array();
		$count	= 0;
		foreach($items as $item)
		{

			$item_content	= unserialize($item['contents']);
			
			//remove contents from the item array
			unset($item['contents']);
			$return[$count]	= $item;
			
			//merge the unserialized contents with the item array
			$return[$count]	= array_merge($return[$count], $item_content);
			
			$count++;
		}
		return $return;
	}
	
	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('orders');
		
		//now delete the order items
		$this->db->where('order_id', $id);
		$this->db->delete('order_items');
	}
	
	function save_order($data, $contents = false){
		$this->load->library('session');
		$shipment_chrg              = $this->session->userdata('shipment_chrg');
		
		if(!empty($shipment_chrg)){
			$data['shipping']			= $shipment_chrg;
		}
		
		if (isset($data['id'])){
			$this->db->insert('order_status', 
				array(
					'order_id'=>$data['id'], 
					'notes'=>$data['notes'], 
					'status' => $data['status'])
				);
			$id	= $this->db->insert_id();
			$this -> db -> last_query();
			
			$this->db->where('id', $data['id']);
			$this->db->update('orders', $data);
			$id = $data['id'];
			
			// we don't need the actual order number for an update
			$order_number = $id;
		}else{
			$this->db->insert('orders', $data);
			$id = $this->db->insert_id();
			$int_id = (int)$id -1;
			//create a unique order number
			//unix time stamp + unique id of the order just submitted.
			
			
		    $ctime = date('d/m/Y', time());
			
			//$ctime='16/09/2016';
			$temp = explode("/", $ctime);
			
			$result	= $this->db->get_where('orders', array('id'=> $int_id))->row();
			if(!empty($result -> order_number)){
				$last_order = explode("-", $result -> order_number);
				$lastno = (int)$last_order[2];
				if($lastno <9999 || empty($lastno)){
					$lastno = $lastno+1;
				}else{
					$lastno = 1;
				}
				$temp_order_no = str_pad($lastno, 4, '0', STR_PAD_LEFT);
			}else{
				$lastno = 1;
				$temp_order_no = str_pad($lastno, 4, '0', STR_PAD_LEFT);
			}
			
			$order	= array('order_number'=> 'NMORDR-'.$temp[2].$temp[1].$temp[0]."-".$temp_order_no);//date('U').$id);
			
			//update the order with this order id
			$this->db->where('id', $id);
			$this->db->update('orders', $order);
						
			//return the order id we generated
			$order_number = $order['order_number'];
		}
		
		//if there are items being submitted with this order add them now
		if($contents){
			// clear existing order items
			$this->db->where('order_id', $id)->delete('order_items');
			// update order items
			foreach($contents as $item){
				$save				= array();
				$save['contents']	= $item;
				$item				= unserialize($item);
				$save['product_id'] = $item['id'];
				$save['quantity'] 	= $item['quantity'];
				$save['order_id']	= $id;
				$this->db->insert('order_items', $save);
			}
		}
		return $order_number;
	}
	
	
	function get_best_sellers($start, $end){
		if(!empty($start)){
			$this->db->where('ordered_on >=', $start);
		}
		
		if(!empty($end)){
			$this->db->where('ordered_on <',  $end);
		}
		
		// just fetch a list of order id's
		$orders	= $this->db->select('id')->get('orders')->result();
		
		$items = array();
		foreach($orders as $order){
			// get a list of product id's and quantities for each
			$order_items	= $this->db->select('product_id, quantity')->where('order_id', $order->id)->get('order_items')->result_array();
			
			foreach($order_items as $i){
				
				if(isset($items[$i['product_id']]))
				{
					$items[$i['product_id']]	+= $i['quantity'];
				}
				else
				{
					$items[$i['product_id']]	= $i['quantity'];
				}

			}
		}
		arsort($items);

		// don't need this anymore
		unset($orders);

		$return	= array();
		foreach($items as $key=>$quantity)
		{
			$product				= $this->db->where('id', $key)->get('products')->row();
			if($product)
			{
				$product->quantity_sold	= $quantity;
			}
			else
			{
				$product = (object) array('sku'=>'Deleted', 'name'=>'Deleted', 'quantity_sold'=>$quantity);
			}

			$return[] = $product;
		}

		return $return;
	}

	//funciton add on july 11
	function get_order_status($order_id){
		$this->db->select('*');
        $this->db->from('order_status');
        $this->db->where('order_id', $order_id);
		$this->db->order_by('id', 'desc');
        return $order_history	= $this->db->get()->result();
	}

	function change_reward_points_status($customerid, $status, $change_amt){
		$result		= $this->db->get_where('user_reward_points', array('user_id'=> $customerid))->row();
		$earn 		= $result -> earn_points;
		$pending	=  $result -> pending_points;

		if($status == "Delivered"){
			$newearn = $earn + $change_amt;
			$newpending = 0;
			if($pending>$change_amt){
				$newpending = $pending-$change_amt;
			}

			$this->db->where('user_id',$customerid);
			$this->db->update('user_reward_points', array('earn_points' => $newearn, 'pending_points' => $newpending));
			return "--".$this -> db -> last_query();
		}else{
			$newearn = 0;
			if($earn>$change_amt){
				$newearn = $earn-$change_amt;
			}
			$newpending = $pending + $change_amt;

			$this->db->where('user_id', $customerid);
			$this->db->update('user_reward_points', array('earn_points' => $newearn, 'pending_points' => $newpending));
			return "**".$this -> db -> last_query();
		}
	}

	// function find_old_status($orderid){
	// 	$this->db->select('*');
	// 	$this->db->from('order_status');
	// 	$this->db->where('order_id', $orderid);
	// 	$query = $this->db->get();
	// 	return $status = $query->row()->status;
	// }

	function find_old_status($orderid){
		$this->db->select('*');
		$this->db->from('orders');
		$this->db->where('order_number', $orderid);
		$query = $this->db->get();
		return $status = $query->row();
	}
	
	function get_setting_info(){
		$this->db->select('*');
        $this->db->from('settings');
        $setting_data = $this->db->get()->result();
		$setting_info = array();
		foreach($setting_data as $setting){
			$key = $setting->setting_key;
			$setting_info[$key] = $setting;
		}
		return $setting_info;
	}
	
	function order_date($orderid)	{			
		$this->db->select('ordered_on');		
		$this->db->from('orders');		
		$this->db->where('order_number', $orderid);		
		$query = $this->db->get();		
		$orderdate = $query->row();		
		return $orderdate->ordered_on;			
	}
	function get_most_bought_customer_list()
	{
		$this->db->select('SUM(subtotal) AS total,cust.email,cust.firstname,cust.lastname');
		$this->db->from('orders AS ord');
		$this->db->join('customers AS cust','cust.id = ord.customer_id','LEFT');
		$this->db->group_by('customer_id');
		$this->db->limit(10,0);
		$result = $this->db->get();
		
		return $result->result();
		
	}
	
}
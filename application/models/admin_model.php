<?php
Class Admin_model extends CI_Model
{
	
	function save($save)
	{
	   
		if ($save['id'])
		{
			$this->db->where('id', $save['id']);
			$this->db->update('admin', $save);

			$id	= $save['id'];
		}
		else
		{
			$this->db->insert('admin', $save);
			$id	= $this->db->insert_id();
		}
       
		
		
		//return the product id
		return $id;
	}
	
	function check_email($str)
    {
        $this->db->select('email');
        $this->db->from('admin');
        $this->db->where('email', $str);
        
        $count = $this->db->count_all_results();
        
        if ($count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
	function get_admin($id)
    {
        return $this->db->get_where('admin', array('id'=>$id))->row();
    }
	
	function verifyEmailAddress($verificationcode){  
		$sql = "update admin set status='1' WHERE email_verification_code=?";
		$this->db->query($sql, array($verificationcode));
		return $this->db->affected_rows(); 
	}
	function getTestimonialList()
	{
		$this->db->select('testimonial.*,customers.firstname,customers.lastname,customers.image');
		$this->db->from('testimonial');
		$this->db->join('customers','customers.email = testimonial.email_id','LEFT');
		$result = $this->db->get();
		return $result->result();
	}
	function changeTestimonialStatus($id,$status)
    {
        $data = array(
               'status' => $status
        );
        $this->db->where('id', $id);
        return $this->db->update('testimonial', $data);
    }
    function updateTesimonial($data,$test_id)
    {
        $this->db->where('id', $test_id);
        return $this->db->update('testimonial', $data);
    }
	function inserTestimonial($data)
    {
        return $this->db->insert('testimonial', $data);
    }
    function delete_testimonial($id)
    {
    	$this->db->where('id', $id);
        return $this->db->delete('testimonial');
    }
    function testimonialDetails($test_id)
    {
        $this->db->select('*');
        $this->db->from('testimonial');
        $this->db->where('id',$test_id);
        $result = $this->db->get();
        return $result->row();
    }
	
	//dashboard work
	function today_order(){
		$this->db->select('*');
        $this->db->from('orders');
       	$this->db->where('ordered_on > ', date('Y/m/d', time()));
        return $count = $this->db->count_all_results();
	    //$this->db->last_query();
	}
	
	function total_order(){
		$this->db->select('*');
        $this->db->from('orders');
        return $count = $this->db->count_all_results();
	}
	
	function total_customers(){
		$this->db->select('*');
        $this->db->from('customers');
		$result = $this->db->get();
        return $result->result();
	}
	
	function total_product_request(){
		$this->db->select('*');
        $this->db->from('product_request');
        return $count = $this->db->count_all_results();
	}
	
	function recent_order(){
		$this->db->select('*');
        $this->db->from('orders');
        $this->db->order_by('id', 'desc');
		$this->db->limit(10, 0);
		return $result	= $this->db->get()->result();
	}
	
	function recent_product(){
		$this->db->select('*');
        $this->db->from('products');
        $this->db->order_by('id', 'desc');
		$this->db->limit(10, 0);
		return $result	= $this->db->get()->result();
	}
	
	function recent_customers(){
		$this->db->select('*');
        $this->db->from('customers');
        $this->db->order_by('id', 'desc');
		$this->db->limit(10, 0);
		return $result	= $this->db->get()->result();
	}	
	
	function weekly_order(){
		for($i=0; $i<=6; $i++){
			$months[] = date("m-d", strtotime( date()." -$i days"));
		}
		
		$output = array(array('Month', "Total Amount"));
		
		foreach($months as $mnth){
			$tempt = explode("-", $mnth);
			$this->db->select('COUNT(id) as no_records, ordered_on, sum(total) as total, sum(reward_amt) as reward_amt, sum(shipping) as shipping');
			$this->db->from('orders');
			$this->db->where('MONTH(ordered_on) =  '.$tempt[0].' AND DAY(ordered_on) =  '.$tempt[1]);
			$result	= $this->db->get()->row();
			//echo $this->db->last_query();
			
			$etotal = $result->total;
			if($result->reward_amt>0){
				$etotal = $etotal - ($result->reward_amt/100);
			}
			
			if($result->shipping > 0){
				$etotal = $etotal + $result->shipping;
			}
			
			if(empty($etotal)){
				$etotal = 0;
			}
			$output[]	= array((string)$tempt[1].", ".$tempt[0], (int)$etotal);
		}		
		return json_encode($output);
	}
	
	function yearly_order(){
		for ($i = 0; $i <= 11; $i++) {
    		$months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
		}

		$ouput = array(array('Month', "Total Amount"));
		foreach($months as $mnth){
			$tempt = explode("-", $mnth);
			$this->db->select('COUNT(id) as no_records, MONTH(ordered_on) as month, YEAR(ordered_on) as year, sum(total) as total, sum(reward_amt) as reward_amt, sum(shipping) as shipping');
			$this->db->from('orders');
			$this->db->where("MONTH(ordered_on) = ".$tempt[1]." AND YEAR(ordered_on) = ".$tempt[0]);
			$result	= $this->db->get()->row();			
			
			$etotal = $result->total;
			if($result->reward_amt>0){
				$etotal = $etotal - ($result->reward_amt/100);
			}
			
			if($result->shipping > 0){
				$etotal = $etotal + $result->shipping;
			}
			
			if(empty($etotal))
				$etotal = 0;
			$ouput[] = array((string)"$tempt[1], $tempt[0]", $etotal);
		}
		return json_encode($ouput);
	}
	
	function log_save($log){
		$this->db->insert('admin_log', $log);
		$id	= $this->db->insert_id();
		
		//replace otp value so that he coud't login again with same otp, or change login status to 1
		$fake_otp = mt_rand(100000,999999);
		$this->db->where('email', $log['admin_email']);
		$this->db->update('admin', array('is_online' => 1, 'temp_otp' =>  $fake_otp));
	}
	
	function get_all_log(){
		$this->db->select('*');
        $this->db->from('admin_log');
        $this->db->order_by('id', 'desc');
		$this->db->limit(50, 0);
		return $result	= $this->db->get()->result();	
	}
	
	function update_logout($logouttime){
		$admin_email = $this->session->userdata['admin_email'];
		
		$this->db->select('*');
        $this->db->where('email',$admin_email);
		$this->db->from('admin');
		$this->db->limit(1);
		$result	= $this->db->get()->row();
		
		$after_logout = array();
		$after_logout['temp_otp']	=	'';
		if(!$result->is_office_employee){
			// if admin is non office user
			$after_logout['status']	=	0;
		}
		
		$this->db->where('email',$admin_email);
		$this->db->update('admin', $after_logout);
		
		$this->db->where('admin_login', $logouttime);
		$this->db->update('admin_log', array('admin_logout' => time()));
	}
	
	function log_reciever_email(){
		$this->db->select('setting');
        $this->db->from('settings');
        $this->db->where('setting_key', 'track_log_email');
		$result	= $this->db->get()->row();
		return $result->setting;
	}
	
	function log_reciever_email_update($emailid){
		$this->db->where('setting_key', "track_log_email");
		$this->db->update('settings', array('setting' => $emailid));
	}
	
	function admin_update_time($post){
		$this->db->where('id', $post['admin_id']);
		$this->db->update('admin', array('stime' => $post['stime'], 'etime' => $post['etime']));
	}
	
	
	function get_all_products(){
		$this->db->select('id');
        $this->db->from('products');
        $this->db->order_by('id', 'desc');
		//$this->db->limit(50, 0);
		return $result	= $this->db->get()->result();	
	}
	
	function get_order_by_status()
	{
		$this->db->select('id,status');
        $this->db->from('orders');
        $result = $this->db->get();
		$orders = $result->result();
		
		$data = array();
		$data['order_placed'] 	= 0;
		$data['pending'] 		= 0;
		$data['processing'] 	= 0;
		$data['shipped'] 		= 0;
		$data['on_hold'] 		= 0;
		$data['cancelled']		= 0;
		$data['delivered'] 		= 0;
		foreach($orders as $order)
		{
			if($order->status == 'Order Placed')
			{
				$data['order_placed'] = $data['order_placed']+1;
			}
			else if($order->status == 'Pending')
			{
				$data['pending'] = $data['pending']+1;
			}
			else if($order->status == 'Processing')
			{
				$data['processing'] = $data['processing']+1;
			}
			else if($order->status == 'Shipped')
			{
				$data['shipped'] = $data['shipped']+1;
			}
			else if($order->status == 'On Hold')
			{
				$data['on_hold'] = $data['on_hold']+1;
			}
			else if($order->status == 'Cancelled')
			{
				$data['cancelled'] = $data['cancelled']+1;
			}
			else if($order->status == 'Delivered')
			{
				$data['delivered'] = $data['delivered']+1;
			}
			
		}
		return $data;
	}
	
	
	//blot section start here
	function getBlogList(){
		$this->db->select('blog.*');
		$this->db->from('blog');
		$result = $this->db->get();
		return $result->result();
	}
	
	function changeBlogStatus($id, $status){
        $data = array(
               'status' => $status
        );
        $this->db->where('id', $id);
        return $this->db->update('blog', $data);
    }
	
    function updateBlog($data,$test_id){
        $this->db->where('id', $test_id);
        return $this->db->update('blog', $data);
    }
	
	function insertBlog($data){
        return $this->db->insert('blog', $data);
    }
	
    function delete_blog($id){
    	$this->db->where('id', $id);
        return $this->db->delete('blog');
    }
	
    function blogDetails($test_id){
        $this->db->select('*');
        $this->db->from('blog');
        $this->db->where('id',$test_id);
        $result = $this->db->get();
        return $result->row();
    }
	
	function save_blog($save){
		if ($save['id']){
			$this->db->where('id', $save['id']);
			$this->db->update('blog', $save);
			$id	= $save['id'];
		}else{
			$this->db->insert('blog', $save);
			$id	= $this->db->insert_id();
		}		
		//return the blog id
		return $id;
	}
	
	function get_all_cat_blogso($cat_id = null){
		$this->db->select('*');
        $this->db->from('blog');
        $this->db->where('status', 1);
		if(!empty($cat_id)){
			$this->db->where('blog_cat', $cat_id);
		}
		$this->db->order_by('id', 'desc');
        $result = $this->db->get()->result();
		
		$temp = array();
		foreach($result as $blog){
			$temp[$blog->blog_cat][] = $blog; 
		}		
		return $temp;
	}
	
	function get_popular_blogs(){
		$this->db->select('*');
        $this->db->from('blog');
		$this->db->where('status', 1);
        $this->db->where('is_popular_blog', '1');
		$this->db->order_by('id', 'desc');
		return $result = $this->db->get()->result();
	}
	
	function get_new_blogs(){
		$this->db->select('*');
        $this->db->from('blog');
		//$this->db->where('status', 1);
        $this->db->where('is_new', 1);
		$this->db->order_by('id', 'desc');
        return $result = $this->db->get()->result();
	}
}
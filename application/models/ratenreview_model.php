<?php
Class Ratenreview_model extends CI_Model{

	//add wishlist functions
	function add_wishlist_table($product_id = 0){
		$cust_id = $this->customer['id'];
		$this->db->select('id');
        $this->db->from('wishlist');
        $this->db->where(array('user_id' => $cust_id, 'product_id' => $product_id));
        $count = $this->db->count_all_results();

		if($count < 1){
			$this->db->insert('wishlist', array('user_id'=>$cust_id, 'product_id'=>$product_id, 'status' => 1));
			$id	= $this->db->insert_id();
			return true;
		}else{
			return false;
		}
	}
	
	function count_user_wishlist(){
		$cust_id = $this->customer['id'];
		$this->db->select('id');
        $this->db->from('wishlist');
        $this->db->where(array('user_id' => $cust_id, 'status' => 1));
        $count = $this->db->count_all_results();
		return $count;
	}

	function get_user_wishlist(){
		$cust_id = $this->customer['id'];
		$this->db->select('product_id');
        $this->db->from('wishlist');
        $this->db->where(array('user_id' => $cust_id, 'status' => 1));
        $this->db->order_by('id', 'desc');
		$result	= $this->db->get()->result();
		return $result;
	}
	
	function get_wish_product_row($wish_product_ids = 0){
		$query = $this->db->query("SELECT * from products WHERE id IN ($wish_product_ids)");
		$product_records  = $query->result();
		return $product_records;
	}

	function delete_from_wishlist($id){
		$cust_id = $this->customer['id'];
		$this->db->delete('wishlist', array('product_id' => $id, 'user_id' => $cust_id));
	}
	
	//compare product
	function get_comp_product_row($comp_product_ids = 0){
		if($comp_product_ids == ''){
			$comp_product_ids = '0';
		}
		$query = $this->db->query("SELECT * from products WHERE id IN ($comp_product_ids)");
		$product_records  = $query->result();
		return $product_records;
	}
	
	function delete_from_compr($id){
		$cust_id = $this->customer['id'];
		$this->db->delete('compare', array('product_id' => $id, 'user_id' => $cust_id));
	}
	
	//recommend functions
	function check_is_recommend($product_id){
		$cust_id = $this->customer['id'];
		$this->db->select('id');
        $this->db->from('recommendation');
        $this->db->where(array('user_id' => $cust_id, 'product_id' => $product_id));
        $count = $this->db->count_all_results();
		if($count < 1){
			return false;
		}else{
			return true;
		}
	}
	
	function add_recommend($product_id, $recommendation_for){
		$cust_id = $this->customer['id'];
		$this->db->insert('recommendation', array('user_id'=>$cust_id, 'product_id'=>$product_id, 'recommendation_for' => $recommendation_for));
		$id	= $this->db->insert_id();	
	}
	
	function count_recommend($prd_id){
		$this->db->select('id');
        $this->db->from('recommendation');
        $this->db->where(array('recommendation_for'=> 1, 'product_id' => $prd_id));
        $yes_rec = $this->db->count_all_results();
		
		$this->db->select('id');
        $this->db->from('recommendation');
        $this->db->where(array('recommendation_for'=> 1, 'product_id' => $prd_id));
        $no_rec = $this->db->count_all_results();
		
		$this->db->select('id');
        $this->db->from('recommendation');
		$this->db->where('product_id', $prd_id);
        $all_rec = $this->db->count_all_results();
		
		return $yes_rec.'*-*'.$no_rec.'*-*'.$all_rec;
	}
	
	//rating funcitons
	function check_is_rate($product_id){
		$cust_id = $this->customer['id'];
		$this->db->select('id');
        $this->db->from('star_rating');
        $this->db->where(array('user_id' => $cust_id, 'product_id' => $product_id));
        $count = $this->db->count_all_results();
		if($count < 1){
			return false;
		}else{
			return true;
		}
	}
	
	function add_rating($product_id, $rating){
		$cust_id = $this->customer['id'];
		$this->db->insert('star_rating', array('user_id'=>$cust_id, 'product_id'=>$product_id, 'rating' => $rating));
		$id	= $this->db->insert_id();
	}
	
	function user_rating($prd_id){
		$cust_id = $this->customer['id'];
		$result	= $this->db->get_where('star_rating', array('user_id'=> $cust_id, 'product_id' => $prd_id))->row();
		return $result -> rating;
	}
	
	function product_rating($prd_id){
		$this->db->select('id');
        $this->db->from('star_rating');
        $this->db->where(array('rating'=> 1, 'product_id' => $prd_id));
        $rate1 = $this->db->count_all_results();
		
		$this->db->select('id');
        $this->db->from('star_rating');
        $this->db->where(array('rating'=> 2, 'product_id' => $prd_id));
        $rate2 = $this->db->count_all_results();
		
		$this->db->select('id');
        $this->db->from('star_rating');
        $this->db->where(array('rating'=> 3, 'product_id' => $prd_id));
        $rate3 = $this->db->count_all_results();
		
		$this->db->select('id');
        $this->db->from('star_rating');
        $this->db->where(array('rating'=> 4, 'product_id' => $prd_id));
        $rate4 = $this->db->count_all_results();
		
		$this->db->select('id');
        $this->db->from('star_rating');
        $this->db->where(array('rating'=> 5, 'product_id' => $prd_id));
        $rate5 = $this->db->count_all_results();
		
		
		$this->db->select('id');
        $this->db->from('star_rating');
		$this->db->where('product_id', $prd_id);
        $rateall = $this->db->count_all_results();
		
		return $rate1.'*-*'.$rate2.'*-*'.$rate3.'*-*'.$rate4.'*-*'.$rate5.'*-*'.$rateall;	
	}
	
	//review functions
	function check_is_review($product_id){
		$cust_id = $this->customer['id'];
		$this->db->select('id');
        $this->db->from('reviews');
        $this->db->where(array('user_id' => $cust_id, 'product_id' => $product_id));
        $count = $this->db->count_all_results();
		if($count < 1){
			return false;
		}else{
			return true;
		}
	}
	
	function add_review($product_id, $review, $review_title){
		$cust_id = $this->customer['id'];
		$this->db->insert('reviews', array('user_id'=>$cust_id, 'product_id'=>$product_id, 'title' => $review_title, 'review' => $review, status => 0));
		$id	= $this->db->insert_id();
	}
	
	function get_approved_reviews(){
		$this->db->select('*');
        $this->db->from('reviews');
        $this->db->where('status', 1);
        $this->db->order_by('id', 'desc');
		$result	= $this->db->get()->result();
		return $result;	
	}
	
	function get_non_approved_reviews(){
		$this->db->select('*');
        $this->db->from('reviews');
        $this->db->where('status', 0);
        $this->db->order_by('id', 'desc');
		$result	= $this->db->get()->result();
		return $result;	
	}
	
	function approve_from_review($id){
		$this->db->where('id', $id);
		$this->db->update('reviews', array('status' => 1));
		//echo $this -> db -> last_query();
		
		$result	= $this->db->get_where('reviews', array('id' => $id))->row();
		
		/*$ecash_setting = $this->ratenreview_model->ecash_setting_data();
		if($ecash_setting[2]->status == 1){
			$this->db->select('id');
			$this->db->from('ecash_track');
			$this->db->where(array('user_id' => $result->user_id, 'product_id' => $result->product_id, 'action' => 'Product Review'));
			$count = $this->db->count_all_results();
			
			if($count < 1){
				$ecash = '';
				$ecash['user_id'] 		= $result->user_id; //user id of user
				$ecash['action'] 		= $ecash_setting[2]->label;
				
				$ecash['product_id'] 	= $result->product_id;	// product id in case of product reivew, vating a poll, like us, share product, 1st referral purchase
				
				$ecash['ecash'] 		= $ecash_setting[2]->ecash;	//ecash points
				$ecash['order_id'] 		= '';	//order id if cash ecash points
				$ecash['tranction'] 	= '1';	// is ecash 1 for add or 0 for withdraw
				//$ecash['date'] 			= '';	// date of tranction
				
				$ecash['status'] 		= '1';	// status of ecash 1 for yes, 0 for delete
				$this->ratenreview_model->track_ecash($ecash);
			}
		}*/		
	}
	
	function non_approve_from_review($id){
		$this->db->where('id', $id);
		$this->db->update('reviews', array('status' => 0));
		//echo $this -> db -> last_query();
	}
	
	function delete_from_review($id){
		$this->db->delete('reviews', array('id' => $id));	
	}
	
	
	function product_title($product_id){
		$result	= $this->db->get_where('products', array('id'=> $product_id))->row();
		return $result -> name;	
	}
	
	function get_rating_users($product_id=0, $star=0){
		$query = "SELECT user_id FROM star_rating WHERE product_id = ".$product_id." AND rating = ".$star;
		$query = $this->db->query($query);
		$result  = $query->result();
		if(empty($result)){
			$user = 0;	
		}else{
			$users = '';
			foreach($result as $rec){
				$users .= $rec->user_id.',';
			}
			$users = rtrim($users, ',');	
		}
		return $users;
	}
	
	function get_product_approved_reviews_recent($product_id, $star){
		if(empty($star)){
			$query = $this->db->query("SELECT a.id,a.user_id,a.product_id,a.title,a.review,a.is_helpful,a.reviewdate,b.firstname,b.lastname,b.image FROM reviews a INNER JOIN customers b ON a.user_id = b.id WHERE a.status = '1' AND a.product_id ='$product_id' ");
			$result = $query->result();

		}else{
			$users = $this -> get_rating_users($product_id, $star);
			if(empty($users)){
				$result = '';
			}else{
				$query = "SELECT * FROM reviews WHERE user_id IN (".$users.") AND product_id = 331 AND status = 1 order by id desc limit 0, 10";
				$query = $this->db->query($query);
				$result  = $query->result();
			}
		}
		return $result;
	}
	
	function get_product_approved_reviews_helpful($product_id, $star){
		if(empty($star)){
			$this->db->select('*');
			$this->db->from('reviews');
			$this->db->where(array('status' => 1, 'product_id' => $product_id));
			$this->db->order_by('is_helpful', 'desc');
			$this->db->limit(10, 0);
			$result	= $this->db->get()->result();
		}else{
			$users = $this -> get_rating_users($product_id, $star);
			if(empty($users)){
				$result = '';
			}else{
				$query = "SELECT * FROM reviews WHERE user_id IN (".$users.") AND product_id = 331 AND status = 1 order by id desc limit 0, 10";
				$query = $this->db->query($query);
				$result  = $query->result();
			}
		}
		return $result;
	}
	
	function count_product_reviews($product_id){
		$this->db->select('id');
        $this->db->from('reviews');
        $this->db->where('product_id', $product_id);
        $count = $this->db->count_all_results();	
		return $count;
	}
	
	function user_details($user_id){
		$this->db->select('*');
        $this->db->from('customers');
        $this->db->where('id', $user_id);
        $result	= $this->db->get()->result();
		return $result;	
	}
	
	function is_helpfule_count($review_id){
		$result	= $this -> db -> get_where('reviews', array('id'=> $review_id))->row();
		return $result;
	}
	
	function update_ishelpfule($review_id){
		$review_data = $this -> is_helpfule_count($review_id);
		$current_helpful = $review_data -> is_helpful;
		$new_helpfule = $current_helpful + 1;
		$this->db->where('id', $review_id);
		$this->db->update('reviews', array('is_helpful' => $new_helpfule));
		return $pruduct_slug = $this -> product_uri($review_data ->  product_id);
	}
	
	function user_reviewrating($product_id, $userid){
		$result	= $this->db->get_where('star_rating', array('user_id'=> $userid, 'product_id' => $product_id))->row();
		return $result -> rating;
	}

	function product_uri($prd_id){
		$result	= $this->db->get_where('products', array('id'=> $prd_id))->row();
		return $result -> slug;
	}

    function get_recent_product(){
		//$session_id = $this->session->userdata(session_id);
	    $this->db->select('*');
        $this->db->from('search');
		$this->db->order_by('id', 'desc');
		$this->db->limit(50, 0);
        //$this->db->where('code', $session_id);
        $result	= $this->db->get()->result();
		$term = '';
		$product_sku = '';

		foreach($result as $row){
			$term = json_decode($row->term, true);
			if(!empty($term)){
				$product_sku .= '"'.$term['term'].'",';
			}			
		}
		$product_sku = rtrim($product_sku, ",");
        $recent_product_data = $this -> recent_products($product_sku);
        return $recent_product_data;
	}

	function recent_products($product_sku){
		if($product_sku)
		{
		$query = 'SELECT * FROM products WHERE sku IN ('.$product_sku.')';
		$query = $this->db->query($query);
		
		$result  = $query->result();
		
		return $result;
		}else{
			return array();
		}
	}
	
	function get_order_details($order_number){
		$order	= $this->db->get_where('orders', array('order_number'=> $order_number))->row();
		return $order;		
	}
	
	function get_product_details($product_id){
		$product= $this->db->get_where('products', array('id'=> $product_id))->row();
		return $product;		
	}
	
	function get_order_items($order_id){
		$this->db->select('*');
        $this->db->from('order_items');
        $this->db->where('order_id', $order_id);
        $order_items	= $this->db->get()->result();
		$order_products = array();
		
		foreach($order_items as $items){
			$prd_info = array();
			$prd_details = $this -> get_product_details($items -> product_id);
			$prd_info['qty'] = $items -> quantity;
			//$prd_info['info'] = $prd_details;
			$prd_info['info'] = (object)unserialize($items->contents);
			$order_products[] = $prd_info;
		}
		return $order_products;
	}
	
	function get_invoice_items($order_id){
		$query = array('order_id'=> $order_id, 'status' => 'Delivered');
		$order_status = $this->db->get_where('order_status', $query)->row();		
		return $order_status -> date;
	}
	
	function get_reward_details($userid = null){
		if(empty($userid)){
			$cust_id = $this->customer['id'];
		}else{
			$cust_id = $userid;
		}
		
		$points = array();
		
		$result	= $this->db->get_where('ecash_balance', array('user_id'=> $cust_id, 'status' => 1))->row();
		//echo $this -> db -> last_query();
		$points['earn'] = $result -> bal;
		$points['pending'] =  $result -> pending;
		return $points;
	}

    function product_group_detail($prd_id){
		$result	= $this->db->get_where('products', array('id'=> $prd_id))->row();
		return $result -> grouped_products;
	}
	
	/* Aug 16 */
	//track ecash tranctions
	function signup_ecash($ecash, $email){
		$this ->  check_is_refer_user($email);
		
		$this->db->insert('ecash_track', $ecash);
		$id	= $this->db->insert_id();	
		
		$ecashbal['user_id'] = $ecash['user_id'];
		$ecashbal['bal'] = $ecash['ecash'];
		$ecashbal['status'] = '1';	
		$this->db->insert('ecash_balance', $ecashbal);
		$id	= $this->db->insert_id();	
	}
	
	function check_is_refer_user($email){
		$this->db->select('*');
        $this->db->from('referall_users');
        $this->db->where(array('email_id' => $email));
		$count = $this->db->count_all_results();
		
		if($count >= 1){
			$ecash_setting = $this->ecash_setting_data();
			if($ecash_setting[7]->status == 1){
				$result	= $this->db->get_where('referall_users', array('email_id' => $email))->row();
				//echo $this -> db->last_query();
				$ecash = '';
				$ecash['user_id'] 		= $result->user_id; //user id of user
				$ecash['action'] 		= $ecash_setting[7]->label." By ".$result -> email_id;
				$ecash['product_id'] 	= '';	
				$ecash['ecash'] 		= $ecash_setting[7]->ecash;	//ecash points
				$ecash['order_id'] 		= '';	//order id if cash ecash points
				$ecash['tranction'] 	= '1';	// is ecash 1 for add or 0 for withdraw
				$ecash['status'] 		= '1';	// status of ecash 1 for yes, 0 for delete			
				$this->track_ecash($ecash);
			}
		}
	}
	//add ecash to user account
	
	function email_user($email){
        $result	= $this->db->get_where('customers', array('email'=> $email))->row();
		return $result -> id;
    }
	
	//dynamic entry into both tables
	function track_ecash($ecash){
		if(!isset($ecash['pending'])){
			$this->db->insert('ecash_track', $ecash);
			$id	= $this->db->insert_id();
		}else{
			$pending['user_id'] = $ecash['user_id'];
			$pending['action'] = 'eCash Pending after placed order';
			$pending['product_id'] = 0;
			$pending['ecash'] = $ecash['pending'];
			$pending['order_id'] = $ecash['order_id'];
			$pending['tranction'] = 2;
			$pending['status'] = 1;
			$this->db->insert('ecash_track', $pending);
			$id	= $this->db->insert_id();
		}
		
		if($ecash['pending']){
			$this->db->set('pending', 'pending + '.$ecash['pending'], FALSE);
			$this->db->where('user_id', $ecash['user_id']);
			$this->db->update('ecash_balance');	
			$this->db->last_query();
		}if($ecash['tranction']){
			$this->db->set('bal', 'bal + '.$ecash['ecash'], FALSE);
			$this->db->where('user_id', $ecash['user_id']);
			$this->db->update('ecash_balance');
		}else {
			$this->db->set('bal', 'bal - '.$ecash['ecash'], FALSE);
			$this->db->where('user_id', $ecash['user_id']);
			$this->db->update('ecash_balance');
		}
	}
	
	function get_user_ecash_track($userid = null){
		if(empty($userid)){
			$cust_id = $this->customer['id'];
		}else{
			$cust_id = $userid;
		}
		$this->db->select('*');
        $this->db->from('ecash_track');
        $this->db->where('user_id', $cust_id);
        $this->db->order_by('id', 'desc');
		$result	= $this->db->get()->result();
		return $result;		
	}
	
	//check user ecash_balance table create or not
	function check_ecash_table(){
		$cust_id = $this->customer['id'];
		$this->db->select('id');
        $this->db->from('ecash_balance');
        $this->db->where(array('user_id' => $cust_id, 'status' => 1));
        $count = $this->db->count_all_results();
		if($count < 1){
			$ecashbal['user_id'] = $cust_id;
			$ecashbal['bal'] = '0';
			$ecashbal['status'] = '1';	
			$this->db->insert('ecash_balance', $ecashbal);
			$id	= $this->db->insert_id();	
		}
	}
	
	//set ecash on current date birthday
	function ecash_event_date($event){
		$this->db->select('*');
        $this->db->from('customers');
		$this->db->where("DAY($event) = DAY(CURDATE()) and MONTH($event) = MONTH(CURDATE())");
		$result	= $this->db->get()->result();
		$this -> db -> last_query();
		$action = '';
		
		$ecash_setting = $this->ratenreview_model->ecash_setting_data();
		
		
		if($event == 'dob'){
			$action = 'On Your Birth Day';	
			$id = '9';
		}else if($event == 'clinic_anniversary'){
			$action = 'On Your Clinic Birthday';
			$id = '10';
		}else if($event == 'marrg_aniversary'){
			$action = 'On Your Marrieage Anniversary';
			$id = '11';
		}else{
			$action = '';
			$id = '';
		}
		
		if($ecash_setting[$id]->status == 1){
			foreach($result as $dobuser){
				$custid =  $cust_id;
				$this->db->select('id');
				$this->db->from('ecash_track');
				$this->db->where(array('user_id' => $dobuser->id, 'status' => 1, 'action' => $action));
				$count = $this->db->count_all_results();
				
				if($count < 1){
					$ecash = '';
					$ecash['user_id'] 		= $dobuser->id; //user id of user
					$ecash['action'] 		= $ecash_setting[$id]->label;
					$ecash['product_id'] 	= '';	// product id in case of product reivew, vating a poll, like us, share product, 1st referral purchase
					$ecash['ecash'] 		= $ecash_setting[$id]->ecash;	//ecash points
					$ecash['order_id'] 		= '';	//order id if cash ecash points
					$ecash['tranction'] 	= '1';	// is ecash 1 for add or 0 for withdraw
					//$ecash['date'] 			= '';	// date of tranction
					$ecash['status'] 		= '1';	// status of ecash 1 for yes, 0 for delete
					
					$this->track_ecash($ecash);
				}else{
					//echo "alreay enter...";
				}
			}
		}	
	}
	
	function set_facebook_share_ecash($product_id){
		$ecash_setting = $this->ratenreview_model->ecash_setting_data();
		
		$cust_id = $this->customer['id'];
		$this->db->select('id');
        $this->db->from('ecash_track');
        $this->db->where(array('user_id' =>$cust_id,'product_id'=>$product_id,'action' =>$ecash_setting[5]->label));
        $count = $this->db->count_all_results();
		$this->db->last_query();
		if($count < 1){
			
			if($ecash_setting[5]->status == 1){
				$ecash = '';
				$ecash['user_id'] 		= $cust_id; //user id of user
				$ecash['action'] 		= $ecash_setting[5]->label;
				$ecash['product_id'] 	= $product_id;	// product id in case of product reivew, vating a poll, like us, share product, 1st referral purchase
				
				$ecash['ecash'] 		= $ecash_setting[5]->ecash;	//ecash points
				$ecash['order_id'] 		= '';	//order id if cash ecash points
				$ecash['tranction'] 	= '1';	// is ecash 1 for add or 0 for withdraw
				//$ecash['date'] 		= '';	// date of tranction
				$ecash['status'] 		= '1';	// status of ecash 1 for yes, 0 for delete
				$this->ratenreview_model->track_ecash($ecash);
			}
		}
	}
	
	
	function set_facebook_like_ecash(){
		$ecash_setting = $this->ratenreview_model->ecash_setting_data();
		$cust_id = $this->customer['id'];
		$this->db->select('id');
        $this->db->from('ecash_track');
        $this->db->where(array('user_id' =>$cust_id, 'action' =>$ecash_setting[4]->label));
        $count = $this->db->count_all_results();
		//$this->db->last_query();
		
		if($count < 1){
			if($ecash_setting[4]->status == 1){
				$ecash = '';
				$ecash['user_id'] 		= $cust_id; //user id of user
				$ecash['action'] 		= $ecash_setting[4]->label;
				$ecash['product_id'] 	= $product_id;	// product id in case of product reivew, vating a poll, like us, share product, 1st referral purchase		
				$ecash['ecash'] 		= $ecash_setting[4]->ecash;	//ecash points
				$ecash['order_id'] 		= '';	//order id if cash ecash points
				$ecash['tranction'] 	= '1';	// is ecash 1 for add or 0 for withdraw
				//$ecash['date'] 		= '';	// date of tranction
				$ecash['status'] 		= '1';	// status of ecash 1 for yes, 0 for delete
				$this->track_ecash($ecash);
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	//set reward points on checkout process
	function earn_on_checkout($checkout){	
		$ecash = '';
		$ecash['user_id'] 		= $checkout['user_id']; //user id of user
		$ecash['action'] 		= "Place Order";
		$ecash['product_id'] 	= '';	
		$ecash['ecash'] 		= $checkout['earn_points'];	//ecash points
		if(!empty($checkout['pending_points'])){
			$ecash['pending'] 		= $checkout['pending_points'];	//ecash points
		}
				
		$ecash['order_id'] 		= $checkout['order_no'];	//order id if cash ecash points
		$ecash['tranction'] 	= '1';	// is ecash 1 for add or 0 for withdraw
		$ecash['status'] 		= '1';	// status of ecash 1 for yes, 0 for delete
		$this->track_ecash($ecash);		
	}
	
	function withdraw_ecash($checkout){
		$ecash = '';
		$ecash['user_id'] 		= $checkout['user_id']; //user id of user
		$ecash['action'] 		= "Withdraw Ecash";
		$ecash['product_id'] 	= '';
		$ecash['ecash'] 		= $checkout['ecash']*100;	//ecash points		
		$ecash['order_id'] 		= $checkout['order_id'];	//order id if cash ecash points
		$ecash['tranction'] 	= '0';	// is ecash 1 for add or 0 for withdraw
		$ecash['status'] 		= '1';	// status of ecash 1 for yes, 0 for delete

		//update order table
		$this->db->where('order_number', $checkout['order_id']);
		$this->db->update('orders', array('reward_amt' => $ecash['ecash']));
		//echo $this->db->last_query();
		
		$this->track_ecash($ecash);
	}
	
	
	function ecash_after_deleivered($save){
	//work on it		
	}
		
	function ecash_mail_refer_save($email){
		$cust_id = $this->customer['id'];
		foreach($email as $single_emails){
			$this->db->insert('referall_users', array('user_id'=>$cust_id, 'email_id'=>$single_emails, 'status' => 0));
			$id	= $this->db->insert_id();
		}		
	}
	
	function ecash_setting_data(){
		$this->db->select('*');
        $this->db->from('ecash_setting');
		$result	= $this->db->get()->result();
		return $result;
	}	
	
	function get_offer_banner(){
		$this->db->select('*');
        $this->db->from('special_banner_collection');
		$result	= $this->db->get()->result();
		return $result;
	}
}
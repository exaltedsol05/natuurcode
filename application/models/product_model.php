<?php
Class Product_model extends CI_Model
{
	
	function product_autocomplete($name, $limit)
	{
		return	$this->db->like('name', $name)->get('products', $limit)->result();
	}
	
	function product_autocomplete_combo($name, $limit)
	{   
		return	$this->db->where('product_type','3')->like('name', $name)->get('products', $limit)->result();
		
	}
	
	
	function products_archive($data=array(), $return_count=false)
	{
		
		
		if(empty($data))
		{
			//if nothing is provided return the whole shabang
			$this->get_all_products();
			
		}
		else
		{
			
			
			//grab the limit
			if(!empty($data['rows']))
			{
				$this->db->limit($data['rows']);
			}
			
			//grab the offset
			if(!empty($data['page']))
			{
				$this->db->offset($data['page']);
			}
			
			//do we order by something other than category_id?
			if(!empty($data['order_by']))
			{
				//if we have an order_by then we must have a direction otherwise KABOOM
				$this->db->order_by($data['order_by'], $data['sort_order']);
			}
			
			$this->db->like('archive', 1);
			
			//do we have a search submitted?
			if(!empty($data['term']))
			{
				$search	= json_decode($data['term']);
				//if we are searching dig through some basic fields
				if(!empty($search->term))
				{
					$this->db->like('name', $search->term);
					$this->db->or_like('description', $search->term);
					$this->db->or_like('excerpt', $search->term);
					$this->db->or_like('sku', $search->term);
				}
				
				if(!empty($search->category_id))
				{
					//lets do some joins to get the proper category products
					$this->db->join('category_products', 'category_products.product_id=products.id', 'right');
					$this->db->where('category_products.category_id', $search->category_id);
					$this->db->order_by('sequence', 'ASC');
				}
			}
			
			if($return_count)
			{
				return $this->db->count_all_results('products');
			}
			else
			{
				return $this->db->get('products')->result();
			}
			
		}
	}
	
	function products_zero_quantity($data=array(), $return_count=false)
	{
		
		
		if(empty($data))
		{
			//if nothing is provided return the whole shabang
			$this->get_all_products();
			
		}
		else
		{
			
			
			//grab the limit
			if(!empty($data['rows']))
			{
				$this->db->limit($data['rows']);
			}
			
			//grab the offset
			if(!empty($data['page']))
			{
				$this->db->offset($data['page']);
			}
			
			//do we order by something other than category_id?
			if(!empty($data['order_by']))
			{
				//if we have an order_by then we must have a direction otherwise KABOOM
				$this->db->order_by($data['order_by'], $data['sort_order']);
			}
			
			$this->db->where('quantity', 0);
			
			//do we have a search submitted?
			if(!empty($data['term']))
			{
				$search	= json_decode($data['term']);
				//if we are searching dig through some basic fields
				if(!empty($search->term))
				{
					$this->db->like('name', $search->term);
					$this->db->or_like('description', $search->term);
					$this->db->or_like('excerpt', $search->term);
					$this->db->or_like('sku', $search->term);
				}
				
				if(!empty($search->category_id))
				{
					//lets do some joins to get the proper category products
					$this->db->join('category_products', 'category_products.product_id=products.id', 'right');
					$this->db->where('category_products.category_id', $search->category_id);
					$this->db->order_by('sequence', 'ASC');
				}
			}
			
			if($return_count)
			{
				return $this->db->count_all_results('products');
			}
			else
			{
				return $this->db->get('products')->result();
			}
			
		}
	}
	
	function products($data=array(), $return_count=false)
	{
		
		
		if(empty($data))
		{
			//if nothing is provided return the whole shabang
			$this->get_all_products();
			
		}
		else
		{
			
			
			//grab the limit
			if(!empty($data['rows']))
			{
				$this->db->limit($data['rows']);
			}
			
			//grab the offset
			if(!empty($data['page']))
			{
				$this->db->offset($data['page']);
			}
			
			//do we order by something other than category_id?
			if(!empty($data['order_by']))
			{
				//if we have an order_by then we must have a direction otherwise KABOOM
				$this->db->order_by($data['order_by'], $data['sort_order']);
			}
			
			$this->db->like('archive', 0);
			
			//do we have a search submitted?
			if(!empty($data['term']))
			{
				$search	= json_decode($data['term']);
				//if we are searching dig through some basic fields
				if(!empty($search->term))
				{
					$this->db->like('name', $search->term);
					$this->db->or_like('description', $search->term);
					$this->db->or_like('excerpt', $search->term);
					$this->db->or_like('sku', $search->term);
				}
				
				if(!empty($search->category_id))
				{
					//lets do some joins to get the proper category products
					$this->db->join('category_products', 'category_products.product_id=products.id', 'right');
					$this->db->where('category_products.category_id', $search->category_id);
					$this->db->order_by('sequence', 'ASC');
				}
			}
			
			if($return_count)
			{
				return $this->db->count_all_results('products');
			}
			else
			{
				return $this->db->get('products')->result();
			}
			
		}
	}
	
	function get_all_products()
	{
		//sort by alphabetically by default
		$this->db->order_by('name', 'ASC');
		$result	= $this->db->get('products');

		return $result->result();
	}
	
	function get_filtered_products($product_ids, $limit = false, $offset = false)
	{
		
		if(count($product_ids)==0)
		{
			return array();
		}
		
		$this->db->select('id, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false)->from('products');
		
		if(count($product_ids)>1)
		{
			$querystr = '';
			foreach($product_ids as $id)
			{
				$querystr .= 'id=\''.$id.'\' OR ';
			}
		
			$querystr = substr($querystr, 0, -3);
			
			$this->db->where($querystr, null, false);
			
		} else {
			$this->db->where('id', $product_ids[0]);
		}
		
		$result	= $this->db->limit($limit)->offset($offset)->get()->result();

		//die($this->db->last_query());

		$contents	= array();
		$count		= 0;
		foreach ($result as $product)
		{

			$contents[$count]	= $this->get_product($product->id);
			$count++;
		}

		return $contents;
		
	}
	
	function get_products($category_id = false, $limit = false, $offset = false, $by=false, $sort=false,$new_tax=false,$minPrice=false,$maxPrice=false,$brand=false)
	{
		//if we are provided a category_id, then get products according to category
		if ($category_id)
		{
			$this->db->select('category_products.*, products.*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false)->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('category_id'=>$category_id, 'enabled'=>1));
			
			if($brand)
			{
				$this->db->where("shop_by_brand IN($brand)");
			}
			if($new_tax)
			{
				$this->db->where($new_tax);
			}
			if($minPrice)
			{
				$this->db->where('saleprice >=',$minPrice);
			}
			if($maxPrice)
			{
				$this->db->where('saleprice <',$maxPrice);
			}
			$this->db->order_by($by, $sort);
			
			if($limit > 0){
				$result	= $this->db->limit($limit)->offset($offset)->get()->result();
			}else{
				$result	= $this->db->get()->result();
			}

			//echo $this->db->last_query(); 
			
			return $result;
		}
		else
		{
			//sort by alphabetically by default
			$this->db->order_by('name', 'ASC');
			$result	= $this->db->get('products');

			return $result->result();
		}
	}
	function get_bestseller_products($category_id, $limit = false, $offset = false, $by=false, $sort=false,$new_tax=false,$minPrice=false,$maxPrice=false,$brand=false)
	{
		//if we are provided a category_id, then get products according to category
		if (!empty($category_id))
		{
			$this->db->select('category_products.*, products.*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false)->from('category_products')->join('products', 'category_products.product_id=products.id')->where_in('category_id', $category_id)->where('enabled', '1');
			
			if($brand)
			{
				$this->db->where("shop_by_brand IN($brand)");
			}
			if($new_tax)
			{
				$this->db->where($new_tax);
			}
			if($minPrice)
			{
				$this->db->where('saleprice >=',$minPrice);
			}
			if($maxPrice)
			{
				$this->db->where('saleprice <',$maxPrice);
			}
			$this->db->order_by($by, $sort);
			
			if($limit > 0){
				$result	= $this->db->limit($limit)->offset($offset)->get()->result();
			}else{
				$result	= $this->db->get()->result();
			}

			//echo $this->db->last_query(); 
			
			return $result;
		}
		else
		{
			//sort by alphabetically by default
			$this->db->order_by('name', 'ASC');
			$result	= $this->db->get('products');

			return $result->result();
		}
	}
	
	function count_all_products()
	{
		return $this->db->count_all_results('products');
	}
	
	function count_products($id)
	{
		return $this->db->select('product_id')->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('category_id'=>$id, 'enabled'=>1))->count_all_results();
	}
	function count_bestseller_products()
	{
		$where_in = array(127,106,128);
		return $this->db->select('product_id')->from('category_products')->join('products', 'category_products.product_id=products.id')->where_in('category_id', $where_in)->where('enabled', '1')->count_all_results();
	}

	function get_product($id, $related=true,$grouped=true,$combo=true,$attech_combo=true,$buy_get=true)
	{
		$result	= $this->db->get_where('products', array('id'=>$id))->row();
		if(!$result)
		{
			return false;
		}

		$related	= json_decode($result->related_products);
		
		if(!empty($related))
		{
			//build the where
			$where = array();
			foreach($related as $r)
			{
				$where[] = '`id` = '.$r;
			}

			$this->db->where('('.implode(' OR ', $where).')', null);
			$this->db->where('enabled', 1);

			$result->related_products	= $this->db->get('products')->result();
		}
		else
		{
			$result->related_products	= array();
		}
		
		
		$result->categories			= $this->get_product_categories($result->id);
		
		return $result;
	}

	function get_product_categories($id)
	{
		return $this->db->where('product_id', $id)->join('categories', 'category_id = categories.id')->get('category_products')->result();
	}

	function get_slug($id)
	{
		return $this->db->get_where('products', array('id'=>$id))->row()->slug;
	}

	function check_slug($str, $id=false)
	{
		$this->db->select('slug');
		$this->db->from('products');
		$this->db->where('slug', $str);
		if ($id)
		{
			$this->db->where('id !=', $id);
		}
		$count = $this->db->count_all_results();

		if ($count > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function save($product, $options=false, $categories=false)
	{
		if ($product['id'])
		{
			$this->db->where('id', $product['id']);
			$this->db->update('products', $product);

			$id	= $product['id'];
		}
		else
		{
			$this->db->insert('products', $product);
			$id	= $this->db->insert_id();
		}

		//loop through the product options and add them to the db
		if($options !== false)
		{
			$obj =& get_instance();
			$obj->load->model('Option_model');

			// wipe the slate
			$obj->Option_model->clear_options($id);

			// save edited values
			$count = 1;
			foreach ($options as $option)
			{
				$values = $option['values'];
				unset($option['values']);
				$option['product_id'] = $id;
				$option['sequence'] = $count;

				$obj->Option_model->save_option($option, $values);
				$count++;
			}
		}
		
		if($categories !== false)
		{
			if($product['id'])
			{
				//get all the categories that the product is in
				$cats	= $this->get_product_categories($id);
				
				//generate cat_id array
				$ids	= array();
				foreach($cats as $c)
				{
					$ids[]	= $c->id;
				}

				//eliminate categories that products are no longer in
				foreach($ids as $c)
				{
					if(!in_array($c, $categories))
					{
						$this->db->delete('category_products', array('product_id'=>$id,'category_id'=>$c));
					}
				}
				
				//add products to new categories
				foreach($categories as $c)
				{
					if(!in_array($c, $ids))
					{
						$this->db->insert('category_products', array('product_id'=>$id,'category_id'=>$c));
					}
				}
			}
			else
			{
				//new product add them all
				foreach($categories as $c)
				{
					$this->db->insert('category_products', array('product_id'=>$id,'category_id'=>$c));
				}
			}
		}
		
		
		//return the product id
		return $id;
	}
	
	function delete_product($id)
	{
		// delete product 
		$this->db->where('id', $id);
		$this->db->delete('products');

		//delete references in the product to category table
		$this->db->where('product_id', $id);
		$this->db->delete('category_products');
		
		// delete coupon reference
		$this->db->where('product_id', $id);
		$this->db->delete('coupons_products');

	}
	
	function delete_product_archive($id)
	{
		$product['archive']=1;
		// delete product 
		$this->db->where('id', $id);
		$this->db->update('products', $product);

		

	}

	function add_product_to_category($product_id, $optionlist_id, $sequence)
	{
		$this->db->insert('product_categories', array('product_id'=>$product_id, 'category_id'=>$category_id, 'sequence'=>$sequence));
	}
	
	function feture_products_from_top($limit=false, $offset=false, $by=false, $sort=false,$new_tax,$minPrice=false,$maxPrice=false){
		$results		= array();
		//count total
		$this->db->select('*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false);
		if(!empty($catid)){	
			$this->db->join('category_products', 'category_products.product_id=products.id', 'right');
			$this->db->where('category_products.category_id', $catid);
		}
		$this->db->where('enabled', 1);
		
		if(!empty($term)){
			$this->db->where('(name LIKE "%'.$term.'%" OR excerpt LIKE "%'.$term.'%")');
		}
		$results['count']	= $this->db->count_all_results('products');
		//count total

		
		
		//fetch records
		$this->db->select('*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false);
		if(!empty($catid)){	
			$this->db->join('category_products', 'category_products.product_id=products.id', 'right');
			$this->db->where('category_products.category_id', $catid);
		}
		$this->db->where('enabled', 1);
		
		if(!empty($term)){
			$this->db->where('(name LIKE "%'.$term.'%" OR excerpt LIKE "%'.$term.'%")');
		}
		

			
			if($brand)
			{
				$this->db->where("shop_by_brand IN($brand)");
			}
			
		if($by && $sort)
		{
			$this->db->order_by($by, $sort);
		}
		
		$results['products']	= $this->db->get('products', $limit, $offset)->result();
		//echo $this->db->last_query();
		return $results;
		
	}
	
	function new_products_from_top($limit=false, $offset=false, $by=false, $sort=false,$new_tax,$minPrice=false,$maxPrice=false){
		$results		= array();
		//count total
		$this->db->select('*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false);
		if(!empty($catid)){	
			$this->db->join('category_products', 'category_products.product_id=products.id', 'right');
			$this->db->where('category_products.category_id', $catid);
		}
		$this->db->where('enabled', 1);
		
		if(!empty($term)){
			$this->db->where('(name LIKE "%'.$term.'%" OR excerpt LIKE "%'.$term.'%")');
		}
		$results['count']	= $this->db->count_all_results('products');
		//count total

		
		
		//fetch records
		$this->db->select('*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false);
		if(!empty($catid)){	
			$this->db->join('category_products', 'category_products.product_id=products.id', 'right');
			$this->db->where('category_products.category_id', $catid);
		}
		$this->db->where('enabled', 1);
		
		if(!empty($term)){
			$this->db->where('(name LIKE "%'.$term.'%" OR excerpt LIKE "%'.$term.'%")');
		}
		

			
			if($brand)
			{
				$this->db->where("shop_by_brand IN($brand)");
			}
			
		if($by && $sort)
		{
			$this->db->order_by($by, $sort);
		}
		
		$results['products']	= $this->db->get('products', $limit, $offset)->result();
		//echo $this->db->last_query();
		return $results;
		
	}

	function search_products($term, $limit=false, $offset=false, $by=false, $sort=false,$booktype=false,$publishertype=false,$new_tax,$minPrice=false,$maxPrice=false,$brand=false,$manufacturer=false,$top_brand=false, $catid){
		$results		= array();
		
		if($top_brand)
		{
			$this->db->select('*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false);
			//this one counts the total number for our pagination
			$this->db->where('enabled', 1);
			
			$results['count']	= $this->db->count_all_results('products');


			$this->db->select('*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false);
			//this one gets just the ones we need.
			
			$this->db->where('enabled', 1);

				
				
				if($by && $sort)
				{
					$this->db->order_by($by, $sort);
				}
			
			$results['products']	= $this->db->get('products', $limit, $offset)->result();
			//echo $this->db->last_query();die;
			return $results;
		}
		else
		{
			
			//count total
			$this->db->select('*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false);
			if(!empty($catid)){	
				$this->db->join('category_products', 'category_products.product_id=products.id', 'right');
				$this->db->where('category_products.category_id', $catid);
			}
			$this->db->where('enabled', 1);
			
			if(!empty($term)){
				$this->db->where('(name LIKE "%'.$term.'%" OR excerpt LIKE "%'.$term.'%")');
			}
			$results['count']	= $this->db->count_all_results('products');
			//count total

			
			
			//fetch records
			$this->db->select('*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false);
			if(!empty($catid)){	
				$this->db->join('category_products', 'category_products.product_id=products.id', 'right');
				$this->db->where('category_products.category_id', $catid);
			}
			$this->db->where('enabled', 1);
			
			if(!empty($term)){
				$this->db->where('(name LIKE "%'.$term.'%" OR excerpt LIKE "%'.$term.'%")');
			}
			

				
			if($by && $sort)
			{
				$this->db->order_by($by, $sort);
			}
			
			$results['products']	= $this->db->get('products', $limit, $offset)->result();
			//echo $this->db->last_query();
			return $results;
		}
		
	}

	// Build a cart-ready product array
	function get_cart_ready_product($id, $quantity=false)
	{
		$product	= $this->db->get_where('products', array('id'=>$id))->row();
		
		//unset some of the additional fields we don't need to keep
		if(!$product)
		{
			return false;
		}
		
		$product->base_price	= $product->price;
		
		if ($product->saleprice != 0.00)
		{ 
			$product->price	= $product->saleprice;
		}
		
		
		// Some products have n/a quantity, such as downloadables
		//overwrite quantity of the product with quantity requested
		if (!$quantity || $quantity <= 0 || $product->fixed_quantity==1)
		{
			$product->quantity = 1;
		}
		else
		{
			$product->quantity = $quantity;
		}
		
		
		
		return (array)$product;
	}
	function getHomeProducts()
	{
		$this->db->where('home_enabled','1');
		$result	= $this->db->get('products');
		return $result->result();
	}

	
	
	function search_products_by_ajax($post){
		$term = $post['search'];
		$category_id = $post['id'];
		
		$results = array();
		$this->db->select('products.name, products.slug, products.excerpt, products.id, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false)->from('category_products')->join('products', 'category_products.product_id=products.id');
		
		//$this -> db -> select('name, slug, excerpt', 'id');
		if($category_id){
			$this->db->where('category_id', $category_id);
		}
		//this one gets just the ones we need.
		$this -> db -> where('enabled', 1);
		$this -> db -> where('(products.name LIKE "%'.$term.'%" OR products.description LIKE "%'.$term.'%" OR products.excerpt LIKE "%'.$term.'%" OR products.sku LIKE "%'.$term.'%")');
		
		
		$results = $this -> db -> get() -> result();
		return $results;
	}
	
	function getFeaturedProducts(){
		//to fetch first fetaure products if not available then return regular products
		$this->db->where('home_enabled','1');
		$this->db->order_by('rand()');
		$this->db->limit(10,0);
		$result	= $this->db->get('products');
        if($result->num_rows() > 0){
            return $result->result();
        }else{
            $this->db->where('enabled','1');
			$this->db->order_by('rand()');
			$this->db->limit(10,0);
			$default_result	= $this->db->get('products');
			return $default_result->result();
        }
	}
	
	function getLatestProducts(){
		$this->db->where('enabled','1');
		$result	= $this->db->get('products');
		$this->db->order_by('id','DESC');
		$this->db->limit(10,0);
		return $result->result();
	}
	
	function getBestSellerProducts(){

		//SELECT category_products.*, products.*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price FROM (`category_products`) JOIN `products` ON `category_products`.`product_id`=`products`.`id` WHERE (`category_id` = '127' && `category_id` = '106' && `category_id` = '128') AND `enabled` = 1 ORDER BY `products`.`id` DESC LIMIT 5


		$this->db->where('enabled','1');
		$this->db->where('is_basket','0');
		$this->db->order_by('rand()');
		$this->db->limit(12,0);
		$result	= $this->db->get('products');
		return $result->result();
	}


    function getBestSellerProducts_P()
	{ 
		$this->db->select('category_products.*, products.*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false)->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('category_id'=>127, 'enabled'=>1));
		$this->db->order_by('rand()');
		$this->db->limit(4);
        $result1	= $this->db->get()->result();


        $this->db->select('category_products.*, products.*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false)->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('category_id'=>106, 'enabled'=>1));
		$this->db->order_by('rand()');
		$this->db->limit(4);
        $result2	= $this->db->get()->result();


        $this->db->select('category_products.*, products.*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false)->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('category_id'=>128, 'enabled'=>1));
		$this->db->order_by('rand()');
		$this->db->limit(4);
        $result3	= $this->db->get()->result();
        
        $result = array_merge($result1, $result2, $result3);
        return ($result);
		 
	}

	function delete_from_compr($id){
		$cust_id = $this->customer['id'];
		$this->db->delete('compare', array('product_id' => $id, 'user_id' => $cust_id));
	}
	
	function order_data($order_id){
		$order_on  = $this->db->get_where('orders', array('order_number'=>$order_id))->row();		
		$date =  $order_on -> ordered_on;
		$date = strtotime($date);
		$order_on->format_data = date("F d, Y", $date);
		//print_r($order_on);
		return $order_on;
	}
	
	function getSpecialOfferProducts(){
		$this->db->where('enabled','1');
		$this->db->order_by('rand()');
		$this->db->limit(10,0);
		$result	= $this->db->get('products');
		return $result->result();
	}
	
	function getSpecialDealProducts(){
		$this->db->where('enabled','1');
		$this->db->order_by('id','DESC');
		$this->db->limit(10,0);
		$result	= $this->db->get('products');
		return $result->result();
	}
	
	function get_hot_deals_products(){
		$ctime = date('Y-m-d', time());
		$this->db->select('products.*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false)->from('products')->where(array('enabled'=>1));
	
		$this->db->where('products.hotdeals_from <', $ctime);
		$this->db->where('products.hotdeals_to >', $ctime);
		$this->db->order_by('products.id', 'DESC');
		$result	= $this->db->get()->result();
		return $result;
	}
	
	function save_ask_question($data){
		$this->db->insert('ask_question', $data);
		$id	= $this->db->insert_id();
		return $id;
	}
	function get_category_product($id){
		
		$this->db->select('category_products.*, products.*')->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('category_id'=>$id, 'enabled'=>1));
		$result	= $this->db->get()->result();
	    return $result;
	}
}
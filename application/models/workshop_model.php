<?php
Class Workshop_model extends CI_Model
{
	//workshop section start here
	function getWorkshopList(){
		$this->db->select('workshop.*');
		$this->db->from('workshop');
		$this->db->order_by('workshop_date', 'DESC');
		$result = $this->db->get();
		return $result->result();
	}
	function getWorkshopListByMonthYear($month,$year){
		$this->db->select('workshop.*');
		$this->db->from('workshop');
		$this->db->where('month(workshop_date)', $month);
		$this->db->where('year(workshop_date)', $year);
		$result = $this->db->get();
		return $result->result();
	}
	function workshop_chk_details($date){
		$this->db->select('workshop.*');
		$this->db->from('workshop');
		$this->db->where('workshop_date', $date);
		$result = $this->db->get();
		return $result->row();
	}
	
	function changeWorkshopStatus($id, $status){
        $data = array(
               'status' => $status
        );
        $this->db->where('id', $id);
        return $this->db->update('workshop', $data);
    }
	
	
    function delete_workshop($id){
    	$this->db->where('id', $id);
        return $this->db->delete('workshop');
    }
	
    function workshopDetails($test_id){
        $this->db->select('*');
        $this->db->from('workshop');
        $this->db->where('id',$test_id);
        $result = $this->db->get();
        return $result->row();
    }
	
	function save_workshop($save){
		if ($save['id']){
			$this->db->where('id', $save['id']);
			$this->db->update('workshop', $save);
			$id	= $save['id'];
		}else{
			$this->db->insert('workshop', $save);
			$id	= $this->db->insert_id();
		}		
		//return the workshop id
		return $id;
	}
	function book_workshop($save){
		$this->db->insert('workshop_booking', $save);
		$id	= $this->db->insert_id();
		
		return $id;
	}
	function get_book_workshop($id){
		$this->db->select('*');
        $this->db->from('workshop_booking');
        $this->db->where('id',$id);
        $result = $this->db->get();
        return $result->row();
	}
	function count_booked_workshop($id){
		$this->db->select('*');
        $this->db->from('workshop_booking');
        $this->db->where('workshop_id',$id);
        return $this->db->count_all_results();
	}
}
<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'naturalmama');

/** MySQL database username */
define('DB_USER', 'naturalmama');

/** MySQL database password */
define('DB_PASSWORD', 'naturalmama@123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '1u<uJnxgD@KF){`=^g(T0buZ?ec>soSR/N?F=ry_{xzvmDOTfE%oHXru.7BG>39E');
define('SECURE_AUTH_KEY',  'OR{bF9LZ2YlDp*/9oN)QIVZGghtt{/Dr%1iM(IIStcyvzN~cEX?SK[S!ay{iWI/i');
define('LOGGED_IN_KEY',    '3vghuD)Dw3LDtP|K1n$tBatxS<, <<|,7QH+O=`o!*s9kO#1Ys?wYpzBuEV?,qfA');
define('NONCE_KEY',        '30<Bpc=GSBhKU|g o}v7i-*#p(tK57MV:h.Gn2UT#..p@A!A?|3|lRp.dWU3+rcB');
define('AUTH_SALT',        '1PDC.5V+_f`{gcUHeDj{G-79Hx{*c`CxeNc%=bWS0A)AI %0]9^SApaKL%#n&+ea');
define('SECURE_AUTH_SALT', 'Fn0u+b1gO6+>J*4 RJSmrp|E47 uD^rIKpE)~y7ekD:oL<eC!v_CXH+@XYsOP3[|');
define('LOGGED_IN_SALT',   'k-V6=%?d @gaPL!CcKO8hJcw/X NIEvsJxl!!NhK#{~_ryebOtC9*4]wRg8NeG|}');
define('NONCE_SALT',       'WwWi]{4RDX)| /KT`DP`E#=Zy} &Cj7-;=[hy)x5bdmAYOCq_W7&8}xS1S/Mvq|X');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
